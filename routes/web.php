<?php

use App\Http\Controllers\Pendaftaran\PendaftaranController;
use App\Http\Controllers\ValidasiBerkas\ValidasiBerkasController;
use App\Http\Controllers\ValidasiKabid\ValidasiKabidController;
use App\Http\Controllers\ValidasiKaban\ValidasiKabanController;
use App\Http\Controllers\Pembayaran\PembayaranController;
use App\Http\Controllers\Activity\HistoryActivityController;
use App\Http\Controllers\CetakLaporan\CetakLaporanController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\LihatData\LihatDataController;
use App\Http\Controllers\CetakSSPD\CetakSSPDController;
use App\Http\Controllers\CaptchaValidationController;
use App\Http\Controllers\Permissions\{
    AssignController,
    PermissionController,
    RoleController,
    UserController
};
use App\Http\Controllers\Setting\{
    SettingKecamatanController,
    SettingKelurahanController,
    SettingLoginBackground,
    SettingPejabatController,
    SettingUserController,
    SettingTarifBphtbController,
    SettingHakTanahController,
    SettingJenisTransaksiController,
    SettingTarifNpoptkpController,
    SettingNotarisController,
    SettingPemdaController,
    SettingPersyaratanController,
    SettingPersentasewajarController,
    SettingHargaAcuanController,
    SettingAcuanTanahController,
    SettingJenisFasilitasController,
    SettingJenisDokumenTanahController,
    SettingTargetController,
    SettingTargetStatusController,
    SettingTempoController,
    SettingPassController,
    SettingBrivaController,
    SettingMenukabanController,
    SettingMenutunggakanpbbController,
    SettingMenutunggakantahunpbbController,
    SettingMenubrivaController,
    SettingMenukonekpbbController,
    SettingMenuuploadController,
    SettingMenugmapController,
    SettingMenucaptchaController,
    SettingMenupelayananController
};
use App\Http\Controllers\Informasiop\InformasiopController;
use App\Http\Controllers\HistoryTransaksi\HistoryTransaksiController;
use App\Http\Controllers\Pelaporan\{
    LaporBulananController,
    PelaporanAjbController,
    DendaLaporajbController,
    DendaAjbController
};
use App\Http\Controllers\Pelayanan\{
    PelayananKeringananController,
    PelayananPembatalanController,
    PelayananPenundaanController,
    PelayananAngsuranController,
    PelayananMutasiController
};
use App\Http\Controllers\Approved\{
    ApprovedKeringananController,
    ApprovedPembatalanController,
    ApprovedPenundaanController,
    ApprovedAngsuranController
};
use App\Http\Controllers\Skpdkb\SkpdkbController;
use App\Http\Controllers\Skpdkbt\SkpdkbtController;
use App\Http\Controllers\Bpn\BpnController;
use App\Http\Controllers\HargaWajar\HargaWajarController;
use App\Http\Controllers\Sismiop\SismiopController;
use App\Models\Pelaporan\LaporBulananHead;
use Facade\FlareClient\Http\Response;
use Hamcrest\Core\Set;
use Illuminate\Support\Facades\{
    Artisan,
    Route,
    Auth
};
use App\Http\Controllers\KppPratama\KppPratamaController;
use App\Http\Controllers\Pemeriksaan\PemeriksaanController;

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('/lihatdata/{id}', [LihatDataController::class, 'lihatdata'])->name('lihatdata');
Route::get('/lihatdata/validasi/{uuid}', [LihatDataController::class, 'validasi']);

Route::get('{spt}/cetaksspdbphtb', [CetakSSPDController::class, 'cetaksspdbphtb'])->name('cetaksspdbphtb');
//Route::get('contact-form-captcha', [App\Http\Controllers\Auth\LoginController::class, 'index']);
Route::post('captcha-validation', [App\Http\Controllers\Auth\LoginController::class, 'capthcaFormValidate']);
Route::get('reload-captcha', [App\Http\Controllers\Auth\LoginController::class, 'reloadCaptcha']);


Route::group(['middleware' => 'auth'], function () {
    Route::get('pelaporan', function () {
        return view('pelaporan.index');
    });

    Route::get('pelayanan', function () {
        return view('pelayanan.index');
    });

    Route::get('informasi', function () {
        return view('informasi.index');
    });

    Route::get('validasi', function () {
        return view('validasi.index');
    });

    Route::get('penetapan', function () {
        return view('penetapan.index');
    });
    
    Route::prefix('pendaftaran')->middleware('permission:MenuPendaftaran')->group(function () {
        Route::get('', [PendaftaranController::class, 'index']);
        Route::get('formtambah', [PendaftaranController::class, 'formtambah']);
        Route::post('datagrid', [PendaftaranController::class, 'datagrid']);

        Route::get('formtambah', [PendaftaranController::class, 'formtambah']);

        Route::post('simpantahappertama', [PendaftaranController::class, 'simpantahappertama']);
        Route::get('tahappertama/{uuid}', [PendaftaranController::class, 'tahappertama']);
        Route::patch('tahappertama/{uuid}', [PendaftaranController::class, 'update']);

        Route::post('simpantahapkedua', [PendaftaranController::class, 'simpantahapkedua']);
        Route::get('tahapkedua/{uuid}', [PendaftaranController::class, 'tahapkedua']);
        Route::patch('tahapkedua/{uuid}', [PendaftaranController::class, 'update']);

        Route::post('simpantahapketiga', [PendaftaranController::class, 'simpantahapketiga']);
        Route::get('tahapketiga/{uuid}', [PendaftaranController::class, 'tahapketiga']);
        Route::patch('tahapketiga/{uuid}', [PendaftaranController::class, 'update']);

        Route::post('simpantahapkeempat', [PendaftaranController::class, 'simpantahapkeempat']);
        Route::get('tahapkeempat/{uuid}', [PendaftaranController::class, 'tahapkeempat']);
        Route::patch('tahapkeempat/{uuid}', [PendaftaranController::class, 'update']);

        Route::post('simpantahapkelima', [PendaftaranController::class, 'simpantahapkelima']);
        Route::get('tahapkelima/{uuid}', [PendaftaranController::class, 'tahapkelima']);
        Route::get('{spt}/detail/{t_idspt}', [PendaftaranController::class, 'cek_detail_history_tahun_berjalan']);
        Route::patch('tahapkelima/{uuid}', [PendaftaranController::class, 'update']);

        Route::post('simpantahapkeenam', [PendaftaranController::class, 'simpantahapkeenam']);
        Route::get('tahapkeenam/{uuid}', [PendaftaranController::class, 'tahapkeenam']);
        Route::patch('tahapkeenam/{uuid}', [PendaftaranController::class, 'update']);

        Route::post('simpantahapketujuh', [PendaftaranController::class, 'simpantahapketujuh']);
        Route::get('tahapketujuh/{uuid}', [PendaftaranController::class, 'tahapketujuh']);
        Route::patch('tahapketujuh/{uuid}', [PendaftaranController::class, 'update']);

        Route::post('cekdatakelurahan', [PendaftaranController::class, 'cekdatakelurahan']);
        Route::post('ceknamakelurahan', [PendaftaranController::class, 'ceknamakelurahan']);
        Route::post('cektunggakanpbb', [PendaftaranController::class, 'cektunggakanpbb']);
        Route::post('datanop', [PendaftaranController::class, 'datanop']);

        Route::post('fetchpersyaratan', [PendaftaranController::class, 'fetchpersyaratan']);
        Route::post('uploadpersyaratan', [PendaftaranController::class, 'uploadpersyaratan']);
        Route::post('edituploadpersyaratan', [PendaftaranController::class, 'edituploadpersyaratan']);
        Route::post('deleteuploadpersyaratan', [PendaftaranController::class, 'deleteuploadpersyaratan']);
        Route::post('updateuploadpersyaratan', [PendaftaranController::class, 'updateuploadpersyaratan']);


        Route::post('fetchfotoobjek', [PendaftaranController::class, 'fetchfotoobjek']);
        Route::post('uploadfotoobjek', [PendaftaranController::class, 'uploadfotoobjek']);
        Route::post('edituploadfotoobjek', [PendaftaranController::class, 'edituploadfotoobjek']);
        Route::post('deleteuploadfotoobjek', [PendaftaranController::class, 'deleteuploadfotoobjek']);
        Route::post('updateuploadfotoobjek', [PendaftaranController::class, 'updateuploadfotoobjek']);

        Route::get('{spt}/cetaksspdbphtb/{id}', [PendaftaranController::class, 'cetaksspdbphtb']);
        Route::get('{spt}/cetaksuratpernyataan', [PendaftaranController::class, 'cetaksuratpernyataan']);

        Route::get('{spt}/cetakbahpbphtb', [PendaftaranController::class, 'cetakbahpbphtb']);

        Route::post('rinciannikbersama', [PendaftaranController::class, 'rinciannikbersama']);
        Route::post('hapussatunikbersama', [PendaftaranController::class, 'hapussatunikbersama']);

        Route::get('detail/{id}', [PendaftaranController::class, 'detail']);
        Route::get('batal/{id}', [PendaftaranController::class, 'cancel']);
        Route::get('delete/{id}', [PendaftaranController::class, 'destroy']);
    });

    Route::prefix('validasi-berkas')->middleware('permission:MenuValidasiBerkas')->group(function () {
        Route::get('', [ValidasiBerkasController::class, 'index']);
        Route::post('datagrid', [ValidasiBerkasController::class, 'datagrid']);
        Route::post('datagridpilihspt', [ValidasiBerkasController::class, 'datagridpilihspt']);
        Route::post('simpanvalidasiberkas', [ValidasiBerkasController::class, 'simpanvalidasiberkas']);
        Route::get('formvalidasiberkas/{uuid}', [ValidasiBerkasController::class, 'formvalidasiberkas']);
        Route::patch('formvalidasiberkas/{uuid}', [ValidasiBerkasController::class, 'formvalidasiberkas']);
        Route::get('detail/{id}', [ValidasiBerkasController::class, 'detail']);
        Route::get('delete/{id}', [ValidasiBerkasController::class, 'destroy']);
    });

    Route::prefix('validasi-kabid')->middleware('permission:MenuValidasiKabid')->group(function () {
        Route::get('', [ValidasiKabidController::class, 'index']);
        Route::post('datagrid', [ValidasiKabidController::class, 'datagrid']);
        Route::post('datagridbelum', [ValidasiKabidController::class, 'datagridbelum']);
        Route::get('pilihspt', [ValidasiKabidController::class, 'pilihspt']);
        Route::post('datagridpilihspt', [ValidasiKabidController::class, 'datagridpilihspt']);

        Route::post('simpanvalidasikabid', [ValidasiKabidController::class, 'simpanvalidasikabid']);
        Route::get('formvalidasikabid/{uuid}', [ValidasiKabidController::class, 'formvalidasikabid']);
        Route::patch('formvalidasikabid/{uuid}', [ValidasiKabidController::class, 'update']);

        Route::post('simpanvalidasikeduakabid', [ValidasiKabidController::class, 'simpanvalidasikeduakabid']);
        Route::get('formvalidasikeduakabid/{uuid}', [ValidasiKabidController::class, 'formvalidasikeduakabid']);
        Route::patch('formvalidasikeduakabid/{uuid}', [ValidasiKabidController::class, 'update']);

        Route::get('detail/{id}', [ValidasiKabidController::class, 'detail']);
        Route::get('delete/{id}', [ValidasiKabidController::class, 'destroy']);
    });

    Route::prefix('validasi-kaban')->middleware('permission:MenuValidasiKaban')->group(function () {
        Route::get('', [ValidasiKabanController::class, 'index']);
        Route::post('datagrid', [ValidasiKabanController::class, 'datagrid']);
        Route::post('datagridbelum', [ValidasiKabanController::class, 'datagridbelum']);

        Route::post('simpanvalidasi', [ValidasiKabanController::class, 'simpanvalidasi']);
        Route::get('formvalidasi/{uuid}', [ValidasiKabanController::class, 'formvalidasi']);
        Route::patch('formvalidasi/{uuid}', [ValidasiKabanController::class, 'update']);

        Route::get('detail/{id}', [ValidasiKabanController::class, 'detail']);
        Route::get('delete/{id}', [ValidasiKabanController::class, 'destroy']);
    });

    Route::prefix('pemeriksaan')->middleware('permission:MenuPemeriksaan')->group(function () {
        Route::get('', [PemeriksaanController::class, 'index']);
        Route::post('datagrid', [PemeriksaanController::class, 'datagrid']);
        Route::post('datagrid-belum', [PemeriksaanController::class, 'datagridBelum']);
        Route::post('hitung-pemeriksaan', [PemeriksaanController::class, 'hitungPemeriksaanJson']);
        Route::post('simpanpemeriksaan', [PemeriksaanController::class, 'simpanpemeriksaan']);
        Route::get('formpemeriksaan/{uuid}', [PemeriksaanController::class, 'formpemeriksaan']);
        Route::patch('formpemeriksaan/{uuid}', [PemeriksaanController::class, 'update']);

        Route::get('detail/{id}', [PemeriksaanController::class, 'detail']);
        Route::get('delete/{id}', [PemeriksaanController::class, 'destroy']);
    });

    Route::prefix('pembayaran')->middleware('permission:MenuPembayaran')->group(function () {
        Route::get('', [PembayaranController::class, 'index']);
        Route::post('datagrid', [PembayaranController::class, 'datagrid']);
        Route::get('pilihspt', [PembayaranController::class, 'pilihspt']);
        Route::post('datagridpilihspt', [PembayaranController::class, 'datagridpilihspt']);
        Route::post('datagridbelum', [PembayaranController::class, 'datagridbelum']);

        Route::post('simpanpembayaran', [PembayaranController::class, 'simpanpembayaran']);
        Route::get('formpembayaran/{uuid}', [PembayaranController::class, 'formpembayaran']);
        Route::patch('formpembayaran/{uuid}', [PembayaranController::class, 'update']);

        Route::get('detail/{id}', [PembayaranController::class, 'detail']);
        Route::get('delete/{id}', [PembayaranController::class, 'destroy']);
    });

    Route::prefix('pelaporan-ajb')->middleware('permission:PelaporanAjb')->group(function () {
        Route::get('', [PelaporanAjbController::class, 'index'])->name('pelaporan-ajb');
        Route::post('datagridbelum', [PelaporanAjbController::class, 'datagridbelum']);
        Route::post('datagrid', [PelaporanAjbController::class, 'datagrid']);
        Route::post('datagrid_spt', [PelaporanAjbController::class, 'datagrid_spt']);
        Route::get('detail/{pelaporan_ajb}', [PelaporanAjbController::class, 'detail'])->name('pelaporan-ajb.detail');
        Route::get('detile_spt', [PelaporanAjbController::class, 'getDataSpt']);
        Route::get('get_detile_spt/{id_spt}', [PelaporanAjbController::class, 'get_detile_spt']);
        Route::get('{spt}/create', [PelaporanAjbController::class, 'create'])->name('pelaporan-ajb.create');
        Route::post('{spt}/store', [PelaporanAjbController::class, 'store'])->name('pelaporan-ajb.store');

        Route::get('{pelaporan_ajb}/edit', [PelaporanAjbController::class, 'edit'])->name('pelaporan-ajb.edit');
        Route::put('{pelaporan_ajb}', [PelaporanAjbController::class, 'update'])->name('pelaporan-ajb.update');

        Route::delete('{pelaporan_ajb}', [PelaporanAjbController::class, 'destroy'])->name('pelaporan-ajb.delete');
        Route::get('export_xls', [PelaporanAjbController::class, 'export_xls'])->name('pelaporan-ajb.export_xls');
        Route::get('export_pdf', [PelaporanAjbController::class, 'export_pdf'])->name('pelaporan-ajb.export_pdf');
    });

    Route::prefix('lapor-bulanan-ajb')->middleware('permission:PelaporanAjb')->group(function () {
        Route::get('', [LaporBulananController::class, 'index'])->name('lapor-bulanan-ajb');
        Route::post('datagrid', [LaporBulananController::class, 'datagrid']);
        Route::post('datagrid-ajb', [LaporBulananController::class, 'datagridAjb']);
        Route::get('detail/{lapor_ajb}', [LaporBulananController::class, 'detail'])->name('lapor-bulanan-ajb.detail');
        Route::get('create', [LaporBulananController::class, 'create'])->name('lapor-bulanan-ajb.create');
        Route::post('', [LaporBulananController::class, 'store'])->name('lapor-bulanan-ajb.store');
        Route::delete('{lapor_ajb}', [LaporBulananController::class, 'destroy'])->name('lapor-bulanan-ajb.delete');
        Route::get('export_xls', [LaporBulananController::class, 'export_xls'])->name('lapor-bulanan-ajb.export_xls');
        Route::get('export_pdf', [LaporBulananController::class, 'export_pdf'])->name('lapor-bulanan-ajb.export_pdf');
    });

    Route::prefix('denda-laporan-ajb')->middleware('permission:DendaLaporanAjb')->group(function () {
        Route::get('', [DendaLaporajbController::class, 'index'])->name('denda-laporan-ajb');
        Route::post('datagrid', [DendaLaporajbController::class, 'datagrid']);
        Route::get('create', [DendaLaporajbController::class, 'create']);
        Route::post('datagrid_lapor_bulanan', [DendaLaporajbController::class, 'datagrid_lapor_bulanan']);
        Route::get('caridata/{id}', [DendaLaporajbController::class, 'get_data_laporan']);
        Route::get('detile/{id}', [DendaLaporajbController::class, 'detail']);
        Route::get('{denda_lapor}/edit', [DendaLaporajbController::class, 'edit']);
        Route::put('{denda_lapor}', [DendaLaporajbController::class, 'update']);
        Route::post('', [DendaLaporajbController::class, 'store'])->name('denda-laporan-ajb.store');
        Route::delete('{id}', [DendaLaporajbController::class, 'destroy'])->name('denda-laporan-ajb.delete');
        Route::get('cetak_tagihan', [DendaLaporajbController::class, 'cetak_surat_tagihan']);
    });

    Route::prefix('kpp-pratama')->middleware('permission:KppPratama')->group(function () {
        Route::get('', [KppPratamaController::class, 'index']);
        Route::post('datagrid', [KppPratamaController::class, 'datagrid']);
    });

    Route::prefix('denda-ajb')->middleware('permission:DendaAjb')->group(function () {
        Route::get('', [DendaAjbController::class, 'index']);
        Route::get('create', [DendaAjbController::class, 'create']);
        Route::post('datagrid', [DendaAjbController::class, 'datagrid']);
        Route::post('datagrid_lapor_bulanan', [DendaAjbController::class, 'datagrid_lapor_bulanan']);
        Route::get('caridata/{id}', [DendaAjbController::class, 'get_data_laporan']);
        Route::post('', [DendaAjbController::class, 'store']);
        Route::get('detile/{id}', [DendaAjbController::class, 'detail']);
        Route::delete('{id}', [DendaAjbController::class, 'destroy']);
        Route::get('cetak_tagihan', [DendaAjbController::class, 'cetak_surat_tagihan']);
    });

    Route::prefix('skpdkb')->middleware('permission:Skpdkb')->group(function () {
        Route::get('', [SkpdkbController::class, 'index']);
        Route::post('datagrid', [SkpdkbController::class, 'datagrid']);
        Route::get('ketetapan', [SkpdkbController::class, 'ketetapan']);
        Route::post('datagrid_ketetapan', [SkpdkbController::class, 'datagrid_ketetapan']);
        Route::get('detail/{id}', [SkpdkbController::class, 'detail']);
        Route::get('{id}/create', [SkpdkbController::class, 'create']);
        Route::post('store', [SkpdkbController::class, 'store']);
        Route::get('delete/{id}', [SkpdkbController::class, 'destroy']);
        Route::get('caridata/{id}', [SkpdkbController::class, 'caridata']);
        Route::get('{id}/hitung/{key}', [SkpdkbController::class, 'hitung']);
        Route::get('cetaksurat/{id}', [SkpdkbController::class, 'cetaksurat']);
        Route::get('export_xls', [SkpdkbController::class, 'export_xls'])->name('skpdkb.export_xls');
        Route::get('export_pdf', [SkpdkbController::class, 'export_pdf'])->name('skpdkb.export_pdf');
    });

    Route::prefix('skpdkbt')->middleware('permission:Skpdkbt')->group(function () {
        Route::get('', [SkpdkbtController::class, 'index']);
        Route::post('datagrid', [SkpdkbtController::class, 'datagrid']);
        Route::get('ketetapan', [SkpdkbtController::class, 'ketetapan']);
        Route::post('datagrid_ketetapan', [SkpdkbtController::class, 'datagrid_ketetapan']);
        Route::get('detail/{id}', [SkpdkbtController::class, 'detail']);
        Route::get('{id}/create', [SkpdkbtController::class, 'create']);
        Route::post('store', [SkpdkbtController::class, 'store']);
        Route::get('delete/{id}', [SkpdkbtController::class, 'destroy']);
        Route::get('caridata/{id}', [SkpdkbtController::class, 'caridata']);
        Route::get('{id}/hitung/{key}', [SkpdkbtController::class, 'hitung']);
        Route::get('cetaksurat/{id}', [SkpdkbtController::class, 'cetaksurat']);
    });

    Route::prefix('approved-keringanan')->middleware('permission:ApprovedKeringanan')->group(function () {
        Route::get('', [ApprovedKeringananController::class, 'index']);
        Route::post('datagrid', [ApprovedKeringananController::class, 'datagrid']);
        Route::get('permohonan', [ApprovedKeringananController::class, 'permohonan']);
        Route::post('datagrid_pelayanan', [ApprovedKeringananController::class, 'datagrid_pelayanan']);
        Route::get('detail/{keringanan}', [ApprovedKeringananController::class, 'detail']);
        Route::get('{keringanan}/create', [ApprovedKeringananController::class, 'create']);
        Route::post('store', [ApprovedKeringananController::class, 'store']);
        Route::get('delete/{keringanan}', [ApprovedKeringananController::class, 'destroy']);
        Route::get('caridata/{id}', [ApprovedKeringananController::class, 'caridata']);
        Route::get('{keringanan}/hitung/{id}/{key}', [ApprovedKeringananController::class, 'hitung']);
        Route::get('cetak_surat', [ApprovedKeringananController::class, 'cetak_surat']);
        Route::get('export_xls', [ApprovedKeringananController::class, 'export_xls'])->name('approved_keringanan.export_xls');
        Route::get('export_pdf', [ApprovedKeringananController::class, 'export_pdf'])->name('approved_keringanan.export_pdf');
    });

    Route::prefix('approved-pembatalan')->middleware('permission:ApprovedPembatalan')->group(function () {
        Route::get('', [ApprovedPembatalanController::class, 'index']);
        Route::post('datagrid', [ApprovedPembatalanController::class, 'datagrid']);
        Route::get('permohonan', [ApprovedPembatalanController::class, 'permohonan']);
        Route::post('datagrid_pelayanan', [ApprovedPembatalanController::class, 'datagrid_pelayanan']);
        Route::get('detail/{pembatalan}', [ApprovedPembatalanController::class, 'detail']);
        Route::get('{pembatalan}/create', [ApprovedPembatalanController::class, 'create']);
        Route::post('store', [ApprovedPembatalanController::class, 'store']);
        Route::get('delete/{pembatalan}', [ApprovedPembatalanController::class, 'destroy']);
        Route::get('caridata/{id}', [ApprovedPembatalanController::class, 'caridata']);
        Route::get('export_xls', [ApprovedPembatalanController::class, 'export_xls'])->name('approved_pembatalan.export_xls');
        Route::get('export_pdf', [ApprovedPembatalanController::class, 'export_pdf'])->name('approved_pembatalan.export_pdf');
    });

    Route::prefix('approved-penundaan')->middleware('permission:ApprovedPenundaan')->group(function () {
        Route::get('', [ApprovedPenundaanController::class, 'index']);
        Route::post('datagrid', [ApprovedPenundaanController::class, 'datagrid']);
        Route::get('permohonan', [ApprovedPenundaanController::class, 'permohonan']);
        Route::post('datagrid_pelayanan', [ApprovedPenundaanController::class, 'datagrid_pelayanan']);
        Route::get('detail/{penundaan}', [ApprovedPenundaanController::class, 'detail']);
        Route::get('{penundaan}/create', [ApprovedPenundaanController::class, 'create']);
        Route::post('store', [ApprovedPenundaanController::class, 'store']);
        Route::get('delete/{penundaan}', [ApprovedPenundaanController::class, 'destroy']);
        Route::get('caridata/{id}', [ApprovedPenundaanController::class, 'caridata']);
        Route::get('export_xls', [ApprovedPenundaanController::class, 'export_xls'])->name('approved_penundaan.export_xls');
        Route::get('export_pdf', [ApprovedPenundaanController::class, 'export_pdf'])->name('approved_penundaan.export_pdf');
    });

    Route::prefix('approved-angsuran')->middleware('permission:ApprovedAngsuran')->group(function () {
        Route::get('', [ApprovedAngsuranController::class, 'index']);
        Route::post('datagrid', [ApprovedAngsuranController::class, 'datagrid']);
        Route::get('permohonan', [ApprovedAngsuranController::class, 'permohonan']);
        Route::post('datagrid_pelayanan', [ApprovedAngsuranController::class, 'datagrid_pelayanan']);
        Route::get('detail/{angsuran}', [ApprovedAngsuranController::class, 'detail']);
        Route::get('{angsuran}/create', [ApprovedAngsuranController::class, 'create']);
        Route::post('store', [ApprovedAngsuranController::class, 'store']);
        Route::get('delete/{angsuran}', [ApprovedAngsuranController::class, 'destroy']);
        Route::get('caridata/{id}', [ApprovedAngsuranController::class, 'caridata']);
        Route::get('{angsuran}/rinciangsuran/{id}/{key}/{page}', [ApprovedAngsuranController::class, 'rinciangsuran']);
        Route::get('export_xls', [ApprovedAngsuranController::class, 'export_xls'])->name('approved_angsuran.export_xls');
        Route::get('export_pdf', [ApprovedAngsuranController::class, 'export_pdf'])->name('approved_angsuran.export_pdf');
    });

    Route::prefix('informasiop')->middleware('permission:Informasiop')->group(function () {
        Route::get('', [InformasiopController::class, 'index']);
        Route::get('caridata/{nik}/{npwpd}/{nop}', [InformasiopController::class, 'caridata']);
        Route::get('export_pdf', [InformasiopController::class, 'export_pdf'])->name('cetaktunggakan.export_pdf');
    });

    Route::prefix('harga-wajar')->middleware('permission:MenuHargaWajar')->group(function () {
        Route::get('', [HargaWajarController::class, 'index']);
        Route::get('caridata/{nop}', [HargaWajarController::class, 'caridata']);
        Route::get('export_pdf', [HargaWajarController::class, 'export_pdf']);
    });

    Route::prefix('history-transaksi')->middleware('permission:MenuHistoryTransaksi')->group(function () {
        Route::get('', [HistoryTransaksiController::class, 'index']);
        Route::get('/detail/{t_idspt}', [HistoryTransaksiController::class, 'cek_detail_history_transaksi']);
        Route::get('/carihistorytransaksi', [HistoryTransaksiController::class, 'cari_history_transaksi']);
    });

    Route::prefix('bpn')->middleware('permission:Bpn')->group(function () {
        Route::get('', [BpnController::class, 'index']);
        Route::get('password', [BpnController::class, 'password']);
        Route::patch('{id}/change', [BpnController::class, 'change']);
        Route::get('caridata/{tgltransaksi}', [BpnController::class, 'caridata']);
        Route::post('store', [BpnController::class, 'store']);
        Route::get('map/{tgltransaksi}', [BpnController::class, 'map']);
        Route::get('carimap/{tgltransaksi}', [BpnController::class, 'carimap']);
        Route::get('databpn', [BpnController::class, 'databpn']);
        Route::post('datagrid', [BpnController::class, 'datagrid']);
        Route::get('detail/{id}', [BpnController::class, 'detail']);
        Route::get('export_xls', [BpnController::class, 'export_xls'])->name('cetakdatabpn.export_xls');
        Route::get('export_pdf', [BpnController::class, 'export_pdf'])->name('cetakdatabpn.export_pdf');
        Route::get('exportbpn_xls', [BpnController::class, 'exportbpn_xls'])->name('cetakdatabpnatr.export_xls');
        Route::get('exportbpn_pdf', [BpnController::class, 'exportbpn_pdf'])->name('cetakdatabpnatr.export_pdf');
    });

    Route::prefix('sismiop')->middleware('permission:MenuSismiop')->group(function () {
        Route::get('', [SismiopController::class, 'index']);
        Route::post('datagridPengajuan', [SismiopController::class, 'datagridPengajuan']);
        Route::post('datagridMutasi', [SismiopController::class, 'datagridMutasi']);
        Route::get('{mutasi}/form-mutasi', [SismiopController::class, 'formMutasi']);
        Route::patch('{mutasi}/form-mutasi', [SismiopController::class, 'update']);
        Route::get('{mutasi}/form-mutasi-penuh', [SismiopController::class, 'formMutasiPenuh']);
        Route::patch('{mutasi}/form-mutasi-penuh', [SismiopController::class, 'updateMutasiPenuh']);
        Route::get('{mutasi}/form-mutasi-sebagian', [SismiopController::class, 'formMutasiSebagian']);
        Route::patch('{mutasi}/form-mutasi-sebagian', [SismiopController::class, 'updateMutasiSebagian']);
        Route::get('delete/{mutasi}', [SismiopController::class, 'destroy']);
        Route::get('caridata/{id}', [SismiopController::class, 'caridata']);
        Route::get('export_xls', [SismiopController::class, 'export_xls'])->name('sismiop.export_xls');
        Route::get('export_pdf', [SismiopController::class, 'export_pdf'])->name('sismiop.export_pdf');
    });

    Route::prefix('pelayanan-keringanan')->middleware('permission:PelayananKeringanan')->group(function () {
        Route::get('', [PelayananKeringananController::class, 'index']);
        Route::post('datagrid', [PelayananKeringananController::class, 'datagrid']);
        Route::post('datagrid_sspdbphtb', [PelayananKeringananController::class, 'datagrid_sspdbphtb']);
        Route::get('detail/{keringanan}', [PelayananKeringananController::class, 'detail']);
        Route::get('create', [PelayananKeringananController::class, 'create']);
        Route::post('store', [PelayananKeringananController::class, 'store']);
        Route::get('{keringanan}/edit', [PelayananKeringananController::class, 'edit']);
        Route::patch('{keringanan}/edit', [PelayananKeringananController::class, 'update']);
        Route::get('delete/{keringanan}', [PelayananKeringananController::class, 'destroy']);
        Route::get('caridata/{id}', [PelayananKeringananController::class, 'caridata']);
        Route::get('export_xls', [PelayananKeringananController::class, 'export_xls'])->name('pelayanan_keringanan.export_xls');
        Route::get('export_pdf', [PelayananKeringananController::class, 'export_pdf'])->name('pelayanan_keringanan.export_pdf');
    });

    Route::prefix('pelayanan-pembatalan')->middleware('permission:PelayananPembatalan')->group(function () {
        Route::get('', [PelayananPembatalanController::class, 'index']);
        Route::post('datagrid', [PelayananPembatalanController::class, 'datagrid']);
        Route::post('datagrid_sspdbphtb', [PelayananPembatalanController::class, 'datagrid_sspdbphtb']);
        Route::get('detail/{pembatalan}', [PelayananPembatalanController::class, 'detail']);
        Route::get('create', [PelayananPembatalanController::class, 'create']);
        Route::post('store', [PelayananPembatalanController::class, 'store']);
        Route::get('{pembatalan}/edit', [PelayananPembatalanController::class, 'edit']);
        Route::patch('{pembatalan}/edit', [PelayananPembatalanController::class, 'update']);
        Route::get('delete/{pembatalan}', [PelayananPembatalanController::class, 'destroy']);
        Route::get('caridata/{id}', [PelayananPembatalanController::class, 'caridata']);
        Route::get('export_xls', [PelayananPembatalanController::class, 'export_xls'])->name('pelayanan_pembatalan.export_xls');
        Route::get('export_pdf', [PelayananPembatalanController::class, 'export_pdf'])->name('pelayanan_pembatalan.export_pdf');
    });

    Route::prefix('cetak-laporan')->middleware('permission:MenuCetakLaporan')->group(function () {
        Route::get('', [CetakLaporanController::class, 'index']);
        Route::get('combo_kelurahan/{id}', [CetakLaporanController::class, 'combo_kelurahan']);
        Route::get('pendaftaran_pdf', [CetakLaporanController::class, 'cetak_pendaftaran_pdf']);
        Route::get('pendaftaran_xls', [CetakLaporanController::class, 'cetak_pendaftaran_xls']);
        Route::get('penerimaan_pdf', [CetakLaporanController::class, 'cetak_penerimaan_pdf']);
        Route::get('penerimaan_xls', [CetakLaporanController::class, 'cetak_penerimaan_xls']);
        Route::get('ajb_pdf', [CetakLaporanController::class, 'cetak_ajb_pdf']);
        Route::get('ajb_xls', [CetakLaporanController::class, 'cetak_ajb_xls']);
        Route::get('sertifikat_pdf', [CetakLaporanController::class, 'cetak_sertifikat_pdf']);
        Route::get('sertifikat_xls', [CetakLaporanController::class, 'cetak_sertifikat_xls']);
        Route::get('rekap_penerimaan_pdf', [CetakLaporanController::class, 'cetak_rekap_penerimaan_pdf']);
        Route::get('rekap_penerimaan_xls', [CetakLaporanController::class, 'cetak_rekap_penerimaan_xls']);
    });

    Route::prefix('pelayanan-penundaan')->middleware('permission:PelayananPenundaan')->group(function () {
        Route::get('', [PelayananPenundaanController::class, 'index']);
        Route::post('datagrid', [PelayananPenundaanController::class, 'datagrid']);
        Route::post('datagrid_sspdbphtb', [PelayananPenundaanController::class, 'datagrid_sspdbphtb']);
        Route::get('detail/{penundaan}', [PelayananPenundaanController::class, 'detail']);
        Route::get('create', [PelayananPenundaanController::class, 'create']);
        Route::post('store', [PelayananPenundaanController::class, 'store']);
        Route::get('{penundaan}/edit', [PelayananPenundaanController::class, 'edit']);
        Route::patch('{penundaan}/edit', [PelayananPenundaanController::class, 'update']);
        Route::get('delete/{penundaan}', [PelayananPenundaanController::class, 'destroy']);
        Route::get('caridata/{id}', [PelayananPenundaanController::class, 'caridata']);
        Route::get('export_xls', [PelayananPenundaanController::class, 'export_xls'])->name('pelayanan_penundaan.export_xls');
        Route::get('export_pdf', [PelayananPenundaanController::class, 'export_pdf'])->name('pelayanan_penundaan.export_pdf');
    });

    Route::prefix('pelayanan-angsuran')->middleware('permission:PelayananAngsuran')->group(function () {
        Route::get('', [PelayananAngsuranController::class, 'index']);
        Route::post('datagrid', [PelayananAngsuranController::class, 'datagrid']);
        Route::post('datagrid_sspdbphtb', [PelayananAngsuranController::class, 'datagrid_sspdbphtb']);
        Route::get('detail/{angsuran}', [PelayananAngsuranController::class, 'detail']);
        Route::get('create', [PelayananAngsuranController::class, 'create']);
        Route::post('store', [PelayananAngsuranController::class, 'store']);
        Route::get('{angsuran}/edit', [PelayananAngsuranController::class, 'edit']);
        Route::patch('{angsuran}/edit', [PelayananAngsuranController::class, 'update']);
        Route::get('delete/{angsuran}', [PelayananAngsuranController::class, 'destroy']);
        Route::get('caridata/{id}', [PelayananAngsuranController::class, 'caridata']);
        Route::get('export_xls', [PelayananAngsuranController::class, 'export_xls'])->name('pelayanan_angsuran.export_xls');
        Route::get('export_pdf', [PelayananAngsuranController::class, 'export_pdf'])->name('pelayanan_angsuran.export_pdf');
    });

    Route::prefix('pelayanan-mutasi')->middleware('permission:PelayananMutasi')->group(function () {
        Route::get('', [PelayananMutasiController::class, 'index']);
        Route::post('datagrid', [PelayananMutasiController::class, 'datagrid']);
        Route::post('datagrid_sspdbphtb', [PelayananMutasiController::class, 'datagrid_sspdbphtb']);
        Route::get('detail/{mutasi}', [PelayananMutasiController::class, 'detail']);
        Route::get('create', [PelayananMutasiController::class, 'create']);
        Route::post('store', [PelayananMutasiController::class, 'store']);
        Route::get('{mutasi}/edit', [PelayananMutasiController::class, 'edit']);
        Route::patch('{mutasi}/edit', [PelayananMutasiController::class, 'update']);
        Route::get('delete/{mutasi}', [PelayananMutasiController::class, 'destroy']);
        Route::get('caridata/{id}', [PelayananMutasiController::class, 'caridata']);
        Route::get('export_xls', [PelayananMutasiController::class, 'export_xls'])->name('pelayanan_mutasi.export_xls');
        Route::get('export_pdf', [PelayananMutasiController::class, 'export_pdf'])->name('pelayanan_mutasi.export_pdf');
    });

    Route::prefix('setting-pemda')->middleware('permission:SettingPemda')->group(function () {
        Route::get('', [SettingPemdaController::class, 'index']);
        Route::post('create-or-update', [SettingPemdaController::class, 'createOrUpdate'])->name('setting-pemda.createOrUpdate');
    });

    Route::prefix('setting-kecamatan')->middleware('permission:SettingKecamatan')->group(function () {
        Route::get('', [SettingKecamatanController::class, 'index']);
        Route::get('sync', [SettingKecamatanController::class, 'sync']);
        Route::post('datagrid', [SettingKecamatanController::class, 'datagrid']);
        Route::get('detail/{kecamatan}', [SettingKecamatanController::class, 'detail']);
        Route::get('create', [SettingKecamatanController::class, 'create']);
        Route::post('store', [SettingKecamatanController::class, 'store']);
        Route::get('{kecamatan}/edit', [SettingKecamatanController::class, 'edit']);
        Route::patch('{kecamatan}/edit', [SettingKecamatanController::class, 'update']);
        Route::get('delete/{kecamatan}', [SettingKecamatanController::class, 'destroy']);
        Route::get('export_xls', [SettingKecamatanController::class, 'export_xls'])->name('setting_kecamatan.export_xls');
        Route::get('export_pdf', [SettingKecamatanController::class, 'export_pdf'])->name('setting_kecamatan.export_pdf');
    });

    Route::prefix('setting-kelurahan')->middleware('permission:SettingKelurahan')->group(function () {
        Route::get('', [SettingKelurahanController::class, 'index']);
        Route::get('sync', [SettingKelurahanController::class, 'sync']);
        Route::post('datagrid', [SettingKelurahanController::class, 'datagrid']);
        Route::get('detail/{kelurahan}', [SettingKelurahanController::class, 'detail']);
        Route::get('create', [SettingKelurahanController::class, 'create']);
        Route::post('store', [SettingKelurahanController::class, 'store']);
        Route::get('{kelurahan}/edit', [SettingKelurahanController::class, 'edit']);
        Route::patch('{kelurahan}/edit', [SettingKelurahanController::class, 'update']);
        Route::get('delete/{kelurahan}', [SettingKelurahanController::class, 'destroy']);
        Route::get('export_xls', [SettingKelurahanController::class, 'export_xls'])->name('setting_kelurahan.export_xls');
        Route::get('export_pdf', [SettingKelurahanController::class, 'export_pdf'])->name('setting_kelurahan.export_pdf');
    });

    Route::prefix('setting-jenis-dokumen-tanah')->middleware('permission:SettingJenisDokumenTanah')->group(function () {
        Route::get('', [SettingJenisDokumenTanahController::class, 'index']);
        Route::post('datagrid', [SettingJenisDokumenTanahController::class, 'datagrid']);
        Route::get('detail/{jenisDokumenTanah}', [SettingJenisDokumenTanahController::class, 'detail']);
        Route::get('create', [SettingJenisDokumenTanahController::class, 'create']);
        Route::post('store', [SettingJenisDokumenTanahController::class, 'store']);
        Route::get('{JenisDokumenTanah}/edit', [SettingJenisDokumenTanahController::class, 'edit']);
        Route::patch('{JenisDokumenTanah}/edit', [SettingJenisDokumenTanahController::class, 'update']);
        Route::get('delete/{JenisDokumenTanah}', [SettingJenisDokumenTanahController::class, 'destroy']);
    });

    Route::prefix('setting-pejabat')->middleware('permission:SettingPejabat')->group(function () {
        Route::get('', [SettingPejabatController::class, 'index'])->name('setting-pejabat');
        Route::post('datagrid', [SettingPejabatController::class, 'datagrid']);
        Route::get('detail/{pejabat}', [SettingPejabatController::class, 'detail']);
        Route::get('create', [SettingPejabatController::class, 'create']);
        Route::post('store', [SettingPejabatController::class, 'store']);
        Route::get('{pejabat}/edit', [SettingPejabatController::class, 'edit']);
        Route::patch('{pejabat}/edit', [SettingPejabatController::class, 'update']);
        Route::get('delete/{pejabat}', [SettingPejabatController::class, 'destroy']);
        Route::get('export_xls', [SettingPejabatController::class, 'export_xls'])->name('setting_pejabat.export_xls');
        Route::get('export_pdf', [SettingPejabatController::class, 'export_pdf'])->name('setting_pejabat.export_pdf');
    });

    Route::prefix('setting-notaris')->middleware('permission:SettingNotaris')->group(function () {
        Route::get('', [SettingNotarisController::class, 'index']);
        Route::post('datagrid', [SettingNotarisController::class, 'datagrid']);
        Route::get('detail/{notaris}', [SettingNotarisController::class, 'detail']);
        Route::get('create', [SettingNotarisController::class, 'create']);
        Route::post('store', [SettingNotarisController::class, 'store']);
        Route::get('{notaris}/edit', [SettingNotarisController::class, 'edit']);
        Route::patch('{notaris}/edit', [SettingNotarisController::class, 'update']);
        Route::get('delete/{notaris}', [SettingNotarisController::class, 'destroy']);
        Route::get('export_xls', [SettingNotarisController::class, 'export_xls'])->name('setting_notaris.export_xls');
        Route::get('export_pdf', [SettingNotarisController::class, 'export_pdf'])->name('setting_notaris.export_pdf');
    });

    Route::prefix('setting-jenis-transaksi')->middleware('permission:SettingJenisTransaksi')->group(function () {
        Route::get('', [SettingJenisTransaksiController::class, 'index']);
        Route::post('datagrid', [SettingJenisTransaksiController::class, 'datagrid']);
        Route::get('detail/{jenis_transaksi}', [SettingJenisTransaksiController::class, 'detail']);
        Route::get('create', [SettingJenisTransaksiController::class, 'create']);
        Route::post('store', [SettingJenisTransaksiController::class, 'store']);
        Route::get('{jenis_transaksi}/edit', [SettingJenisTransaksiController::class, 'edit']);
        Route::patch('{jenis_transaksi}/edit', [SettingJenisTransaksiController::class, 'update']);
        Route::get('delete/{jenis_transaksi}', [SettingJenisTransaksiController::class, 'destroy']);
        Route::get('export_xls', [SettingJenisTransaksiController::class, 'export_xls'])->name('setting-jenis-transaksi.export_xls');
        Route::get('export_pdf', [SettingJenisTransaksiController::class, 'export_pdf'])->name('setting-jenis-transaksi.export_pdf');
    });

    Route::prefix('setting-persyaratan')->middleware('permission:SettingPersyaratan')->group(function () {
        Route::get('', [SettingPersyaratanController::class, 'index'])->name('setting-persyaratan');
        Route::post('datagrid', [SettingPersyaratanController::class, 'datagrid'])->name('setting-persyaratan.datagrid');
        Route::get('create', [SettingPersyaratanController::class, 'create'])->name('setting-persyaratan.create');
        Route::post('', [SettingPersyaratanController::class, 'store'])->name('setting-persyaratan.store');
        Route::get('{setting_persyaratan}/edit', [SettingPersyaratanController::class, 'edit'])->name('setting-persyaratan.edit');
        Route::patch('{setting_persyaratan}', [SettingPersyaratanController::class, 'update'])->name('setting-persyaratan.update');
        Route::delete('{setting_persyaratan}', [SettingPersyaratanController::class, 'destroy'])->name('setting-persyaratan.destroy');
        Route::get('detail/{persyaratan}', [SettingPersyaratanController::class, 'detail'])->name('setting-persyaratan.detail');
        Route::get('cariMaxUrut/{persyaratan}', [SettingPersyaratanController::class, 'cariMaxUrut'])->name('setting-persyaratan.cariMaxUrut');
        Route::get('export_xls', [SettingPersyaratanController::class, 'export_xls'])->name('setting-persyaratan.export_xls');
        Route::get('export_pdf', [SettingPersyaratanController::class, 'export_pdf'])->name('setting-persyaratan.export_pdf');
    });

    Route::prefix('setting-target')->middleware('permission:SettingTarget')->group(function () {
        Route::get('', [SettingTargetController::class, 'index'])->name('setting-target');
        Route::post('datagrid', [SettingTargetController::class, 'datagrid'])->name('setting-target.datagrid');
        Route::get('detail/{setting_persyaratan}', [SettingTargetController::class, 'detail'])->name('setting-target.detail');
        Route::get('create', [SettingTargetController::class, 'create'])->name('setting-target.create');
        Route::post('', [SettingTargetController::class, 'store'])->name('setting-target.store');
        Route::get('{setting_target}/edit', [SettingTargetController::class, 'edit'])->name('setting-target.edit');
        Route::put('{setting_target}', [SettingTargetController::class, 'update'])->name('setting-target.update');
        Route::delete('{setting_target}', [SettingTargetController::class, 'destroy'])->name('setting-target.destroy');
        Route::get('export_xls', [SettingTargetController::class, 'export_xls'])->name('setting-target.export_xls');
        Route::get('export_pdf', [SettingTargetController::class, 'export_pdf'])->name('setting-target.export_pdf');
    });

    Route::prefix('setting-target-status')->middleware('permission:SettingTargetStatus')->group(function () {
        Route::get('', [SettingTargetStatusController::class, 'index']);
        Route::post('datagrid', [SettingTargetStatusController::class, 'datagrid']);
        Route::get('detail/{target_status}', [SettingTargetStatusController::class, 'detail']);
        Route::post('', [SettingTargetStatusController::class, 'store']);
        Route::put('{target_status}', [SettingTargetStatusController::class, 'update']);
        Route::delete('{target_status}', [SettingTargetStatusController::class, 'destroy']);
    });

    Route::prefix('setting-hak-tanah')->middleware('permission:SettingHakTanah')->group(function () {
        Route::get('', [SettingHakTanahController::class, 'index']);
        Route::post('datagrid', [SettingHakTanahController::class, 'datagrid']);
        Route::get('detail/{hak_tanah}', [SettingHakTanahController::class, 'detail']);
        Route::get('create', [SettingHakTanahController::class, 'create'])->name('setting-hak-tanah.create');
        Route::post('store', [SettingHakTanahController::class, 'store'])->name('setting-hak-tanah.store');
        Route::get('{hak_tanah}/edit', [SettingHakTanahController::class, 'edit']);
        Route::patch('{hak_tanah}/edit', [SettingHakTanahController::class, 'update']);
        Route::get('delete/{hak_tanah}', [SettingHakTanahController::class, 'destroy']);
        Route::get('export_xls', [SettingHakTanahController::class, 'export_xls'])->name('setting-hak-tanah.export_xls');
        Route::get('export_pdf', [SettingHakTanahController::class, 'export_pdf'])->name('setting-hak-tanah.export_pdf');
    });

    Route::prefix('setting-tarif-bphtb')->middleware('permission:SettingTarifBphtb')->group(function () {
        Route::get('', [SettingTarifBphtbController::class, 'index']);
        Route::post('datagrid', [SettingTarifBphtbController::class, 'datagrid']);
        Route::get('detail/{tarif_bphtb}', [SettingTarifBphtbController::class, 'detail']);
        Route::get('create', [SettingTarifBphtbController::class, 'create'])->name('setting_tarif_bphtb.create');
        Route::post('store', [SettingTarifBphtbController::class, 'store']);
        Route::get('{tarif_bphtb}/edit', [SettingTarifBphtbController::class, 'edit']);
        Route::patch('{tarif_bphtb}/edit', [SettingTarifBphtbController::class, 'update']);
        Route::get('delete/{tarif_bphtb}', [SettingTarifBphtbController::class, 'destroy']);
        Route::get('export_xls', [SettingTarifBphtbController::class, 'export_xls'])->name('setting_tarif_bphtb.export_xls');
        Route::get('export_pdf', [SettingTarifBphtbController::class, 'export_pdf'])->name('setting_tarif_bphtb.export_pdf');
    });

    Route::prefix('setting-tempo')->middleware('permission:SettingTempo')->group(function () {
        Route::get('', [SettingTempoController::class, 'index']);
        Route::post('datagrid', [SettingTempoController::class, 'datagrid']);
        Route::get('detail/{tempo}', [SettingTempoController::class, 'detail']);
        Route::get('create', [SettingTempoController::class, 'create'])->name('setting-tempo.create');
        Route::post('store', [SettingTempoController::class, 'store'])->name('setting-tempo.store');
        Route::get('{tempo}/edit', [SettingTempoController::class, 'edit'])->name('setting-tempo.edit');
        Route::patch('{tempo}/edit', [SettingTempoController::class, 'update']);
        Route::get('delete/{tempo}', [SettingTempoController::class, 'destroy']);
        Route::get('export_xls', [SettingTempoController::class, 'export_xls'])->name('setting-tempo.export_xls');
        Route::get('export_pdf', [SettingTempoController::class, 'export_pdf'])->name('setting-tempo.export_pdf');
    });

    Route::prefix('setting-jenis-fasilitas')->middleware('permission:SettingJenisFasilitas')->group(function () {
        Route::get('', [SettingJenisFasilitasController::class, 'index']);
        Route::post('datagrid', [SettingJenisFasilitasController::class, 'datagrid']);
        Route::get('detail/{jenis_fasilitas}', [SettingJenisFasilitasController::class, 'detail']);
        Route::get('create', [SettingJenisFasilitasController::class, 'create']);
        Route::post('store', [SettingJenisFasilitasController::class, 'store']);
        Route::get('{jenis_fasilitas}/edit', [SettingJenisFasilitasController::class, 'edit']);
        Route::patch('{jenis_fasilitas}/edit', [SettingJenisFasilitasController::class, 'update']);
        Route::get('delete/{jenis_fasilitas}', [SettingJenisFasilitasController::class, 'destroy']);
        Route::get('export_xls', [SettingJenisFasilitasController::class, 'export_xls'])->name('setting_jenis_fasilitas.export_xls');
        Route::get('export_pdf', [SettingJenisFasilitasController::class, 'export_pdf'])->name('setting_jenis_fasilitas.export_pdf');
    });

    /**
     * harga acuan tanah
     */
    Route::prefix('setting-harga-acuan')->middleware('permission:SettingHargaAcuan')->group(function () {
        Route::get('', [SettingHargaAcuanController::class, 'index']);
        Route::post('datagrid', [SettingHargaAcuanController::class, 'datagrid']);
        Route::get('detail/{acuan}', [SettingHargaAcuanController::class, 'detail']);
        Route::get('create', [SettingHargaAcuanController::class, 'create']);
        Route::post('store', [SettingHargaAcuanController::class, 'store']);
        Route::get('{acuan}/edit', [SettingHargaAcuanController::class, 'edit']);
        Route::patch('{acuan}/edit', [SettingHargaAcuanController::class, 'update']);
        Route::get('delete/{acuan}', [SettingHargaAcuanController::class, 'destroy']);
        Route::get('export_xls', [SettingHargaAcuanController::class, 'export_xls'])->name('setting-harga-acuan.export_xls');
        Route::get('export_pdf', [SettingHargaAcuanController::class, 'export_pdf'])->name('setting-harga-acuan.export_pdf');
    });

    /**
     * harga acuan per jenis tanah
     */
    Route::prefix('setting-acuan-tanah')->middleware('permission:SettingAcuanTanah')->group(function () {
        Route::get('', [SettingAcuanTanahController::class, 'index'])->name('setting-acuan-tanah.index');
        Route::post('datagrid', [SettingAcuanTanahController::class, 'datagrid'])->name('setting-acuan-tanah.datagrid');
        Route::get('detail/{acuantanah}', [SettingAcuanTanahController::class, 'detail'])->name('setting-acuan-tanah.detail');
        Route::get('create', [SettingAcuanTanahController::class, 'create'])->name('setting-acuan-tanah.create');
        Route::post('', [SettingAcuanTanahController::class, 'store'])->name('setting-acuan-tanah.store');
        Route::get('{acuantanah}/edit', [SettingAcuanTanahController::class, 'edit'])->name('setting-acuan-tanah.edit');
        Route::patch('{acuantanah}', [SettingAcuanTanahController::class, 'update'])->name('setting-acuan-tanah.update');
        Route::delete('{acuantanah}', [SettingAcuanTanahController::class, 'destroy'])->name('setting-acuan-tanah.destroy');
        Route::get('export_xls', [SettingAcuanTanahController::class, 'export_xls'])->name('setting-acuan-tanah.export_xls');
        Route::get('export_pdf', [SettingAcuanTanahController::class, 'export_pdf'])->name('setting-acuan-tanah.export_pdf');
    });

    Route::prefix('setting-tarif-npoptkp')->middleware('permission:SettingTarifNpoptkp')->group(function () {
        Route::get('', [SettingTarifNpoptkpController::class, 'index']);
        Route::post('datagrid', [SettingTarifNpoptkpController::class, 'datagrid']);
        Route::get('detail/{notaris}', [SettingTarifNpoptkpController::class, 'detail']);
        Route::get('create', [SettingTarifNpoptkpController::class, 'create']);
        Route::post('store', [SettingTarifNpoptkpController::class, 'store']);
        Route::get('{tarifnpoptkp}/edit', [SettingTarifNpoptkpController::class, 'edit']);
        Route::patch('{tarifnpoptkp}/edit', [SettingTarifNpoptkpController::class, 'update']);
        Route::get('delete/{tarifnpoptkp}', [SettingTarifNpoptkpController::class, 'destroy']);
        Route::get('export_xls', [SettingTarifNpoptkpController::class, 'export_xls'])->name('setting_tarif_npoptkp.export_xls');
        Route::get('export_pdf', [SettingTarifNpoptkpController::class, 'export_pdf'])->name('setting_tarif_npoptkp.export_pdf');
    });

    // Route::prefix('setting-acuan')->middleware('permission:SettingAcuan')->group(function () {
    //     Route::get('', [SettingAcuanController::class, 'index']);
    //     Route::post('datagrid', [SettingAcuanController::class, 'datagrid']);
    //     Route::get('detail/{notaris}', [SettingAcuanController::class, 'detail']);
    //     Route::get('create', [SettingAcuanController::class, 'create']);
    //     Route::post('store', [SettingAcuanController::class, 'store']);
    //     Route::get('{Acuan}/edit', [SettingAcuanController::class, 'edit']);
    //     Route::patch('{Acuan}/edit', [SettingAcuanController::class, 'update']);
    //     Route::get('delete/{Acuan}', [SettingAcuanController::class, 'destroy']);
    //     Route::get('export_xls', [SettingAcuanController::class, 'export_xls'])->name('setting_notaris.export_xls');
    //     Route::get('export_pdf', [SettingAcuanController::class, 'export_pdf'])->name('setting_notaris.export_pdf');
    // });

    Route::prefix('setting-login-background')->middleware('permission:SettingLoginBackground')->group(function () {
        Route::get('', [SettingLoginBackground::class, 'index']);
        Route::post('datagrid', [SettingLoginBackground::class, 'datagrid']);
        Route::get('detail/{login_background}', [SettingLoginBackground::class, 'detail']);
        Route::get('create', [SettingLoginBackground::class, 'create'])->name('setting_login_background.create');
        Route::post('store', [SettingLoginBackground::class, 'store']);
        Route::get('{login_background}/edit', [SettingLoginBackground::class, 'edit']);
        Route::patch('{login_background}/edit', [SettingLoginBackground::class, 'update']);
        Route::get('delete/{login_background}', [SettingLoginBackground::class, 'destroy']);
        Route::get('export_xls', [SettingLoginBackground::class, 'export_xls'])->name('setting_login_background.export_xls');
        Route::get('export_pdf', [SettingLoginBackground::class, 'export_pdf'])->name('setting_login_background.export_pdf');
    });

    Route::prefix('setting-user')->middleware('permission:SettingUser')->group(function () {
        Route::get('index', [SettingUserController::class, 'index'])->name('setting-user.index');
        Route::get('tambah', [SettingUserController::class, 'tambah'])->name('setting-user.tambah');
        Route::post('create', [SettingUserController::class, 'store'])->name('setting-user.create');
        Route::post('datagrid', [SettingUserController::class, 'datagrid']);
        Route::get('detail/{user}', [SettingUserController::class, 'detail']);
        Route::get('listpejabatnotaris/{s_tipe_pejabat}', [SettingUserController::class, 'listpejabatnotaris']);
        Route::get('{user}/edit', [SettingUserController::class, 'edit'])->name('setting-user.edit');
        Route::put('{user}/edit', [SettingUserController::class, 'update']);
        Route::get('delete/{user}', [SettingUserController::class, 'delete']);
        Route::get('export', [SettingUserController::class, 'export'])->name('setting-user.export');
        Route::get('export_pdf', [SettingUserController::class, 'export_pdf'])->name('setting-user.export_pdf');
        Route::get('backupdb', [SettingUserController::class, 'backupdb'])->name('setting-user.backupdb');
    });

    Route::prefix('setting-briva')->middleware('permission:SettingBrivaController')->group(function () {
        Route::get('', [SettingBrivaController::class, 'index']);
        Route::post('create-or-update', [SettingBrivaController::class, 'createOrUpdate'])->name('setting-briva.createOrUpdate');
    });


    Route::prefix('setting-menukaban')->middleware('permission:SettingMenukaban')->group(function () {
        Route::get('', [SettingMenukabanController::class, 'index'])->name('setting-menukaban');
        Route::post('datagrid', [SettingMenukabanController::class, 'datagrid']);
        Route::get('detail/{menukaban}', [SettingMenukabanController::class, 'detail']);
        Route::get('create', [SettingMenukabanController::class, 'create']);
        Route::post('store', [SettingMenukabanController::class, 'store']);
        Route::get('{menukaban}/edit', [SettingMenukabanController::class, 'edit']);
        Route::patch('{menukaban}/edit', [SettingMenukabanController::class, 'update']);
        Route::get('delete/{menukaban}', [SettingMenukabanController::class, 'destroy']);
        Route::get('export_xls', [SettingMenukabanController::class, 'export_xls'])->name('setting_menukaban.export_xls');
        Route::get('export_pdf', [SettingMenukabanController::class, 'export_pdf'])->name('setting_menukaban.export_pdf');
    });


    Route::prefix('setting-menutunggakanpbb')->middleware('permission:SettingMenutunggakanpbb')->group(function () {
        Route::get('', [SettingMenutunggakanpbbController::class, 'index'])->name('setting-menutunggakanpbb');
        Route::post('datagrid', [SettingMenutunggakanpbbController::class, 'datagrid']);
        Route::get('detail/{menutunggakanpbb}', [SettingMenutunggakanpbbController::class, 'detail']);
        Route::get('create', [SettingMenutunggakanpbbController::class, 'create']);
        Route::post('store', [SettingMenutunggakanpbbController::class, 'store']);
        Route::get('{menutunggakanpbb}/edit', [SettingMenutunggakanpbbController::class, 'edit']);
        Route::patch('{menutunggakanpbb}/edit', [SettingMenutunggakanpbbController::class, 'update']);
        Route::get('delete/{menutunggakanpbb}', [SettingMenutunggakanpbbController::class, 'destroy']);
        Route::get('export_xls', [SettingMenutunggakanpbbController::class, 'export_xls'])->name('setting_menutunggakanpbb.export_xls');
        Route::get('export_pdf', [SettingMenutunggakanpbbController::class, 'export_pdf'])->name('setting_menutunggakanpbb.export_pdf');
    });


    Route::prefix('setting-menutunggakantahunpbb')->middleware('permission:SettingMenutunggakantahunpbb')->group(function () {
        Route::get('', [SettingMenutunggakantahunpbbController::class, 'index'])->name('setting-menutunggakantahunpbb');
        Route::post('datagrid', [SettingMenutunggakantahunpbbController::class, 'datagrid']);
        Route::get('detail/{menutunggakanpbb}', [SettingMenutunggakantahunpbbController::class, 'detail']);
        Route::get('create', [SettingMenutunggakantahunpbbController::class, 'create']);
        Route::post('store', [SettingMenutunggakantahunpbbController::class, 'store']);
        Route::get('{menutunggakanpbb}/edit', [SettingMenutunggakantahunpbbController::class, 'edit']);
        Route::patch('{menutunggakanpbb}/edit', [SettingMenutunggakantahunpbbController::class, 'update']);
        Route::get('delete/{menutunggakanpbb}', [SettingMenutunggakantahunpbbController::class, 'destroy']);
        //Route::get('export_xls', [SettingMenutunggakantahunpbbController::class, 'export_xls'])->name('setting_menutunggakanpbb.export_xls');
        //Route::get('export_pdf', [SettingMenutunggakantahunpbbController::class, 'export_pdf'])->name('setting_menutunggakanpbb.export_pdf');
    });


    Route::prefix('setting-menubriva')->middleware('permission:SettingMenubriva')->group(function () {
        Route::get('', [SettingMenubrivaController::class, 'index'])->name('setting-menubriva');
        Route::post('datagrid', [SettingMenubrivaController::class, 'datagrid']);
        Route::get('detail/{menubriva}', [SettingMenubrivaController::class, 'detail']);
        Route::get('create', [SettingMenubrivaController::class, 'create']);
        Route::post('store', [SettingMenubrivaController::class, 'store']);
        Route::get('{menubriva}/edit', [SettingMenubrivaController::class, 'edit']);
        Route::patch('{menubriva}/edit', [SettingMenubrivaController::class, 'update']);
        Route::get('delete/{menubriva}', [SettingMenubrivaController::class, 'destroy']);
        Route::get('export_xls', [SettingMenubrivaController::class, 'export_xls'])->name('setting_menutunggakanpbb.export_xls');
        Route::get('export_pdf', [SettingMenubrivaController::class, 'export_pdf'])->name('setting_menutunggakanpbb.export_pdf');
    });



    Route::prefix('setting-menukonekpbb')->middleware('permission:SettingMenukonekpbb')->group(function () {
        Route::get('', [SettingMenukonekpbbController::class, 'index'])->name('setting-menukonekpbb');
        Route::post('datagrid', [SettingMenukonekpbbController::class, 'datagrid']);
        Route::get('detail/{menukonekpbb}', [SettingMenukonekpbbController::class, 'detail']);
        Route::get('create', [SettingMenukonekpbbController::class, 'create']);
        Route::post('store', [SettingMenukonekpbbController::class, 'store']);
        Route::get('{menukonekpbb}/edit', [SettingMenukonekpbbController::class, 'edit']);
        Route::patch('{menukonekpbb}/edit', [SettingMenukonekpbbController::class, 'update']);
        Route::get('delete/{menukonekpbb}', [SettingMenukonekpbbController::class, 'destroy']);
        Route::get('export_xls', [SettingMenukonekpbbController::class, 'export_xls'])->name('setting_menukonekpbb.export_xls');
        Route::get('export_pdf', [SettingMenukonekpbbController::class, 'export_pdf'])->name('setting_menukonekpbb.export_pdf');
    });


    Route::prefix('setting-menuupload')->middleware('permission:SettingMenuupload')->group(function () {
        Route::get('', [SettingMenuuploadController::class, 'index'])->name('setting-menuupload');
        Route::post('datagrid', [SettingMenuuploadController::class, 'datagrid']);
        Route::get('detail/{menuupload}', [SettingMenuuploadController::class, 'detail']);
        Route::get('create', [SettingMenuuploadController::class, 'create']);
        Route::post('store', [SettingMenuuploadController::class, 'store']);
        Route::get('{menuupload}/edit', [SettingMenuuploadController::class, 'edit']);
        Route::patch('{menuupload}/edit', [SettingMenuuploadController::class, 'update']);
        Route::get('delete/{menuupload}', [SettingMenuuploadController::class, 'destroy']);
        Route::get('export_xls', [SettingMenuuploadController::class, 'export_xls'])->name('setting_menuupload.export_xls');
        Route::get('export_pdf', [SettingMenuuploadController::class, 'export_pdf'])->name('setting_menuupload.export_pdf');
    });


    Route::prefix('setting-menugmap')->middleware('permission:SettingMenugmap')->group(function () {
        Route::get('', [SettingMenugmapController::class, 'index'])->name('setting-menugmap');
        Route::post('datagrid', [SettingMenugmapController::class, 'datagrid']);
        Route::get('detail/{menugmap}', [SettingMenugmapController::class, 'detail']);
        Route::get('create', [SettingMenugmapController::class, 'create']);
        Route::post('store', [SettingMenugmapController::class, 'store']);
        Route::get('{menugmap}/edit', [SettingMenugmapController::class, 'edit']);
        Route::patch('{menugmap}/edit', [SettingMenugmapController::class, 'update']);
        Route::get('delete/{menugmap}', [SettingMenugmapController::class, 'destroy']);
        Route::get('export_xls', [SettingMenugmapController::class, 'export_xls'])->name('setting_menugmap.export_xls');
        Route::get('export_pdf', [SettingMenugmapController::class, 'export_pdf'])->name('setting_menugmap.export_pdf');
    });

    Route::prefix('setting-menucaptcha')->middleware('permission:SettingMenucaptcha')->group(function () {
        Route::get('', [SettingMenucaptchaController::class, 'index'])->name('setting-menucaptcha');
        Route::post('datagrid', [SettingMenucaptchaController::class, 'datagrid']);
        Route::get('detail/{menucaptcha}', [SettingMenucaptchaController::class, 'detail']);
        Route::get('create', [SettingMenucaptchaController::class, 'create']);
        Route::post('store', [SettingMenucaptchaController::class, 'store']);
        Route::get('{menucaptcha}/edit', [SettingMenucaptchaController::class, 'edit']);
        Route::patch('{menucaptcha}/edit', [SettingMenucaptchaController::class, 'update']);
        Route::get('delete/{menucaptcha}', [SettingMenucaptchaController::class, 'destroy']);
        Route::get('export_xls', [SettingMenucaptchaController::class, 'export_xls'])->name('setting_menucaptcha.export_xls');
        Route::get('export_pdf', [SettingMenucaptchaController::class, 'export_pdf'])->name('setting_menucaptcha.export_pdf');
    });


    Route::prefix('setting-menupelayanan')->middleware('permission:SettingMenupelayanan')->group(function () {
        Route::get('', [SettingMenupelayananController::class, 'index'])->name('setting-menupelayanan');
        Route::post('datagrid', [SettingMenupelayananController::class, 'datagrid']);
        Route::get('detail/{menupelayanan}', [SettingMenupelayananController::class, 'detail']);
        Route::get('create', [SettingMenupelayananController::class, 'create']);
        Route::post('store', [SettingMenupelayananController::class, 'store']);
        Route::get('{menupelayanan}/edit', [SettingMenupelayananController::class, 'edit']);
        Route::patch('{menupelayanan}/edit', [SettingMenupelayananController::class, 'update']);
        Route::get('delete/{menupelayanan}', [SettingMenupelayananController::class, 'destroy']);
        Route::get('export_xls', [SettingMenupelayananController::class, 'export_xls'])->name('setting_menupelayanan.export_xls');
        Route::get('export_pdf', [SettingMenupelayananController::class, 'export_pdf'])->name('setting_menupelayanan.export_pdf');
    });

    Route::prefix('ubah-pass')->middleware('permission:SettingPass')->group(function () {
        Route::get('index', [SettingPassController::class, 'index'])->name('ubah-pass.index');
        Route::post('updatepass', [SettingPassController::class, 'updatepass'])->name('ubah-pass.updatepass');
    });

    Route::prefix('history-activity')->middleware('permission:HistoryActivity')->group(function () {
        Route::get('', [HistoryActivityController::class, 'index'])->name('history-activity.index');
        Route::post('datagrid', [HistoryActivityController::class, 'datagrid']);
        Route::get('detail/{user}', [HistoryActivityController::class, 'detail']);
        Route::get('export', [HistoryActivityController::class, 'export'])->name('history-activity.export');
        Route::get('export_pdf', [HistoryActivityController::class, 'export_pdf'])->name('history-activity.export_pdf');
    });

    Route::prefix('role-and-permission')->namespace('Permissions')->group(function () {
        Route::prefix('roles')->middleware('permission:SettingRole')->group(function () {
            Route::get('', [RoleController::class, 'index'])->name('roles.index');
            Route::post('create', [RoleController::class, 'store'])->name('roles.create');
            Route::post('datagrid', [RoleController::class, 'datagrid']);
            Route::get('detail/{role}', [RoleController::class, 'detail']);
            Route::get('{role}/edit', [RoleController::class, 'edit'])->name('roles.edit');
            Route::put('{role}/edit', [RoleController::class, 'update']);
            Route::get('delete/{role}', [RoleController::class, 'delete']);
        });

        Route::prefix('permissions')->middleware('permission:SettingPermission')->group(function () {
            Route::get('', [PermissionController::class, 'index'])->name('permissions.index');
            Route::post('create', [PermissionController::class, 'store'])->name('permissions.create');
            Route::post('datagrid', [PermissionController::class, 'datagrid']);
            Route::get('detail/{role}', [PermissionController::class, 'detail']);
            Route::get('{permission}/edit', [PermissionController::class, 'edit'])->name('permissions.edit');
            Route::put('{permission}/edit', [PermissionController::class, 'update']);
            Route::get('delete/{role}', [PermissionController::class, 'delete']);
        });

        Route::prefix('assignable')->middleware('permission:SettingAssignPermissionToRole')->group(function () {
            Route::get('', [AssignController::class, 'index'])->name('assign.index');
            Route::post('datagrid', [AssignController::class, 'datagrid']);
            Route::get('detail/{role}', [AssignController::class, 'detail']);
            Route::get('create', [AssignController::class, 'create'])->name('assign.create');
            Route::post('create', [AssignController::class, 'store']);
            Route::get('{role}/edit', [AssignController::class, 'edit'])->name('assign.edit');
            Route::post('{role}/edit', [AssignController::class, 'update']);
            Route::get('delete/{role}', [AssignController::class, 'delete']);
        });

        Route::prefix('assign')->middleware('permission:SettingAssignRoleToUser')->group(function () {
            Route::get('user', [UserController::class, 'index'])->name('assign.user.index');
            Route::post('user/datagrid', [UserController::class, 'datagrid']);
            Route::get('user/detail/{role}', [UserController::class, 'detail']);
            Route::get('user/create', [UserController::class, 'create'])->name('assign.user.create');
            Route::post('user/create', [UserController::class, 'store']);
            Route::get('user/{user}/edit', [UserController::class, 'edit'])->name('assign.user.edit');
            Route::put('user/{user}/edit', [UserController::class, 'update']);
            Route::get('user/delete/{role}', [UserController::class, 'delete']);
        });
    });


    Route::prefix('setting-persentasewajar')->middleware('permission:SettingPersentasewajar')->group(function () {
        Route::get('', [SettingPersentasewajarController::class, 'index']);
        Route::post('datagrid', [SettingPersentasewajarController::class, 'datagrid']);
        Route::get('detail/{pejabat}', [SettingPersentasewajarController::class, 'detail']);
        Route::get('create', [SettingPersentasewajarController::class, 'create']);
        Route::post('store', [SettingPersentasewajarController::class, 'store']);
        Route::get('{persentasewajar}/edit', [SettingPersentasewajarController::class, 'edit']);
        Route::patch('{persentasewajar}/edit', [SettingPersentasewajarController::class, 'update']);
        Route::get('delete/{pejabat}', [SettingPersentasewajarController::class, 'destroy']);
        Route::get('export_xls', [SettingPersentasewajarController::class, 'export_xls'])->name('setting_persentasewajar.export_xls');
        Route::get('export_pdf', [SettingPersentasewajarController::class, 'export_pdf'])->name('setting_persentasewajar.export_pdf');
    });
});

// Route::get('backup-db', function () {

    // Artisan::call('backup:run', [
    //     'name' => '--only-db'
    // ]);
    // dd(date("H:i:s"));
    // Artisan::call('backup:run');
    // $exitCode = Artisan::output();
    // return $exitCode;
// })->name('backup-db');
