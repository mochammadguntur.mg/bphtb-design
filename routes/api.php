<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

use App\Http\Controllers\Api\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('getBPHTBService', [ApiController::class, 'index']);
Route::get('getBPHTBService', [ApiController::class, 'index']);

Route::post('getPBBService', [ApiController::class, 'pbb']);
Route::get('getPBBService', [ApiController::class, 'pbb']);
