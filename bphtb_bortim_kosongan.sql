--
-- PostgreSQL database dump
--

-- Dumped from database version 12.5
-- Dumped by pg_dump version 12.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: activity_log; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.activity_log (
    id bigint NOT NULL,
    log_name character varying(255),
    description text NOT NULL,
    subject_type character varying(255),
    subject_id bigint,
    causer_type character varying(255),
    causer_id bigint,
    properties json,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.activity_log OWNER TO edge;

--
-- Name: activity_log_id_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.activity_log_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.activity_log_id_seq OWNER TO edge;

--
-- Name: activity_log_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.activity_log_id_seq OWNED BY public.activity_log.id;


--
-- Name: failed_jobs; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.failed_jobs (
    id bigint NOT NULL,
    uuid character varying(255) NOT NULL,
    connection text NOT NULL,
    queue text NOT NULL,
    payload text NOT NULL,
    exception text NOT NULL,
    failed_at timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL
);


ALTER TABLE public.failed_jobs OWNER TO edge;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.failed_jobs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.failed_jobs_id_seq OWNER TO edge;

--
-- Name: failed_jobs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.failed_jobs_id_seq OWNED BY public.failed_jobs.id;


--
-- Name: his_getbphtb_bpn; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.his_getbphtb_bpn (
    t_idhis_getbpn integer NOT NULL,
    t_username character varying(255),
    t_password character varying(255),
    t_nop character varying(255),
    t_ntpd character varying(255),
    t_nik character varying(255),
    t_nama character varying(255),
    t_alamat text,
    t_kelurahan_op character varying(255),
    t_kecamatan_op character varying(255),
    t_kota_kab_op character varying(255),
    t_luastanah double precision,
    t_luasbangunan double precision,
    t_pembayaran character varying(255),
    t_status character varying(255),
    t_tanggal_pembayaran timestamp(0) without time zone,
    t_jenisbayar character varying(255),
    respon_code character varying(255),
    t_iduser integer,
    t_tglby_system timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.his_getbphtb_bpn OWNER TO edge;

--
-- Name: his_getbphtb_bpn_t_idhis_getbpn_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.his_getbphtb_bpn_t_idhis_getbpn_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.his_getbphtb_bpn_t_idhis_getbpn_seq OWNER TO edge;

--
-- Name: his_getbphtb_bpn_t_idhis_getbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.his_getbphtb_bpn_t_idhis_getbpn_seq OWNED BY public.his_getbphtb_bpn.t_idhis_getbpn;


--
-- Name: his_getpbb_bpn; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.his_getpbb_bpn (
    t_idhis_pbbbpn integer NOT NULL,
    t_username character varying(255) NOT NULL,
    t_password character varying(255) NOT NULL,
    t_nop character varying(255) NOT NULL,
    t_nik character varying(255) NOT NULL,
    t_nama_wp character varying(255) NOT NULL,
    t_alamat_op text NOT NULL,
    t_kecamatan_op character varying(255) NOT NULL,
    t_kelurahan_op character varying(255) NOT NULL,
    t_kota_kab_op character varying(255) NOT NULL,
    t_luas_tanah_op double precision,
    t_luas_bangunan_op double precision,
    t_njop_tanah_op double precision,
    t_njop_bangunan_op double precision,
    t_status_tunggakan character varying(255) NOT NULL,
    t_iduser integer,
    t_tglby_system timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.his_getpbb_bpn OWNER TO edge;

--
-- Name: his_getpbb_bpn_t_idhis_pbbbpn_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.his_getpbb_bpn_t_idhis_pbbbpn_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.his_getpbb_bpn_t_idhis_pbbbpn_seq OWNER TO edge;

--
-- Name: his_getpbb_bpn_t_idhis_pbbbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.his_getpbb_bpn_t_idhis_pbbbpn_seq OWNED BY public.his_getpbb_bpn.t_idhis_pbbbpn;


--
-- Name: his_postdatabpn; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.his_postdatabpn (
    t_idhis_posbpn integer NOT NULL,
    t_username character varying(255) NOT NULL,
    t_password character varying(255) NOT NULL,
    t_aktaid character varying(255) NOT NULL,
    t_tgl_akta timestamp(0) without time zone NOT NULL,
    t_nop character varying(255) NOT NULL,
    t_nik character varying(255) NOT NULL,
    t_nib character varying(255) NOT NULL,
    t_npwp character varying(255) NOT NULL,
    t_nama_wp character varying(255) NOT NULL,
    t_alamat_op text NOT NULL,
    t_kecamatan_op character varying(255) NOT NULL,
    t_kelurahan_op character varying(255) NOT NULL,
    t_kota_kab_op character varying(255) NOT NULL,
    t_luastanah_op double precision,
    t_luasbangunan_op double precision,
    t_no_sertipikat character varying(255) NOT NULL,
    t_no_akta character varying(255) NOT NULL,
    t_ppat character varying(255) NOT NULL,
    t_status character varying(255) NOT NULL,
    t_iduser integer,
    t_tglby_system timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.his_postdatabpn OWNER TO edge;

--
-- Name: his_postdatabpn_t_idhis_posbpn_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.his_postdatabpn_t_idhis_posbpn_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.his_postdatabpn_t_idhis_posbpn_seq OWNER TO edge;

--
-- Name: his_postdatabpn_t_idhis_posbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.his_postdatabpn_t_idhis_posbpn_seq OWNED BY public.his_postdatabpn.t_idhis_posbpn;


--
-- Name: integrasi_sertifikat_bpn; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.integrasi_sertifikat_bpn (
    t_idsertifikat integer NOT NULL,
    aktaid character varying(255) NOT NULL,
    tgl_akta timestamp(0) without time zone NOT NULL,
    nop character varying(255) NOT NULL,
    nib character varying(255) NOT NULL,
    nik character varying(255) NOT NULL,
    npwp character varying(255) NOT NULL,
    nama_wp character varying(255) NOT NULL,
    alamat_op character varying(255) NOT NULL,
    kelurahan_op character varying(255) NOT NULL,
    kecamatan_op character varying(255) NOT NULL,
    kota_op character varying(255) NOT NULL,
    luastanah_op double precision,
    luasbangunan_op double precision,
    ppat character varying(255) NOT NULL,
    no_sertipikat character varying(255) NOT NULL,
    no_akta character varying(255) NOT NULL,
    t_iduser integer,
    t_tgltransaksi timestamp(0) without time zone NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.integrasi_sertifikat_bpn OWNER TO edge;

--
-- Name: integrasi_sertifikat_bpn_t_idsertifikat_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.integrasi_sertifikat_bpn_t_idsertifikat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.integrasi_sertifikat_bpn_t_idsertifikat_seq OWNER TO edge;

--
-- Name: integrasi_sertifikat_bpn_t_idsertifikat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.integrasi_sertifikat_bpn_t_idsertifikat_seq OWNED BY public.integrasi_sertifikat_bpn.t_idsertifikat;


--
-- Name: migrations; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.migrations (
    id integer NOT NULL,
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE public.migrations OWNER TO edge;

--
-- Name: migrations_id_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.migrations_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.migrations_id_seq OWNER TO edge;

--
-- Name: migrations_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.migrations_id_seq OWNED BY public.migrations.id;


--
-- Name: model_has_permissions; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.model_has_permissions (
    permission_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_permissions OWNER TO edge;

--
-- Name: model_has_roles; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.model_has_roles (
    role_id bigint NOT NULL,
    model_type character varying(255) NOT NULL,
    model_id bigint NOT NULL
);


ALTER TABLE public.model_has_roles OWNER TO edge;

--
-- Name: notifications; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.notifications (
    id uuid NOT NULL,
    type character varying(255) NOT NULL,
    notifiable_type character varying(255) NOT NULL,
    notifiable_id bigint NOT NULL,
    data text NOT NULL,
    read_at timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.notifications OWNER TO edge;

--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE public.password_resets OWNER TO edge;

--
-- Name: permissions; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.permissions (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.permissions OWNER TO edge;

--
-- Name: permissions_id_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.permissions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.permissions_id_seq OWNER TO edge;

--
-- Name: permissions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.permissions_id_seq OWNED BY public.permissions.id;


--
-- Name: role_has_permissions; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.role_has_permissions (
    permission_id bigint NOT NULL,
    role_id bigint NOT NULL
);


ALTER TABLE public.role_has_permissions OWNER TO edge;

--
-- Name: roles; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.roles (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    guard_name character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.roles OWNER TO edge;

--
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.roles_id_seq OWNER TO edge;

--
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.roles_id_seq OWNED BY public.roles.id;


--
-- Name: s_acuan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_acuan (
    s_idacuan integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_kd_blok character varying(3),
    s_permetertanah double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_acuan OWNER TO edge;

--
-- Name: s_acuan_jenistanah; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_acuan_jenistanah (
    s_idacuan_jenis integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_kd_blok character varying(3),
    id_jenistanah integer,
    s_permetertanah double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_acuan_jenistanah OWNER TO edge;

--
-- Name: s_acuan_jenistanah_s_idacuan_jenis_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_acuan_jenistanah_s_idacuan_jenis_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_acuan_jenistanah_s_idacuan_jenis_seq OWNER TO edge;

--
-- Name: s_acuan_jenistanah_s_idacuan_jenis_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_acuan_jenistanah_s_idacuan_jenis_seq OWNED BY public.s_acuan_jenistanah.s_idacuan_jenis;


--
-- Name: s_acuan_s_idacuan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_acuan_s_idacuan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_acuan_s_idacuan_seq OWNER TO edge;

--
-- Name: s_acuan_s_idacuan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_acuan_s_idacuan_seq OWNED BY public.s_acuan.s_idacuan;


--
-- Name: s_background; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_background (
    id bigint NOT NULL,
    s_thumbnail text,
    s_id_status integer NOT NULL,
    s_iduser integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_background OWNER TO edge;

--
-- Name: s_background_id_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_background_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_background_id_seq OWNER TO edge;

--
-- Name: s_background_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_background_id_seq OWNED BY public.s_background.id;


--
-- Name: s_hak_akses; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_hak_akses (
    s_id_hakakses integer NOT NULL,
    s_nama_hakakses character varying(255)
);


ALTER TABLE public.s_hak_akses OWNER TO edge;

--
-- Name: s_hak_akses_s_id_hakakses_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_hak_akses_s_id_hakakses_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_hak_akses_s_id_hakakses_seq OWNER TO edge;

--
-- Name: s_hak_akses_s_id_hakakses_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_hak_akses_s_id_hakakses_seq OWNED BY public.s_hak_akses.s_id_hakakses;


--
-- Name: s_harga_history; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_harga_history (
    s_idhistory integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_kd_blok character varying(3),
    s_kd_urut character varying(4),
    s_kd_jenis integer,
    s_tahun_sppt integer,
    s_permetertanah double precision,
    id_jenistanah integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_harga_history OWNER TO edge;

--
-- Name: s_harga_history_s_idhistory_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_harga_history_s_idhistory_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_harga_history_s_idhistory_seq OWNER TO edge;

--
-- Name: s_harga_history_s_idhistory_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_harga_history_s_idhistory_seq OWNED BY public.s_harga_history.s_idhistory;


--
-- Name: s_jenis_bidangusaha; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_jenis_bidangusaha (
    s_idbidang_usaha integer NOT NULL,
    s_nama_bidangusaha character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenis_bidangusaha OWNER TO edge;

--
-- Name: s_jenis_bidangusaha_s_idbidang_usaha_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_jenis_bidangusaha_s_idbidang_usaha_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenis_bidangusaha_s_idbidang_usaha_seq OWNER TO edge;

--
-- Name: s_jenis_bidangusaha_s_idbidang_usaha_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_jenis_bidangusaha_s_idbidang_usaha_seq OWNED BY public.s_jenis_bidangusaha.s_idbidang_usaha;


--
-- Name: s_jenisdoktanah; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_jenisdoktanah (
    s_iddoktanah integer NOT NULL,
    s_namadoktanah text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenisdoktanah OWNER TO edge;

--
-- Name: s_jenisdoktanah_s_iddoktanah_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_jenisdoktanah_s_iddoktanah_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenisdoktanah_s_iddoktanah_seq OWNER TO edge;

--
-- Name: s_jenisdoktanah_s_iddoktanah_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_jenisdoktanah_s_iddoktanah_seq OWNED BY public.s_jenisdoktanah.s_iddoktanah;


--
-- Name: s_jenisfasilitas; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_jenisfasilitas (
    s_idjenisfasilitas integer NOT NULL,
    s_kodejenisfasilitas character varying(5),
    s_namajenisfasilitas character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenisfasilitas OWNER TO edge;

--
-- Name: s_jenisfasilitas_s_idjenisfasilitas_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_jenisfasilitas_s_idjenisfasilitas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenisfasilitas_s_idjenisfasilitas_seq OWNER TO edge;

--
-- Name: s_jenisfasilitas_s_idjenisfasilitas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_jenisfasilitas_s_idjenisfasilitas_seq OWNED BY public.s_jenisfasilitas.s_idjenisfasilitas;


--
-- Name: s_jenishaktanah; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_jenishaktanah (
    s_idhaktanah integer NOT NULL,
    s_kodehaktanah character varying(10),
    s_namahaktanah text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenishaktanah OWNER TO edge;

--
-- Name: s_jenishaktanah_s_idhaktanah_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_jenishaktanah_s_idhaktanah_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenishaktanah_s_idhaktanah_seq OWNER TO edge;

--
-- Name: s_jenishaktanah_s_idhaktanah_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_jenishaktanah_s_idhaktanah_seq OWNED BY public.s_jenishaktanah.s_idhaktanah;


--
-- Name: s_jenisketetapan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_jenisketetapan (
    s_idjenisketetapan integer NOT NULL,
    s_namajenisketetapan character varying(255) NOT NULL,
    s_namasingkatjenisketetapan character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenisketetapan OWNER TO edge;

--
-- Name: s_jenisketetapan_s_idjenisketetapan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_jenisketetapan_s_idjenisketetapan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenisketetapan_s_idjenisketetapan_seq OWNER TO edge;

--
-- Name: s_jenisketetapan_s_idjenisketetapan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_jenisketetapan_s_idjenisketetapan_seq OWNED BY public.s_jenisketetapan.s_idjenisketetapan;


--
-- Name: s_jenispengurangan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_jenispengurangan (
    s_idpengurangan integer NOT NULL,
    s_persentase integer,
    s_namapengurangan text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenispengurangan OWNER TO edge;

--
-- Name: s_jenispengurangan_s_idpengurangan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_jenispengurangan_s_idpengurangan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenispengurangan_s_idpengurangan_seq OWNER TO edge;

--
-- Name: s_jenispengurangan_s_idpengurangan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_jenispengurangan_s_idpengurangan_seq OWNED BY public.s_jenispengurangan.s_idpengurangan;


--
-- Name: s_jenistanah; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_jenistanah (
    id_jenistanah integer NOT NULL,
    nama_jenistanah character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenistanah OWNER TO edge;

--
-- Name: s_jenistanah_id_jenistanah_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_jenistanah_id_jenistanah_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenistanah_id_jenistanah_seq OWNER TO edge;

--
-- Name: s_jenistanah_id_jenistanah_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_jenistanah_id_jenistanah_seq OWNED BY public.s_jenistanah.id_jenistanah;


--
-- Name: s_jenistransaksi; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_jenistransaksi (
    s_idjenistransaksi integer NOT NULL,
    s_kodejenistransaksi character varying(5),
    s_namajenistransaksi character varying(255),
    s_idstatus_pht integer,
    s_id_dptnpoptkp integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_jenistransaksi OWNER TO edge;

--
-- Name: s_jenistransaksi_s_idjenistransaksi_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_jenistransaksi_s_idjenistransaksi_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_jenistransaksi_s_idjenistransaksi_seq OWNER TO edge;

--
-- Name: s_jenistransaksi_s_idjenistransaksi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_jenistransaksi_s_idjenistransaksi_seq OWNED BY public.s_jenistransaksi.s_idjenistransaksi;


--
-- Name: s_kecamatan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_kecamatan (
    s_idkecamatan integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_namakecamatan text NOT NULL,
    s_latitude character varying(255),
    s_longitude character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_kecamatan OWNER TO edge;

--
-- Name: s_kecamatan_s_idkecamatan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_kecamatan_s_idkecamatan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_kecamatan_s_idkecamatan_seq OWNER TO edge;

--
-- Name: s_kecamatan_s_idkecamatan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_kecamatan_s_idkecamatan_seq OWNED BY public.s_kecamatan.s_idkecamatan;


--
-- Name: s_kelurahan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_kelurahan (
    s_idkelurahan integer NOT NULL,
    s_idkecamatan integer NOT NULL,
    s_kd_propinsi character varying(2),
    s_kd_dati2 character varying(2),
    s_kd_kecamatan character varying(3),
    s_kd_kelurahan character varying(3),
    s_namakelurahan text NOT NULL,
    s_latitude character varying(255),
    s_longitude character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_kelurahan OWNER TO edge;

--
-- Name: s_kelurahan_s_idkelurahan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_kelurahan_s_idkelurahan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_kelurahan_s_idkelurahan_seq OWNER TO edge;

--
-- Name: s_kelurahan_s_idkelurahan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_kelurahan_s_idkelurahan_seq OWNED BY public.s_kelurahan.s_idkelurahan;


--
-- Name: s_koderekening; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_koderekening (
    s_korekid integer NOT NULL,
    s_korektipe character varying(2) NOT NULL,
    s_korekkelompok character varying(2) NOT NULL,
    s_korekjenis character varying(2) NOT NULL,
    s_korekobjek character varying(4) NOT NULL,
    s_korekrincian character varying(4) NOT NULL,
    s_korekrinciansub character varying(10) NOT NULL,
    s_koreknama character varying(255) NOT NULL,
    s_korekketerangan text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_koderekening OWNER TO edge;

--
-- Name: s_koderekening_s_korekid_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_koderekening_s_korekid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_koderekening_s_korekid_seq OWNER TO edge;

--
-- Name: s_koderekening_s_korekid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_koderekening_s_korekid_seq OWNED BY public.s_koderekening.s_korekid;


--
-- Name: s_notaris; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_notaris (
    s_idnotaris integer NOT NULL,
    s_namanotaris character varying(255),
    s_alamatnotaris text,
    s_kodenotaris character varying(255),
    s_sknotaris character varying(255),
    s_noid_bpn character varying(255),
    s_tgl1notaris timestamp(0) without time zone,
    s_tgl2notaris timestamp(0) without time zone,
    s_statusnotaris integer,
    s_npwpd_notaris character varying(255),
    s_daerahkerja text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_notaris OWNER TO edge;

--
-- Name: s_notaris_s_idnotaris_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_notaris_s_idnotaris_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_notaris_s_idnotaris_seq OWNER TO edge;

--
-- Name: s_notaris_s_idnotaris_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_notaris_s_idnotaris_seq OWNED BY public.s_notaris.s_idnotaris;


--
-- Name: s_pejabat; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_pejabat (
    s_idpejabat integer NOT NULL,
    s_namapejabat text NOT NULL,
    s_jabatanpejabat text,
    s_nippejabat character varying(255),
    s_golonganpejabat text,
    s_alamat text,
    s_filettd text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_pejabat OWNER TO edge;

--
-- Name: s_pejabat_s_idpejabat_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_pejabat_s_idpejabat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_pejabat_s_idpejabat_seq OWNER TO edge;

--
-- Name: s_pejabat_s_idpejabat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_pejabat_s_idpejabat_seq OWNED BY public.s_pejabat.s_idpejabat;


--
-- Name: s_pemda; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_pemda (
    s_idpemda integer NOT NULL,
    s_namaprov character varying(255),
    s_namakabkot character varying(255),
    s_namaibukotakabkot character varying(255),
    s_kodeprovinsi character varying(3),
    s_kodekabkot character varying(4),
    s_namainstansi character varying(255),
    s_namasingkatinstansi character varying(255),
    s_alamatinstansi text,
    s_notelinstansi character varying(15),
    s_namabank character varying(255),
    s_norekbank character varying(255),
    s_logo text,
    s_namacabang character varying(255),
    s_kodecabang character varying(255),
    s_kodepos character varying(5),
    s_email text,
    s_urlbphtb character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_pemda OWNER TO edge;

--
-- Name: s_pemda_s_idpemda_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_pemda_s_idpemda_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_pemda_s_idpemda_seq OWNER TO edge;

--
-- Name: s_pemda_s_idpemda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_pemda_s_idpemda_seq OWNED BY public.s_pemda.s_idpemda;


--
-- Name: s_persyaratan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_persyaratan (
    s_idpersyaratan integer NOT NULL,
    s_idjenistransaksi integer NOT NULL,
    s_namapersyaratan character varying(255),
    s_idwajib_up integer,
    s_lokasimenu integer,
    "order" integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_persyaratan OWNER TO edge;

--
-- Name: s_persyaratan_s_idpersyaratan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_persyaratan_s_idpersyaratan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_persyaratan_s_idpersyaratan_seq OWNER TO edge;

--
-- Name: s_persyaratan_s_idpersyaratan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_persyaratan_s_idpersyaratan_seq OWNED BY public.s_persyaratan.s_idpersyaratan;


--
-- Name: s_presentase_wajar; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_presentase_wajar (
    s_idpresentase_wajar integer NOT NULL,
    s_nilaimin double precision NOT NULL,
    s_nilaimax double precision NOT NULL,
    s_ketpresentase_wajar character varying(255),
    s_css_warna character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_presentase_wajar OWNER TO edge;

--
-- Name: s_presentase_wajar_s_idpresentase_wajar_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_presentase_wajar_s_idpresentase_wajar_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_presentase_wajar_s_idpresentase_wajar_seq OWNER TO edge;

--
-- Name: s_presentase_wajar_s_idpresentase_wajar_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_presentase_wajar_s_idpresentase_wajar_seq OWNED BY public.s_presentase_wajar.s_idpresentase_wajar;


--
-- Name: s_status; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_status (
    s_id_status integer NOT NULL,
    s_nama_status character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status OWNER TO edge;

--
-- Name: s_status_bayar; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_status_bayar (
    s_id_status_bayar integer NOT NULL,
    s_nama_status_bayar character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_bayar OWNER TO edge;

--
-- Name: s_status_bayar_s_id_status_bayar_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_status_bayar_s_id_status_bayar_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_bayar_s_id_status_bayar_seq OWNER TO edge;

--
-- Name: s_status_bayar_s_id_status_bayar_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_status_bayar_s_id_status_bayar_seq OWNED BY public.s_status_bayar.s_id_status_bayar;


--
-- Name: s_status_berkas; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_status_berkas (
    s_id_status_berkas integer NOT NULL,
    s_nama_status_berkas character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_berkas OWNER TO edge;

--
-- Name: s_status_berkas_s_id_status_berkas_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_status_berkas_s_id_status_berkas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_berkas_s_id_status_berkas_seq OWNER TO edge;

--
-- Name: s_status_berkas_s_id_status_berkas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_status_berkas_s_id_status_berkas_seq OWNED BY public.s_status_berkas.s_id_status_berkas;


--
-- Name: s_status_dptnpoptkp; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_status_dptnpoptkp (
    s_id_dptnpoptkp integer NOT NULL,
    s_nama_dptnpoptkp character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_dptnpoptkp OWNER TO edge;

--
-- Name: s_status_dptnpoptkp_s_id_dptnpoptkp_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_status_dptnpoptkp_s_id_dptnpoptkp_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_dptnpoptkp_s_id_dptnpoptkp_seq OWNER TO edge;

--
-- Name: s_status_dptnpoptkp_s_id_dptnpoptkp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_status_dptnpoptkp_s_id_dptnpoptkp_seq OWNED BY public.s_status_dptnpoptkp.s_id_dptnpoptkp;


--
-- Name: s_status_kabid; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_status_kabid (
    s_id_status_kabid integer NOT NULL,
    s_nama_status_kabid character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_kabid OWNER TO edge;

--
-- Name: s_status_kabid_s_id_status_kabid_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_status_kabid_s_id_status_kabid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_kabid_s_id_status_kabid_seq OWNER TO edge;

--
-- Name: s_status_kabid_s_id_status_kabid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_status_kabid_s_id_status_kabid_seq OWNED BY public.s_status_kabid.s_id_status_kabid;


--
-- Name: s_status_lihat; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_status_lihat (
    s_id_status_lihat integer NOT NULL,
    s_nama_status_lihat character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_lihat OWNER TO edge;

--
-- Name: s_status_lihat_s_id_status_lihat_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_status_lihat_s_id_status_lihat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_lihat_s_id_status_lihat_seq OWNER TO edge;

--
-- Name: s_status_lihat_s_id_status_lihat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_status_lihat_s_id_status_lihat_seq OWNED BY public.s_status_lihat.s_id_status_lihat;


--
-- Name: s_status_npwpd; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_status_npwpd (
    s_idstatus_npwpd integer NOT NULL,
    s_nama_status character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_npwpd OWNER TO edge;

--
-- Name: s_status_npwpd_s_idstatus_npwpd_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_status_npwpd_s_idstatus_npwpd_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_npwpd_s_idstatus_npwpd_seq OWNER TO edge;

--
-- Name: s_status_npwpd_s_idstatus_npwpd_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_status_npwpd_s_idstatus_npwpd_seq OWNED BY public.s_status_npwpd.s_idstatus_npwpd;


--
-- Name: s_status_pelayanan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_status_pelayanan (
    s_id_status_layanan integer NOT NULL,
    s_nama_status_layanan character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_pelayanan OWNER TO edge;

--
-- Name: s_status_pelayanan_s_id_status_layanan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_status_pelayanan_s_id_status_layanan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_pelayanan_s_id_status_layanan_seq OWNER TO edge;

--
-- Name: s_status_pelayanan_s_id_status_layanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_status_pelayanan_s_id_status_layanan_seq OWNED BY public.s_status_pelayanan.s_id_status_layanan;


--
-- Name: s_status_perhitungan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_status_perhitungan (
    s_idstatus_pht integer NOT NULL,
    s_nama character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_status_perhitungan OWNER TO edge;

--
-- Name: s_status_perhitungan_s_idstatus_pht_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_status_perhitungan_s_idstatus_pht_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_perhitungan_s_idstatus_pht_seq OWNER TO edge;

--
-- Name: s_status_perhitungan_s_idstatus_pht_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_status_perhitungan_s_idstatus_pht_seq OWNED BY public.s_status_perhitungan.s_idstatus_pht;


--
-- Name: s_status_s_id_status_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_status_s_id_status_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_status_s_id_status_seq OWNER TO edge;

--
-- Name: s_status_s_id_status_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_status_s_id_status_seq OWNED BY public.s_status.s_id_status;


--
-- Name: s_target_bphtb; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_target_bphtb (
    s_id_target_bphtb integer NOT NULL,
    s_id_target_status integer,
    s_tahun_target integer,
    s_nilai_target double precision,
    s_keterangan character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_target_bphtb OWNER TO edge;

--
-- Name: s_target_bphtb_s_id_target_bphtb_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_target_bphtb_s_id_target_bphtb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_target_bphtb_s_id_target_bphtb_seq OWNER TO edge;

--
-- Name: s_target_bphtb_s_id_target_bphtb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_target_bphtb_s_id_target_bphtb_seq OWNED BY public.s_target_bphtb.s_id_target_bphtb;


--
-- Name: s_target_status; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_target_status (
    s_id_target_status integer NOT NULL,
    s_nama_status character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_target_status OWNER TO edge;

--
-- Name: s_target_status_s_id_target_status_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_target_status_s_id_target_status_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_target_status_s_id_target_status_seq OWNER TO edge;

--
-- Name: s_target_status_s_id_target_status_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_target_status_s_id_target_status_seq OWNED BY public.s_target_status.s_id_target_status;


--
-- Name: s_tarif_bphtb; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_tarif_bphtb (
    s_idtarifbphtb integer NOT NULL,
    s_tarif_bphtb double precision,
    s_tgl_awal date,
    s_tgl_akhir date,
    s_dasar_hukum text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_tarif_bphtb OWNER TO edge;

--
-- Name: s_tarif_bphtb_s_idtarifbphtb_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_tarif_bphtb_s_idtarifbphtb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_tarif_bphtb_s_idtarifbphtb_seq OWNER TO edge;

--
-- Name: s_tarif_bphtb_s_idtarifbphtb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_tarif_bphtb_s_idtarifbphtb_seq OWNED BY public.s_tarif_bphtb.s_idtarifbphtb;


--
-- Name: s_tarifnpoptkp; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_tarifnpoptkp (
    s_idtarifnpoptkp integer NOT NULL,
    s_idjenistransaksinpoptkp integer,
    s_tarifnpoptkp double precision,
    s_tarifnpoptkptambahan double precision,
    s_dasarhukumnpoptkp text,
    s_statusnpoptkp integer,
    s_tglberlaku_awal date,
    s_tglberlaku_akhir date,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_tarifnpoptkp OWNER TO edge;

--
-- Name: s_tarifnpoptkp_s_idtarifnpoptkp_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_tarifnpoptkp_s_idtarifnpoptkp_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_tarifnpoptkp_s_idtarifnpoptkp_seq OWNER TO edge;

--
-- Name: s_tarifnpoptkp_s_idtarifnpoptkp_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_tarifnpoptkp_s_idtarifnpoptkp_seq OWNED BY public.s_tarifnpoptkp.s_idtarifnpoptkp;


--
-- Name: s_tempo; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_tempo (
    s_idtempo integer NOT NULL,
    s_haritempo integer NOT NULL,
    s_tglberlaku_awal timestamp(0) without time zone,
    s_tglberlaku_akhir timestamp(0) without time zone,
    s_id_status integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_tempo OWNER TO edge;

--
-- Name: s_tempo_s_idtempo_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_tempo_s_idtempo_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_tempo_s_idtempo_seq OWNER TO edge;

--
-- Name: s_tempo_s_idtempo_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_tempo_s_idtempo_seq OWNED BY public.s_tempo.s_idtempo;


--
-- Name: s_users_api; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_users_api (
    s_idusers_api integer NOT NULL,
    s_username character varying(255),
    s_password character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_users_api OWNER TO edge;

--
-- Name: s_users_api_s_idusers_api_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_users_api_s_idusers_api_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_users_api_s_idusers_api_seq OWNER TO edge;

--
-- Name: s_users_api_s_idusers_api_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_users_api_s_idusers_api_seq OWNED BY public.s_users_api.s_idusers_api;


--
-- Name: s_wajib_up; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.s_wajib_up (
    s_idwajib_up integer NOT NULL,
    s_ket_wjb character varying(255) NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.s_wajib_up OWNER TO edge;

--
-- Name: s_wajib_up_s_idwajib_up_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.s_wajib_up_s_idwajib_up_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.s_wajib_up_s_idwajib_up_seq OWNER TO edge;

--
-- Name: s_wajib_up_s_idwajib_up_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.s_wajib_up_s_idwajib_up_seq OWNED BY public.s_wajib_up.s_idwajib_up;


--
-- Name: t_bpn; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_bpn (
    t_idbpn integer NOT NULL,
    t_noakta character varying(100),
    t_tglakta timestamp(0) without time zone,
    t_namappat character varying(255),
    t_nop character varying(255),
    t_nib character varying(255),
    t_ntpd character varying(255),
    t_nik character varying(255),
    t_npwp character varying(50),
    t_namawp character varying(255),
    t_kelurahanop character varying(255),
    t_kecamatanop character varying(255),
    t_kotaop character varying(255),
    t_luastanahop double precision,
    t_jenishak character varying(255),
    t_koordinat_x character varying(255),
    t_koordinat_y character varying(255),
    t_tgltransaksi timestamp(0) without time zone,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_bpn OWNER TO edge;

--
-- Name: t_bpn_t_idbpn_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_bpn_t_idbpn_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_bpn_t_idbpn_seq OWNER TO edge;

--
-- Name: t_bpn_t_idbpn_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_bpn_t_idbpn_seq OWNED BY public.t_bpn.t_idbpn;


--
-- Name: t_file_keringanan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_file_keringanan (
    t_idfile_keringanan integer NOT NULL,
    t_idspt integer,
    t_iduser_upload integer,
    letak_file text NOT NULL,
    nama_file text NOT NULL,
    t_tgl_upload timestamp(0) without time zone,
    t_keterangan_file text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_file_keringanan OWNER TO edge;

--
-- Name: t_file_keringanan_t_idfile_keringanan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_file_keringanan_t_idfile_keringanan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_file_keringanan_t_idfile_keringanan_seq OWNER TO edge;

--
-- Name: t_file_keringanan_t_idfile_keringanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_file_keringanan_t_idfile_keringanan_seq OWNED BY public.t_file_keringanan.t_idfile_keringanan;


--
-- Name: t_file_objek; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_file_objek (
    t_idfile_objek integer NOT NULL,
    t_idspt integer,
    t_iduser_upload integer,
    letak_file text,
    nama_file text,
    t_tgl_upload timestamp(0) without time zone,
    t_keterangan_file text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_file_objek OWNER TO edge;

--
-- Name: t_file_objek_t_idfile_objek_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_file_objek_t_idfile_objek_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_file_objek_t_idfile_objek_seq OWNER TO edge;

--
-- Name: t_file_objek_t_idfile_objek_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_file_objek_t_idfile_objek_seq OWNED BY public.t_file_objek.t_idfile_objek;


--
-- Name: t_filesyarat_bphtb; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_filesyarat_bphtb (
    t_id_filesyarat integer NOT NULL,
    t_idspt integer,
    s_idpersyaratan integer,
    s_idjenistransaksi integer,
    t_iduser_upload integer,
    letak_file text,
    nama_file text,
    t_tgl_upload timestamp(0) without time zone,
    t_keterangan_file text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_filesyarat_bphtb OWNER TO edge;

--
-- Name: t_filesyarat_bphtb_t_id_filesyarat_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_filesyarat_bphtb_t_id_filesyarat_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_filesyarat_bphtb_t_id_filesyarat_seq OWNER TO edge;

--
-- Name: t_filesyarat_bphtb_t_id_filesyarat_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_filesyarat_bphtb_t_id_filesyarat_seq OWNED BY public.t_filesyarat_bphtb.t_id_filesyarat;


--
-- Name: t_inputajb; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_inputajb (
    t_idajb integer NOT NULL,
    t_idspt integer,
    t_tgl_ajb timestamp(0) without time zone,
    t_no_ajb character varying(255) NOT NULL,
    t_iduser_input integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_inputajb OWNER TO edge;

--
-- Name: t_inputajb_t_idajb_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_inputajb_t_idajb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_inputajb_t_idajb_seq OWNER TO edge;

--
-- Name: t_inputajb_t_idajb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_inputajb_t_idajb_seq OWNED BY public.t_inputajb.t_idajb;


--
-- Name: t_laporbulanan_detail; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_laporbulanan_detail (
    t_idlaporbulanan integer,
    t_idajb integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_laporbulanan_detail OWNER TO edge;

--
-- Name: t_laporbulanan_head; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_laporbulanan_head (
    t_idlaporbulanan integer NOT NULL,
    t_no_lapor integer,
    t_tgl_lapor timestamp(0) without time zone,
    t_untuk_bulan integer,
    t_untuk_tahun integer,
    t_keterangan text NOT NULL,
    t_iduser_input integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_laporbulanan_head OWNER TO edge;

--
-- Name: t_laporbulanan_head_t_idlaporbulanan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_laporbulanan_head_t_idlaporbulanan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_laporbulanan_head_t_idlaporbulanan_seq OWNER TO edge;

--
-- Name: t_laporbulanan_head_t_idlaporbulanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_laporbulanan_head_t_idlaporbulanan_seq OWNED BY public.t_laporbulanan_head.t_idlaporbulanan;


--
-- Name: t_nik_bersama; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_nik_bersama (
    t_id_nik_bersama integer NOT NULL,
    t_idspt integer,
    t_nik_bersama character varying(255),
    t_nama_bersama character varying(255),
    t_nama_jalan text,
    t_rt_bersama character varying(255),
    t_rw_bersama character varying(255),
    t_kecamatan_bersama character varying(255),
    t_kelurahan_bersama character varying(255),
    t_kabkota_bersama character varying(255),
    t_nohp_bersama character varying(255),
    t_kodepos_bersama character varying(255),
    t_npwp_bersama character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_nik_bersama OWNER TO edge;

--
-- Name: t_nik_bersama_t_id_nik_bersama_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_nik_bersama_t_id_nik_bersama_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_nik_bersama_t_id_nik_bersama_seq OWNER TO edge;

--
-- Name: t_nik_bersama_t_id_nik_bersama_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_nik_bersama_t_id_nik_bersama_seq OWNED BY public.t_nik_bersama.t_id_nik_bersama;


--
-- Name: t_notif_validasi_berkas; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_notif_validasi_berkas (
    t_id_notif_berkas integer NOT NULL,
    t_idspt integer,
    t_isipesan text,
    t_dari integer,
    t_tglkirim timestamp(0) without time zone,
    t_untuk integer,
    s_id_status_lihat integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_notif_validasi_berkas OWNER TO edge;

--
-- Name: t_notif_validasi_berkas_t_id_notif_berkas_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_notif_validasi_berkas_t_id_notif_berkas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_notif_validasi_berkas_t_id_notif_berkas_seq OWNER TO edge;

--
-- Name: t_notif_validasi_berkas_t_id_notif_berkas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_notif_validasi_berkas_t_id_notif_berkas_seq OWNED BY public.t_notif_validasi_berkas.t_id_notif_berkas;


--
-- Name: t_notif_validasi_kabid; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_notif_validasi_kabid (
    t_id_notif_kabid integer NOT NULL,
    t_idspt integer,
    t_isipesan text,
    t_dari integer,
    t_tglkirim timestamp(0) without time zone,
    t_untuk integer,
    s_id_status_lihat integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_notif_validasi_kabid OWNER TO edge;

--
-- Name: t_notif_validasi_kabid_t_id_notif_kabid_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_notif_validasi_kabid_t_id_notif_kabid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_notif_validasi_kabid_t_id_notif_kabid_seq OWNER TO edge;

--
-- Name: t_notif_validasi_kabid_t_id_notif_kabid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_notif_validasi_kabid_t_id_notif_kabid_seq OWNED BY public.t_notif_validasi_kabid.t_id_notif_kabid;


--
-- Name: t_pelayanan_angsuran; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pelayanan_angsuran (
    t_idangsuran integer NOT NULL,
    t_idspt integer,
    t_noangsuran integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_angsuran character varying(255),
    t_jmlhangsuran double precision,
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_angsuran OWNER TO edge;

--
-- Name: t_pelayanan_angsuran_t_idangsuran_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pelayanan_angsuran_t_idangsuran_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_angsuran_t_idangsuran_seq OWNER TO edge;

--
-- Name: t_pelayanan_angsuran_t_idangsuran_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pelayanan_angsuran_t_idangsuran_seq OWNED BY public.t_pelayanan_angsuran.t_idangsuran;


--
-- Name: t_pelayanan_keberatan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pelayanan_keberatan (
    t_idkeberatan integer NOT NULL,
    t_idspt integer,
    t_nokeberatan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_keberatan character varying(255),
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    t_jmlhpotongan_disetujui double precision,
    t_persentase_disetujui double precision,
    t_jmlh_spt_sebenarnya double precision,
    t_jmlh_spt_hasilpot double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_keberatan OWNER TO edge;

--
-- Name: t_pelayanan_keberatan_t_idkeberatan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pelayanan_keberatan_t_idkeberatan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_keberatan_t_idkeberatan_seq OWNER TO edge;

--
-- Name: t_pelayanan_keberatan_t_idkeberatan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pelayanan_keberatan_t_idkeberatan_seq OWNED BY public.t_pelayanan_keberatan.t_idkeberatan;


--
-- Name: t_pelayanan_keringanan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pelayanan_keringanan (
    t_idkeringanan integer NOT NULL,
    t_idspt integer,
    t_nokeringanan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_keringanan character varying(255),
    t_jmlhpotongan double precision,
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    t_jmlhpotongan_disetujui double precision,
    t_persentase_disetujui double precision,
    t_jmlh_spt_sebenarnya double precision,
    t_jmlh_spt_hasilpot double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_keringanan OWNER TO edge;

--
-- Name: t_pelayanan_keringanan_t_idkeringanan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pelayanan_keringanan_t_idkeringanan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_keringanan_t_idkeringanan_seq OWNER TO edge;

--
-- Name: t_pelayanan_keringanan_t_idkeringanan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pelayanan_keringanan_t_idkeringanan_seq OWNED BY public.t_pelayanan_keringanan.t_idkeringanan;


--
-- Name: t_pelayanan_mutasi; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pelayanan_mutasi (
    t_idmutasi integer NOT NULL,
    t_idspt integer,
    t_nomutasi integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_mutasi character varying(255),
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_mutasi OWNER TO edge;

--
-- Name: t_pelayanan_mutasi_t_idmutasi_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pelayanan_mutasi_t_idmutasi_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_mutasi_t_idmutasi_seq OWNER TO edge;

--
-- Name: t_pelayanan_mutasi_t_idmutasi_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pelayanan_mutasi_t_idmutasi_seq OWNED BY public.t_pelayanan_mutasi.t_idmutasi;


--
-- Name: t_pelayanan_pembatalan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pelayanan_pembatalan (
    t_idpembatalan integer NOT NULL,
    t_idspt integer,
    t_nopembatalan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_keterangan_pembatalan text,
    t_iduser_pengajuan integer,
    t_nosk_pembatalan character varying(255) NOT NULL,
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_pembatalan OWNER TO edge;

--
-- Name: t_pelayanan_pembatalan_t_idpembatalan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pelayanan_pembatalan_t_idpembatalan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_pembatalan_t_idpembatalan_seq OWNER TO edge;

--
-- Name: t_pelayanan_pembatalan_t_idpembatalan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pelayanan_pembatalan_t_idpembatalan_seq OWNED BY public.t_pelayanan_pembatalan.t_idpembatalan;


--
-- Name: t_pelayanan_penundaan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pelayanan_penundaan (
    t_idpenundaan integer NOT NULL,
    t_idspt integer,
    t_nopenundaan integer,
    t_tglpengajuan timestamp(0) without time zone,
    t_iduser_pengajuan integer,
    t_keterangan_permohoanan text,
    t_nosk_penundaan character varying(255),
    t_idstatus_disetujui integer,
    t_idoperator integer,
    t_tglpersetujuan timestamp(0) without time zone,
    t_keterangan_disetujui text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pelayanan_penundaan OWNER TO edge;

--
-- Name: t_pelayanan_penundaan_t_idpenundaan_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pelayanan_penundaan_t_idpenundaan_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pelayanan_penundaan_t_idpenundaan_seq OWNER TO edge;

--
-- Name: t_pelayanan_penundaan_t_idpenundaan_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pelayanan_penundaan_t_idpenundaan_seq OWNED BY public.t_pelayanan_penundaan.t_idpenundaan;


--
-- Name: t_pembayaran_bphtb; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pembayaran_bphtb (
    t_id_pembayaran integer NOT NULL,
    t_idspt integer,
    t_nourut_bayar integer,
    s_id_status_bayar integer,
    t_iduser_bayar integer,
    t_tglpembayaran_pokok timestamp(0) without time zone,
    t_jmlhbayar_pokok double precision,
    t_tglpembayaran_denda timestamp(0) without time zone,
    t_jmlhbayar_denda double precision,
    t_jmlhbayar_total double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pembayaran_bphtb OWNER TO edge;

--
-- Name: t_pembayaran_bphtb_t_id_pembayaran_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pembayaran_bphtb_t_id_pembayaran_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pembayaran_bphtb_t_id_pembayaran_seq OWNER TO edge;

--
-- Name: t_pembayaran_bphtb_t_id_pembayaran_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pembayaran_bphtb_t_id_pembayaran_seq OWNED BY public.t_pembayaran_bphtb.t_id_pembayaran;


--
-- Name: t_pembayaran_skpdkb; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pembayaran_skpdkb (
    t_id_bayar_skpdkb integer NOT NULL,
    t_idspt integer,
    t_idskpdkb integer,
    t_nourut_bayar integer,
    s_id_status_bayar integer,
    t_iduser_bayar integer,
    t_tglpembayaran_skpdkb timestamp(0) without time zone,
    t_jmlhbayar_skpdkb double precision,
    t_tglpembayaran_denda timestamp(0) without time zone,
    t_jmlhbayar_denda double precision,
    t_jmlhbayar_total double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pembayaran_skpdkb OWNER TO edge;

--
-- Name: t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq OWNER TO edge;

--
-- Name: t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq OWNED BY public.t_pembayaran_skpdkb.t_id_bayar_skpdkb;


--
-- Name: t_pembayaran_skpdkbt; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pembayaran_skpdkbt (
    t_id_bayar_skpdkb integer NOT NULL,
    t_idspt integer,
    t_idskpdkbt integer,
    t_nourut_bayar integer,
    s_id_status_bayar integer,
    t_iduser_bayar integer,
    t_tglpembayaran_skpdkbt timestamp(0) without time zone,
    t_jmlhbayar_skpdkbt double precision,
    t_tglpembayaran_denda timestamp(0) without time zone,
    t_jmlhbayar_denda double precision,
    t_jmlhbayar_total double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pembayaran_skpdkbt OWNER TO edge;

--
-- Name: t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq OWNER TO edge;

--
-- Name: t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq OWNED BY public.t_pembayaran_skpdkbt.t_id_bayar_skpdkb;


--
-- Name: t_pemeriksaan; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pemeriksaan (
    t_idpemeriksa integer NOT NULL,
    t_idspt integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    t_iduser_buat integer,
    t_idpejabat1 integer,
    t_idpejabat2 integer,
    t_idpejabat3 integer,
    t_idpejabat4 integer,
    t_noperiksa character varying(255),
    t_keterangan text,
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_luastanah_pemeriksa double precision,
    t_njoptanah_pemeriksa double precision,
    t_totalnjoptanah_pemeriksa double precision,
    t_luasbangunan_pemeriksa double precision,
    t_njopbangunan_pemeriksa double precision,
    t_totalnjopbangunan_pemeriksa double precision,
    t_grandtotalnjop_pemeriksa double precision,
    t_nilaitransaksispt_pemeriksa double precision,
    t_trf_aphb_kali_pemeriksa double precision,
    t_trf_aphb_bagi_pemeriksa double precision,
    t_permeter_tanah_pemeriksa double precision,
    t_idtarifbphtb_pemeriksa integer,
    t_persenbphtb_pemeriksa double precision,
    t_npop_bphtb_pemeriksa double precision,
    t_npoptkp_bphtb_pemeriksa double precision,
    t_npopkp_bphtb_pemeriksa double precision,
    t_nilaibphtb_pemeriksa double precision,
    t_idpersetujuan_pemeriksa integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pemeriksaan OWNER TO edge;

--
-- Name: t_pemeriksaan_t_idpemeriksa_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pemeriksaan_t_idpemeriksa_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pemeriksaan_t_idpemeriksa_seq OWNER TO edge;

--
-- Name: t_pemeriksaan_t_idpemeriksa_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pemeriksaan_t_idpemeriksa_seq OWNED BY public.t_pemeriksaan.t_idpemeriksa;


--
-- Name: t_pen_denda_ajb_notaris; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pen_denda_ajb_notaris (
    t_id_ajbdenda integer NOT NULL,
    t_idspt integer,
    t_idnotaris integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_nourut_ajbdenda integer,
    t_nilai_ajbdenda double precision,
    t_iduser_buat integer,
    s_id_status_bayar integer,
    t_tglbayar_ajbdenda timestamp(0) without time zone,
    t_jmlhbayar_ajbdenda double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pen_denda_ajb_notaris OWNER TO edge;

--
-- Name: t_pen_denda_ajb_notaris_t_id_ajbdenda_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pen_denda_ajb_notaris_t_id_ajbdenda_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pen_denda_ajb_notaris_t_id_ajbdenda_seq OWNER TO edge;

--
-- Name: t_pen_denda_ajb_notaris_t_id_ajbdenda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pen_denda_ajb_notaris_t_id_ajbdenda_seq OWNED BY public.t_pen_denda_ajb_notaris.t_id_ajbdenda;


--
-- Name: t_pen_denda_lpor_notaris; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_pen_denda_lpor_notaris (
    t_id_pendenda integer NOT NULL,
    t_idlaporbulanan integer,
    t_idnotaris integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_nourut_pendenda integer,
    t_nilai_pendenda double precision,
    t_iduser_buat integer,
    s_id_status_bayar integer,
    t_tglbayar_pendenda timestamp(0) without time zone,
    t_jmlhbayar_pendenda double precision,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_pen_denda_lpor_notaris OWNER TO edge;

--
-- Name: t_pen_denda_lpor_notaris_t_id_pendenda_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_pen_denda_lpor_notaris_t_id_pendenda_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_pen_denda_lpor_notaris_t_id_pendenda_seq OWNER TO edge;

--
-- Name: t_pen_denda_lpor_notaris_t_id_pendenda_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_pen_denda_lpor_notaris_t_id_pendenda_seq OWNED BY public.t_pen_denda_lpor_notaris.t_id_pendenda;


--
-- Name: t_skpdkb; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_skpdkb (
    t_idskpdkb integer NOT NULL,
    t_idspt integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    t_iduser_buat integer,
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_luastanah_skpdkb double precision,
    t_njoptanah_skpdkb double precision,
    t_totalnjoptanah_skpdkb double precision,
    t_luasbangunan_skpdkb double precision,
    t_njopbangunan_skpdkb double precision,
    t_totalnjopbangunan_skpdkb double precision,
    t_grandtotalnjop_skpdkb double precision,
    t_nilaitransaksispt_skpdkb double precision,
    t_trf_aphb_kali_skpdkb double precision,
    t_trf_aphb_bagi_skpdkb double precision,
    t_permeter_tanah_skpdkb double precision,
    t_idtarifbphtb_skpdkb integer,
    t_persenbphtb_skpdkb double precision,
    t_npop_bphtb_skpdkb double precision,
    t_npoptkp_bphtb_skpdkb double precision,
    t_npopkp_bphtb_skpdkb double precision,
    t_nilai_bayar_sblumnya double precision,
    t_nilai_pokok_skpdkb double precision,
    t_jmlh_blndenda double precision,
    t_jmlh_dendaskpdkb double precision,
    t_jmlh_totalskpdkb double precision,
    t_tglbuat_kodebyar timestamp(0) without time zone,
    t_nourut_kodebayar integer,
    t_kodebayar_skpdkb character varying(255),
    t_tgljatuhtempo_skpdkb timestamp(0) without time zone,
    t_idjenisketetapan integer,
    t_idpersetujuan_skpdkb integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_skpdkb OWNER TO edge;

--
-- Name: t_skpdkb_t_idskpdkb_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_skpdkb_t_idskpdkb_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_skpdkb_t_idskpdkb_seq OWNER TO edge;

--
-- Name: t_skpdkb_t_idskpdkb_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_skpdkb_t_idskpdkb_seq OWNED BY public.t_skpdkb.t_idskpdkb;


--
-- Name: t_skpdkbt; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_skpdkbt (
    t_idskpdkbt integer NOT NULL,
    t_idspt integer,
    t_idskpdkb integer,
    t_tglpenetapan timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    t_iduser_buat integer,
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_luastanah_skpdkbt double precision,
    t_njoptanah_skpdkbt double precision,
    t_totalnjoptanah_skpdkbt double precision,
    t_luasbangunan_skpdkbt double precision,
    t_njopbangunan_skpdkbt double precision,
    t_totalnjopbangunan_skpdkbt double precision,
    t_grandtotalnjop_skpdkbt double precision,
    t_nilaitransaksispt_skpdkbt double precision,
    t_trf_aphb_kali_skpdkbt double precision,
    t_trf_aphb_bagi_skpdkbt double precision,
    t_permeter_tanah_skpdkbt double precision,
    t_idtarifbphtb_skpdkbt integer,
    t_persenbphtb_skpdkbt double precision,
    t_npop_bphtb_skpdkbt double precision,
    t_npoptkp_bphtb_skpdkbt double precision,
    t_npopkp_bphtb_skpdkbt double precision,
    t_nilai_bayar_sblumnya double precision,
    t_nilai_pokok_skpdkbt double precision,
    t_jmlh_blndenda double precision,
    t_jmlh_dendaskpdkbt double precision,
    t_jmlh_totalskpdkbt double precision,
    t_tglbuat_kodebyar timestamp(0) without time zone,
    t_nourut_kodebayar integer,
    t_kodebayar_skpdkbt character varying(255),
    t_tgljatuhtempo_skpdkbt timestamp(0) without time zone,
    t_idjenisketetapan integer,
    t_idpersetujuan_skpdkb integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_skpdkbt OWNER TO edge;

--
-- Name: t_skpdkbt_t_idskpdkbt_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_skpdkbt_t_idskpdkbt_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_skpdkbt_t_idskpdkbt_seq OWNER TO edge;

--
-- Name: t_skpdkbt_t_idskpdkbt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_skpdkbt_t_idskpdkbt_seq OWNED BY public.t_skpdkbt.t_idskpdkbt;


--
-- Name: t_spt; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_spt (
    t_idspt integer NOT NULL,
    t_kohirspt integer,
    t_tgldaftar_spt timestamp(0) without time zone,
    t_tglby_system timestamp(0) without time zone DEFAULT CURRENT_TIMESTAMP NOT NULL,
    t_periodespt integer,
    t_iduser_buat integer,
    t_idjenistransaksi integer,
    t_idnotaris_spt integer,
    t_npwpd character varying(255),
    t_idstatus_npwpd integer,
    t_idbidang_usaha integer,
    t_nama_pembeli character varying(255),
    t_nik_pembeli character varying(255),
    t_npwp_pembeli character varying(255),
    t_jalan_pembeli text,
    t_kabkota_pembeli character varying(255),
    t_idkec_pembeli integer,
    t_namakecamatan_pembeli character varying(255),
    t_idkel_pembeli integer,
    t_namakelurahan_pembeli character varying(255),
    t_rt_pembeli character varying(255),
    t_rw_pembeli character varying(255),
    t_nohp_pembeli character varying(255),
    t_notelp_pembeli character varying(255),
    t_email_pembeli character varying(255),
    t_kodepos_pembeli character varying(255),
    t_siup_pembeli character varying(255),
    t_nib_pembeli character varying(255),
    t_ketdomisili_pembeli character varying(255),
    t_b_nama_pngjwb_pembeli character varying(255),
    t_b_nik_pngjwb_pembeli character varying(255),
    t_b_npwp_pngjwb_pembeli character varying(255),
    t_b_statusjab_pngjwb_pembeli character varying(255),
    t_b_jalan_pngjwb_pembeli text,
    t_b_kabkota_pngjwb_pembeli character varying(255),
    t_b_idkec_pngjwb_pembeli integer,
    t_b_namakec_pngjwb_pembeli character varying(255),
    t_b_idkel_pngjwb_pembeli integer,
    t_b_namakel_pngjwb_pembeli character varying(255),
    t_b_rt_pngjwb_pembeli character varying(255),
    t_b_rw_pngjwb_pembeli character varying(255),
    t_b_nohp_pngjwb_pembeli character varying(255),
    t_b_notelp_pngjwb_pembeli character varying(255),
    t_b_email_pngjwb_pembeli character varying(255),
    t_b_kodepos_pngjwb_pembeli character varying(255),
    t_nop_sppt character varying(255),
    t_tahun_sppt character varying(255),
    t_nama_sppt character varying(255),
    t_jalan_sppt character varying(255),
    t_kabkota_sppt character varying(255),
    t_kecamatan_sppt character varying(255),
    t_kelurahan_sppt character varying(255),
    t_rt_sppt character varying(255),
    t_rw_sppt character varying(255),
    t_luastanah_sismiop double precision,
    t_luasbangunan_sismiop double precision,
    t_njoptanah_sismiop double precision,
    t_njopbangunan_sismiop double precision,
    t_luastanah double precision,
    t_njoptanah double precision,
    t_totalnjoptanah double precision,
    t_luasbangunan double precision,
    t_njopbangunan double precision,
    t_totalnjopbangunan double precision,
    t_grandtotalnjop double precision,
    t_nilaitransaksispt double precision,
    t_tarif_pembagian_aphb_kali double precision,
    t_tarif_pembagian_aphb_bagi double precision,
    t_permeter_tanah double precision,
    t_idjenisfasilitas integer,
    t_idjenishaktanah integer,
    t_idjenisdoktanah integer,
    t_idpengurangan integer,
    t_id_jenistanah integer,
    t_nosertifikathaktanah character varying(255),
    t_tgldok_tanah timestamp(0) without time zone,
    s_latitude character varying(255),
    s_longitude character varying(255),
    t_idtarifbphtb integer,
    t_persenbphtb double precision,
    t_npop_bphtb double precision,
    t_npoptkp_bphtb double precision,
    t_npopkp_bphtb double precision,
    t_nilai_bphtb_fix double precision,
    t_nilai_bphtb_real double precision,
    t_idbidang_penjual integer,
    t_nama_penjual character varying(255),
    t_nik_penjual character varying(255),
    t_npwp_penjual character varying(255),
    t_jalan_penjual text,
    t_kabkota_penjual character varying(255),
    t_idkec_penjual integer,
    t_namakec_penjual character varying(255),
    t_idkel_penjual integer,
    t_namakel_penjual character varying(255),
    t_rt_penjual character varying(255),
    t_rw_penjual character varying(255),
    t_nohp_penjual character varying(255),
    t_notelp_penjual character varying(255),
    t_email_penjual character varying(255),
    t_kodepos_penjual character varying(255),
    t_siup_penjual character varying(255),
    t_nib_penjual character varying(255),
    t_ketdomisili_penjual character varying(255),
    t_b_nama_pngjwb_penjual character varying(255),
    t_b_nik_pngjwb_penjual character varying(255),
    t_b_npwp_pngjwb_penjual character varying(255),
    t_b_statusjab_pngjwb_penjual character varying(255),
    t_b_jalan_pngjwb_penjual text,
    t_b_kabkota_pngjwb_penjual character varying(255),
    t_b_idkec_pngjwb_penjual integer,
    t_b_namakec_pngjwb_penjual character varying(255),
    t_b_idkel_pngjwb_penjual integer,
    t_b_namakel_pngjwb_penjual character varying(255),
    t_b_rt_pngjwb_penjual character varying(255),
    t_b_rw_pngjwb_penjual character varying(255),
    t_b_nohp_pngjwb_penjual character varying(255),
    t_b_notelp_pngjwb_penjual character varying(255),
    t_b_email_pngjwb_penjual character varying(255),
    t_b_kodepos_pngjwb_penjual character varying(255),
    t_tglbuat_kodebyar timestamp(0) without time zone,
    t_nourut_kodebayar integer,
    t_kodebayar_bphtb character varying(255),
    t_tglketetapan_spt timestamp(0) without time zone,
    t_tgljatuhtempo_spt timestamp(0) without time zone,
    t_idjenisketetapan integer,
    t_idpersetujuan_bphtb integer,
    isdeleted integer,
    t_idoperator_deleted integer,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    t_ntpd character varying(255)
);


ALTER TABLE public.t_spt OWNER TO edge;

--
-- Name: t_spt_t_idspt_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_spt_t_idspt_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_spt_t_idspt_seq OWNER TO edge;

--
-- Name: t_spt_t_idspt_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_spt_t_idspt_seq OWNED BY public.t_spt.t_idspt;


--
-- Name: t_validasi_berkas; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_validasi_berkas (
    t_id_validasi_berkas integer NOT NULL,
    t_idspt integer,
    s_id_status_berkas integer,
    t_tglvalidasi timestamp(0) without time zone,
    t_iduser_validasi integer,
    t_keterangan_berkas text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    t_persyaratan_validasi_berkas character varying(255)
);


ALTER TABLE public.t_validasi_berkas OWNER TO edge;

--
-- Name: t_validasi_berkas_t_id_validasi_berkas_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_validasi_berkas_t_id_validasi_berkas_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_validasi_berkas_t_id_validasi_berkas_seq OWNER TO edge;

--
-- Name: t_validasi_berkas_t_id_validasi_berkas_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_validasi_berkas_t_id_validasi_berkas_seq OWNED BY public.t_validasi_berkas.t_id_validasi_berkas;


--
-- Name: t_validasi_kabid; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.t_validasi_kabid (
    t_id_validasi_kabid integer NOT NULL,
    t_idspt integer,
    s_id_status_kabid integer,
    t_tglvalidasi timestamp(0) without time zone,
    t_iduser_validasi integer,
    t_keterangan_kabid text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.t_validasi_kabid OWNER TO edge;

--
-- Name: t_validasi_kabid_t_id_validasi_kabid_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.t_validasi_kabid_t_id_validasi_kabid_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.t_validasi_kabid_t_id_validasi_kabid_seq OWNER TO edge;

--
-- Name: t_validasi_kabid_t_id_validasi_kabid_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.t_validasi_kabid_t_id_validasi_kabid_seq OWNED BY public.t_validasi_kabid.t_id_validasi_kabid;


--
-- Name: users; Type: TABLE; Schema: public; Owner: edge
--

CREATE TABLE public.users (
    id bigint NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    username character varying(255),
    email_verified_at timestamp(0) without time zone,
    password character varying(255) NOT NULL,
    s_tipe_pejabat integer,
    s_idpejabat integer,
    s_idnotaris integer,
    s_id_hakakses integer,
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE public.users OWNER TO edge;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: edge
--

CREATE SEQUENCE public.users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.users_id_seq OWNER TO edge;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: edge
--

ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;


--
-- Name: activity_log id; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.activity_log ALTER COLUMN id SET DEFAULT nextval('public.activity_log_id_seq'::regclass);


--
-- Name: failed_jobs id; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.failed_jobs ALTER COLUMN id SET DEFAULT nextval('public.failed_jobs_id_seq'::regclass);


--
-- Name: his_getbphtb_bpn t_idhis_getbpn; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.his_getbphtb_bpn ALTER COLUMN t_idhis_getbpn SET DEFAULT nextval('public.his_getbphtb_bpn_t_idhis_getbpn_seq'::regclass);


--
-- Name: his_getpbb_bpn t_idhis_pbbbpn; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.his_getpbb_bpn ALTER COLUMN t_idhis_pbbbpn SET DEFAULT nextval('public.his_getpbb_bpn_t_idhis_pbbbpn_seq'::regclass);


--
-- Name: his_postdatabpn t_idhis_posbpn; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.his_postdatabpn ALTER COLUMN t_idhis_posbpn SET DEFAULT nextval('public.his_postdatabpn_t_idhis_posbpn_seq'::regclass);


--
-- Name: integrasi_sertifikat_bpn t_idsertifikat; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.integrasi_sertifikat_bpn ALTER COLUMN t_idsertifikat SET DEFAULT nextval('public.integrasi_sertifikat_bpn_t_idsertifikat_seq'::regclass);


--
-- Name: migrations id; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.migrations ALTER COLUMN id SET DEFAULT nextval('public.migrations_id_seq'::regclass);


--
-- Name: permissions id; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.permissions ALTER COLUMN id SET DEFAULT nextval('public.permissions_id_seq'::regclass);


--
-- Name: roles id; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.roles ALTER COLUMN id SET DEFAULT nextval('public.roles_id_seq'::regclass);


--
-- Name: s_acuan s_idacuan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_acuan ALTER COLUMN s_idacuan SET DEFAULT nextval('public.s_acuan_s_idacuan_seq'::regclass);


--
-- Name: s_acuan_jenistanah s_idacuan_jenis; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_acuan_jenistanah ALTER COLUMN s_idacuan_jenis SET DEFAULT nextval('public.s_acuan_jenistanah_s_idacuan_jenis_seq'::regclass);


--
-- Name: s_background id; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_background ALTER COLUMN id SET DEFAULT nextval('public.s_background_id_seq'::regclass);


--
-- Name: s_hak_akses s_id_hakakses; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_hak_akses ALTER COLUMN s_id_hakakses SET DEFAULT nextval('public.s_hak_akses_s_id_hakakses_seq'::regclass);


--
-- Name: s_harga_history s_idhistory; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_harga_history ALTER COLUMN s_idhistory SET DEFAULT nextval('public.s_harga_history_s_idhistory_seq'::regclass);


--
-- Name: s_jenis_bidangusaha s_idbidang_usaha; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenis_bidangusaha ALTER COLUMN s_idbidang_usaha SET DEFAULT nextval('public.s_jenis_bidangusaha_s_idbidang_usaha_seq'::regclass);


--
-- Name: s_jenisdoktanah s_iddoktanah; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenisdoktanah ALTER COLUMN s_iddoktanah SET DEFAULT nextval('public.s_jenisdoktanah_s_iddoktanah_seq'::regclass);


--
-- Name: s_jenisfasilitas s_idjenisfasilitas; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenisfasilitas ALTER COLUMN s_idjenisfasilitas SET DEFAULT nextval('public.s_jenisfasilitas_s_idjenisfasilitas_seq'::regclass);


--
-- Name: s_jenishaktanah s_idhaktanah; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenishaktanah ALTER COLUMN s_idhaktanah SET DEFAULT nextval('public.s_jenishaktanah_s_idhaktanah_seq'::regclass);


--
-- Name: s_jenisketetapan s_idjenisketetapan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenisketetapan ALTER COLUMN s_idjenisketetapan SET DEFAULT nextval('public.s_jenisketetapan_s_idjenisketetapan_seq'::regclass);


--
-- Name: s_jenispengurangan s_idpengurangan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenispengurangan ALTER COLUMN s_idpengurangan SET DEFAULT nextval('public.s_jenispengurangan_s_idpengurangan_seq'::regclass);


--
-- Name: s_jenistanah id_jenistanah; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenistanah ALTER COLUMN id_jenistanah SET DEFAULT nextval('public.s_jenistanah_id_jenistanah_seq'::regclass);


--
-- Name: s_jenistransaksi s_idjenistransaksi; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenistransaksi ALTER COLUMN s_idjenistransaksi SET DEFAULT nextval('public.s_jenistransaksi_s_idjenistransaksi_seq'::regclass);


--
-- Name: s_kecamatan s_idkecamatan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_kecamatan ALTER COLUMN s_idkecamatan SET DEFAULT nextval('public.s_kecamatan_s_idkecamatan_seq'::regclass);


--
-- Name: s_kelurahan s_idkelurahan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_kelurahan ALTER COLUMN s_idkelurahan SET DEFAULT nextval('public.s_kelurahan_s_idkelurahan_seq'::regclass);


--
-- Name: s_koderekening s_korekid; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_koderekening ALTER COLUMN s_korekid SET DEFAULT nextval('public.s_koderekening_s_korekid_seq'::regclass);


--
-- Name: s_notaris s_idnotaris; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_notaris ALTER COLUMN s_idnotaris SET DEFAULT nextval('public.s_notaris_s_idnotaris_seq'::regclass);


--
-- Name: s_pejabat s_idpejabat; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_pejabat ALTER COLUMN s_idpejabat SET DEFAULT nextval('public.s_pejabat_s_idpejabat_seq'::regclass);


--
-- Name: s_pemda s_idpemda; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_pemda ALTER COLUMN s_idpemda SET DEFAULT nextval('public.s_pemda_s_idpemda_seq'::regclass);


--
-- Name: s_persyaratan s_idpersyaratan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_persyaratan ALTER COLUMN s_idpersyaratan SET DEFAULT nextval('public.s_persyaratan_s_idpersyaratan_seq'::regclass);


--
-- Name: s_presentase_wajar s_idpresentase_wajar; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_presentase_wajar ALTER COLUMN s_idpresentase_wajar SET DEFAULT nextval('public.s_presentase_wajar_s_idpresentase_wajar_seq'::regclass);


--
-- Name: s_status s_id_status; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status ALTER COLUMN s_id_status SET DEFAULT nextval('public.s_status_s_id_status_seq'::regclass);


--
-- Name: s_status_bayar s_id_status_bayar; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_bayar ALTER COLUMN s_id_status_bayar SET DEFAULT nextval('public.s_status_bayar_s_id_status_bayar_seq'::regclass);


--
-- Name: s_status_berkas s_id_status_berkas; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_berkas ALTER COLUMN s_id_status_berkas SET DEFAULT nextval('public.s_status_berkas_s_id_status_berkas_seq'::regclass);


--
-- Name: s_status_dptnpoptkp s_id_dptnpoptkp; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_dptnpoptkp ALTER COLUMN s_id_dptnpoptkp SET DEFAULT nextval('public.s_status_dptnpoptkp_s_id_dptnpoptkp_seq'::regclass);


--
-- Name: s_status_kabid s_id_status_kabid; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_kabid ALTER COLUMN s_id_status_kabid SET DEFAULT nextval('public.s_status_kabid_s_id_status_kabid_seq'::regclass);


--
-- Name: s_status_lihat s_id_status_lihat; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_lihat ALTER COLUMN s_id_status_lihat SET DEFAULT nextval('public.s_status_lihat_s_id_status_lihat_seq'::regclass);


--
-- Name: s_status_npwpd s_idstatus_npwpd; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_npwpd ALTER COLUMN s_idstatus_npwpd SET DEFAULT nextval('public.s_status_npwpd_s_idstatus_npwpd_seq'::regclass);


--
-- Name: s_status_pelayanan s_id_status_layanan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_pelayanan ALTER COLUMN s_id_status_layanan SET DEFAULT nextval('public.s_status_pelayanan_s_id_status_layanan_seq'::regclass);


--
-- Name: s_status_perhitungan s_idstatus_pht; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_perhitungan ALTER COLUMN s_idstatus_pht SET DEFAULT nextval('public.s_status_perhitungan_s_idstatus_pht_seq'::regclass);


--
-- Name: s_target_bphtb s_id_target_bphtb; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_target_bphtb ALTER COLUMN s_id_target_bphtb SET DEFAULT nextval('public.s_target_bphtb_s_id_target_bphtb_seq'::regclass);


--
-- Name: s_target_status s_id_target_status; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_target_status ALTER COLUMN s_id_target_status SET DEFAULT nextval('public.s_target_status_s_id_target_status_seq'::regclass);


--
-- Name: s_tarif_bphtb s_idtarifbphtb; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_tarif_bphtb ALTER COLUMN s_idtarifbphtb SET DEFAULT nextval('public.s_tarif_bphtb_s_idtarifbphtb_seq'::regclass);


--
-- Name: s_tarifnpoptkp s_idtarifnpoptkp; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_tarifnpoptkp ALTER COLUMN s_idtarifnpoptkp SET DEFAULT nextval('public.s_tarifnpoptkp_s_idtarifnpoptkp_seq'::regclass);


--
-- Name: s_tempo s_idtempo; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_tempo ALTER COLUMN s_idtempo SET DEFAULT nextval('public.s_tempo_s_idtempo_seq'::regclass);


--
-- Name: s_users_api s_idusers_api; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_users_api ALTER COLUMN s_idusers_api SET DEFAULT nextval('public.s_users_api_s_idusers_api_seq'::regclass);


--
-- Name: s_wajib_up s_idwajib_up; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_wajib_up ALTER COLUMN s_idwajib_up SET DEFAULT nextval('public.s_wajib_up_s_idwajib_up_seq'::regclass);


--
-- Name: t_bpn t_idbpn; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_bpn ALTER COLUMN t_idbpn SET DEFAULT nextval('public.t_bpn_t_idbpn_seq'::regclass);


--
-- Name: t_file_keringanan t_idfile_keringanan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_file_keringanan ALTER COLUMN t_idfile_keringanan SET DEFAULT nextval('public.t_file_keringanan_t_idfile_keringanan_seq'::regclass);


--
-- Name: t_file_objek t_idfile_objek; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_file_objek ALTER COLUMN t_idfile_objek SET DEFAULT nextval('public.t_file_objek_t_idfile_objek_seq'::regclass);


--
-- Name: t_filesyarat_bphtb t_id_filesyarat; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_filesyarat_bphtb ALTER COLUMN t_id_filesyarat SET DEFAULT nextval('public.t_filesyarat_bphtb_t_id_filesyarat_seq'::regclass);


--
-- Name: t_inputajb t_idajb; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_inputajb ALTER COLUMN t_idajb SET DEFAULT nextval('public.t_inputajb_t_idajb_seq'::regclass);


--
-- Name: t_laporbulanan_head t_idlaporbulanan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_laporbulanan_head ALTER COLUMN t_idlaporbulanan SET DEFAULT nextval('public.t_laporbulanan_head_t_idlaporbulanan_seq'::regclass);


--
-- Name: t_nik_bersama t_id_nik_bersama; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_nik_bersama ALTER COLUMN t_id_nik_bersama SET DEFAULT nextval('public.t_nik_bersama_t_id_nik_bersama_seq'::regclass);


--
-- Name: t_notif_validasi_berkas t_id_notif_berkas; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_berkas ALTER COLUMN t_id_notif_berkas SET DEFAULT nextval('public.t_notif_validasi_berkas_t_id_notif_berkas_seq'::regclass);


--
-- Name: t_notif_validasi_kabid t_id_notif_kabid; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_kabid ALTER COLUMN t_id_notif_kabid SET DEFAULT nextval('public.t_notif_validasi_kabid_t_id_notif_kabid_seq'::regclass);


--
-- Name: t_pelayanan_angsuran t_idangsuran; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_angsuran ALTER COLUMN t_idangsuran SET DEFAULT nextval('public.t_pelayanan_angsuran_t_idangsuran_seq'::regclass);


--
-- Name: t_pelayanan_keberatan t_idkeberatan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keberatan ALTER COLUMN t_idkeberatan SET DEFAULT nextval('public.t_pelayanan_keberatan_t_idkeberatan_seq'::regclass);


--
-- Name: t_pelayanan_keringanan t_idkeringanan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keringanan ALTER COLUMN t_idkeringanan SET DEFAULT nextval('public.t_pelayanan_keringanan_t_idkeringanan_seq'::regclass);


--
-- Name: t_pelayanan_mutasi t_idmutasi; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_mutasi ALTER COLUMN t_idmutasi SET DEFAULT nextval('public.t_pelayanan_mutasi_t_idmutasi_seq'::regclass);


--
-- Name: t_pelayanan_pembatalan t_idpembatalan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan ALTER COLUMN t_idpembatalan SET DEFAULT nextval('public.t_pelayanan_pembatalan_t_idpembatalan_seq'::regclass);


--
-- Name: t_pelayanan_penundaan t_idpenundaan; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_penundaan ALTER COLUMN t_idpenundaan SET DEFAULT nextval('public.t_pelayanan_penundaan_t_idpenundaan_seq'::regclass);


--
-- Name: t_pembayaran_bphtb t_id_pembayaran; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_bphtb ALTER COLUMN t_id_pembayaran SET DEFAULT nextval('public.t_pembayaran_bphtb_t_id_pembayaran_seq'::regclass);


--
-- Name: t_pembayaran_skpdkb t_id_bayar_skpdkb; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb ALTER COLUMN t_id_bayar_skpdkb SET DEFAULT nextval('public.t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq'::regclass);


--
-- Name: t_pembayaran_skpdkbt t_id_bayar_skpdkb; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt ALTER COLUMN t_id_bayar_skpdkb SET DEFAULT nextval('public.t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq'::regclass);


--
-- Name: t_pemeriksaan t_idpemeriksa; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pemeriksaan ALTER COLUMN t_idpemeriksa SET DEFAULT nextval('public.t_pemeriksaan_t_idpemeriksa_seq'::regclass);


--
-- Name: t_pen_denda_ajb_notaris t_id_ajbdenda; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris ALTER COLUMN t_id_ajbdenda SET DEFAULT nextval('public.t_pen_denda_ajb_notaris_t_id_ajbdenda_seq'::regclass);


--
-- Name: t_pen_denda_lpor_notaris t_id_pendenda; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris ALTER COLUMN t_id_pendenda SET DEFAULT nextval('public.t_pen_denda_lpor_notaris_t_id_pendenda_seq'::regclass);


--
-- Name: t_skpdkb t_idskpdkb; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkb ALTER COLUMN t_idskpdkb SET DEFAULT nextval('public.t_skpdkb_t_idskpdkb_seq'::regclass);


--
-- Name: t_skpdkbt t_idskpdkbt; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkbt ALTER COLUMN t_idskpdkbt SET DEFAULT nextval('public.t_skpdkbt_t_idskpdkbt_seq'::regclass);


--
-- Name: t_spt t_idspt; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt ALTER COLUMN t_idspt SET DEFAULT nextval('public.t_spt_t_idspt_seq'::regclass);


--
-- Name: t_validasi_berkas t_id_validasi_berkas; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_validasi_berkas ALTER COLUMN t_id_validasi_berkas SET DEFAULT nextval('public.t_validasi_berkas_t_id_validasi_berkas_seq'::regclass);


--
-- Name: t_validasi_kabid t_id_validasi_kabid; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_validasi_kabid ALTER COLUMN t_id_validasi_kabid SET DEFAULT nextval('public.t_validasi_kabid_t_id_validasi_kabid_seq'::regclass);


--
-- Name: users id; Type: DEFAULT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);


--
-- Data for Name: activity_log; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.activity_log (id, log_name, description, subject_type, subject_id, causer_type, causer_id, properties, created_at, updated_at) FROM stdin;
1	default	created	App\\Models\\Setting\\Kecamatan	38	App\\Models\\User	1	{"attributes":{"s_kd_propinsi":"62","s_kd_dati2":"12","s_kd_kecamatan":"000","s_namakecamatan":"Luar Daerah","s_latitude":null,"s_longitude":null}}	2021-03-03 14:04:29	2021-03-03 14:04:29
2	default	created	App\\Models\\Spt\\Spt	22	App\\Models\\User	1	{"attributes":{"t_kohirspt":1,"t_tgldaftar_spt":"2021-03-07 15:16:17","t_tglby_system":"2021-03-07 15:16:17","t_periodespt":2021,"t_idjenistransaksi":1,"t_idnotaris_spt":1,"t_npwpd":null,"t_idstatus_npwpd":null,"t_idbidang_usaha":1,"t_nama_pembeli":"Ahmad","t_nik_pembeli":"1111111111111111","t_npwp_pembeli":"11.111.111.1-111.111","t_jalan_pembeli":"Jalan","t_kabkota_pembeli":"Barito Timur","t_idkec_pembeli":28,"t_namakecamatan_pembeli":"KARUSEN JANANG","t_idkel_pembeli":195,"t_namakelurahan_pembeli":"SIMPANG NANENG","t_rt_pembeli":"1","t_rw_pembeli":"1","t_nohp_pembeli":"085785795123","t_notelp_pembeli":"085785795123","t_email_pembeli":"ahmad@gmail.com","t_kodepos_pembeli":"63383","t_siup_pembeli":null,"t_nib_pembeli":null,"t_ketdomisili_pembeli":null,"t_b_nama_pngjwb_pembeli":null,"t_b_nik_pngjwb_pembeli":null,"t_b_npwp_pngjwb_pembeli":null,"t_b_statusjab_pngjwb_pembeli":null,"t_b_jalan_pngjwb_pembeli":null,"t_b_kabkota_pngjwb_pembeli":null,"t_b_idkec_pngjwb_pembeli":null,"t_b_namakec_pngjwb_pembeli":null,"t_b_idkel_pngjwb_pembeli":null,"t_b_namakel_pngjwb_pembeli":null,"t_b_rt_pngjwb_pembeli":null,"t_b_rw_pngjwb_pembeli":null,"t_b_nohp_pngjwb_pembeli":null,"t_b_notelp_pngjwb_pembeli":null,"t_b_email_pngjwb_pembeli":null,"t_b_kodepos_pngjwb_pembeli":null,"t_nop_sppt":null,"t_tahun_sppt":null,"t_nama_sppt":null,"t_jalan_sppt":null,"t_kabkota_sppt":null,"t_kecamatan_sppt":null,"t_kelurahan_sppt":null,"t_rt_sppt":null,"t_rw_sppt":null,"t_luastanah_sismiop":null,"t_luasbangunan_sismiop":null,"t_njoptanah_sismiop":null,"t_njopbangunan_sismiop":null,"t_luastanah":null,"t_njoptanah":null,"t_totalnjoptanah":null,"t_luasbangunan":null,"t_njopbangunan":null,"t_totalnjopbangunan":null,"t_grandtotalnjop":null,"t_nilaitransaksispt":null,"t_tarif_pembagian_aphb_kali":null,"t_tarif_pembagian_aphb_bagi":null,"t_permeter_tanah":null,"t_idjenisfasilitas":null,"t_idjenishaktanah":null,"t_idjenisdoktanah":null,"t_idpengurangan":null,"t_id_jenistanah":null,"t_nosertifikathaktanah":null,"t_tgldok_tanah":null,"s_latitude":null,"s_longitude":null,"t_idtarifbphtb":null,"t_persenbphtb":null,"t_npop_bphtb":null,"t_npoptkp_bphtb":null,"t_npopkp_bphtb":null,"t_nilai_bphtb_fix":null,"t_nilai_bphtb_real":null,"t_nama_penjual":null,"t_nik_penjual":null,"t_npwp_penjual":null,"t_jalan_penjual":null,"t_kabkota_penjual":null,"t_idkec_penjual":null,"t_namakec_penjual":null,"t_idkel_penjual":null,"t_namakel_penjual":null,"t_rt_penjual":null,"t_rw_penjual":null,"t_nohp_penjual":null,"t_notelp_penjual":null,"t_email_penjual":null,"t_kodepos_penjual":null,"t_siup_penjual":null,"t_nib_penjual":null,"t_ketdomisili_penjual":null,"t_b_nama_pngjwb_penjual":null,"t_b_nik_pngjwb_penjual":null,"t_b_npwp_pngjwb_penjual":null,"t_b_statusjab_pngjwb_penjual":null,"t_b_jalan_pngjwb_penjual":null,"t_b_kabkota_pngjwb_penjual":null,"t_b_idkec_pngjwb_penjual":null,"t_b_namakec_pngjwb_penjual":null,"t_b_idkel_pngjwb_penjual":null,"t_b_namakel_pngjwb_penjual":null,"t_b_rt_pngjwb_penjual":null,"t_b_rw_pngjwb_penjual":null,"t_b_nohp_pngjwb_penjual":null,"t_b_notelp_pngjwb_penjual":null,"t_b_email_pngjwb_penjual":null,"t_b_kodepos_pngjwb_penjual":null,"t_tglbuat_kodebyar":null,"t_nourut_kodebayar":null,"t_kodebayar_bphtb":null,"t_tglketetapan_spt":null,"t_tgljatuhtempo_spt":null,"t_idjenisketetapan":1,"t_idpersetujuan_bphtb":null}}	2021-03-07 15:16:17	2021-03-07 15:16:17
3	default	created	App\\Models\\Spt\\Filesyarat	1	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_idpersyaratan":1,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"smpd_logo_hd.png","t_tgl_upload":"2021-03-07 15:21:03","t_keterangan_file":null,"created_at":"2021-03-07T08:21:03.000000Z","updated_at":"2021-03-07T08:21:03.000000Z"}}	2021-03-07 15:21:03	2021-03-07 15:21:03
4	default	created	App\\Models\\Spt\\Filesyarat	2	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_idpersyaratan":2,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"blobs.png","t_tgl_upload":"2021-03-07 15:21:20","t_keterangan_file":null,"created_at":"2021-03-07T08:21:20.000000Z","updated_at":"2021-03-07T08:21:20.000000Z"}}	2021-03-07 15:21:20	2021-03-07 15:21:20
5	default	created	App\\Models\\Spt\\Filesyarat	3	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_idpersyaratan":3,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"smpd.png","t_tgl_upload":"2021-03-07 15:21:29","t_keterangan_file":null,"created_at":"2021-03-07T08:21:29.000000Z","updated_at":"2021-03-07T08:21:29.000000Z"}}	2021-03-07 15:21:29	2021-03-07 15:21:29
6	default	created	App\\Models\\Spt\\Filesyarat	4	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_idpersyaratan":4,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"edge4.png","t_tgl_upload":"2021-03-07 15:21:53","t_keterangan_file":null,"created_at":"2021-03-07T08:21:53.000000Z","updated_at":"2021-03-07T08:21:53.000000Z"}}	2021-03-07 15:21:53	2021-03-07 15:21:53
7	default	created	App\\Models\\Spt\\Filesyarat	5	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_idpersyaratan":5,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"human.png","t_tgl_upload":"2021-03-07 15:22:10","t_keterangan_file":null,"created_at":"2021-03-07T08:22:10.000000Z","updated_at":"2021-03-07T08:22:10.000000Z"}}	2021-03-07 15:22:10	2021-03-07 15:22:10
8	default	created	App\\Models\\Spt\\Filesyarat	6	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_idpersyaratan":6,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"blobs-2.png","t_tgl_upload":"2021-03-07 15:22:38","t_keterangan_file":null,"created_at":"2021-03-07T08:22:38.000000Z","updated_at":"2021-03-07T08:22:38.000000Z"}}	2021-03-07 15:22:38	2021-03-07 15:22:38
9	default	created	App\\Models\\Spt\\Filesyarat	7	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_idpersyaratan":7,"s_idjenistransaksi":1,"t_iduser_upload":1,"letak_file":"upload\\/file_syarat\\/2021\\/jualbeli\\/22\\/","nama_file":"mybphtb.png","t_tgl_upload":"2021-03-07 15:22:50","t_keterangan_file":null,"created_at":"2021-03-07T08:22:50.000000Z","updated_at":"2021-03-07T08:22:50.000000Z"}}	2021-03-07 15:22:50	2021-03-07 15:22:50
10	default	created	App\\Models\\Spt\\Filefotoobjek	1	App\\Models\\User	1	{"attributes":{"t_idspt":22,"t_iduser_upload":1,"letak_file":"upload\\/file_fotoobjek\\/2021\\/jualbeli\\/22\\/","nama_file":"thanks.png","t_tgl_upload":"2021-03-07 15:23:34","t_keterangan_file":null,"created_at":"2021-03-07T08:23:34.000000Z","updated_at":"2021-03-07T08:23:34.000000Z"}}	2021-03-07 15:23:34	2021-03-07 15:23:34
11	default	created	App\\Models\\ValidasiBerkas\\ValidasiBerkas	12	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_id_status_berkas":1,"t_tglvalidasi":"2021-03-07 15:26:06","t_iduser_validasi":1,"t_keterangan_berkas":"ok lengkap","t_persyaratan_validasi_berkas":"1,2,3,4,5,6,7"}}	2021-03-07 15:26:06	2021-03-07 15:26:06
12	default	created	App\\Models\\ValidasiKabid\\ValidasiKabid	8	App\\Models\\User	1	{"attributes":{"t_idspt":22,"s_id_status_kabid":1,"t_tglvalidasi":"2021-03-07 15:26:44","t_iduser_validasi":1,"t_keterangan_kabid":"ok setuju"}}	2021-03-07 15:26:44	2021-03-07 15:26:44
\.


--
-- Data for Name: failed_jobs; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.failed_jobs (id, uuid, connection, queue, payload, exception, failed_at) FROM stdin;
\.


--
-- Data for Name: his_getbphtb_bpn; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.his_getbphtb_bpn (t_idhis_getbpn, t_username, t_password, t_nop, t_ntpd, t_nik, t_nama, t_alamat, t_kelurahan_op, t_kecamatan_op, t_kota_kab_op, t_luastanah, t_luasbangunan, t_pembayaran, t_status, t_tanggal_pembayaran, t_jenisbayar, respon_code, t_iduser, t_tglby_system, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: his_getpbb_bpn; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.his_getpbb_bpn (t_idhis_pbbbpn, t_username, t_password, t_nop, t_nik, t_nama_wp, t_alamat_op, t_kecamatan_op, t_kelurahan_op, t_kota_kab_op, t_luas_tanah_op, t_luas_bangunan_op, t_njop_tanah_op, t_njop_bangunan_op, t_status_tunggakan, t_iduser, t_tglby_system, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: his_postdatabpn; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.his_postdatabpn (t_idhis_posbpn, t_username, t_password, t_aktaid, t_tgl_akta, t_nop, t_nik, t_nib, t_npwp, t_nama_wp, t_alamat_op, t_kecamatan_op, t_kelurahan_op, t_kota_kab_op, t_luastanah_op, t_luasbangunan_op, t_no_sertipikat, t_no_akta, t_ppat, t_status, t_iduser, t_tglby_system, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: integrasi_sertifikat_bpn; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.integrasi_sertifikat_bpn (t_idsertifikat, aktaid, tgl_akta, nop, nib, nik, npwp, nama_wp, alamat_op, kelurahan_op, kecamatan_op, kota_op, luastanah_op, luasbangunan_op, ppat, no_sertipikat, no_akta, t_iduser, t_tgltransaksi, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.migrations (id, migration, batch) FROM stdin;
1	2020_11_24_000000_create_s_pejabat_table	1
2	2020_11_24_000001_create_s_notaris_table	1
3	2020_11_24_000002_create_users_table	1
4	2020_11_24_000003_create_failed_jobs_table	1
5	2020_11_24_000004_create_password_resets_table	1
6	2020_11_25_000001_create_s_status_table	1
7	2020_11_25_000002_create_s_acuan_table	1
8	2020_11_25_000003_create_s_harga_history_table	1
9	2020_11_25_000004_create_s_jenisdoktanah_table	1
10	2020_11_25_000005_create_s_jenishaktanah_table	1
11	2020_11_25_000006_create_s_jenisketetapan_table	1
12	2020_11_25_000007_create_s_jenispengurangan_table	1
13	2020_11_25_000008_create_s_jenistanah_table	1
14	2020_11_25_000009_create_s_jenistransaksi_table	1
15	2020_11_25_000010_create_s_kecamatan_table	1
16	2020_11_25_000011_create_s_kelurahan_table	1
17	2020_11_25_000012_create_s_koderekening_table	1
18	2020_11_25_000013_create_s_pemda_table	1
19	2020_11_25_000014_create_s_presentase_wajar_table	1
20	2020_11_25_000015_create_s_wajib_up_table	1
21	2020_11_25_000016_create_s_tarifbphtb_table	1
22	2020_11_25_000017_create_s_tarifnpoptkp_table	1
23	2020_11_25_000018_create_s_tempo_table	1
24	2020_11_25_000019_create_s_background_table	1
25	2020_11_25_000020_create_s_persyaratan	1
26	2020_11_25_000021_create_permission_tables	1
27	2020_11_25_000022_create_activity_log_table	1
28	2020_11_25_000023_create_s_jenisfasilitas_table	1
29	2020_11_25_000024_create_t_spt_table	1
30	2020_11_25_000025_create_pelayanan_keringanans_table	1
31	2020_11_25_000026_create_pelayanan_pembatalans_table	1
32	2020_11_26_000001_create_integrasi_sertifikat_bpn_table	1
33	2020_11_26_000002_create_his_get_bphtb_bpn_table	1
34	2020_11_26_000002_create_his_getpbb_bpn_table	1
35	2020_11_26_000003_create_his_postpbb_bpn_table	1
36	2020_11_26_000004_create_t_inputajb_table	1
37	2020_11_26_000005_create_s_target_table	1
38	2020_11_26_000006_create_s_acuan_jenistanah_table	1
39	2020_11_26_000007_create_t_validasi_berkas_table	1
40	2021_01_12_140000_create_notifications_table	1
41	2021_01_12_184458_add_column_to_validasi_berkas	1
42	2021_02_24_150805_alter_spt_table	1
\.


--
-- Data for Name: model_has_permissions; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.model_has_permissions (permission_id, model_type, model_id) FROM stdin;
\.


--
-- Data for Name: model_has_roles; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.model_has_roles (role_id, model_type, model_id) FROM stdin;
1	App\\Models\\User	1
2	App\\Models\\User	2
3	App\\Models\\User	3
4	App\\Models\\User	4
5	App\\Models\\User	5
6	App\\Models\\User	6
7	App\\Models\\User	7
8	App\\Models\\User	8
9	App\\Models\\User	9
\.


--
-- Data for Name: notifications; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.notifications (id, type, notifiable_type, notifiable_id, data, read_at, created_at, updated_at) FROM stdin;
5054c615-88ff-4552-a826-ef349670283c	App\\Notifications\\PendaftaranNotification	App\\Models\\User	1	{"t_idspt":22,"target_notif":"verifikasi-berkas","created_at":"2021-03-07T08:16:17.000000Z"}	\N	2021-03-07 15:25:06	2021-03-07 15:25:06
\.


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: permissions; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.permissions (id, name, guard_name, created_at, updated_at) FROM stdin;
1	MenuPendaftaran	web	2021-03-03 13:55:20	2021-03-03 13:55:20
2	SettingPemda	web	2021-03-03 13:55:20	2021-03-03 13:55:20
3	SettingKecamatan	web	2021-03-03 13:55:20	2021-03-03 13:55:20
4	SettingKelurahan	web	2021-03-03 13:55:20	2021-03-03 13:55:20
5	SettingPejabat	web	2021-03-03 13:55:20	2021-03-03 13:55:20
6	SettingNotaris	web	2021-03-03 13:55:20	2021-03-03 13:55:20
7	SettingJenisTransaksi	web	2021-03-03 13:55:20	2021-03-03 13:55:20
8	SettingPersyaratan	web	2021-03-03 13:55:20	2021-03-03 13:55:20
9	SettingHakTanah	web	2021-03-03 13:55:20	2021-03-03 13:55:20
10	SettingTarifBphtb	web	2021-03-03 13:55:20	2021-03-03 13:55:20
11	SettingTarifNpoptkp	web	2021-03-03 13:55:20	2021-03-03 13:55:20
12	SettingLoginBackground	web	2021-03-03 13:55:20	2021-03-03 13:55:20
13	SettingUser	web	2021-03-03 13:55:20	2021-03-03 13:55:20
14	SettingRole	web	2021-03-03 13:55:20	2021-03-03 13:55:20
15	SettingPermission	web	2021-03-03 13:55:20	2021-03-03 13:55:20
16	SettingAssignPermissionToRole	web	2021-03-03 13:55:20	2021-03-03 13:55:20
17	SettingAssignRoleToUser	web	2021-03-03 13:55:20	2021-03-03 13:55:20
18	SettingPersentasewajar	web	2021-03-03 13:55:20	2021-03-03 13:55:20
19	PelaporanAjb	web	2021-03-03 13:55:20	2021-03-03 13:55:20
20	Informasiop	web	2021-03-03 13:55:20	2021-03-03 13:55:20
21	PelayananKeringanan	web	2021-03-03 13:55:20	2021-03-03 13:55:20
22	PelayananKeberatan	web	2021-03-03 13:55:20	2021-03-03 13:55:20
23	PelayananAngsuran	web	2021-03-03 13:55:20	2021-03-03 13:55:20
24	PelayananPenundaan	web	2021-03-03 13:55:20	2021-03-03 13:55:20
25	PelayananPermohonanmutasi	web	2021-03-03 13:55:20	2021-03-03 13:55:20
26	PelayananPembatalan	web	2021-03-03 13:55:20	2021-03-03 13:55:20
27	Bpn	web	2021-03-03 13:55:20	2021-03-03 13:55:20
28	KppPratama	web	2021-03-03 13:55:20	2021-03-03 13:55:20
29	HistoryActivity	web	2021-03-03 13:55:20	2021-03-03 13:55:20
30	ValidasiBerkas	web	2021-03-03 13:55:20	2021-03-03 13:55:20
31	ValidasiKabid	web	2021-03-03 13:55:20	2021-03-03 13:55:20
32	Pemeriksaan	web	2021-03-03 13:55:20	2021-03-03 13:55:20
33	InformasiHargawajar	web	2021-03-03 13:55:20	2021-03-03 13:55:20
34	Pembayaran	web	2021-03-03 13:55:20	2021-03-03 13:55:20
35	Menucekniknib	web	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: role_has_permissions; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.role_has_permissions (permission_id, role_id) FROM stdin;
1	1
2	1
3	1
4	1
5	1
6	1
7	1
8	1
9	1
10	1
11	1
12	1
13	1
14	1
15	1
16	1
17	1
18	1
19	1
20	1
21	1
22	1
23	1
24	1
25	1
26	1
27	1
28	1
29	1
30	1
31	1
32	1
33	1
34	1
35	1
1	4
19	4
20	4
21	4
22	4
23	4
24	4
25	4
26	4
1	2
20	2
21	2
22	2
23	2
24	2
25	2
26	2
30	2
33	2
35	2
1	3
20	3
21	3
22	3
23	3
24	3
25	3
26	3
29	3
31	3
33	3
35	3
27	5
28	6
34	7
26	8
32	9
\.


--
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.roles (id, name, guard_name, created_at, updated_at) FROM stdin;
1	admin	web	2021-03-03 13:55:20	2021-03-03 13:55:20
2	validasiberkas	web	2021-03-03 13:55:20	2021-03-03 13:55:20
3	validasikabid	web	2021-03-03 13:55:20	2021-03-03 13:55:20
4	notaris	web	2021-03-03 13:55:20	2021-03-03 13:55:20
5	bpn	web	2021-03-03 13:55:20	2021-03-03 13:55:20
6	kpppratama	web	2021-03-03 13:55:20	2021-03-03 13:55:20
7	bendahara	web	2021-03-03 13:55:20	2021-03-03 13:55:20
8	wajibpajak	web	2021-03-03 13:55:20	2021-03-03 13:55:20
9	pemeriksa	web	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_acuan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_acuan (s_idacuan, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_kd_blok, s_permetertanah, created_at, updated_at) FROM stdin;
1	63	10	010	036	001	1000000	2021-03-03 13:55:20	2021-03-03 13:55:20
2	63	10	010	036	002	2000000	2021-03-03 13:55:20	2021-03-03 13:55:20
3	63	10	010	036	003	500000	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_acuan_jenistanah; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_acuan_jenistanah (s_idacuan_jenis, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_kd_blok, id_jenistanah, s_permetertanah, created_at, updated_at) FROM stdin;
1	63	10	010	036	003	1	500000	2021-03-03 13:55:20	2021-03-03 13:55:20
2	63	10	010	036	003	2	1500000	2021-03-03 13:55:20	2021-03-03 13:55:20
3	63	10	010	036	003	1	300000	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_background; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_background (id, s_thumbnail, s_id_status, s_iduser, created_at, updated_at) FROM stdin;
1	/public/upload/21anew.png	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
2	/public/upload/21anew.png	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_hak_akses; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_hak_akses (s_id_hakakses, s_nama_hakakses) FROM stdin;
1	admin
2	operatorberkas
3	operatorkabid
4	notaris
5	bpn
6	kpppratama
7	operatorbendahara
8	wp
9	pemeriksa
\.


--
-- Data for Name: s_harga_history; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_harga_history (s_idhistory, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_kd_blok, s_kd_urut, s_kd_jenis, s_tahun_sppt, s_permetertanah, id_jenistanah, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: s_jenis_bidangusaha; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_jenis_bidangusaha (s_idbidang_usaha, s_nama_bidangusaha, created_at, updated_at) FROM stdin;
1	Pribadi	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Badan Usaha	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_jenisdoktanah; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_jenisdoktanah (s_iddoktanah, s_namadoktanah, created_at, updated_at) FROM stdin;
1	Sertifikat	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Warkah	2021-03-03 13:55:20	2021-03-03 13:55:20
3	Girik	2021-03-03 13:55:20	2021-03-03 13:55:20
4	Letter C	2021-03-03 13:55:20	2021-03-03 13:55:20
5	SPT	2021-03-03 13:55:20	2021-03-03 13:55:20
6	NIB (Nomor Identifikasi Bidang Tanah)	2021-03-03 13:55:20	2021-03-03 13:55:20
7	Akta	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_jenisfasilitas; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_jenisfasilitas (s_idjenisfasilitas, s_kodejenisfasilitas, s_namajenisfasilitas, created_at, updated_at) FROM stdin;
1	01	Perwakilan diplomatik, konsulat berdasarkan asas perlakuan timbal balik	2021-03-03 13:55:20	2021-03-03 13:55:20
2	02	Negara untuk penyelenggaraan pemerintah dan atau untuk pelaksanaan pembangunan guna kepentingan umum	2021-03-03 13:55:20	2021-03-03 13:55:20
3	03	Badan atau perwakilan organisasi internasional yang ditetapkan oleh Menteri Keuangan	2021-03-03 13:55:20	2021-03-03 13:55:20
4	04	Orang pribadi atau badan karena konversi hak dan perbuatan hukum lain dengan tidak adanya perubahan nama	2021-03-03 13:55:20	2021-03-03 13:55:20
5	05	Karena wakaf atau warisan	2021-03-03 13:55:20	2021-03-03 13:55:20
6	06	Untuk digunakan kepentingan ibadah	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_jenishaktanah; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_jenishaktanah (s_idhaktanah, s_kodehaktanah, s_namahaktanah, created_at, updated_at) FROM stdin;
1	01	Hak Milik	2021-03-03 13:55:20	2021-03-03 13:55:20
2	02	Hak Guna Usaha	2021-03-03 13:55:20	2021-03-03 13:55:20
3	03	Hak Guna Bangunan	2021-03-03 13:55:20	2021-03-03 13:55:20
4	04	Hak Pakai	2021-03-03 13:55:20	2021-03-03 13:55:20
5	05	Hak Milik Atas Satuan Rumah	2021-03-03 13:55:20	2021-03-03 13:55:20
6	05	Hak Pengelolaan	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_jenisketetapan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_jenisketetapan (s_idjenisketetapan, s_namajenisketetapan, s_namasingkatjenisketetapan, created_at, updated_at) FROM stdin;
1	Surat Setoran Pajak Daerah	SSPD	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Surat Ketetapan Pajak Daerah Kurang Bayar	SKPDKB	2021-03-03 13:55:20	2021-03-03 13:55:20
3	Surat Ketetapan Pajak Daerah Kurang Bayar Tambahan	SKPDKBT	2021-03-03 13:55:20	2021-03-03 13:55:20
4	Surat Ketetapan Pajak Daerah Lebih Bayar	SKPDLB	2021-03-03 13:55:20	2021-03-03 13:55:20
5	Surat Ketetapan Pajak Daerah Nihil	SKPDN	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_jenispengurangan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_jenispengurangan (s_idpengurangan, s_persentase, s_namapengurangan, created_at, updated_at) FROM stdin;
1	50	Waris / Hibah Wasiat	2021-03-03 13:55:20	2021-03-03 13:55:20
2	75	Pensiunan TNI/POLRI 2	2021-03-03 13:55:20	2021-03-03 13:55:20
3	75	Perumahan Dinas TNI/POLRI	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_jenistanah; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_jenistanah (id_jenistanah, nama_jenistanah, created_at, updated_at) FROM stdin;
1	Bukan Perumahan	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Perumahan	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_jenistransaksi; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_jenistransaksi (s_idjenistransaksi, s_kodejenistransaksi, s_namajenistransaksi, s_idstatus_pht, s_id_dptnpoptkp, created_at, updated_at) FROM stdin;
1	01	Jual Beli	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
2	02	Tukar Menukar	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
3	03	Hibah	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
4	04	Hibah Wasiat	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
5	05	Waris	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
6	06	Pemasukan Dalam Perseroan atau Badan Hukum Lainnya	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
7	07	Pemisahan Hak yang Mengakibatkan Peralihan	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
8	08	Penunjukan Pembeli Dalam Lelang	3	2	2021-03-03 13:55:20	2021-03-03 13:55:20
9	09	Pelaksanaan Putusan Hakim yang Mempunyai Kekuatan Hukum Tetap	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
10	10	Penggabungan Usaha	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
11	11	Peleburan Usaha	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
12	12	Pemekaran Usaha	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
13	13	Hadiah	3	2	2021-03-03 13:55:20	2021-03-03 13:55:20
14	14	Perolehan Hak Rumah Sederhana Sehat dan RSS Melalui KPR Bersubsidi	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
15	15	Pemberian Hak Baru Sebagai Kelanjutan Pelepasan Hak	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
16	16	Pemberian Hak Baru Diluar Pelepasan Hak	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_kecamatan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_kecamatan (s_idkecamatan, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_namakecamatan, s_latitude, s_longitude, created_at, updated_at) FROM stdin;
28	62	12	073	KARUSEN JANANG	\N	\N	\N	\N
29	62	12	071	PAKU	\N	\N	\N	\N
30	62	12	072	RAREN BATUAH	\N	\N	\N	\N
31	62	12	111	PAJU EPAT	\N	\N	\N	\N
32	62	12	070	DUSUN TENGAH	\N	\N	\N	\N
33	62	12	080	PEMATANG KARAU	\N	\N	\N	\N
34	62	12	090	AWANG	\N	\N	\N	\N
35	62	12	100	PETANGKEP TUTUI	\N	\N	\N	\N
36	62	12	110	DUSUN TIMUR	\N	\N	\N	\N
37	62	12	120	BENUA LIMA	\N	\N	\N	\N
38	00	00	000	LUAR DAERAH	\N	\N	2021-03-03 14:04:29	2021-03-03 14:04:29
\.


--
-- Data for Name: s_kelurahan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_kelurahan (s_idkelurahan, s_idkecamatan, s_kd_propinsi, s_kd_dati2, s_kd_kecamatan, s_kd_kelurahan, s_namakelurahan, s_latitude, s_longitude, created_at, updated_at) FROM stdin;
176	29	62	12	071	004	TAMPA	\N	\N	\N	\N
177	29	62	12	071	005	KALAMUS	\N	\N	\N	\N
178	31	62	12	111	001	TELANG BARU	\N	\N	\N	\N
179	31	62	12	111	002	JURU BANU	\N	\N	\N	\N
180	31	62	12	111	010	MURUTUWU	\N	\N	\N	\N
181	31	62	12	111	012	TAMPU LANGIT	\N	\N	\N	\N
182	31	62	12	111	014	MAIPE	\N	\N	\N	\N
183	31	62	12	111	015	BALAWA	\N	\N	\N	\N
184	30	62	12	072	013	PURI	\N	\N	\N	\N
185	30	62	12	072	014	LENGGANG	\N	\N	\N	\N
186	30	62	12	072	017	BATUAH	\N	\N	\N	\N
187	30	62	12	072	018	UNSUM	\N	\N	\N	\N
188	30	62	12	072	019	BARUYAN	\N	\N	\N	\N
189	29	62	12	071	006	SIMPANG BINGKUANG	\N	\N	\N	\N
190	29	62	12	071	007	PATUNG	\N	\N	\N	\N
191	29	62	12	071	021	RUNGGU RAYA	\N	\N	\N	\N
192	37	62	12	120	026	NN	\N	\N	\N	\N
193	32	62	12	070	022	SUMBER GARUNGGUNG	\N	\N	\N	\N
194	30	62	12	072	004	NN	\N	\N	\N	\N
195	28	62	12	073	004	SIMPANG NANENG	\N	\N	\N	\N
196	28	62	12	073	005	LAGAN	\N	\N	\N	\N
197	28	62	12	073	006	PUTUT TAWULUH	\N	\N	\N	\N
198	28	62	12	073	007	KANDRIS	\N	\N	\N	\N
199	32	62	12	070	023	MUARA AWANG	\N	\N	\N	\N
200	29	62	12	071	023	KUPANG BARU	\N	\N	\N	\N
201	29	62	12	071	024	LUAU JAWUK	\N	\N	\N	\N
202	29	62	12	071	025	TARINSING	\N	\N	\N	\N
203	29	62	12	071	026	BANTAI NAPU	\N	\N	\N	\N
204	29	62	12	071	027	PANGKAN	\N	\N	\N	\N
205	29	62	12	071	028	GANDRUNG	\N	\N	\N	\N
206	30	62	12	072	020	TURAN AMIS	\N	\N	\N	\N
207	30	62	12	072	021	TANGKUM	\N	\N	\N	\N
208	30	62	12	072	022	SIBUNG	\N	\N	\N	\N
209	30	62	12	072	023	MALINTUT	\N	\N	\N	\N
210	33	62	12	080	011	TUMPUNG ULUNG	\N	\N	\N	\N
211	33	62	12	080	012	MURUDUYUNG	\N	\N	\N	\N
212	33	62	12	080	013	SUMBEREJO	\N	\N	\N	\N
213	34	62	12	090	009	PIANGGU	\N	\N	\N	\N
214	34	62	12	090	010	DANAU	\N	\N	\N	\N
215	34	62	12	090	011	JANAH MANSIWUI	\N	\N	\N	\N
216	35	62	12	100	008	BETANG NALONG	\N	\N	\N	\N
217	35	62	12	100	009	LALAP	\N	\N	\N	\N
218	36	62	12	110	021	HARARA	\N	\N	\N	\N
219	36	62	12	110	022	MANGKARAP	\N	\N	\N	\N
220	36	62	12	110	023	MATARAH	\N	\N	\N	\N
221	36	62	12	110	024	GUMPA	\N	\N	\N	\N
222	36	62	12	110	025	MARAGUT	\N	\N	\N	\N
223	36	62	12	110	026	SUMUR	\N	\N	\N	\N
224	31	62	12	111	016	SIONG	\N	\N	\N	\N
225	31	62	12	111	017	KALINAPU	\N	\N	\N	\N
226	37	62	12	120	007	GUDANG SENG	\N	\N	\N	\N
227	29	62	12	071	022	PAKU BETO	\N	\N	\N	\N
228	28	62	12	073	001	DAYU	\N	\N	\N	\N
229	28	62	12	073	002	WURAN	\N	\N	\N	\N
230	28	62	12	073	003	IPUMEA	\N	\N	\N	\N
231	32	62	12	070	008	SAING	\N	\N	\N	\N
232	32	62	12	070	009	RODOK	\N	\N	\N	\N
233	32	62	12	070	010	AMPAH II	\N	\N	\N	\N
234	32	62	12	070	011	PUTAI	\N	\N	\N	\N
235	32	62	12	070	012	AMPAH	\N	\N	\N	\N
236	32	62	12	070	020	NETAMPIN	\N	\N	\N	\N
237	33	62	12	080	001	MUARA PLANTAU	\N	\N	\N	\N
238	33	62	12	080	002	KETAP	\N	\N	\N	\N
239	33	62	12	080	003	KUPANG BERSIH	\N	\N	\N	\N
240	33	62	12	080	004	TUYAU	\N	\N	\N	\N
241	33	62	12	080	005	PINANG TUNGGAL	\N	\N	\N	\N
242	33	62	12	080	006	NAGALEAH	\N	\N	\N	\N
243	33	62	12	080	007	LAMPEONG	\N	\N	\N	\N
244	33	62	12	080	008	BARARAWA	\N	\N	\N	\N
245	33	62	12	080	009	BAMBULUNG	\N	\N	\N	\N
246	33	62	12	080	010	LEBO	\N	\N	\N	\N
247	34	62	12	090	001	JANAH JARI	\N	\N	\N	\N
248	34	62	12	090	002	BANGKIRAYEN	\N	\N	\N	\N
249	34	62	12	090	003	TANGKAN	\N	\N	\N	\N
250	34	62	12	090	004	AMPARI	\N	\N	\N	\N
251	34	62	12	090	005	WUNGKUR NANAKAN	\N	\N	\N	\N
252	34	62	12	090	006	HAYAPING	\N	\N	\N	\N
253	34	62	12	090	007	BIWAN	\N	\N	\N	\N
254	34	62	12	090	008	APAR BATU	\N	\N	\N	\N
255	35	62	12	100	001	PULAU PADANG	\N	\N	\N	\N
256	31	62	12	111	013	TELANG 	\N	\N	\N	\N
257	35	62	12	100	010	NN	\N	\N	\N	\N
258	35	62	12	100	013	JANGO	\N	\N	\N	\N
259	35	62	12	100	011	NN	\N	\N	\N	\N
260	35	62	12	100	002	KAMBITIN	\N	\N	\N	\N
261	35	62	12	100	003	RAMANIA	\N	\N	\N	\N
262	35	62	12	100	004	BENTOT	\N	\N	\N	\N
263	35	62	12	100	005	AMPARI BURA	\N	\N	\N	\N
264	35	62	12	100	006	KOTAM	\N	\N	\N	\N
265	36	62	12	110	003	PULAU PATAI	\N	\N	\N	\N
266	36	62	12	110	005	MAGANTIS	\N	\N	\N	\N
267	36	62	12	110	006	JAAR	\N	\N	\N	\N
268	36	62	12	110	007	MATABU	\N	\N	\N	\N
269	36	62	12	110	009	SARAPAT	\N	\N	\N	\N
270	36	62	12	110	016	JAWETEN	\N	\N	\N	\N
271	36	62	12	110	017	HARINGEN	\N	\N	\N	\N
272	36	62	12	110	018	DORONG	\N	\N	\N	\N
273	36	62	12	110	019	DIDI	\N	\N	\N	\N
274	36	62	12	110	020	KARANG LANGIT	\N	\N	\N	\N
275	37	62	12	120	001	TANIRAN	\N	\N	\N	\N
276	37	62	12	120	002	KANDRIS	\N	\N	\N	\N
277	37	62	12	120	003	BANYU LANDAS	\N	\N	\N	\N
278	37	62	12	120	004	BAGOK	\N	\N	\N	\N
279	37	62	12	120	005	BAMBAN	\N	\N	\N	\N
280	37	62	12	120	006	TEWAH PUPUH	\N	\N	\N	\N
281	35	62	12	100	007	NN	\N	\N	\N	\N
282	35	62	12	100	012	MAWANI	\N	\N	\N	\N
283	36	62	12	110	008	TAMIANG LAYANG	\N	\N	\N	\N
284	38	62	12	000	000	LUAR DAERAH	-3.4758984864668454	115.88877185214845	\N	2021-03-03 02:03:22
\.


--
-- Data for Name: s_koderekening; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_koderekening (s_korekid, s_korektipe, s_korekkelompok, s_korekjenis, s_korekobjek, s_korekrincian, s_korekrinciansub, s_koreknama, s_korekketerangan, created_at, updated_at) FROM stdin;
1	4	1	1	11			Bea Perolehan Hak Atas Tanah dan Bangunan (BPHTB) - LRA Bea Perolehan Hak Atas Tanah dan Bangunan (BPHTB) - LRA		2021-03-03 13:55:20	2021-03-03 13:55:20
2	4	1	1	11	01		BPHTB		2021-03-03 13:55:20	2021-03-03 13:55:20
3	4	1	7	11	01		Denda BPHTB		2021-03-03 13:55:20	2021-03-03 13:55:20
4	4	1	7	11	02		Sanksi BPHTB		2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_notaris; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_notaris (s_idnotaris, s_namanotaris, s_alamatnotaris, s_kodenotaris, s_sknotaris, s_noid_bpn, s_tgl1notaris, s_tgl2notaris, s_statusnotaris, s_npwpd_notaris, s_daerahkerja, created_at, updated_at) FROM stdin;
1	Gustimansah, SH, M.Kn	Jalan Kolonel Wahid Udin Kel. Serasan Jaya Kec. Sekayu	01	01/01/2018	1	2020-01-01 00:00:00	2020-12-31 00:00:00	1	123	Lilin	2021-03-03 13:55:19	2021-03-03 13:55:19
2	Marleni, SH, M.Kn	Jalan Mawar no 2 Kel. Serasan Jaya Kec. Sekayu	01	01/01/2018	2	2020-01-01 00:00:00	2020-12-31 00:00:00	1	123	Lilin	2021-03-03 13:55:19	2021-03-03 13:55:19
\.


--
-- Data for Name: s_pejabat; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_pejabat (s_idpejabat, s_namapejabat, s_jabatanpejabat, s_nippejabat, s_golonganpejabat, s_alamat, s_filettd, created_at, updated_at) FROM stdin;
1	Rafiansyah, SE, M.Si	Kasubbid Pendataan, Pendaftaran, Penetapan dan Penilaian PBB dan BPHTB	19730709 199303 1 005	Penata Tingkat I - III D	Jalan raya no 1 Kel. Serasan Jaya Kec. Sekayu		2021-03-03 13:55:20	2021-03-03 13:55:20
2	H. RIKI JUNAIDI, AP, M. Si	Kepala Badan Pengelola Pajak dan Retribusi Daerah	19740615 199311 1 001	Pembina Utama Muda - IV C	Jalan melati no 3 Kel. Serasan Jaya Kec. Sekayu		2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_pemda; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_pemda (s_idpemda, s_namaprov, s_namakabkot, s_namaibukotakabkot, s_kodeprovinsi, s_kodekabkot, s_namainstansi, s_namasingkatinstansi, s_alamatinstansi, s_notelinstansi, s_namabank, s_norekbank, s_logo, s_namacabang, s_kodecabang, s_kodepos, s_email, s_urlbphtb, created_at, updated_at) FROM stdin;
1	Jawa Timur	Kabupaten Tulungagung	Tulungagung	35	04	Badan Pendapatan Daerah	BAPENDA	Jalan A. Yani Timur No. 37	(0355) 320098	Badan Pendapatan Daerah	149.30.00001	upload/logo_tulungagung.png	Tulungagung	08	66217	bapenda@tulungagung.co.id		2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_persyaratan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_persyaratan (s_idpersyaratan, s_idjenistransaksi, s_namapersyaratan, s_idwajib_up, s_lokasimenu, "order", created_at, updated_at) FROM stdin;
1	1	Foto copy SPPT tahun bersangkutan dan Foto copy Bukti pembayaran PBB / STTS	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
2	1	Foto copy KTP yang diberi kuasa	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
3	1	Surat kuasa dari wajib pajak (apabila dikuasakan) materai 6 ribu	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
4	1	Surat Pernyataan perolehan harga transaksi Jual Beli	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
5	1	FC. KTP Penjual (Suami/Istri)	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
6	1	Foto copy KTP Pembeli (untuk Akta Jual Beli)	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
7	1	Foto copy draft akta (akta jual beli) 1 set dengan hal. lengkap dan / atau bukti hak kepemilikan tanah	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
8	2	FC. Kartu Keluarga / KK	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
9	2	FC. KTP Penerima Hak Tukar	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
10	2	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
11	2	Pernyataan Harga Transaksi	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
12	2	Surat Kuasa materai 6000	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
13	2	Foto copy KTP yang diberi kuasa	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
14	2	FC. KTP Pemberi Hak Tukar	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
15	2	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
16	2	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	9	2021-03-03 13:55:20	2021-03-03 13:55:20
17	3	Surat Kuasa materai 6000	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
18	3	Foto copy KTP Pemberi Hibah	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
19	3	FC. Kartu Identitas / KTP / Surat Domisili Dari Desa	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
20	3	Foto copy KTP yang diberi kuasa	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
21	3	Pernyataan Harga Transaksi	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
22	3	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
23	3	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
24	3	FC. Kartu Keluarga / KK	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
25	4	Surat Kuasa materai 6000	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
26	4	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
27	4	FC. Surat Kematian	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
28	4	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
29	4	Foto copy KTP yang diberi kuasa	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
30	4	FC. Kartu Identitas / KTP / Surat Domisili Dari Desa	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
31	4	Pernyataan Harga Transaksi	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
32	4	FC. Kartu Keluarga / KK	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
33	4	Surat Wasiat	1	1	9	2021-03-03 13:55:20	2021-03-03 13:55:20
34	5	KTP Penerima Kuasa	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
35	5	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
36	5	Foto Lokasi Objek Pajak 	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
37	5	FC. Kartu Keluarga / KK	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
38	5	FC. Kartu Identitas / KTP / Surat Domisili Dari Desa	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
39	5	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
40	5	FC. Surat Kematian	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
41	5	FC. Surat Keterangan Waris / SKW	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
42	5	Surat Kuasa bermaterei 6 ribu	1	1	9	2021-03-03 13:55:20	2021-03-03 13:55:20
43	5	Surat Pernyataan Nilai Pasar	1	1	10	2021-03-03 13:55:20	2021-03-03 13:55:20
44	6	Foto copy KTP yang diberi kuasa	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
45	6	FC. KTP	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
46	6	Akta Badan Hukum	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
47	6	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
48	6	FC. Kartu Keluarga / KK	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
49	6	Pernyataan Harga Transaksi	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
50	6	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
51	6	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
52	6	Surat Kuasa materai 6000	1	1	9	2021-03-03 13:55:20	2021-03-03 13:55:20
53	7	Foto copy KTP yang diberi kuasa	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
54	7	Surat Kuasa materai 6000	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
55	7	Pernyataan Harga Transaksi	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
56	7	FC. Kartu Keluarga / KK	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
57	7	FC. Bukti Kepemilikan (Sertifikat, Warkah. SPT)	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
58	7	FC. KTP	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
59	7	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
60	7	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
61	8	FC. KTP	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
62	8	FC. Bukti Kepemilikan (Sertifikat, Warkah. SPT)	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
63	8	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
64	8	Pernyataan Harga Transaksi	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
65	8	Surat Kuasa materai 6000	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
66	8	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
67	8	Risalah/Tanda Penerima Uang Hasil lelang dari Kantor Pelayanan Kakayaan Negara dan Lelang Malang	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
68	8	Foto copy KTP yang diberi kuasa	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
69	8	FC. Kartu Keluarga / KK	1	1	9	2021-03-03 13:55:20	2021-03-03 13:55:20
70	9	Surat Kuasa materai 6000	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
71	9	Fotokopi Sertifikat	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
72	9	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
73	9	FC. KTP	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
74	9	FC. Kartu Keluarga / KK	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
75	9	Putusan Pengadilan	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
76	9	Foto copy KTP yang diberi kuasa	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
77	9	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
78	9	Pernyataan Harga Transaksi	1	1	9	2021-03-03 13:55:20	2021-03-03 13:55:20
79	10	Pernyataan Harga Transaksi	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
80	10	Fotokopi Sertifikat	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
81	10	FC. KTP	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
82	10	Surat Kuasa materai 6000	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
83	10	Foto copy KTP yang diberi kuasa	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
84	10	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
85	10	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
86	10	FC. Kartu Keluarga / KK	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
87	11	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
88	11	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
89	11	FC. Kartu Keluarga / KK	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
90	11	FC. KTP	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
91	11	Foto copy KTP yang diberi kuasa	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
92	11	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
93	11	Pernyataan Harga Transaksi	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
94	11	Surat Kuasa materai 6000	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
95	12	Pernyataan Harga Transaksi	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
96	12	SPPT PBB Tahun Terakhir	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
97	12	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
98	12	Foto copy KTP yang diberi kuasa	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
99	12	Surat Kuasa materai 6000	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
100	12	FC. Kartu Keluarga / KK	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
101	12	FC. KTP	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
102	12	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
103	13	Pernyataan Harga Transaksi	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
104	13	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
105	13	Fotokopi Sertifikat	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
106	13	FC. Kartu Keluarga / KK	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
107	13	FC. KTP Penerima Hadiah	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
108	13	FC. KTP Pemberi Hadiah	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
109	13	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
110	13	Surat Kuasa materai 6000	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
111	13	Foto copy KTP yang diberi kuasa	1	1	9	2021-03-03 13:55:20	2021-03-03 13:55:20
112	14	Foto copy KTP yang diberi kuasa	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
113	14	FC. Kartu Keluarga / KK	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
114	14	Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
115	14	Surat Kuasa materai 6000	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
116	14	Pernyataan Harga Transaksi	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
117	14	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
118	14	FC. KTP	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
119	14	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
120	15	FC. KTP	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
121	15	Fotokopi SPT (Surat Tanah)	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
122	15	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
123	15	 Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
124	15	Surat Kuasa materai 6000	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
125	15	Foto copy KTP yang diberi kuasa	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
126	15	FC. Kartu Keluarga / KK	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
127	15	Pernyataan Harga Transaksi	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
128	16	 Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
129	16	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
130	16	FC. Kartu Keluarga / KK	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
131	16	Pernyataan Harga Transaksi	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
132	16	Foto copy KTP yang diberi kuasa	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
133	16	FC. KTP	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
134	16	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
135	16	Surat Kuasa materai 6000	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
136	17	Foto copy KTP yang diberi kuasa	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
137	17	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
138	17	FC. Kartu Keluarga / KK	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
139	17	Surat Kuasa materai 6000	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
140	17	FC. KTP	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
141	17	Pernyataan Harga Transaksi	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
142	17	 Bukti-bukti lainnya yang dapat dijadikan perhitungan BPHTB	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
143	17	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
144	18	Akta PHB	1	1	1	2021-03-03 13:55:20	2021-03-03 13:55:20
145	18	FC.Kartu Keluarga/KK	1	1	2	2021-03-03 13:55:20	2021-03-03 13:55:20
146	18	FC. Surat Kematian	1	1	3	2021-03-03 13:55:20	2021-03-03 13:55:20
147	18	FC. Surat Keterangan Waris / SKW	1	1	4	2021-03-03 13:55:20	2021-03-03 13:55:20
148	18	FC. SPPT PBB Tahun Pengajuan ( Wajib Lunas PBB 5 Tahun + Tahun Pengajuan)	1	1	5	2021-03-03 13:55:20	2021-03-03 13:55:20
149	18	FC. Sertifikat Tanah / Kutipan C & Surat Keterangan Riwayat Tanah Dari Desa	1	1	6	2021-03-03 13:55:20	2021-03-03 13:55:20
150	18	Formulir Surat Setoran Pajak Daerah BPHTB/ SSPD BPHTB	1	1	7	2021-03-03 13:55:20	2021-03-03 13:55:20
151	18	FC. Kartu Identitas/ KTP/ Surat Domisili dari desa	1	1	8	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_presentase_wajar; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_presentase_wajar (s_idpresentase_wajar, s_nilaimin, s_nilaimax, s_ketpresentase_wajar, s_css_warna, created_at, updated_at) FROM stdin;
1	0	50	Tidak Wajar	red	2021-03-03 13:55:20	2021-03-03 13:55:20
2	50.1	60	Kurang Wajar	yellow	2021-03-03 13:55:20	2021-03-03 13:55:20
3	60.1	80	Sedang	blue	2021-03-03 13:55:20	2021-03-03 13:55:20
4	80.1	100	Wajar	green	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_status; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_status (s_id_status, s_nama_status, created_at, updated_at) FROM stdin;
1	Aktif	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Tidak	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_status_bayar; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_status_bayar (s_id_status_bayar, s_nama_status_bayar, created_at, updated_at) FROM stdin;
1	Sudah	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Belum	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_status_berkas; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_status_berkas (s_id_status_berkas, s_nama_status_berkas, created_at, updated_at) FROM stdin;
1	Lengkap	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Tidak Lengkap	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_status_dptnpoptkp; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_status_dptnpoptkp (s_id_dptnpoptkp, s_nama_dptnpoptkp, created_at, updated_at) FROM stdin;
1	NPOPTKP Setiap Transaksi Dapat	2021-03-03 13:55:20	2021-03-03 13:55:20
2	NPOPTKP 1 Tahun Sekali dapatnya	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_status_kabid; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_status_kabid (s_id_status_kabid, s_nama_status_kabid, created_at, updated_at) FROM stdin;
1	Setuju	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Tidak Setuju	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_status_lihat; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_status_lihat (s_id_status_lihat, s_nama_status_lihat, created_at, updated_at) FROM stdin;
1	Sudah	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Belum	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_status_npwpd; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_status_npwpd (s_idstatus_npwpd, s_nama_status, created_at, updated_at) FROM stdin;
1	Ada	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Tidak Ada	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_status_pelayanan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_status_pelayanan (s_id_status_layanan, s_nama_status_layanan, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: s_status_perhitungan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_status_perhitungan (s_idstatus_pht, s_nama, created_at, updated_at) FROM stdin;
1	Dari Perbandingan NJOP dengan Nilai Transaksi	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Dari NJOP PBB	2021-03-03 13:55:20	2021-03-03 13:55:20
3	Dari Nilai Transaksi	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_target_bphtb; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_target_bphtb (s_id_target_bphtb, s_id_target_status, s_tahun_target, s_nilai_target, s_keterangan, created_at, updated_at) FROM stdin;
1	1	2020	1000000000	Target BPHTB Murni	2021-03-03 13:55:20	2021-03-03 13:55:20
2	2	2020	1600000000	Target BPHTB Perubahan	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_target_status; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_target_status (s_id_target_status, s_nama_status, created_at, updated_at) FROM stdin;
1	Murni	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Perubahan	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_tarif_bphtb; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_tarif_bphtb (s_idtarifbphtb, s_tarif_bphtb, s_tgl_awal, s_tgl_akhir, s_dasar_hukum, created_at, updated_at) FROM stdin;
1	5	2015-01-01	2030-12-01		2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_tarifnpoptkp; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_tarifnpoptkp (s_idtarifnpoptkp, s_idjenistransaksinpoptkp, s_tarifnpoptkp, s_tarifnpoptkptambahan, s_dasarhukumnpoptkp, s_statusnpoptkp, s_tglberlaku_awal, s_tglberlaku_akhir, created_at, updated_at) FROM stdin;
1	1	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
2	2	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
3	3	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
4	4	300000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
5	5	300000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
6	6	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
7	7	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
8	8	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
9	9	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
10	10	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
11	11	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
12	12	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
13	13	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
14	14	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
15	15	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
16	16	60000000	0	Dasar Hukum	1	2015-01-01	2030-12-01	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_tempo; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_tempo (s_idtempo, s_haritempo, s_tglberlaku_awal, s_tglberlaku_akhir, s_id_status, created_at, updated_at) FROM stdin;
1	5	2015-01-01 00:00:00	2030-12-01 00:00:00	1	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: s_users_api; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_users_api (s_idusers_api, s_username, s_password, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: s_wajib_up; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.s_wajib_up (s_idwajib_up, s_ket_wjb, created_at, updated_at) FROM stdin;
1	Wajib	2021-03-03 13:55:20	2021-03-03 13:55:20
2	Tidak	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Data for Name: t_bpn; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_bpn (t_idbpn, t_noakta, t_tglakta, t_namappat, t_nop, t_nib, t_ntpd, t_nik, t_npwp, t_namawp, t_kelurahanop, t_kecamatanop, t_kotaop, t_luastanahop, t_jenishak, t_koordinat_x, t_koordinat_y, t_tgltransaksi, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_file_keringanan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_file_keringanan (t_idfile_keringanan, t_idspt, t_iduser_upload, letak_file, nama_file, t_tgl_upload, t_keterangan_file, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_file_objek; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_file_objek (t_idfile_objek, t_idspt, t_iduser_upload, letak_file, nama_file, t_tgl_upload, t_keterangan_file, created_at, updated_at) FROM stdin;
1	22	1	upload/file_fotoobjek/2021/jualbeli/22/	thanks.png	2021-03-07 15:23:34	\N	2021-03-07 15:23:34	2021-03-07 15:23:34
\.


--
-- Data for Name: t_filesyarat_bphtb; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_filesyarat_bphtb (t_id_filesyarat, t_idspt, s_idpersyaratan, s_idjenistransaksi, t_iduser_upload, letak_file, nama_file, t_tgl_upload, t_keterangan_file, created_at, updated_at) FROM stdin;
1	22	1	1	1	upload/file_syarat/2021/jualbeli/22/	smpd_logo_hd.png	2021-03-07 15:21:03	\N	2021-03-07 15:21:03	2021-03-07 15:21:03
2	22	2	1	1	upload/file_syarat/2021/jualbeli/22/	blobs.png	2021-03-07 15:21:20	\N	2021-03-07 15:21:20	2021-03-07 15:21:20
3	22	3	1	1	upload/file_syarat/2021/jualbeli/22/	smpd.png	2021-03-07 15:21:29	\N	2021-03-07 15:21:29	2021-03-07 15:21:29
4	22	4	1	1	upload/file_syarat/2021/jualbeli/22/	edge4.png	2021-03-07 15:21:53	\N	2021-03-07 15:21:53	2021-03-07 15:21:53
5	22	5	1	1	upload/file_syarat/2021/jualbeli/22/	human.png	2021-03-07 15:22:10	\N	2021-03-07 15:22:10	2021-03-07 15:22:10
6	22	6	1	1	upload/file_syarat/2021/jualbeli/22/	blobs-2.png	2021-03-07 15:22:38	\N	2021-03-07 15:22:38	2021-03-07 15:22:38
7	22	7	1	1	upload/file_syarat/2021/jualbeli/22/	mybphtb.png	2021-03-07 15:22:50	\N	2021-03-07 15:22:50	2021-03-07 15:22:50
\.


--
-- Data for Name: t_inputajb; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_inputajb (t_idajb, t_idspt, t_tgl_ajb, t_no_ajb, t_iduser_input, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_laporbulanan_detail; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_laporbulanan_detail (t_idlaporbulanan, t_idajb, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_laporbulanan_head; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_laporbulanan_head (t_idlaporbulanan, t_no_lapor, t_tgl_lapor, t_untuk_bulan, t_untuk_tahun, t_keterangan, t_iduser_input, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_nik_bersama; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_nik_bersama (t_id_nik_bersama, t_idspt, t_nik_bersama, t_nama_bersama, t_nama_jalan, t_rt_bersama, t_rw_bersama, t_kecamatan_bersama, t_kelurahan_bersama, t_kabkota_bersama, t_nohp_bersama, t_kodepos_bersama, t_npwp_bersama, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_notif_validasi_berkas; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_notif_validasi_berkas (t_id_notif_berkas, t_idspt, t_isipesan, t_dari, t_tglkirim, t_untuk, s_id_status_lihat, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_notif_validasi_kabid; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_notif_validasi_kabid (t_id_notif_kabid, t_idspt, t_isipesan, t_dari, t_tglkirim, t_untuk, s_id_status_lihat, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_angsuran; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pelayanan_angsuran (t_idangsuran, t_idspt, t_noangsuran, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_angsuran, t_jmlhangsuran, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_keberatan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pelayanan_keberatan (t_idkeberatan, t_idspt, t_nokeberatan, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_keberatan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, t_jmlhpotongan_disetujui, t_persentase_disetujui, t_jmlh_spt_sebenarnya, t_jmlh_spt_hasilpot, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_keringanan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pelayanan_keringanan (t_idkeringanan, t_idspt, t_nokeringanan, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_keringanan, t_jmlhpotongan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, t_jmlhpotongan_disetujui, t_persentase_disetujui, t_jmlh_spt_sebenarnya, t_jmlh_spt_hasilpot, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_mutasi; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pelayanan_mutasi (t_idmutasi, t_idspt, t_nomutasi, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_mutasi, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_pembatalan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pelayanan_pembatalan (t_idpembatalan, t_idspt, t_nopembatalan, t_tglpengajuan, t_keterangan_pembatalan, t_iduser_pengajuan, t_nosk_pembatalan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pelayanan_penundaan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pelayanan_penundaan (t_idpenundaan, t_idspt, t_nopenundaan, t_tglpengajuan, t_iduser_pengajuan, t_keterangan_permohoanan, t_nosk_penundaan, t_idstatus_disetujui, t_idoperator, t_tglpersetujuan, t_keterangan_disetujui, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pembayaran_bphtb; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pembayaran_bphtb (t_id_pembayaran, t_idspt, t_nourut_bayar, s_id_status_bayar, t_iduser_bayar, t_tglpembayaran_pokok, t_jmlhbayar_pokok, t_tglpembayaran_denda, t_jmlhbayar_denda, t_jmlhbayar_total, created_at, updated_at) FROM stdin;
5	22	\N	1	1	2021-03-07 15:27:00	2000000	2021-03-07 15:27:00	0	2000000	\N	\N
\.


--
-- Data for Name: t_pembayaran_skpdkb; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pembayaran_skpdkb (t_id_bayar_skpdkb, t_idspt, t_idskpdkb, t_nourut_bayar, s_id_status_bayar, t_iduser_bayar, t_tglpembayaran_skpdkb, t_jmlhbayar_skpdkb, t_tglpembayaran_denda, t_jmlhbayar_denda, t_jmlhbayar_total, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pembayaran_skpdkbt; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pembayaran_skpdkbt (t_id_bayar_skpdkb, t_idspt, t_idskpdkbt, t_nourut_bayar, s_id_status_bayar, t_iduser_bayar, t_tglpembayaran_skpdkbt, t_jmlhbayar_skpdkbt, t_tglpembayaran_denda, t_jmlhbayar_denda, t_jmlhbayar_total, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pemeriksaan; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pemeriksaan (t_idpemeriksa, t_idspt, t_tglpenetapan, t_tglby_system, t_iduser_buat, t_idpejabat1, t_idpejabat2, t_idpejabat3, t_idpejabat4, t_noperiksa, t_keterangan, t_nop_sppt, t_tahun_sppt, t_luastanah_pemeriksa, t_njoptanah_pemeriksa, t_totalnjoptanah_pemeriksa, t_luasbangunan_pemeriksa, t_njopbangunan_pemeriksa, t_totalnjopbangunan_pemeriksa, t_grandtotalnjop_pemeriksa, t_nilaitransaksispt_pemeriksa, t_trf_aphb_kali_pemeriksa, t_trf_aphb_bagi_pemeriksa, t_permeter_tanah_pemeriksa, t_idtarifbphtb_pemeriksa, t_persenbphtb_pemeriksa, t_npop_bphtb_pemeriksa, t_npoptkp_bphtb_pemeriksa, t_npopkp_bphtb_pemeriksa, t_nilaibphtb_pemeriksa, t_idpersetujuan_pemeriksa, isdeleted, t_idoperator_deleted, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pen_denda_ajb_notaris; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pen_denda_ajb_notaris (t_id_ajbdenda, t_idspt, t_idnotaris, t_tglpenetapan, t_nourut_ajbdenda, t_nilai_ajbdenda, t_iduser_buat, s_id_status_bayar, t_tglbayar_ajbdenda, t_jmlhbayar_ajbdenda, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_pen_denda_lpor_notaris; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_pen_denda_lpor_notaris (t_id_pendenda, t_idlaporbulanan, t_idnotaris, t_tglpenetapan, t_nourut_pendenda, t_nilai_pendenda, t_iduser_buat, s_id_status_bayar, t_tglbayar_pendenda, t_jmlhbayar_pendenda, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_skpdkb; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_skpdkb (t_idskpdkb, t_idspt, t_tglpenetapan, t_tglby_system, t_iduser_buat, t_nop_sppt, t_tahun_sppt, t_luastanah_skpdkb, t_njoptanah_skpdkb, t_totalnjoptanah_skpdkb, t_luasbangunan_skpdkb, t_njopbangunan_skpdkb, t_totalnjopbangunan_skpdkb, t_grandtotalnjop_skpdkb, t_nilaitransaksispt_skpdkb, t_trf_aphb_kali_skpdkb, t_trf_aphb_bagi_skpdkb, t_permeter_tanah_skpdkb, t_idtarifbphtb_skpdkb, t_persenbphtb_skpdkb, t_npop_bphtb_skpdkb, t_npoptkp_bphtb_skpdkb, t_npopkp_bphtb_skpdkb, t_nilai_bayar_sblumnya, t_nilai_pokok_skpdkb, t_jmlh_blndenda, t_jmlh_dendaskpdkb, t_jmlh_totalskpdkb, t_tglbuat_kodebyar, t_nourut_kodebayar, t_kodebayar_skpdkb, t_tgljatuhtempo_skpdkb, t_idjenisketetapan, t_idpersetujuan_skpdkb, isdeleted, t_idoperator_deleted, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_skpdkbt; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_skpdkbt (t_idskpdkbt, t_idspt, t_idskpdkb, t_tglpenetapan, t_tglby_system, t_iduser_buat, t_nop_sppt, t_tahun_sppt, t_luastanah_skpdkbt, t_njoptanah_skpdkbt, t_totalnjoptanah_skpdkbt, t_luasbangunan_skpdkbt, t_njopbangunan_skpdkbt, t_totalnjopbangunan_skpdkbt, t_grandtotalnjop_skpdkbt, t_nilaitransaksispt_skpdkbt, t_trf_aphb_kali_skpdkbt, t_trf_aphb_bagi_skpdkbt, t_permeter_tanah_skpdkbt, t_idtarifbphtb_skpdkbt, t_persenbphtb_skpdkbt, t_npop_bphtb_skpdkbt, t_npoptkp_bphtb_skpdkbt, t_npopkp_bphtb_skpdkbt, t_nilai_bayar_sblumnya, t_nilai_pokok_skpdkbt, t_jmlh_blndenda, t_jmlh_dendaskpdkbt, t_jmlh_totalskpdkbt, t_tglbuat_kodebyar, t_nourut_kodebayar, t_kodebayar_skpdkbt, t_tgljatuhtempo_skpdkbt, t_idjenisketetapan, t_idpersetujuan_skpdkb, isdeleted, t_idoperator_deleted, created_at, updated_at) FROM stdin;
\.


--
-- Data for Name: t_spt; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_spt (t_idspt, t_kohirspt, t_tgldaftar_spt, t_tglby_system, t_periodespt, t_iduser_buat, t_idjenistransaksi, t_idnotaris_spt, t_npwpd, t_idstatus_npwpd, t_idbidang_usaha, t_nama_pembeli, t_nik_pembeli, t_npwp_pembeli, t_jalan_pembeli, t_kabkota_pembeli, t_idkec_pembeli, t_namakecamatan_pembeli, t_idkel_pembeli, t_namakelurahan_pembeli, t_rt_pembeli, t_rw_pembeli, t_nohp_pembeli, t_notelp_pembeli, t_email_pembeli, t_kodepos_pembeli, t_siup_pembeli, t_nib_pembeli, t_ketdomisili_pembeli, t_b_nama_pngjwb_pembeli, t_b_nik_pngjwb_pembeli, t_b_npwp_pngjwb_pembeli, t_b_statusjab_pngjwb_pembeli, t_b_jalan_pngjwb_pembeli, t_b_kabkota_pngjwb_pembeli, t_b_idkec_pngjwb_pembeli, t_b_namakec_pngjwb_pembeli, t_b_idkel_pngjwb_pembeli, t_b_namakel_pngjwb_pembeli, t_b_rt_pngjwb_pembeli, t_b_rw_pngjwb_pembeli, t_b_nohp_pngjwb_pembeli, t_b_notelp_pngjwb_pembeli, t_b_email_pngjwb_pembeli, t_b_kodepos_pngjwb_pembeli, t_nop_sppt, t_tahun_sppt, t_nama_sppt, t_jalan_sppt, t_kabkota_sppt, t_kecamatan_sppt, t_kelurahan_sppt, t_rt_sppt, t_rw_sppt, t_luastanah_sismiop, t_luasbangunan_sismiop, t_njoptanah_sismiop, t_njopbangunan_sismiop, t_luastanah, t_njoptanah, t_totalnjoptanah, t_luasbangunan, t_njopbangunan, t_totalnjopbangunan, t_grandtotalnjop, t_nilaitransaksispt, t_tarif_pembagian_aphb_kali, t_tarif_pembagian_aphb_bagi, t_permeter_tanah, t_idjenisfasilitas, t_idjenishaktanah, t_idjenisdoktanah, t_idpengurangan, t_id_jenistanah, t_nosertifikathaktanah, t_tgldok_tanah, s_latitude, s_longitude, t_idtarifbphtb, t_persenbphtb, t_npop_bphtb, t_npoptkp_bphtb, t_npopkp_bphtb, t_nilai_bphtb_fix, t_nilai_bphtb_real, t_idbidang_penjual, t_nama_penjual, t_nik_penjual, t_npwp_penjual, t_jalan_penjual, t_kabkota_penjual, t_idkec_penjual, t_namakec_penjual, t_idkel_penjual, t_namakel_penjual, t_rt_penjual, t_rw_penjual, t_nohp_penjual, t_notelp_penjual, t_email_penjual, t_kodepos_penjual, t_siup_penjual, t_nib_penjual, t_ketdomisili_penjual, t_b_nama_pngjwb_penjual, t_b_nik_pngjwb_penjual, t_b_npwp_pngjwb_penjual, t_b_statusjab_pngjwb_penjual, t_b_jalan_pngjwb_penjual, t_b_kabkota_pngjwb_penjual, t_b_idkec_pngjwb_penjual, t_b_namakec_pngjwb_penjual, t_b_idkel_pngjwb_penjual, t_b_namakel_pngjwb_penjual, t_b_rt_pngjwb_penjual, t_b_rw_pngjwb_penjual, t_b_nohp_pngjwb_penjual, t_b_notelp_pngjwb_penjual, t_b_email_pngjwb_penjual, t_b_kodepos_pngjwb_penjual, t_tglbuat_kodebyar, t_nourut_kodebayar, t_kodebayar_bphtb, t_tglketetapan_spt, t_tgljatuhtempo_spt, t_idjenisketetapan, t_idpersetujuan_bphtb, isdeleted, t_idoperator_deleted, created_at, updated_at, t_ntpd) FROM stdin;
22	1	2021-03-07 15:16:17	2021-03-07 15:16:17	2021	\N	1	1	\N	\N	1	Ahmad	1111111111111111	11.111.111.1-111.111	Jalan	Barito Timur	28	KARUSEN JANANG	195	SIMPANG NANENG	1	1	085785795123	085785795123	ahmad@gmail.com	63383	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	62.12.072.023.002.0038.0	2021	HERI, SP	AMPAH	BARITO TIMUR	RAREN BATUAH	MALINTUT	000	00	20000	0	1700	0	20000	1700	34000000	0	0	0	34000000	100000000	\N	\N	5000	\N	1	1	\N	1	1234567890	1970-01-01 00:00:00	-7.423844	109.231146	1	5	100000000	60000000	40000000	2000000	2000000	1	Budi	1213123123213131	11.212.131.3-131.313	Jalan	Barito Timur	29	PAKU	176	TAMPA	1	1	085785795123	02131341414314	budi@gmail.com	63383	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	\N	2021-03-07 15:26:44	1	3504012100001	\N	\N	1	1	\N	\N	2021-03-07 15:16:17	2021-03-07 15:27:06	012100001
\.


--
-- Data for Name: t_validasi_berkas; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_validasi_berkas (t_id_validasi_berkas, t_idspt, s_id_status_berkas, t_tglvalidasi, t_iduser_validasi, t_keterangan_berkas, created_at, updated_at, t_persyaratan_validasi_berkas) FROM stdin;
12	22	1	2021-03-07 15:26:06	1	ok lengkap	2021-03-07 15:26:06	2021-03-07 15:26:06	1,2,3,4,5,6,7
\.


--
-- Data for Name: t_validasi_kabid; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.t_validasi_kabid (t_id_validasi_kabid, t_idspt, s_id_status_kabid, t_tglvalidasi, t_iduser_validasi, t_keterangan_kabid, created_at, updated_at) FROM stdin;
8	22	1	2021-03-07 15:26:44	1	ok setuju	2021-03-07 15:26:44	2021-03-07 15:26:44
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: edge
--

COPY public.users (id, name, email, username, email_verified_at, password, s_tipe_pejabat, s_idpejabat, s_idnotaris, s_id_hakakses, remember_token, created_at, updated_at) FROM stdin;
1	admin	admin@mail.com	admin	\N	$2y$10$7jqRyVsHABg4EZ2gbXUp7OHipOHgT1v4H1.4SXL8koreIH4.DfRVa	\N	\N	\N	1	\N	2021-03-03 13:55:19	2021-03-03 13:55:19
2	validasi-berkas	validasi-berkas@mail.com	validasiberkas	\N	$2y$10$ssPdMA9Faa2YfhbSerGdIODy1znY4XvmxjBwhnpcTDyKv3fQwhj26	\N	\N	\N	2	\N	2021-03-03 13:55:19	2021-03-03 13:55:19
3	validasi-kabid	validasi-kabid@mail.com	validasikabid	\N	$2y$10$3MsSk8A.VXHm1wZEgYN0FeFFjIgPqS12qP8nHH4SqXK00t1WE1HqW	\N	\N	\N	3	\N	2021-03-03 13:55:19	2021-03-03 13:55:19
4	notaris	notaris@mail.com	notaris	\N	$2y$10$lsogpgQSFze.HDCULv2x1.ZNLpVDmfVka95HYW5YIyifziW0smLv2	\N	\N	1	4	\N	2021-03-03 13:55:19	2021-03-03 13:55:19
5	bpn	bpn@mail.com	bpn	\N	$2y$10$5IEWd6/IkEf.nCOXOpNU5.BbfbnvwSGhY/104ug4znwYD0xIEN9lq	\N	\N	\N	5	\N	2021-03-03 13:55:19	2021-03-03 13:55:19
6	kpppratama	kpppratama@mail.com	kpppratama	\N	$2y$10$LVnKpAz5sHMdSxeKR8nsV.79fGb0FVBXNZ/.RB3unS8muO5wL.9CC	\N	\N	\N	6	\N	2021-03-03 13:55:19	2021-03-03 13:55:19
7	operatorbendahara	operatorbendahara@mail.com	operatorbendahara	\N	$2y$10$Ap5K5zFxgAdBzdr79aPXGeeCu8K3xe9zbfEAFXCs3TEAknncXLfkW	\N	\N	\N	7	\N	2021-03-03 13:55:19	2021-03-03 13:55:19
8	wajibpajak	wajibpajak@mail.com	wajibpajak	\N	$2y$10$Mhlq3Ll4dJoM6kSZFd3G0eWnzdnWEtloGlXJbkr7/NczxAVUCYafW	\N	\N	\N	8	\N	2021-03-03 13:55:19	2021-03-03 13:55:19
9	pemeriksa	pemeriksa@mail.com	pemeriksa	\N	$2y$10$6kjfs78WJuffmZ96TjZo4OVtH6ywT6bShijVHHoBAVPL/SYcU6IxG	\N	\N	\N	9	\N	2021-03-03 13:55:20	2021-03-03 13:55:20
\.


--
-- Name: activity_log_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.activity_log_id_seq', 12, true);


--
-- Name: failed_jobs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.failed_jobs_id_seq', 1, false);


--
-- Name: his_getbphtb_bpn_t_idhis_getbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.his_getbphtb_bpn_t_idhis_getbpn_seq', 1, false);


--
-- Name: his_getpbb_bpn_t_idhis_pbbbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.his_getpbb_bpn_t_idhis_pbbbpn_seq', 1, false);


--
-- Name: his_postdatabpn_t_idhis_posbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.his_postdatabpn_t_idhis_posbpn_seq', 1, false);


--
-- Name: integrasi_sertifikat_bpn_t_idsertifikat_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.integrasi_sertifikat_bpn_t_idsertifikat_seq', 1, false);


--
-- Name: migrations_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.migrations_id_seq', 42, true);


--
-- Name: permissions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.permissions_id_seq', 1, false);


--
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.roles_id_seq', 1, false);


--
-- Name: s_acuan_jenistanah_s_idacuan_jenis_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_acuan_jenistanah_s_idacuan_jenis_seq', 4, false);


--
-- Name: s_acuan_s_idacuan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_acuan_s_idacuan_seq', 4, false);


--
-- Name: s_background_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_background_id_seq', 3, false);


--
-- Name: s_hak_akses_s_id_hakakses_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_hak_akses_s_id_hakakses_seq', 10, false);


--
-- Name: s_harga_history_s_idhistory_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_harga_history_s_idhistory_seq', 1, false);


--
-- Name: s_jenis_bidangusaha_s_idbidang_usaha_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_jenis_bidangusaha_s_idbidang_usaha_seq', 3, false);


--
-- Name: s_jenisdoktanah_s_iddoktanah_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_jenisdoktanah_s_iddoktanah_seq', 8, false);


--
-- Name: s_jenisfasilitas_s_idjenisfasilitas_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_jenisfasilitas_s_idjenisfasilitas_seq', 7, false);


--
-- Name: s_jenishaktanah_s_idhaktanah_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_jenishaktanah_s_idhaktanah_seq', 7, false);


--
-- Name: s_jenisketetapan_s_idjenisketetapan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_jenisketetapan_s_idjenisketetapan_seq', 6, false);


--
-- Name: s_jenispengurangan_s_idpengurangan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_jenispengurangan_s_idpengurangan_seq', 4, false);


--
-- Name: s_jenistanah_id_jenistanah_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_jenistanah_id_jenistanah_seq', 3, false);


--
-- Name: s_jenistransaksi_s_idjenistransaksi_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_jenistransaksi_s_idjenistransaksi_seq', 17, false);


--
-- Name: s_kecamatan_s_idkecamatan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_kecamatan_s_idkecamatan_seq', 38, true);


--
-- Name: s_kelurahan_s_idkelurahan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_kelurahan_s_idkelurahan_seq', 284, true);


--
-- Name: s_koderekening_s_korekid_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_koderekening_s_korekid_seq', 5, false);


--
-- Name: s_notaris_s_idnotaris_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_notaris_s_idnotaris_seq', 3, false);


--
-- Name: s_pejabat_s_idpejabat_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_pejabat_s_idpejabat_seq', 3, false);


--
-- Name: s_pemda_s_idpemda_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_pemda_s_idpemda_seq', 1, false);


--
-- Name: s_persyaratan_s_idpersyaratan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_persyaratan_s_idpersyaratan_seq', 152, false);


--
-- Name: s_presentase_wajar_s_idpresentase_wajar_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_presentase_wajar_s_idpresentase_wajar_seq', 5, false);


--
-- Name: s_status_bayar_s_id_status_bayar_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_status_bayar_s_id_status_bayar_seq', 3, false);


--
-- Name: s_status_berkas_s_id_status_berkas_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_status_berkas_s_id_status_berkas_seq', 3, false);


--
-- Name: s_status_dptnpoptkp_s_id_dptnpoptkp_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_status_dptnpoptkp_s_id_dptnpoptkp_seq', 3, false);


--
-- Name: s_status_kabid_s_id_status_kabid_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_status_kabid_s_id_status_kabid_seq', 3, false);


--
-- Name: s_status_lihat_s_id_status_lihat_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_status_lihat_s_id_status_lihat_seq', 3, false);


--
-- Name: s_status_npwpd_s_idstatus_npwpd_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_status_npwpd_s_idstatus_npwpd_seq', 3, false);


--
-- Name: s_status_pelayanan_s_id_status_layanan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_status_pelayanan_s_id_status_layanan_seq', 1, false);


--
-- Name: s_status_perhitungan_s_idstatus_pht_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_status_perhitungan_s_idstatus_pht_seq', 4, false);


--
-- Name: s_status_s_id_status_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_status_s_id_status_seq', 3, false);


--
-- Name: s_target_bphtb_s_id_target_bphtb_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_target_bphtb_s_id_target_bphtb_seq', 3, false);


--
-- Name: s_target_status_s_id_target_status_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_target_status_s_id_target_status_seq', 3, false);


--
-- Name: s_tarif_bphtb_s_idtarifbphtb_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_tarif_bphtb_s_idtarifbphtb_seq', 2, false);


--
-- Name: s_tarifnpoptkp_s_idtarifnpoptkp_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_tarifnpoptkp_s_idtarifnpoptkp_seq', 17, false);


--
-- Name: s_tempo_s_idtempo_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_tempo_s_idtempo_seq', 2, false);


--
-- Name: s_users_api_s_idusers_api_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_users_api_s_idusers_api_seq', 1, false);


--
-- Name: s_wajib_up_s_idwajib_up_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.s_wajib_up_s_idwajib_up_seq', 3, false);


--
-- Name: t_bpn_t_idbpn_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_bpn_t_idbpn_seq', 1, false);


--
-- Name: t_file_keringanan_t_idfile_keringanan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_file_keringanan_t_idfile_keringanan_seq', 1, false);


--
-- Name: t_file_objek_t_idfile_objek_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_file_objek_t_idfile_objek_seq', 1, true);


--
-- Name: t_filesyarat_bphtb_t_id_filesyarat_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_filesyarat_bphtb_t_id_filesyarat_seq', 7, true);


--
-- Name: t_inputajb_t_idajb_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_inputajb_t_idajb_seq', 1, false);


--
-- Name: t_laporbulanan_head_t_idlaporbulanan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_laporbulanan_head_t_idlaporbulanan_seq', 1, false);


--
-- Name: t_nik_bersama_t_id_nik_bersama_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_nik_bersama_t_id_nik_bersama_seq', 1, false);


--
-- Name: t_notif_validasi_berkas_t_id_notif_berkas_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_notif_validasi_berkas_t_id_notif_berkas_seq', 1, false);


--
-- Name: t_notif_validasi_kabid_t_id_notif_kabid_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_notif_validasi_kabid_t_id_notif_kabid_seq', 1, false);


--
-- Name: t_pelayanan_angsuran_t_idangsuran_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pelayanan_angsuran_t_idangsuran_seq', 1, false);


--
-- Name: t_pelayanan_keberatan_t_idkeberatan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pelayanan_keberatan_t_idkeberatan_seq', 1, false);


--
-- Name: t_pelayanan_keringanan_t_idkeringanan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pelayanan_keringanan_t_idkeringanan_seq', 1, false);


--
-- Name: t_pelayanan_mutasi_t_idmutasi_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pelayanan_mutasi_t_idmutasi_seq', 1, false);


--
-- Name: t_pelayanan_pembatalan_t_idpembatalan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pelayanan_pembatalan_t_idpembatalan_seq', 1, false);


--
-- Name: t_pelayanan_penundaan_t_idpenundaan_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pelayanan_penundaan_t_idpenundaan_seq', 1, false);


--
-- Name: t_pembayaran_bphtb_t_id_pembayaran_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pembayaran_bphtb_t_id_pembayaran_seq', 5, true);


--
-- Name: t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pembayaran_skpdkb_t_id_bayar_skpdkb_seq', 1, false);


--
-- Name: t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pembayaran_skpdkbt_t_id_bayar_skpdkb_seq', 1, false);


--
-- Name: t_pemeriksaan_t_idpemeriksa_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pemeriksaan_t_idpemeriksa_seq', 1, false);


--
-- Name: t_pen_denda_ajb_notaris_t_id_ajbdenda_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pen_denda_ajb_notaris_t_id_ajbdenda_seq', 1, false);


--
-- Name: t_pen_denda_lpor_notaris_t_id_pendenda_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_pen_denda_lpor_notaris_t_id_pendenda_seq', 1, false);


--
-- Name: t_skpdkb_t_idskpdkb_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_skpdkb_t_idskpdkb_seq', 1, false);


--
-- Name: t_skpdkbt_t_idskpdkbt_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_skpdkbt_t_idskpdkbt_seq', 1, false);


--
-- Name: t_spt_t_idspt_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_spt_t_idspt_seq', 22, true);


--
-- Name: t_validasi_berkas_t_id_validasi_berkas_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_validasi_berkas_t_id_validasi_berkas_seq', 12, true);


--
-- Name: t_validasi_kabid_t_id_validasi_kabid_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.t_validasi_kabid_t_id_validasi_kabid_seq', 8, true);


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: edge
--

SELECT pg_catalog.setval('public.users_id_seq', 1, false);


--
-- Name: activity_log activity_log_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.activity_log
    ADD CONSTRAINT activity_log_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_pkey PRIMARY KEY (id);


--
-- Name: failed_jobs failed_jobs_uuid_unique; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.failed_jobs
    ADD CONSTRAINT failed_jobs_uuid_unique UNIQUE (uuid);


--
-- Name: his_getbphtb_bpn his_getbphtb_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.his_getbphtb_bpn
    ADD CONSTRAINT his_getbphtb_bpn_pkey PRIMARY KEY (t_idhis_getbpn);


--
-- Name: his_getpbb_bpn his_getpbb_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.his_getpbb_bpn
    ADD CONSTRAINT his_getpbb_bpn_pkey PRIMARY KEY (t_idhis_pbbbpn);


--
-- Name: his_postdatabpn his_postdatabpn_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.his_postdatabpn
    ADD CONSTRAINT his_postdatabpn_pkey PRIMARY KEY (t_idhis_posbpn);


--
-- Name: integrasi_sertifikat_bpn integrasi_sertifikat_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.integrasi_sertifikat_bpn
    ADD CONSTRAINT integrasi_sertifikat_bpn_pkey PRIMARY KEY (t_idsertifikat);


--
-- Name: migrations migrations_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.migrations
    ADD CONSTRAINT migrations_pkey PRIMARY KEY (id);


--
-- Name: model_has_permissions model_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_pkey PRIMARY KEY (permission_id, model_id, model_type);


--
-- Name: model_has_roles model_has_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_pkey PRIMARY KEY (role_id, model_id, model_type);


--
-- Name: notifications notifications_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.notifications
    ADD CONSTRAINT notifications_pkey PRIMARY KEY (id);


--
-- Name: permissions permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.permissions
    ADD CONSTRAINT permissions_pkey PRIMARY KEY (id);


--
-- Name: role_has_permissions role_has_permissions_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_pkey PRIMARY KEY (permission_id, role_id);


--
-- Name: roles roles_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- Name: s_acuan_jenistanah s_acuan_jenistanah_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_acuan_jenistanah
    ADD CONSTRAINT s_acuan_jenistanah_pkey PRIMARY KEY (s_idacuan_jenis);


--
-- Name: s_acuan s_acuan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_acuan
    ADD CONSTRAINT s_acuan_pkey PRIMARY KEY (s_idacuan);


--
-- Name: s_background s_background_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_background
    ADD CONSTRAINT s_background_pkey PRIMARY KEY (id);


--
-- Name: s_hak_akses s_hak_akses_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_hak_akses
    ADD CONSTRAINT s_hak_akses_pkey PRIMARY KEY (s_id_hakakses);


--
-- Name: s_harga_history s_harga_history_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_harga_history
    ADD CONSTRAINT s_harga_history_pkey PRIMARY KEY (s_idhistory);


--
-- Name: s_jenis_bidangusaha s_jenis_bidangusaha_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenis_bidangusaha
    ADD CONSTRAINT s_jenis_bidangusaha_pkey PRIMARY KEY (s_idbidang_usaha);


--
-- Name: s_jenisdoktanah s_jenisdoktanah_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenisdoktanah
    ADD CONSTRAINT s_jenisdoktanah_pkey PRIMARY KEY (s_iddoktanah);


--
-- Name: s_jenisfasilitas s_jenisfasilitas_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenisfasilitas
    ADD CONSTRAINT s_jenisfasilitas_pkey PRIMARY KEY (s_idjenisfasilitas);


--
-- Name: s_jenishaktanah s_jenishaktanah_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenishaktanah
    ADD CONSTRAINT s_jenishaktanah_pkey PRIMARY KEY (s_idhaktanah);


--
-- Name: s_jenisketetapan s_jenisketetapan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenisketetapan
    ADD CONSTRAINT s_jenisketetapan_pkey PRIMARY KEY (s_idjenisketetapan);


--
-- Name: s_jenispengurangan s_jenispengurangan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenispengurangan
    ADD CONSTRAINT s_jenispengurangan_pkey PRIMARY KEY (s_idpengurangan);


--
-- Name: s_jenistanah s_jenistanah_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenistanah
    ADD CONSTRAINT s_jenistanah_pkey PRIMARY KEY (id_jenistanah);


--
-- Name: s_jenistransaksi s_jenistransaksi_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenistransaksi
    ADD CONSTRAINT s_jenistransaksi_pkey PRIMARY KEY (s_idjenistransaksi);


--
-- Name: s_kecamatan s_kecamatan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_kecamatan
    ADD CONSTRAINT s_kecamatan_pkey PRIMARY KEY (s_idkecamatan);


--
-- Name: s_kelurahan s_kelurahan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_kelurahan
    ADD CONSTRAINT s_kelurahan_pkey PRIMARY KEY (s_idkelurahan);


--
-- Name: s_koderekening s_koderekening_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_koderekening
    ADD CONSTRAINT s_koderekening_pkey PRIMARY KEY (s_korekid);


--
-- Name: s_notaris s_notaris_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_notaris
    ADD CONSTRAINT s_notaris_pkey PRIMARY KEY (s_idnotaris);


--
-- Name: s_pejabat s_pejabat_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_pejabat
    ADD CONSTRAINT s_pejabat_pkey PRIMARY KEY (s_idpejabat);


--
-- Name: s_pemda s_pemda_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_pemda
    ADD CONSTRAINT s_pemda_pkey PRIMARY KEY (s_idpemda);


--
-- Name: s_persyaratan s_persyaratan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_persyaratan
    ADD CONSTRAINT s_persyaratan_pkey PRIMARY KEY (s_idpersyaratan);


--
-- Name: s_presentase_wajar s_presentase_wajar_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_presentase_wajar
    ADD CONSTRAINT s_presentase_wajar_pkey PRIMARY KEY (s_idpresentase_wajar);


--
-- Name: s_status_bayar s_status_bayar_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_bayar
    ADD CONSTRAINT s_status_bayar_pkey PRIMARY KEY (s_id_status_bayar);


--
-- Name: s_status_berkas s_status_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_berkas
    ADD CONSTRAINT s_status_berkas_pkey PRIMARY KEY (s_id_status_berkas);


--
-- Name: s_status_dptnpoptkp s_status_dptnpoptkp_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_dptnpoptkp
    ADD CONSTRAINT s_status_dptnpoptkp_pkey PRIMARY KEY (s_id_dptnpoptkp);


--
-- Name: s_status_kabid s_status_kabid_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_kabid
    ADD CONSTRAINT s_status_kabid_pkey PRIMARY KEY (s_id_status_kabid);


--
-- Name: s_status_lihat s_status_lihat_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_lihat
    ADD CONSTRAINT s_status_lihat_pkey PRIMARY KEY (s_id_status_lihat);


--
-- Name: s_status_npwpd s_status_npwpd_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_npwpd
    ADD CONSTRAINT s_status_npwpd_pkey PRIMARY KEY (s_idstatus_npwpd);


--
-- Name: s_status_pelayanan s_status_pelayanan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_pelayanan
    ADD CONSTRAINT s_status_pelayanan_pkey PRIMARY KEY (s_id_status_layanan);


--
-- Name: s_status_perhitungan s_status_perhitungan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status_perhitungan
    ADD CONSTRAINT s_status_perhitungan_pkey PRIMARY KEY (s_idstatus_pht);


--
-- Name: s_status s_status_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_status
    ADD CONSTRAINT s_status_pkey PRIMARY KEY (s_id_status);


--
-- Name: s_target_bphtb s_target_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_target_bphtb
    ADD CONSTRAINT s_target_bphtb_pkey PRIMARY KEY (s_id_target_bphtb);


--
-- Name: s_target_status s_target_status_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_target_status
    ADD CONSTRAINT s_target_status_pkey PRIMARY KEY (s_id_target_status);


--
-- Name: s_tarif_bphtb s_tarif_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_tarif_bphtb
    ADD CONSTRAINT s_tarif_bphtb_pkey PRIMARY KEY (s_idtarifbphtb);


--
-- Name: s_tarifnpoptkp s_tarifnpoptkp_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_tarifnpoptkp
    ADD CONSTRAINT s_tarifnpoptkp_pkey PRIMARY KEY (s_idtarifnpoptkp);


--
-- Name: s_tempo s_tempo_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_tempo
    ADD CONSTRAINT s_tempo_pkey PRIMARY KEY (s_idtempo);


--
-- Name: s_users_api s_users_api_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_users_api
    ADD CONSTRAINT s_users_api_pkey PRIMARY KEY (s_idusers_api);


--
-- Name: s_wajib_up s_wajib_up_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_wajib_up
    ADD CONSTRAINT s_wajib_up_pkey PRIMARY KEY (s_idwajib_up);


--
-- Name: t_bpn t_bpn_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_bpn
    ADD CONSTRAINT t_bpn_pkey PRIMARY KEY (t_idbpn);


--
-- Name: t_file_keringanan t_file_keringanan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_file_keringanan
    ADD CONSTRAINT t_file_keringanan_pkey PRIMARY KEY (t_idfile_keringanan);


--
-- Name: t_file_objek t_file_objek_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_file_objek
    ADD CONSTRAINT t_file_objek_pkey PRIMARY KEY (t_idfile_objek);


--
-- Name: t_filesyarat_bphtb t_filesyarat_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_pkey PRIMARY KEY (t_id_filesyarat);


--
-- Name: t_inputajb t_inputajb_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_inputajb
    ADD CONSTRAINT t_inputajb_pkey PRIMARY KEY (t_idajb);


--
-- Name: t_laporbulanan_head t_laporbulanan_head_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_laporbulanan_head
    ADD CONSTRAINT t_laporbulanan_head_pkey PRIMARY KEY (t_idlaporbulanan);


--
-- Name: t_nik_bersama t_nik_bersama_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_nik_bersama
    ADD CONSTRAINT t_nik_bersama_pkey PRIMARY KEY (t_id_nik_bersama);


--
-- Name: t_notif_validasi_berkas t_notif_validasi_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_pkey PRIMARY KEY (t_id_notif_berkas);


--
-- Name: t_notif_validasi_kabid t_notif_validasi_kabid_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_pkey PRIMARY KEY (t_id_notif_kabid);


--
-- Name: t_pelayanan_angsuran t_pelayanan_angsuran_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_pkey PRIMARY KEY (t_idangsuran);


--
-- Name: t_pelayanan_keberatan t_pelayanan_keberatan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_pkey PRIMARY KEY (t_idkeberatan);


--
-- Name: t_pelayanan_keringanan t_pelayanan_keringanan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_pkey PRIMARY KEY (t_idkeringanan);


--
-- Name: t_pelayanan_mutasi t_pelayanan_mutasi_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_pkey PRIMARY KEY (t_idmutasi);


--
-- Name: t_pelayanan_pembatalan t_pelayanan_pembatalan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_pkey PRIMARY KEY (t_idpembatalan);


--
-- Name: t_pelayanan_penundaan t_pelayanan_penundaan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_pkey PRIMARY KEY (t_idpenundaan);


--
-- Name: t_pembayaran_bphtb t_pembayaran_bphtb_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_pkey PRIMARY KEY (t_id_pembayaran);


--
-- Name: t_pembayaran_skpdkb t_pembayaran_skpdkb_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_pkey PRIMARY KEY (t_id_bayar_skpdkb);


--
-- Name: t_pembayaran_skpdkbt t_pembayaran_skpdkbt_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_pkey PRIMARY KEY (t_id_bayar_skpdkb);


--
-- Name: t_pemeriksaan t_pemeriksaan_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_pkey PRIMARY KEY (t_idpemeriksa);


--
-- Name: t_pen_denda_ajb_notaris t_pen_denda_ajb_notaris_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_pkey PRIMARY KEY (t_id_ajbdenda);


--
-- Name: t_pen_denda_lpor_notaris t_pen_denda_lpor_notaris_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_pkey PRIMARY KEY (t_id_pendenda);


--
-- Name: t_skpdkb t_skpdkb_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_pkey PRIMARY KEY (t_idskpdkb);


--
-- Name: t_skpdkbt t_skpdkbt_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_pkey PRIMARY KEY (t_idskpdkbt);


--
-- Name: t_spt t_spt_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_pkey PRIMARY KEY (t_idspt);


--
-- Name: t_validasi_berkas t_validasi_berkas_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_pkey PRIMARY KEY (t_id_validasi_berkas);


--
-- Name: t_validasi_kabid t_validasi_kabid_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_pkey PRIMARY KEY (t_id_validasi_kabid);


--
-- Name: users users_email_unique; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: activity_log_log_name_index; Type: INDEX; Schema: public; Owner: edge
--

CREATE INDEX activity_log_log_name_index ON public.activity_log USING btree (log_name);


--
-- Name: causer; Type: INDEX; Schema: public; Owner: edge
--

CREATE INDEX causer ON public.activity_log USING btree (causer_type, causer_id);


--
-- Name: model_has_permissions_model_id_model_type_index; Type: INDEX; Schema: public; Owner: edge
--

CREATE INDEX model_has_permissions_model_id_model_type_index ON public.model_has_permissions USING btree (model_id, model_type);


--
-- Name: model_has_roles_model_id_model_type_index; Type: INDEX; Schema: public; Owner: edge
--

CREATE INDEX model_has_roles_model_id_model_type_index ON public.model_has_roles USING btree (model_id, model_type);


--
-- Name: notifications_notifiable_type_notifiable_id_index; Type: INDEX; Schema: public; Owner: edge
--

CREATE INDEX notifications_notifiable_type_notifiable_id_index ON public.notifications USING btree (notifiable_type, notifiable_id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: edge
--

CREATE INDEX password_resets_email_index ON public.password_resets USING btree (email);


--
-- Name: subject; Type: INDEX; Schema: public; Owner: edge
--

CREATE INDEX subject ON public.activity_log USING btree (subject_type, subject_id);


--
-- Name: model_has_permissions model_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.model_has_permissions
    ADD CONSTRAINT model_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: model_has_roles model_has_roles_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.model_has_roles
    ADD CONSTRAINT model_has_roles_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: role_has_permissions role_has_permissions_permission_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_permission_id_foreign FOREIGN KEY (permission_id) REFERENCES public.permissions(id) ON DELETE CASCADE;


--
-- Name: role_has_permissions role_has_permissions_role_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.role_has_permissions
    ADD CONSTRAINT role_has_permissions_role_id_foreign FOREIGN KEY (role_id) REFERENCES public.roles(id) ON DELETE CASCADE;


--
-- Name: s_acuan_jenistanah s_acuan_jenistanah_id_jenistanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_acuan_jenistanah
    ADD CONSTRAINT s_acuan_jenistanah_id_jenistanah_foreign FOREIGN KEY (id_jenistanah) REFERENCES public.s_jenistanah(id_jenistanah);


--
-- Name: s_background s_background_s_id_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_background
    ADD CONSTRAINT s_background_s_id_status_foreign FOREIGN KEY (s_id_status) REFERENCES public.s_status(s_id_status);


--
-- Name: s_jenistransaksi s_jenistransaksi_s_id_dptnpoptkp_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenistransaksi
    ADD CONSTRAINT s_jenistransaksi_s_id_dptnpoptkp_foreign FOREIGN KEY (s_id_dptnpoptkp) REFERENCES public.s_status_dptnpoptkp(s_id_dptnpoptkp);


--
-- Name: s_jenistransaksi s_jenistransaksi_s_idstatus_pht_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_jenistransaksi
    ADD CONSTRAINT s_jenistransaksi_s_idstatus_pht_foreign FOREIGN KEY (s_idstatus_pht) REFERENCES public.s_status_perhitungan(s_idstatus_pht);


--
-- Name: s_kelurahan s_kelurahan_s_idkecamatan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_kelurahan
    ADD CONSTRAINT s_kelurahan_s_idkecamatan_foreign FOREIGN KEY (s_idkecamatan) REFERENCES public.s_kecamatan(s_idkecamatan);


--
-- Name: s_persyaratan s_persyaratan_s_idwajib_up_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_persyaratan
    ADD CONSTRAINT s_persyaratan_s_idwajib_up_foreign FOREIGN KEY (s_idwajib_up) REFERENCES public.s_wajib_up(s_idwajib_up);


--
-- Name: s_target_bphtb s_target_bphtb_s_id_target_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_target_bphtb
    ADD CONSTRAINT s_target_bphtb_s_id_target_status_foreign FOREIGN KEY (s_id_target_status) REFERENCES public.s_target_status(s_id_target_status);


--
-- Name: s_tarifnpoptkp s_tarifnpoptkp_s_idjenistransaksinpoptkp_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_tarifnpoptkp
    ADD CONSTRAINT s_tarifnpoptkp_s_idjenistransaksinpoptkp_foreign FOREIGN KEY (s_idjenistransaksinpoptkp) REFERENCES public.s_jenistransaksi(s_idjenistransaksi);


--
-- Name: s_tarifnpoptkp s_tarifnpoptkp_s_statusnpoptkp_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_tarifnpoptkp
    ADD CONSTRAINT s_tarifnpoptkp_s_statusnpoptkp_foreign FOREIGN KEY (s_statusnpoptkp) REFERENCES public.s_status(s_id_status);


--
-- Name: s_tempo s_tempo_s_id_status_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.s_tempo
    ADD CONSTRAINT s_tempo_s_id_status_foreign FOREIGN KEY (s_id_status) REFERENCES public.s_status(s_id_status);


--
-- Name: t_file_keringanan t_file_keringanan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_file_keringanan
    ADD CONSTRAINT t_file_keringanan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_file_keringanan t_file_keringanan_t_iduser_upload_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_file_keringanan
    ADD CONSTRAINT t_file_keringanan_t_iduser_upload_foreign FOREIGN KEY (t_iduser_upload) REFERENCES public.users(id);


--
-- Name: t_file_objek t_file_objek_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_file_objek
    ADD CONSTRAINT t_file_objek_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_file_objek t_file_objek_t_iduser_upload_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_file_objek
    ADD CONSTRAINT t_file_objek_t_iduser_upload_foreign FOREIGN KEY (t_iduser_upload) REFERENCES public.users(id);


--
-- Name: t_filesyarat_bphtb t_filesyarat_bphtb_s_idjenistransaksi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_s_idjenistransaksi_foreign FOREIGN KEY (s_idjenistransaksi) REFERENCES public.s_jenistransaksi(s_idjenistransaksi);


--
-- Name: t_filesyarat_bphtb t_filesyarat_bphtb_s_idpersyaratan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_s_idpersyaratan_foreign FOREIGN KEY (s_idpersyaratan) REFERENCES public.s_persyaratan(s_idpersyaratan);


--
-- Name: t_filesyarat_bphtb t_filesyarat_bphtb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_filesyarat_bphtb t_filesyarat_bphtb_t_iduser_upload_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_filesyarat_bphtb
    ADD CONSTRAINT t_filesyarat_bphtb_t_iduser_upload_foreign FOREIGN KEY (t_iduser_upload) REFERENCES public.users(id);


--
-- Name: t_inputajb t_inputajb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_inputajb
    ADD CONSTRAINT t_inputajb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_inputajb t_inputajb_t_iduser_input_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_inputajb
    ADD CONSTRAINT t_inputajb_t_iduser_input_foreign FOREIGN KEY (t_iduser_input) REFERENCES public.users(id);


--
-- Name: t_laporbulanan_detail t_laporbulanan_detail_t_idajb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_laporbulanan_detail
    ADD CONSTRAINT t_laporbulanan_detail_t_idajb_foreign FOREIGN KEY (t_idajb) REFERENCES public.t_inputajb(t_idajb);


--
-- Name: t_laporbulanan_detail t_laporbulanan_detail_t_idlaporbulanan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_laporbulanan_detail
    ADD CONSTRAINT t_laporbulanan_detail_t_idlaporbulanan_foreign FOREIGN KEY (t_idlaporbulanan) REFERENCES public.t_laporbulanan_head(t_idlaporbulanan);


--
-- Name: t_laporbulanan_head t_laporbulanan_head_t_iduser_input_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_laporbulanan_head
    ADD CONSTRAINT t_laporbulanan_head_t_iduser_input_foreign FOREIGN KEY (t_iduser_input) REFERENCES public.users(id);


--
-- Name: t_nik_bersama t_nik_bersama_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_nik_bersama
    ADD CONSTRAINT t_nik_bersama_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_notif_validasi_berkas t_notif_validasi_berkas_s_id_status_lihat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_s_id_status_lihat_foreign FOREIGN KEY (s_id_status_lihat) REFERENCES public.users(id);


--
-- Name: t_notif_validasi_berkas t_notif_validasi_berkas_t_dari_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_t_dari_foreign FOREIGN KEY (t_dari) REFERENCES public.users(id);


--
-- Name: t_notif_validasi_berkas t_notif_validasi_berkas_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_notif_validasi_berkas t_notif_validasi_berkas_t_untuk_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_berkas
    ADD CONSTRAINT t_notif_validasi_berkas_t_untuk_foreign FOREIGN KEY (t_untuk) REFERENCES public.users(id);


--
-- Name: t_notif_validasi_kabid t_notif_validasi_kabid_s_id_status_lihat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_s_id_status_lihat_foreign FOREIGN KEY (s_id_status_lihat) REFERENCES public.users(id);


--
-- Name: t_notif_validasi_kabid t_notif_validasi_kabid_t_dari_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_t_dari_foreign FOREIGN KEY (t_dari) REFERENCES public.users(id);


--
-- Name: t_notif_validasi_kabid t_notif_validasi_kabid_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_notif_validasi_kabid t_notif_validasi_kabid_t_untuk_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_notif_validasi_kabid
    ADD CONSTRAINT t_notif_validasi_kabid_t_untuk_foreign FOREIGN KEY (t_untuk) REFERENCES public.users(id);


--
-- Name: t_pelayanan_angsuran t_pelayanan_angsuran_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_angsuran t_pelayanan_angsuran_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_angsuran t_pelayanan_angsuran_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_angsuran t_pelayanan_angsuran_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_angsuran
    ADD CONSTRAINT t_pelayanan_angsuran_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pelayanan_keberatan t_pelayanan_keberatan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_keberatan t_pelayanan_keberatan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_keberatan t_pelayanan_keberatan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_keberatan t_pelayanan_keberatan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keberatan
    ADD CONSTRAINT t_pelayanan_keberatan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pelayanan_keringanan t_pelayanan_keringanan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_keringanan t_pelayanan_keringanan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_keringanan t_pelayanan_keringanan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_keringanan t_pelayanan_keringanan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_keringanan
    ADD CONSTRAINT t_pelayanan_keringanan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pelayanan_mutasi t_pelayanan_mutasi_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_mutasi t_pelayanan_mutasi_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_mutasi t_pelayanan_mutasi_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_mutasi t_pelayanan_mutasi_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_mutasi
    ADD CONSTRAINT t_pelayanan_mutasi_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pelayanan_pembatalan t_pelayanan_pembatalan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_pembatalan t_pelayanan_pembatalan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_pembatalan t_pelayanan_pembatalan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_pembatalan t_pelayanan_pembatalan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_pembatalan
    ADD CONSTRAINT t_pelayanan_pembatalan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pelayanan_penundaan t_pelayanan_penundaan_t_idoperator_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_idoperator_foreign FOREIGN KEY (t_idoperator) REFERENCES public.users(id);


--
-- Name: t_pelayanan_penundaan t_pelayanan_penundaan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pelayanan_penundaan t_pelayanan_penundaan_t_idstatus_disetujui_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_idstatus_disetujui_foreign FOREIGN KEY (t_idstatus_disetujui) REFERENCES public.s_status_pelayanan(s_id_status_layanan);


--
-- Name: t_pelayanan_penundaan t_pelayanan_penundaan_t_iduser_pengajuan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pelayanan_penundaan
    ADD CONSTRAINT t_pelayanan_penundaan_t_iduser_pengajuan_foreign FOREIGN KEY (t_iduser_pengajuan) REFERENCES public.users(id);


--
-- Name: t_pembayaran_bphtb t_pembayaran_bphtb_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES public.s_status_bayar(s_id_status_bayar);


--
-- Name: t_pembayaran_bphtb t_pembayaran_bphtb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pembayaran_bphtb t_pembayaran_bphtb_t_iduser_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_bphtb
    ADD CONSTRAINT t_pembayaran_bphtb_t_iduser_bayar_foreign FOREIGN KEY (t_iduser_bayar) REFERENCES public.users(id);


--
-- Name: t_pembayaran_skpdkb t_pembayaran_skpdkb_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES public.s_status_bayar(s_id_status_bayar);


--
-- Name: t_pembayaran_skpdkb t_pembayaran_skpdkb_t_idskpdkb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_t_idskpdkb_foreign FOREIGN KEY (t_idskpdkb) REFERENCES public.t_skpdkb(t_idskpdkb);


--
-- Name: t_pembayaran_skpdkb t_pembayaran_skpdkb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pembayaran_skpdkb t_pembayaran_skpdkb_t_iduser_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkb
    ADD CONSTRAINT t_pembayaran_skpdkb_t_iduser_bayar_foreign FOREIGN KEY (t_iduser_bayar) REFERENCES public.users(id);


--
-- Name: t_pembayaran_skpdkbt t_pembayaran_skpdkbt_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES public.s_status_bayar(s_id_status_bayar);


--
-- Name: t_pembayaran_skpdkbt t_pembayaran_skpdkbt_t_idskpdkbt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_t_idskpdkbt_foreign FOREIGN KEY (t_idskpdkbt) REFERENCES public.t_skpdkb(t_idskpdkb);


--
-- Name: t_pembayaran_skpdkbt t_pembayaran_skpdkbt_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pembayaran_skpdkbt t_pembayaran_skpdkbt_t_iduser_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pembayaran_skpdkbt
    ADD CONSTRAINT t_pembayaran_skpdkbt_t_iduser_bayar_foreign FOREIGN KEY (t_iduser_bayar) REFERENCES public.users(id);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES public.users(id);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idpejabat1_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat1_foreign FOREIGN KEY (t_idpejabat1) REFERENCES public.s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idpejabat2_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat2_foreign FOREIGN KEY (t_idpejabat2) REFERENCES public.s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idpejabat3_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat3_foreign FOREIGN KEY (t_idpejabat3) REFERENCES public.s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idpejabat4_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idpejabat4_foreign FOREIGN KEY (t_idpejabat4) REFERENCES public.s_pejabat(s_idpejabat);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_idtarifbphtb_pemeriksa_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_idtarifbphtb_pemeriksa_foreign FOREIGN KEY (t_idtarifbphtb_pemeriksa) REFERENCES public.s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_pemeriksaan t_pemeriksaan_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pemeriksaan
    ADD CONSTRAINT t_pemeriksaan_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_pen_denda_ajb_notaris t_pen_denda_ajb_notaris_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES public.s_status_bayar(s_id_status_bayar);


--
-- Name: t_pen_denda_ajb_notaris t_pen_denda_ajb_notaris_t_idnotaris_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_t_idnotaris_foreign FOREIGN KEY (t_idnotaris) REFERENCES public.s_notaris(s_idnotaris);


--
-- Name: t_pen_denda_ajb_notaris t_pen_denda_ajb_notaris_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_pen_denda_ajb_notaris t_pen_denda_ajb_notaris_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_ajb_notaris
    ADD CONSTRAINT t_pen_denda_ajb_notaris_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_pen_denda_lpor_notaris t_pen_denda_lpor_notaris_s_id_status_bayar_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_s_id_status_bayar_foreign FOREIGN KEY (s_id_status_bayar) REFERENCES public.s_status_bayar(s_id_status_bayar);


--
-- Name: t_pen_denda_lpor_notaris t_pen_denda_lpor_notaris_t_idlaporbulanan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_t_idlaporbulanan_foreign FOREIGN KEY (t_idlaporbulanan) REFERENCES public.t_laporbulanan_head(t_idlaporbulanan);


--
-- Name: t_pen_denda_lpor_notaris t_pen_denda_lpor_notaris_t_idnotaris_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_t_idnotaris_foreign FOREIGN KEY (t_idnotaris) REFERENCES public.s_notaris(s_idnotaris);


--
-- Name: t_pen_denda_lpor_notaris t_pen_denda_lpor_notaris_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_pen_denda_lpor_notaris
    ADD CONSTRAINT t_pen_denda_lpor_notaris_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_skpdkb t_skpdkb_t_idjenisketetapan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idjenisketetapan_foreign FOREIGN KEY (t_idjenisketetapan) REFERENCES public.s_jenisketetapan(s_idjenisketetapan);


--
-- Name: t_skpdkb t_skpdkb_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES public.users(id);


--
-- Name: t_skpdkb t_skpdkb_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_skpdkb t_skpdkb_t_idtarifbphtb_skpdkb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_idtarifbphtb_skpdkb_foreign FOREIGN KEY (t_idtarifbphtb_skpdkb) REFERENCES public.s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_skpdkb t_skpdkb_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkb
    ADD CONSTRAINT t_skpdkb_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_skpdkbt t_skpdkbt_t_idjenisketetapan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idjenisketetapan_foreign FOREIGN KEY (t_idjenisketetapan) REFERENCES public.s_jenisketetapan(s_idjenisketetapan);


--
-- Name: t_skpdkbt t_skpdkbt_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES public.users(id);


--
-- Name: t_skpdkbt t_skpdkbt_t_idskpdkb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idskpdkb_foreign FOREIGN KEY (t_idskpdkb) REFERENCES public.t_skpdkb(t_idskpdkb);


--
-- Name: t_skpdkbt t_skpdkbt_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_skpdkbt t_skpdkbt_t_idtarifbphtb_skpdkbt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_idtarifbphtb_skpdkbt_foreign FOREIGN KEY (t_idtarifbphtb_skpdkbt) REFERENCES public.s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_skpdkbt t_skpdkbt_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_skpdkbt
    ADD CONSTRAINT t_skpdkbt_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_spt t_spt_t_b_idkec_pngjwb_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_b_idkec_pngjwb_pembeli_foreign FOREIGN KEY (t_b_idkec_pngjwb_pembeli) REFERENCES public.s_kecamatan(s_idkecamatan);


--
-- Name: t_spt t_spt_t_b_idkec_pngjwb_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_b_idkec_pngjwb_penjual_foreign FOREIGN KEY (t_b_idkec_pngjwb_penjual) REFERENCES public.s_kecamatan(s_idkecamatan);


--
-- Name: t_spt t_spt_t_b_idkel_pngjwb_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_b_idkel_pngjwb_pembeli_foreign FOREIGN KEY (t_b_idkel_pngjwb_pembeli) REFERENCES public.s_kelurahan(s_idkelurahan);


--
-- Name: t_spt t_spt_t_b_idkel_pngjwb_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_b_idkel_pngjwb_penjual_foreign FOREIGN KEY (t_b_idkel_pngjwb_penjual) REFERENCES public.s_kelurahan(s_idkelurahan);


--
-- Name: t_spt t_spt_t_id_jenistanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_id_jenistanah_foreign FOREIGN KEY (t_id_jenistanah) REFERENCES public.s_jenistanah(id_jenistanah);


--
-- Name: t_spt t_spt_t_idbidang_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idbidang_penjual_foreign FOREIGN KEY (t_idbidang_penjual) REFERENCES public.s_jenis_bidangusaha(s_idbidang_usaha);


--
-- Name: t_spt t_spt_t_idbidang_usaha_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idbidang_usaha_foreign FOREIGN KEY (t_idbidang_usaha) REFERENCES public.s_jenis_bidangusaha(s_idbidang_usaha);


--
-- Name: t_spt t_spt_t_idjenisdoktanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idjenisdoktanah_foreign FOREIGN KEY (t_idjenisdoktanah) REFERENCES public.s_jenisdoktanah(s_iddoktanah);


--
-- Name: t_spt t_spt_t_idjenisfasilitas_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idjenisfasilitas_foreign FOREIGN KEY (t_idjenisfasilitas) REFERENCES public.s_jenisfasilitas(s_idjenisfasilitas);


--
-- Name: t_spt t_spt_t_idjenishaktanah_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idjenishaktanah_foreign FOREIGN KEY (t_idjenishaktanah) REFERENCES public.s_jenishaktanah(s_idhaktanah);


--
-- Name: t_spt t_spt_t_idjenisketetapan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idjenisketetapan_foreign FOREIGN KEY (t_idjenisketetapan) REFERENCES public.s_jenisketetapan(s_idjenisketetapan);


--
-- Name: t_spt t_spt_t_idjenistransaksi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idjenistransaksi_foreign FOREIGN KEY (t_idjenistransaksi) REFERENCES public.s_jenistransaksi(s_idjenistransaksi);


--
-- Name: t_spt t_spt_t_idkec_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idkec_pembeli_foreign FOREIGN KEY (t_idkec_pembeli) REFERENCES public.s_kecamatan(s_idkecamatan);


--
-- Name: t_spt t_spt_t_idkec_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idkec_penjual_foreign FOREIGN KEY (t_idkec_penjual) REFERENCES public.s_kecamatan(s_idkecamatan);


--
-- Name: t_spt t_spt_t_idkel_pembeli_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idkel_pembeli_foreign FOREIGN KEY (t_idkel_pembeli) REFERENCES public.s_kelurahan(s_idkelurahan);


--
-- Name: t_spt t_spt_t_idkel_penjual_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idkel_penjual_foreign FOREIGN KEY (t_idkel_penjual) REFERENCES public.s_kelurahan(s_idkelurahan);


--
-- Name: t_spt t_spt_t_idnotaris_spt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idnotaris_spt_foreign FOREIGN KEY (t_idnotaris_spt) REFERENCES public.s_notaris(s_idnotaris);


--
-- Name: t_spt t_spt_t_idoperator_deleted_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idoperator_deleted_foreign FOREIGN KEY (t_idoperator_deleted) REFERENCES public.users(id);


--
-- Name: t_spt t_spt_t_idpengurangan_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idpengurangan_foreign FOREIGN KEY (t_idpengurangan) REFERENCES public.s_jenispengurangan(s_idpengurangan);


--
-- Name: t_spt t_spt_t_idstatus_npwpd_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idstatus_npwpd_foreign FOREIGN KEY (t_idstatus_npwpd) REFERENCES public.s_status_npwpd(s_idstatus_npwpd);


--
-- Name: t_spt t_spt_t_idtarifbphtb_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_idtarifbphtb_foreign FOREIGN KEY (t_idtarifbphtb) REFERENCES public.s_tarif_bphtb(s_idtarifbphtb);


--
-- Name: t_spt t_spt_t_iduser_buat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_spt
    ADD CONSTRAINT t_spt_t_iduser_buat_foreign FOREIGN KEY (t_iduser_buat) REFERENCES public.users(id);


--
-- Name: t_validasi_berkas t_validasi_berkas_s_id_status_berkas_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_s_id_status_berkas_foreign FOREIGN KEY (s_id_status_berkas) REFERENCES public.s_status_berkas(s_id_status_berkas);


--
-- Name: t_validasi_berkas t_validasi_berkas_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_validasi_berkas t_validasi_berkas_t_iduser_validasi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_validasi_berkas
    ADD CONSTRAINT t_validasi_berkas_t_iduser_validasi_foreign FOREIGN KEY (t_iduser_validasi) REFERENCES public.users(id);


--
-- Name: t_validasi_kabid t_validasi_kabid_s_id_status_kabid_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_s_id_status_kabid_foreign FOREIGN KEY (s_id_status_kabid) REFERENCES public.s_status_kabid(s_id_status_kabid);


--
-- Name: t_validasi_kabid t_validasi_kabid_t_idspt_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_t_idspt_foreign FOREIGN KEY (t_idspt) REFERENCES public.t_spt(t_idspt);


--
-- Name: t_validasi_kabid t_validasi_kabid_t_iduser_validasi_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.t_validasi_kabid
    ADD CONSTRAINT t_validasi_kabid_t_iduser_validasi_foreign FOREIGN KEY (t_iduser_validasi) REFERENCES public.users(id);


--
-- Name: users users_s_id_hakakses_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_s_id_hakakses_foreign FOREIGN KEY (s_id_hakakses) REFERENCES public.s_hak_akses(s_id_hakakses);


--
-- Name: users users_s_idnotaris_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_s_idnotaris_foreign FOREIGN KEY (s_idnotaris) REFERENCES public.s_notaris(s_idnotaris);


--
-- Name: users users_s_idpejabat_foreign; Type: FK CONSTRAINT; Schema: public; Owner: edge
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_s_idpejabat_foreign FOREIGN KEY (s_idpejabat) REFERENCES public.s_pejabat(s_idpejabat);


--
-- PostgreSQL database dump complete
--

