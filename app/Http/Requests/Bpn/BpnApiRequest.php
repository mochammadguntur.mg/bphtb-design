<?php

namespace App\Http\Requests\Bpn;

use Illuminate\Foundation\Http\FormRequest;

class BpnApiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_username' => 'required',
            's_passold' => 'required',
            's_password' => 'required',
            's_password2' => 'required'
        ];
    }

    public function messages(){
        return [
            's_username.required' => 'Username harus diisi!',
            's_passold.required' => 'Password Lama harus diisi!',
            's_password.required' => 'Password Baru harus diisi!',
            's_password2.required' => 'Ulangi Password Baru harus diisi!',
        ];
    }
}
