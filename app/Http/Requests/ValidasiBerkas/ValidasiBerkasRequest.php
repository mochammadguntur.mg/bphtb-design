<?php

namespace App\Http\Requests\ValidasiBerkas;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiBerkasRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            "t_persyaratan_validasi_berkas" => "required",
            "t_keterangan_berkas" => "required",
        ];
    }

}
