<?php

namespace App\Http\Requests\Setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class PejabatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_namapejabat' => 'required',
            's_nippejabat' => [
                'required',
                Rule::unique('s_pejabat', 's_nippejabat')->ignore($this->pejabat)
            ],
            's_filettd' => 'mimes:png|max:1024',
        ];
    }

    public function messages(){
        return [
            's_namapejabat.required' => 'Kolom harus diisi.',
            's_nippejabat.required' => 'Kolom harus diisi.',
            's_nippejabat.unique'    => 'Maaf, NIP Tersebut Sudah di Input',
            's_filettd.mimes' => 'Upload harus berupa file .png',
            
        ];
    }
}
