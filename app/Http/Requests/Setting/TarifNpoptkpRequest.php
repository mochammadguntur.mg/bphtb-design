<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class TarifNpoptkpRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            's_idjenistransaksinpoptkp' => 'required',
            's_tarifnpoptkp' => 'required',
            's_tarifnpoptkptambahan' => 'required',
            's_dasarhukumnpoptkp' => 'required',
            's_statusnpoptkp' => 'required',
            's_tglberlaku_awal' => 'required',
            's_tglberlaku_akhir' => 'required',
        ];
    }

    public function messages() {
        return [
            's_idjenistransaksinpoptkp.required' => 'Kolom harus diisi.',
            's_tarifnpoptkp.required' => 'Kolom harus diisi.',
            's_tarifnpoptkptambahan.required' => 'Kolom harus diisi.',
            's_dasarhukumnpoptkp.required' => 'Kolom harus diisi.',
            's_statusnpoptkp.required' => 'Kolom harus diisi.',
            's_tglberlaku_awal.required' => 'Kolom harus diisi.',
            's_tglberlaku_akhir.required' => 'Kolom harus diisi.',
        ];
    }

}
