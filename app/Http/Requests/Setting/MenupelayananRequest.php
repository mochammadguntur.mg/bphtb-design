<?php

namespace App\Http\Requests\Setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class MenupelayananRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_id_statusmenupelayanan' => 'required',
            //'s_ketmenupelayanan' => 'required',
        ];
    }

    public function messages(){
        return [
            's_id_statusmenupelayanan.required' => 'Kolom harus diisi.',
            //'s_ketmenupelayanan.required' => 'Kolom harus diisi.',
            
            
        ];
    }
}
