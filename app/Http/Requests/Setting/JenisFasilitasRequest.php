<?php

namespace App\Http\Requests\Setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class JenisFasilitasRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            's_kodejenisfasilitas' => [
                'required',
                Rule::unique('s_jenisfasilitas', 's_kodejenisfasilitas')->ignore($this->jenis_fasilitas)
            ],
            's_namajenisfasilitas' => 'required'
        ];
    }

    public function messages() {
        return [
            's_kodejenisfasilitas.required' => 'Kolom harus diisi.',
            's_kodejenisfasilitas.unique' => 'Maaf, Kode Jenis Fasilitas sudah digunakan!',
            's_namajenisfasilitas.required' => 'Kolom harus diisi.',
        ];
    }

}
