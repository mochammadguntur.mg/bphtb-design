<?php

namespace App\Http\Requests\Setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class MenucaptchaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_id_statusmenucaptcha' => 'required',
            //'s_ketmenucaptcha' => 'required',
        ];
    }

    public function messages(){
        return [
            's_id_statusmenucaptcha.required' => 'Kolom harus diisi.',
            //'s_ketmenucaptcha.required' => 'Kolom harus diisi.',
            
            
        ];
    }
}
