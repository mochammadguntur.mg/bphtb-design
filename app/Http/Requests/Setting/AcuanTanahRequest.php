<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class AcuanTanahRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            's_kd_propinsi' => ['required', 'digits:2'],
            's_kd_dati2' => ['required', 'digits:2'],
            's_kd_kecamatan' => ['required', 'digits:3'],
            's_kd_kelurahan' => ['required', 'digits:3'],
            's_kd_blok' => ['required', 'digits:3'],
            's_permetertanah' => 'required',
            'id_jenistanah' => 'required',
        ];
    }

    public function messages() {
        return[
            's_kd_propinsi.required' => 'Kolom harus diisi.',
            's_kd_dati2.required' => 'Kolom harus diisi.',
            's_kd_kecamatan.required' => 'Kolom harus diisi.',
            's_kd_kelurahan.required' => 'Kolom harus diisi.',
            's_kd_blok.required' => 'Kolom harus diisi.',
            'id_jenistanah.required' => 'Kolom harus diisi.',
            's_permetertanah.required' => 'Kolom harus diisi.',
        ];
    }

}
