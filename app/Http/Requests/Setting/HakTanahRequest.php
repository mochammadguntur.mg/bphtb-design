<?php

namespace App\Http\Requests\Setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class HakTanahRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_kodehaktanah' => [
                'required', 
                Rule::unique('s_jenishaktanah', 's_kodehaktanah')->ignore($this->hak_tanah)
            ],
            's_namahaktanah' => 'required',
        ];
    }

    public function messages(){
        return [
            's_kodehaktanah.required' => 'Kolom harus diisi.',
            's_kodehaktanah.unique' => 'Maaf, Kode Jenis Hak Tanah sudah digunakan!',
            's_namahaktanah.required' => 'Kolom harus diisi.',
        ];
    }
}
