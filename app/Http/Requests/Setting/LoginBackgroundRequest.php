<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class LoginBackgroundRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'thumbnail' => 'required|image|mimes:jpeg,jpg,png,svg|max:2048',
            'status' => 'required',
        ];
    }

    public function messages(){
        return [
            'thumbnail.required' => 'Kolom harus diisi.',
            'thumbnail.image' => 'Kolom harus berupa gambar.',
            'thumbnail.mimes' => 'Kolom harus berupa jpeg,jpg,png,svg.',
            'thumbnail.max' => 'Kolom Max 2 mb',
            'status.required' => 'Kolom harus diisi.',
        ];
    }
}
