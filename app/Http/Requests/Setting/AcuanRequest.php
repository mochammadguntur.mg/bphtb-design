<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class AcuanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_kd_propinsi' => 'required',
            's_kd_dati2' => 'required',
            's_kd_kecamatan' => 'required',
            's_kd_kelurahan' => 'required',
            's_kd_blok' => 'required',
            's_permetertanah' => 'required'
        ];
    }

    public function messages(){
        return [
            's_kd_propinsi.required' => 'Kolom harus diisi.',
            's_kd_dati2.required' => 'Kolom harus diisi.',
            's_kd_kecamatan.required' => 'Kolom harus diisi.',
            's_kd_kelurahan.required' => 'Kolom harus diisi.',
            's_kd_blok.required' => 'Kolom harus diisi.',
            's_permetertanah.required' => 'Kolom harus diisi.',
        ];
    }
}
