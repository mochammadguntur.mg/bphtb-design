<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class PemdaRequest extends FormRequest
{
    /**
     * Determine eif thee user is authorized to make this request.
     *e
     * @return boole
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *e
     * @return arraye
     */
    public function rules()
    {
        return [
            's_namaprov' => 'required',
            's_namakabkot' => 'required',
            's_namaibukotakabkot' => 'required',
            's_kodeprovinsi' => 'required',
            's_kodekabkot' => 'required',
            's_logo' => 'mimes:jpeg,jpg,png|max:1024',
            's_latitude_pemda' => 'required',
            's_longitude_pemda' => 'required',
        ];
    }

    public function messages()
    {
        return [
            's_namaprov.required' => 'Nama Provinsi tidak boleh kosong',
            's_namakabkot.required' => 'Nama Kabupaten/Kota tidak boleh kosong',
            's_namaibukotakabkot.required' => 'Kode Provinsi tidak boleh kosong',
            's_kodeprovinsi.required' => 'Kode Kabupaten/Kota tidak boleh kosong',
            's_namaibukotakabkot.required' => 'Nama Ibu Kota tidak boleh kosong',
            'mimes' => 'type file logo hanya boleh JPEG, JPG, & PNG',
            'uploaded' => 'Ukuran file tidak boleh lebih dari 1MB',
            's_latitude_pemda.required' => 'Latitude tidak boleh kosong',
            's_longitude_pemda.required' => 'Longitude tidak boleh kosong',
        ];
    }
}
