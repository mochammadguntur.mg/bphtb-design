<?php

namespace App\Http\Requests\Setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class NotarisRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_namanotaris' => 'required',
            's_alamatnotaris' => 'required',
            's_sknotaris' => 'required',
            's_kodenotaris' => [
                'required',
//                Rule::unique('s_notaris', 's_kodenotaris')->ignore($this->notaris)
            ],
            's_statusnotaris' => 'required',
        ];
    }

    public function messages(){
        return [
            's_kodenotaris.required' => 'Kolom harus diisi.',
//            's_kodenotaris.unique' => 'Maaf, Kode Notaris ini sudah digunakan!',
            's_namanotaris.required' => 'Kolom harus diisi.',
            's_alamatnotaris.required' => 'Kolom harus diisi.',
            's_sknotaris.required' => 'Kolom harus diisi.',
            's_statusnotaris.required' => 'Kolom harus diisi.',
        ];
    }
}
