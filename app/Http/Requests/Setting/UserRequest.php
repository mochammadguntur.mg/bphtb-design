<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string', 'max:255'],
            'username' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'password' => ['required', 'string', 'min:8'],
            's_tipe_pejabat' => ['required'],
            's_idpejabat_idnotaris' => ['required']
        ];
    }

    public function messages(){
        return [
            'name.required' => 'Kolom harus diisi.',
            'username.required' => 'Kolom harus diisi.',
            'email.required' => 'Kolom harus diisi.',
            'password.required' => 'Kolom harus diisi.',
            's_tipe_pejabat.required' => 'Kolom harus diisi.',
            's_idpejabat_idnotaris.required' => 'Kolom harus diisi.',
        ];
    }
}
