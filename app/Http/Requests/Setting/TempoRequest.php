<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class TempoRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            //
            's_haritempo' => 'required',
            's_tglberlaku_awal' => 'required',
            's_tglberlaku_akhir' => 'required',
            's_id_status' => 'required',
        ];
    }

    public function messages() {
        return[
            's_haritempo.required' => 'Kolom harus diisi.',
            's_tglberlaku_awal.required' => 'Kolom harus diisi.',
            's_tglberlaku_akhir.required' => 'Kolom harus diisi.',
            's_id_status.required' => 'Kolom harus diisi.',
        ];
    }

}
