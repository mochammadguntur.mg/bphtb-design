<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class BrivaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_id' => 'required',
            'secret_id' => 'required',
            'institution_code' => 'required',
            'briva_no' => 'required',
            'url_development' => 'required',
            'url_production' => 'required',
            'url_aktif' => 'required',
            'status' => 'required',
        ];
    }

    public function messages()
    {
        return [
            'client_id.required' => 'Client ID tidak boleh kosong',
            'secret_id.required' => 'Secret ID tidak boleh kosong',
            'institution_code.required' => 'Institution Code tidak boleh kosong',
            'briva_no.required' => 'Briva No tidak boleh kosong',
            'url_development.required' => 'URL Development tidak boleh kosong',
            'url_production.required' => 'URL Production tidak boleh kosong',
            'url_aktif.required' => 'Silahkan Pilih URL aktif',
            'status.required' => 'Silahkan Pilih Status',
        ];
    }
}
