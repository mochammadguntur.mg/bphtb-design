<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class TargetPenerimaanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_id_target_status' => 'required',
            's_tahun_target' => 'required|digits_between:1,4',
            's_nilai_target' => 'required|min:3|max:15',
        ];
    }

    public function messages(){
        return [
            's_id_target_status.required' => 'Kolom harus diisi.',
            's_tahun_target.required' => 'Kolom harus diisi.',
            's_nilai_target.required' => 'Kolom harus diisi.'
        ];
    }
}
