<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class PersyaratanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_idjenistransaksi' => 'required',
            's_namapersyaratan' => 'required',
            'order' => 'required',
            's_idwajib_up' => 'required',
        ];
    }

    public function messages()
    {
        return [
            's_idjenistransaksi.required' =>  'Kolom harus diisi.',
            's_namapersyaratan.required' =>  'Kolom harus diisi.',
            'order.required' =>  'Kolom harus diisi.',
            's_idwajib_up.required' =>  'Kolom harus diisi.',
        ];
    }
}
