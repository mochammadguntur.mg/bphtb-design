<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class KelurahanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_idkecamatan' => 'required',
            's_kd_kelurahan' => 'required',
            's_namakelurahan' => 'required',
            's_latitude' => 'required',
            's_longitude' => 'required'
        ];
    }

    public function messages(){
        return [
            's_idkecamatan.required' => 'Kolom harus diisi.',
            's_kd_kelurahan.required' => 'Kolom harus diisi.',
            's_namakelurahan.required' => 'Kolom harus diisi.',
            's_latitude.required' => 'Kolom harus diisi.',
            's_longitude.required' => 'Kolom harus diisi.'
        ];
    }
}
