<?php

namespace App\Http\Requests\Setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class JenisTransaksiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {
        return [
            's_kodejenistransaksi' => [
                'required',
                Rule::unique('s_jenistransaksi', 's_kodejenistransaksi')->ignore($this->jenis_transaksi)
            ],
            's_namajenistransaksi' => 'required',
        ];
    }

    public function messages(){
        return [
            's_kodejenistransaksi.required' => 'Kolom harus diisi.',
            's_kodejenistransaksi.unique' => 'Maaf, Kode Jenis Transaksi sudah digunakan!',
            's_namajenistransaksi.required' => 'Kolom harus diisi.',
        ];
    }
}
