<?php

namespace App\Http\Requests\Setting;

use Illuminate\Foundation\Http\FormRequest;

class TarifBphtbRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_tarif_bphtb' => 'required',
            's_dasar_hukum' => 'required',
            's_tgl_awal' => 'required',
            's_tgl_akhir' => 'required',
        ];
    }

    public function messages(){
        return [
            's_tarif_bphtb.required' => 'Kolom harus diisi.',
            's_dasar_hukum.required' => 'Kolom harus diisi.',
            's_tgl_awal.required' => 'Kolom harus diisi.',
            's_tgl_akhir.required' => 'Kolom harus diisi.',
        ];
    }
}
