<?php

namespace App\Http\Requests\Setting;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class MenukonekpbbRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            's_id_statuskonekpbb' => 'required',
            //'s_ketkonekpbb' => 'required',
        ];
    }

    public function messages(){
        return [
            's_id_statuskonekpbb.required' => 'Kolom harus diisi.',
            //'s_ketkonekpbb.required' => 'Kolom harus diisi.',
            
            
        ];
    }
}
