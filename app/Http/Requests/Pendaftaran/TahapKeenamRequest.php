<?php

namespace App\Http\Requests\Pendaftaran;

use Illuminate\Foundation\Http\FormRequest;

class TahapKeenamRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules() {
        if ($this->input('t_idbidang_penjual') == 1) {
            return [
                't_idbidang_penjual' => ['required'],
                't_nik_penjual' => ['required'],
                't_nama_penjual' => ['required'],
                't_npwp_penjual' => ['required'],
                't_jalan_penjual' => ['required'],
                't_rt_penjual' => ['required'],
                't_rw_penjual' => ['required'],
                't_idkec_penjual' => ['required'],
                't_idkel_penjual' => ['required'],
                't_namakec_penjual' => ['required'],
                't_namakel_penjual' => ['required'],
                't_kabkota_penjual' => ['required'],
                't_nohp_penjual' => ['required'],
                't_notelp_penjual' => ['required'],
                't_email_penjual' => ['required'],
                't_kodepos_penjual' => ['required'],
            ];
        } else {
            return [
                't_idbidang_penjual' => ['required'],
                't_siup_penjual' => ['required'],
                't_ketdomisili_penjual' => ['required'],
                't_nama_penjual' => ['required'],
                't_npwp_penjual' => ['required'],
                't_jalan_penjual' => ['required'],
                't_rt_penjual' => ['required'],
                't_rw_penjual' => ['required'],
                't_idkec_penjual' => ['required'],
                't_idkel_penjual' => ['required'],
                't_namakec_penjual' => ['required'],
                't_namakel_penjual' => ['required'],
                't_kabkota_penjual' => ['required'],
                't_nohp_penjual' => ['required'],
                't_notelp_penjual' => ['required'],
                't_email_penjual' => ['required'],
                't_kodepos_penjual' => ['required'],
                't_b_nik_pngjwb_penjual' => ['required'],
                't_b_nama_pngjwb_penjual' => ['required'],
                't_b_npwp_pngjwb_penjual' => ['required'],
                't_b_statusjab_pngjwb_penjual' => ['required'],
                't_b_jalan_pngjwb_penjual' => ['required'],
                't_b_rt_pngjwb_penjual' => ['required'],
                't_b_rw_pngjwb_penjual' => ['required'],
                't_b_idkec_pngjwb_penjual' => ['required'],
                't_b_namakec_pngjwb_penjual' => ['required'],
                't_b_idkel_pngjwb_penjual' => ['required'],
                't_b_namakel_pngjwb_penjual' => ['required'],
                't_b_kabkota_pngjwb_penjual' => ['required'],
                't_b_nohp_pngjwb_penjual' => ['required'],
                't_b_notelp_pngjwb_penjual' => ['required'],
                't_b_email_pngjwb_penjual' => ['required'],
                't_b_kodepos_pngjwb_penjual' => ['required'],
            ];
        }
    }
    
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        if ($this->input('t_idbidang_penjual') == 1) {
            return [
                't_idbidang_penjual.required' => 'Kolom harus diisi.',
                't_nik_penjual.required' => 'Kolom harus diisi.',
                't_nama_penjual.required' => 'Kolom harus diisi.',
                't_npwp_penjual.required' => 'Kolom harus diisi.',
                't_jalan_penjual.required' => 'Kolom harus diisi.',
                't_rt_penjual.required' => 'Kolom harus diisi.',
                't_rw_penjual.required' => 'Kolom harus diisi.',
                't_idkec_penjual.required' => 'Kolom harus diisi.',
                't_idkel_penjual.required' => 'Kolom harus diisi.',
                't_namakec_penjual.required' => 'Kolom harus diisi.',
                't_namakel_penjual.required' => 'Kolom harus diisi.',
                't_kabkota_penjual.required' => 'Kolom harus diisi.',
                't_nohp_penjual.required' => 'Kolom harus diisi.',
                't_notelp_penjual.required' => 'Kolom harus diisi.',
                't_email_penjual.required' => 'Kolom harus diisi.',
                't_kodepos_penjual.required' => 'Kolom harus diisi.',
            ];
        } else {
            return [
                't_idbidang_penjual.required' => 'Kolom harus diisi.',
                't_siup_penjual.required' => 'Kolom harus diisi.',
                't_ketdomisili_penjual.required' => 'Kolom harus diisi.',
                't_nama_penjual.required' => 'Kolom harus diisi.',
                't_npwp_penjual.required' => 'Kolom harus diisi.',
                't_jalan_penjual.required' => 'Kolom harus diisi.',
                't_rt_penjual.required' => 'Kolom harus diisi.',
                't_rw_penjual.required' => 'Kolom harus diisi.',
                't_idkec_penjual.required' => 'Kolom harus diisi.',
                't_idkel_penjual.required' => 'Kolom harus diisi.',
                't_namakec_penjual.required' => 'Kolom harus diisi.',
                't_namakel_penjual.required' => 'Kolom harus diisi.',
                't_kabkota_penjual.required' => 'Kolom harus diisi.',
                't_nohp_penjual.required' => 'Kolom harus diisi.',
                't_notelp_penjual.required' => 'Kolom harus diisi.',
                't_email_penjual.required' => 'Kolom harus diisi.',
                't_kodepos_penjual.required' => 'Kolom harus diisi.',
                't_b_nama_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_nik_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_npwp_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_statusjab_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_jalan_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_kabkota_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_idkec_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_namakec_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_idkel_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_namakel_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_rt_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_rw_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_nohp_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_notelp_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_email_pngjwb_penjual.required' => 'Kolom harus diisi.',
                't_b_kodepos_pngjwb_penjual.required' => 'Kolom harus diisi.',
            ];
        }
    }
}
