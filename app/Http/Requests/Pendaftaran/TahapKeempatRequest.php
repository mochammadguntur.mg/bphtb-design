<?php

namespace App\Http\Requests\Pendaftaran;

use Illuminate\Foundation\Http\FormRequest;

class TahapKeempatRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function rules() {
        return [
            's_latitude' => ['required'],
            's_longitude' => ['required'],
        ];
    }

    public function messages() {
        return [
            's_latitude.required' => 'Kolom harus diisi.',
            's_longitude.required' => 'Kolom harus diisi.'
        ];
    }
}
