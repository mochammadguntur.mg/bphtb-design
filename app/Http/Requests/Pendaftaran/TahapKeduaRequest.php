<?php

namespace App\Http\Requests\Pendaftaran;

use Illuminate\Foundation\Http\FormRequest;

class TahapKeduaRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            't_nop_sppt' => ['required'],
            't_tahun_sppt' => ['required'],
            't_nama_sppt' => ['required'],
            't_jalan_sppt' => ['required'],
            't_kabkota_sppt' => ['required'],
            't_kecamatan_sppt' => ['required'],
            't_kelurahan_sppt' => ['required'],
            't_rt_sppt' => ['required'],
            't_rw_sppt' => ['required'],
            't_luastanah' => ['required'],
            't_njoptanah' => ['required'],
            't_totalnjoptanah' => ['required'],
            't_luasbangunan' => ['required'],
            't_njopbangunan' => ['required'],
            't_totalnjopbangunan' => ['required'],
            't_grandtotalnjop' => ['required'],
            't_nilaitransaksispt' => ['required'],
            't_idjenishaktanah' => ['required'],
            't_idjenisdoktanah' => ['required'],
            't_id_jenistanah' => ['required'],
            't_nosertifikathaktanah' => ['required'],
            't_tgldok_tanah' => ['required'],
        ];
    }

    public function messages() {
        return [
            't_nop_sppt.required' => 'Kolom harus diisi.',
            't_tahun_sppt.required' => 'Kolom harus diisi.',
            't_nama_sppt.required' => 'Kolom harus diisi.',
            't_jalan_sppt.required' => 'Kolom harus diisi.',
            't_kabkota_sppt.required' => 'Kolom harus diisi.',
            't_kecamatan_sppt.required' => 'Kolom harus diisi.',
            't_kelurahan_sppt.required' => 'Kolom harus diisi.',
            't_rt_sppt.required' => 'Kolom harus diisi.',
            't_rw_sppt.required' => 'Kolom harus diisi.',
            't_luastanah.required' => 'Kolom harus diisi.',
            't_njoptanah.required' => 'Kolom harus diisi.',
            't_totalnjoptanah.required' => 'Kolom harus diisi.',
            't_luasbangunan.required' => 'Kolom harus diisi.',
            't_njopbangunan.required' => 'Kolom harus diisi.',
            't_totalnjopbangunan.required' => 'Kolom harus diisi.',
            't_grandtotalnjop.required' => 'Kolom harus diisi.',
            't_nilaitransaksispt.required' => 'Kolom harus diisi.',
            't_idjenishaktanah.required' => 'Kolom harus diisi.',
            't_idjenisdoktanah.required' => 'Kolom harus diisi.',
            't_id_jenistanah.required' => 'Kolom harus diisi.',
            't_nosertifikathaktanah.required' => 'Kolom harus diisi.',
            't_tgldok_tanah.required' => 'Kolom harus diisi.',
        ];
    }

}
