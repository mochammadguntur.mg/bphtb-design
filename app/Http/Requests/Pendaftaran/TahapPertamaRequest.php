<?php

namespace App\Http\Requests\Pendaftaran;

use Illuminate\Foundation\Http\FormRequest;

class TahapPertamaRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        if ($this->input('t_idbidang_usaha') == 1) {
            return [
                't_tgldaftar_spt' => ['required'],
                't_idjenistransaksi' => ['required'],
                't_idnotaris_spt' => ['required'],
                't_idbidang_usaha' => ['required'],
                't_nik_pembeli' => ['required'],
                't_nama_pembeli' => ['required'],
                't_npwp_pembeli' => ['required'],
                't_jalan_pembeli' => ['required'],
                't_rt_pembeli' => ['required'],
                't_rw_pembeli' => ['required'],
                't_idkec_pembeli' => ['required'],
                't_idkel_pembeli' => ['required'],
                't_namakecamatan_pembeli' => ['required'],
                't_namakelurahan_pembeli' => ['required'],
                't_kabkota_pembeli' => ['required'],
                't_nohp_pembeli' => ['required'],
                't_notelp_pembeli' => ['required'],
                't_email_pembeli' => ['required'],
                't_kodepos_pembeli' => ['required'],
            ];
        } else {
            return [
                't_idjenistransaksi' => ['required'],
                't_idnotaris_spt' => ['required'],
                't_idbidang_usaha' => ['required'],
                't_siup_pembeli' => ['required'],
                't_ketdomisili_pembeli' => ['required'],
                't_nama_pembeli' => ['required'],
                't_npwp_pembeli' => ['required'],
                't_jalan_pembeli' => ['required'],
                't_rt_pembeli' => ['required'],
                't_rw_pembeli' => ['required'],
                't_idkec_pembeli' => ['required'],
                't_idkel_pembeli' => ['required'],
                't_namakecamatan_pembeli' => ['required'],
                't_namakelurahan_pembeli' => ['required'],
                't_kabkota_pembeli' => ['required'],
                't_nohp_pembeli' => ['required'],
                't_notelp_pembeli' => ['required'],
                't_email_pembeli' => ['required'],
                't_kodepos_pembeli' => ['required'],
                't_b_nama_pngjwb_pembeli' => ['required'],
                't_b_nik_pngjwb_pembeli' => ['required'],
                't_b_npwp_pngjwb_pembeli' => ['required'],
                't_b_statusjab_pngjwb_pembeli' => ['required'],
                't_b_jalan_pngjwb_pembeli' => ['required'],
                't_b_kabkota_pngjwb_pembeli' => ['required'],
                't_b_idkec_pngjwb_pembeli' => ['required'],
                't_b_namakec_pngjwb_pembeli' => ['required'],
                't_b_idkel_pngjwb_pembeli' => ['required'],
                't_b_namakel_pngjwb_pembeli' => ['required'],
                't_b_rt_pngjwb_pembeli' => ['required'],
                't_b_rw_pngjwb_pembeli' => ['required'],
                't_b_nohp_pngjwb_pembeli' => ['required'],
                't_b_notelp_pngjwb_pembeli' => ['required'],
                't_b_email_pngjwb_pembeli' => ['required'],
                't_b_kodepos_pngjwb_pembeli' => ['required'],
            ];
        }
    }
    
    /**
     * Custom message for validation
     *
     * @return array
     */
    public function messages()
    {
        if ($this->input('t_idbidang_usaha') == 1) {
            return [
                't_idjenistransaksi.required' => 'Kolom harus diisi.',
                't_idnotaris_spt.required' => 'Kolom harus diisi.',
                't_idbidang_usaha.required' => 'Kolom harus diisi.',
                't_nik_pembeli.required' => 'Kolom harus diisi.',
                't_nama_pembeli.required' => 'Kolom harus diisi.',
                't_npwp_pembeli.required' => 'Kolom harus diisi.',
                't_jalan_pembeli.required' => 'Kolom harus diisi.',
                't_rt_pembeli.required' => 'Kolom harus diisi.',
                't_rw_pembeli.required' => 'Kolom harus diisi.',
                't_idkec_pembeli.required' => 'Kolom harus diisi.',
                't_idkel_pembeli.required' => 'Kolom harus diisi.',
                't_namakecamatan_pembeli.required' => 'Kolom harus diisi.',
                't_namakelurahan_pembeli.required' => 'Kolom harus diisi.',
                't_kabkota_pembeli.required' => 'Kolom harus diisi.',
                't_nohp_pembeli.required' => 'Kolom harus diisi.',
                't_notelp_pembeli.required' => 'Kolom harus diisi.',
                't_email_pembeli.required' => 'Kolom harus diisi.',
                't_kodepos_pembeli.required' => 'Kolom harus diisi.',
            ];
        } else {
            return [
                't_idjenistransaksi.required' => 'Kolom harus diisi.',
                't_idnotaris_spt.required' => 'Kolom harus diisi.',
                't_idbidang_usaha.required' => 'Kolom harus diisi.',
                't_siup_pembeli.required' => 'Kolom harus diisi.',
                't_ketdomisili_pembeli.required' => 'Kolom harus diisi.',
                't_nama_pembeli.required' => 'Kolom harus diisi.',
                't_npwp_pembeli.required' => 'Kolom harus diisi.',
                't_jalan_pembeli.required' => 'Kolom harus diisi.',
                't_rt_pembeli.required' => 'Kolom harus diisi.',
                't_rw_pembeli.required' => 'Kolom harus diisi.',
                't_idkec_pembeli.required' => 'Kolom harus diisi.',
                't_idkel_pembeli.required' => 'Kolom harus diisi.',
                't_namakecamatan_pembeli.required' => 'Kolom harus diisi.',
                't_namakelurahan_pembeli.required' => 'Kolom harus diisi.',
                't_kabkota_pembeli.required' => 'Kolom harus diisi.',
                't_nohp_pembeli.required' => 'Kolom harus diisi.',
                't_notelp_pembeli.required' => 'Kolom harus diisi.',
                't_email_pembeli.required' => 'Kolom harus diisi.',
                't_kodepos_pembeli.required' => 'Kolom harus diisi.',
                't_b_nama_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_nik_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_npwp_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_statusjab_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_jalan_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_kabkota_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_idkec_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_namakec_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_idkel_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_namakel_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_rt_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_rw_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_nohp_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_notelp_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_email_pngjwb_pembeli.required' => 'Kolom harus diisi.',
                't_b_kodepos_pngjwb_pembeli.required' => 'Kolom harus diisi.',
            ];
        }
    }

}
