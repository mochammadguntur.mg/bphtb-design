<?php

namespace App\Http\Requests\Pelayanan;

use Illuminate\Foundation\Http\FormRequest;

class PelayananPembatalanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            't_tglpengajuan' => 'required',
            't_kohirspt' => 'required',
            't_nama_pembeli' => 'required',
            't_keterangan_pembatalan' => 'required',
        ];
    }

    public function messages(){
        return [
            't_tglpengajuan.required' => 'Field Tgl Pengajuan Tidak Boleh Kosong!',
            't_kohirspt.required' => 'No Kohir Tidak Boleh Kosong',
            't_p_nama_pembeli.required' => 'Nama Wp Pembeli Tidak Boleh Kosong',
            't_keterangan_pembatalan.required' => 'Field Keterangan Permohonan Tidak Boleh Kosong!',
        ];
    }
}