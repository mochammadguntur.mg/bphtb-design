<?php

namespace App\Http\Requests\Pelayanan;

use Illuminate\Foundation\Http\FormRequest;

class PelayananMutasiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            't_tglpengajuan' => 'required',
            't_kohirspt' => 'required',
            't_nama_pembeli' => 'required',
            't_keterangan_permohonan' => 'required',
        ];
    }
}