<?php

namespace App\Http\Requests\Sismiop;

use Illuminate\Foundation\Http\FormRequest;

class DatSubjekPajakRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'SUBJEK_PAJAK_ID' => 'required',
            'NM_WP' => 'required',
            'JALAN_WP' => 'required',
            'BLOK_KAV_NO_WP' => 'required',
            'RT_WP' => 'required',
            'RW_WP' => 'required',
            'KELURAHAN_WP' => 'required',
            'KOTA_WP' => 'required',
            'KD_POS_WP' => 'required',
            'TELP_WP' => 'required',
            'NPWP' => 'required',
            'STATUS_PEKERJAAN_WP' => 'required'
        ];
    }
}
