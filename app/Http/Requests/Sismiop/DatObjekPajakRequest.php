<?php

namespace App\Http\Requests\Sismiop;

use Illuminate\Foundation\Http\FormRequest;

class DatObjekPajakRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'KD_PROPINSI' => 'required',
            'KD_DATI2' => 'required',
            'KD_KECAMATAN' => 'required',
            'KD_KELURAHAN' => 'required',
            'KD_BLOK' => 'required', 
            'NO_URUT' => 'required', 
            'KD_JNS_OP' => 'required',
            'SUBJEK_PAJAK_ID' => 'required', 
            'NO_FORMULIR_SPOP' => 'required', 
            'NO_PERSIL' => 'required', 
            'JALAN_OP' => 'required', 
            'BLOK_KAV_NO_OP' => 'required', 
            'RW_OP' => 'required', 
            'RT_OP' => 'required',
            'KD_STATUS_CABANG' => 'required', 
            'KD_STATUS_WP' => 'required', 
            'TOTAL_LUAS_BUMI' => 'required', 
            'TOTAL_LUAS_BNG' => 'required', 
            'NJOP_BUMI' => 'required', 
            'NJOP_BNG' => 'required',
            'STATUS_PETA_OP' => 'required', 
            'JNS_TRANSAKSI_OP' => 'required'
        ];
    }
}
