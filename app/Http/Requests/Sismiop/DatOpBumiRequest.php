<?php

namespace App\Http\Requests\Sismiop;

use Illuminate\Foundation\Http\FormRequest;

class DatOpBumiRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'KD_PROPINSI' => 'required', 
            'KD_DATI2' => 'required', 
            'KD_KECAMATAN' => 'required', 
            'KD_KELURAHAN' => 'required', 
            'KD_BLOK' => 'required', 
            'NO_URUT' => 'required', 
            'KD_JNS_OP' => 'required',
            'NO_BUMI' => 'required', 
            'KD_ZNT' => 'required', 
            'LUAS_BUMI' => 'required', 
            'JNS_BUMI' => 'required', 
            'NILAI_SISTEM_BUMI' => 'required'
        ];
    }
}
