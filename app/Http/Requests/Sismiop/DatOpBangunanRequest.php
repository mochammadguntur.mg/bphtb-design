<?php

namespace App\Http\Requests\Sismiop;

use Illuminate\Foundation\Http\FormRequest;

class DatOpBangunanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'KD_PROPINSI' => 'required', 
            'KD_DATI2' => 'required', 
            'KD_KECAMATAN' => 'required', 
            'KD_KELURAHAN' => 'required', 
            'KD_BLOK' => 'required', 
            'NO_URUT' => 'required', 
            'KD_JNS_OP' => 'required',
            'NO_BNG' => 'required', 
            'KD_JPB' => 'required', 
            'NO_FORMULIR_LSPOP' => 'required', 
            'THN_DIBANGUN_BNG' => 'required', 
            'THN_RENOVASI_BNG' => 'required', 
            'LUAS_BNG' => 'required',
            'JML_LANTAI_BNG' => 'required', 
            'KONDISI_BNG' => 'required', 
            'JNS_KONSTRUKSI_BNG' => 'required', 
            'JNS_ATAP_BNG' => 'required', 
            'KD_DINDING' => 'required', 
            'KD_LANTAI' => 'required',
            'KD_LANGIT_LANGIT' => 'required', 
            'NILAI_SISTEM_BNG' => 'required', 
            'JNS_TRANSAKSI_BNG' => 'required', 
            'TGL_PENDATAAN_BNG' => 'required', 
            'NIP_PENDATA_BNG' => 'required',
            'TGL_PEMERIKSAAN_BNG' => 'required', 
            'NIP_PEMERIKSA_BNG' => 'required', 
            'TGL_PEREKAMAN_BNG' => 'required', 
            'NIP_PEREKAM_BNG' => 'required'
        ];
    }
}
