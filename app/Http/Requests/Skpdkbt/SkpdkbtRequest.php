<?php

namespace App\Http\Requests\Skpdkbt;

use Illuminate\Foundation\Http\FormRequest;

class SkpdkbtRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            't_luastanah' => 'required',
            't_luasbangunan' => 'required',
            't_nilaitransaksispt' => 'required'
        ];
    }
}