<?php

namespace App\Http\Requests\Approved;

use Illuminate\Foundation\Http\FormRequest;

class ApprovedKeringananRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            't_idkeringanan' => 'required',
            't_tglpersetujuan' => 'required',
            't_nosk_keringanan' => 'required',
            't_jmlhpotongan_disetujui' => 'required',
            't_persentase_disetujui' => 'required',
            't_jmlh_spt_sebenarnya' => 'required',
            't_jmlh_spt_hasilpot' => 'required',
            't_keterangan_disetujui' => 'required',
            't_idstatus_disetujui' => 'required'
        ];
    }
}