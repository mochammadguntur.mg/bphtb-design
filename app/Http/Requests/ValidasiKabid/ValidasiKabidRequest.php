<?php

namespace App\Http\Requests\ValidasiKabid;

use Illuminate\Foundation\Http\FormRequest;

class ValidasiKabidRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        if ($this->input('s_id_status_kabid') == 2) {
            return [
                't_keterangan_kabid' => 'required'
            ];
        }else{
            return [];
        }
    }

    public function messages() {
        if ($this->input('s_id_status_kabid') == 2) {
            return [
                't_keterangan_kabid.required' => 'Kolom harus diisi.'
            ];
        }else{
            return [];
        }
    }

}
