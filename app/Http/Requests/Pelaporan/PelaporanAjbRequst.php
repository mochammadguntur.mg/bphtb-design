<?php

namespace App\Http\Requests\Pelaporan;

use App\Models\Pelaporan\InputAjb;
use Illuminate\Foundation\Http\FormRequest;

class PelaporanAjbRequst extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $ajb = new InputAjb();
        $listAjb = $ajb->from('t_inputajb as a')
                        ->leftJoin('t_spt as b', 'b.t_idspt', '=', 'a.t_idspt')
                        ->where('b.t_idnotaris_spt', $this->request->get('t_idnotaris_spt'))
                        ->where('a.t_no_ajb', $this->request->get('t_no_ajb'))
                        ->select('t_no_ajb', 'b.t_idnotaris_spt')->first();

        return [
            't_idspt' => 'required',
            't_tgl_ajb' => 'required|date',
            't_no_ajb' => ['required', function ($attribute, $val, $fail) use ($listAjb) {
                    if(!empty($listAjb)) {
                        if ($val === $listAjb['t_no_ajb']) {
                            $fail('No Akta sudah digunakan');
                        }
                    }
                }],
        ];
    }

    public function messages() {
        return [
            't_idspt.required' => 'Pilih/isi dahulu No Pendaftaranya',
            't_tgl_ajb.required' => 'Tanggal Akta tidak boleh kosong',
            't_tgl_ajb.date' => 'Pastikan format Tanggal Akta anda dengan benar',
            't_no_ajb.required' => 'No Akta tidak boleh kosong'
        ];
    }

}
