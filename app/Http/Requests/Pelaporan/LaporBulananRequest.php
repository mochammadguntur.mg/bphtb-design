<?php

namespace App\Http\Requests\Pelaporan;

use Illuminate\Foundation\Http\FormRequest;

class LaporBulananRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            't_untuk_bulan' => 'required',
            't_untuk_tahun' => 'required',
            't_no_ajb' => 'required',
            't_keterangan' => 'required'
        ];
    }

    public function messages(){
        return [
            't_untuk_bulan.required' => 'Bulan Lapor tidak boleh kosong',
            't_untuk_tahun.required' => 'Tahun Lapon tidak boleh kosong',
            't_no_ajb.required' => 'Harus pilih minimal satu data',
            't_keterangan.required' => 'Keterangan tidak boleh kosong'
        ];
    }
}
