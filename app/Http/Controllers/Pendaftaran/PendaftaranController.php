<?php

namespace App\Http\Controllers\Pendaftaran;

use App\Http\Controllers\Controller;
use App\Models\Spt\Spt;
use App\Models\Spt\Filesyarat;
use App\Models\Spt\Filefotoobjek;
use App\Models\ValidasiBerkas\ValidasiBerkas;
use App\Models\ValidasiKabid\ValidasiKabid;
use App\Models\Pembayaran\PembayaranBphtb;
use Illuminate\Http\Request;
use App\Models\Setting\Pemda;
use Illuminate\Support\Facades\DB;
use App\Models\Pbb\Sppt;
use App\Models\Pendaftaran\Pendaftaran;
use Illuminate\Support\Facades\Auth;
use Illuminate\Auth\Events\Registered;
use MPDF;
use App\Http\Requests\Pendaftaran\TahapPertamaRequest;
use App\Models\Pendaftaran\NikBersama;
use App\Http\Requests\Pendaftaran\TahapKeduaRequest;
use App\Http\Requests\Pendaftaran\TahapKeempatRequest;
use App\Http\Requests\Pendaftaran\TahapKeenamRequest;
use App\Models\Setting\Kecamatan;
use App\Models\Setting\Kelurahan;
use Ramsey\Uuid\Uuid;

class PendaftaranController extends Controller {

    public function index() {
        $view = view('pendaftaran.index');
        return $view;
    }

    public function formtambah() {
        return view('pendaftaran.formtambah', [
            'dataspt' => new Spt(),
            'data_notaris' => (new Spt())->cekdata_notaris(),
            'data_jenistransaksi' => (new Spt())->cekdata_jenis_transaksi(),
            'data_jenisbidangusaha' => (new Spt())->cekdata_jenis_bidangusaha(),
            'data_kecamatan' => (new Spt())->cekdata_kecamatan(),
            'all_session' => Auth::user()
        ]);
    }

    public function datagrid(Request $request) {
        $all_session = Auth::user();
        $response = (new Pendaftaran())->datagridPendaftaran($request, $all_session);
        $dataArr = [];
        foreach ($response as $v) {
            $persyaratan = \App\Models\Setting\Persyaratan::where('s_idjenistransaksi', '=', $v['t_idjenistransaksi'])->get();
            $file = [];
            foreach ($persyaratan as $upload) {
                $cekUploadFile = DB::table('t_filesyarat_bphtb')->where([
                            ['t_idspt', '=', $v['t_idspt']],
                            ['s_idpersyaratan', '=', $upload['s_idpersyaratan']]
                        ])->count();
                $file[] = $cekUploadFile;
            }

            if (empty($v['t_nop_sppt'])) {
                $edit = '<a href="pendaftaran/tahapkedua/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
            } elseif (in_array(0, $file)) {
                $edit = '<a href="pendaftaran/tahapketiga/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
            } elseif (empty($v['s_latitude'])) {
                $edit = '<a href="pendaftaran/tahapkeempat/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
            } elseif (!empty($v['s_latitude']) && empty($v['t_nama_penjual'])) {
                $edit = '<a href="pendaftaran/tahapkelima/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
            } elseif (empty($v['t_nama_penjual'])) {
                $edit = '<a href="pendaftaran/tahapkeenam/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
            } elseif (empty($v['t_idpersetujuan_bphtb'])) {
                $edit = '<a href="pendaftaran/tahapketujuh/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
            } else {
                $edit = '<a href="pendaftaran/tahappertama/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
            }

            if (!empty($v['s_id_status_berkas'])) {
                if ($v['s_id_status_berkas'] == 1) {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                    $edit = '';
                } else {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button><small class="text-warning">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                }
            } else {
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            $cetak_sspd = '';
            if (!empty($v['s_id_status_kabid'])) {
                if ($v['s_id_status_kabid'] == 1) {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SETUJU</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                    //$cetak_sspd = '<a href="pendaftaran/' . $v['t_idspt'] . '/cetaksspdbphtb" target="_blank" class="btn btn-primary btn-block btn-xs" title="CETAK SSPD"><i class="fa fa-fw fa-print"></i> CETAK SSPD</a>';
                } else {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK SETUJU</button><small class="text-warning">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                }
            } else {
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }
            
            
            if(!empty($v['t_kodebayar_bphtb'])){
                $cetak_sspd = '<a href="pendaftaran/' . $v['t_idspt_ori'] . '/cetaksspdbphtb/'.$v['t_idjenisketetapan'].'" target="_blank" class="btn btn-primary btn-block btn-xs" title="CETAK SSPD"><i class="fa fa-fw fa-print"></i> CETAK SSPD</a>';
            }else{
                $cetak_sspd = '';
            }
            

            $status_bayar = !empty($v['t_tglpembayaran_pokok']) ? '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> SUDAH</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglpembayaran_pokok'])).'</small>' : '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';
            $status_persetujuan = !empty($v['t_idpersetujuan_bphtb']) ? '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])).'</small>' : '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>';
            $bahp = !empty($v['t_idpemeriksa']) ? '<a href="pendaftaran/' . $v['t_idspt'] . '/cetakbahpbphtb" target="_blank" class="btn btn-success btn-block btn-xs" title="CETAK BAHP"><i class="fa fa-fw fa-print"></i> CETAK BAHP</a>' : '';
            $lihat_data = '<a href="/lihatdata/' . $v['t_uuidspt'] . '" class="btn btn-block bg-gradient-info btn-sm" title="Lihat Data"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>';
            $suratPernyataan = '<a href="pendaftaran/' . $v['t_idspt'] . '/cetaksuratpernyataan" target="_blank" class="btn btn-primary btn-block btn-xs" title="CETAK SURAT PERNYATAAN"><i class="fa fa-fw fa-print"></i> CETAK SURAT PERNYATAAN</a>';
            $batal_permohonan = ($all_session['s_id_hakakses'] == 1) ? '<a onclick="javascript:showBatalDialog(' . $v['t_idspt'] . ')" class="btn btn-block bg-danger btn-sm" title="Batal Pengajuan"><i class="fa fa-fw fa-close"></i> Batal Pengajuan</a>' : '';
            $hapus_permanen = ($all_session['s_id_hakakses'] == 1) ? '<a onclick="javascript:showDeleteDialog(' . $v['t_idspt'] . ')" class="btn btn-block bg-danger btn-sm" title="Hapus Permanen"><i class="fa fa-fw fa-trash"></i> Hapus Permanen</a>' : '';
            $perintah = '<div class="dropdown">
                            <button onclick="myFunction(' . $v['t_idspt'] . ')" class="dropbtn btn-primary dropdown-toggle btn btn-xs">PERINTAH <span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                            <div id="myDropdown' . $v['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:80px; border-color: blue;">
                                    ' . $lihat_data . '
                                    ' . $edit . '
                                    ' . $cetak_sspd . '
                                    ' . $bahp . '
                                    
                            </div>
                        </div>';
            //' . $batal_permohonan . '
            //' . $hapus_permanen . '
            $dataArr[] = [
                't_idspt' => $perintah,
                't_kohirspt' => $v['t_kohirspt'],
                't_tgldaftar_spt' => date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])),
                't_periodespt' => $v['t_periodespt'],
                't_idnotaris_spt' => $v['s_namanotaris'],
                't_idjenistransaksi' => $v['s_namajenistransaksi'],
                't_nama_pembeli' => $v['t_nama_pembeli'],
                't_nop_sppt' => !empty($v['t_nop_sppt']) ? $v['t_nop_sppt'] : '',
                't_nilai_bphtb_fix' => number_format($v['t_nilai_bphtb_fix'], 0, ',', '.'),
                't_idpersetujuan_bphtb' => $status_persetujuan,
                't_id_validasi_berkas' => $status_validasi_berkas,
                't_id_validasi_kabid' => $status_validasi_kabid,
                't_id_pembayaran' => $status_bayar,
                't_kodebayar_bphtb' => !empty($v['t_kodebayar_bphtb']) ? $v['t_kodebayar_bphtb'] : '',
                'ntpd' => !empty($v['t_ntpd']) ? $v['t_ntpd'] : '',
                't_idjenisketetapan' => '<center><b>'.$v['s_namasingkatjenisketetapan'].'</b></center>',
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function cekdatakelurahan(Request $Request) {
        $attr = $Request->all();
        $nama_kelurahan = "";
        if (!empty($attr['s_idkecamatan'])) {
            $cek_data_kecamatan = (new Spt())->cek_id_kecamatan($attr['s_idkecamatan']);
            $cek_data_kelurahan = (new Spt())->cek_id_datakelurahan($attr['s_idkecamatan']);
            $opsi = "";
            $opsi .= '<option value="">Silahkan Pilih</option>';
            foreach ($cek_data_kelurahan as $r) {
                if (!empty($attr['s_idkelurahan'])) {
                    if ($attr['s_idkelurahan'] == $r->s_idkelurahan) {
                        $selected = 'selected';
                        $nama_kelurahan = $r->s_namakelurahan;
                    } else {
                        $selected = '';
                    }
                } else {
                    $selected = '';
                }
                $opsi .= '<option value=" ' . $r->s_idkelurahan . ' " ' . $selected . '>' . str_pad($r->s_kd_kecamatan, 3, '0', STR_PAD_LEFT) . '.' . str_pad($r->s_kd_kelurahan, 3, '0', STR_PAD_LEFT) . ' - ' . $r->s_namakelurahan . '</option>';
            }
        } else {
            $opsi = '<option value="">Silahkan Pilih</option>';
        }
        if (!empty(@$attr['t_idspt'])) {
            $cek_data_spt = (new Spt())->cek_id_spt($attr['t_idspt']);
            if ($cek_data_kecamatan->s_kd_kecamatan == '000') {
                if (@$attr['daritahap'] == 1) {
                    if (@$attr['darimana'] == 1) {
                        $nama_kecamatannya = $cek_data_spt->t_namakecamatan_pembeli;
                        $nama_kelurahannya = $cek_data_spt->t_namakelurahan_pembeli;
                    } elseif (@$attr['darimana'] == 1) {
                        $nama_kecamatannya = $cek_data_spt->t_b_namakec_pngjwb_pembeli;
                        $nama_kelurahannya = $cek_data_spt->t_b_namakel_pngjwb_pembeli;
                    } else {
                        $nama_kecamatannya = '';
                        $nama_kelurahannya = '';
                    }
                } elseif (@$attr['daritahap'] == 2) {
                    if (@$attr['darimana'] == 1) {
                        $nama_kecamatannya = $cek_data_spt->t_namakec_penjual;
                        $nama_kelurahannya = $cek_data_spt->t_namakel_penjual;
                    } elseif (@$attr['darimana'] == 1) {
                        $nama_kecamatannya = $cek_data_spt->t_b_namakec_pngjwb_penjual;
                        $nama_kelurahannya = $cek_data_spt->t_b_namakel_pngjwb_penjual;
                    } else {
                        $nama_kecamatannya = '';
                        $nama_kelurahannya = '';
                    }
                } else {
                    $nama_kecamatannya = $cek_data_kecamatan->s_namakecamatan;
                    $nama_kelurahannya = $nama_kelurahan;
                }
            } else {
                $nama_kecamatannya = $cek_data_kecamatan->s_namakecamatan;
                $nama_kelurahannya = $nama_kelurahan;
            }
        } else {
            $nama_kecamatannya = $cek_data_kecamatan->s_namakecamatan;
            $nama_kelurahannya = $nama_kelurahan;
        }
        return response()->json([
                    'res' => $opsi,
                    's_kd_kecamatan' => $cek_data_kecamatan->s_kd_kecamatan,
                    'nama_kec' => $nama_kecamatannya,
                    'nama_kelurahan' => $nama_kelurahannya,
        ]);
    }

    public function ceknamakelurahan(Request $Request) {
        $data_get = $Request->all();
        if (!empty($data_get['s_idkelurahan'])) {
            $cek_data_kelurahan = (new Spt())->cek_id_kelurahan($data_get['s_idkelurahan']);
        }
        return response()->json([
                    's_kd_kelurahan' => $cek_data_kelurahan->s_kd_kelurahan,
                    'nama_kel' => $cek_data_kelurahan->s_namakelurahan,
        ]);
    }

    public function datanop(Request $request) {
        $data_get = $request->all();
        $hasil_data = array();
        if (!empty($data_get['t_nop_sppt']) && !empty($data_get['t_tahun_sppt'])) {
            $cek_data = (new Sppt())->getDataNOP($data_get['t_nop_sppt'], $data_get['t_tahun_sppt']);
            $hasil_data = !empty($cek_data) ? $cek_data : $hasil_data;
        }

        return response()->json($hasil_data);
    }

    public function cektunggakanpbb(Request $Request) {
        $data_get = $Request->all();
        $html = '';
        $PBB_YG_HARUS_DIBAYAR_SPPT = 0;
        
        $cek_tahuntunggakanpbb = (new Spt())->cek_tahuntunggakanpbb();
        
        if(!empty($cek_tahuntunggakanpbb->s_rentangthntgkpbb)){
            $tahunpbbmin = date('Y') - $cek_tahuntunggakanpbb->s_rentangthntgkpbb;
        }else{
            $tahunpbbmin = $cek_tahuntunggakanpbb->s_thntgkpbb;
            
        }
        
        if (!empty($data_get['t_nop_sppt'])) {
            $nop = explode('.', $data_get['t_nop_sppt']);
            $KD_PROPINSI = $nop[0];
            $KD_DATI2 = $nop[1];
            $KD_KECAMATAN = $nop[2];
            $KD_KELURAHAN = $nop[3];
            $KD_BLOK = $nop[4];
            $NO_URUT = $nop[5];
            $KD_JNS_OP = $nop[6];
            if(!empty($cek_tahuntunggakanpbb->s_thntgkpbb) || !empty($cek_tahuntunggakanpbb->s_rentangthntgkpbb) ){
                $cek_data_tunggakan = Sppt::select(
                                    'SPPT.THN_PAJAK_SPPT',
                                    'SPPT.PBB_YG_HARUS_DIBAYAR_SPPT',
                                    DB::raw('TO_CHAR(SPPT.TGL_JATUH_TEMPO_SPPT,\'DD-MM-YYYY\') AS JATUH_TEMPO'))
                            ->where([
                                ['SPPT.KD_PROPINSI', '=', $KD_PROPINSI],
                                ['SPPT.KD_DATI2', '=', $KD_DATI2],
                                ['SPPT.KD_KECAMATAN', '=', $KD_KECAMATAN],
                                ['SPPT.KD_KELURAHAN', '=', $KD_KELURAHAN],
                                ['SPPT.KD_BLOK', '=', $KD_BLOK],
                                ['SPPT.NO_URUT', '=', $NO_URUT],
                                ['SPPT.KD_JNS_OP', '=', $KD_JNS_OP],
                                ['SPPT.STATUS_PEMBAYARAN_SPPT', '=', '0'],
                                ['SPPT.THN_PAJAK_SPPT', '>=', $tahunpbbmin]
                            ])->orderBy('SPPT.THN_PAJAK_SPPT', 'ASC')->get();
            }else{
                $cek_data_tunggakan = Sppt::select(
                                    'SPPT.THN_PAJAK_SPPT',
                                    'SPPT.PBB_YG_HARUS_DIBAYAR_SPPT',
                                    DB::raw('TO_CHAR(SPPT.TGL_JATUH_TEMPO_SPPT,\'DD-MM-YYYY\') AS JATUH_TEMPO'))
                            ->where([
                                ['SPPT.KD_PROPINSI', '=', $KD_PROPINSI],
                                ['SPPT.KD_DATI2', '=', $KD_DATI2],
                                ['SPPT.KD_KECAMATAN', '=', $KD_KECAMATAN],
                                ['SPPT.KD_KELURAHAN', '=', $KD_KELURAHAN],
                                ['SPPT.KD_BLOK', '=', $KD_BLOK],
                                ['SPPT.NO_URUT', '=', $NO_URUT],
                                ['SPPT.KD_JNS_OP', '=', $KD_JNS_OP],
                                ['SPPT.STATUS_PEMBAYARAN_SPPT', '=', '0']
                                
                            ])->orderBy('SPPT.THN_PAJAK_SPPT', 'ASC')->get();
            }
            $html = "<div class='row'>
                          <div class='col-md-12'>
                          <div class='panel panel-primary'>
                          <div class='panel-heading'><strong>Tunggakan SPPT-PBB</strong></div>
                          <table class='table table-striped table-bordered'>";
            $html .= "<tr>";
            $html .= "<th>No.</th>";
            $html .= "<th>Tahun</th>";
            $html .= "<th>Tunggakan (Rp.)</th>";
            $html .= "<th>Jatuh Tempo</th>";
            $html .= "<th>Denda (Rp.)</th>";
            $html .= "</tr>";
            $i = 1;
            $jumlahdenda = 0;
            $PBB_YG_HARUS_DIBAYAR_SPPT = 0;
            foreach ($cek_data_tunggakan as $row) {
                $html .= "<tr>";
                $html .= "<td style='text-align: center'> " . $i . " </td>";
                $html .= "<td style='text-align: center'> " . $row['thn_pajak_sppt'] . " </td>";
                $html .= "<td style='text-align: right'> " . number_format($row['pbb_yg_harus_dibayar_sppt'], 0, ',', '.') . " </td>";
                $html .= "<td style='text-align: center'>  " . $row['jatuh_tempo'] . " </td>";
                if (strtotime($row['jatuh_tempo']) < strtotime(date('Y-m-d'))) {
                    $cek_denda = $this->cek_jumlahbulandenda($row['jatuh_tempo'], $row['pbb_yg_harus_dibayar_sppt']);
                    $denda = $cek_denda['denda'];
                } else {
                    $denda = 0;
                }
                $html .= "<td style='text-align: right'>  " . number_format($denda, 0, ',', '.') . " </td>";
                $html .= "</tr>";
                $i++;
                $PBB_YG_HARUS_DIBAYAR_SPPT += $row['pbb_yg_harus_dibayar_sppt'];
                $jumlahdenda = $jumlahdenda + $denda;
            }
            $html .= "<tr style='font-size:16px; font-weight:bold;'>";
            $html .= "<td colspan='2' style='text-align: right'>Jumlah Tunggakan</td>";
            $html .= "<td style='text-align: right'> " . number_format($PBB_YG_HARUS_DIBAYAR_SPPT, 0, ',', '.') . " </td>";
            $html .= "<td style='text-align: right'>Jumlah Denda</td>";
            $html .= "<td style='text-align: right'> " . number_format($jumlahdenda, 0, ',', '.') . " </td>";
            $html .= "</tr>";
            $html .= "<tr style='font-size:16px; font-weight:bold;'>";
            $html .= "<td colspan='3' style='text-align: right'>Jumlah Seluruh Tunggakan</td>";
            $html .= "<td colspan='2' style='text-align: right'> Rp. " . number_format($PBB_YG_HARUS_DIBAYAR_SPPT + $jumlahdenda, 0, ',', '.') . "</td>";
            $html .= "</tr>";
            $html .= "</table></div></div></div>";
        }
        return response()->json([
                    'datatunggakan' => $html,
                    'PBB_YG_HARUS_DIBAYAR_SPPT' => $PBB_YG_HARUS_DIBAYAR_SPPT,
        ]);
    }

    public function simpantahappertama(TahapPertamaRequest $tahapPertamaRequest) {
        $attr = $tahapPertamaRequest->validated();
        $req = $tahapPertamaRequest->all();
        $attr['t_iduser_buat'] = Auth::user()->id;
        // $attr['t_tgldaftar_spt'] = date('Y-m-d H:i:s');
        $attr['t_periodespt'] = date('Y');
        $attr['t_idjenisketetapan'] = 1;
        // dd($attr);
        if ($attr['t_idbidang_usaha'] == 2) {
            $attr['t_nib_pembeli'] = $req['t_nib_pembeli'];
        }
        if (empty($req['t_idspt'])) {
            $cek_nourut = (new Spt())->cek_nourut_pertahun(date('Y'));
            $attr['t_kohirspt'] = $cek_nourut->no_urut;
            $attr['t_uuidspt'] = Uuid::uuid4()->getHex();
            $create = Spt::create($attr);
            $uuid = $create->t_uuidspt;
            $t_idspt = $create->t_idspt;
            $ketsimpan = 'Data baru berhasil ditambahkan.';
            if (!empty($req['nik_bersama'])) {
                for ($i = 1; $i < count($req['id_nik_bersama']); $i++) {
                    $dataNik = [
                        't_idspt' => $t_idspt,
                        't_nik_bersama' => $req['nik_bersama'][$i],
                        't_nama_bersama' => $req['nama_bersama'][$i],
                        't_nama_jalan' => $req['nama_jalanbersama'][$i],
                        't_rt_bersama' => $req['rt_bersama'][$i],
                        't_rw_bersama' => $req['rw_bersama'][$i],
                        't_kecamatan_bersama' => $req['kecamatan_bersama'][$i],
                        't_kelurahan_bersama' => $req['kelurahan_bersama'][$i],
                        't_kabkota_bersama' => $req['kabkota_bersama'][$i]
                    ];
                    if (empty($req['id_nik_bersama'][$i])) {
                        if (!empty($req['nik_bersama'][$i])) {
                            NikBersama::create($dataNik);
                        }
                    } else {
                        DB::table('t_nik_bersama')->where('t_id_nik_bersama', $req['id_nik_bersama'][$i])->update($dataNik);
                    }
                }
            }
        } else {
            $create = Spt::where('t_idspt', $req['t_idspt'])->update($attr);
            $uuid = $req['t_uuidspt'];
            $t_idspt = $req['t_idspt'];
            $ketsimpan = 'Data baru berhasil diupdate.';
            if (!empty($req['nik_bersama'])) {
                for ($i = 1; $i < count($req['id_nik_bersama']); $i++) {
                    $dataNik = [
                        't_idspt' => $t_idspt,
                        't_nik_bersama' => $req['nik_bersama'][$i],
                        't_nama_bersama' => $req['nama_bersama'][$i],
                        't_nama_jalan' => $req['nama_jalanbersama'][$i],
                        't_rt_bersama' => $req['rt_bersama'][$i],
                        't_rw_bersama' => $req['rw_bersama'][$i],
                        't_kecamatan_bersama' => $req['kecamatan_bersama'][$i],
                        't_kelurahan_bersama' => $req['kelurahan_bersama'][$i],
                        't_kabkota_bersama' => $req['kabkota_bersama'][$i]
                    ];
                    if (empty($req['id_nik_bersama'][$i])) {
                        if (!empty($req['nik_bersama'][$i])) {
                            NikBersama::create($dataNik);
                        }
                    } else {
                        DB::table('t_nik_bersama')->where('t_id_nik_bersama', $req['id_nik_bersama'][$i])->update($dataNik);
                    }
                }
            }
        }
        if ($create) {
            session()->flash('success', 'Data baru berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('pendaftaran/tahapkedua/' . $uuid);
    }

    public function tahappertama($uuid, Spt $Spt) {
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        $jmlnikbersama = (new Spt())->get_nikbersama_count($Spt['t_idspt']);
        // dd($cek_data_spt);
        $view = view('pendaftaran.tahappertama', [
            'dataspt' => $cek_data_spt,
            'data_notaris' => $data_notaris,
            'data_jenistransaksi' => $data_jenistransaksi,
            'data_jenisbidangusaha' => $data_jenisbidangusaha,
            'data_kecamatan' => (new Spt())->cekdata_kecamatan(),
            'jmlnikbersama' => $jmlnikbersama
        ]);
        return $view;
    }

    //========================================================================== TAHAP KEDUA

    function cekedit_perhitunganbphtb($idspt, $nikbersama, $ex) {
        $nikbersama = str_ireplace("'", "", $nikbersama);
        $tahunproses = date('Y', strtotime($ex->t_tgldaftar_spt));
        $ceknik = (new Spt())->ceknik($nikbersama, $tahunproses);
        $no = 1;
        $id_spt_pertama = '';
        foreach ($ceknik as $key => $v) {
            if ($no == 1) {
                $id_spt_pertama = $v->t_idspt;
            }
            $no++;
        }
        $idcek = $id_spt_pertama;
        $data = array();
        if (!empty($idcek)) {
            if ($idcek == $idspt) {
                $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                $data['dapatgak'] = 1;
            } else {
                $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                $data['dapatgak'] = 0;
            }
        } else {
            $data['ket_npoptkp'] = 'Dapat NPOPTKP';
            $data['dapatgak'] = 1;
        }
        return $data;
    }

    function hitung_pajak_bphtb($data_get) {
        $perhitungan = array();
        $cek_data_spt = (new Spt())->cek_uuid_spt($data_get['t_uuidspt']);
//        dd($cek_data_spt->t_idspt);
        $cek_jenis_transaksi = (new Spt())->cek_jenis_transaksi($cek_data_spt->t_idjenistransaksi);
        $perhitungan['t_luastanah'] = str_ireplace(".", "", $data_get['t_luastanah']);
        $perhitungan['t_njoptanah'] = str_ireplace(".", "", $data_get['t_njoptanah']);
        $perhitungan['t_totalnjoptanah'] = $perhitungan['t_luastanah'] * $perhitungan['t_njoptanah'];
        $perhitungan['t_luasbangunan'] = str_ireplace(".", "", $data_get['t_luasbangunan']);
        $perhitungan['t_njopbangunan'] = str_ireplace(".", "", $data_get['t_njopbangunan']);
        $perhitungan['t_totalnjopbangunan'] = $perhitungan['t_luasbangunan'] * $perhitungan['t_njopbangunan'];
        $perhitungan['t_grandtotalnjop'] = $perhitungan['t_totalnjoptanah'] + $perhitungan['t_totalnjopbangunan'];
        $perhitungan['t_nilaitransaksispt'] = str_ireplace(".", "", $data_get['t_nilaitransaksispt']);
        $cek_tarifbphtb = (new Spt())->cek_tarifbphtb(date('Y-m-d'));
        $perhitungan['t_idtarifbphtb'] = $cek_tarifbphtb->s_idtarifbphtb;
        $perhitungan['t_persenbphtb'] = $cek_tarifbphtb->s_tarif_bphtb;
        if ($cek_jenis_transaksi->s_idstatus_pht == 1) {    //================== Dari Perbandingan NJOP dengan Nilai Transaksi
            if ($perhitungan['t_nilaitransaksispt'] >= $perhitungan['t_grandtotalnjop']) {
                $perhitungan['t_npop_bphtb'] = $perhitungan['t_nilaitransaksispt'];
            } else {
                $perhitungan['t_npop_bphtb'] = $perhitungan['t_grandtotalnjop'];
            }
        } elseif ($cek_jenis_transaksi->s_idstatus_pht == 2) { //================ Dari NJOP PBB
            $perhitungan['t_npop_bphtb'] = $perhitungan['t_grandtotalnjop'];
        } elseif ($cek_jenis_transaksi->s_idstatus_pht == 3) { //================ Dari Nilai Transaksi
            $perhitungan['t_npop_bphtb'] = $perhitungan['t_nilaitransaksispt'];
        } else {
            if ($perhitungan['t_nilaitransaksispt'] >= $perhitungan['t_grandtotalnjop']) {
                $perhitungan['t_npop_bphtb'] = $perhitungan['t_nilaitransaksispt'];
            } else {
                $perhitungan['t_npop_bphtb'] = $perhitungan['t_grandtotalnjop'];
            }
        }
        $cek_npoptkp = (new Spt())->cek_npoptkp($cek_data_spt->t_idjenistransaksi);
        if ($cek_jenis_transaksi->s_id_dptnpoptkp == 1) {   //NPOPTKP Setiap Transaksi Dapat
            $perhitungan['t_npoptkp_bphtb'] = $cek_npoptkp->s_tarifnpoptkp;
        } elseif ($cek_jenis_transaksi->s_id_dptnpoptkp == 2) {  //NPOPTKP 1 Tahun Sekali dapatnya
            $ceknikbersama = (new Spt())->get_nikbersama($cek_data_spt->t_idspt);
            $nikbersama = '';
            if (count($ceknikbersama) > 0) {
                foreach ($ceknikbersama as $key => $v) {
                    $nikbersama .= "'" . $v->t_nik_bersama . "',";
                }
            }

            if (!empty($nikbersama) && ($nikbersama != ',')) {
                $semuadatanikbersama = substr_replace($nikbersama, "", -1) . "," . "'" . $cek_data_spt->t_nik_pembeli . "'";
            } else {
                $semuadatanikbersama = "'" . $cek_data_spt->t_nik_pembeli . "'";
            }

            $tahunproses = date('Y', strtotime($cek_data_spt->t_tgldaftar_spt));
            $explodnikbersama = explode(',', $semuadatanikbersama);

            $jmlnikbersama = (count($explodnikbersama));
            $ceknikbersama = (new Spt())->ceknikbersama($tahunproses, $semuadatanikbersama);

            //===================== kalo salah satu belum dapat potongan maka di anggap dapat semua
            $semuanikygdapat = '';
            $semuanikygtdkdapat = '';

            foreach ($ceknikbersama as $key => $hasilnik) {
                if (in_array($hasilnik->t_nikwppembeli, $explodnikbersama)) {
                    $semuanikygdapat .= $hasilnik->t_nikwppembeli . ',';
                } else {
                    $semuanikygdapat .= '';
                }
            }
            $explodnik_dpt = explode(',', $semuanikygdapat);
            $perhitungan['nikpernah_dapat'] = '<b style="font-size: 20px; color: red;">NIK YANG SUDAH PERNAH DAPAT NPOPTKP : ' . substr_replace($semuanikygdapat, "", -1) . ' </b>';
            for ($ii = 0; $ii < count($explodnikbersama); $ii++) {
                if (in_array($explodnikbersama[$ii], $explodnik_dpt)) {
                    $semuanikygtdkdapat .= '';
                } else {
                    $semuanikygtdkdapat .= $explodnikbersama[$ii] . ',';
                }
            }
            $perhitungan['nikpernah_tdkdapat'] = '<b style="font-size: 20px; color: blue;">NIK YANG BELUM PERNAH DAPAT NPOPTKP : ' . substr_replace($semuanikygtdkdapat, "", -1) . ' </b>';
            $tampildetailhistoritransaksi = '';
            $tampildetailhistoritransaksi = '<div class="col-sm-12">
                                                        <div class="col-sm-12">
                                                            <div class="form-group" id="historybphtb">
                                                                <div class="modal-content">
                                                                    <div class="modal-header" style="background-color: #0073b7; color: #fff;">
                                                                        <center><h4 class="modal-title" id="myModalLabel"><i class="fa fa-fw fa-table"></i> CEK DATA HISTORI NIK TAHUN ' . $tahunproses . '</h4></center>
                                                                    </div>
                                                                    <div id="historydiv" class="tablecoyscroll">';
            $tampildetailhistoritransaksi .= '<table class="tablecoy" style="width: 100%;"><tr><td><center><b>NO</b></center></td><td><center><b>NIK</b></center></td><td><center><b>WARIS / HIBAH WASIAT</b></center></td><td><center><b>SELAIN WARIS / HIBAH WASIAT</b></center></td></tr>';
            $notrasaksinya = 1;
            for ($ii = 0; $ii < count($explodnikbersama); $ii++) {
                // iki nggo cek hibah wasiat atau waris
                $ceknikbersama_hibahwaris = (new Spt())->ceknikbersama_pertransaksi($tahunproses, $explodnikbersama[$ii], 2);
                // iki nggo cek selain hibah wasiat atau waris
                $ceknikbersama_selain_hibahwaris = (new Spt())->ceknikbersama_pertransaksi($tahunproses, $explodnikbersama[$ii], 1);

                if (count($ceknikbersama_hibahwaris) > 0) {
                    $status_transaksi_hibahwaris = '<span style="color: red;">SUDAH</span>';
                } else {
                    $status_transaksi_hibahwaris = '<span style="color: green;">BELUM</span>';
                }

                if (count($ceknikbersama_selain_hibahwaris) > 0) {
                    $status_transaksiselain_hibahwaris = '<span style="color: red;">SUDAH</span>';
                } else {
                    $status_transaksiselain_hibahwaris = '<span style="color: green;">BELUM</span>';
                }

                $tampildetailhistoritransaksi .= '<tr><td><center>' . $notrasaksinya . '</center></td><td><center>' . $explodnikbersama[$ii] . '</center></td><td><center>' . $status_transaksi_hibahwaris . '</center></td><td><center>' . $status_transaksiselain_hibahwaris . '</center></td></tr>';
                $notrasaksinya++;
            }
            $tampildetailhistoritransaksi .= '<table></div></div></div></div></div>';
            $perhitungan['detail_nikbersama'] = $tampildetailhistoritransaksi;
            $id = $cek_data_spt->t_idspt; //$data_get['t_idspt'];
            $hasilcek = 0;
            for ($ii = 0; $ii < count($explodnikbersama); $ii++) {
                $jmlcekidspt = $this->cekedit_perhitunganbphtb($id, $explodnikbersama[$ii], $cek_data_spt);
                $hasilcek += $jmlcekidspt['dapatgak'];
            }
            if ($hasilcek > 0) {
                //if ($hasilcek == count($explodnikbersama)) {
                $perhitungan['t_npoptkp_bphtb'] = $cek_npoptkp->s_tarifnpoptkp;
                $perhitungan['ket_npoptkp'] = 'Dapat NPOPTKP';
            } else {
                $perhitungan['t_npoptkp_bphtb'] = 0;
                $perhitungan['ket_npoptkp'] = 'Tidak Dapat NPOPTKP'; //'.$hasilcek;
            }
        } else {
            $perhitungan['t_npoptkp_bphtb'] = $cek_npoptkp->s_tarifnpoptkp;
        }

        if (!empty($perhitungan['t_luastanah'])) {
            $perhitungan['t_permeter_tanah'] = ($perhitungan['t_npop_bphtb'] - $perhitungan['t_totalnjopbangunan']) / $perhitungan['t_luastanah'];
        } else {
            $perhitungan['t_permeter_tanah'] = 0;
        }
        $perhitungan['t_npopkp_bphtb'] = $perhitungan['t_npop_bphtb'] - $perhitungan['t_npoptkp_bphtb'];

        if ($perhitungan['t_npopkp_bphtb'] < 0) {
            $perhitungan['t_npopkp_bphtb'] = 0;
        } else {
            $perhitungan['t_npopkp_bphtb'] = $perhitungan['t_npopkp_bphtb'];
        }

        if (!empty($data_get['t_idjenisfasilitas'])) {
            $perhitungan['t_nilai_bphtb_fix'] = 0;
            $perhitungan['t_nilai_bphtb_real'] = 0;
        } else {
            $perhitungan['t_nilai_bphtb_fix'] = ceil($perhitungan['t_npopkp_bphtb'] * $perhitungan['t_persenbphtb'] / 100);
            $perhitungan['t_nilai_bphtb_real'] = $perhitungan['t_nilai_bphtb_fix'];
        }
        return $perhitungan;
    }

    public function tahapkedua($uuid) {
        $all_session = Auth::user();
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        $data_jenishaktanah = (new Spt())->cekdata_jenis_haktanah();
        $data_jenisdoktanah = (new Spt())->cekdata_jenis_doktanah();
        $data_jenispengurangan = (new Spt())->cekdata_jenis_pengurangan();
        $data_jenisperumahantanah = (new Spt())->cekdata_jenis_perumahan_tanah();
        $data_jenisfasilitas = (new Spt())->cekdata_jenis_fasilitas();
        $view = view('pendaftaran.tahapkedua', [
            'dataspt' => $cek_data_spt,
            'session' => $all_session,
            'data_notaris' => $data_notaris,
            'data_jenistransaksi' => $data_jenistransaksi,
            'data_jenisbidangusaha' => $data_jenisbidangusaha,
            'data_jenishaktanah' => $data_jenishaktanah,
            'data_jenisdoktanah' => $data_jenisdoktanah,
            'data_jenispengurangan' => $data_jenispengurangan,
            'data_jenisperumahantanah' => $data_jenisperumahantanah,
            'data_jenisfasilitas' => $data_jenisfasilitas
        ]);
        return $view;
    }

    public function simpantahapkedua(TahapKeduaRequest $tahapKeduaRequest) {
        $attr = $tahapKeduaRequest->validated();
        $req = $tahapKeduaRequest->all();
        $attr['t_uuidspt'] = $req['t_uuidspt'];
        $hitung_pajak_bphtb = $this->hitung_pajak_bphtb($req);
        if (!empty($attr['t_tarif_pembagian_aphb_kali'])) {
            $attr['t_tarif_pembagian_aphb_kali'] = $attr['t_tarif_pembagian_aphb_kali'];
        }
        if (!empty($attr['t_tarif_pembagian_aphb_bagi'])) {
            $attr['t_tarif_pembagian_aphb_bagi'] = $attr['t_tarif_pembagian_aphb_bagi'];
        }
        $perhitungan = [
            't_luastanah_sismiop' => $req['t_luastanah_sismiop'],
            't_luasbangunan_sismiop' => $req['t_luasbangunan_sismiop'],
            't_njoptanah_sismiop' => $req['t_njoptanah_sismiop'],
            't_njopbangunan_sismiop' => $req['t_njopbangunan_sismiop'],
            't_luastanah' => str_ireplace(".", "", $attr['t_luastanah']),
            't_njoptanah' => str_ireplace(".", "", $attr['t_njoptanah']),
            't_totalnjoptanah' => $hitung_pajak_bphtb['t_totalnjoptanah'],
            't_luasbangunan' => str_ireplace(".", "", $attr['t_luasbangunan']),
            't_njopbangunan' => str_ireplace(".", "", $attr['t_njopbangunan']),
            't_totalnjopbangunan' => $hitung_pajak_bphtb['t_totalnjopbangunan'],
            't_grandtotalnjop' => $hitung_pajak_bphtb['t_grandtotalnjop'],
            't_nilaitransaksispt' => $hitung_pajak_bphtb['t_nilaitransaksispt'],
            't_tgldok_tanah' => date('Y-m-d', strtotime($attr['t_tgldok_tanah'])),
            't_permeter_tanah' => $hitung_pajak_bphtb['t_permeter_tanah'],
            't_idtarifbphtb' => $hitung_pajak_bphtb['t_idtarifbphtb'],
            't_persenbphtb' => $hitung_pajak_bphtb['t_persenbphtb'],
            't_npop_bphtb' => $hitung_pajak_bphtb['t_npop_bphtb'],
            't_npoptkp_bphtb' => $hitung_pajak_bphtb['t_npoptkp_bphtb'],
            't_npopkp_bphtb' => $hitung_pajak_bphtb['t_npopkp_bphtb'],
            't_nilai_bphtb_fix' => $hitung_pajak_bphtb['t_nilai_bphtb_fix'],
            't_nilai_bphtb_real' => $hitung_pajak_bphtb['t_nilai_bphtb_real'],
        ];
        $data = array_merge($attr, $perhitungan);
        $create = Spt::where('t_uuidspt', $attr['t_uuidspt'])->update($data);
        $uuid = $data['t_uuidspt'];
        if ($create) {
            session()->flash('success', 'Data baru berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('pendaftaran/tahapketiga/' . $uuid);
    }

    //========================================================================== END TAHAP KEDUA
    //========================================================================== TAHAP KETIGA
    public function tahapketiga($uuid) {
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $cek_persyaratan = (new Spt())->cekdata_id_syaratupload($cek_data_spt->t_idjenistransaksi);
        $view = view('pendaftaran.tahapketiga', [
            'dataspt' => $cek_data_spt,
            'cek_persyaratan' => $cek_persyaratan,
        ]);
        return $view;
    }

    public function simpantahapketiga(Request $Request) {
        $attr = $Request->all();
        $id = $attr['t_uuidspt'];
        $cek_data_spt = (new Spt())->cek_uuid_spt($id);
        $persyaratan = \App\Models\Setting\Persyaratan::where('s_idjenistransaksi', '=', $cek_data_spt->t_idjenistransaksi)->get();
        foreach ($persyaratan as $v) {
            $cekUploadFile = DB::table('t_filesyarat_bphtb')->where([
                        ['t_idspt', '=', $cek_data_spt->t_idspt],
                        ['s_idpersyaratan', '=', $v['s_idpersyaratan']]
                    ])->count();
            if ($cekUploadFile == 0) {
                session()->flash('error', 'Mohon cek kembali data yang anda upload. Upload file berkas harus lengkap.');
                return redirect('pendaftaran/tahapketiga/' . $id);
            }
        }
        return redirect('pendaftaran/tahapkeempat/' . $id);
    }

    //========================================================================== END TAHAP KETIGA
    //========================================================================== TAHAP KEEMPAT
    public function tahapkeempat($uuid) {
        $data_pemda = Pemda::orderBy('updated_at', 'desc')->first();
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $nop = explode(".", $cek_data_spt->t_nop_sppt);
        $kordinatKelurahan = \App\Models\Setting\Kelurahan::where([
                    ['s_kd_propinsi', '=', $nop[0]],
                    ['s_kd_dati2', '=', $nop[1]],
                    ['s_kd_kecamatan', '=', $nop[2]],
                    ['s_kd_kelurahan', '=', $nop[3]]
                ])->first();
        if (empty($cek_data_spt->s_latitude) && empty($cek_data_spt->s_longitude)) {
            if (!empty($kordinatKelurahan['s_latitude']) && !empty($kordinatKelurahan['s_longitude'])) {
                $cek_data_spt->s_latitude = $kordinatKelurahan['s_latitude'];
                $cek_data_spt->s_longitude = $kordinatKelurahan['s_longitude'];
            } else {
                $kordinatKecamatan = \App\Models\Setting\Kecamatan::where([
                            ['s_kd_propinsi', '=', $nop[0]],
                            ['s_kd_dati2', '=', $nop[1]],
                            ['s_kd_kecamatan', '=', $nop[2]]
                        ])->first();
                $cek_data_spt->s_latitude = ((isset($kordinatKecamatan['s_latitude'])) ? $kordinatKecamatan['s_latitude'] : "");
                $cek_data_spt->s_longitude = ((isset($kordinatKecamatan['s_longitude'])) ? $kordinatKecamatan['s_longitude'] : "");
            }
        }
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();

        $view = view('pendaftaran.tahapkeempat', [
            'data_pemda' => $data_pemda,
            'dataspt' => $cek_data_spt,
            'data_notaris' => $data_notaris,
            'data_jenistransaksi' => $data_jenistransaksi,
            'data_jenisbidangusaha' => $data_jenisbidangusaha
        ]);

        return $view;
    }

    public function simpantahapkeempat(TahapKeempatRequest $tahapKeempatRequest) {
        $attr = $tahapKeempatRequest->validated();
        $req = $tahapKeempatRequest->all();
        $create = Spt::where('t_uuidspt', $req['t_uuidspt'])->update($attr);
        $id = $req['t_uuidspt'];
        $cek_data_spt = (new Spt())->cek_uuid_spt($id);
        $cekUploadFile = DB::table('t_file_objek')->where([['t_idspt', '=', $cek_data_spt->t_idspt]])->count();
        if ($cekUploadFile == 0) {
            session()->flash('error', 'Mohon cek kembali data yang anda upload. Foto objek pajak wajib diupload.');
            return redirect('pendaftaran/tahapkeempat/' . $id . '');
        }
        if ($create) {
            session()->flash('success', 'Data baru berhasil ditambahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('pendaftaran/tahapkelima/' . $id . '');
    }

    //========================================================================== END TAHAP KEEMPAT
    //========================================================================== TAHAP KELIMA
    public function tahapkelima($uuid) {
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        $history_tahun_berjalan = (new Spt())->cek_history_tahun_berjalan($cek_data_spt->t_idspt);
        $view = view('pendaftaran.tahapkelima', [
            'dataspt' => $cek_data_spt,
            'data_notaris' => $data_notaris,
            'data_jenistransaksi' => $data_jenistransaksi,
            'data_jenisbidangusaha' => $data_jenisbidangusaha,
            'data_history_tahun_berjalan' => $history_tahun_berjalan
        ]);
        return $view;
    }

    public function simpantahapkelima(Request $Request) {
        $attr = $Request->all();
        if ($attr['t_idspt'] == null) {
            $create = Spt::create($attr);
            $id = $create->t_idspt;
        } else {
            $create = Spt::where('t_idspt', $attr['t_idspt'])->update($attr);
            $id = $attr['t_idspt'];
        }
        if ($create) {
            session()->flash('success', 'Data baru berhasil ditambahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('pendaftaran/tahapkeenam/' . $id);
    }

    //========================================================================== END TAHAP KELIMA

    public function cek_detail_history_tahun_berjalan(Request $request) {
        //dd($request['id']);
        $detail_history_tahun_berjalan = (new Spt())->cek_detail_history_tahun_berjalan($request['id']);
        //dd($detail_history_tahun_berjalan);
        return response()->json(
                        $detail_history_tahun_berjalan,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    //========================================================================== TAHAP KEENAM

    public function tahapkeenam($uuid) {
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        $data_kec = (new Kecamatan())->get();
        if ($cek_data_spt->t_nama_penjual == null) {
            $cek_data_spt->t_nama_penjual = $cek_data_spt->t_nama_sppt;
            $cek_data_spt->t_jalan_penjual = $cek_data_spt->t_jalan_sppt;
            $cek_data_spt->t_rt_penjual = $cek_data_spt->t_rt_sppt;
            $cek_data_spt->t_rw_penjual = $cek_data_spt->t_rw_sppt;
            $cek_data_spt->t_kabkota_penjual = $cek_data_spt->t_kabkota_sppt;
            foreach ($data_kec as $v) {
                if ($cek_data_spt->t_kecamatan_sppt == $v['s_namakecamatan']) {
                    $cek_data_spt->t_idkec_penjual = $v['s_idkecamatan'];
                    $cek_data_spt->t_namakec_penjual = $v['s_namakecamatan'];
                }
            }
            $data_kel = (new Kelurahan())->where('s_idkecamatan', $cek_data_spt->t_idkec_penjual)->orderBy('s_idkelurahan')->get();
            foreach ($data_kel as $v) {
                if ($cek_data_spt->t_kelurahan_sppt == $v['s_namakelurahan']) {
                    $cek_data_spt->t_idkel_penjual = $v['s_idkelurahan'];
                    $cek_data_spt->t_namakel_penjual = $v['s_namakelurahan'];
                }
            }
        }
        $view = view('pendaftaran.tahapkeenam', [
            'dataspt' => $cek_data_spt,
            'data_notaris' => $data_notaris,
            'data_jenistransaksi' => $data_jenistransaksi,
            'data_jenisbidangusaha' => $data_jenisbidangusaha,
            'data_kecamatan' => (new Spt())->cekdata_kecamatan(),
        ]);
        return $view;
    }

    public function simpantahapkeenam(TahapKeenamRequest $tahapKeenamRequest) {
        $attr = $tahapKeenamRequest->validated();
        $req = $tahapKeenamRequest->all();
        if ($attr['t_idbidang_penjual'] == 2) {
            $attr['t_nib_penjual'] = $req['t_nib_penjual'];
        }
        $create = Spt::where('t_uuidspt', $req['t_uuidspt'])->update($attr);
        $id = $req['t_uuidspt'];
        if ($create) {
            session()->flash('success', 'Data baru berhasil ditambahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('pendaftaran/tahapketujuh/' . $id . '');
    }

    //========================================================================== END TAHAP KEENAM
    //========================================================================== TAHAP KETUJUH

    public function tahapketujuh($uuid) {
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        if (!empty($cek_data_spt->t_idpersetujuan_bphtb)) {
            $displaynya_persetujuan = 'style="display: block;"';
        } else {
            $displaynya_persetujuan = 'style="display: none;"';
        }
        $view = view('pendaftaran.tahapketujuh', [
            'dataspt' => $cek_data_spt,
            'data_notaris' => $data_notaris,
            'data_jenistransaksi' => $data_jenistransaksi,
            'data_jenisbidangusaha' => $data_jenisbidangusaha,
            'displaynya_persetujuan' => $displaynya_persetujuan
        ]);
        return $view;
    }

    public function simpantahapketujuh(Request $Request) {
        $attr = $Request->all();
        $create = [];
        if (!empty($attr['t_idpersetujuan_bphtb'])) {
            $data_simpan = ['t_idpersetujuan_bphtb' => 1];
            $create = Spt::where('t_uuidspt', $attr['t_uuidspt'])->update($data_simpan);
        }
        if ($create) {
            $data = Spt::where('t_uuidspt', $attr['t_uuidspt'])->first();
            event(new Registered($data));
            session()->flash('success', 'Data baru berhasil ditambahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('pendaftaran');
    }

    //========================================================================== END TAHAP KETUJUH
    //========================================================================== CETAK SSPD
    public function kekata($x) {
        $x = abs($x);
        $angka = array(
            "", "Satu", "Dua", "Tiga", "Empat", "Lima",
            "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"
        );
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = $this->kekata($x - 10) . " Belas";
        } else if ($x < 100) {
            $temp = $this->kekata($x / 10) . " Puluh" . $this->kekata($x % 10);
        } else if ($x < 200) {
            $temp = " Seratus" . $this->kekata($x - 100);
        } else if ($x < 1000) {
            $temp = $this->kekata($x / 100) . " Ratus" . $this->kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " Seribu" . $this->kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " Ribu" . $this->kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " Juta" . $this->kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " Milyar" . $this->kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " Trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

    function terbilang($x, $style = 4) {
        if ($x < 0) {
            $hasil = "MINUS " . trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }

    public function cetaksspdbphtb(Spt $Spt, $id) {
        //var_dump($id); exit();
        $data_pemda = Pemda::orderBy('updated_at', 'desc')->first();
        $cek_data_spt = (new Spt())->cetak_sspd_bphtb($Spt['t_idspt'], $id);
        $cek_menukaban = (new Spt())->cek_setmenukaban(); 
        $link = $data_pemda->s_urlbphtb.'/lihatdata/validasi/'.$Spt['t_uuidspt'];
        $terbilang_pajak = $this->terbilang($cek_data_spt->t_nilai_bphtb_fix);
        $barcode = \SimpleSoftwareIO\QrCode\Facades\QrCode::size(70)->generate($cek_data_spt->t_kodebayar_bphtb);
        $qrcode_validasi = ($cek_data_spt->t_tglvalidasikaban) ? \SimpleSoftwareIO\QrCode\Facades\QrCode::size(100)->generate($link) : '';
        $config = [
            'format' => 'A4-P', // Landscape
        ];
        $pdf = MPDF::loadview('pendaftaran.cetaksspdbphtb', [
                    'data_pemda' => $data_pemda,
                    'dataspt' => $cek_data_spt,
                    'cek_menukaban' => $cek_menukaban,
                    'terbilang_pajak' => $terbilang_pajak,
                    'barcode' => $barcode,
                    'qrcode_validasi' => $qrcode_validasi,
                    't_tglvalidasikaban' => ($cek_data_spt->t_tglvalidasikaban) ? 'Tanggal '. date('d-m-Y', strtotime($cek_data_spt->t_tglvalidasikaban)) : ''
                        ], $config);
        return $pdf->stream();
    }

    //========================================================================== END CETAK SSPD
    //========================================================================== CETAK SURAT PERNYATAAN

    public function cetaksuratpernyataan(Spt $Spt) {
        $data_pemda = Pemda::orderBy('updated_at', 'desc')->first();
        $cek_data_spt = (new Spt())->cek_id_spt($Spt['t_idspt']);

        $config = [
            'format' => 'Legal-P', // Landscape
        ];
        $pdf = MPDF::loadview('pendaftaran.cetaksuratpernyataan', [
                    'data_pemda' => $data_pemda,
                    'dataspt' => $cek_data_spt,
                        ], $config);
        return $pdf->stream();
    }

    //========================================================================== END CETAK SURAT PERNYATAAN
    //========================================================================== CEK JUMLAH DENDA

    public function cek_jumlahbulandenda($tgl_tempo, $tagihan, $tglbayarnya = null) {
        if (!empty($tglbayarnya)) {
            $tgl_bayar = $tglbayarnya;
        } else {
            $tgl_bayar = date('Y-m-d');
        }
        $tgl_tempo = date('Y-m-d', strtotime($tgl_tempo));
        $tgl_bayar = explode("-", $tgl_bayar);
        $tgl_tempo = explode("-", $tgl_tempo);
        $tahun = $tgl_bayar[0] - $tgl_tempo[0];
        $bulan = $tgl_bayar[1] - $tgl_tempo[1];
        $hari = $tgl_bayar[2] - $tgl_tempo[2];
        if (($tahun == 0) || ($tahun < 1)) {
            if (($bulan == 0) || ($bulan < 1)) {
                if ($bulan < 0) {
                    $months = 0;
                } else {
                    if (($hari == 0) || ($hari < 1)) {
                        $months = 0;
                    } else {
                        $months = 1;
                    }
                }
            } else {
                if ($bulan == 1) {
                    if (($hari == 0) || ($hari < 1)) {
                        $months = $bulan;
                    } else {
                        $months = $bulan + 1;
                    }
                } else {
                    if (($hari == 0) || ($hari < 1)) {
                        $months = $bulan;
                    } else {
                        $months = $bulan + 1;
                    }
                }
            }
        } else {
            $jmltahun = $tahun * 12;
            if ($bulan == 0) {
                //$months = $jmltahun;
                if (($hari == 0) || ($hari < 1)) {
                    $months = $jmltahun;
                } else {
                    $months = $jmltahun + 1;
                    //var_dump($months); exit();
                }
            } elseif ($bulan < 1) {
                //$months = $jmltahun + $bulan;
                if (($hari == 0) || ($hari < 1)) {
                    $months = $jmltahun + $bulan;
                } else {
                    $months = $jmltahun + $bulan + 1;
                    //var_dump($months); exit();
                }
            } else {
                //$months = $bulan + $jmltahun;
                if (($hari == 0) || ($hari < 1)) {
                    $months = $jmltahun + $bulan;
                } else {
                    $months = $jmltahun + $bulan + 1;
                }
            }
        }
        if ($months > 24)
            $months = 24;
        if ($months > 0)
            $jmldenda = $months * 2 / 100 * $tagihan;
        else
            $jmldenda = 0;
        $ubahstring = (string) ($jmldenda);
        $result['denda'] = ceil((float) $ubahstring);
        $result['jumlahbulandenda'] = $months;
        return $result;
    }

    //========================================================================== END CEK JUMLAH DENDA
    //========================================================================== upload multi persyaratan

    public function fetchpersyaratan(Request $Request) {
        $attr = $Request->all();
        $cek_data_fileupload = (new Spt())->cek_fileupload_syarat($attr);
        $output = '';
        $output .= '<table class="table table-bordered table-striped">
                     <tr>
                      <th>No</th>
                      <th>Gambar</th>
                      <th>Tgl. Upload</th>
                      <th>Nama</th>
                      <th>Keterangan</th>
                      <th>#</th>
                     </tr>
                   ';
        if (count($cek_data_fileupload) > 0) {
            $count = 0;
            foreach ($cek_data_fileupload as $row) {
                $count++;
                $output .= '<tr>';
                $output .= '<td>' . $count . '</td>';
                $output .= '<td><img src="' . asset($row->letak_file . '' . $row->nama_file) . '" class="img-thumbnail" width="100" height="100" /></td>';
                $output .= '<td>' . date('d-m-Y H:i:s', strtotime($row->t_tgl_upload)) . '</td>';
                $output .= '<td>
                                <a style="display:none;" data-ng-href="' . asset($row->letak_file . '' . $row->nama_file) . '" title="' . $row->nama_file . '" download="' . $row->nama_file . '" data-gallery="" href="' . asset($row->letak_file . '' . $row->nama_file) . '"><img data-ng-src="' . asset($row->letak_file . '' . $row->nama_file) . '" alt="" src="' . asset($row->letak_file . '' . $row->nama_file) . '"></a>
                                <a data-ng-switch-when="true" data-ng-href="' . asset($row->letak_file . '' . $row->nama_file) . '" title="' . $row->nama_file . '" download="' . $row->nama_file . '" data-gallery="" class="ng-binding ng-scope" href="' . asset($row->letak_file . '' . $row->nama_file) . '">' . $row->nama_file . ' </a>
                            </td>';
                $output .= '<td>' . $row->t_keterangan_file . '</td>';
                $edit = '<button type="button" class="btn btn-warning btn-xs edit' . $row->s_idpersyaratan . '" id="' . $row->t_id_filesyarat . '">Edit</button>';
                $hapus = '<button type="button" class="btn btn-danger btn-xs delete' . $row->s_idpersyaratan . '" id="' . $row->t_id_filesyarat . '" data-image_name="' . $row->nama_file . '">Hapus</button>';
                $output .= '<td style="text-align:center">' . $edit . " " . $hapus . '</td>';
                $output .= '</tr>';
            }
        } else {
            $output .= '<tr><td colspan="6" align="center">Tidak ada data</td></tr>';
        }
        $output .= '</table>';

        return response()->json([
                    'tampildata' => $output
        ]);
    }

    public function uploadpersyaratan(Request $Request) {
        $all_session = Auth::user();
        $attr = $Request->all();
        $idspt = $Request->query('t_idspt');
        //var_dump($attr['t_idspt']); exit();

        $cek_data_spt = (new Spt())->cek_id_spt($idspt);

        //$_FILES = array_merge_recursive($attr->toArray(), $attr->getFiles()->toArray());

        if (count($attr["file"]) > 0) {
            //$output = count($attr["file"]);
            sleep(3);
            //for ($count = 0; $count < count($_FILES["file"]["name"]); $count++) {
            for ($count = 0; $count < count($attr["file"]); $count++) {
                $file_name = $attr["file"][$count]->getClientOriginalName(); //["name"]

                $tmp_name = $attr["file"][$count]->getPathName(); //['tmp_name']
                //var_dump($tmp_name); exit();
                $file_array = explode(".", $file_name);
                $file_extension = end($file_array);
                if ($this->file_already_uploaded_persyaratan($file_name)) {
                    $file_name = $file_array[0] . '-' . rand() . '.' . $file_extension;
                }

                $letak_dir1 = "upload";
                $letak_dir2 = "file_syarat";
                $letak_dir3 = date('Y', strtotime($cek_data_spt->t_tgldaftar_spt));
                $letak_dir4 = strtolower(str_replace(' ', '', $cek_data_spt->s_namajenistransaksi));
                $letak_dir5 = $cek_data_spt->t_idspt;

                $letak_dir = $letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/';
                //mkdir("kampret/module/asem/src/anjir/Controller", 0777, true);

                if (is_dir($letak_dir1) == false) {
                    mkdir("" . $letak_dir1 . "", 0777, true);  // Create directory if it does not exist
                }
                if (is_dir($letak_dir1 . '/' . $letak_dir2) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2, 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "/" . $letak_dir5, 0777, true);  // Create directory if it does not exist
                }

                $location = $letak_dir . $file_name; //'files/' . $file_name;
                //$destinationPath = 'upload/';
                //$files->move($destinationPath, $files->getClientOriginalName());
                //$attr['s_filettd'] = $destinationPath . $files->getClientOriginalName();

                if (move_uploaded_file($tmp_name, $location)) {

                    //$simpanfileupload = $this->getServiceLocator()->get('KonsultasiTable')->simpanfileupload_persyaratan($file_name, $letak_dir, $data_get, $session);
                    $data_simpan = array(
                        't_idspt' => $cek_data_spt->t_idspt,
                        's_idpersyaratan' => $Request->query('s_idpersyaratan'),
                        's_idjenistransaksi' => $Request->query('s_idjenistransaksi'),
                        't_iduser_upload' => $all_session->id,
                        'letak_file' => $letak_dir,
                        'nama_file' => $file_name,
                        't_tgl_upload' => date('Y-m-d H:i:s'),
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $create = Filesyarat::create($data_simpan);
                    //$id = $create->t_idspt;
                    //$ketsimpan = 'Data baru berhasil ditambahkan.';
                }
            }
        }

        return response()->json([]);
    }

    function file_already_uploaded_persyaratan($file_name) {
        $number_of_rows = (new Spt())->cek_imageupload_persyaratan($file_name);

        if ($number_of_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function edituploadpersyaratan(Request $Request) {
        $data_get = $Request->all();
        //var_dump($data_get); exit();
        $result = (new Spt())->cek_idimage_persyaratan($data_get['image_id']);

        $file_array = explode(".", $result->nama_file);


        return response()->json([
                    'image_name' => $file_array[0],
                    'image_description' => $result->t_keterangan_file,
                    's_idpersyaratan' => $data_get['s_idpersyaratan']
        ]);
    }

    public function updateuploadpersyaratan(Request $Request) {
        $data_get = $Request->all();
        $idspt = $Request->query('t_idspt');
        //var_dump($data_get); exit();
        $result = (new Spt())->cek_idimage_persyaratan($data_get['image_id']);

        if (isset($data_get["image_id"])) {
            $old_name = $this->get_old_image_name_persyaratan($data_get["image_id"]);
            $file_array = explode(".", $old_name);
            $file_extension = end($file_array);
            $new_name = $data_get["image_name"] . '.' . $file_extension;
            $query = '';
            if ($old_name != $new_name) {

                $cek_data_spt = (new Spt())->cek_id_spt($idspt);

                $letak_dir1 = "upload";
                $letak_dir2 = "file_syarat";
                $letak_dir3 = date('Y', strtotime($cek_data_spt->t_tgldaftar_spt));
                $letak_dir4 = strtolower(str_replace(' ', '', $cek_data_spt->s_namajenistransaksi));
                $letak_dir5 = $cek_data_spt->t_idspt;


                $letak_dir = $letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/';
                //mkdir("kampret/module/asem/src/anjir/Controller", 0777, true);

                if (is_dir($letak_dir1) == false) {
                    mkdir("" . $letak_dir1 . "", 0777, true);  // Create directory if it does not exist
                }
                if (is_dir($letak_dir1 . '/' . $letak_dir2) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2, 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "/" . $letak_dir5, 0777, true);  // Create directory if it does not exist
                }



                $old_path = $letak_dir . $old_name; //'files/' . $old_name;
                $new_path = $letak_dir . $new_name; //'files/' . $new_name;
                if (rename($old_path, $new_path)) {


                    $data_simpan = array(
                        't_keterangan_file' => $data_get['image_description'],
                    );
                    if (!empty($data_get['image_name'])) {
                        $data_simpan['nama_file'] = $new_name;
                    }
                    $updatedata = Filesyarat::where('t_id_filesyarat', $data_get['image_id'])
                            ->update($data_simpan);
                }
            } else {


                $data_simpan = array(
                    't_keterangan_file' => $data_get['image_description'],
                );
                if (!empty($data_get['image_name'])) {
                    $data_simpan['nama_file'] = $new_name;
                }
                $updatedata = Filesyarat::where('t_id_filesyarat', $data_get['image_id'])
                        ->update($data_simpan);
            }
        }


        return response()->json([
                    $updatedata
        ]);
    }

    function get_old_image_name_persyaratan($image_id) {
        $result = (new Spt())->cek_idimage_persyaratan($image_id);
        return $result->nama_file;
    }

    public function deleteuploadpersyaratan(Request $Request) {
        $data_get = $Request->all();
        $idspt = $Request->query('t_idspt');
        //var_dump($data_get); exit();
        if (isset($data_get["image_id"])) {

            $cek_data_spt = (new Spt())->cek_id_spt($idspt);

            $letak_dir1 = "upload";
            $letak_dir2 = "file_syarat";
            $letak_dir3 = date('Y', strtotime($cek_data_spt->t_tgldaftar_spt));
            $letak_dir4 = strtolower(str_replace(' ', '', $cek_data_spt->s_namajenistransaksi));
            $letak_dir5 = $cek_data_spt->t_idspt;

            $letak_dir = $letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/';
            //mkdir("kampret/module/asem/src/anjir/Controller", 0777, true);

            if (is_dir($letak_dir1) == false) {
                mkdir("" . $letak_dir1 . "", 0777, true);  // Create directory if it does not exist
            }
            if (is_dir($letak_dir1 . '/' . $letak_dir2) == false) {
                mkdir("" . $letak_dir1 . "/" . $letak_dir2, 0777, true);  // Create directory if it does not exist
            }

            if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
            }

            if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
            }

            if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5) == false) {
                mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "/" . $letak_dir5, 0777, true);  // Create directory if it does not exist
            }

            $file_path = $letak_dir . $data_get["image_name"]; //'files/' . $post["image_name"];
            if (unlink($file_path)) {
                $result = Filesyarat::where('t_id_filesyarat', '=', $data_get["image_id"])->delete();
            }
        }

        return response()->json([$result]);
    }

    //========================================================================== end upload multi persyaratan
    //========================================================================== upload multi foto objek

    public function fetchfotoobjek(Request $Request) {
        $attr = $Request->all();
        $cek_data_fileupload = (new Spt())->cek_fileupload_fotoobjek($attr);
        $output = '';
        $output .= '
                    <table class="table table-bordered table-striped">
                     <tr>
                      <th>No</th>
                      <th>Image</th>
                      <th>Tgl Upload</th>
                      <th>Nama</th>
                      <th>Keterangan</th>
                      <th>#</th>
                     </tr>
                   ';
        if (count($cek_data_fileupload) > 0) {
            $count = 0;
            foreach ($cek_data_fileupload as $row) {
                $count++;
                $output .= '<tr>';
                $output .= '<td>' . $count . '</td>';
                $output .= '<td><img src="' . asset($row->letak_file . '' . $row->nama_file) . '" class="img-thumbnail" width="100" height="100" /></td>';
                $output .= '<td>' . date('d-m-Y H:i:s', strtotime($row->t_tgl_upload)) . '</td> ';
                $output .= '<td>
                        <a style="display:none;" data-ng-href="' . asset($row->letak_file . '' . $row->nama_file) . '" title="' . $row->nama_file . '" download="' . $row->nama_file . '" data-gallery="" href="' . asset($row->letak_file . '' . $row->nama_file) . '"><img data-ng-src="' . asset($row->letak_file . '' . $row->nama_file) . '" alt="" src="' . asset($row->letak_file . '' . $row->nama_file) . '"></a>
                        <a data-ng-switch-when="true" data-ng-href="' . asset($row->letak_file . '' . $row->nama_file) . '" title="' . $row->nama_file . '" download="' . $row->nama_file . '" data-gallery="" class="ng-binding ng-scope" href="' . asset($row->letak_file . '' . $row->nama_file) . '">' . $row->nama_file . ' </a>
                    </td>';
                $output .= '<td>' . $row->t_keterangan_file . '</td>';
                $edit = '<button type="button" class="btn btn-warning btn-xs edit" id="' . $row->t_idfile_objek . '">Edit</button>';
                $hapus = '<button type="button" class="btn btn-danger btn-xs delete" id="' . $row->t_idfile_objek . '" data-image_name="' . $row->nama_file . '">Hapus</button>';
                $output .= '<td style="text-align: center">' . $edit . ' ' . $hapus . '</td>';
                $output .= '</tr>';
            }
        } else {
            $output .= '<tr><td colspan="6" align="center">Tidak ada data</td></tr>';
        }
        $output .= '</table>';
        return response()->json([
                    'tampildata' => $output
        ]);
    }

    public function uploadfotoobjek(Request $Request) {
        $all_session = Auth::user();
        $attr = $Request->all();
        $idspt = $Request->query('t_idspt');
        //var_dump($attr['t_idspt']); exit();

        $cek_data_spt = (new Spt())->cek_id_spt($idspt);

        //$_FILES = array_merge_recursive($attr->toArray(), $attr->getFiles()->toArray());

        if (count($attr["file"]) > 0) {
            //$output = count($attr["file"]);
            sleep(3);
            //for ($count = 0; $count < count($_FILES["file"]["name"]); $count++) {
            for ($count = 0; $count < count($attr["file"]); $count++) {
                $file_name = $attr["file"][$count]->getClientOriginalName(); //["name"]

                $tmp_name = $attr["file"][$count]->getPathName(); //['tmp_name']
                //var_dump($tmp_name); exit();
                $file_array = explode(".", $file_name);
                $file_extension = end($file_array);
                if ($this->file_already_uploaded_fotoobjek($file_name)) {
                    $file_name = $file_array[0] . '-' . rand() . '.' . $file_extension;
                }

                $letak_dir1 = "upload";
                $letak_dir2 = "file_fotoobjek";
                $letak_dir3 = date('Y', strtotime($cek_data_spt->t_tgldaftar_spt));
                $letak_dir4 = strtolower(str_replace(' ', '', $cek_data_spt->s_namajenistransaksi));
                $letak_dir5 = $cek_data_spt->t_idspt;

                $letak_dir = $letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/';
                //mkdir("kampret/module/asem/src/anjir/Controller", 0777, true);

                if (is_dir($letak_dir1) == false) {
                    mkdir("" . $letak_dir1 . "", 0777, true);  // Create directory if it does not exist
                }
                if (is_dir($letak_dir1 . '/' . $letak_dir2) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2, 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "/" . $letak_dir5, 0777, true);  // Create directory if it does not exist
                }

                $location = $letak_dir . $file_name; //'files/' . $file_name;
                //$destinationPath = 'upload/';
                //$files->move($destinationPath, $files->getClientOriginalName());
                //$attr['s_filettd'] = $destinationPath . $files->getClientOriginalName();

                if (move_uploaded_file($tmp_name, $location)) {

                    //$simpanfileupload = $this->getServiceLocator()->get('KonsultasiTable')->simpanfileupload_persyaratan($file_name, $letak_dir, $data_get, $session);
                    $data_simpan = array(
                        't_idspt' => $cek_data_spt->t_idspt,
                        't_iduser_upload' => $all_session->id,
                        'letak_file' => $letak_dir,
                        'nama_file' => $file_name,
                        't_tgl_upload' => date('Y-m-d H:i:s'),
                        'created_at' => date('Y-m-d H:i:s')
                    );
                    $create = Filefotoobjek::create($data_simpan);
                    //$id = $create->t_idspt;
                    //$ketsimpan = 'Data baru berhasil ditambahkan.';
                }
            }
        }

        return response()->json([]);
    }

    function file_already_uploaded_fotoobjek($file_name) {
        $number_of_rows = (new Spt())->cek_imageupload_fotoobjek($file_name);

        if ($number_of_rows > 0) {
            return true;
        } else {
            return false;
        }
    }

    public function edituploadfotoobjek(Request $Request) {
        $data_get = $Request->all();
        $result = (new Spt())->cek_idimage_fotoobjek($data_get['image_id']);

        $file_array = explode(".", $result->nama_file);


        return response()->json([
                    'image_name' => $file_array[0],
                    'image_description' => $result->t_keterangan_file,
                    'image_id' => $data_get['image_id']
        ]);
    }

    public function updateuploadfotoobjek(Request $Request) {
        $data_get = $Request->all();
        $idspt = $Request->query('t_idspt');
        $result = (new Spt())->cek_idimage_fotoobjek($data_get['image_id']);

        if (isset($data_get["image_id"])) {
            $old_name = $this->get_old_image_name_fotoobjek($data_get["image_id"]);
            $file_array = explode(".", $old_name);
            $file_extension = end($file_array);
            $new_name = $data_get["image_name"] . '.' . $file_extension;
            $query = '';
            if ($old_name != $new_name) {

                $cek_data_spt = (new Spt())->cek_id_spt($idspt);

                $letak_dir1 = "upload";
                $letak_dir2 = "file_fotoobjek";
                $letak_dir3 = date('Y', strtotime($cek_data_spt->t_tgldaftar_spt));
                $letak_dir4 = strtolower(str_replace(' ', '', $cek_data_spt->s_namajenistransaksi));
                $letak_dir5 = $cek_data_spt->t_idspt;


                $letak_dir = $letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/';

                if (is_dir($letak_dir1) == false) {
                    mkdir("" . $letak_dir1 . "", 0777, true);  // Create directory if it does not exist
                }
                if (is_dir($letak_dir1 . '/' . $letak_dir2) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2, 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
                }

                if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5) == false) {
                    mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "/" . $letak_dir5, 0777, true);  // Create directory if it does not exist
                }
                $old_path = $letak_dir . $old_name; //'files/' . $old_name;
                $new_path = $letak_dir . $new_name; //'files/' . $new_name;
                if (rename($old_path, $new_path)) {
                    $data_simpan = array(
                        't_keterangan_file' => $data_get['image_description'],
                    );
                    if (!empty($data_get['image_name'])) {
                        $data_simpan['nama_file'] = $new_name;
                    }
                    $updatedata = Filefotoobjek::where('t_idfile_objek', $data_get['image_id'])
                            ->update($data_simpan);
                }
            } else {


                $data_simpan = array(
                    't_keterangan_file' => $data_get['image_description'],
                );
                if (!empty($data_get['image_name'])) {
                    $data_simpan['nama_file'] = $new_name;
                }
                $updatedata = Filefotoobjek::where('t_idfile_objek', $data_get['image_id'])
                        ->update($data_simpan);
            }
        }


        return response()->json([
                    $updatedata
        ]);
    }

    function get_old_image_name_fotoobjek($image_id) {
        $result = (new Spt())->cek_idimage_fotoobjek($image_id);
        return $result->nama_file;
    }

    public function deleteuploadfotoobjek(Request $Request) {
        $data_get = $Request->all();
        $idspt = $Request->query('t_idspt');
        if (isset($data_get["image_id"])) {

            $cek_data_spt = (new Spt())->cek_id_spt($idspt);

            $letak_dir1 = "upload";
            $letak_dir2 = "file_fotoobjek";
            $letak_dir3 = date('Y', strtotime($cek_data_spt->t_tgldaftar_spt));
            $letak_dir4 = strtolower(str_replace(' ', '', $cek_data_spt->s_namajenistransaksi));
            $letak_dir5 = $cek_data_spt->t_idspt;

            $letak_dir = $letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5 . '/';

            if (is_dir($letak_dir1) == false) {
                mkdir("" . $letak_dir1 . "", 0777, true);  // Create directory if it does not exist
            }
            if (is_dir($letak_dir1 . '/' . $letak_dir2) == false) {
                mkdir("" . $letak_dir1 . "/" . $letak_dir2, 0777, true);  // Create directory if it does not exist
            }

            if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
            }

            if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4) == false) {
                mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "", 0777, true);  // Create directory if it does not exist
            }

            if (is_dir($letak_dir1 . '/' . $letak_dir2 . '/' . $letak_dir3 . '/' . $letak_dir4 . '/' . $letak_dir5) == false) {
                mkdir("" . $letak_dir1 . "/" . $letak_dir2 . "/" . $letak_dir3 . "/" . $letak_dir4 . "/" . $letak_dir5, 0777, true);  // Create directory if it does not exist
            }

            $file_path = $letak_dir . $data_get["image_name"]; //'files/' . $post["image_name"];
            if (unlink($file_path)) {
                $result = Filefotoobjek::where('t_idfile_objek', '=', $data_get["image_id"])->delete();
            }
        }

        return response()->json([
                    $result
        ]);
    }

    //========================================================================== end upload multi foto objek

    public function cekdetailhistory(Request $Request) {
        echo 'cekdetailhistory';
    }

    //========================================================================== CEK NIK BERSAMA

    public function rinciannikbersama(Request $Request) {
        $attr = $Request->all();
        $ceknikbersama = (new Spt())->get_nikbersama($attr['t_idspt']);
        $jmlnikbersama = count($ceknikbersama);
        $datarincian = '';
        $datarincian .= ' <table class="table table-bordered" id="dynamic_field">  ';
        if (count($ceknikbersama) > 0) {
            $no = 2;
            foreach ($ceknikbersama as $key => $datanikbersama) {
                $datarincian .= '<tr id="row' . $no . '">
                                    <td>
                                         <input name="id_nik_bersama[]" id="id_nik_bersama' . $no . '" value="' . $datanikbersama->t_id_nik_bersama . '" type="hidden">
                                         <input type="text" maxlength="16" name="nik_bersama[]" id="nik_bersama' . $no . '" onkeypress="return numbersonly(this, event)" value="' . $datanikbersama->t_nik_bersama . '" onchange="return numbersonly(this, event);" onblur="return numbersonly(this, event);" onfocus="return numbersonly(this, event);" placeholder="NIK" class="form-control name_list" readonly="true" />
                                    </td>
                                    <td>
                                        <input type="text" class="form-control name_list" value="' . $datanikbersama->t_nama_bersama . '" name="nama_bersama[]" id="nama_bersama' . $no . '" placeholder="NAMA" readonly="true"/>
                                    </td>
                                    <td><input type="text" class="form-control name_list" value="' . $datanikbersama->t_nama_jalan . '" name="nama_jalanbersama[]" id="nama_jalanbersama' . $no . '" placeholder="NAMA JALAN" readonly="true"/></td>
                                    <td><input type="text" class="form-control name_list" value="' . $datanikbersama->t_rt_bersama . '" name="rt_bersama[]" id="rt_bersama' . $no . '" maxlength="3" onkeypress="return numbersonly(this, event)" onchange="return numbersonly(this, event);" onblur="return numbersonly(this, event);" onfocus="return numbersonly(this, event);" placeholder="RT" readonly="true"/></td>
                                    <td><input type="text" class="form-control name_list" value="' . $datanikbersama->t_rw_bersama . '" name="rw_bersama[]" id="rw_bersama' . $no . '" maxlength="3" onkeypress="return numbersonly(this, event)" onchange="return numbersonly(this, event);" onblur="return numbersonly(this, event);" onfocus="return numbersonly(this, event);" placeholder="RW" readonly="true"/></td>
                                    <td><input type="text" class="form-control name_list" value="' . $datanikbersama->t_kecamatan_bersama . '" name="kecamatan_bersama[]" id="kecamatan_bersama' . $no . '" placeholder="Kecamatan" readonly="true"/></td>
                                    <td><input type="text" class="form-control name_list" value="' . $datanikbersama->t_kelurahan_bersama . '" name="kelurahan_bersama[]" id="kelurahan_bersama' . $no . '" placeholder="Kelurahan" readonly="true"/></td>
                                    <td><input type="text" class="form-control name_list" value="' . $datanikbersama->t_kabkota_bersama . '" name="kabkota_bersama[]" id="kabkota_bersama' . $no . '" placeholder="Kab/Kota" readonly="true"/></td>
                                    <td>
                                         <button type="button" name="editnikbersama' . $no . '" id="editnikbersama' . $no . '" onclick="editnikbersama(' . $no . ');" class="btn btn-success"><i class="fa fa-fw fa-edit"></i>EDIT</button>

                                    </td>
                                    <td>
                                         <button type="button" name="hapusnikbersama' . $no . '" id="hapusnikbersama' . $no . '" onclick="hapusnikbersama(' . $datanikbersama->t_id_nik_bersama . ');" class="btn btn-danger"><i class="fa fa-fw fa-trash"></i>HAPUS</button>
                                    </td>
                    </tr>
                ';
                $no++;
            }
            //<button type="button" name="updatenikbersama'.$no.'" id="updatenikbersama'.$no.'" onclick="updatenikbersama('.$no.', '.$datanikbersama['ID_NIK_BERSAMA'].');" class="hidden btn btn-success"><i class="fa fa-fw fa-plus"></i>UPDATE</button>
        }
        $datarincian .= ' </table>';

        return response()->json([
                    "rincian_nikbersama" => $datarincian,
                    "jmlnikbersama" => $jmlnikbersama
        ]);
    }

    public function hapussatunikbersama(Request $Request) {
        $attr = $Request->all();
        $hasilhapus = Tnikbersama::where('t_id_nik_bersama', '=', $attr['id_nik_bersama'])->delete();

        return $hasilhapus;
    }

    //========================================================================== END CEK NIK BERSAMA

    public function cetakbahpbphtb(Spt $Spt) {
        $data_pemda = Pemda::orderBy('updated_at', 'desc')->first();
        $cek_data_spt = (new Spt())->cek_id_spt_pemeriksaan($Spt['t_idspt']);
        $cek_data_pejabat1 = (new Spt())->cek_id_pejabat($cek_data_spt->t_idpejabat1);
        $cek_data_pejabat2 = (new Spt())->cek_id_pejabat($cek_data_spt->t_idpejabat2);
        $cek_data_pejabat3 = (new Spt())->cek_id_pejabat($cek_data_spt->t_idpejabat3);
        $cek_data_pejabat4 = (new Spt())->cek_id_pejabat($cek_data_spt->t_idpejabat4);
        $config = [
            'format' => 'A4-P',
        ];
        $pdf = MPDF::loadview('pendaftaran.cetakbahpbphtb', [
                    'data_pemda' => $data_pemda,
                    'dataspt' => $cek_data_spt,
                    'pejabat1' => $cek_data_pejabat1,
                    'pejabat2' => $cek_data_pejabat2,
                    'pejabat3' => $cek_data_pejabat3,
                    'pejabat4' => $cek_data_pejabat4,
                        ], $config);

        return $pdf->stream();
    }
    
    public function detail(Request $request) {
        $detail = Spt::where('t_idspt', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }
    
    public function cancel(Request $request) {
        Spt::where('t_idspt', '=', $request->query('id'))->update([
            'isdeleted' => 1,
            't_idoperator_deleted' => Auth::id()
        ]);
    }
    
    public function destroy(Request $request) {
        Spt::where('t_idspt', '=', $request->query('id'))->delete();
        Filesyarat::where('t_idspt', '=', $request->query('id'))->delete();
        Filefotoobjek::where('t_idspt', '=', $request->query('id'))->delete();
        ValidasiBerkas::where('t_idspt', '=', $request->query('id'))->delete();
        ValidasiKabid::where('t_idspt', '=', $request->query('id'))->delete();
        PembayaranBphtb::where('t_idspt', '=', $request->query('id'))->delete();
    }

}
