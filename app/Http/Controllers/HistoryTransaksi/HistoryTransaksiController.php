<?php
namespace App\Http\Controllers\HistoryTransaksi;
use App\Http\Controllers\Controller;
use App\Models\Spt\Spt;
use Illuminate\Http\Request;

class HistoryTransaksiController extends Controller {

    public function index() 
    {
        $view = view(
            'history-transaksi.index', [
                  'hasilcari' => NULL
              ]
          );
        return $view;
    }

    public function cari_history_transaksi(Request $request)
    {
        $carinik = $request->carihistorytransaksinik;
        $carinib = $request->carihistorytransaksinib;
        $view = NULL;

        // jika ada nik cari pake nik
        if (!is_null($carinik))
        {
            $result =  (new Spt())->cari_history_transaksi($carinik);
            $view = view(
                'history-transaksi.index', [
                    'hasilcari' => $result
                  ]
              );
        }
        // jika ada nib cari pake nib
        if (!is_null($carinib))
        {
            $result =  (new Spt())->cari_history_transaksi($carinib);
            $view = view(
                'history-transaksi.index', [
                    'hasilcari' => $result
                  ]
              );
        }
        return $view;
    }
    
    public function cek_detail_history_transaksi(Request $request){
        $detail_history_transaksi = (new Spt())->cek_detail_history_transaksi($request['id']);
        return response()->json(
            $detail_history_transaksi,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }
}
?>