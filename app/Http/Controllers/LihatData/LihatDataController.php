<?php

namespace App\Http\Controllers\LihatData;

use App\Http\Controllers\Controller;
use App\Models\Spt\Spt;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class LihatDataController extends Controller {

    //
    public function lihatdata($id) {
        $cek_data_spt = (new Spt())->cek_uuid_spt($id);
        $cek_persyaratan = (new Spt())->cek_all_data_syarat_dan_file_upload($cek_data_spt->t_idspt, $cek_data_spt->t_idjenistransaksi);
        $data_statusberkas = (new Spt())->cekdata_data_status_berkas();
        $data_statuskabid = (new Spt())->cekdata_data_status_kabid();
        $data_nik_bersama = (new Spt())->cek_data_nik($cek_data_spt->t_idspt);
        $data_fotoobjek = (new Spt())->cek_foto_objek($cek_data_spt->t_idspt);
        $all_session = Auth::user();
        $data_pejabat = (new Spt())->cekdata_pejabat_all();
        $view = view(
                'lihatdata.lihatdata',
                [
                    'dataspt' => $cek_data_spt,
                    'nik_bersama' => $data_nik_bersama,
                    'cek_persyaratan' => $cek_persyaratan,
                    'data_statusberkas' => $data_statusberkas,
                    'data_statuskabid' => $data_statuskabid,
                    'data_fotoobjek' => $data_fotoobjek,
                    'all_session' => $all_session,
                    'data_pejabat' => $data_pejabat,
                ]
        );

        return $view;
    }

    public function validasi($id) {
        $ar_spt = (new Spt())->cek_uuid_spt($id);
        $bgValidasi = ($ar_spt->t_tglvalidasikaban) ? '#e2fce2' : '#ffebe9';
        $colorValidasi = ($ar_spt->t_tglvalidasikaban) ? 'text-green' : 'text-red';
        $iconValidasi = ($ar_spt->t_tglvalidasikaban) ? 'fa-check' : 'fa-close';
        $statusValidasi = ($ar_spt->t_tglvalidasikaban) ? 'TERVALIDASI' : 'BELUM DIVALIDASI';
        $tglValidasi = ($ar_spt->t_tglvalidasikaban) ? 'Tanggal '. date('d-m-Y H:i:s', strtotime($ar_spt->t_tglvalidasikaban)) : '';
        $view = view('lihatdata.validasi', [
            'dataspt' => $ar_spt,
            'bgValidasi' => $bgValidasi,
            'statusValidasi' => $statusValidasi,
            'colorValidasi' => $colorValidasi,
            'iconValidasi' => $iconValidasi,
            'tglValidasi' => $tglValidasi
        ]);

        return $view;
    }

}
