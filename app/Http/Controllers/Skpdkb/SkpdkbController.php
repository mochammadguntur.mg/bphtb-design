<?php

namespace App\Http\Controllers\Skpdkb;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Skpdkb\SkpdkbRequest;
use App\Models\Skpdkb\Skpdkb;
use App\Models\Spt\Spt;
use App\Models\Setting\Pemda;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Skpdkb\SkpdkbExport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use MPDF;

class SkpdkbController extends Controller
{
    public function index()
    {
        return view('skpdkb.index');
    }

    public function datagrid(Request $request)
    {
        $response = Skpdkb::select(
            't_spt.t_kohirspt',
            't_skpdkb.t_idskpdkb',
            't_skpdkb.t_nourut_kodebayar',
            't_tglpenetapan',
            't_nama_pembeli',
            't_skpdkb.t_nop_sppt',
            't_jmlh_totalskpdkb',
            't_kodebayar_skpdkb',
            't_tglpembayaran_skpdkb'
        )
            ->leftJoin('t_pembayaran_skpdkb', 't_pembayaran_skpdkb.t_idskpdkb', '=', 't_skpdkb.t_idskpdkb')
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_skpdkb.t_idspt');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_idskpdkb');
        }

        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['noskpdkb'] != null) {
            $response = $response->where('t_nourut_kodebayar', 'like', "%" . $request['filter']['noskpdkb'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_skpdkb', 'ilike', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['status'] != null) {
            $response = ($request['filter']['status'] == 1) ? $response->whereNotNull('t_tglpembayaran_skpdkb') : $response->whereNull('t_tglpembayaran_skpdkb');
        }
        if ($request['filter']['tglskpdkb'] != null) {
            $date = explode(' - ', $request['filter']['tglskpdkb']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpenetapan', [$startDate, $endDate]);
        }
        if ($request['filter']['tglbayar'] != null) {
            $date = explode(' - ', $request['filter']['tglbayar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpembayaran_skpdkb', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'kohirspt' => $v['t_kohirspt'],
                'noskpdkb' => $v['t_nourut_kodebayar'],
                'tglskpdkb' => ($v['t_tglpenetapan']) ? Carbon::parse($v['t_tglpenetapan'])->format('d-m-Y') : '-',
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop' => $v['t_nop_sppt'],
                'jmlhpajakskpdkb' => number_format($v['t_jmlh_totalskpdkb'], 0, ',', '.'),
                'kodebayar' => $v['t_kodebayar_skpdkb'],
                'status' => ($v['t_tglpembayaran_skpdkb']) ? '<span class="btn btn-xs btn-success"><i class="fa fa-money"></i> LUNAS</span>' : '<span class="btn btn-xs btn-danger"><i class="fa fa-money"></i> BELUM</span>',
                'tglbayar' => ($v['t_tglpembayaran_skpdkb']) ? Carbon::parse($v['t_tglpembayaran_skpdkb'])->format('d-m-Y H:i:s') : '-',
                'actionList' => [
                    [
                        'actionName' => 'print',
                        'actionUrl' => 'javascript:printSk(' . $v['t_idskpdkb'] . ')',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['t_idskpdkb'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function ketetapan()
    {
        return view('skpdkb.ketetapan');
    }

    public function datagrid_ketetapan(Request $request)
    {
        $response = Spt::select(
            't_spt.t_idspt',
            't_kohirspt',
            't_nama_pembeli',
            't_spt.t_nop_sppt',
            't_jmlhbayar_pokok',
            't_tglpembayaran_pokok',
            't_kodebayar_bphtb'
        )
            ->leftJoin('t_pembayaran_bphtb', 't_pembayaran_bphtb.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('t_skpdkb', 't_skpdkb.t_idspt', '=', 't_spt.t_idspt')
            ->whereNotNull('t_tglpembayaran_pokok')
            ->whereNull('t_tglpenetapan');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_idspt');
        }

        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['tglpembayaran'] != null) {
            $date = explode(' - ', $request['filter']['tglpembayaran']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpembayaran_pokok', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop' => $v['t_nop_sppt'],
                'jmlhbayar' => number_format($v['t_jmlhbayar_pokok'], 0, ',', '.'),
                'tglpembayaran' => ($v['t_tglpembayaran_pokok']) ? date('d-m-Y', strtotime($v['t_tglpembayaran_pokok'])) : '-',
                'kodebayar' => $v['t_kodebayar_bphtb'],
                'actionList' => [
                    [
                        'actionName' => 'tetapkan',
                        'actionUrl' => 'skpdkb/' . $v['t_idspt'] . '/create',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create($id)
    {
        $permohonan = Spt::select()
            ->leftJoin('t_pembayaran_bphtb', 't_pembayaran_bphtb.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
            ->leftJoin('s_jenisdoktanah', 's_jenisdoktanah.s_iddoktanah', '=', 't_spt.t_idjenisdoktanah')
            ->leftJoin('s_jenishaktanah', 's_jenishaktanah.s_idhaktanah', '=', 't_spt.t_idjenishaktanah')
            ->where('t_spt.t_idspt', $id)->get();

        return view('skpdkb.create', [
            'data' => $permohonan[0]
        ]);
    }

    public function store(SkpdkbRequest $skpdkbRequest, Skpdkb $skpdkb)
    {
        $attr = $skpdkbRequest->all();
        $pemda = Pemda::select()->first()->get()[0];
        $noskpdkb = $skpdkb->whereYear('t_tglpenetapan', date('Y'))->max('t_nourut_kodebayar') + 1;

        if ($attr['t_jmlhpajakskpdkb'] > 0) {
            $tgljatuhtempo = date('d-m-Y', strtotime("+30 days"));
            $denda = hitungDenda(str_replace(".", "", $attr['t_jmlhpajakskpdkb']), $tgljatuhtempo);
            $insert = $skpdkb->create([
                't_idspt' => $attr['t_idspt'],
                't_nourut_kodebayar' => $noskpdkb,
                't_tglpenetapan' => Carbon::now(),
                't_tglby_system' => Carbon::now(),
                't_tgljatuhtempo_skpdkb' => Carbon::createFromFormat('d-m-Y', $tgljatuhtempo)->format('Y-m-d'),
                't_nop_sppt' => $attr['t_nop_sppt'],
                't_tahun_sppt' => $attr['t_tahun_sppt'],
                't_luastanah_skpdkb' => str_replace(".", "", $attr['t_luastanah']),
                't_njoptanah_skpdkb' => str_replace(".", "", $attr['t_njoptanah']),
                't_totalnjoptanah_skpdkb' => str_replace(".", "", $attr['t_totalnjoptanah']),
                't_luasbangunan_skpdkb' => str_replace(".", "", $attr['t_luasbangunan']),
                't_njopbangunan_skpdkb' => str_replace(".", "", $attr['t_njopbangunan']),
                't_totalnjopbangunan_skpdkb' => str_replace(".", "", $attr['t_totalnjopbangunan']),
                't_grandtotalnjop_skpdkb' => str_replace(".", "", $attr['t_grandtotalnjop']),
                't_nilaitransaksispt_skpdkb' => str_replace(".", "", $attr['t_nilaitransaksispt']),
                't_npop_bphtb_skpdkb' => str_replace(".", "", $attr['t_npop_bphtb']),
                't_npoptkp_bphtb_skpdkb' => str_replace(".", "", $attr['t_npoptkp_bphtb']),
                't_npopkp_bphtb_skpdkb' => str_replace(".", "", $attr['t_npopkp_bphtb']),
                't_jmlh_blndenda' => $denda['jmlbulan'],
                't_persenbphtb_skpdkb' => $attr['t_persenbphtb'],
                't_jmlh_dendaskpdkb' => $denda['jmldenda'],
                't_nilai_bayar_sblumnya' => str_replace(".", "", $attr['t_jmlhbayar_pokok']),
                't_nilai_pokok_skpdkb' => str_replace(".", "", $attr['t_jmlhpajakskpdkb']),
                't_jmlh_totalskpdkb' => str_replace(".", "", $attr['t_jmlhpajakskpdkb']) + $denda['jmldenda'],
                't_kodebayar_skpdkb' => substr($pemda->s_kodekabkot,1,1) . '02' . date('y') . str_pad($noskpdkb, 5, "0", STR_PAD_LEFT),//$pemda->s_kodeprovinsi . 
                't_idjenisketetapan' => 2,
                't_tglbuat_kodebyar' => Carbon::now(),
                't_iduser_buat' => Auth::user()->id,
            ]);

            if ($insert) {
                session()->flash('success', 'Data Ketetapan SKPDKB Berhasil Disimpan.');
            } else {
                session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
            }
        } else {
            session()->flash('error', 'Ketetapan Gagal Disimpan, karena bukan Ketetapan SKPDKB!');
        }

        return redirect('skpdkb');
    }

    public function destroy(Request $request)
    {
        Skpdkb::where('t_idskpdkb', '=', $request->query('id'))->delete();
    }

    public function detail(Request $request)
    {
        $detail = Skpdkb::where('t_idskpdkb', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function cetaksurat($id)
    {
        $pemda = Pemda::select()->first()->get()[0];
        $data = Skpdkb::select()
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_skpdkb.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_pembayaran_bphtb.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
            ->where('t_idskpdkb', '=', $id)->get()[0];

        $terbilang = terbilang($data->t_jmlh_totalskpdkb);
        $barcode = \SimpleSoftwareIO\QrCode\Facades\QrCode::size(100)->generate($data->t_kodebayar_skpdkb);

        $config = [
            'format' => 'legal-P', // Landscape
            // 'margin_top' => 0
        ];

        $pdf = MPDF::loadview('skpdkb.exports.cetaksurat', [
            'pemda' => $pemda,
            'data' => $data,
            'barcode' => $barcode,
            'terbilang' => $terbilang,
            'denda' => hitungDenda($data->t_nilai_pokok_skpdkb, $data->t_tgljatuhtempo_skpdkb)
        ], $config);
        return $pdf->stream();
    }

    public function export_xls(Request $request)
    {
        return Excel::download(
            new SkpdkbExport(
                $request->query('noskpdkb'),
                $request->query('tglskpdkb'),
                $request->query('nop'),
                $request->query('nama_pembeli'),
                $request->query('kodebayar'),
                $request->query('status'),
                $request->query('tglbayar'),
                $request->query('created_at')
            ),
            'skpdkb.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request)
    {
        $sheet = Excel::download(
            new SkpdkbExport(
                $request->query('noskpdkb'),
                $request->query('tglskpdkb'),
                $request->query('nop'),
                $request->query('nama_pembeli'),
                $request->query('kodebayar'),
                $request->query('status'),
                $request->query('tglbayar'),
                $request->query('created_at')
            ),
            'skpdkb.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }
}
