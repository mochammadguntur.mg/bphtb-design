<?php

namespace App\Http\Controllers;

use App\Models\Pembayaran\Pembayaran;
use App\Models\Pembayaran\PembayaranBphtb;
use Illuminate\Http\Request;
use Auth;
use App\Models\Spt\Spt;
use App\Models\Setting\Pemda;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $data_pemda = Pemda::orderBy('updated_at', 'desc')->first();
        $all_session = Auth::user();
        $jml_data_pendaftaran = (new Spt())->cek_jumlah_pendaftaran_sspd($all_session);
        $data_pendaftaran_sspd_terbaru = (new Spt())->cek_jumlah_pendaftaran_sspd_terbaru($all_session);
        $jml_data_berkas = (new Spt())->cek_jumlah_validasi_berkas($all_session);
        $jml_data_kabid = (new Spt())->cek_jumlah_validasi_kabid($all_session);
        $jml_data_pembayaran = (new Spt())->cek_jumlah_pembayaran($all_session);
        $jml_data_ajb = (new Spt())->cek_jumlah_ajb($all_session);
        $jml_data_lapor_bulanan = (new Spt())->cek_jumlah_lapor_bulanan($all_session);
        $jml_data_skpdkb = (new Spt())->cek_jumlah_skpdkb($all_session);
        $jmlreal_jan = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '01' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_feb = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '02' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_mar = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '03' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_apr = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '04' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_mei = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '05' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_jun = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '06' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_jul = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '07' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_agus = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '08' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_sept = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '09' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_okt = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '10' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_nov = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '11' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();
        $jmlreal_des = PembayaranBphtb::select(DB::raw('sum(t_jmlhbayar_total) as jumlah'))->whereRaw("EXTRACT(MONTH from t_tglpembayaran_pokok) = '12' and EXTRACT(YEAR from t_tglpembayaran_pokok) = '".date('Y')."'")->first();

        return view('home', [
            'data_pemda' => $data_pemda,
            'jml_data_pendaftaran' => $jml_data_pendaftaran->jml_spt,
            'data_pendaftaran_sspd_terbaru' => $data_pendaftaran_sspd_terbaru,
            'jml_data_berkas' => $jml_data_berkas->jml_spt,
            'jml_data_kabid' => $jml_data_kabid->jml_spt,
            'jml_data_pembayaran' => $jml_data_pembayaran->jml_spt,
            'jml_data_ajb' => $jml_data_ajb->jml_spt,
            'jml_data_lapor_bulanan' => $jml_data_lapor_bulanan->jml_spt,
            'jml_data_skpdkb' => $jml_data_skpdkb->jml_skpdkb,
            'grafik_jmlreali_jan' => $jmlreal_jan->jumlah,
            'grafik_jmlreali_feb' => $jmlreal_feb->jumlah,
            'grafik_jmlreali_mar' => $jmlreal_mar->jumlah,
            'grafik_jmlreali_apr' => $jmlreal_apr->jumlah,
            'grafik_jmlreali_mei' => $jmlreal_mei->jumlah,
            'grafik_jmlreali_jun' => $jmlreal_jun->jumlah,
            'grafik_jmlreali_jul' => $jmlreal_jul->jumlah,
            'grafik_jmlreali_agus' => $jmlreal_agus->jumlah,
            'grafik_jmlreali_sept' => $jmlreal_sept->jumlah,
            'grafik_jmlreali_okt' => $jmlreal_okt->jumlah,
            'grafik_jmlreali_nov' => $jmlreal_nov->jumlah,
            'grafik_jmlreali_des' => $jmlreal_des->jumlah
        ]);
    }
}
