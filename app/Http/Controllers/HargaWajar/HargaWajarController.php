<?php

namespace App\Http\Controllers\HargaWajar;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Spt\Spt;
use App\Models\Setting\Acuan;
use App\Models\Setting\AcuanTanah;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class HargaWajarController extends Controller {

    public function index() {
        return view('harga-wajar.index');
    }

    public function caridata(Request $request) {
        // dd($request);
        $NOP = $request->query('nop');
        $KD_PROV = substr($NOP, 0, 2);
        $KD_DATI2 = substr($NOP, 3, 2);
        $KD_KEC = substr($NOP, 6, 3);
        $KD_KEL = substr($NOP, 10, 3);
        $KD_BLOK = substr($NOP, 14, 3);
        $dataTransaksi = Spt::select()
                        ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
                        ->leftJoin('t_pembayaran_bphtb', 't_pembayaran_bphtb.t_idspt', '=', 't_spt.t_idspt')
                        ->where('t_nop_sppt', 'like', substr($NOP, 0, 17) . "%")
                        ->whereYear('t_tgldaftar_spt', '>=', date('Y') - 3)
                        ->whereNotNull('t_tglpembayaran_pokok')
                        ->orderBy('t_njoptanah', 'DESC')
                        ->orderBy('t_nilai_bphtb_fix', 'DESC')->get();
        $dataAcuan = AcuanTanah::select()
                        ->where('s_kd_propinsi', 'like', $KD_PROV)
                        ->where('s_kd_dati2', 'like', $KD_DATI2)
                        ->where('s_kd_kecamatan', 'like', $KD_KEC)
                        ->where('s_kd_kelurahan', 'like', $KD_KEL)
                        ->where('s_kd_blok', 'like', $KD_BLOK)
                        // ->where('id_jenistanah', $dataTransaksi[0][])
                        ->get();
// dd($dataAcuan);
        $response = '';
        $maxTransaksi = 0;
        if ($dataTransaksi->count() == 0) {
            $response .= '<tr><td colspan="11" align="center">Tidak Ada Data!</td></tr>';
        }else{
            if ($dataTransaksi[0]['t_luastanah'] != 0) {
                $maxTransaksi = ($dataTransaksi[0]['t_nilaitransaksispt'] - $dataTransaksi[0]['t_totalnjopbangunan']) / $dataTransaksi[0]['t_luastanah'];
            }
        }
        $counter = 1;
        foreach ($dataTransaksi as $v) {
            $response .= '<tr>';
            $response .= '<td>' . $counter++ . '</td>';
            $response .= '<td>' . $v->t_nop_sppt . '</td>';
            $response .= '<td>' . $v->t_nama_pembeli . '</td>';
            $response .= '<td class="text-center">' . $v->t_periodespt . '</td>';
            $response .= '<td>' . $v->s_namajenistransaksi . '</td>';
            $response .= '<td class="text-right">' . number_format($v->t_luastanah, 0, ',', '.') . '</td>';
            $response .= '<td class="text-right">' . number_format($v->t_totalnjoptanah, 0, ',', '.') . '</td>';
            $response .= '<td class="text-right">' . number_format($v->t_luasbangunan, 0, ',', '.') . '</td>';
            $response .= '<td class="text-right">' . number_format($v->t_totalnjopbangunan, 0, ',', '.') . '</td>';
            $response .= '<td class="text-right">' . number_format($v->t_nilaitransaksispt, 0, ',', '.') . '</td>';
            $response .= '<td class="text-right">' . number_format(($v->t_nilaitransaksispt - $v->t_totalnjopbangunan) / $v->t_luastanah, 0, ',', '.') . '</td>';
            $response .= '</tr>';
        }
        $acuan = '<table style="font-size:12pt;">';
        // $acuan .= '<tr><td>Harga Transaksi (<b class="text-orange">MIN</b>) </td><td>: Rp. </td><td class="text-right">'. number_format($minHistory,0, ',','.').',-</td></tr>';
        // $acuan .= '<tr><td>Harga Transaksi (<b class="text-blue">RATA<sup>2</sup></b>) </td><td>: Rp. </td><td class="text-right">'. number_format($avgHistory,0, ',','.').',-</td></tr>';
        $acuan .= '<tr><td>Harga Transaksi (<b class="text-red">MAX</b>) </td><td>: Rp. </td><td class="text-right">' . number_format($maxTransaksi, 0, ',', '.') . ',-</td></tr>';
        $acuan .= '<tr><td colspan="3"><br></td></tr>';
        $acuan .= '<tr><td>Harga Acuan </td><td>: Rp. </td><td class="text-right">' . number_format(@$dataAcuan[0]['s_permetertanah'], 0, ',', '.') . ',-</td></tr>';
        $acuan .= '</table>';

        // $map = [];

        $res = [
            'data' => [
                'response' => $response,
                'acuan' => $acuan,
                'map' => $dataTransaksi
            ]
        ];

        return response()->json(
                        $res,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

}
