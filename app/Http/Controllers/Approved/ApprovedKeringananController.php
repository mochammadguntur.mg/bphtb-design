<?php

namespace App\Http\Controllers\Approved;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Approved\ApprovedKeringananRequest;
use App\Models\Pelayanan\PelayananKeringanan;
use App\Models\Spt\Spt;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Approved\ApprovedKeringananExport;
use App\Models\Setting\Pemda;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use PDF;

class ApprovedKeringananController extends Controller
{
    public function index(){
        return view('approved-keringanan.index', [
            'permohonan' => PelayananKeringanan::whereNull('t_tglpersetujuan')->count(),
            'approved' => PelayananKeringanan::whereNotNull('t_tglpersetujuan')->where('t_idstatus_disetujui',1)->count(),
            'rejected' => PelayananKeringanan::whereNotNull('t_tglpersetujuan')->where('t_idstatus_disetujui',2)->count()
        ]);
    }

    public function datagrid(Request $request){
        $response = PelayananKeringanan::select(
                't_idkeringanan',
                't_nokeringanan',
                't_tglpengajuan',
                't_kohirspt',
                't_nama_pembeli',
                't_tglpersetujuan',
                't_idstatus_disetujui',
                't_pelayanan_keringanan.created_at')
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_keringanan.t_idspt')
            ->whereNotNull('t_tglpersetujuan');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idkeringanan');
        }

        if ($request['filter']['nokeringanan'] != null) {
            $response = $response->where('t_nokeringanan', 'like', "%" . $request['filter']['nokeringanan'] . "%");
        }
        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['tglpengajuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpengajuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpengajuan', [$startDate, $endDate]);
        }
        if ($request['filter']['tglpersetujuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpersetujuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpersetujuan', [$startDate, $endDate]);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $statusPersetujuan = [1=>'<strong class="text-green">Disetujui</strong>',
                                2=>'<strong class="text-red">Ditolak</strong>'];
            $status = (($v['t_tglpersetujuan'])?$statusPersetujuan[$v['t_idstatus_disetujui']] : '<strong class="text-green">Baru</strong>');
            $dataArr[] = [
                'nokeringanan' => $v['t_nokeringanan'],
                'tglpengajuan' => ($v['t_tglpengajuan']) ? Carbon::parse($v['t_tglpengajuan'])->format('d-m-Y') : '-',
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'tglpersetujuan' => ($v['t_tglpersetujuan']) ? Carbon::parse($v['t_tglpersetujuan'])->format('d-m-Y') : '-',
                'status' => $status,
                'created_at' => Carbon::parse($v['created_at'])->format('d-m-Y H:i:s'),
                'actionList' => [
                    [
                        'actionName' => 'print',
                        'actionUrl' => 'javascript:printSk(' . $v['t_idkeringanan'] . ')',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['t_idkeringanan'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function permohonan(){
        return view('approved-keringanan.permohonan');
    }

    public function datagrid_pelayanan(Request $request){
        $response = PelayananKeringanan::select()
            ->select(
                't_idkeringanan',
                't_nokeringanan',
                't_tglpengajuan',
                't_kohirspt',
                't_nama_pembeli',
                't_nop_sppt',
                't_tglpersetujuan',
                't_pelayanan_keringanan.created_at')
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_keringanan.t_idspt')
            ->whereNull('t_tglpersetujuan');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idkeringanan');
        }

        if ($request['filter']['nokeringanan'] != null) {
            $response = $response->where('t_nokeringanan', 'like', "%" . $request['filter']['nokeringanan'] . "%");
        }
        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tglpengajuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpengajuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpengajuan', [$startDate, $endDate]);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'nokeringanan' => $v['t_nokeringanan'],
                'tglpengajuan' => ($v['t_tglpengajuan']) ? date('d-m-Y', strtotime($v['t_tglpengajuan'])) : '-',
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop' => $v['t_nop_sppt'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'pilih',
                        'actionUrl' => $v['t_idkeringanan'] . '/create',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request){
        $detail = PelayananKeringanan::where('t_idkeringanan', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create($keringanan){
        $permohonan = PelayananKeringanan::select()
        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_keringanan.t_idspt')
        ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
        ->leftJoin('s_jenisdoktanah', 's_jenisdoktanah.s_iddoktanah', '=', 't_spt.t_idjenisdoktanah')
        ->leftJoin('s_jenishaktanah', 's_jenishaktanah.s_idhaktanah', '=', 't_spt.t_idjenishaktanah')
        ->where('t_idkeringanan', $keringanan)->get();
        return view('approved-keringanan.create', [
            'data' => $permohonan[0]
        ]);
    }

    public function store(ApprovedKeringananRequest $approvedKeringananRequest, PelayananKeringanan $pelayananKeringanan){
        $attr = $approvedKeringananRequest->all();

        $update = $pelayananKeringanan->where('t_idkeringanan',$attr['t_idkeringanan'])
        ->update([
            't_tglpersetujuan' => Carbon::createFromFormat('d-m-Y',$attr['t_tglpersetujuan'])->format('Y-m-d H:i:s'),
            't_nosk_keringanan' => $attr['t_nosk_keringanan'],
            't_keterangan_disetujui' => $attr['t_keterangan_disetujui'],
            't_idoperator' => Auth::user()->id,
            't_idstatus_disetujui' => $attr['t_idstatus_disetujui'],
            't_jmlhpotongan' => str_replace(".", "", $attr['t_jmlhpotongan_disetujui']),
            't_persentase_disetujui' => str_replace(".", "", $attr['t_persentase_disetujui']),
            't_jmlhpotongan_disetujui' => str_replace(".", "", $attr['t_jmlhpotongan_disetujui']),
            't_jmlh_spt_sebenarnya' => str_replace(".", "", $attr['t_jmlh_spt_sebenarnya']),
            't_jmlh_spt_hasilpot' => str_replace(".", "", $attr['t_jmlh_spt_hasilpot'])
        ]);

        if($update){
            if($attr['t_idstatus_disetujui'] == 1){
                session()->flash('success', 'Data Persetujuan Keringanan Disetujui.');
            }else{
                session()->flash('success', 'Data Persetujuan Keringanan Ditolak.');
            }
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('approved-keringanan');
    }

    public function caridata(Request $request){
        $detail = PelayananKeringanan::select()
        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_keringanan.t_idspt')
        ->where('t_idkeringanan', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function destroy(Request $request){
        PelayananKeringanan::where('t_idkeringanan', '=', $request->query('id'))
        ->update([
            't_tglpersetujuan' => null,
            't_nosk_keringanan' => 0,
            't_keterangan_disetujui' => null,
            't_idoperator' => Auth::user()->id,
            't_idstatus_disetujui' => null,
            't_jmlhpotongan' => null,
            't_persentase_disetujui' => null,
            't_jmlhpotongan_disetujui' => null,
            't_jmlh_spt_sebenarnya' => null,
            't_jmlh_spt_hasilpot' => null
        ]);
    }

    public function hitung(Request $request){
        $potongan = str_replace(".", "", $request->query('key')) * str_replace(".", "", $request->query('id')) / 100;
        $hasil = str_replace(".", "", $request->query('key')) - $potongan;

        $response = [
            'data' => [
                'potongan' => number_format($potongan,0,',','.'),
                'hasil' => number_format($hasil,0,',','.')
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function cetak_surat(Request $request){
        $pemda = Pemda::select()->first()->get();
        $permohonan = PelayananKeringanan::select()
        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_keringanan.t_idspt')
        ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
        ->leftJoin('s_jenisdoktanah', 's_jenisdoktanah.s_iddoktanah', '=', 't_spt.t_idjenisdoktanah')
        ->leftJoin('s_jenishaktanah', 's_jenishaktanah.s_idhaktanah', '=', 't_spt.t_idjenishaktanah')
        ->where('t_idkeringanan', $request->query('id'))->get();
        $pdf = PDF::loadView('approved-keringanan.exports.cetak-surat', [
            'data' => $permohonan[0],
            'pemda' => $pemda[0]
        ]);

        return $pdf->stream();
    }

    public function export_xls(Request $request){
        return Excel::download(
            new ApprovedKeringananExport($request->query('t_nokeringanan'),
            $request->query('t_tglpengajuan'),
            $request->query('t_kohirspt'),
            $request->query('t_nama_pembeli'),
            $request->query('t_tglpersetujuan'),
            $request->query('created_at')),
            'approved_keringanan.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request){
        $sheet = Excel::download(
            new ApprovedKeringananExport($request->query('t_nokeringanan'),
            $request->query('t_tglpengajuan'),
            $request->query('t_kohirspt'),
            $request->query('t_nama_pembeli'),
            $request->query('t_tglpersetujuan'),
            $request->query('created_at')),
            'approved_keringanan.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }
}