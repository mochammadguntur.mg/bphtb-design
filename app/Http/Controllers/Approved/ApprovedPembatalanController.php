<?php

namespace App\Http\Controllers\Approved;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Approved\ApprovedPembatalanRequest;
use App\Models\Pelayanan\PelayananPembatalan;
use App\Models\Spt\Spt;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Approved\ApprovedPembatalanExport;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ApprovedPembatalanController extends Controller
{
    public function index(){
        return view('approved-pembatalan.index', [
            'permohonan' => PelayananPembatalan::whereNull('t_tglpersetujuan')->count(),
            'approved' => PelayananPembatalan::whereNotNull('t_tglpersetujuan')->where('t_idstatus_disetujui',1)->count(),
            'rejected' => PelayananPembatalan::whereNotNull('t_tglpersetujuan')->where('t_idstatus_disetujui',2)->count()
        ]);
    }

    public function datagrid(Request $request){
        $response = PelayananPembatalan::select(
                't_idpembatalan',
                't_nopembatalan',
                't_tglpengajuan',
                't_kohirspt',
                't_nama_pembeli',
                't_tglpersetujuan',
                't_idstatus_disetujui',
                't_pelayanan_pembatalan.created_at')
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_pembatalan.t_idspt')
            ->whereNotNull('t_tglpersetujuan');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idpembatalan');
        }

        if ($request['filter']['nopembatalan'] != null) {
            $response = $response->where('t_nopembatalan', 'like', "%" . $request['filter']['nopembatalan'] . "%");
        }
        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['tglpengajuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpengajuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpengajuan', [$startDate, $endDate]);
        }
        if ($request['filter']['tglpersetujuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpersetujuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpersetujuan', [$startDate, $endDate]);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $statusPersetujuan = [1=>'<strong class="text-green">Disetujui</strong>',
                                2=>'<strong class="text-red">Ditolak</strong>'];
            $status = (($v['t_tglpersetujuan'])?$statusPersetujuan[$v['t_idstatus_disetujui']] : '<strong class="text-green">Baru</strong>');
            $dataArr[] = [
                'nopembatalan' => $v['t_nopembatalan'],
                'tglpengajuan' => ($v['t_tglpengajuan']) ? Carbon::parse($v['t_tglpengajuan'])->format('d-m-Y') : '-',
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'tglpersetujuan' => ($v['t_tglpersetujuan']) ? Carbon::parse($v['t_tglpersetujuan'])->format('d-m-Y') : '-',
                'status' => $status,
                'created_at' => Carbon::parse($v['created_at'])->format('d-m-Y H:i:s'),
                'actionList' => [
                    [
                        'actionName' => 'print',
                        'actionUrl' => 'javascript:printSk(' . $v['t_idpembatalan'] . ')',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['t_idpembatalan'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function permohonan(){
        return view('approved-pembatalan.permohonan');
    }

    public function datagrid_pelayanan(Request $request){
        $response = PelayananPembatalan::select()
            ->select(
                't_idpembatalan',
                't_nopembatalan',
                't_tglpengajuan',
                't_kohirspt',
                't_nama_pembeli',
                't_nop_sppt',
                't_tglpersetujuan',
                't_pelayanan_pembatalan.created_at')
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_pembatalan.t_idspt')
            ->whereNull('t_tglpersetujuan');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idpembatalan');
        }

        if ($request['filter']['nopembatalan'] != null) {
            $response = $response->where('t_nopembatalan', 'like', "%" . $request['filter']['nopembatalan'] . "%");
        }
        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tglpengajuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpengajuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpengajuan', [$startDate, $endDate]);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'nopembatalan' => $v['t_nopembatalan'],
                'tglpengajuan' => ($v['t_tglpengajuan']) ? date('d-m-Y', strtotime($v['t_tglpengajuan'])) : '-',
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop' => $v['t_nop_sppt'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'pilih',
                        'actionUrl' => $v['t_idpembatalan'] . '/create',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request){
        $detail = PelayananPembatalan::where('t_idpembatalan', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create($pembatalan){
        $permohonan = PelayananPembatalan::select()
        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_pembatalan.t_idspt')
        ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
        ->leftJoin('s_jenisdoktanah', 's_jenisdoktanah.s_iddoktanah', '=', 't_spt.t_idjenisdoktanah')
        ->leftJoin('s_jenishaktanah', 's_jenishaktanah.s_idhaktanah', '=', 't_spt.t_idjenishaktanah')
        ->where('t_idpembatalan', $pembatalan)->get();
        return view('approved-pembatalan.create', [
            'pembatalan' => new PelayananPembatalan(),
            'data' => $permohonan[0]
        ]);
    }

    public function store(ApprovedPembatalanRequest $approvedPembatalanRequest, PelayananPembatalan $pelayananpembatalan){
        $attr = $approvedPembatalanRequest->all();

        $update = $pelayananpembatalan->where('t_idpembatalan',$attr['t_idpembatalan'])
        ->update([
            't_tglpersetujuan' => Carbon::createFromFormat('d-m-Y',$attr['t_tglpersetujuan'])->format('Y-m-d H:i:s'),
            't_nosk_pembatalan' => $attr['t_nosk_pembatalan'],
            't_keterangan_disetujui' => $attr['t_keterangan_disetujui'],
            't_idoperator' => Auth::user()->id,
            't_idstatus_disetujui' => $attr['t_idstatus_disetujui']
        ]);

        $idspt = PelayananPembatalan::select('t_idspt')->where('t_idpembatalan', '=', $attr['t_idpembatalan'])->get();

        if($update){
            if($attr['t_idstatus_disetujui'] == 1){
                Spt::where('t_idspt', $idspt[0]['t_idspt'])
                ->update([
                    'isdeleted' => 1,
                    't_idoperator_deleted' => Auth::user()->id
                ]);
                session()->flash('success', 'Data Persetujuan Pembatalan Disetujui.');
            }else{
                session()->flash('success', 'Data Persetujuan Pembatalan Ditolak.');
            }
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('approved-pembatalan');
    }

    public function caridata(Request $request){
        $detail = PelayananPembatalan::select()
        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_pembatalan.t_idspt')
        ->where('t_idpembatalan', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function destroy(Request $request){
        PelayananPembatalan::where('t_idpembatalan', '=', $request->query('id'))
        ->update([
            't_tglpersetujuan' => null,
            't_nosk_pembatalan' => 0,
            't_keterangan_disetujui' => null,
            't_idoperator' => null,
            't_idstatus_disetujui' => null
        ]);

        $idspt = PelayananPembatalan::select('t_idspt')->where('t_idpembatalan', '=', $request->query('id'))->get();
        Spt::where('t_idspt', '=', $idspt[0]['t_idspt'])
        ->update([
            'isdeleted' => null,
            't_idoperator_deleted' => null
        ]);
    }

    public function export_xls(Request $request){
        return Excel::download(
            new ApprovedPembatalanExport($request->query('t_nopembatalan'),
            $request->query('t_tglpengajuan'),
            $request->query('t_kohirspt'),
            $request->query('t_nama_pembeli'),
            $request->query('t_tglpersetujuan'),
            $request->query('created_at')),
            'approved_pembatalan.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request){
        $sheet = Excel::download(
            new ApprovedPembatalanExport($request->query('t_nopembatalan'),
            $request->query('t_tglpengajuan'),
            $request->query('t_kohirspt'),
            $request->query('t_nama_pembeli'),
            $request->query('t_tglpersetujuan'),
            $request->query('created_at')),
            'approved_pembatalan.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }

}