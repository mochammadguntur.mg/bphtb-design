<?php

namespace App\Http\Controllers\Approved;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Approved\ApprovedAngsuranRequest;
use App\Models\Pelayanan\PelayananAngsuran;
use App\Models\Spt\Spt;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Approved\ApprovedAngsuranExport;
use App\Models\Pelayanan\PelayananKeringanan;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ApprovedAngsuranController extends Controller
{
    public function index(){
        return view('approved-angsuran.index', [
            'permohonan' => PelayananAngsuran::whereNull('t_tglpersetujuan')->count(),
            'approved' => PelayananAngsuran::whereNotNull('t_tglpersetujuan')->where('t_idstatus_disetujui',1)->count(),
            'rejected' => PelayananAngsuran::whereNotNull('t_tglpersetujuan')->where('t_idstatus_disetujui',2)->count()
        ]);
    }

    public function datagrid(Request $request){
        $response = PelayananAngsuran::select(
                't_idangsuran',
                't_noangsuran',
                't_tglpengajuan',
                't_kohirspt',
                't_nama_pembeli',
                't_tglpersetujuan',
                't_idstatus_disetujui',
                't_pelayanan_angsuran.created_at')
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_angsuran.t_idspt')
            ->whereNotNull('t_tglpersetujuan');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idangsuran');
        }

        if ($request['filter']['noangsuran'] != null) {
            $response = $response->where('t_noangsuran', 'like', "%" . $request['filter']['noangsuran'] . "%");
        }
        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['tglpengajuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpengajuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpengajuan', [$startDate, $endDate]);
        }
        if ($request['filter']['tglpersetujuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpersetujuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpersetujuan', [$startDate, $endDate]);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $statusPersetujuan = [1=>'<strong class="text-green">Disetujui</strong>',
                                2=>'<strong class="text-red">Ditolak</strong>'];
            $status = (($v['t_tglpersetujuan'])?$statusPersetujuan[$v['t_idstatus_disetujui']] : '<strong class="text-green">Baru</strong>');
            $dataArr[] = [
                'noangsuran' => $v['t_noangsuran'],
                'tglpengajuan' => ($v['t_tglpengajuan']) ? Carbon::parse($v['t_tglpengajuan'])->format('d-m-Y') : '-',
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'tglpersetujuan' => ($v['t_tglpersetujuan']) ? Carbon::parse($v['t_tglpersetujuan'])->format('d-m-Y') : '-',
                'status' => $status,
                'created_at' => Carbon::parse($v['created_at'])->format('d-m-Y H:i:s'),
                'actionList' => [
                    [
                        'actionName' => 'print',
                        'actionUrl' => 'javascript:printSk(' . $v['t_idangsuran'] . ')',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['t_idangsuran'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function permohonan(){
        return view('approved-angsuran.permohonan');
    }


    public function datagrid_pelayanan(Request $request){
        $response = PelayananAngsuran::select()
            ->select(
                't_idangsuran',
                't_noangsuran',
                't_tglpengajuan',
                't_kohirspt',
                't_nama_pembeli',
                't_nop_sppt',
                't_tglpersetujuan',
                't_pelayanan_angsuran.created_at')
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_angsuran.t_idspt')
            ->whereNull('t_tglpersetujuan');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idangsuran');
        }

        if ($request['filter']['noangsuran'] != null) {
            $response = $response->where('t_noangsuran', 'like', "%" . $request['filter']['noangsuran'] . "%");
        }
        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tglpengajuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpengajuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpengajuan', [$startDate, $endDate]);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'noangsuran' => $v['t_noangsuran'],
                'tglpengajuan' => ($v['t_tglpengajuan']) ? date('d-m-Y', strtotime($v['t_tglpengajuan'])) : '-',
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop' => $v['t_nop_sppt'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'pilih',
                        'actionUrl' => $v['t_idangsuran'] . '/create',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request){
        $detail = PelayananAngsuran::where('t_idangsuran', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create($angsuran){
        $permohonan = PelayananAngsuran::select()
        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_angsuran.t_idspt')
        ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
        ->leftJoin('s_jenisdoktanah', 's_jenisdoktanah.s_iddoktanah', '=', 't_spt.t_idjenisdoktanah')
        ->leftJoin('s_jenishaktanah', 's_jenishaktanah.s_idhaktanah', '=', 't_spt.t_idjenishaktanah')
        ->where('t_idangsuran', $angsuran)->get();
        return view('approved-angsuran.create', [
            'angsuran' => new PelayananAngsuran(),
            'data' => $permohonan[0]
        ]);
    }

    public function store(ApprovedAngsuranRequest $approvedAngsuranRequest, PelayananAngsuran $pelayananAngsuran){
        $attr = $approvedAngsuranRequest->all();

        $update = $pelayananAngsuran->where('t_idangsuran',$attr['t_idangsuran'])
        ->update([
            't_tglpersetujuan' => $attr['t_tglpersetujuan'],
            't_nosk_angsuran' => $attr['t_nosk_angsuran'],
            't_keterangan_disetujui' => $attr['t_keterangan_disetujui'],
            't_idoperator' => Auth::user()->id,
            't_idstatus_disetujui' => $attr['t_idstatus_disetujui']
        ]);

        if($update){
            if($attr['t_idstatus_disetujui'] == 1){
                session()->flash('success', 'Data Persetujuan Angsuran Disetujui.');
            }else{
                session()->flash('success', 'Data Persetujuan Angsuran Ditolak.');
            }
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('approved-angsuran');
    }

    public function caridata(Request $request){
        $detail = PelayananAngsuran::select()
        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_angsuran.t_idspt')
        ->where('t_idangsuran', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function destroy(Request $request){
        PelayananAngsuran::where('t_idangsuran', '=', $request->query('id'))
        ->update([
            't_tglpersetujuan' => null,
            't_nosk_angsuran' => 0,
            't_keterangan_disetujui' => null,
            't_idoperator' => Auth::user()->id,
            't_idstatus_disetujui' => null
        ]);
    }

    public function rinciangsuran(Request $request){
        $response = '';
        $response .= '<table class="table table-hover table-striped table-bordered">
            <thead>
                <tr>
                    <th colspan="6">RINCIAN ANGSURAN</th>
                </tr>
                <tr class="bg-primary">
                    <td class="text-center">Angsuran Ke-</td>
                    <td class="text-center">Rencana Tgl. Pembayaran Angsuran</td>
                    <td class="text-center">Jumlah Angsuran (Rp.)</td>
                    <td class="text-center">Jumlah Bulan Terlambat</td>
                    <td class="text-center">Bunga (Rp.)</td>
                    <td class="text-center">Total (Rp.)</td>
                </tr>
            </thead>
            <tbody>';
            $k = 1;
            for ($i = 0; $request->query('id') > $i; $i++) {

                $tgl = date('d-m-Y', strtotime($request->query('page'). "+$i months"));
                $jadwalbayar = strtotime($tgl);
                $tgl1 = date('d-m-Y', strtotime($request->query('page'). "+1 months"));
                $jatuhtempo = strtotime($tgl1);

                $year1 = date('Y', $jatuhtempo);
                $year2 = date('Y', $jadwalbayar);

                $month1 = date('m', $jatuhtempo);
                $month2 = date('m', $jadwalbayar);

                $day1 = date('d', $jatuhtempo);
                $day2 = date('d', $jadwalbayar);
                if ($day1 < $day2) {
                    $tambahanbulan = 1;
                } else {
                    $tambahanbulan = 0;
                }

                $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
                if ($jmlbulan > 24) {
                    $jmlbulan = 24;
                } elseif ($jmlbulan <= 0) {
                    $jmlbulan = 0;
                }
                $response .= '<tr>
                    <td class="text-center"><label>'.$k.'</label>
                    <input type="hidden" class="form-control" name="t_idangsuran[]" id="t_idangsuran"' . $k . '">
                    <input type="hidden" class="form-control" name="t_angsuranke[]" id="t_angsuranke"' . $k . '" value="' . $k . '" readonly="">
                    </td>
                    <td><input type="text" class="form-control" name="t_tgljadwalangsuran[]" id="t_tgljadwalangsuran'.$i.'" value="' . $tgl . '" readonly=""></td>
                    <td><input type="text" class="form-control" name="t_nominalangsuran[]" id="t_nominalangsuran' . $k . '" value="0"</td>
                    <td><input type="text" class="form-control" name="t_jmlhbln[]" id="t_jmlhbln' . $k . '" readonly="true" value="'.$jmlbulan.'" onblur="HitungRincianAngsuran()"></td>
                    <td><input type="text" class="form-control" name="t_bunga[]" id="t_bunga' . $k . '" readonly="true" onblur="HitungRincianAngsuran()"></td>
                    <td><input type="text" class="form-control" name="t_total[]" id="t_total' . $k . '" readonly="true" onblur="HitungRincianAngsuran()"></td>
                </tr>';
                $k++;
            }
        $response .= '</tbody>
            </table>';
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function export_xls(Request $request){
        return Excel::download(
            new ApprovedAngsuranExport($request->query('t_noangsuran'),
            $request->query('t_tglpengajuan'),
            $request->query('t_kohirspt'),
            $request->query('t_nama_pembeli'),
            $request->query('t_tglpersetujuan'),
            $request->query('created_at')),
            'approved_angsuran.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request){
        $sheet = Excel::download(
            new ApprovedAngsuranExport($request->query('t_noangsuran'),
            $request->query('t_tglpengajuan'),
            $request->query('t_kohirspt'),
            $request->query('t_nama_pembeli'),
            $request->query('t_tglpersetujuan'),
            $request->query('created_at')),
            'approved_angsuran.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }
}