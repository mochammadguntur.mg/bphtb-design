<?php

namespace App\Http\Controllers\Skpdkbt;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Skpdkbt\SkpdkbtRequest;
use App\Models\Skpdkbt\Skpdkbt;
use App\Models\Spt\Spt;
use App\Models\Setting\Pemda;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use MPDF;

class SkpdkbtController extends Controller
{
    public function index(){
        return view('skpdkbt.index');
    }

    public function datagrid(Request $request){
        $response = Skpdkbt::select(
            't_spt.t_kohirspt',
            't_skpdkbt.t_idskpdkbt',
            't_skpdkbt.t_nourut_kodebayar',
            't_tglpenetapan',
            't_nama_pembeli',
            't_skpdkbt.t_nop_sppt',
            't_jmlh_totalskpdkbt',
            't_kodebayar_skpdkbt',
            't_tglpembayaran_skpdkbt')
            ->leftJoin('t_pembayaran_skpdkbt', 't_pembayaran_skpdkbt.t_idskpdkbt', '=', 't_skpdkbt.t_idskpdkbt')
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_skpdkbt.t_idspt');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idskpdkbt');
        }

        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['noskpdkbt'] != null) {
            $response = $response->where('t_nourut_kodebayar', 'like', "%" . $request['filter']['noskpdkbt'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_skpdkbt', 'ilike', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['status'] != null) {
            $response = ($request['filter']['status'] == 1) ? $response->whereNotNull('t_tglpembayaran_skpdkbt') : $response->whereNull('t_tglpembayaran_skpdkbt');
        }
        if ($request['filter']['tglskpdkbt'] != null) {
            $date = explode(' - ', $request['filter']['tglskpdkbt']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpenetapan', [$startDate, $endDate]);
        }
        if ($request['filter']['tglbayar'] != null) {
            $date = explode(' - ', $request['filter']['tglbayar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpembayaran_skpdkbt', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'kohirspt' => $v['t_kohirspt'],
                'noskpdkbt' => $v['t_nourut_kodebayar'],
                'tglskpdkbt' => ($v['t_tglpenetapan']) ? Carbon::parse($v['t_tglpenetapan'])->format('d-m-Y') : '-',
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop' => $v['t_nop_sppt'],
                'jmlhpajakskpdkbt' => number_format($v['t_jmlh_totalskpdkbt'], 0, ',','.'),
                'kodebayar' => $v['t_kodebayar_skpdkbt'],
                'status' => ($v['t_tglpembayaran_skpdkbt']) ? '<span class="btn btn-xs btn-success"><i class="fa fa-money"></i> LUNAS</span>' : '<span class="btn btn-xs btn-danger"><i class="fa fa-money"></i> BELUM</span>',
                'tglbayar' => ($v['t_tglpembayaran_skpdkbt']) ? Carbon::parse($v['t_tglpembayaran_skpdkbt'])->format('d-m-Y H:i:s') : '-',
                'actionList' => [
                    [
                        'actionName' => 'print',
                        'actionUrl' => 'javascript:printSk(' . $v['t_idskpdkbt'] . ')',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['t_idskpdkbt'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function ketetapan(){
        return view('skpdkbt.ketetapan');
    }


    public function datagrid_ketetapan(Request $request){
        $response = Spt::select(
            't_spt.t_idspt',
            't_kohirspt',
            't_nama_pembeli',
            't_skpdkb.t_nop_sppt',
            't_jmlhbayar_skpdkb',
            't_tglpembayaran_skpdkb',
            't_kodebayar_skpdkb'
            )
            ->leftJoin('t_skpdkb', 't_skpdkb.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('t_pembayaran_skpdkb', 't_pembayaran_skpdkb.t_idspt', '=', 't_spt.t_idspt')
            ->leftJoin('t_skpdkbt', 't_skpdkbt.t_idskpdkb', '=', 't_skpdkb.t_idskpdkb')
            ->whereNotNull('t_tglpembayaran_skpdkb')
            ->whereNull('t_idskpdkbt');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idspt');
        }

        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_skpdkb', 'ilike', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['tglpembayaran'] != null) {
            $date = explode(' - ', $request['filter']['tglpembayaran']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpembayaran_skpdkb', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop' => $v['t_nop_sppt'],
                'jmlhbayar' => number_format($v['t_jmlhbayar_skpdkb'],0,',','.'),
                'tglpembayaran' => ($v['t_tglpembayaran_skpdkb']) ? date('d-m-Y', strtotime($v['t_tglpembayaran_skpdkb'])) : '-',
                'kodebayar' => $v['t_kodebayar_skpdkb'],
                'actionList' => [
                    [
                        'actionName' => 'tetapkan',
                        'actionUrl' => 'skpdkbt/'.$v['t_idspt'] . '/create',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create($id){
        $permohonan = Spt::select()
        ->leftJoin('t_pembayaran_bphtb', 't_pembayaran_bphtb.t_idspt', '=', 't_spt.t_idspt')
        ->leftJoin('t_skpdkb', 't_skpdkb.t_idspt', '=', 't_spt.t_idspt')
        ->leftJoin('t_pembayaran_skpdkb', 't_pembayaran_skpdkb.t_idskpdkb', '=', 't_skpdkb.t_idskpdkb')
        ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
        ->leftJoin('s_jenisdoktanah', 's_jenisdoktanah.s_iddoktanah', '=', 't_spt.t_idjenisdoktanah')
        ->leftJoin('s_jenishaktanah', 's_jenishaktanah.s_idhaktanah', '=', 't_spt.t_idjenishaktanah')
        ->where('t_spt.t_idspt', $id)->get();
        return view('skpdkbt.create', [
            'data' => $permohonan[0]
        ]);
    }

    public function store(SkpdkbtRequest $skpdkbtRequest, Skpdkbt $skpdkbt){
        $attr = $skpdkbtRequest->all();
        $pemda = Pemda::select()->first()->get()[0];
        // dd($attr);
        $noskpdkbt = $skpdkbt->whereYear('t_tglpenetapan', date('Y'))->max('t_nourut_kodebayar')+1;

        if ($attr['t_jmlhpajakskpdkbt'] > 0) {
            $tgljatuhtempo = date('d-m-Y', strtotime("+30 days"));
            $denda = hitungDenda(str_replace(".", "", $attr['t_jmlhpajakskpdkbt']), $tgljatuhtempo);
            $insert = $skpdkbt->create([
                't_idspt' => $attr['t_idspt'],
                't_idskpdkb' => $attr['t_idskpdkb'],
                't_nourut_kodebayar' => $noskpdkbt,
                't_tglpenetapan' => Carbon::now(),
                't_tglby_system' => Carbon::now(),
                't_tgljatuhtempo_skpdkbt' => Carbon::createFromFormat('d-m-Y',$tgljatuhtempo)->format('Y-m-d'),
                't_nop_sppt' => $attr['t_nop_sppt'],
                't_tahun_sppt' => $attr['t_tahun_sppt'],
                't_luastanah_skpdkbt' => str_replace(".", "", $attr['t_luastanah']),
                't_njoptanah_skpdkbt' => str_replace(".", "", $attr['t_njoptanah']),
                't_totalnjoptanah_skpdkbt' => str_replace(".", "", $attr['t_totalnjoptanah']),
                't_luasbangunan_skpdkbt' => str_replace(".", "", $attr['t_luasbangunan']),
                't_njopbangunan_skpdkbt' => str_replace(".", "", $attr['t_njopbangunan']),
                't_totalnjopbangunan_skpdkbt' => str_replace(".", "", $attr['t_totalnjopbangunan']),
                't_grandtotalnjop_skpdkbt' => str_replace(".", "", $attr['t_grandtotalnjop']),
                't_nilaitransaksispt_skpdkbt' => str_replace(".", "", $attr['t_nilaitransaksispt']),
                't_npop_bphtb_skpdkbt' => str_replace(".", "", $attr['t_npop_bphtb']),
                't_npoptkp_bphtb_skpdkbt' => str_replace(".", "", $attr['t_npoptkp_bphtb']),
                't_npopkp_bphtb_skpdkbt' => str_replace(".", "", $attr['t_npopkp_bphtb']),
                't_jmlh_blndenda' => $denda['jmlbulan'],
                't_persenbphtb_skpdkbt' => $attr['t_persenbphtb'],
                't_jmlh_dendaskpdkbt' => $denda['jmldenda'],
                't_nilai_bayar_sblumnya' => str_replace(".", "", $attr['t_jmlhpajakskpdkb']),
                't_nilai_pokok_skpdkbt' => str_replace(".", "", $attr['t_jmlhpajakskpdkbt']),
                't_jmlh_totalskpdkbt' => str_replace(".", "", $attr['t_jmlhpajakskpdkbt'])+$denda['jmldenda'],
                't_kodebayar_skpdkbt' => substr($pemda->s_kodekabkot,1,1).'03'.date('y').str_pad($noskpdkbt, 5, "0", STR_PAD_LEFT),//$pemda->s_kodeprovinsi
                't_idjenisketetapan' => 3,
                't_tglbuat_kodebyar' => Carbon::now(),
                't_iduser_buat' => Auth::user()->id,
            ]);

            if($insert){
                session()->flash('success', 'Data Ketetapan SKPDKBT Berhasil Disimpan.');
            }else{
                session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
            }

        }else{
            session()->flash('error', 'Ketetapan Gagal Disimpan, karena bukan Ketetapan SKPDKBT!');
        }

        return redirect('skpdkbt');
    }

    public function destroy(Request $request){
        Skpdkbt::where('t_idskpdkbt', '=', $request->query('id'))->delete();
    }

    public function detail(Request $request){
        $detail = Skpdkbt::where('t_idskpdkbt', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function cetaksurat($id){
        $pemda = Pemda::select()->first()->get()[0];
        $data = Skpdkbt::select()
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_skpdkbt.t_idspt')
            // ->leftJoin('t_pembayaran_bphtb', 't_pembayaran_bphtb.t_idspt', '=', 't_spt.t_idspt')
            // ->leftJoin('t_skpdkb', 't_skpdkb.t_idspt', '=', 't_spt.t_idspt')
            // ->leftJoin('t_pembayaran_skpdkb', 't_pembayaran_skpdkb.t_idskpdkb', '=', 't_skpdkb.t_idskpdkb')
            ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
            ->leftJoin('s_jenisdoktanah', 's_jenisdoktanah.s_iddoktanah', '=', 't_spt.t_idjenisdoktanah')
            ->leftJoin('s_jenishaktanah', 's_jenishaktanah.s_idhaktanah', '=', 't_spt.t_idjenishaktanah')
            ->where('t_idskpdkbt', '=', $id)->get()[0];

        $terbilang = terbilang($data->t_jmlh_totalskpdkbt);
        $barcode = \SimpleSoftwareIO\QrCode\Facades\QrCode::size(100)->generate($data->t_kodebayar_skpdkbt);

        $config = [
            'format' => 'legal-P', // Landscape
            // 'margin_top' => 0
        ];

        $pdf = MPDF::loadview('skpdkbt.exports.cetaksurat', [
                'pemda' => $pemda,
                'data' => $data,
                'barcode' => $barcode,
                'terbilang' => $terbilang,
                'denda' => hitungDenda($data->t_nilai_pokok_skpdkbt, $data->t_tgljatuhtempo_skpdkbt)
                ], $config);
        return $pdf->stream();
    }

}