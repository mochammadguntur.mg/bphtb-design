<?php

namespace App\Http\Controllers\Sismiop;

use App\Http\Controllers\Controller;
use App\Http\Requests\Sismiop\DatSubjekPajakRequest;
use App\Http\Requests\Sismiop\MutasiRequest;
use App\Models\Pbb\DatSubjekPajak;
use App\Models\Pelayanan\PelayananMutasi;
use Illuminate\Http\Request;

class SismiopController extends Controller {

    public function index() {
        return view('sismiop.index');
    }

    public function datagridPengajuan(Request $request) {
        $response = PelayananMutasi::select()
                ->select(
                        't_idmutasi',
                        't_nomutasi',
                        't_tglpengajuan',
                        't_kohirspt',
                        't_nama_pembeli',
                        't_pelayanan_mutasi.created_at',
                        't_luastanah_sismiop',
                        't_luastanah'
                )
                ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_mutasi.t_idspt');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_idmutasi');
        }
        $response = $response->whereNull('t_tglpersetujuan');
        if ($request['filter']['nomutasi'] != null) {
            $response = $response->where('t_nomutasi', 'like', "%" . $request['filter']['nomutasi'] . "%");
        }
        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['tglpengajuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpengajuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpengajuan', [$startDate, $endDate]);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            if ($v['t_luastanah_sismiop'] == $v['t_luastanah']) {
                $act = [
                    [
                        'actionName' => 'mutasi-penuh',
                        'actionUrl' => 'sismiop/' . $v['t_idmutasi'] . '/form-mutasi-penuh',
                        'actionActive' => true
                    ]
                ];
            } elseif ($v['t_luastanah_sismiop'] > $v['t_luastanah']) {
                $act = [
                    [
                        'actionName' => 'mutasi-sebagian',
                        'actionUrl' => 'sismiop/' . $v['t_idmutasi'] . '/form-mutasi-sebagian',
                        'actionActive' => true
                    ]
                ];
            }
            $dataArr[] = [
                'nomutasi' => $v['t_nomutasi'],
                'tglpengajuan' => ($v['t_tglpengajuan']) ? date('d-m-Y', strtotime($v['t_tglpengajuan'])) : '-',
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                't_luastanah' => $v['t_luastanah'],
                't_luastanah_sismiop' => $v['t_luastanah_sismiop'],
                'status' => $v['t_luastanah'] == $v['t_luastanah_sismiop'] ? '<span style="color:red">Mutasi Penuh</span>' : '<span style="color:blue">Mutasi Sebagian</span>',
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => $act
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function datagridMutasi(Request $request) {
        $response = PelayananMutasi::select()
                ->select(
                        't_idmutasi',
                        't_nosk_mutasi',
                        't_tglpersetujuan',
                        't_kohirspt',
                        't_nama_pembeli',
                        't_pelayanan_mutasi.created_at',
                        't_idstatus_disetujui'
                )
                ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_mutasi.t_idspt');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_idmutasi');
        }

        $response = $response->whereNotNull('t_tglpersetujuan');
        if ($request['filter']['nosk'] != null) {
            $response = $response->where('t_nosk_mutasi', 'like', "%" . $request['filter']['nosk'] . "%");
        }
        if ($request['filter']['tglpersetujuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpersetujuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpersetujuan', [$startDate, $endDate]);
        }
        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                't_nosk_mutasi' => $v['t_nosk_mutasi'],
                'tglpersetujuan' => ($v['t_tglpersetujuan']) ? date('d-m-Y', strtotime($v['t_tglpersetujuan'])) : '-',
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'status' => ($v['t_idstatus_disetujui'] == 1) ? '<strong class="text-blue">Disetujui</strong>' : '<strong class="text-red">Ditolak</strong>',
                'actionList' => [
                    [
                        'actionName' => 'print',
                        // 'actionUrl' => 'sismiop/' . $v['t_idmutasi'] . '/form-mutasi',
                        'actionUrl' => '#',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function formMutasi($mutasi) {
        $datamutasi = PelayananMutasi::select()
                        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_mutasi.t_idspt')
                        ->where('t_idmutasi', $mutasi)->first();
        // dd($datamutasi);
        return view('sismiop.form-mutasi', [
            'data' => $datamutasi
        ]);
    }

    public function update($mutasi, MutasiRequest $mutasiRequest, PelayananMutasi $pelayananMutasi) {
        $attr = $mutasiRequest->all();
        $update = $pelayananMutasi->where('t_idmutasi', $mutasi)
                ->update([
            't_tglpersetujuan' => $attr['t_tglpersetujuan'],
            't_nosk_mutasi' => $attr['t_nosk_mutasi'],
            't_keterangan_disetujui' => $attr['t_keterangan_disetujui'],
            't_idstatus_disetujui' => $attr['t_idstatus_disetujui'],
        ]);

        if ($update) {
            session()->flash('success', 'Data Permohonan Mutasi SPOP Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('sismiop');
    }

    public function formMutasiPenuh($mutasi) {
        $datamutasi = PelayananMutasi::select()
                        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_mutasi.t_idspt')
                        ->where('t_idmutasi', $mutasi)->first();
        // dd($datamutasi);
        return view('sismiop.form-mutasi-penuh', [
            'data' => $datamutasi
        ]);
    }

    public function updateMutasiPenuh($mutasi, MutasiRequest $mutasiRequest, DatSubjekPajakRequest $datSubjekPajakRequest, PelayananMutasi $pelayananMutasi) {
//        $attrSubjek = $datSubjekPajakRequest->all();
//        $chekSubjekExist = DatSubjekPajak::select()->where('SUBJEK_PAJAK_ID', $attrSubjek['SUBJEK_PAJAK_ID'])->get();
//        dd($chekSubjekExist);
        $attr = $mutasiRequest->all();
        $update = $pelayananMutasi->where('t_idmutasi', $mutasi)
                ->update([
            't_tglpersetujuan' => $attr['t_tglpersetujuan'],
            't_nosk_mutasi' => $attr['t_nosk_mutasi'],
            't_keterangan_disetujui' => $attr['t_keterangan_disetujui'],
            't_idstatus_disetujui' => $attr['t_idstatus_disetujui'],
        ]);

        if ($update) {
            session()->flash('success', 'Data Permohonan Mutasi SPOP Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('sismiop');
    }

    public function formMutasiSebagian($mutasi) {
        $datamutasi = PelayananMutasi::select()
                        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_mutasi.t_idspt')
                        ->where('t_idmutasi', $mutasi)->first();
        return view('sismiop.form-mutasi-sebagian', [
            'data' => $datamutasi
        ]);
    }

    public function updateMutasiSebagian($mutasi, MutasiRequest $mutasiRequest, PelayananMutasi $pelayananMutasi) {
        $attr = $mutasiRequest->all();
        $update = $pelayananMutasi->where('t_idmutasi', $mutasi)
                ->update([
            't_tglpersetujuan' => $attr['t_tglpersetujuan'],
            't_nosk_mutasi' => $attr['t_nosk_mutasi'],
            't_keterangan_disetujui' => $attr['t_keterangan_disetujui'],
            't_idstatus_disetujui' => $attr['t_idstatus_disetujui'],
        ]);

        if ($update) {
            session()->flash('success', 'Data Permohonan Mutasi SPOP Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('sismiop');
    }

}
