<?php

namespace App\Http\Controllers\CetakLaporan;

use App\Exports\CetakLaporan\AjbExport;
use App\Exports\CetakLaporan\PendaftaranExport;
use App\Exports\CetakLaporan\PenerimaanExport;
use App\Exports\CetakLaporan\RekapPenerimaanExport;
use App\Exports\CetakLaporan\SertifikatExport;
use App\Http\Controllers\Controller;
use App\Models\Setting\JenisTransaksi;
use App\Models\Setting\Kecamatan;
use App\Models\Setting\Kelurahan;
use App\Models\Setting\Notaris;
use App\Models\Setting\Pejabat;
use App\Models\Setting\Pemda;
use App\Models\Spt\Spt;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use MPDF;

class CetakLaporanController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('cetak-laporan.index', [
            'pejabat' => Pejabat::get(),
            'jenis_transaksi' => JenisTransaksi::get(),
            'kecamatan' => Kecamatan::get(),
            'ppat' => Notaris::get()
        ]);
    }

    public function combo_kelurahan(Request $request)
    {
        $kelurahan = Kelurahan::where('s_idkecamatan', $request->query('id'))->get();
        return response()->json(
            $kelurahan,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function cetak_pendaftaran_pdf(Request $request)
    {
        $response = Spt::from('t_spt as a')
            ->leftJoin('s_jenistransaksi as b', 'b.s_idjenistransaksi', '=', 'a.t_idjenistransaksi')
            ->leftJoin('s_notaris as c', 'c.s_idnotaris', '=', 'a.t_idnotaris_spt')
            ->select(
                'a.t_nama_pembeli',
                'a.t_nik_pembeli',
                'a.t_jalan_pembeli',
                'a.t_nama_penjual',
                'a.t_namakec_penjual',
                'a.t_namakel_penjual',
                'a.t_kabkota_penjual',
                'a.t_nop_sppt',
                'a.t_tahun_sppt',
                'a.t_nama_sppt',
                'a.t_jalan_sppt',
                'a.t_npop_bphtb',
                'a.t_luastanah',
                'a.t_luasbangunan',
                'b.s_namajenistransaksi',
                'c.s_namanotaris'
            )
            ->whereRaw('a.t_idpersetujuan_bphtb=1 and a.isdeleted IS NULL')
            ->orderBy('t_tgldaftar_spt');

        if (!empty($request->query('tgl_pendaftaran'))) {
            $tgl_daftar = explode(' - ', $request->query('tgl_pendaftaran'));
            $tgl_awal = Carbon::parse($tgl_daftar[0])->format('Y-m-d');
            $tgl_akhir = Carbon::parse($tgl_daftar[1])->format('Y-m-d 23:59:59');
            // $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response = $response->whereBetween('t_tgldaftar_spt', [$tgl_awal, $tgl_akhir]);
        }

        if (!empty($request->query('jenis_peralihan'))) {
            $response = $response->where('t_idjenistransaksi', $request->query('jenis_peralihan'));
        }

        if (!empty($request->query('notaris'))) {
            $response = $response->where('t_idnotaris_spt', $request->query('notaris'));
        }

        if (!empty($request->query('kecamatan'))) {
            $response = $response->where('t_kecamatan_sppt', 'ilike', '%' . $request->query('kecamatan') . '%');
        }

        if (!empty($request->query('kelurahan'))) {
            $response = $response->where('t_kelurahan_sppt', 'ilike', '%' . $request->query('kelurahan') . '%');
        }

        $response = $response->get();
        $ttd_megetahui = Pejabat::find($request->query('mengetahui'));
        $ttd_diproses = Pejabat::find($request->query('diproses'));
        $pemda = Pemda::first();

        $config = [
            'title' => 'Data Pendaftaran',
            'format' => [216, 330],
            'orientation' => 'L',
        ];

        $pdf = MPDF::loadView('cetak-laporan.exports.cetak-pendaftaran', [
            'pendaftaran' => $response,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
            'tgl_cetak' => $request->query('tgl_cetak'),
            'mengetahui' => $ttd_megetahui,
            'diproses' => $ttd_diproses,
            'pemda' => $pemda,
            'image' => true
        ], $config);

        return $pdf->stream();
    }

    public function cetak_pendaftaran_xls(Request $request)
    {
        $param = [
            'tgl_pendaftaran' => $request->query('tgl_pendaftaran'),
            'notaris' => $request->query('notaris'),
            'jenis_peralihan' => $request->query('jenis_peralihan'),
            'kecamatan' => $request->query('kecamatan'),
            'kelurahan' => $request->query('kelurahan'),
            'tgl_cetak' => $request->query('tgl_cetak'),
            'mengetahui' => $request->query('mengetahui'),
            'diproses' => $request->query('diproses')
        ];

        return Excel::download(
            new PendaftaranExport(
                $param
            ),
            'rekap-pendaftaran.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function cetak_penerimaan_pdf(Request $request)
    {
        $data_get = $request->query();
        $param = [
            'tgl_penerimaan_awal' => $request->query('tgl_penerimaan_awal'),
            'tgl_penerimaan_akhir' => $request->query('tgl_penerimaan_akhir'),
            'notaris' => $request->query('notaris'),
            'jenis_peralihan' => $request->query('jenis_peralihan'),
            'kecamatan' => $request->query('kecamatan'),
            'kelurahan' => $request->query('kelurahan'),
            'tgl_cetak' => $request->query('tgl_cetak'),
            'mengetahui' => $request->query('mengetahui'),
            'diproses' => $request->query('diproses'),
            'tipecetak' => $request->query('tipecetak')
        ];
        
        
        
        
        $response =  (new PenerimaanExport($data_get))->cetak_data_penerimaan($data_get);
        
        $ttd_megetahui = Pejabat::find($param['mengetahui']);
        $ttd_diproses = Pejabat::find($param['diproses']);
        
        $config = [
            'format' => 'A4', // Landscape
            'orientation' => 'L'
        ];
        
        $data_cetak = array(
                    'penerimaan' => $response,
                    'tgl_awal' => $param['tgl_penerimaan_awal'],
                    'tgl_akhir' => $param['tgl_penerimaan_akhir'],
                    'tgl_cetak' => $param['tgl_cetak'],
                    'mengetahui' => $ttd_megetahui,
                    'diproses' => $ttd_diproses,
                    'pemda' => Pemda::first(),
                    'tipecetak' => $param['tipecetak'],
                    'data_get' => $data_get
        );
        
        if ($param['tipecetak'] == 1){
            
            if($data_get['jeniscetak'] == 1){
                $pdf = MPDF::loadView('cetak-laporan.exports.cetak-penerimaan', $data_cetak, [], 
                                    [
                                        'title' => 'Cetak Penerimaan'.$data_get['tgl_penerimaan_awal'].' s_d '.$data_get['tgl_penerimaan_akhir'],
                                        'margin_top' => 10,
                                        'format' => 'Legal', // Landscape
                                        'orientation' => 'P'
                                    ]); //->save($pdfFilePath)
                return $pdf->stream();
            }elseif($data_get['jeniscetak'] == 2){
                return Excel::download(
                    new PenerimaanExport(
                        $data_get
                    ),
                    'rekap-penerimaan.xlsx',
                    \Maatwebsite\Excel\Excel::XLSX
                );
            }elseif($data_get['jeniscetak'] == 3){
                $view = view('cetak-laporan.exports.cetak-penerimaan', $data_cetak);
                
            }
        }else{
            if($data_get['jeniscetak'] == 1){
                $pdf = MPDF::loadView('cetak-laporan.exports.cetak-penerimaan', $data_cetak, [], [
                    'title' => 'Cetak Penerimaan',
                    'margin_top' => 10,
                    'format' => 'Legal', // Landscape
                    'orientation' => 'L'
                  ]); //->save($pdfFilePath)
                return $pdf->stream();
            }elseif($data_get['jeniscetak'] == 2){
                return Excel::download(
                    new PenerimaanExport(
                        $data_get
                    ),
                    'rekap-penerimaan.xlsx',
                    \Maatwebsite\Excel\Excel::XLSX
                );
            }elseif($data_get['jeniscetak'] == 3){
                $view = view('cetak-laporan.exports.cetak-penerimaan', $data_cetak);
                
            }    
        }
        

        /* return Excel::download(
            new PenerimaanExport(
                $param
            ),
            'rekap-penerimaan.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        ); */
    }

    public function cetak_penerimaan_xls(Request $request)
    {
        $param = [
            'tgl_penerimaan_awal' => $request->query('tgl_penerimaan_awal'),
            'tgl_penerimaan_akhir' => $request->query('tgl_penerimaan_akhir'),
            'notaris' => $request->query('notaris'),
            'jenis_peralihan' => $request->query('jenis_peralihan'),
            'kecamatan' => $request->query('kecamatan'),
            'kelurahan' => $request->query('kelurahan'),
            'tgl_cetak' => $request->query('tgl_cetak'),
            'mengetahui' => $request->query('mengetahui'),
            'diproses' => $request->query('diproses'),
            'tipecetak' => $request->query('tipecetak')
        ];

        return Excel::download(
            new PenerimaanExport(
                $param
            ),
            'rekap-penerimaan.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function cetak_ajb_pdf(Request $request)
    {
        $param = [
            'tgl_penerimaan' =>  $request->query('tgl_penerimaan'),
            'notaris' =>  $request->query('notaris'),
            'jenis_peralihan' => $request->query('jenis_peralihan'),
            'kecamatan' => $request->query('kecamatan'),
            'kelurahan' => $request->query('kelurahan'),
            'tgl_cetak' => $request->query('tgl_cetak'),
            'mengetahui' =>  $request->query('mengetahui'),
            'diproses' =>  $request->query('diproses')
        ];
        return Excel::download(
            new AjbExport(
                $param
            ),
            'rekap-ajb.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
    }

    public function cetak_ajb_xls(Request $request)
    {
        $param = [
            'tgl_penerimaan' =>  $request->query('tgl_penerimaan'),
            'notaris' =>  $request->query('notaris'),
            'jenis_peralihan' => $request->query('jenis_peralihan'),
            'kecamatan' => $request->query('kecamatan'),
            'kelurahan' => $request->query('kelurahan'),
            'tgl_cetak' => $request->query('tgl_cetak'),
            'mengetahui' =>  $request->query('mengetahui'),
            'diproses' =>  $request->query('diproses')
        ];

        return Excel::download(
            new AjbExport(
                $param
            ),
            'rekap-ajb.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function cetak_sertifikat_pdf(Request $request)
    {
        $param = [
            'tgl_sertipikat' =>   $request->query('tgl_sertipikat'),
            'kecamatan' =>   $request->query('kecamatan'),
            'kelurahan' =>   $request->query('kelurahan'),
            'tgl_cetak' =>   $request->query('tgl_cetak'),
            'mengetahui' =>   $request->query('mengetahui'),
            'diproses' =>   $request->query('diproses')
        ];

        return Excel::download(
            new SertifikatExport(
                $param
            ),
            'rekap-sertifikat.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
    }

    public function cetak_sertifikat_xls(Request $request)
    {
        $param = [
            'tgl_sertipikat' =>   $request->query('tgl_sertipikat'),
            'kecamatan' =>   $request->query('kecamatan'),
            'kelurahan' =>   $request->query('kelurahan'),
            'tgl_cetak' =>   $request->query('tgl_cetak'),
            'mengetahui' =>   $request->query('mengetahui'),
            'diproses' =>   $request->query('diproses')
        ];

        return Excel::download(
            new SertifikatExport(
                $param
            ),
            'rekap-sertifikat.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function cetak_rekap_penerimaan_pdf(Request $request)
    {
        $ttd_megetahui = Pejabat::find($request->query('mengetahui'));
        $ttd_diproses = Pejabat::find($request->query('diproses'));

        $jenis_transaksi = JenisTransaksi::get();
        $spt = Spt::groupBy('t_idjenistransaksi')
            ->leftJoin('t_pembayaran_bphtb as d', 'd.t_idspt', '=', 't_spt.t_idspt')
            ->selectRaw('t_idjenistransaksi, sum(t_nilai_bphtb_fix) as total_terima')
            ->whereNotNull('d.t_tglpembayaran_pokok');

        if (!empty($request->query('tgl_penerimaan'))) {
            // $tgl_bayar = explode(' - ', $request->query('tgl_penerimaan_awal'));
            $tgl_awal = Carbon::parse($request->query('tgl_penerimaan_awal'))->format('Y-m-d');
            $tgl_akhir = Carbon::parse($request->query('tgl_penerimaan_akhir'))->format('Y-m-d');
            $spt = $spt->whereRaw("d.t_tglpembayaran_pokok::date >= '".$tgl_awal."' and d.t_tglpembayaran_pokok::date <= '".$tgl_akhir."' ");
            //$spt = $spt->whereBetween('d.t_tglpembayaran_pokok', [$tgl_awal, $tgl_akhir]);
        }

        if (!empty($request->query('kecamatan'))) {
            $spt = $spt->where('t_kecamatan_sppt', 'ilike', '%' . $request->query('kecamatan') . '%');
        }

        if (!empty($request->query('kelurahan'))) {
            $spt = $spt->where('t_kelurahan_sppt', 'ilike', '%' . $request->query('kelurahan') . '%');
        }

        $response = [];
        $tot_terima = [];

        foreach ($spt->get() as $s) {
            $tot_terima[$s->t_idjenistransaksi] = $s->total_terima;
        }

        foreach ($jenis_transaksi as $row) {
            $response[] = [
                's_namajenistransaksi' => $row->s_namajenistransaksi,
                'total_penerimaan' => isset($tot_terima[$row->s_idjenistransaksi]) ? $tot_terima[$row->s_idjenistransaksi] : 0
            ];
        }

        $config = [
            'title' => 'Rekap Penerimaan',
            'format' => [216, 330]
        ];

        $pdf = MPDF::loadView('cetak-laporan.exports.cetak-rekap-penerimaan', [
            'penerimaan' => $response,
            'tgl_awal' => $request->query('tgl_penerimaan_awal'),
            'tgl_akhir' => $request->query('tgl_penerimaan_akhir'),
            'tgl_cetak' => $request->query('tgl_cetak'),
            'mengetahui' => $ttd_megetahui,
            'diproses' => $ttd_diproses,
            'pemda' => Pemda::first()
        ], $config);

        return $pdf->stream();
    }

    public function cetak_rekap_penerimaan_xls(Request $request)
    {
        return Excel::download(
            new RekapPenerimaanExport(
                $request->query('tgl_penerimaan_awal'),
                $request->query('tgl_penerimaan_awal'),
                $request->query('kecamatan'),
                $request->query('kelurahan'),
                $request->query('tgl_cetak'),
                $request->query('mengetahui'),
                $request->query('diproses')
            ),
            'rekap-penerimaan.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }
}
