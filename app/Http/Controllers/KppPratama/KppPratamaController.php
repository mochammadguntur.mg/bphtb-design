<?php

namespace App\Http\Controllers\KppPratama;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Spt\Spt;
use Auth;
use Illuminate\Support\Facades\DB;

class KppPratamaController extends Controller
{
    
    public $data_field = array(
                't_idspt',
                't_kohirspt',
                't_tgldaftar_spt',
                //'t_tglby_system',
                't_periodespt',
                't_idjenistransaksi',
                't_idnotaris_spt',
                't_idpersetujuan_bphtb',
                's_id_status_berkas', 
                's_id_status_kabid',
                't_id_pembayaran',
        
                't_npwpd',
                //'t_idstatus_npwpd',
                 't_idbidang_usaha',
                't_nama_pembeli',
                't_nik_pembeli',
                't_npwp_pembeli',
                't_jalan_pembeli',
                't_kabkota_pembeli',
                //'t_idkec_pembeli',
                't_namakecamatan_pembeli',
                //'t_idkel_pembeli',
                't_namakelurahan_pembeli',
                //'t_rt_pembeli',
                //'t_rw_pembeli',
                //'t_nohp_pembeli',
                //'t_email_pembeli',
                //'t_kodepos_pembeli',
        
                //'t_siup_pembeli',
                't_nib_pembeli',
                //'t_ketdomisili_pembeli',
        
                //'t_b_nama_pngjwb_pembeli',
                //'t_b_nik_pngjwb_pembeli',
                //'t_b_npwp_pngjwb_pembeli',
                //'t_b_statusjab_pngjwb_pembeli',
                //'t_b_jalan_pngjwb_pembeli',
                //'t_b_kabkota_pngjwb_pembeli',
                //'t_b_idkec_pngjwb_pembeli',
                //'t_b_namakec_pngjwb_pembeli',
                //'t_b_idkel_pngjwb_pembeli',
                //'t_b_namakel_pngjwb_pembeli',
                //'t_b_rt_pngjwb_pembeli',
                //'t_b_rw_pngjwb_pembeli',
                //'t_b_nohp_pngjwb_pembeli',
                //'t_b_email_pngjwb_pembeli',
                //'t_b_kodepos_pngjwb_pembeli',
                't_nop_sppt',
                't_tahun_sppt',
                't_nama_sppt',
                't_jalan_sppt' ,
                't_kabkota_sppt',
                't_kecamatan_sppt',
                't_kelurahan_sppt',
                //'t_rt_sppt',
                //'t_rw_sppt',
                //'t_luastanah_sismiop',
                //'t_luasbangunan_sismiop',
                //'t_njoptanah_sismiop',
                //'t_njopbangunan_sismiop',
                't_luastanah',
                //'t_njoptanah',
                //'t_totalnjoptanah',
                't_luasbangunan',
                //'t_njopbangunan',
                //'t_totalnjopbangunan',
                't_grandtotalnjop',
                't_nilaitransaksispt',
                //'t_tarif_pembagian_aphb_kali',
                //'t_tarif_pembagian_aphb_bagi' ,
                //'t_permeter_tanah',
                't_idjenisfasilitas',
                't_idjenishaktanah',
                't_idjenisdoktanah' ,
                't_idpengurangan',
                't_id_jenistanah',
                //'t_nosertifikathaktanah',
                //'t_tgldok_tanah',
                //'s_latitude',
                //'s_longitude',
                //'t_idtarifbphtb',
                //'t_persenbphtb',
                //'t_npop_bphtb',
                //'t_npoptkp_bphtb',
                //'t_npopkp_bphtb',
                't_nilai_bphtb_fix',
                //'t_nilai_bphtb_real',
                't_nama_penjual',
                't_nik_penjual',
                //'t_npwp_penjual',
                //'t_jalan_penjual',
                //'t_kabkota_penjual' ,
                //'t_idkec_penjual',
                //'t_namakec_penjual' ,
                //'t_idkel_penjual',
                //'t_namakel_penjual',
                //'t_rt_penjual',
                //'t_rw_penjual',
                //'t_nohp_penjual' ,
                //'t_email_penjual',
                //'t_kodepos_penjual' ,
        
                //'t_siup_penjual',
                //'t_nib_penjual',
                //'t_ketdomisili_penjual',
        
                //'t_b_nama_pngjwb_penjual',
                //'t_b_nik_pngjwb_penjual',
                //'t_b_npwp_pngjwb_penjual',
                //'t_b_statusjab_pngjwb_penjual',
                //'t_b_jalan_pngjwb_penjual',
                //'t_b_kabkota_pngjwb_penjual',
                //'t_b_idkec_pngjwb_penjual',
                //'t_b_namakec_pngjwb_penjual',
                //'t_b_idkel_pngjwb_penjual',
                //'t_b_namakel_pngjwb_penjual',
                //'t_b_rt_pngjwb_penjual',
                //'t_b_rw_pngjwb_penjual',
                //'t_b_nohp_pngjwb_penjual',
                //'t_b_email_pngjwb_penjual',
                //'t_b_kodepos_pngjwb_penjual',
                //'t_tglbuat_kodebyar',
                //'t_nourut_kodebayar',
                't_kodebayar_bphtb',
                //'t_tglketetapan_spt',
                //'t_tgljatuhtempo_spt',
                't_idjenisketetapan'
                 
        );

    public $data_field_alias = array(
                'Perintah',
                'No Urut',
                'Tanggal Daftar',
                //'Tanggal Sistem',
                'Tahun',
                'Jenis Transaksi',
                'Notaris',
                'Persetujuan BPHTB',
                'Validasi Berkas', 
                'Validasi Kabid',
                'Status Pembayaran',
        
                'NPWPD',
                //'ID STATUS NPWPD',
                 'Bidang Usaha',
                'Nama Pembeli',
                'NIK Pembeli',
                'NPWP Pembeli',
                'Jalan Pembeli',
                'Kab/Kota Pembeli',
                //'ID Kecamatan Pembeli',
                'Nama Kecamatan Pembeli',
                //'ID Kelurhanan Pembeli',
                'Nama Kelurahan Pembeli',
                //'RT Pembeli Pribadi',
                //'RW Pembeli Pribadi',
                //'NO Hp Pembeli Pribadi',
                //'Email Pembeli Pribadi',
                //'Kodepos Pembeli Pribadi',
        
                //'SIUP Pembeli',
                'NIB Pembeli',
                //'Ket Domisili Pembeli',
        
                //'Nama Pngjwb Pembeli Badan',
                //'NIK Pngjwb Pembeli Badan',
                //'NPWP Pngjwb Pembeli Badan',
                //'Statusjab Pngjwb Pembeli Badan',
                //'Jalan Pngjwb Pembeli Badan',
                //'Kab/kota Pngjwb Pembeli Badan',
                //'ID Kec Pngjwb Pembeli Badan',
                //'Nama Kec Pngjwb Pembeli Badan',
                //'ID Kel Pngjwb Pembeli Badan',
                //'Nama kel Pngjwb Pembeli Badan',
                //'RT pngjwb Pembeli Badan',
                //'RW pngjwb Pembeli Badan',
                //'No Hp pngjwb Pembeli Badan',
                //'Email pngjwb Pembeli Badan',
                //'Kodepos Pngjwb Pembeli Badan',
                'NOP SPPT',
                'Tahun Sppt',
                'Nama SPPT',
                'Jalan SPPT' ,
                'Kab/kota SPPT',
                'Kecamatan SPPT',
                'Kelurahan SPPT',
                //'RT SPPT',
                //'RW SPPT',
                //'Luas Tanah Sismiop',
                //'Luas Bangunan Sismiop',
                //'NJOP Tanah Sismiop',
                //'NJOP Bangunan Sismiop',
                'Luas Tanah',
                //'NJOP Tanah',
                //'Total NJOP Tanah',
                'Luas Bangunan',
                //'NJOP Bangunan',
                //'Total NJOP Bangunan',
                'Total NJOP',
                'Nilai Transaksi',
                //'Tarif pembagian APHB Kali',
                //'Tarif pembagian Aphb Bagi' ,
                //'Permeter Tanah',
                'Jenis Fasilitas',
                'Jenis Hak Tanah',
                'Jenis Dok Tanah' ,
                'Jenis Pengurangan',
                'Jenis Tanah',
                //'No Sertifikat Hak Tanah',
                //'Tgl Dok Tanah',
                //'Latitude',
                //'Longitude',
                //'ID Tarif BPHTB',
                //'Persen BPHTB',
                //'NPOP',
                //'NPOPTKP',
                //'NPOPKP',
                'Nilai Bphtb fix',
                //'Nilai Bphtb real',
                'Nama Penjual',
                'NIK Penjual',
                //'NPWP Penjual',
                //'Jalan Penjual',
                //'Kab/kota Penjual' ,
                //'ID Kec Penjual',
                //'Nama kec Penjual' ,
                //'ID Kel Penjual',
                //'Nama kel Penjual',
                //'RT Penjual',
                //'RW Penjual',
                //'No HP Penjual' ,
                //'Email Penjual',
                //'Kodepos Penjual' ,
        
                //'SIUP Penjual',
                //'NIB Penjual',
                //'Ket Domisili Penjual',
        
                //'Nama pngjwb Penjual Badan',
                //'NIK pngjwb penjual Badan',
                //'NPWP pngjwb penjual Badan',
                //'Status jab pngjwb penjual Badan',
                //'Jalan pngjwb penjual Badan',
                //'Kab/kota pngjwb penjual Badan',
                //'ID kec pngjwb penjual Badan',
                //'Nama kec pngjwb penjual Badan',
                //'ID kel pngjwb penjual Badan',
                //'Nama kel pngjwb penjual Badan',
                //'RT pngjwb penjual Badan',
                //'RW pngjwb penjual Badan',
                //'No Hp pngjwb penjual Badan',
                //'Email pngjwb penjual Badan',
                //'Kodepos pngjwb penjual Badan',
                //'Tgl Buat Kodebyar',
                //'No urut Kodebayar',
                'Kodebayar Bphtb',
                //'Tgl Ketetapan',
                //'Tgl jatuhtempo',
                'Jenis Ketetapan'
                
        );
    
    public function index()
    {
        $all_session = Auth::user();
        $data_field = $this->data_field;
        $data_field_alias = $this->data_field_alias;
        $data_all = '';
        foreach ($data_field as $v) {
            $data_all .= '#'.$v.',';
        }
        $fix_data_field = substr($data_all, 0, -1);
        
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        
        $data_statusberkas = (new Spt())->cekdata_data_status_berkas();
        $data_statuskabid = (new Spt())->cekdata_data_status_kabid();
        $data_statusbayar = (new Spt())->cekdata_data_status_bayar();
        
        $data_jenisfasilitas = (new Spt())->cekdata_jenis_fasilitas();
        $data_haktanah = (new Spt())->cekdata_jenis_haktanah();
        $data_doktanah = (new Spt())->cekdata_jenis_doktanah();
        $data_jenispengurangan = (new Spt())->cekdata_jenis_pengurangan();
        $data_jenisperumahan = (new Spt())->cekdata_jenis_perumahan_tanah();
        
        $data_jenisketetapan = (new Spt())->cekdata_jenis_ketetapan();
        
        $view = view(
                      'kpp-pratama.index', [
                            'data_field'=> $data_field, 
                            'data_field_alias' => $data_field_alias, 
                            'fix_data_field' => $fix_data_field,
                            'data_notaris' => $data_notaris,
                            'data_jenistransaksi' => $data_jenistransaksi,
                            'data_statusberkas' => $data_statusberkas,
                            'data_statuskabid' => $data_statuskabid,
                            'data_statusbayar' => $data_statusbayar,
                            'data_jenisbidangusaha' => $data_jenisbidangusaha,
                          
                            'data_jenisfasilitas' => $data_jenisfasilitas,
                            'data_haktanah' => $data_haktanah,
                            'data_doktanah' => $data_doktanah,
                            'data_jenispengurangan' => $data_jenispengurangan,
                            'data_jenisperumahan' => $data_jenisperumahan,
                          
                            'data_jenisketetapan' => $data_jenisketetapan
                        ]
                    );
        
        return $view;
    }
    
    function cek_data_ada_tidak($datacek, $cekformat = null){
        if(!empty($datacek)){
            if(!empty($cekformat)){
                $hasilcek = '<span style="align: right;">'.number_format($datacek, 0, ',', '.').'</span>';
            }else{
                $hasilcek = $datacek;
            }
        }else{
            $hasilcek = '';
        }
        
        return $hasilcek;
    }

    public function datagrid(Request $request){
        $all_session = Auth::user();
        $data_field = $this->data_field;
        
        
        $response = (new Spt())::select(
                                            't_spt.*',
                                            's_jenistransaksi.s_namajenistransaksi',
                                            's_notaris.s_namanotaris',
                                            's_jenis_bidangusaha.s_nama_bidangusaha',
                                            't_validasi_berkas.t_id_validasi_berkas','t_validasi_berkas.s_id_status_berkas',
                                            't_validasi_kabid.t_id_validasi_kabid','t_validasi_kabid.s_id_status_kabid',
                                            't_pembayaran_bphtb.t_id_pembayaran','t_pembayaran_bphtb.t_tglpembayaran_pokok',
                                            's_jenisketetapan.s_namajenisketetapan','s_jenisketetapan.s_namasingkatjenisketetapan',
                                            's_jenisfasilitas.s_namajenisfasilitas',
                                            's_jenishaktanah.s_namahaktanah',
                                            's_jenisdoktanah.s_namadoktanah',
                                            's_jenispengurangan.s_namapengurangan',
                                            's_jenistanah.nama_jenistanah'
                                            
                                        )
                            
                            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
                            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
                            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
                            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
                            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
                            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
                            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
                            ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
                            ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
                            ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
                            ->whereNotNull('t_validasi_kabid.t_id_validasi_kabid')
                            ->whereNotNull('t_pembayaran_bphtb.t_id_pembayaran')
                            ->orderBy('t_spt.t_idspt', 'desc')
                ;
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idspt');
        }
        
        foreach ($data_field as $v) {
            if($v == 't_idspt'){
                
            }else{
                if ($request['filter'][$v] != null) {
                  
                    if($v == 't_idjenistransaksi'){
                        $response = $response->where('t_idjenistransaksi', '=', $request['filter']['t_idjenistransaksi']);
                        
                    }elseif($v == 't_idnotaris_spt'){
                        $response = $response->where('t_idnotaris_spt', '=', $request['filter']['t_idnotaris_spt']);
                        
                    }elseif($v == 't_idpersetujuan_bphtb'){
                        if($request['filter']['t_idpersetujuan_bphtb'] == 1){
                            $response = $response->where('t_idpersetujuan_bphtb', '=', 1);
                        }elseif($request['filter']['t_idpersetujuan_bphtb'] == 2){
                            $response = $response->whereNull('t_idpersetujuan_bphtb');
                        }else{
                            
                        }
                    }elseif($v == 's_id_status_berkas'){
                        if($request['filter']['s_id_status_berkas'] == 1){
                            $response = $response->where('s_id_status_berkas', '=', 1);
                        }elseif($request['filter']['s_id_status_berkas'] == 2){
                            $response = $response->where('s_id_status_berkas', '=', 2);
                        }elseif($request['filter']['s_id_status_berkas'] == 3){
                            $response = $response->whereNull('s_id_status_berkas');
                        }else{
                            
                        }
                    }elseif($v == 's_id_status_kabid'){
                        if($request['filter']['s_id_status_kabid'] == 1){
                            $response = $response->where('s_id_status_kabid', '=', 1);
                        }elseif($request['filter']['s_id_status_kabid'] == 2){
                            $response = $response->where('s_id_status_kabid', '=', 2);
                        }elseif($request['filter']['s_id_status_kabid'] == 3){
                            $response = $response->whereNull('s_id_status_kabid');
                        }else{
                            
                        }
                    }elseif($v == 't_id_pembayaran'){
                        if($request['filter']['t_id_pembayaran'] == 1){
                            $response = $response->whereNotNull('t_id_pembayaran');
                        }elseif($request['filter']['t_id_pembayaran'] == 2){
                            $response = $response->whereNull('t_id_pembayaran');
                        }else{
                            
                        }
                    }else{
                
                        $response = $response->where($v, 'ilike', "%" . $request['filter'][$v] . "%");
                    }    
                    //'t_idjenisfasilitas', 't_idjenishaktanah', 't_idjenisdoktanah' ,
                }
            }
        }
        //if ($request['filter']['t_kohirspt'] != null) {
        //    $response = $response->where('t_kohirspt', 'ilike', "%" . $request['filter']['t_kohirspt'] . "%");
        //}
        
        
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        
        //var_dump($response); exit();
        $dataArr = [];
        foreach ($response as $v) {
            if(!empty($v['t_tgldaftar_spt'])){
                $tgl_daftar = $v['t_tgldaftar_spt'];
            }else{
                $tgl_daftar = '';
            }
            
            $perintah = '<div class="dropdown">
                    <button onclick="myFunction('.$v['t_idspt'].')" class="dropbtn btn-primary dropdown-toggle btn btn-xs">PERINTAH <span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown'.$v['t_idspt'].'" class="dropdown-content dropdown-menu" style="left:80px; border-color: blue;">
                            <a href="pendaftaran/' . $v['t_idspt'] . '/tahappertama" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>
                            <a href="pendaftaran/'.$v['t_idspt'].'/cetaksspdbphtb" target="_blank" class="btn btn-primary btn-block btn-xs" title="CETAK SSPD"><i class="fa fa-fw fa-print"></i> CETAK SSPD</a>
                            <a href="pendaftaran/'.$v['t_idspt'].'/cetaksuratpernyataan" target="_blank" class="btn btn-primary btn-block btn-xs" title="CETAK SURAT PERNYATAAN"><i class="fa fa-fw fa-print"></i> CETAK SURAT PERNYATAAN</a> 
                            
                                                      
                    </div>
                  </div>';
            
            if(!empty($v['s_id_status_berkas'])){
                if($v['s_id_status_berkas'] == 1){
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button>';
                }else{
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button>'; 
                }
                
            }else{
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>'; 
            }
            
            if(!empty($v['s_id_status_kabid'])){
                if($v['s_id_status_kabid'] == 1){
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button>';
                }else{
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button>'; 
                }
                
            }else{
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>'; 
            }
            
            if(!empty($v['t_tglpembayaran_pokok'])){
                $status_bayar = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> SUDAH</button>'; 
            }else{
                $status_bayar = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';  
            }
            
            if(!empty($v['t_idpersetujuan_bphtb'])){
                $status_persetujuan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button>'; 
            }else{
                $status_persetujuan = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>'; 
            }
            
            $dataArr[] = [
                //'actionList' => [
                    /* [
                        'actionName' => 'edit',
                        'actionUrl' => 'pendaftaran/' . $v['t_idspt'] . '/tahappertama',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['t_idspt'] . ')',
                        'actionActive' => true
                    ] */
                 //   ],
                
                
                
                't_idspt' => '',
                't_kohirspt' => $this->cek_data_ada_tidak($v['t_kohirspt']),
                't_tgldaftar_spt' => $this->cek_data_ada_tidak($v['t_tgldaftar_spt']),
                //'t_tglby_system' => $v['t_tglby_system'],
                't_periodespt' => $this->cek_data_ada_tidak($v['t_periodespt']),
                't_idjenistransaksi' => $this->cek_data_ada_tidak($v['s_namajenistransaksi']),
                //'t_idnotaris_spt' => $v['t_idnotaris_spt'],
                't_idnotaris_spt' => $this->cek_data_ada_tidak($v['s_namanotaris']),
                
                't_idpersetujuan_bphtb' => $status_persetujuan, 
                't_id_validasi_berkas' => $status_validasi_berkas,
                't_id_validasi_kabid' => $status_validasi_kabid,
                't_id_pembayaran' => $status_bayar,
                
                't_npwpd' => $this->cek_data_ada_tidak($v['t_npwpd']),
                //'t_idstatus_npwpd' => $v['t_idstatus_npwpd'],
                't_idbidang_usaha' => $this->cek_data_ada_tidak($v['s_nama_bidangusaha']),
                't_nama_pembeli' => $this->cek_data_ada_tidak($v['t_nama_pembeli']),
                't_nik_pembeli' => $this->cek_data_ada_tidak($v['t_nik_pembeli']),
                't_npwp_pembeli' => $this->cek_data_ada_tidak($v['t_npwp_pembeli']),
                't_jalan_pembeli' => $this->cek_data_ada_tidak($v['t_jalan_pembeli']),
                't_kabkota_pembeli' => $this->cek_data_ada_tidak($v['t_kabkota_pembeli']),
                //'t_idkec_pembeli' => $this->cek_data_ada_tidak($v['t_idkec_pembeli']),
                't_namakecamatan_pembeli' => $this->cek_data_ada_tidak($v['t_namakecamatan_pembeli']),
                //'t_idkel_pembeli' => $this->cek_data_ada_tidak($v['t_idkel_pembeli']),
                't_namakelurahan_pembeli' => $this->cek_data_ada_tidak($v['t_namakelurahan_pembeli']),
                //'t_rt_pembeli' => $this->cek_data_ada_tidak($v['t_rt_pembeli']),
                //'t_rw_pembeli' => $this->cek_data_ada_tidak($v['t_rw_pembeli']),
                //'t_nohp_pembeli' => $this->cek_data_ada_tidak($v['t_nohp_pembeli']),
                //'t_email_pembeli' => $this->cek_data_ada_tidak($v['t_email_pembeli']),
                //'t_kodepos_pembeli' => $this->cek_data_ada_tidak($v['t_kodepos_pembeli']),
                
                //'t_siup_pembeli' => $this->cek_data_ada_tidak($v['t_siup_pembeli']),
                't_nib_pembeli' => $this->cek_data_ada_tidak($v['t_nib_pembeli']),
                //'t_ketdomisili_pembeli' => $this->cek_data_ada_tidak($v['t_ketdomisili_pembeli']),
                
                //'t_b_nama_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_nama_pngjwb_pembeli']),
                //'t_b_nik_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_nik_pngjwb_pembeli']),
                //'t_b_npwp_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_npwp_pngjwb_pembeli']),
                //'t_b_statusjab_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_statusjab_pngjwb_pembeli']),
                //'t_b_jalan_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_jalan_pngjwb_pembeli']),
                //'t_b_kabkota_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_kabkota_pngjwb_pembeli']),
                //'t_b_idkec_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_idkec_pngjwb_pembeli']),
                //'t_b_namakec_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_namakec_pngjwb_pembeli']),
                //'t_b_idkel_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_idkel_pngjwb_pembeli']),
                //'t_b_namakel_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_namakel_pngjwb_pembeli']),
                //'t_b_rt_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_rt_pngjwb_pembeli']),
                //'t_b_rw_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_rw_pngjwb_pembeli']),
                //'t_b_nohp_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_nohp_pngjwb_pembeli']),
                //'t_b_email_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_email_pngjwb_pembeli']),
                //'t_b_kodepos_pngjwb_pembeli' => $this->cek_data_ada_tidak($v['t_b_kodepos_pngjwb_pembeli']),
                't_nop_sppt' => $this->cek_data_ada_tidak($v['t_nop_sppt']),
                't_tahun_sppt' => $this->cek_data_ada_tidak($v['t_tahun_sppt']),
                't_nama_sppt' => $this->cek_data_ada_tidak($v['t_nama_sppt']),
                't_jalan_sppt' => $this->cek_data_ada_tidak($v['t_jalan_sppt']),
                't_kabkota_sppt' => $this->cek_data_ada_tidak($v['t_kabkota_sppt']),
                't_kecamatan_sppt' => $this->cek_data_ada_tidak($v['t_kecamatan_sppt']),
                't_kelurahan_sppt' => $this->cek_data_ada_tidak($v['t_kelurahan_sppt']),
                //'t_rt_sppt' => $this->cek_data_ada_tidak($v['t_rt_sppt']),
                //'t_rw_sppt' => $this->cek_data_ada_tidak($v['t_rw_sppt']),
                //'t_luastanah_sismiop' => $this->cek_data_ada_tidak($v['t_luastanah_sismiop']),
                //'t_luasbangunan_sismiop' => $this->cek_data_ada_tidak($v['t_luasbangunan_sismiop']),
                //'t_njoptanah_sismiop' => $this->cek_data_ada_tidak($v['t_njoptanah_sismiop'], 1),
                //'t_njopbangunan_sismiop' => $this->cek_data_ada_tidak($v['t_njopbangunan_sismiop'], 1),
                't_luastanah' => $this->cek_data_ada_tidak($v['t_luastanah']),
                //'t_njoptanah' => $this->cek_data_ada_tidak($v['t_njoptanah'], 1),
                //'t_totalnjoptanah' => $this->cek_data_ada_tidak($v['t_totalnjoptanah'], 1),
                't_luasbangunan' => $this->cek_data_ada_tidak($v['t_luasbangunan']),
                //'t_njopbangunan' => $this->cek_data_ada_tidak($v['t_njopbangunan'], 1),
                //'t_totalnjopbangunan' => $this->cek_data_ada_tidak($v['t_totalnjopbangunan'], 1),
                't_grandtotalnjop' => $this->cek_data_ada_tidak($v['t_grandtotalnjop'], 1),
                't_nilaitransaksispt' => $this->cek_data_ada_tidak($v['t_nilaitransaksispt'], 1),
                //'t_tarif_pembagian_aphb_kali' => $this->cek_data_ada_tidak($v['t_tarif_pembagian_aphb_kali']),
                //'t_tarif_pembagian_aphb_bagi' => $this->cek_data_ada_tidak($v['t_tarif_pembagian_aphb_bagi']),
                //'t_permeter_tanah' => $this->cek_data_ada_tidak($v['t_permeter_tanah'], 1),
                't_idjenisfasilitas' => $this->cek_data_ada_tidak($v['s_namajenisfasilitas']),
                't_idjenishaktanah' => $this->cek_data_ada_tidak($v['s_namahaktanah']),
                't_idjenisdoktanah' => $this->cek_data_ada_tidak($v['s_namadoktanah']),
                't_idpengurangan' => $this->cek_data_ada_tidak($v['s_namapengurangan']),
                't_id_jenistanah' => $this->cek_data_ada_tidak($v['nama_jenistanah']),
                //'t_nosertifikathaktanah' => $this->cek_data_ada_tidak($v['t_nosertifikathaktanah']),
                //'t_tgldok_tanah' => $this->cek_data_ada_tidak($v['t_tgldok_tanah']),
                //'s_latitude' => $this->cek_data_ada_tidak($v['s_latitude']),
                //'s_longitude' => $this->cek_data_ada_tidak($v['s_longitude']),
                //'t_idtarifbphtb' => $this->cek_data_ada_tidak($v['t_idtarifbphtb']),
                //'t_persenbphtb' => $this->cek_data_ada_tidak($v['t_persenbphtb']),
                //'t_npop_bphtb' => $this->cek_data_ada_tidak($v['t_npop_bphtb'], 1),
                //'t_npoptkp_bphtb' => $this->cek_data_ada_tidak($v['t_npoptkp_bphtb'], 1),
                //'t_npopkp_bphtb' => $this->cek_data_ada_tidak($v['t_npopkp_bphtb'], 1),
                't_nilai_bphtb_fix' => $this->cek_data_ada_tidak($v['t_nilai_bphtb_fix'], 1),
                //'t_nilai_bphtb_real' => $this->cek_data_ada_tidak($v['t_nilai_bphtb_real'], 1),
                't_nama_penjual' => $this->cek_data_ada_tidak($v['t_nama_penjual']),
                't_nik_penjual' => $this->cek_data_ada_tidak($v['t_nik_penjual']),
                //'t_npwp_penjual' => $this->cek_data_ada_tidak($v['t_npwp_penjual']),
                //'t_jalan_penjual' => $this->cek_data_ada_tidak($v['t_jalan_penjual']),
                //'t_kabkota_penjual' => $this->cek_data_ada_tidak($v['t_kabkota_penjual']),
                //'t_idkec_penjual' => $this->cek_data_ada_tidak($v['t_idkec_penjual']),
                //'t_namakec_penjual' => $this->cek_data_ada_tidak($v['t_namakec_penjual']),
                //'t_idkel_penjual' => $this->cek_data_ada_tidak($v['t_idkel_penjual']),
                //'t_namakel_penjual' => $this->cek_data_ada_tidak($v['t_namakel_penjual']),
                //'t_rt_penjual' => $this->cek_data_ada_tidak($v['t_rt_penjual']),
                //'t_rw_penjual' => $this->cek_data_ada_tidak($v['t_rw_penjual']),
                //'t_nohp_penjual' => $this->cek_data_ada_tidak($v['t_nohp_penjual']),
                //'t_email_penjual' => $this->cek_data_ada_tidak($v['t_email_penjual']),
                //'t_kodepos_penjual' => $this->cek_data_ada_tidak($v['t_kodepos_penjual']),
                
                //'t_siup_penjual' => $this->cek_data_ada_tidak($v['t_siup_penjual']),
                //'t_nib_penjual' => $this->cek_data_ada_tidak($v['t_nib_penjual']),
                //'t_ketdomisili_penjual' => $this->cek_data_ada_tidak($v['t_ketdomisili_penjual']),
                
                //'t_b_nama_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_nama_pngjwb_penjual']),
                //'t_b_nik_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_nik_pngjwb_penjual']),
                //'t_b_npwp_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_npwp_pngjwb_penjual']),
                //'t_b_statusjab_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_statusjab_pngjwb_penjual']),
                //'t_b_jalan_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_jalan_pngjwb_penjual']),
                //'t_b_kabkota_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_kabkota_pngjwb_penjual']),
                //'t_b_idkec_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_idkec_pngjwb_penjual']),
                //'t_b_namakec_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_namakec_pngjwb_penjual']),
                //'t_b_idkel_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_idkel_pngjwb_penjual']),
                //'t_b_namakel_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_namakel_pngjwb_penjual']),
                //'t_b_rt_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_rt_pngjwb_penjual']),
                //'t_b_rw_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_rw_pngjwb_penjual']),
                //'t_b_nohp_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_nohp_pngjwb_penjual']),
                //'t_b_email_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_email_pngjwb_penjual']),
                //'t_b_kodepos_pngjwb_penjual' => $this->cek_data_ada_tidak($v['t_b_kodepos_pngjwb_penjual']),
                //'t_tglbuat_kodebyar' => $this->cek_data_ada_tidak($v['t_tglbuat_kodebyar']),
                //'t_nourut_kodebayar' => $this->cek_data_ada_tidak($v['t_nourut_kodebayar']),
                't_kodebayar_bphtb' => $this->cek_data_ada_tidak($v['t_kodebayar_bphtb']),
                //'t_tglketetapan_spt' => $this->cek_data_ada_tidak($v['t_tglketetapan_spt']),
                //'t_tgljatuhtempo_spt' => $this->cek_data_ada_tidak($v['t_tgljatuhtempo_spt']),
                't_idjenisketetapan' => '<center><b style="color: blue;">'.$this->cek_data_ada_tidak($v['s_namasingkatjenisketetapan']).'</b></center>'
                
                
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }
    
}
