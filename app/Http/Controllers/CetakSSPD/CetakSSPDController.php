<?php

namespace App\Http\Controllers\CetakSSPD;

use App\Http\Controllers\Controller;
use App\Models\Spt\Spt;
use App\Models\Setting\Pemda;
use App\Models\Pendaftaran\Pendaftaran;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Helpers\number_helper;
use MPDF;

class CetakSSPDController extends Controller
{
    public function cetaksspdbphtb(Spt $Spt) {
        $data_pemda = Pemda::orderBy('updated_at', 'desc')->first();
        $cek_data_spt = (new Spt())->cek_id_spt($Spt['t_idspt']);
        $terbilang_pajak = $this->terbilang($cek_data_spt->t_nilai_bphtb_fix);
        // $terbilang_pajak = (new number_helper)->terbilang($cek_data_spt->t_nilai_bphtb_fix);
        $barcode = \SimpleSoftwareIO\QrCode\Facades\QrCode::size(100)->generate($cek_data_spt->t_kodebayar_bphtb);
        $config = [
            'format' => 'A4-P',
        ];

        $pdf = MPDF::loadview('pendaftaran.cetaksspdbphtb', [
                    'data_pemda' => $data_pemda,
                    'dataspt' => $cek_data_spt,
                    'terbilang_pajak' => $terbilang_pajak,
                    'barcode' => $barcode
                        ], $config);
        
        return $pdf->stream();

    }

    public function kekata($x) {
        $x = abs($x);
        $angka = array(
            "", "Satu", "Dua", "Tiga", "Empat", "Lima",
            "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"
        );
        $temp = "";
        if ($x < 12) {
            $temp = " " . $angka[$x];
        } else if ($x < 20) {
            $temp = $this->kekata($x - 10) . " Belas";
        } else if ($x < 100) {
            $temp = $this->kekata($x / 10) . " Puluh" . $this->kekata($x % 10);
        } else if ($x < 200) {
            $temp = " Seratus" . $this->kekata($x - 100);
        } else if ($x < 1000) {
            $temp = $this->kekata($x / 100) . " Ratus" . $this->kekata($x % 100);
        } else if ($x < 2000) {
            $temp = " Seribu" . $this->kekata($x - 1000);
        } else if ($x < 1000000) {
            $temp = $this->kekata($x / 1000) . " Ribu" . $this->kekata($x % 1000);
        } else if ($x < 1000000000) {
            $temp = $this->kekata($x / 1000000) . " Juta" . $this->kekata($x % 1000000);
        } else if ($x < 1000000000000) {
            $temp = $this->kekata($x / 1000000000) . " Milyar" . $this->kekata(fmod($x, 1000000000));
        } else if ($x < 1000000000000000) {
            $temp = $this->kekata($x / 1000000000000) . " Trilyun" . $this->kekata(fmod($x, 1000000000000));
        }
        return $temp;
    }

    function terbilang($x, $style = 4) {
        if ($x < 0) {
            $hasil = "MINUS " . trim($this->kekata($x));
        } else {
            $hasil = trim($this->kekata($x));
        }
        switch ($style) {
            case 1:
                $hasil = strtoupper($hasil);
                break;
            case 2:
                $hasil = strtolower($hasil);
                break;
            case 3:
                $hasil = ucwords($hasil);
                break;
            default:
                $hasil = ucfirst($hasil);
                break;
        }
        return $hasil;
    }
}
