<?php

namespace App\Http\Controllers\Pembayaran;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Spt\Spt;
use Illuminate\Support\Facades\DB;
use Auth;
use Carbon\Carbon;
use App\Models\ValidasiKabid\ValidasiKabid;
use App\Models\Pembayaran\Pembayaran;
use App\Models\Pembayaran\PembayaranBphtb;
use App\Models\Pembayaran\PembayaranSkpdkb;
use App\Models\Skpdkb\Skpdkb;
use MPDF;

class PembayaranController extends Controller {

    public function index() {
        return view('pembayaran.index', [
            'count_belum' => (new Pembayaran())->datagridBelumBayar()->count(),
            'count_lunas' => (new Pembayaran())->datagridSudahBayar(1)->count()
        ]);
    }

    public function formtambah() {
        return view('pembayaran.formtambah', [
            'dataspt' => new Spt(),
            'data_notaris' => (new Spt())->cekdata_notaris(), // Notaris::get(),
            'data_jenistransaksi' => (new Spt())->cekdata_jenis_transaksi(),
            'data_jenisbidangusaha' => (new Spt())->cekdata_jenis_bidangusaha(),
        ]);
    }

    function cek_data_ada_tidak($datacek, $cekformat = null) {
        if (!empty($datacek)) {
            if (!empty($cekformat)) {
                $hasilcek = '<span style="align: right;">' . number_format($datacek, 0, ',', '.') . '</span>';
            } else {
                $hasilcek = $datacek;
            }
        } else {
            $hasilcek = '';
        }

        return $hasilcek;
    }

    public function datagrid(Request $request) {
        $all_session = Auth::user();
        $response = (new Pembayaran())->datagridSudahBayar();

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_tgldaftar_spt');
        }

        if ($request['filter']['nodaftar'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['nodaftar'] . "%");
        }
        if ($request['filter']['tahun'] != null) {
            $response = $response->where('t_periodespt', 'like', "%" . $request['filter']['tahun'] . "%");
        }
        if ($request['filter']['notaris'] != null) {
            $response = $response->where('t_idnotaris_spt', '=', $request['filter']['notaris']);
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['namawp'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['namawp'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['status_persetujuan'] != null) {
            $response = ((($request['filter']['status_persetujuan'] == 1)) ? $response->where('t_idpersetujuan_bphtb', '=', $request['filter']['status_persetujuan']) : $response->whereNull('t_idpersetujuan_bphtb'));
        }
        if ($request['filter']['status_validasi_berkas'] != null) {
            if ($request['filter']['status_validasi_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['status_validasi_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['status_validasi_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }
        if ($request['filter']['status_validasi_kabid'] != null) {
            if ($request['filter']['status_validasi_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['status_validasi_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['status_validasi_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }
        if ($request['filter']['status_pembayaran'] != null) {
            if ($request['filter']['status_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['status_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {

            if (!empty($v['s_id_status_berkas'])) {
                if ($v['s_id_status_berkas'] == 1) {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                } else {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button><small class="text-danger">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                }
            } else {
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['s_id_status_kabid'])) {
                if ($v['s_id_status_kabid'] == 1) {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SETUJU</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                    $cetak_sspd = '<a href="/pendaftaran/' . $v['t_idspt'] . '/cetaksspdbphtb" target="_blank" class="btn btn-primary btn-block btn-xs" title="CETAK SSPD"><i class="fa fa-fw fa-print"></i> CETAK SSPD</a>';
                } else {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK SETUJU</button><small class="text-danger">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                    $cetak_sspd = '';
                }
            } else {
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
                $cetak_sspd = '';
            }

            if (!empty($v['t_tglpembayaran_pokok'])) {
                $edit_validasi = '';
                $status_bayar = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> LUNAS</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglpembayaran_pokok'])).'</small>';
            } else {
                $edit_validasi = '<a href="' . url('validasi-kabid') . '/' . $v['t_idspt'] . '/formvalidasikabid" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
                $status_bayar = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';
            }

            if (!empty($v['t_idpersetujuan_bphtb'])) {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])).'</small>';
            } else {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>';
            }
            $lihat_data = '<a href="/lihatdata/' . $v['t_uuidspt'] . '" class="btn btn-block bg-gradient-info btn-sm" title="Lihat Data"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>';

            $hapus_pembayaran = ($all_session['s_id_hakakses'] == 1) ? '<a onclick="javascript:showDeleteDialog(' . $v['t_idspt'] . ')" class="btn btn-block bg-danger btn-sm" title="Batal Pembayaran"><i class="fa fa-fw fa-trash"></i> Batal Pembayaran</a>' : '';
            $perintah = '<div class="dropdown">
                    <button onclick="myFunction(' . $v['t_idspt'] . ')" class="dropbtn btn-primary dropdown-toggle btn btn-xs">PERINTAH <span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $v['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:80px; border-color: blue;">
                            ' . $lihat_data . '
                            ' . $edit_validasi . '
                            ' . $cetak_sspd . '
                            ' . $hapus_pembayaran . '
                    </div>
                </div>';

            $ketetapan = DB::connection('pgsql')
                    ->table('s_jenisketetapan')
                    ->select('s_namasingkatjenisketetapan')
                    ->where('s_jenisketetapan.s_idjenisketetapan', $v['jenis_ketetapan'])
                    ->first();
            $nilai_bphtb_fix = ($v['t_jmlh_totalskpdkb'] != null) ? $v['t_jmlh_totalskpdkb'] + $v['t_nilai_bphtb_fix'] : $v['t_nilai_bphtb_fix'];

            $dataArr[] = [
                't_idspt' => $perintah,
                'nodaftar' => $v['t_kohirspt'],
                'tglskpdkb' => ($v['t_tgldaftar_spt']) ? Carbon::parse($v['t_tgldaftar_spt'])->format('d-m-Y H:i:s') : '-',
                'tahun' => $v['t_periodespt'],
                'notaris' => $v['s_namanotaris'],
                'jenistransaksi' => $v['s_namajenistransaksi'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop_sppt' => $v['t_nop_sppt'],
                'nilai_bphtb_fix' => number_format($nilai_bphtb_fix, 0, ',', '.'),
                'status_persetujuan_bphtb' => $status_persetujuan,
                'status_validasi_berkas' => $status_validasi_berkas,
                'status_validasi_kabid' => $status_validasi_kabid,
                'status_pembayaran' => $status_bayar,
                'jenisketetapan' => $ketetapan->s_namasingkatjenisketetapan,
                't_kodebayar_bphtb' => !empty($v['t_kodebayar_bphtb']) ? $v['t_kodebayar_bphtb'] : '',
                'ntpd' => !empty($v['t_ntpd']) ? $v['t_ntpd'] : ''
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function datagridbelum(Request $request) {
        $response = (new Pembayaran())->datagridBelumBayar();

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_tgldaftar_spt');
        }

        if ($request['filter']['nodaftar'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['nodaftar'] . "%");
        }
        if ($request['filter']['tahun'] != null) {
            $response = $response->where('t_periodespt', 'like', "%" . $request['filter']['tahun'] . "%");
        }
        if ($request['filter']['notaris'] != null) {
            $response = $response->where('t_idnotaris_spt', '=', $request['filter']['notaris']);
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['namawp'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['namawp'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['status_persetujuan'] != null) {
            $response = ((($request['filter']['status_persetujuan'] == 1)) ? $response->where('t_idpersetujuan_bphtb', '=', $request['filter']['status_persetujuan']) : $response->whereNull('t_idpersetujuan_bphtb'));
        }
        if ($request['filter']['status_validasi_berkas'] != null) {
            if ($request['filter']['status_validasi_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['status_validasi_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['status_validasi_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }
        if ($request['filter']['status_validasi_kabid'] != null) {
            if ($request['filter']['status_validasi_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['status_validasi_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['status_validasi_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }
        if ($request['filter']['status_pembayaran'] != null) {
            if ($request['filter']['status_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['status_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];

        foreach ($response as $v) {
            if (!empty($v['s_id_status_berkas'])) {
                if ($v['s_id_status_berkas'] == 1) {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                } else {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button><small class="text-danger">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                }
            } else {
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-sudah btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['s_id_status_kabid'])) {
                if ($v['s_id_status_kabid'] == 1) {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SETUJU</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                } else {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK SETUJU</button><small class="text-danger">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                }
            } else {
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['t_tglpembayaran_pokok'])) {
                $status_bayar = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> LUNAS</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglpembayaran_pokok'])).'</small>';
            } else {
                $status_bayar = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';
            }

            if (!empty($v['t_idpersetujuan_bphtb'])) {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])).'</small>';
            } else {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>';
            }

            $perintah = '<a href="' . url('pembayaran') . '/formpembayaran/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="PILIH"><i class="fas fa-fw fa-money"></i> <strong>BAYAR</strong></a>';
            $ketetapan = DB::connection('pgsql')
                    ->table('s_jenisketetapan')
                    ->select('s_namasingkatjenisketetapan')
                    ->where('s_jenisketetapan.s_idjenisketetapan', $v['jenis_ketetapan'])
                    ->first();
            $nilai_bphtb_fix = ($v['t_jmlh_totalskpdkb'] != null) ? $v['t_jmlh_totalskpdkb'] + $v['t_nilai_bphtb_fix'] : $v['t_nilai_bphtb_fix'];

            $dataArr[] = [
                't_idspt' => $perintah,
                'nodaftar' => $v['t_kohirspt'],
                'tglskpdkb' => ($v['t_tgldaftar_spt']) ? Carbon::parse($v['t_tgldaftar_spt'])->format('d-m-Y H:i:s') : '-',
                'tahun' => $v['t_periodespt'],
                'notaris' => $v['s_namanotaris'],
                'jenistransaksi' => $v['s_namajenistransaksi'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop_sppt' => $v['t_nop_sppt'],
                'nilai_bphtb_fix' => number_format($nilai_bphtb_fix, 0, ',', '.'),
                'status_persetujuan_bphtb' => $status_persetujuan,
                'status_validasi_berkas' => $status_validasi_berkas,
                'status_validasi_kabid' => $status_validasi_kabid,
                'status_pembayaran' => $status_bayar,
                'jenisketetapan' => $ketetapan->s_namasingkatjenisketetapan,
                'kodebayar' => ($ketetapan->s_namasingkatjenisketetapan == 'SSPD') ? $v['t_kodebayar_bphtb'] : $v['t_kodebayar_skpdkb'],
                'ntpd' => $v['t_ntpd'],
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    //========================================================================== DATAGRID PILIH SPT

    public function pilihspt() {
        $all_session = Auth::user();
        $data_field = $this->data_field;
        $data_field_alias = $this->data_field_alias;
        $data_all = '';
        foreach ($data_field as $v) {
            $data_all .= '#' . $v . ',';
        }
        $fix_data_field = substr($data_all, 0, -1);

        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();

        $data_statusberkas = (new Spt())->cekdata_data_status_berkas();
        $data_statuskabid = (new Spt())->cekdata_data_status_kabid();
        $data_statusbayar = (new Spt())->cekdata_data_status_bayar();

        $data_jenisfasilitas = (new Spt())->cekdata_jenis_fasilitas();
        $data_haktanah = (new Spt())->cekdata_jenis_haktanah();
        $data_doktanah = (new Spt())->cekdata_jenis_doktanah();
        $data_jenispengurangan = (new Spt())->cekdata_jenis_pengurangan();
        $data_jenisperumahan = (new Spt())->cekdata_jenis_perumahan_tanah();

        $data_jenisketetapan = (new Spt())->cekdata_jenis_ketetapan();

        $view = view(
                'pembayaran.pilihspt',
                [
                    'data_field' => $data_field,
                    'data_field_alias' => $data_field_alias,
                    'fix_data_field' => $fix_data_field,
                    'data_notaris' => $data_notaris,
                    'data_jenistransaksi' => $data_jenistransaksi,
                    'data_statusberkas' => $data_statusberkas,
                    'data_statuskabid' => $data_statuskabid,
                    'data_statusbayar' => $data_statusbayar,
                    'data_jenisbidangusaha' => $data_jenisbidangusaha,
                    'data_jenisfasilitas' => $data_jenisfasilitas,
                    'data_haktanah' => $data_haktanah,
                    'data_doktanah' => $data_doktanah,
                    'data_jenispengurangan' => $data_jenispengurangan,
                    'data_jenisperumahan' => $data_jenisperumahan,
                    'data_jenisketetapan' => $data_jenisketetapan
                ]
        );

        return $view;
    }

    public function formpembayaran($uuid) {
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        $cek_persyaratan = (new Spt())->cekdata_id_syaratupload($cek_data_spt->t_idjenistransaksi);
        $data_fotoobjek = (new Spt())->cek_foto_objek($cek_data_spt->t_idspt);
        $data_statusberkas = (new Spt())->cekdata_data_status_berkas();
        $data_statuskabid = (new Spt())->cekdata_data_status_kabid();
        $all_session = Auth::user();
        $skpdkb = Skpdkb::where('t_idspt', $cek_data_spt->t_idspt)->first();
        // dd($skpdkb);

        return view('pembayaran.formpembayaran', [
            'dataspt' => $cek_data_spt,
            'data_notaris' => $data_notaris,
            'data_jenistransaksi' => $data_jenistransaksi,
            'data_jenisbidangusaha' => $data_jenisbidangusaha,
            'data_kecamatan' => (new Spt())->cekdata_kecamatan(),
            'cek_persyaratan' => $cek_persyaratan,
            'data_fotoobjek' => $data_fotoobjek,
            'data_statusberkas' => $data_statusberkas,
            'data_statuskabid' => $data_statuskabid,
            'all_session' => $all_session,
            'skpdkb' => $skpdkb
        ]);
    }

    public function simpanpembayaran(Request $request) {
        $all_session = Auth::user();
        $attr = $request->all();
        $id = $attr['t_idspt'];
        if (!$attr['t_idskpdkb']) {
            $result = $this->bayarSspd($attr, $all_session);
        } else {
            $result = $this->bayarSkpdkb($attr, $all_session);
        }

        if ($result['create']) {
            session()->flash('success', 'OK ' . $result['ketsimpan']);
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('pembayaran');
    }

    function bayarSspd($attr, $all_session) {
        $sspd = new PembayaranBphtb();
        $cek_data_spt = (new Spt())->cek_id_spt($attr['t_idspt']);
        if (empty($attr['t_id_pembayaran'])) {
            $create = $sspd->insert([
                't_idspt' => $attr['t_idspt'],
                's_id_status_bayar' => 1,
                't_tglpembayaran_pokok' => Carbon::parse($attr['t_tglpembayaran_pokok'] . $attr['t_waktujam'])->format('Y-m-d H:i:s'),
                't_jmlhbayar_pokok' => $cek_data_spt->t_nilai_bphtb_fix,
                't_tglpembayaran_denda' => Carbon::parse($attr['t_tglpembayaran_pokok'] . $attr['t_waktujam'])->format('Y-m-d H:i:s'),
                't_jmlhbayar_denda' => 0,
                't_jmlhbayar_total' => $cek_data_spt->t_nilai_bphtb_fix,
                't_iduser_bayar' => $all_session->id
            ]);
            $ketsimpan = 'Data baru berhasil ditambahkan.';
        } else {
            $create = $sspd->where('t_id_pembayaran', $attr['t_id_pembayaran'])->update([
                's_id_status_bayar' => 1,
                't_tglpembayaran_pokok' => Carbon::parse($attr['t_tglpembayaran_pokok'] . $attr['t_waktujam'])->format('Y-m-d H:i:s'),
                't_jmlhbayar_pokok' => $cek_data_spt->t_nilai_bphtb_fix,
                't_tglpembayaran_denda' => Carbon::parse($attr['t_tglpembayaran_pokok'] . $attr['t_waktujam'])->format('Y-m-d H:i:s'),
                't_jmlhbayar_denda' => 0,
                't_jmlhbayar_total' => $cek_data_spt->t_nilai_bphtb_fix,
                't_iduser_bayar' => $all_session->id
            ]);
            $ketsimpan = 'Data baru berhasil diupdate.';
        }

//        $t_ntpd = $this->createNTPD($cek_data_spt->t_kodebayar_bphtb);
//        $data_update_spt = [
//            't_ntpd' => $t_ntpd
//        ];
//        Spt::where('t_idspt', $attr['t_idspt'])->update($data_update_spt);

        return ['create' => $create, 'ketsimpan' => $ketsimpan];
    }

    function bayarSkpdkb($attr, $all_session) {
        $skpdkb = new PembayaranSkpdkb();
        $no_urut = $skpdkb->max('t_nourut_bayar');
        $cek_data_spt = (new Spt())->cek_id_spt($attr['t_idspt']);
        $jmlh_bayar = (int) str_replace('.', '', $attr['t_jmlhbayar']);
        $t_jmlhbayar_total = $cek_data_spt->t_nilai_bphtb_fix + $jmlh_bayar;

        $create = $skpdkb->insert([
            't_idspt' => $attr['t_idspt'],
            't_idskpdkb' => $attr['t_idskpdkb'],
            't_nourut_bayar' => $no_urut + 1,
            's_id_status_bayar' => 1,
            't_iduser_bayar' => $all_session->id,
            't_tglpembayaran_skpdkb' => Carbon::parse($attr['t_tglpembayaran_pokok'] . $attr['t_waktujam'])->format('Y-m-d H:i:s'),
            't_jmlhbayar_skpdkb' => $jmlh_bayar,
            't_tglpembayaran_denda' => Carbon::parse($attr['t_tglpembayaran_pokok'] . $attr['t_waktujam'])->format('Y-m-d H:i:s'),
            't_jmlhbayar_denda' => 0,
            't_jmlhbayar_total' => $t_jmlhbayar_total
        ]);

        $ketsimpan = 'Data baru berhasil ditambahkan.';

        return ['create' => $create, 'ketsimpan' => $ketsimpan];
    }

    public function createNTPD($nourutKodeBayar) {
        $ntpd = substr($nourutKodeBayar, 4, 9);
        return $ntpd;
    }
    
    public function detail(Request $request) {
        $detail = Spt::where('t_idspt', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }
    
    public function destroy(Request $request) {
        PembayaranBphtb::where('t_idspt', '=', $request->query('id'))->delete();
    }

}
