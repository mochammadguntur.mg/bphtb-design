<?php

namespace App\Http\Controllers\Pemeriksaan;

use App\Http\Controllers\Controller;
use App\Models\Pemeriksaan\Pemeriksaan;
use Illuminate\Http\Request;
use App\Models\Spt\Spt;
use App\Models\ValidasiKabid\ValidasiKabid;
use App\Models\Setting\Pemda;
use Auth;
use Carbon\Carbon;

class PemeriksaanController extends Controller {

    public function index() {
        $jmlh_blmperiksa = Spt::leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
                ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
                ->whereNull('t_pembayaran_bphtb.t_idspt')
                ->whereNull('t_pemeriksaan.t_idspt')
                ->where('t_validasi_kabid.s_id_status_kabid', '=', 2)
                ->count();
        $jmlh_sudahperiksa = Pemeriksaan::count();

        return view('pemeriksaan.index', [
            'jmlh_blmperiksa' => $jmlh_blmperiksa,
            'jmlh_sudahperiksa' => $jmlh_sudahperiksa
        ]);
    }

    public function formtambah() {
        return view('pemeriksaan.formtambah', [
            'dataspt' => new Spt(),
            'data_notaris' => (new Spt())->cekdata_notaris(),
            'data_jenistransaksi' => (new Spt())->cekdata_jenis_transaksi(),
            'data_jenisbidangusaha' => (new Spt())->cekdata_jenis_bidangusaha(),
        ]);
    }

    function cek_data_ada_tidak($datacek, $cekformat = null) {
        if (!empty($datacek)) {
            if (!empty($cekformat)) {
                $hasilcek = '<span style="align: right;">' . number_format($datacek, 0, ',', '.') . '</span>';
            } else {
                $hasilcek = $datacek;
            }
        } else {
            $hasilcek = '';
        }

        return $hasilcek;
    }

    public function datagrid(Request $request) {
        $all_session = Auth::user();
        if ($request['status']) {
            $response = (new Pemeriksaan())->datagridPemeriksaanSudah($request);
        } else {
            $response = (new Pemeriksaan())->datagridPemeriksaanBelum($request);
        }

        $dataArr = [];
        foreach ($response as $v) {

            if (!empty($v['s_id_status_berkas'])) {
                if ($v['s_id_status_berkas'] == 1) {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])) . '</small>';
                } else {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])) . '</small>';
                }
            } else {
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['s_id_status_kabid'])) {
                if ($v['s_id_status_kabid'] == 1) {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SETUJU</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])) . '</small>';
                } else {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK SETUJU</button><small class="text-warning">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])) . '</small>';
                }
            } else {
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['t_tglpembayaran_pokok'])) {
                $status_bayar = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> LUNAS</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglpembayaran_pokok'])) . '</small>';
            } else {
                $status_bayar = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';
            }

            if (!empty($v['t_idpersetujuan_bphtb'])) {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])) . '</small>';
            } else {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>';
            }

            if ($request['status']) {
                $lihat_data = '<a href="/lihatdata/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="Lihat Data"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>';
                $bahp = !empty($v['t_idpemeriksa']) ? '<a href="pendaftaran/' . $v['t_idspt'] . '/cetakbahpbphtb" target="_blank" class="btn btn-success btn-block btn-xs" title="CETAK BAHP"><i class="fa fa-fw fa-print"></i> CETAK BAHP</a>' : '';
                $status_pemeriksaan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SUDAH</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglpemeriksaan'])) . '</small>';
                $hapus_pemeriksaan = ($all_session['s_id_hakakses'] == 1) ? 
                    (!empty($v['t_tglpembayaran_pokok']) ? '<a onclick="javascript:showAlertDialog(' . $v['t_idspt'] . ')" class="btn btn-block bg-danger btn-sm" title="Batal Validasi Berkas"><i class="fa fa-fw fa-trash"></i> Batal Validasi</a>' : '<a onclick="javascript:showDeleteDialog(' . $v['t_idspt'] . ')" class="btn btn-block bg-danger btn-sm" title="Batal Pemeriksaan"><i class="fa fa-fw fa-trash"></i> Batal Pemeriksaan</a>') 
                    : '';
                $perintah = '<div class="dropdown">
                                <button onclick="myFunction(' . $v['t_idspt'] . ')" class="dropbtn btn-primary dropdown-toggle btn btn-xs">PERINTAH <span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                                <div id="myDropdown' . $v['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:80px; border-color: blue;">
                                        ' . $lihat_data . '
                                        ' . $bahp . '
                                        ' . $hapus_pemeriksaan . '
                                </div>
                            </div>';
            } else {
                $status_pemeriksaan = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
                $perintah = '<a href="' . url('pemeriksaan') . '/formpemeriksaan/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="PILIH"><i class="fa fa-fw fa-hand-pointer-o"></i> PERIKSA</a>';
            }

            $dataArr[] = [
                't_idspt' => $perintah,
                'nodaftar' => $v['t_kohirspt'],
                'tglskpdkb' => ($v['t_tgldaftar_spt']) ? Carbon::parse($v['t_tgldaftar_spt'])->format('d-m-Y H:i:s') : '-',
                'tahun' => $v['t_periodespt'],
                'notaris' => $v['s_namanotaris'],
                'jenistransaksi' => $v['s_namajenistransaksi'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop_sppt' => $v['t_nop_sppt'],
                'nilai_bphtb_fix' => number_format($v['t_nilai_bphtb_fix'], 0, ',', '.'),
                'status_persetujuan_bphtb' => $status_persetujuan,
                'status_validasi_berkas' => $status_validasi_berkas,
                'status_validasi_kabid' => $status_validasi_kabid,
                'status_pemeriksaan' => $status_pemeriksaan,
                'status_pembayaran' => $status_bayar,
                't_kodebayar_bphtb' => !empty($v['t_kodebayar_bphtb']) ? $v['t_kodebayar_bphtb'] : '',
                'ntpd' => !empty($v['t_ntpd']) ? $v['t_ntpd'] : ''
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function formpemeriksaan($uuid) {
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        $cek_persyaratan = (new Spt())->cek_all_data_syarat_dan_file_upload($cek_data_spt->t_idspt, $cek_data_spt->t_idjenistransaksi);
        $data_statusberkas = (new Spt())->cekdata_data_status_berkas();
        $data_statuskabid = (new Spt())->cekdata_data_status_kabid();
        $data_fotoobjek = (new Spt())->cek_foto_objek($cek_data_spt->t_idspt);
        $all_session = Auth::user();
        $data_pejabat = (new Spt())->cekdata_pejabat_all();
        $cek_data_idspt_pemeriksaan = (new Spt())->cek_idspt_pemeriksaan($cek_data_spt->t_idspt);
        $cek_jenis_transaksi = (new Spt())->cek_jenis_transaksi($cek_data_spt->t_idjenistransaksi);

        if (!empty($cek_data_spt->t_idpengurangan)) {
            $cek_pengurangan = (new Spt())->cek_jenis_pengurangan($cek_data_spt->t_idpengurangan);
            $tarif_pengurangan = $cek_pengurangan->s_persentase / 100;
        } else {
            $tarif_pengurangan = 0;
        }

        return view(
                'pemeriksaan.formpemeriksaan',
                [
                    'dataspt' => $cek_data_spt,
                    'data_notaris' => $data_notaris,
                    'data_jenistransaksi' => $data_jenistransaksi,
                    'data_jenisbidangusaha' => $data_jenisbidangusaha,
                    'data_kecamatan' => (new Spt())->cekdata_kecamatan(),
                    'cek_persyaratan' => $cek_persyaratan,
                    'data_statusberkas' => $data_statusberkas,
                    'data_statuskabid' => $data_statuskabid,
                    'data_fotoobjek' => $data_fotoobjek,
                    'all_session' => $all_session,
                    'data_pejabat' => $data_pejabat,
                    'cek_data_idspt_pemeriksaan' => $cek_data_idspt_pemeriksaan,
                    'cek_jenis_transaksi' => $cek_jenis_transaksi,
                    'tarif_pengurangan' => $tarif_pengurangan
                ]
        );
    }

    function cekedit_perhitunganbphtb($idspt, $nikbersama, $ex) {

        $nikbersama = str_ireplace("'", "", $nikbersama);
        $tahunproses = date('Y', strtotime($ex->t_tgldaftar_spt));
        $ceknik = (new Spt())->ceknik($nikbersama, $tahunproses);

        $no = 1;
        $id_spt_pertama = '';
        foreach ($ceknik as $key => $v) {
            if ($no == 1) {
                $id_spt_pertama = $v->t_idspt;
            }
            $no++;
        }

        $idcek = $id_spt_pertama;
        $data = array();

        if (!empty($idcek)) {
            if ($idcek == $idspt) {
                $data['ket_npoptkp'] = 'Dapat NPOPTKP';
                $data['dapatgak'] = 1;
            } else {
                $data['ket_npoptkp'] = 'Tidak Dapat NPOPTKP';
                $data['dapatgak'] = 0;
            }
        } else {
            $data['ket_npoptkp'] = 'Dapat NPOPTKP';
            $data['dapatgak'] = 1;
        }

        return $data;
    }

    function hitung_pemeriksaan($data_get) {
        $perhitungan = array();
        $cek_data_spt = (new Spt())->cek_id_spt($data_get['t_idspt']);
        $cek_jenis_transaksi = (new Spt())->cek_jenis_transaksi($cek_data_spt->t_idjenistransaksi);

        $perhitungan['t_luastanah'] = str_ireplace(".", "", $data_get['t_luastanah']);
        $perhitungan['t_njoptanah'] = str_ireplace(".", "", $data_get['t_njoptanah']);
        $perhitungan['t_totalnjoptanah'] = $perhitungan['t_luastanah'] * $perhitungan['t_njoptanah'];

        $perhitungan['t_luasbangunan'] = str_ireplace(".", "", $data_get['t_luasbangunan']);
        $perhitungan['t_njopbangunan'] = str_ireplace(".", "", $data_get['t_njopbangunan']);
        $perhitungan['t_totalnjopbangunan'] = $perhitungan['t_luasbangunan'] * $perhitungan['t_njopbangunan'];

        $perhitungan['t_grandtotalnjop'] = $perhitungan['t_totalnjoptanah'] + $perhitungan['t_totalnjopbangunan'];
        $perhitungan['t_nilaitransaksispt'] = str_ireplace(".", "", $data_get['t_nilaitransaksispt']);

        $cek_tarifbphtb = (new Spt())->cek_tarifbphtb(date('Y-m-d'));

        $perhitungan['t_idtarifbphtb'] = $cek_tarifbphtb->s_idtarifbphtb;
        $perhitungan['t_persenbphtb'] = $cek_tarifbphtb->s_tarif_bphtb;

        switch ($cek_jenis_transaksi->s_idstatus_pht) {
            case 2:
                $perhitungan['t_npop_bphtb'] = $perhitungan['t_grandtotalnjop'];
                break;
            case 3:
                $perhitungan['t_npop_bphtb'] = $perhitungan['t_nilaitransaksispt'];
                break;
            default:
                if ($perhitungan['t_nilaitransaksispt'] >= $perhitungan['t_grandtotalnjop']) {
                    $perhitungan['t_npop_bphtb'] = $perhitungan['t_nilaitransaksispt'];
                } else {
                    $perhitungan['t_npop_bphtb'] = $perhitungan['t_grandtotalnjop'];
                }
                break;
        }

        $cek_npoptkp = (new Spt())->cek_npoptkp($cek_data_spt->t_idjenistransaksi);

        $perhitungan['t_npoptkp_bphtb'] = $cek_data_spt->t_npoptkp_bphtb;

        if (!empty($perhitungan['t_luastanah'])) {
            $perhitungan['t_permeter_tanah'] = ($perhitungan['t_npop_bphtb'] - $perhitungan['t_totalnjopbangunan']) / $perhitungan['t_luastanah'];
        } else {
            $perhitungan['t_permeter_tanah'] = 0;
        }
        $perhitungan['t_npopkp_bphtb'] = $perhitungan['t_npop_bphtb'] - $perhitungan['t_npoptkp_bphtb'];

        if ($perhitungan['t_npopkp_bphtb'] < 0) {
            $perhitungan['t_npopkp_bphtb'] = 0;
        } else {
            $perhitungan['t_npopkp_bphtb'] = $perhitungan['t_npopkp_bphtb'];
        }

        if (!empty($cek_data_spt->t_idjenisfasilitas)) {
            $perhitungan['t_nilai_bphtb_fix'] = 0;
            $perhitungan['t_nilai_bphtb_real'] = 0;
        } else {
            $perhitungan['t_nilai_bphtb_fix'] = ceil($perhitungan['t_npopkp_bphtb'] * $perhitungan['t_persenbphtb'] / 100);
            $perhitungan['t_nilai_bphtb_real'] = $perhitungan['t_nilai_bphtb_fix'];
        }

        return $perhitungan;
    }

    public function hitungPemeriksaanJson(Request $request) {
        $attr = $request->all();
        $hitung_pajak_bphtb = $this->hitung_pemeriksaan($attr);

        return response()->json([
                    $hitung_pajak_bphtb,
        ]);
    }

    public function simpanpemeriksaan(Request $request) {
        $all_session = Auth::user();
        $attr = $request->all();
        $cek_data_spt = (new Spt())->cek_id_spt($attr['t_idspt']);
        $pemda = Pemda::orderBy('updated_at', 'desc')->first();
        // $nourutKodeBayar = Spt::select()->whereYear('t_tglbuat_kodebyar', date('Y'))->max('t_nourut_kodebayar')+1;
        $hitung_pajak_bphtb = $this->hitung_pemeriksaan($attr);

        $data_simpan = array(
            't_idspt' => $attr['t_idspt'],
            't_tglpenetapan' => date('Y-m-d H:i:s'),
            't_iduser_buat' => $all_session->id,
            't_nop_sppt' => $cek_data_spt->t_nop_sppt,
            't_tahun_sppt' => $cek_data_spt->t_tahun_sppt,
            't_luastanah_pemeriksa' => $hitung_pajak_bphtb['t_luastanah'],
            't_njoptanah_pemeriksa' => $hitung_pajak_bphtb['t_njoptanah'],
            't_totalnjoptanah_pemeriksa' => $hitung_pajak_bphtb['t_totalnjoptanah'],
            't_luasbangunan_pemeriksa' => $hitung_pajak_bphtb['t_luasbangunan'],
            't_njopbangunan_pemeriksa' => $hitung_pajak_bphtb['t_njopbangunan'],
            't_totalnjopbangunan_pemeriksa' => $hitung_pajak_bphtb['t_totalnjopbangunan'],
            't_grandtotalnjop_pemeriksa' => $hitung_pajak_bphtb['t_grandtotalnjop'],
            't_nilaitransaksispt_pemeriksa' => $hitung_pajak_bphtb['t_nilaitransaksispt'],
            't_permeter_tanah_pemeriksa' => $hitung_pajak_bphtb['t_permeter_tanah'],
            't_idtarifbphtb_pemeriksa' => $hitung_pajak_bphtb['t_idtarifbphtb'],
            't_persenbphtb_pemeriksa' => $hitung_pajak_bphtb['t_persenbphtb'],
            't_npop_bphtb_pemeriksa' => $hitung_pajak_bphtb['t_npop_bphtb'],
            't_npoptkp_bphtb_pemeriksa' => $hitung_pajak_bphtb['t_npoptkp_bphtb'],
            't_npopkp_bphtb_pemeriksa' => $hitung_pajak_bphtb['t_npopkp_bphtb'],
            't_nilaibphtb_pemeriksa' => $hitung_pajak_bphtb['t_nilai_bphtb_fix'],
            't_idpejabat1' => (empty($attr['t_idpejabat1'])) ? null : $attr['t_idpejabat1'],
            't_idpejabat2' => (empty($attr['t_idpejabat2'])) ? null : $attr['t_idpejabat2'],
            't_idpejabat3' => (empty($attr['t_idpejabat3'])) ? null : $attr['t_idpejabat3'],
            't_idpejabat4' => (empty($attr['t_idpejabat4'])) ? null : $attr['t_idpejabat4'],
            't_keterangan' => (!empty($attr['t_keterangan'])) ? $attr['t_keterangan'] : null,
            't_noperiksa' => (!empty($attr['t_noperiksa'])) ? $attr['t_noperiksa'] : null
        );

        if (empty($attr['t_idpemeriksa'])) {
            $cek_data_idspt_pemeriksaan = (new Spt())->cek_idspt_pemeriksaan($attr['t_idspt']);
            if (empty($cek_data_idspt_pemeriksaan->t_idpemeriksa)) {
                $create = Pemeriksaan::create($data_simpan);
                Spt::where('t_idspt', $attr['t_idspt'])
                        ->update([
                            't_nilai_bphtb_fix' => $hitung_pajak_bphtb['t_nilai_bphtb_fix']
                ]);

                $ketsimpan = 'Data baru berhasil ditambahkan.';
            } else {
                $ketsimpan = 'Data ini sudah pernah di validasi';
            }
        } else {
            $create = Pemeriksaan::where('t_idpemeriksa', $attr['t_idpemeriksa'])
                    ->update($data_simpan);

            Spt::where('t_idspt', $attr['t_idspt'])
                    ->update([
                        't_nilai_bphtb_fix' => $hitung_pajak_bphtb['t_nilai_bphtb_fix']
            ]);

            $ketsimpan = 'Data baru berhasil diupdate.';
        }

        if ($create) {
            session()->flash('success', 'OK ' . $ketsimpan);
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('pemeriksaan');
    }
    
    public function detail(Request $request) {
        $detail = Spt::where('t_idspt', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }
    
    public function destroy(Request $request) {
        Pemeriksaan::where('t_idspt', '=', $request->query('id'))->delete();
    }

}
