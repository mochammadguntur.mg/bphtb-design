<?php

namespace App\Http\Controllers\Pelayanan;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Pelayanan\PelayananAngsuranRequest;
use App\Models\Pelayanan\PelayananAngsuran;
use App\Models\Spt\Spt;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Pelayanan\PelayananAngsuranExport;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class PelayananAngsuranController extends Controller
{
    public function index(){
        return view('pelayanan-angsuran.index');
    }

    public function datagrid(Request $request){
        $response = PelayananAngsuran::select()
            ->select(
                't_idangsuran',
                't_noangsuran',
                't_tglpengajuan',
                't_kohirspt',
                't_nama_pembeli',
                't_tglpersetujuan',
                't_pelayanan_angsuran.created_at')
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_angsuran.t_idspt');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idangsuran');
        }

        if ($request['filter']['noangsuran'] != null) {
            $response = $response->where('t_noangsuran', 'like', "%" . $request['filter']['noangsuran'] . "%");
        }
        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['nama_pembeli'] . "%");
        }
        if ($request['filter']['tglpersetujuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpersetujuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpersetujuan', [$startDate, $endDate]);
        }
        if ($request['filter']['tglpengajuan'] != null) {
            $date = explode(' - ', $request['filter']['tglpengajuan']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpengajuan', [$startDate, $endDate]);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'noangsuran' => $v['t_noangsuran'],
                'tglpengajuan' => ($v['t_tglpengajuan']) ? date('d-m-Y', strtotime($v['t_tglpengajuan'])) : '-',
                'kohirspt' => $v['t_kohirspt'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'tglpersetujuan' => ($v['t_tglpersetujuan']) ? date('d-m-Y', strtotime($v['t_tglpersetujuan'])) : '-',
                'status' => ($v['t_tglpersetujuan']) ? '<strong class="text-blue">Approved</strong>' : '<strong class="text-green">Baru</strong>',
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'pelayanan-angsuran/' . $v['t_idangsuran'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['t_idangsuran'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function datagrid_sspdbphtb(Request $request){
        $response = Spt::select()
        ->select(
            't_spt.t_idspt',
            't_tgldaftar_spt',
            't_kohirspt',
            't_nama_pembeli',
            't_nilai_bphtb_fix',
            't_nop_sppt',
            't_idjenistransaksi',
            's_namajenistransaksi')
        ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
        ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
        ->whereNotNull('t_validasi_kabid.t_tglvalidasi');
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_tgldaftar_spt', 'desc');
            $response = $response->orderBy('t_kohirspt', 'desc');
        }

        if ($request['filter']['kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['kohirspt'] . "%");
        }
        if ($request['filter']['namawp'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['namawp'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'tgldaftar' => ($v['t_tgldaftar_spt']) ? date('d-m-Y', strtotime($v['t_tgldaftar_spt'])) : '-',
                'kohirspt' => $v['t_kohirspt'],
                'namawp' => $v['t_nama_pembeli'],
                'jmlhpajak' => number_format($v['t_nilai_bphtb_fix'],0,',','.'),
                'jenistransaksi' => $v['s_namajenistransaksi'],
                'nop' => $v['t_nop_sppt'],
                'actionList' => [
                    [
                        'actionName' => 'pilih',
                        'actionUrl' => 'javascript:pilihTransaksi(' . $v['t_idspt'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request){
        $detail = PelayananAngsuran::where('t_idangsuran', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create(){
        return view('pelayanan-angsuran.create', [
            'pelayananangsuran' => new PelayananAngsuran()
        ]);
    }

    public function store(PelayananAngsuranRequest $pelayananAngsuranRequest){
        $attr = $pelayananAngsuranRequest->all();

        $cekExist = PelayananAngsuran::where('t_idspt', '=', $attr['t_idspt'])->get();

        if(count($cekExist) == 0){
            $create = PelayananAngsuran::create([
                't_idspt' => $attr['t_idspt'],
                't_noangsuran' => PelayananAngsuran::whereYear('t_tglpengajuan', date('Y'))->max('t_noangsuran')+1,
                't_tglpengajuan' => Carbon::createFromFormat('d-m-Y',$attr['t_tglpengajuan'])->format('Y-m-d H:i:s'),
                't_keterangan_permohoanan' => $attr['t_keterangan_permohonan'],
                't_nosk_angsuran' => 0,
                't_iduser_pengajuan' => Auth::user()->id,
            ]);

            if($create){
                session()->flash('success', 'Data Permohonan Angsuran berhasil disimpan.');
            }else{
                session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
            }
        }else{
            session()->flash('error', 'No Kohir '.$attr['t_kohirspt'].' sudah pernah diajukan sebelumnya!');
        }

        return redirect('pelayanan-angsuran');

    }


    public function edit($angsuran){
        $dataangsuran = PelayananAngsuran::select()
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_angsuran.t_idspt')
            ->where('t_idangsuran',$angsuran)->get();
        return view('pelayanan-angsuran.edit', [
            'pelayananangsuran' => $dataangsuran[0]
        ]);
    }

    public function update($angsuran, PelayananAngsuranRequest $pelayananAngsuranRequest, PelayananAngsuran $pelayananAngsuran){
        $attr = $pelayananAngsuranRequest->all();
        $update = $pelayananAngsuran->where('t_idangsuran',$angsuran)
        ->update([
            't_idspt' => $attr['t_idspt'],
            //'t_tglpengajuan' => Carbon::createFromFormat('d-m-Y',$attr['t_tglpengajuan'])->format('Y-m-d H:i:s'),
            't_keterangan_permohoanan' => $attr['t_keterangan_permohonan'],
            't_nosk_angsuran' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
        ]);

        if($update){
            session()->flash('success', 'Data Permohonan Angsuran Behasil diupdate.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('pelayanan-angsuran');
    }

    public function destroy(Request $request){
        PelayananAngsuran::where('t_idangsuran', '=', $request->query('id'))->delete();
    }

    public function caridata(Request $request){
        $detail = Spt::where('t_idspt', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function export_xls(Request $request){
        return Excel::download(
            new PelayananAngsuranExport($request->query('t_noangsuran'),
            $request->query('t_tglpengajuan'),
            $request->query('t_kohirspt'),
            $request->query('t_nama_pembeli'),
            $request->query('t_tglpersetujuan'),
            $request->query('created_at')),
            'pelayanan_angsuran.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request){
        $sheet = Excel::download(
            new PelayananAngsuranExport($request->query('t_noangsuran'),
            $request->query('t_tglpengajuan'),
            $request->query('t_kohirspt'),
            $request->query('t_nama_pembeli'),
            $request->query('t_tglpersetujuan'),
            $request->query('created_at')),
            'pelayanan_angsuran.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }
}