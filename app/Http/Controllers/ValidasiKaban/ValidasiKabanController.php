<?php

namespace App\Http\Controllers\ValidasiKaban;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\ValidasiKaban\ValidasiKabanRequest;
use App\Models\Spt\Spt;
use App\Models\ValidasiKaban\ValidasiKaban;
use App\Models\Setting\Pemda;
use App\Models\Setting\Briva;
use App\Models\Logs\logBriva;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ValidasiKabanController extends Controller {

    public function index() {
        $view = view(
                'validasi-kaban.index', [
            'count_belum' => (new ValidasiKaban())->datagridValidasiKabanBelum()->count(),
            'count_diterima' => (new ValidasiKaban())->datagridValidasiKabanStatus(1)->count(),
            'count_ditolak' => (new ValidasiKaban())->datagridValidasiKabanStatus(2)->count()
                ]
        );
        return $view;
    }

    public function datagrid(Request $request) {
        $all_session = Auth::user();
        $response = (new ValidasiKaban())->datagridValidasiKabanSudah();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_tglvalidasikaban');
        }

        if ($request['filter']['nodaftar'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['nodaftar'] . "%");
        }
        if ($request['filter']['tahun'] != null) {
            $response = $response->where('t_periodespt', 'like', "%" . $request['filter']['tahun'] . "%");
        }
        if ($request['filter']['notaris'] != null) {
            $response = $response->where('t_idnotaris_spt', '=', $request['filter']['notaris']);
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['namawp'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['namawp'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['status_persetujuan'] != null) {
            $response = ((($request['filter']['status_persetujuan'] == 1)) ? $response->where('t_idpersetujuan_bphtb', '=', $request['filter']['status_persetujuan']) : $response->whereNull('t_idpersetujuan_bphtb'));
        }
        if ($request['filter']['status_validasi_berkas'] != null) {
            if ($request['filter']['status_validasi_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['status_validasi_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['status_validasi_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }
        if ($request['filter']['status_validasi_kabid'] != null) {
            if ($request['filter']['status_validasi_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['status_validasi_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['status_validasi_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }
        if ($request['filter']['status_pemeriksaan'] != null) {
            if ($request['filter']['status_pemeriksaan'] == 1) {
                $response = $response->whereNotNull('t_idpemeriksa');
            } elseif ($request['filter']['status_pemeriksaan'] == 2) {
                $response = $response->whereNull('t_idpemeriksa');
            }
        }
        if ($request['filter']['status_pembayaran'] != null) {
            if ($request['filter']['status_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['status_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {

            if (!empty($v['s_id_status_berkas'])) {
                if ($v['s_id_status_berkas'] == 1) {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])) . '</small>';
                } else {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button><small class="text-warning">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])) . '</small>';
                }
            } else {
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['s_id_status_kabid'])) {
                if ($v['s_id_status_kabid'] == 1) {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SETUJU</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])) . '</small>';
                    $cetak_sspd = '<a href="' . url('pendaftaran') . '/' . $v['t_idspt'] . '/cetaksspdbphtb" target="_blank" class="btn btn-primary btn-block btn-xs" title="CETAK SSPD"><i class="fa fa-fw fa-print"></i> CETAK SSPD</a>';
                } else {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK SETUJU</button><small class="text-warning">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])) . '</small>';
                    $cetak_sspd = '';
                }
            } else {
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
                $cetak_sspd = '';
            }

            if (!empty($v['t_idpemeriksa'])) {
                $status_pemeriksaan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fas fa-fw fa-check-double"></i> SUDAH</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglpemeriksaan'])) . '</small>';
            } else {
                $status_pemeriksaan = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }


            if (!empty($v['t_idpemeriksa'])) {
                $validasi_pemeriksaan = '<a href="' . url('validasi-kabid') . '/formvalidasikeduakabid/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="VALIDASI KABID HASIL PEMERIKSAAN"><i class="fa fa-fw fa-edit"></i> VALIDASI KABID HASIL PEMERIKSAAN</a>';
                $edit_validasi = '';
            } else {
                $validasi_pemeriksaan = '';
                $edit_validasi = '<a href="' . url('validasi-kabid') . '/formvalidasikabid/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
            }

            if ($v['s_id_status_kabid'] == 1) {
                $validasi_pemeriksaan = '';
            }



            if (!empty($v['t_tglpembayaran_pokok'])) {
                $status_bayar = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> LUNAS</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglpembayaran_pokok'])) . '</small>';
            } else {
                $status_bayar = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';
            }

            if (!empty($v['t_idpersetujuan_bphtb'])) {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])) . '</small>';
            } else {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>';
            }

            if ($all_session['s_id_hakakses'] == 1) {
                if (!empty($v['t_tglpembayaran_pokok'])) {
                    $onClick = 'javascript:showAlertDialog(' . $v['t_idspt'] . ')';
                } else {
                    $onClick = 'javascript:showDeleteDialog(' . $v['t_idspt'] . ')';
                }
                $hapus_validasi = '<a onclick="' . $onClick . '" class="btn btn-block bg-danger btn-sm" title="Batal Validasi Kaban"><i class="fa fa-fw fa-trash"></i> Batal Validasi</a>';
            } else {
                $hapus_validasi = '';
            }

            $perintah = '<div class="dropdown">
                    <button onclick="myFunction(' . $v['t_idspt'] . ')" class="dropbtn btn-primary dropdown-toggle btn btn-xs">PERINTAH <span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $v['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:80px; border-color: blue;">
                            ' . $edit_validasi . '
                            ' . $cetak_sspd . '
                            ' . $validasi_pemeriksaan . '
                            ' . $hapus_validasi . '
                    </div>
                </div>';
            $dataArr[] = [
                't_idspt' => $perintah,
                'nodaftar' => $v['t_kohirspt'],
                'tglskpdkb' => ($v['t_tgldaftar_spt']) ? Carbon::parse($v['t_tgldaftar_spt'])->format('d-m-Y H:i:s') : '-',
                'tahun' => $v['t_periodespt'],
                'notaris' => $v['s_namanotaris'],
                'jenistransaksi' => $v['s_namajenistransaksi'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop_sppt' => $v['t_nop_sppt'],
                'nilai_bphtb_fix' => number_format($v['t_nilai_bphtb_fix'], 0, ',', '.'),
                'status_persetujuan_bphtb' => $status_persetujuan,
                'status_validasi_berkas' => $status_validasi_berkas,
                'status_validasi_kabid' => $status_validasi_kabid,
                'status_pemeriksaan' => $status_pemeriksaan,
                'status_pembayaran' => $status_bayar,
                't_kodebayar_bphtb' => !empty($v['t_kodebayar_bphtb']) ? $v['t_kodebayar_bphtb'] : '',
                'ntpd' => !empty($v['t_ntpd']) ? $v['t_ntpd'] : ''
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function datagridbelum(Request $request) {

        $response = (new ValidasiKaban())->datagridValidasiKabanBelum();

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_tglvalidasikabid');
        }

        if ($request['filter']['nodaftar'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['nodaftar'] . "%");
        }
        if ($request['filter']['tahun'] != null) {
            $response = $response->where('t_periodespt', 'like', "%" . $request['filter']['tahun'] . "%");
        }
        if ($request['filter']['notaris'] != null) {
            $response = $response->where('t_idnotaris_spt', '=', $request['filter']['notaris']);
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['namawp'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['namawp'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['status_persetujuan'] != null) {
            $response = ((($request['filter']['status_persetujuan'] == 1)) ? $response->where('t_idpersetujuan_bphtb', '=', $request['filter']['status_persetujuan']) : $response->whereNull('t_idpersetujuan_bphtb'));
        }
        if ($request['filter']['status_validasi_berkas'] != null) {
            if ($request['filter']['status_validasi_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['status_validasi_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['status_validasi_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }
        if ($request['filter']['status_validasi_kabid'] != null) {
            if ($request['filter']['status_validasi_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['status_validasi_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['status_validasi_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }
        if ($request['filter']['status_pemeriksaan'] != null) {
            if ($request['filter']['status_pemeriksaan'] == 1) {
                $response = $response->whereNotNull('t_idpemeriksa');
            } elseif ($request['filter']['status_pemeriksaan'] == 2) {
                $response = $response->whereNull('t_idpemeriksa');
            }
        }
        if ($request['filter']['status_pembayaran'] != null) {
            if ($request['filter']['status_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['status_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {

            if (!empty($v['s_id_status_berkas'])) {
                if ($v['s_id_status_berkas'] == 1) {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])) . '</small>';
                } else {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button><small class="text-warning">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])) . '</small>';
                }
            } else {
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['s_id_status_kabid'])) {
                if ($v['s_id_status_kabid'] == 1) {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SETUJU</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])) . '</small>';
                } else {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK SETUJU</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])) . '</small>';
                }
            } else {
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['t_tglpembayaran_pokok'])) {
                $status_bayar = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> LUNAS</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglpembayaran_pokok'])) . '</small>';
            } else {
                $status_bayar = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';
            }

            if (!empty($v['t_tglpemeriksaan'])) {
                $status_pemeriksaan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fas fa-fw fa-check-double"></i> SUDAH</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tglpemeriksaan'])) . '</small>';
            } else {
                $status_pemeriksaan = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['t_idpersetujuan_bphtb'])) {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button><small class="text-green">' . date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])) . '</small>';
            } else {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>';
            }

            $perintah = '<a href="' . url('validasi-kaban') . '/formvalidasi/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="Validasi Kaban"><i class="fas fa-fw fa-check-double"></i> <strong>VALIDASI</strong></a>';

            $dataArr[] = [
                't_idspt' => $perintah,
                'nodaftar' => $v['t_kohirspt'],
                'tgldaftar' => ($v['t_tgldaftar_spt']) ? Carbon::parse($v['t_tgldaftar_spt'])->format('d-m-Y H:i:s') : '-',
                'tahun' => $v['t_periodespt'],
                'notaris' => $v['s_namanotaris'],
                'jenistransaksi' => $v['s_namajenistransaksi'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop_sppt' => $v['t_nop_sppt'],
                'nilai_bphtb_fix' => number_format($v['t_nilai_bphtb_fix'], 0, ',', '.'),
                'status_persetujuan_bphtb' => $status_persetujuan,
                'status_validasi_berkas' => $status_validasi_berkas,
                'status_validasi_kabid' => $status_validasi_kabid,
                'status_pemeriksaan' => $status_pemeriksaan,
                'status_pembayaran' => $status_bayar,
                't_kodebayar_bphtb' => !empty($v['t_kodebayar_bphtb']) ? $v['t_kodebayar_bphtb'] : '',
                'ntpd' => !empty($v['t_ntpd']) ? $v['t_ntpd'] : ''
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }
    
    public function formvalidasi($uuid) {
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        $cek_persyaratan = (new Spt())->cek_all_data_syarat_dan_file_upload($cek_data_spt->t_idspt, $cek_data_spt->t_idjenistransaksi);
        $data_statusberkas = (new Spt())->cekdata_data_status_berkas();
        $data_statuskabid = (new Spt())->cekdata_data_status_kabid();
        $data_fotoobjek = (new Spt())->cek_foto_objek($cek_data_spt->t_idspt);
        $all_session = Auth::user();
        $historyTransaksi = Spt::select()
                        ->leftJoin('s_jenistransaksi', 's_jenistransaksi.s_idjenistransaksi', '=', 't_spt.t_idjenistransaksi')
                        ->where('t_nop_sppt', 'like', substr($cek_data_spt->t_nop_sppt, 0, 16) . "%")
                        ->whereYear('t_tgldaftar_spt', '>=', date('Y') - 3)
                        ->orderBy('t_njoptanah', 'DESC')
                        ->orderBy('t_nilai_bphtb_fix', 'DESC')->get();

        $view = view('validasi-kaban.formvalidasi', [
            'dataspt' => $cek_data_spt,
            'data_notaris' => $data_notaris,
            'data_jenistransaksi' => $data_jenistransaksi,
            'data_jenisbidangusaha' => $data_jenisbidangusaha,
            'data_kecamatan' => (new Spt())->cekdata_kecamatan(),
            'cek_persyaratan' => $cek_persyaratan,
            'data_statusberkas' => $data_statusberkas,
            'data_statuskabid' => $data_statuskabid,
            'data_fotoobjek' => $data_fotoobjek,
            'all_session' => $all_session,
            'historyTransaksi' => $historyTransaksi,
                ]
        );
        return $view;
    }
    
    public function simpanvalidasi(ValidasiKabanRequest $validasiKabanRequest) {
        $all_session = Auth::user();
        $attr = $validasiKabanRequest->all();
        $pemda = Pemda::orderBy('updated_at', 'desc')->first();
        
        $cek_setmenubriva = (new Spt())->cek_setmenubriva();
        if($cek_setmenubriva->s_id_statusmenubriva == 1){ 
            $briva = Briva::orderBy('updated_at', 'desc')->first();
        }    
        $bikin_kodebayar = (new Spt())->bikin_kodebayar(date('Y'));
        $nourutKodeBayar = $bikin_kodebayar->t_nourut_kodebayar;
        //$nourutKodeBayar = Spt::select()->whereYear('t_tglbuat_kodebyar', date('Y'))->max('t_nourut_kodebayar') + 1;
        if (empty($attr['t_id_validasi_kaban'])) {
            $cek_data_idspt_validasi_kaban = (new Spt())->cek_idspt_validasi_kaban($attr['t_idspt']);
//            dd($cek_data_idspt_validasi_kabid);
            if (empty($cek_data_idspt_validasi_kaban->t_id_validasi_kaban)) {
                $data_simpan = array(
                    't_idspt' => $attr['t_idspt'],
                    's_id_status_kaban' => $attr['s_id_status_kaban'],
                    't_tglvalidasikaban' => Carbon::now(),
                    't_keterangan_kaban' => $attr['t_keterangan_kaban'],
                    't_iduser_validasikaban' => $all_session->id
                );
                $create = ValidasiKaban::create($data_simpan);
                $kirim_briva = '';
                if ($attr['s_id_status_kaban'] == 1) {
                    //$kodebayar = $this->createKodeBayar($pemda, $nourutKodeBayar);
                    $jenisketetapan = 1; //SSPD
                    $tahun_kodebayar = date('Y');
                    $kodebayar = (new Spt())->format_idbilling($tahun_kodebayar, $nourutKodeBayar, $jenisketetapan);
                    
                    if($cek_setmenubriva->s_id_statusmenubriva == 1){ 
                        $kodebayarnya = $briva->briva_no . $kodebayar;
                    }else{
                        $kodebayarnya = $kodebayar;
                    }
                    
                    $data_update_spt = array(
                        't_tglbuat_kodebyar' => Carbon::now(),
                        't_nourut_kodebayar' => $nourutKodeBayar,
                        't_kodebayar_bphtb' => $kodebayarnya //$briva->briva_no . $kodebayar
                    );
                    $data_update_spt['t_ntpd'] = $this->createNTPD($nourutKodeBayar);
                    $cek_data_spt = (new Spt())->cek_id_spt($attr['t_idspt']);
                    if ($cek_data_spt->t_nilai_bphtb_fix == 0) {
                        $kirim_briva = '';
                        $id_kabid = ValidasiKaban::where('t_idspt', $attr['t_idspt'])->get();
                        $data_pembayaran_nihil = array(
                            '_token' => $attr['_token'],
                            't_idspt' => $attr['t_idspt'],
                            't_id_validasi_berkas' => $attr['t_id_validasi_berkas'],
                            't_id_validasi_kabid' => $id_kabid[0]->t_id_validasi_kabid,
                            't_tglpembayaran_pokok' => date('d-m-Y'),
                            't_waktujam' => date('H:i'),
                            't_idskpdkb' => null,
                            't_jmlhbayar' => 0
                        );
                        app('App\Http\Controllers\Pembayaran\PembayaranController')->bayarSspd($data_pembayaran_nihil, $all_session);
                    } else {
                        $kirim_briva = '';
                        if($cek_setmenubriva->s_id_statusmenubriva == 1){ 
                            if ($briva->status == 1) {
                                $namawp = ($cek_data_spt->t_idbidang_usaha == 1) ? $cek_data_spt->t_nama_pembeli : $cek_data_spt->t_nama_pembeli;
                                $kirim_briva = $this->BrivaCreate($kodebayar, $namawp, $cek_data_spt->t_nilai_bphtb_fix);
                            }
                        }
                    }
                    $create2 = Spt::where('t_idspt', $attr['t_idspt'])->update($data_update_spt);
                    if($cek_setmenubriva->s_id_statusmenubriva == 1){ 
                        $cek_kodebayar = (new Spt())->looping_cek_kodebayar_all($briva->briva_no . $kodebayar, date('Y'), $jenisketetapan);
                    }else{
                        $cek_kodebayar = (new Spt())->looping_cek_kodebayar_all($kodebayar, date('Y'), $jenisketetapan);
                    }
                }
                $ketsimpan = $kirim_briva . ' - Data berhasil divalidasi.';
            } else {
                $ketsimpan = 'Data ini sudah pernah di validasi';
            }
        } else {
            $cek_data_spt = (new Spt())->cek_id_spt($attr['t_idspt']);
            $create = ValidasiKaban::where('t_id_validasi_kaban', $attr['t_id_validasi_kaban'])->update(
                    [
                        's_id_status_kaban' => $attr['s_id_status_kaban'],
                        't_keterangan_kaban' => $attr['t_keterangan_kaban'],
                        't_iduser_validasikaban' => $all_session->id
                    ]
            );
            if (empty($cek_data_spt->t_kodebayar_bphtb)) {
                //$kodebayar = $this->createKodeBayar($pemda, $nourutKodeBayar);
                $jenisketetapan = 1; //SSPD
                $tahun_kodebayar = date('Y');
                $kodebayar = (new Spt())->format_idbilling($tahun_kodebayar, $nourutKodeBayar, $jenisketetapan);
                
                if($cek_setmenubriva->s_id_statusmenubriva == 1){ 
                    $kodebayarnya = $briva->briva_no . $kodebayar;
                }else{
                    $kodebayarnya = $kodebayar;
                }
                
                $data_update_spt = array(
                    't_tglbuat_kodebyar' => Carbon::now(),
                    't_nourut_kodebayar' => $nourutKodeBayar,
                    't_kodebayar_bphtb' => $kodebayarnya //$briva->briva_no . $kodebayar
                );
                $data_update_spt['t_ntpd'] = $this->createNTPD($nourutKodeBayar);
                $cek_data_spt = (new Spt())->cek_id_spt($attr['t_idspt']);
                $create2 = Spt::where('t_idspt', $attr['t_idspt'])->update($data_update_spt);
                if($cek_setmenubriva->s_id_statusmenubriva == 1){ 
                    $cek_kodebayar = (new Spt())->looping_cek_kodebayar_all($briva->briva_no . $kodebayar, date('Y'), $jenisketetapan);
                }else{
                    $cek_kodebayar = (new Spt())->looping_cek_kodebayar_all($kodebayar, date('Y'), $jenisketetapan);
                }
            }
            $ketsimpan = 'Data baru berhasil diupdate.';
        }
        if ($create) {
            session()->flash('success', 'OK ' . $ketsimpan);
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('validasi-kaban');
    }

    public function detail(Request $request) {
        $detail = Spt::where('t_idspt', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }
    
    public function destroy(Request $request) {
        ValidasiKaban::where('t_idspt', '=', $request->query('id'))->delete();
        Spt::where('t_idspt', '=', $request->query('id'))->update([
            't_tglbuat_kodebyar' => NULL,
            't_nourut_kodebayar' => NULL,
            't_kodebayar_bphtb' => NULL,
            't_ntpd' => NULL
        ]);
    }

    /* Generate Token */

    function BRIVAgenerateToken($client_id, $secret_id) {
        $briva = Briva::orderBy('updated_at', 'desc')->first();
        if ($briva->url_aktif == 1) {
            $url = $briva->url_development . "/oauth/client_credential/accesstoken?grant_type=client_credentials";
        } else {
            $url = $briva->url_production . "/oauth/client_credential/accesstoken?grant_type=client_credentials";
        }

        $data = "client_id=" . $client_id . "&client_secret=" . $secret_id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $json = json_decode($result, true);
        $accesstoken = $json['access_token'];

        return $accesstoken;
    }

    public function createKodeBayar($pemda, $nourutKodeBayar) {
        $kodebayar = substr($pemda->s_kodekabkot, 1, 1) . "01" . date('y') . str_pad($nourutKodeBayar, 5, "0", STR_PAD_LEFT); //$pemda->s_kodeprovinsi . 
        return $kodebayar;
    }

    public function createNTPD($nourutKodeBayar) {
        $briva = Briva::orderBy('updated_at', 'desc')->first();
        $ntpd = $briva->briva_no . "701" . date('y') . str_pad($nourutKodeBayar, 5, "0", STR_PAD_LEFT);
        return $ntpd;
    }
    
    /* Generate signature */

    function BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret) {
        $payloads = "path=$path&verb=$verb&token=Bearer $token&timestamp=$timestamp&body=$payload";
        $signPayload = hash_hmac('sha256', $payloads, $secret, true);
        return base64_encode($signPayload);
    }

    public function BrivaCreate($kodebayar, $namawp, $totalamount) {

        $briva = Briva::orderBy('updated_at', 'desc')->first();

        $client_id = $briva->client_id;
        $secret_id = $briva->secret_id;
        $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = $briva->institution_code;
        $brivaNo = $briva->briva_no;
        $custCode = $kodebayar;
        $nama = $namawp;
        $amount = $totalamount;
        $keterangan = "BPHTB KAB KEPAHIANG";
        $expiredDate = date('Y-m-d 23:59:00');

        $datas = array(
            'institutionCode' => $institutionCode,
            'brivaNo' => $brivaNo,
            'custCode' => $custCode,
            'nama' => $nama,
            'amount' => $amount,
            'keterangan' => $keterangan,
            'expiredDate' => $expiredDate
        );

        $payload = json_encode($datas, true);
        $path = "/v1/briva";
        $verb = "POST";
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Content-Type:" . "application/json",
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        if ($briva->url_aktif == 1) {
            $urlPost = $briva->url_development . "/v1/briva";
        } else {
            $urlPost = $briva->url_production . "/v1/briva";
        }
        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chPost, CURLOPT_SSL_VERIFYPEER, false);

        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        $decode = json_decode($resultPost, true);

        if ($decode['status'] == true) {
            $messageResponse = $decode['responseCode'] . ' - ' . $decode['responseDescription'];
        } else {
            $messageResponse = $decode['responseCode'] . ' - ' . $decode['errDesc'];
        }

        logBriva::create([
            'briva_no' => $brivaNo,
            'cust_code' => $kodebayar,
            'nama' => $nama,
            'amount' => ($amount > 0) ? $amount : 0,
            'keterangan' => 'Create BRIVA BPHTB { ' . $messageResponse . ' }',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]);

        return $messageResponse;
    }
    
}
