<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Api\BphtbRequest;
use App\Models\Spt\Spt;
use App\Models\Pbb\DatObjekPajak;
use App\Models\Pbb\Sppt;
use App\Models\Logs\LogBpnapi;

class ApiController extends Controller {

    public function index(Request $request) {
        $kd_prov = substr($request->NOP, 0, 2);
        $kd_dati2 = substr($request->NOP, 2, 2);
        $kd_kec = substr($request->NOP, 4, 3);
        $kd_kel = substr($request->NOP, 7, 3);
        $kd_blok = substr($request->NOP, 10, 3);
        $kd_nourut = substr($request->NOP, 13, 4);
        $kd_jns_op = substr($request->NOP, 17, 1);
        $NOP = $kd_prov . '.' . $kd_dati2 . '.' . $kd_kec . '.' . $kd_kel . '.' . $kd_blok . '.' . $kd_nourut . '.' . $kd_jns_op;
        $NTPD = $request->NTPD;

        $data = Spt::select([
                            't_nop_sppt as NOP',
                            't_nik_pembeli as NIK',
                            't_nama_pembeli as NAMA',
                            't_jalan_pembeli as ALAMAT',
                            't_kelurahan_sppt as KELURAHAN_OP',
                            't_kecamatan_sppt as KECAMATAN_OP',
                            't_kabkota_sppt as KOTA_OP',
                            't_luastanah_sismiop as LUASTANAH',
                            't_luasbangunan_sismiop as LUASBANGUNAN',
                            't_jmlhbayar_pokok as PEMBAYARAN',
                            't_tglpembayaran_pokok as STATUS',
                            't_tglpembayaran_pokok as TANGGAL_PEMBAYARAN',
                            't_ntpd as NTPD',
                            't_tglpembayaran_pokok as JENISBAYAR',
                        ])
                        ->leftJoin('t_pembayaran_bphtb', 't_pembayaran_bphtb.t_idspt', '=', 't_spt.t_idspt')
                        ->where(['t_nop_sppt' => $NOP, 't_ntpd' => $NTPD])->first();

        if (empty($request->NOP)) {
            $message = 'NOP Tidak boleh kosong!';
        }

        if (empty($request->NTPD)) {
            $message = 'NTPD Tidak boleh kosong!';
        }


        if ($data != NULL) {
            $data['NOP'] = ($data->NOP) ? str_ireplace('.', '', $data->NOP) : "";
            $data['PEMBAYARAN'] = ($data->TANGGAL_PEMBAYARAN) ? $data->PEMBAYARAN : "0";
            $data['STATUS'] = ($data->TANGGAL_PEMBAYARAN) ? 'Y' : 'T';
            $data['JENISBAYAR'] = ($data->TANGGAL_PEMBAYARAN) ? 'L' : 'H';
            $data['TANGGAL_PEMBAYARAN'] = ($data->TANGGAL_PEMBAYARAN) ? date('d/m/Y', strtotime($data->TANGGAL_PEMBAYARAN)) : '';
            $ar_data = $data;
            $message = 'OK';

            $response = [
                'result' => $ar_data,
                'respon_code' => $message
            ];
        } else {
            $ar_data = '';
            $getNop = Spt::where(['t_nop_sppt' => $NOP])->count();
            $getNtpd = Spt::where(['t_ntpd' => $NTPD])->count();

            if (empty($getNop) && empty($getNtpd)) {
                $message = 'Data tidak ditemukan';
            } else {
                if (empty($getNop)) {
                    $message = 'NOP tidak ditemukan';
                } elseif (empty($getNtpd)) {
                    $message = 'NTPD tidak ditemukan';
                }
            }

            $response = [
                'respon_code' => $message
            ];
        }


        LogBpnapi::insert([
            'timestamp' => now(),
            'message' => $request . ' Response Code "' . $message . '", ' . $ar_data,
            'created_at' => now(),
            'updated_at' => now()
        ]);

        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function pbb(Request $request) {
        $kd_prov = substr($request->NOP, 0, 2);
        $kd_dati2 = substr($request->NOP, 2, 2);
        $kd_kec = substr($request->NOP, 4, 3);
        $kd_kel = substr($request->NOP, 7, 3);
        $kd_blok = substr($request->NOP, 10, 3);
        $kd_nourut = substr($request->NOP, 13, 4);
        $kd_jns_op = substr($request->NOP, 17, 1);

        $data = DatObjekPajak::select([
                            'SPPT.KD_PROPINSI AS NOP',
                            'DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID AS NIK',
                            'DAT_SUBJEK_PAJAK.NM_WP AS NAMA_WP',
                            'DAT_OBJEK_PAJAK.JALAN_OP AS ALAMAT_OP',
                            'REF_KECAMATAN.NM_KECAMATAN AS KECAMATAN_OP',
                            'SPPT.KELURAHAN_WP_SPPT AS KELURAHAN_OP',
                            'SPPT.KOTA_WP_SPPT AS KOTA_OP',
                            'SPPT.LUAS_BUMI_SPPT AS LUASTANAH_OP',
                            'SPPT.LUAS_BNG_SPPT AS LUASBANGUNAN_OP',
                            'SPPT.NJOP_BUMI_SPPT AS NJOP_TANAH_OP',
                            'SPPT.NJOP_BNG_SPPT AS NJOP_BANGUNAN_OP',
                            'SPPT.KD_PROPINSI AS STATUS_TUNGGAKAN'
                        ])
                        ->leftJoin('DAT_SUBJEK_PAJAK', 'DAT_OBJEK_PAJAK.SUBJEK_PAJAK_ID', '=', 'DAT_SUBJEK_PAJAK.SUBJEK_PAJAK_ID')
                        ->leftJoin('SPPT', [
                            ['DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                            ['DAT_OBJEK_PAJAK.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                            ['DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                            ['DAT_OBJEK_PAJAK.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN'],
                            ['DAT_OBJEK_PAJAK.KD_BLOK', '=', 'DAT_OBJEK_PAJAK.KD_BLOK'],
                            ['DAT_OBJEK_PAJAK.NO_URUT', '=', 'DAT_OBJEK_PAJAK.NO_URUT'],
                            ['DAT_OBJEK_PAJAK.KD_JNS_OP', '=', 'DAT_OBJEK_PAJAK.KD_JNS_OP']
                        ])
                        ->leftJoin('REF_KECAMATAN', [
                            ['DAT_OBJEK_PAJAK.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                            ['DAT_OBJEK_PAJAK.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                            ['DAT_OBJEK_PAJAK.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                        ])
                        ->where([
                            'DAT_OBJEK_PAJAK.KD_PROPINSI' => $kd_prov,
                            'DAT_OBJEK_PAJAK.KD_DATI2' => $kd_dati2,
                            'DAT_OBJEK_PAJAK.KD_KECAMATAN' => $kd_kec,
                            'DAT_OBJEK_PAJAK.KD_KELURAHAN' => $kd_kel,
                            'DAT_OBJEK_PAJAK.KD_BLOK' => $kd_blok,
                            'DAT_OBJEK_PAJAK.NO_URUT' => $kd_nourut,
                            'DAT_OBJEK_PAJAK.KD_JNS_OP' => $kd_jns_op
                        ])->first();

        $countTunggakan = Sppt::where([
                            'KD_PROPINSI' => $kd_prov,
                            'KD_DATI2' => $kd_dati2,
                            'KD_KECAMATAN' => $kd_kec,
                            'KD_KELURAHAN' => $kd_kel,
                            'KD_BLOK' => $kd_blok,
                            'NO_URUT' => $kd_nourut,
                            'KD_JNS_OP' => $kd_jns_op
                        ])->count();
        $countPembayaran = Sppt::where([
                            'KD_PROPINSI' => $kd_prov,
                            'KD_DATI2' => $kd_dati2,
                            'KD_KECAMATAN' => $kd_kec,
                            'KD_KELURAHAN' => $kd_kel,
                            'KD_BLOK' => $kd_blok,
                            'NO_URUT' => $kd_nourut,
                            'KD_JNS_OP' => $kd_jns_op,
                            'STATUS_PEMBAYARAN_SPPT' => '1'
                        ])->count();
        $statusTunggakan = ($countPembayaran/$countTunggakan*100);
        
        if (empty($request->NOP)) {
            $message = 'NOP Tidak boleh kosong!';
        }
        
        if ($data != NULL) {
            $data['nop'] = $request->NOP;
            $data['status_tunggakan'] = number_format(round($statusTunggakan),0,',','.').'% LUNAS';
            
            $ar_data = $data;
            $message = 'OK';
            
            $response = [
                'result' => $ar_data,
                'respon_code' => $message
            ];
        } else {
            $ar_data = "";
            $response = [
                'respon_code' => $message
            ];
        }

        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

}
