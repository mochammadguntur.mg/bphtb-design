<?php

namespace App\Http\Controllers\Permissions;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class AssignController extends Controller
{

    public function index()
    {
        return view('permission.assign.index', [
            'roles' => Role::get(),
            'permissions' => Permission::get()
        ]);
    }

    public function create()
    {
        return view('permission.assign.create', [
            'roles' => Role::get(),
            'permissions' => Permission::get()
        ]);
    }

    public function store()
    {
        request()->validate([
            'role' => 'required',
            'permissions' => 'array|required'
        ]);
        $role = Role::find(request('role'));
        $role->givePermissionTo(request('permissions'));
        return back()->with('success', "Permissions has be assigned to the role {$role->name}");
    }

    public function edit(Role $role)
    {
        return view('permission.assign.sync', [
            'role' => $role,
            'roles' => Role::get(),
            'permissions' => Permission::get()
        ]);
    }

    public function update(Role $role)
    {
        request()->validate([
            'role' => 'required',
            'permissions' => 'array|required'
        ]);
        $role->syncPermissions(request('permissions'));
        return redirect()->route('assign.index')->with('success', "Permissions has be synced.");
    }

    public function datagrid(Request $request)
    {
        $response = new Role;
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('id');
        }

        if ($request['filter']['name'] != null) {
            $response = $response->where('name', 'like', "%" . $request['filter']['name'] . "%");
        }
        if ($request['filter']['guard_name'] != null) {
            $response = $response->where('guard_name', 'like', "%" . $request['filter']['guard_name'] . "%");
        }
        if ($request['filter']['the_permissions'] != null) {
            $response = $response->where('the_permissions', 'like', "%" . $request['filter']['the_permissions'] . "%");
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'name' => $v['name'],
                'guard_name' => $v['guard_name'],
                'the_permissions' => implode(", ", $v->getPermissionNames()->toArray()),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'assignable/' . $v['id'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['id'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request)
    {
        $role = Role::where('id', '=', $request->query('id'))->get();
        return response()->json(
            $role,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function delete(Request $request){
        Role::where('id', '=', $request->query('id'))->delete();
    }


}
