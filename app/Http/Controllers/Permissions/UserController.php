<?php

namespace App\Http\Controllers\Permissions;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{
    public function index()
    {
        return view('permission.assign.user.index', [
            'roles' => Role::get(),
            'users' => User::has('roles')->get()
        ]);

    }

    public function create()
    {
        $roles = Role::get();
        $users = User::has('roles')->get();
        return view('permission.assign.user.create', compact('roles', 'users'));
    }

    public function store()
    {
        $user = User::where('email', request('email'))->first();
        $user->assignRole(request('roles'));
        return back();
    }

    public function edit(User $user)
    {
        return view('permission.assign.user.edit', [
            'user' => $user,
            'roles' => Role::get(),
            'users' => User::has('roles')->get()
        ]);
    }

    public function update(User $user){
        $user->syncRoles(request('roles'));
        return redirect()->route('assign.user.index');
    }

    public function datagrid(Request $request)
    {
        $response = new User;
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('id');
        }

        if ($request['filter']['name'] != null) {
            $response = $response->where('name', 'like', "%" . $request['filter']['name'] . "%");
        }
        if ($request['filter']['the_roles'] != null) {
            $response = $response->where('the_roles', 'like', "%" . $request['filter']['the_roles'] . "%");
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'name' => $v['name'],
                'the_roles' => implode(", ", $v->getRoleNames()->toArray()),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'user/' . $v['id'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['id'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request)
    {
        $role = Role::where('id', '=', $request->query('id'))->get();
        return response()->json(
            $role,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function delete(Request $request){
        Role::where('id', '=', $request->query('id'))->delete();
    }
}
