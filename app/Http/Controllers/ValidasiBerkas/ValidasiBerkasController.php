<?php

namespace App\Http\Controllers\ValidasiBerkas;

use App\Http\Controllers\Controller;
use App\Http\Requests\ValidasiBerkas\ValidasiBerkasRequest;
use App\Http\Requests\ValidasiKabid\ValidasiKabidRequest;
use Illuminate\Http\Request;
use App\Models\Spt\Spt;
use App\Models\ValidasiBerkas\ValidasiBerkas;
use Auth;
use Illuminate\Support\Facades\Auth as FacadesAuth;

class ValidasiBerkasController extends Controller {

    const LENGKAP = 1;
    const BELUM_LENGKAP = 2;

    public function index() {
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        $data_statusberkas = (new Spt())->cekdata_data_status_berkas();
        $data_statuskabid = (new Spt())->cekdata_data_status_kabid();
        $data_statusbayar = (new Spt())->cekdata_data_status_bayar();
        $data_jenisfasilitas = (new Spt())->cekdata_jenis_fasilitas();
        $data_haktanah = (new Spt())->cekdata_jenis_haktanah();
        $data_doktanah = (new Spt())->cekdata_jenis_doktanah();
        $data_jenispengurangan = (new Spt())->cekdata_jenis_pengurangan();
        $data_jenisperumahan = (new Spt())->cekdata_jenis_perumahan_tanah();
        $data_jenisketetapan = (new Spt())->cekdata_jenis_ketetapan();
        $countValidasiBelum = (new ValidasiBerkas())->dataGridBelum()->count();
        $countValidasiSetuju = ValidasiBerkas::where(["s_id_status_berkas" => "1"])->get()->count();
        $countValidasiTidakSetuju = ValidasiBerkas::where(["s_id_status_berkas" => "2"])->get()->count();
        $view = view(
                'validasi-berkas.index', [
            'data_notaris' => $data_notaris,
            'data_jenistransaksi' => $data_jenistransaksi,
            'data_statusberkas' => $data_statusberkas,
            'data_statuskabid' => $data_statuskabid,
            'data_statusbayar' => $data_statusbayar,
            'data_jenisbidangusaha' => $data_jenisbidangusaha,
            'data_jenisfasilitas' => $data_jenisfasilitas,
            'data_haktanah' => $data_haktanah,
            'data_doktanah' => $data_doktanah,
            'data_jenispengurangan' => $data_jenispengurangan,
            'data_jenisperumahan' => $data_jenisperumahan,
            'data_jenisketetapan' => $data_jenisketetapan,
            'countValidasiBelum' => $countValidasiBelum,
            'countValidasiSetuju' => $countValidasiSetuju,
            'countValidasiTidakSetuju' => $countValidasiTidakSetuju,
                ]
        );
        return $view;
    }

    public function datagrid(Request $request) {
        $all_session = Auth::user();
        $response = (new ValidasiBerkas())->dataGridValidasiBerkasSudah();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_idspt');
        }
        if ($request['filter']["jenistransaksi"] != "") {
            $response = $response->where('t_spt.t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']["kodebayar"] != "") {
            $response = $response->where('t_spt.t_kodebayar_bphtb', '=', $request['filter']['kodebayar']);
        }
        if ($request['filter']["ntpd"] != "") {
            $response = $response->where('t_spt.t_kodebayar_bphtb', '=', $request['filter']['ntpd']);
        }
        if ($request['filter']["namawp"] != "") {
            $response = $response->where("t_spt.t_nama_pembeli", 'ilike', "%" . $request['filter']["namawp"] . "%");
        }
        if ($request['filter']["nodaftar"] != "") {
            $response = $response->where('t_spt.t_kohirspt', '=', $request['filter']['nodaftar']);
        }
        if ($request['filter']["nop"] != "") {
            $response = $response->where('t_spt.t_nop_sppt', '=', $request['filter']['nop']);
        }
        if ($request['filter']["notaris"] != "") {
            $response = $response->where('t_spt.t_idnotaris_spt', '=', $request['filter']['notaris']);
        }
        if ($request['filter']['status_pembayaran'] != null) {
            if ($request['filter']['status_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['status_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        if ($request['filter']['status_persetujuan'] != null) {
            $response = ((($request['filter']['status_persetujuan'] == 1)) ? $response->where('t_idpersetujuan_bphtb', '=', $request['filter']['status_persetujuan']) : $response->whereNull('t_idpersetujuan_bphtb'));
        }
        if ($request['filter']['status_validasi_berkas'] != null) {
            if ($request['filter']['status_validasi_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['status_validasi_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['status_validasi_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }
        if ($request['filter']['status_validasi_kabid'] != null) {
            if ($request['filter']['status_validasi_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['status_validasi_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['status_validasi_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }
        if ($request['filter']['tahun'] != null) {
            $response = $response->where('t_periodespt', 'like', "%" . $request['filter']['tahun'] . "%");
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $edit_validasi = '<a href="' . url('validasi-berkas') . '/formvalidasiberkas/' . $v['t_uuidspt'].'" class="btn btn-warning btn-block btn-xs" title="EDIT"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
            if (!empty($v['t_tgldaftar_spt'])) {
                $tgl_daftar = $v['t_tgldaftar_spt'];
            } else {
                $tgl_daftar = '';
            }
            if (!empty($v['s_id_status_berkas'])) {
                if ($v['s_id_status_berkas'] == 1) {
                    $edit_validasi = "";
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                } else {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button><small class="text-warning">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                }
            } else {
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }
            if (!empty($v['s_id_status_kabid'])) {
                if ($v['s_id_status_kabid'] == 1) {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SETUJU</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                    $cetak_sspd = '<a href="' . url('pendaftaran') . '/' . $v['t_idspt'] . '/cetaksspdbphtb" target="_blank" class="btn btn-primary btn-block btn-xs" title="CETAK SSPD"><i class="fa fa-fw fa-print"></i> CETAK SSPD</a>';
                } else {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK SETUJU</button><small class="text-warning">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                    $cetak_sspd = '';
                }
            } else {
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
                $cetak_sspd = '';
            }
            if (!empty($v['t_tglpembayaran_pokok'])) {
                $status_bayar = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> SUDAH</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglpembayaran_pokok'])).'</small>';
            } else {
                $status_bayar = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';
            }
            if (!empty($v['t_idpersetujuan_bphtb'])) {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])).'</small>';
            } else {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>';
            }
            $hapus_validasi = ($all_session['s_id_hakakses'] == 1) ? 
                    (!empty($v['s_id_status_kabid']) ? '<a onclick="javascript:showAlertDialog(' . $v['t_idspt'] . ')" class="btn btn-block bg-danger btn-sm" title="Batal Validasi Berkas"><i class="fa fa-fw fa-trash"></i> Batal Validasi</a>' : '<a onclick="javascript:showDeleteDialog(' . $v['t_idspt'] . ')" class="btn btn-block bg-danger btn-sm" title="Batal Validasi Berkas"><i class="fa fa-fw fa-trash"></i> Batal Validasi</a>') 
                    : '';
            $perintah = '<div class="dropdown">
                    <button onclick="myFunction(' . $v['t_idspt'] . ')" class="dropbtn btn-primary dropdown-toggle btn btn-xs">PERINTAH <span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $v['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:80px; border-color: blue;">
                            ' . $edit_validasi . '
                            ' . $cetak_sspd . '
                            ' . $hapus_validasi . '
                    </div>
                </div>';
            $dataArr[] = [
                't_idspt' => $perintah,
                't_kohirspt' => $v['t_kohirspt'],
                't_tgldaftar_spt' => $tgl_daftar,
                't_periodespt' => $v['t_periodespt'],
                't_idnotaris_spt' => $v['s_namanotaris'],
                't_idjenistransaksi' => $v['s_namajenistransaksi'],
                't_nama_pembeli' => $v['t_nama_pembeli'],
                't_nop_sppt' => $v['t_nop_sppt'],
                't_nilai_bphtb_fix' => number_format($v['t_nilai_bphtb_fix'], 0, ',', '.'),
                't_idpersetujuan_bphtb' => $status_persetujuan,
                't_id_validasi_berkas' => $status_validasi_berkas,
                't_id_validasi_kabid' => $status_validasi_kabid,
                't_id_pembayaran' => $status_bayar,
                't_kodebayar_bphtb' => !empty($v['t_kodebayar_bphtb']) ? $v['t_kodebayar_bphtb'] : '',
                'ntpd' => !empty($v['t_ntpd']) ? $v['t_ntpd'] : ''
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function datagridpilihspt(Request $request) {
        $response = (new ValidasiBerkas())->dataGridBelum();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_idspt');
        }
        if ($request['filter']["jenistransaksi"] != "") {
            $response = $response->where('t_spt.t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']["kodebayar"] != "") {
            $response = $response->where('t_spt.t_kodebayar_bphtb', '=', $request['filter']['kodebayar']);
        }
        if ($request['filter']["ntpd"] != "") {
            $response = $response->where('t_spt.t_kodebayar_bphtb', '=', $request['filter']['ntpd']);
        }
        if ($request['filter']["namawp"] != "") {
            $response = $response->where("t_spt.t_nama_pembeli", 'ilike', "%" . $request['filter']["namawp"] . "%");
        }
        if ($request['filter']["nodaftar"] != "") {
            $response = $response->where('t_spt.t_kohirspt', '=', $request['filter']['nodaftar']);
        }
        if ($request['filter']["nop"] != "") {
            $response = $response->where('t_spt.t_nop_sppt', '=', $request['filter']['nop']);
        }
        if ($request['filter']["notaris"] != "") {
            $response = $response->where('t_spt.t_idnotaris_spt', '=', $request['filter']['notaris']);
        }
        if ($request['filter']['status_pembayaran'] != null) {
            if ($request['filter']['status_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['status_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        if ($request['filter']['status_persetujuan'] != null) {
            $response = ((($request['filter']['status_persetujuan'] == 1)) ? $response->where('t_idpersetujuan_bphtb', '=', $request['filter']['status_persetujuan']) : $response->whereNull('t_idpersetujuan_bphtb'));
        }
        if ($request['filter']['status_validasi_berkas'] != null) {
            if ($request['filter']['status_validasi_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['status_validasi_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['status_validasi_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }
        if ($request['filter']['status_validasi_kabid'] != null) {
            if ($request['filter']['status_validasi_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['status_validasi_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['status_validasi_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }
        if ($request['filter']['tahun'] != null) {
            $response = $response->where('t_periodespt', 'like', "%" . $request['filter']['tahun'] . "%");
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            if (!empty($v['t_tgldaftar_spt'])) {
                $tgl_daftar = $v['t_tgldaftar_spt'];
            } else {
                $tgl_daftar = '';
            }
            if (!empty($v['s_id_status_berkas'])) {
                if ($v['s_id_status_berkas'] == 1) {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                } else {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button><small class="text-warning">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                }
            } else {
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }
            if (!empty($v['s_id_status_kabid'])) {
                if ($v['s_id_status_kabid'] == 1) {
                    $edit_validasi = '';
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SETUJU</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                    $cetak_sspd = '<a href="' . url('pendaftaran') . '/' . $v['t_idspt'] . '/cetaksspdbphtb" target="_blank" class="btn btn-primary btn-block btn-xs" title="CETAK SSPD"><i class="fa fa-fw fa-print"></i> CETAK SSPD</a>';
                } else {
                    $edit_validasi = '<a href="' . url('validasi-berkas') . '/formvalidasiberkas/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK SETUJU</button><small class="text-warning">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                    $cetak_sspd = '';
                }
            } else {
                $edit_validasi = '<a href="' . url('validasi-berkas') . '/formvalidasiberkas/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
                $cetak_sspd = '';
            }
            if (!empty($v['t_tglpembayaran_pokok'])) {
                $status_bayar = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> SUDAH</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglpembayaran_pokok'])).'</small>';
            } else {
                $status_bayar = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';
            }
            if (!empty($v['t_idpersetujuan_bphtb'])) {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])).'</small>';
            } else {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>';
            }
            $perintah = '<a href="' . url('validasi-berkas') . '/formvalidasiberkas/' . $v['t_uuidspt'] . '" class="btn btn-warning btn-block btn-xs" title="PILIH"><i class="fa fa-fw fa-check"></i> VALIDASI</a>';
            $dataArr[] = [
                't_idspt' => $perintah,
                't_kohirspt' => $v['t_kohirspt'],
                't_tgldaftar_spt' => $tgl_daftar,
                't_periodespt' => $v['t_periodespt'],
                't_idnotaris_spt' => $v['s_namanotaris'],
                't_idjenistransaksi' => $v['s_namajenistransaksi'],
                't_nama_pembeli' => $v['t_nama_pembeli'],
                't_nop_sppt' => $v['t_nop_sppt'],
                't_nilai_bphtb_fix' => number_format($v['t_nilai_bphtb_fix'], 0, ',', '.'),
                't_idpersetujuan_bphtb' => $status_persetujuan,
                't_id_validasi_berkas' => $status_validasi_berkas,
                't_id_validasi_kabid' => $status_validasi_kabid,
                't_id_pembayaran' => $status_bayar,
                't_kodebayar_bphtb' => !empty($v['t_kodebayar_bphtb']) ? $v['t_kodebayar_bphtb'] : '',
                'ntpd' => !empty($v['t_ntpd']) ? $v['t_ntpd'] : ''
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function formvalidasiberkas($uuid) {
        $cek_data_spt = (new Spt())->cek_uuid_spt($uuid);
        $data_notaris = (new Spt())->cekdata_notaris();
        $data_jenistransaksi = (new Spt())->cekdata_jenis_transaksi();
        $data_jenisbidangusaha = (new Spt())->cekdata_jenis_bidangusaha();
        $cek_persyaratan = (new Spt())->cek_all_data_syarat_dan_file_upload($cek_data_spt->t_idspt, $cek_data_spt->t_idjenistransaksi);
        $data_statusberkas = (new Spt())->cekdata_data_status_berkas();
        $data_nik_bersama = (new Spt())->cek_data_nik($cek_data_spt->t_idspt);
        $data_fotoobjek = (new Spt())->cek_foto_objek($cek_data_spt->t_idspt);
        $all_session = FacadesAuth::user();
        $persyaratanCentang = [];
        if ($cek_data_spt->t_persyaratan_validasi_berkas != null)
            $persyaratanCentang = explode(",", $cek_data_spt->t_persyaratan_validasi_berkas);
        $view = view(
                'validasi-berkas.formvalidasiberkas', [
            'dataspt' => $cek_data_spt,
            'data_notaris' => $data_notaris,
            'nik_bersama' => $data_nik_bersama,
            'data_jenistransaksi' => $data_jenistransaksi,
            'data_jenisbidangusaha' => $data_jenisbidangusaha,
            'data_kecamatan' => (new Spt())->cekdata_kecamatan(),
            'cek_persyaratan' => $cek_persyaratan,
            'data_statusberkas' => $data_statusberkas,
            'data_fotoobjek' => $data_fotoobjek,
            'all_session' => $all_session,
            'persyaratanCentang' => $persyaratanCentang
                ]
        );
        return $view;
    }

    public function simpanvalidasiberkas(Request $Request) {
        $all_session = FacadesAuth::user();
        $attr = $Request->all();
        if (!empty($attr["t_persyaratan_validasi_berkas"])) {
            $checkPersyaratan = $this->checkPersyaratan($attr["t_idjenistransaksi"], $attr["t_persyaratan_validasi_berkas"]);
        } else {
            $checkPersyaratan = $this->checkPersyaratan($attr["t_idjenistransaksi"], []);
        }
//        if(($checkPersyaratan["status"] == $this::LENGKAP) && ($attr["s_id_status_berkas"] == $this::LENGKAP)){
        if (($checkPersyaratan["status"] == $this::LENGKAP)) {
            $s_id_status_berkas = $this::LENGKAP;
            $validated = $Request->all();
        } else {
            $s_id_status_berkas = $this::BELUM_LENGKAP;
            $validated = $Request->validate([
                "t_keterangan_berkas" => "required",
            ]);
        }
        $idValidasiBerkas = ValidasiBerkas::where("t_idspt", $attr["t_idspt"])->first();
        if (empty($idValidasiBerkas['t_id_validasi_berkas'])) {
            $data = [
                't_idspt' => $attr['t_idspt'],
                's_id_status_berkas' => $s_id_status_berkas,
                't_tglvalidasi' => date('Y-m-d H:i:s'),
                't_keterangan_berkas' => $attr['t_keterangan_berkas'],
                't_iduser_validasi' => $all_session->id,
                't_persyaratan_validasi_berkas' => ((!empty($attr["t_persyaratan_validasi_berkas"])) ? implode(',', $attr["t_persyaratan_validasi_berkas"]) : null)
            ];
            $create = ValidasiBerkas::create($data);
            $ketsimpan = 'Data baru berhasil ditambahkan.';
        } else {
            $data = [
                's_id_status_berkas' => $s_id_status_berkas,
                't_keterangan_berkas' => $attr['t_keterangan_berkas'],
                't_iduser_validasi' => $all_session->id,
                't_persyaratan_validasi_berkas' => ((!empty($attr["t_persyaratan_validasi_berkas"])) ? implode(',', $attr["t_persyaratan_validasi_berkas"]) : null)
            ];
            $create = ValidasiBerkas::where('t_id_validasi_berkas', $attr['t_id_validasi_berkas'])->update($data);
            $ketsimpan = 'Data baru berhasil diupdate.';
        }
        if ($create) {
            session()->flash('success', 'OK ' . $ketsimpan);
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('validasi-berkas');
    }

    public function detail(Request $request) {
        $detail = Spt::where('t_idspt', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }
    
    public function destroy(Request $request) {
        ValidasiBerkas::where('t_idspt', '=', $request->query('id'))->delete();
    }

    public function checkPersyaratan($idJenisTransaksi, $arPersyaratanChecklist) {
        $dataPersyaratan = (new SPT())->cekdata_id_syaratupload($idJenisTransaksi);
        $arSyaratSudah = [];
        $arSyaratBelum = [];
        $checkSyarat = [];
        $status = $this::BELUM_LENGKAP;
        foreach ($dataPersyaratan as $row) {
            if (in_array($row->s_idpersyaratan, $arPersyaratanChecklist)) {
                $arSyaratSudah[] = [
                    "idSyarat" => $row->s_idpersyaratan,
                    "namaSyarat" => $row->s_namapersyaratan
                ];
            } else {
                $arSyaratBelum[] = [
                    "idSyarat" => $row->s_idpersyaratan,
                    "namaSyarat" => $row->s_namapersyaratan
                ];
            }
            $checkSyarat[] = $row->s_idpersyaratan;
        }
        if ((implode(',', $arPersyaratanChecklist)) == (implode(',', $checkSyarat)))
            $status = $this::LENGKAP;
        return [
            "status" => $status,
            "arSyaratSudah" => $arSyaratSudah,
            "arSyaratBelum" => $arSyaratBelum,
        ];
    }

}
