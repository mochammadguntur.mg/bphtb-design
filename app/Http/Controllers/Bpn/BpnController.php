<?php

namespace App\Http\Controllers\Bpn;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Bpn\BpnApi;
use App\Models\Bpn\Bpn;
use App\Models\Spt\Spt;
use App\Http\Requests\Bpn\BpnApiRequest;
use App\Http\Requests\Bpn\BpnRequest;
use App\Exports\Bpn\BpnExport;
use App\Exports\Bpn\BpnApiExport;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;

class BpnController extends Controller
{

    public function index()
    {
        return view('bpn.index');
    }

    public function password()
    {
        return view('bpn.password',[
            'bpnapi' => BpnApi::select()->where('s_idusers_api','=',1)->get()
        ]);
    }

    public function change(BpnApiRequest $bpnApiRequest, BpnApi $bpnApi){

        $attr = $bpnApiRequest->all();

        $passOld = $bpnApi->select()->where('s_password','=',$attr['s_passold'])->get();

        if(count($passOld) != 0){
            if($attr['s_password'] == $attr['s_password2']){
                $curl = curl_init();

                curl_setopt_array($curl, array(
                CURLOPT_URL => "https://services.atrbpn.go.id/BPNApiService/Api/BPHTB/ChangePassword",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_SSL_VERIFYPEER => false,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 30,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_POSTFIELDS => "{\r\n\"username\": \"".$passOld[0]->s_username."\",\r\n\"password\": \"".$passOld[0]->s_password."\",\r\n\"newpassword\": \"".$attr['s_password2']."\"\r\n}",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/json",
                    ),
                ));

                $response = curl_exec($curl);
                @$decode = json_decode($response, true);

                if($decode['respon_code'] == 'OK'){

                    $update = $bpnApi->where('s_idusers_api',1)->update([
                        's_password' => $attr['s_password2'],
                    ]);

                    if($update){
                        session()->flash('success', 'Password Behasil diupdate.');
                    }else{
                        session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
                    }
                }else{
                    session()->flash('error', $decode['respon_code']);
                }

            }else{
                session()->flash('error', 'Password Baru dan ulangi password anda tidak sama!');
            }
        }else{
            session()->flash('error', 'Password Lama anda tidak sama!');
        }

        return redirect('bpn/password');
    }

    public function caridata(Request $request)
    {
        $tgltransaksi = date('d/m/Y', strtotime($request->query('tgltransaksi')));

        $userApi = BpnApi::select()->where('s_idusers_api','=',1)->get();

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://services.atrbpn.go.id/BPNApiService/Api/BPHTB/getDataATRBPN",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n\t\"username\": \"".$userApi[0]->s_username."\",\n\t\"password\": \"".$userApi[0]->s_password."\",\n\t\"TANGGAL\":\"".$tgltransaksi."\"\n}",
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json"
            ],
        ]);

        $response = curl_exec($curl);

        @$decode = json_decode($response, true);

        $grid = "";
        if(!empty($decode['result'])){
        $counter = 1;
        foreach ($decode['result'] as $row) {
            $grid .= '<tr>';
            $grid .= '<td>'.$counter++.'.<input type="hidden" name="t_tgltransaksi" value="'.$tgltransaksi.'"></td>
                <td>'.strtoupper($row['NOMOR_AKTA']).'<input type="hidden" name="t_noakta[]" value="'.strtoupper($row['NOMOR_AKTA']).'"></td>
                <td>'.strtoupper($row['TANGGAL_AKTA']).'<input type="hidden" name="t_tglakta[]" value="'.strtoupper($row['TANGGAL_AKTA']).'"></td>
                <td>'.strtoupper($row['NAMA_PPAT']).'<input type="hidden" name="t_namappat[]" value="'.strtoupper($row['NAMA_PPAT']).'"></td>
                <td>'.strtoupper($row['NOP']).'<input type="hidden" name="t_nop[]" value="'.strtoupper($row['NOP']).'"></td>
                <td>'.strtoupper($row['NTPD']).'<input type="hidden" name="t_ntpd[]" value="'.strtoupper($row['NTPD']).'"></td>
                <td>'.strtoupper($row['NOMOR_INDUK_BIDANG']).'<input type="hidden" name="t_nib[]" value="'.strtoupper($row['NOMOR_INDUK_BIDANG']).'"></td>
                <td>'.strtoupper($row['NIK']).'<input type="hidden" name="t_nik[]" value="'.strtoupper($row['NIK']).'"></td>
                <td>'.strtoupper($row['NPWP']).'<input type="hidden" name="t_npwp[]" value="'.strtoupper($row['NPWP']).'"></td>
                <td>'.strtoupper($row['NAMA_WP']).'<input type="hidden" name="t_namawp[]" value="'.strtoupper($row['NAMA_WP']).'"></td>
                <td>'.strtoupper($row['KELURAHAN_OP']).'<input type="hidden" name="t_kelurahanop[]" value="'.strtoupper($row['KELURAHAN_OP']).'"></td>
                <td>'.strtoupper($row['KECAMATAN_OP']).'<input type="hidden" name="t_kecamatanop[]" value="'.strtoupper($row['KECAMATAN_OP']).'"></td>
                <td>'.strtoupper($row['KOTA_OP']).'<input type="hidden" name="t_kotaop[]" value="'.strtoupper($row['KOTA_OP']).'"></td>
                <td align="right">'.number_format($row['LUASTANAH_OP'], 2, ",", ".").'<input type="hidden" name="t_luastanahop[]" value="'.$row['LUASTANAH_OP'].'"></td>
                <td>'.strtoupper($row['JENIS_HAK']).'<input type="hidden" name="t_jenishak[]" value="'.strtoupper($row['JENIS_HAK']).'"></td>
                <td>'.strtoupper($row['KOORDINAT_X']).'<input type="hidden" name="t_koordinat_x[]" value="'.strtoupper($row['KOORDINAT_X']).'"></td>
                <td>'.strtoupper($row['KOORDINAT_Y']).'<input type="hidden" name="t_koordinat_y[]" value="'.strtoupper($row['KOORDINAT_Y']).'"></td>';
            $grid .= '</tr>';
        };
        }else{
            $grid .= '';
        }

        return response()->json(
            $grid,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function store(BpnRequest $bpnRequest){
        $attr = $bpnRequest->all();

        $tgltransaksi = Bpn::where('t_tgltransaksi','=', $attr['t_tgltransaksi'])->get();
        if(count($tgltransaksi) > 0){
            Bpn::where('t_tgltransaksi','=', $attr['t_tgltransaksi'])->delete();
        }

        for ($i = 0; count($attr['t_noakta']) > $i; $i++) {
            $create = Bpn::create([
                't_tgltransaksi' => Carbon::createFromFormat('d/m/Y',$attr['t_tgltransaksi'])->format('Y-m-d'),
                't_noakta' => $attr['t_noakta'][$i],
                't_tglakta' => Carbon::createFromFormat('d/m/Y',$attr['t_tglakta'][$i])->format("Y-m-d"),
                't_namappat' => $attr['t_namappat'][$i],
                't_nop' => $attr['t_nop'][$i],
                't_ntpd' => $attr['t_ntpd'][$i],
                't_nib' => $attr['t_nib'][$i],
                't_nik' => $attr['t_nik'][$i],
                't_npwp' => $attr['t_npwp'][$i],
                't_namawp' => $attr['t_namawp'][$i],
                't_kelurahanop' => $attr['t_kelurahanop'][$i],
                't_kecamatanop' => $attr['t_kecamatanop'][$i],
                't_kotaop' => $attr['t_kotaop'][$i],
                't_luastanahop' => $attr['t_luastanahop'][$i],
                't_jenishak' => $attr['t_jenishak'][$i],
                't_koordinat_x' => $attr['t_koordinat_x'][$i],
                't_koordinat_y' => $attr['t_koordinat_y'][$i],
            ]);
        }

        if($create){
            session()->flash('success', 'Data baru berhasil ditambahkan.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('bpn');

    }

    public function map(Request $request)
    {
        $tgltransaksi = date('d/m/Y', strtotime($request->query('tgltransaksi')));

        $userApi = BpnApi::select()->where('s_idusers_api','=',1)->get();

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://services.atrbpn.go.id/BPNApiService/Api/BPHTB/getDataATRBPN",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n\t\"username\": \"".$userApi[0]->s_username."\",\n\t\"password\": \"".$userApi[0]->s_password."\",\n\t\"TANGGAL\":\"".$tgltransaksi."\"\n}",
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json"
            ],
        ]);

        $response = curl_exec($curl);

        @$decode = json_decode($response, true);
        // $location = $decode['result'];

        $res = [
            'data' => $decode['result'],
            'count' => count($decode['result'])
        ];

        return response()->json(
            $res,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function databpn()
    {
        return view('bpn.databpn');
    }

    public function datagrid(Request $request){
        $response = new Bpn();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('t_idbpn');
        }

        if ($request['filter']['akta'] != null) {
            $response = $response->where('t_noakta', 'like', "%" . $request['filter']['akta']. "%")
                                            ->orWhere('t_tglakta', 'like', "%" . $request['filter']['akta']. "%");
        }
        if ($request['filter']['ppat'] != null) {
            $response = $response->where('t_namappat', 'ilike', "%" . $request['filter']['ppat'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['ntpd'] != null) {
            $response = $response->where('t_ntpd', 'like', "%" . $request['filter']['ntpd'] . "%");
        }
        if ($request['filter']['nib'] != null) {
            $response = $response->where('t_nib', 'like', "%" . $request['filter']['nib'] . "%");
        }
        if ($request['filter']['nik'] != null) {
            $response = $response->where('t_nik', 'like', "%" . $request['filter']['nik'] . "%");
        }
        if ($request['filter']['npwp'] != null) {
            $response = $response->where('t_npwp', 'like', "%" . $request['filter']['npwp'] . "%");
        }
        if ($request['filter']['namawp'] != null) {
            $response = $response->where('t_namawp', 'ilike', "%" . $request['filter']['namawp'] . "%");
        }
        if ($request['filter']['alamatop'] != null) {
            $response = $response->where('t_kelurahanop', 'ilike', "%" . $request['filter']['alamatop'] . "%")
                    ->orWhere('t_kecamatanop', 'ilike', "%" . $request['filter']['alamatop'] . "%")
                    ->orWhere('t_kotaop', 'ilike', "%" . $request['filter']['alamatop'] . "%");
        }
        if ($request['filter']['jenishak'] != null) {
            $response = $response->where('t_jenishak', 'like', "%" . $request['filter']['jenishak'] . "%");
        }
        if ($request['filter']['tgltransaksi'] != null) {
            $date = explode(' - ', $request['filter']['tgltransaksi']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgltransaksi', [$startDate, $endDate]);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $status = Spt::where('t_nik_pembeli', $v['t_nik'])->orWhere('t_nop_sppt', $v['t_nop'])->get();
            $dataArr[] = [
                'tgltransaksi' => ($v['t_tgltransaksi']) ? date('d-m-Y', strtotime($v['t_tgltransaksi'])) : '-',
                'status' => (($status->count() > 0) ? '<i class="fas fa-check-circle text-green"></i><div class="text-green">Checked</div>' : '<i class="fas fa-minus-circle text-red"></i><div class="text-red">Not Found</div>'),
                'akta' => $v['t_noakta'].'<br>'.date('d-m-Y', strtotime($v['t_tglakta'])),
                'ppat' => $v['t_namappat'],
                'nop' => $v['t_nop'],
                'ntpd' => $v['t_ntpd'],
                'nib' => $v['t_nib'],
                'nik' => ($v['t_nik']) ? $v['t_nik'] : '-',
                'npwp' => ($v['t_npwp']) ? $v['t_npwp'] : '-',
                'namawp' => $v['t_namawp'],
                'alamatop' => 'Kel/Desa. '.$v['t_kelurahanop'].'<br>Kec. '.$v['t_kecamatanop'],
                'luastanah' => $v['t_luastanahop'],
                'jenishak' => $v['t_jenishak'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'review',
                        'actionUrl' => 'javascript:showReview(' . $v['t_idbpn'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request){
        $detail = Bpn::where('t_idbpn', '=', $request->query('id'))->get();
        $data = $detail[0];
        $html = '<table class="table table-striped table-hover table-sm" style="margin-top:10px;">
                <tr><td><b>Nama PPAT</b></td><td>: <b>'.$data['t_namappat'].'</b></td></tr>
                <tr><td>NOP</td><td>: '.substr($data['t_nop'],0,2).'.'.substr($data['t_nop'],2,2).'.'.substr($data['t_nop'],4,3).'.'.substr($data['t_nop'],7,3).'.'.substr($data['t_nop'],10,3).'.'.substr($data['t_nop'],13,4).'.'.substr($data['t_nop'],17,1).'</td></tr>
                <tr><td>NTPD</td><td>: '.$data['t_ntpd'].'</td></tr>
                <tr><td>NIK</td><td>: '.$data['t_nik'].'</td></tr>
                <tr><td><b>Nama WP</b></td><td>: <b>'.$data['t_namawp'].'</b></td></tr>
                <tr><td style="vertical-align:top;">Alamat OP</td><td style="vertical-align:top;">: Kel/Desa. '.$data['t_kelurahanop'].'<br>Kec. '.$data['t_kecamatanop'].'<br>Kab/Kota. '.$data['t_kotaop'].'</td></tr>
                <tr><td>Nomor/Tgl AKTA</td><td>: '.$data['t_noakta'].' - '.date('d-m-Y', strtotime($data['t_tglakta'])).'</td></tr>
                <tr><td>Luas Tanah (m<sup>2</sup>)</td><td>: '.number_format($data['t_luastanahop'], 2, ',','.').'</td></tr>
                <tr><td>Jenis Hak</td><td>: '.$data['t_jenishak'].'</td></tr>
            </table>';

        $response = [
            'data' => [
                'detail' => $detail,
                'html' => $html
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function export_xls(Request $request){
        $sheet = Excel::download(
            new BpnExport($request->query('t_tgltransaksi')),
            'cetakdatabpn.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
        return $sheet;
    }

    public function export_pdf(Request $request){
        $sheet = Excel::download(
            new BpnExport($request->query('t_tgltransaksi')),
            'cetakdatabpn.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }

    public function exportbpn_xls(Request $request){
        $sheet = Excel::download(
            new BpnApiExport($request->query('tgltransaksi'),
            $request->query('akta'),
            $request->query('ppat'),
            $request->query('nop'),
            $request->query('ntpd'),
            $request->query('nib'),
            $request->query('nik'),
            $request->query('npwp'),
            $request->query('namawp'),
            $request->query('alamatop'),
            $request->query('jenishak'),
            $request->query('created_at')),
            'cetakdatabpnatr.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
        return $sheet;
    }

    public function exportbpn_pdf(Request $request){
        $sheet = Excel::download(
            new BpnApiExport($request->query('tgltransaksi'),
            $request->query('akta'),
            $request->query('ppat'),
            $request->query('nop'),
            $request->query('ntpd'),
            $request->query('nib'),
            $request->query('nik'),
            $request->query('npwp'),
            $request->query('namawp'),
            $request->query('alamatop'),
            $request->query('jenishak'),
            $request->query('created_at')),
            'cetakdatabpnatr.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }

}