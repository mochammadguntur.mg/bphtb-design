<?php

namespace App\Http\Controllers\Pelaporan;

use App\Http\Controllers\Controller;
use App\Models\Pelaporan\DendaAjb;
use App\Models\Pelaporan\InputAjb;
use App\Models\Pelaporan\LaporBulananHead;
use App\Models\Setting\Pemda;
use Illuminate\Http\Request;
use Carbon\Carbon;
use DB;
use Illuminate\Support\Facades\Auth;
use MPDF;

class DendaAjbController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        return view('denda-ajb.index');
    }

    public function create()
    {
        return view('denda-ajb.create');
    }

    public function datagrid(Request $request)
    {
        $response = new DendaAjb();
        $response = $response->from('t_pen_denda_ajb_notaris as a')
            ->leftJoin('t_inputajb as b', 'b.t_idspt', '=', 'a.t_idspt')
            ->leftJoin('s_notaris as c', 'c.s_idnotaris', '=', 'a.t_idnotaris')
            ->leftJoin('s_status_bayar as d', 'd.s_id_status_bayar', '=', 'a.s_id_status_bayar')
            ->leftJoin('t_spt as f', 'f.t_idspt', '=', 'a.t_idspt')
            ->leftJoin('t_pembayaran_bphtb as g', 'g.t_idspt', '=', 'a.t_idspt');

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];

        foreach ($response as $v) {
            $dataArr[] = [
                't_nourut_ajbdenda' => $v['t_nourut_ajbdenda'],
                't_tglpenetapan' => Carbon::parse($v['t_tglpenetapan'])->format('d-m-Y'),
                's_namanotaris' => $v['s_namanotaris'],
                't_kohirspt' => $v['t_kohirspt'],
                't_tgl_ajb' => Carbon::parse($v['t_tgl_ajb'])->format('d-m-Y'),
                't_tglpembayaran_pokok' => Carbon::parse($v['t_tglpembayaran_pokok'])->format('d-m-Y'),
                't_nilai_ajbdenda' => number_format($v['t_nilai_ajbdenda'], 0, ',', '.'),
                's_nama_status_bayar' => $v['s_nama_status_bayar'],
                'actionList' => [
                    [
                        'actionName' => 'cetak-tagihan',
                        'actionUrl' => 'javascript:cetakTagihanDenda(' . $v['t_id_ajbdenda'] . ')',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['t_id_ajbdenda'] . ')',
                        'actionActive' => true
                    ]
                ],
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function datagrid_lapor_bulanan(Request $request)
    {
        $response = new InputAjb();
        $response = $response->from('t_inputajb as c')
            ->leftJoin('t_spt as d', 'd.t_idspt', '=', 'c.t_idspt')
            ->leftJoin('s_notaris as e', 'e.s_idnotaris', '=', 'd.t_idnotaris_spt')
            ->leftJoin('t_pembayaran_bphtb as f', 'f.t_idspt', '=', 'd.t_idspt')
            ->join('t_laporbulanan_detail as b', 'b.t_idajb', '=', 'c.t_idajb')
            ->select('t_kohirspt', 't_tgl_ajb', 't_no_ajb', 's_namanotaris', 't_tglpembayaran_pokok', 'c.t_idajb');
        $response->whereRaw("c.t_tgl_ajb < f.t_tglpembayaran_pokok");

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];

        foreach ($response as $v) {
            $dataArr[] = [
                't_kohirspt' => $v['t_kohirspt'],
                't_no_ajb' => $v['t_no_ajb'],
                't_nama_ppat' => $v['s_namanotaris'],
                't_tgl_ajb' => Carbon::parse($v['t_tgl_ajb'])->format('d-m-Y'),
                't_tglpembayaran_pokok' => Carbon::parse($v['t_tglpembayaran_pokok'])->format('d-m-Y'),
                'actionList' => [
                    [
                        'actionName' => 'pilih',
                        'actionUrl' => 'javascript:pilihData(' . $v['t_idajb'] . ')',
                        'actionActive' => true
                    ]
                ],
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function get_data_laporan(Request $request)
    {
        $data_ajb = InputAjb::join('t_spt as spt', 'spt.t_idspt', '=', 't_inputajb.t_idspt')
            ->join('s_notaris as c', 'c.s_idnotaris', '=', 'spt.t_idnotaris_spt')
            ->join('t_pembayaran_bphtb as d', 'd.t_idspt', '=', 'spt.t_idspt')
            ->where('t_idajb', $request->query('id'))
            ->first();

        $data = [
            't_idspt' => $data_ajb['t_idspt'],
            't_kohir' => $data_ajb['t_kohirspt'],
            't_tgl_ajb' => Carbon::parse($data_ajb['t_tgl_ajb'])->format('d-m-Y'),
            't_tglpembayaran_pokok' => Carbon::parse($data_ajb['t_tglpembayaran_pokok'])->format('d-m-Y'),
            't_idnotaris' => $data_ajb['t_idnotaris_spt'],
            's_namanotaris' => $data_ajb['s_namanotaris']
        ];

        return response()->json(
            $data,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request)
    {
        $detail = DendaAjb::from('t_pen_denda_ajb_notaris as a')
            ->leftJoin('s_notaris as c', 'c.s_idnotaris', '=', 'a.t_idnotaris')
            ->where('t_id_ajbdenda', '=', $request->query('id'))
            ->first();

        $data = [
            't_id_ajbdenda' => $detail['t_id_ajbdenda'],
            't_nourut_ajbdenda' => $detail['t_nourut_ajbdenda'],
            's_namanotaris' => $detail['s_namanotaris']
        ];

        return response()->json(
            $data,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function store(Request $request)
    {
        $attr = $request->except(['t_kohir', 's_namanotaris']);
        $denda = new DendaAjb();
        $no_urut = $denda->max('t_nourut_ajbdenda');

        $data = [
            't_idspt' => $attr['t_idspt'],
            't_idnotaris' => $attr['t_idnotaris'],
            't_tglpenetapan' => Carbon::parse($attr['t_tglpenetapan'])->format('Y-m-d h:m:s'),
            't_nourut_ajbdenda' => $no_urut + 1,
            't_nilai_ajbdenda' => 7500000,
            't_iduser_buat' => Auth::user()->id,
            's_id_status_bayar' => 2,
        ];

        $create = $denda->create($data);

        if ($create) {
            session()->flash('success', 'Penetapan denda berhasil di simpan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('denda-ajb');
    }

    public function destroy($id)
    {
        DendaAjb::where('t_id_ajbdenda', '=', $id)->delete();

        return response()->json([
            'success' => 'Data berhasil dihapus!',
            204,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        ]);
    }

    public function cetak_surat_tagihan(Request $request)
    {
        $detail = DendaAjb::from('t_pen_denda_ajb_notaris as a')
            ->leftJoin('s_notaris as c', 'c.s_idnotaris', '=', 'a.t_idnotaris')
            ->leftJoin('t_spt as b', 'b.t_idspt', '=', 'a.t_idspt')
            ->leftJoin('t_inputajb as d', 'd.t_idspt', '=', 'a.t_idspt')
            ->leftJoin('t_pembayaran_bphtb as e', 'e.t_idspt', '=', 'a.t_idspt')
            ->leftJoin('s_jenistransaksi as f', 'f.s_idjenistransaksi', '=', 'b.t_idjenistransaksi')
            ->where('t_id_ajbdenda', '=', $request->query('t_id_ajbdenda'))
            ->first();

        $config = [
            'format' => 'A4-P', // Landscape
        ];

        $pdf = MPDF::loadView('denda-ajb.exports.cetak-surat-tagihan', [
            'data' => $detail,
            'pemda' => Pemda::first()
        ], $config);

        return $pdf->stream();
    }
}
