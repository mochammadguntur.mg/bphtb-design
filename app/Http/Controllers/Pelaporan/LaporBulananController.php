<?php

namespace App\Http\Controllers\Pelaporan;

use App\Exports\Pelaporan\PelaporanAjbExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Pelaporan\LaporBulananRequest;
use App\Models\Pelaporan\InputAjb;
use App\Models\Pelaporan\LaporBulananHead;
use App\Models\Setting\JenisTransaksi;
use App\Models\Setting\Notaris;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Maatwebsite\Excel\Facades\Excel;
use Spatie\Backup\Helpers\Format;

class LaporBulananController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        return view('pelaporan-bulanan-ajb.index', [
            'jenis_peralihan' => JenisTransaksi::get(),
            'ppats' => Notaris::get()
        ]);
    }

    public function datagridAjb(Request $request)
    {
        $user = Auth::user();
        $response = new InputAjb();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_idajb');
        }
        
        if ($user->s_idnotaris != null) {
            $id_notaris = $user->s_idnotaris;
            $response = $response->with(['spt' => function ($q) use ($id_notaris) {
                $q->where('t_idnotaris_spt', $id_notaris);
            }]);
        }

        if (!empty($request['filter']['ppat'])) {
            $id_notaris = $request['filter']['ppat'];
            $response = $response->with(['spt' => function ($q) use ($id_notaris) {
                $q->where('t_idnotaris_spt', $id_notaris);
            }]);
        }

        if (!empty($request['filter']['bulan'])) {
            $bulan = $request['filter']['bulan'];
            $response = $response->whereRaw("EXTRACT(MONTH FROM t_tgl_ajb) = $bulan");
        }

        if (!empty($request['filter']['tahun'])) {
            $tahun = $request['filter']['tahun'];
            $response = $response->whereRaw("EXTRACT(YEAR FROM t_tgl_ajb) = $tahun");
        }

        if (!empty($request['filter']['no_daftar'])) {
            $no_daftar = $request['filter']['no_daftar'];
            $response = $response->with(['spt' => function ($q) use ($no_daftar) {
                $q->where('t_kohirspt', $no_daftar);
            }]);
        }

        if (!empty($request['filter']['tgl_daftar'])) {
            $tgl_daftar = Carbon::parse($request['filter']['tgl_ajb'])->format('Y-m-d');
            $response = $response->with(['spt' => function ($query) use ($tgl_daftar) {
                $query->where('t_tgldaftar_spt', $tgl_daftar);
            }]);
        }

        if (!empty($request['filter']['jenis_peralihan'])) {
            $t_idjenistransaksi = $request['filter']['jenis_peralihan'];
            $response = $response->with(['spt' => function ($query) use ($t_idjenistransaksi) {
                $query->where('t_idjenistransaksi', $t_idjenistransaksi);
            }]);
        }

        if (!empty($request['filter']['nama_wp'])) {
            $nama_wp = $request['filter']['nama_wp'];
            $response = $response->with(['spt' => function ($query) use ($nama_wp) {
                $query->where('t_nama_pembeli', 'ilike', '%' . $nama_wp . '%');
            }]);
        }

        if (!empty($request['filter']['no_ajb'])) {
            $response = $response->where('t_no_ajb', 'ilike', '%' . $request['filter']['no_ajb'] . '%');
        }

        if (!empty($request['filter']['tgl_ajb'])) {
            $tgl_ajb = Carbon::parse($request['filter']['tgl_ajb'])->format('Y-m-d');
            $response = $response->where('t_tgl_ajb', $tgl_ajb);
        }

        if (isset($request['checkbox'])) {
            $response = $response->whereDoesntHave('laporBulanans');
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];

        foreach ($response as $v) {
            $hasLapor = $v->laporBulanans()->where('t_idajb', $v['t_idajb'])->exists();

            if (isset($request['checkbox']) && $v->spt) {
                $dataArr[] = [
                    't_kohirspt' => $v->spt['t_kohirspt'],
                    't_tgldaftar_spt' => Carbon::parse($v->spt['t_tgldaftar_spt'])->format('d-m-Y'),
                    't_idjenistransaksi' => $v->spt['jenisTransaksi']['s_namajenistransaksi'],
                    't_nama_pembeli' => $v->spt['t_nama_pembeli'],
                    't_no_ajb' => $v['t_no_ajb'],
                    't_tgl_ajb' => Carbon::parse($v['t_tgl_ajb'])->format('d-m-Y'),
                    'checkbox' => '<input name="t_no_ajb[]" value="' . $v['t_idajb'] . '" lable-value="' . $v['t_no_ajb'] . '" type="checkbox" />',
                ];
            } else {
                if ($v->spt) {
                    $dataArr[] = [
                        't_ppat' => $v->spt->notaris['s_namanotaris'],
                        't_idjenistransaksi' => $v->spt->jenisTransaksi['s_namajenistransaksi'],
                        't_nama_pembeli' => $v->spt['t_nama_pembeli'],
                        't_no_ajb' => $v['t_no_ajb'],
                        't_tgl_ajb' => Carbon::parse($v['t_tgl_ajb'])->format('d-m-Y'),
                        't_status_lapor' => ($hasLapor) ? 'Sudah Lapor' : 'Belum Lapor',
                        'actionList' => [
                            [
                                'actionName' => 'edit',
                                'actionUrl' => 'pelaporan-ajb/' . $v['t_idajb'] . '/edit',
                                'actionActive' => true
                            ],
                            [
                                'actionName' => 'detail',
                                'actionUrl' => 'javascript:showDetailDialog(' . $v->spt['t_idspt'] . ')',
                                'actionActive' => true
                            ],
                            [
                                'actionName' => 'delete',
                                'actionUrl' => 'javascript:showDeleteDialog(' . $v['t_idajb'] . ')',
                                'actionActive' => true
                            ]
                        ],
                    ];
                }
            }
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function datagrid(Request $request)
    {
        $all_session = Auth::user();
        $response = new LaporBulananHead();
        $response = $response->from('t_laporbulanan_head as a')
                ->leftJoin('t_laporbulanan_detail as b', 'b.t_idlaporbulanan', '=', 'a.t_idlaporbulanan')
                ->leftJoin('t_inputajb as c', 'c.t_idajb', '=', 'b.t_idajb')
                ->leftJoin('t_spt as d', 'd.t_idspt', '=', 'c.t_idspt')
                ->leftJoin('s_jenistransaksi as e', 'e.s_idjenistransaksi', '=', 'd.t_idjenistransaksi' )
                ->leftJoin('s_notaris as f', 'f.s_idnotaris', '=', 'd.t_idnotaris_spt');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('a.t_idlaporbulanan');
        }

        if ($all_session->s_id_hakakses == 4) {
            $response = $response->where('t_idnotaris_spt', '=', $all_session->s_idnotaris);
        }

        if (!empty($request['filter']['tgl_lapor'])) {
            $tgl_lapor = Carbon::parse($request['filter']['tgl_lapor'])->format('Y-m-d');
            $response = $response->where('t_tgl_lapor', $tgl_lapor);
        }
        
        if (!empty($request['filter']['nama_wp'])) {
            $response = $response->where('t_nama_pembeli', 'ilike', '%' . $request['filter']['nama_wp'] . '%');
        }

        if (!empty($request['filter']['nop_sppt'])) {
            $response = $response->where('t_nop_sppt', 'ilike', '%' . $request['filter']['nop_sppt'] . '%');
        }

        if (!empty($request['filter']['ppat'])) {
            $response = $response->where('t_idnotaris_spt', $request['filter']['ppat']);
        }

        if (!empty($request['filter']['jenis_peralihan'])) {
            $response = $response->where('d.t_idjenistransaksi', $request['filter']['jenis_peralihan']);
        }

        if (!empty($request['filter']['lapor_untuk'])) {
            $exploded = explode("-", $request['filter']['lapor_untuk']);
            $bulan = str_replace("0", "", $exploded[0]);
            $tahun = $exploded[1];
            $response = $response->where('t_untuk_bulan', $bulan);
            $response = $response->where('t_untuk_tahun', $tahun);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            
            $dataArr[] = [
                't_no_lapor' => $v['t_no_lapor'],
                't_tgl_lapor' => Carbon::parse($v['t_tgl_lapor'])->format('d-m-Y'),
                't_ppat' => $v['s_namanotaris'],
                't_nama_wp' => $v['t_nama_pembeli'],
                't_nop' => $v['t_nop_sppt'],
                't_jenis_peralihan' => $v['s_namajenistransaksi'],
                't_untuk_bulan' => date("F", mktime(0, 0, 0, $v['t_untuk_bulan'], 10)) . ', ' . $v['t_untuk_tahun'],
                't_no_ajb' => $v['t_no_ajb'],
                't_tgl_ajb' => Carbon::parse($v['t_tgl_ajb'])->format('d-m-Y')
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create()
    {
        return view('pelaporan-bulanan-ajb.create', [
            'notaris' => Notaris::get(),
            'session' => Auth::user()
        ]);
    }

    public function store(LaporBulananRequest $laporBulananRequest)
    {
        $attr = $laporBulananRequest->all();
        $lapor = new LaporBulananHead;
        $no_lapor = $lapor->max('t_no_lapor');
        $lapor->t_no_lapor = ($no_lapor == null) ? 1 : $no_lapor + 1;
        $lapor->t_tgl_lapor = Carbon::parse($attr['t_tgl_lapor'])->format('Y-m-d');
        $lapor->t_untuk_bulan = $attr['t_untuk_bulan'];
        $lapor->t_untuk_tahun = $attr['t_untuk_tahun'];
        $lapor->t_keterangan = $attr['t_keterangan'];
        $lapor->t_iduser_input = Auth::user()->id;

        if ($lapor->save()) {
            $lapor->inputAjbs()->attach($attr['t_no_ajb']);
            session()->flash('success', 'Data berhasil dilaporkan');
        } else {
            session()->flash('error', 'Terjadi kesalaha! mohon periksa inputan anda');
        }

        return redirect()->route('lapor-bulanan-ajb');
    }

    public function export_xls(Request $request)
    {
        return Excel::download(
            new PelaporanAjbExport(
                $request->query('tgl_lapor'),
                $request->query('nama_wp'),
                $request->query('lapor_untuk'),
                $request->query('jenis_peralihan'),
            ),
            'setting-target.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request)
    {
        return Excel::download(
            new PelaporanAjbExport(
                $request->query('tgl_lapor'),
                $request->query('nama_wp'),
                $request->query('lapor_untuk'),
                $request->query('jenis_peralihan'),
            ),
            'laporan-bulanan.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
    }
}
