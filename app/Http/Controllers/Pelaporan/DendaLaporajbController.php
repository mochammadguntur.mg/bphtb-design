<?php

namespace App\Http\Controllers\Pelaporan;

use App\Http\Controllers\Controller;
use App\Models\Pelaporan\DendaLapor;
use App\Models\Pelaporan\LaporBulananHead;
use App\Models\Setting\Notaris;
use App\Models\Setting\Pemda;
use Illuminate\Http\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use DB;
use MPDF;

class DendaLaporajbController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {

        return view('denda-laporan-ajb.index');
    }

    public function datagrid(Request $request)
    {
        $response = new DendaLapor();
        $response = $response->from('t_pen_denda_lpor_notaris as a')
            ->leftJoin('t_laporbulanan_head as b', 'b.t_idlaporbulanan', '=', 'a.t_idlaporbulanan')
            ->leftJoin('s_notaris as c', 'c.s_idnotaris', '=', 'a.t_idnotaris')
            ->leftJoin('s_status_bayar as d', 'd.s_id_status_bayar', '=', 'a.s_id_status_bayar');

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];

        foreach ($response as $v) {
            $dataArr[] = [
                't_no_lapor' => $v['t_no_lapor'],
                't_tgl_lapor' => Carbon::parse($v['t_tgl_lapor'])->format('d-m-Y'),
                's_namanotaris' => $v['s_namanotaris'],
                't_nourut_pendenda' => $v['t_nourut_pendenda'],
                't_tglpenetapan' => Carbon::parse($v['t_tglpenetapan'])->format('d-m-Y'),
                't_nilai_pendenda' => number_format($v['t_nilai_pendenda']),
                's_nama_status_bayar' => $v['s_nama_status_bayar'],
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'denda-laporan-ajb/' . $v['t_id_pendenda'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'cetak-tagihan',
                        'actionUrl' => 'javascript:cetakTagihanDenda(' . $v['t_id_pendenda'] . ')',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['t_id_pendenda'] . ')',
                        'actionActive' => true
                    ]
                ],
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function datagrid_lapor_bulanan(Request $request)
    {
        $response = new LaporBulananHead();
        $response = $response->from('t_laporbulanan_head')
            ->leftJoin('t_laporbulanan_detail as b', 'b.t_idlaporbulanan', '=', 't_laporbulanan_head.t_idlaporbulanan')
            ->leftJoin('t_inputajb as c', 'c.t_idajb', '=', 'b.t_idajb')
            ->leftJoin('t_spt as d', 'd.t_idspt', '=', 'c.t_idspt')
            ->leftJoin('s_notaris as e', 'e.s_idnotaris', '=', 'd.t_idnotaris_spt')
            ->select(DB::raw('max(b.t_idajb) as idjab, t_laporbulanan_head.t_idlaporbulanan, t_no_lapor, t_tgl_lapor, t_untuk_bulan, t_untuk_tahun,  max(e.s_namanotaris) as s_namanotaris, max(d.t_idnotaris_spt) as id_notaris'))
            ->groupBy('t_laporbulanan_head.t_idlaporbulanan');
        $response->doesntHave('dendaLaporan');
        $response = $response->where('t_laporbulanan_head.t_tgl_lapor', '>', DB::raw('to_char( t_laporbulanan_head.t_tgl_lapor, \'YYYY-MM-10\' )::date'));

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);

        $dataArr = [];

        foreach ($response as $v) {
            // $denda = DendaLapor::where('t_idlaporbulanan', $v['t_idlaporbulanan'])->find();
            $dataArr[] = [
                't_no_lapor' => $v['t_no_lapor'],
                't_nama_ppat' => $v['s_namanotaris'],
                't_tgl_lapor' => Carbon::parse($v['t_tgl_lapor'])->format('d-m-Y'),
                't_untuk_bulan' => date("F", mktime(0, 0, 0, $v['t_untuk_bulan'], 10)) . ', ' . $v['t_untuk_tahun'],
                'actionList' => [
                    [
                        'actionName' => 'pilih',
                        'actionUrl' => 'javascript:pilihData(' . $v['t_idlaporbulanan'] . ',' . $v['id_notaris'] . ')',
                        'actionActive' => true
                    ]
                ],
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function get_data_laporan(Request $request)
    {
        $notaris = Notaris::find($request->query('id_ppat'));
        $lapor_bulanan = LaporBulananHead::find($request->query('id_laporan'));

        $data = [
            't_idlaporbulanan' => $lapor_bulanan['t_idlaporbulanan'],
            't_no_lapor' => $lapor_bulanan['t_no_lapor'],
            't_tgl_lapor' => Carbon::parse($lapor_bulanan['t_tgl_lapor'])->format('d-m-Y'),
            't_untuk_bulan' => date("F", mktime(0, 0, 0, $lapor_bulanan['t_untuk_bulan'], 10)) . ', ' . $lapor_bulanan['t_untuk_tahun'],
            's_idnotaris' => $notaris['s_idnotaris'],
            's_namanotaris' => $notaris['s_namanotaris']
        ];

        return response()->json(
            $data,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request)
    {
        $detail = DendaLapor::from('t_pen_denda_lpor_notaris as a')
            ->leftJoin('t_laporbulanan_head as b', 'b.t_idlaporbulanan', '=', 'a.t_idlaporbulanan')
            ->leftJoin('s_notaris as c', 'c.s_idnotaris', '=', 'a.t_idnotaris')
            ->where('t_id_pendenda', '=', $request->query('id'))
            ->first();

        $data = [
            't_id_pendenda' => $detail['t_id_pendenda'],
            't_nourut_pendenda' => $detail['t_nourut_pendenda'],
            't_idlaporbulanan' => $detail['t_idlaporbulanan'],
            's_namanotaris' => $detail['s_namanotaris']
        ];

        return response()->json(
            $data,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create()
    {
        return view('denda-laporan-ajb.create');
    }

    public function store(Request $request)
    {
        $attr = $request->all();
        $denda = new DendaLapor();
        $no_lapor = $denda->max('t_nourut_pendenda');
        $id_user_input = Auth::user()->id;

        $create = $denda->create([
            't_idlaporbulanan' => $attr['t_idlaporbulanan'],
            't_idnotaris' => $attr['t_idnotaris'],
            't_tglpenetapan' => Carbon::parse($attr['t_tglpenetapan'])->format('Y-m-d'),
            't_nourut_pendenda' => $no_lapor + 1,
            't_nilai_pendenda' => 250000,
            't_iduser_buat' => $id_user_input,
            's_id_status_bayar' => 2
        ]);

        if ($create) {
            session()->flash('success', 'Penetapan denda berhasil ditetapkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect()->route('denda-laporan-ajb');
    }

    public function edit(DendaLapor $denda_lapor)
    {
        $ppat = Notaris::where('s_idnotaris', $denda_lapor['t_idnotaris'])->first();
        $lap_ppat = LaporBulananHead::where('t_idlaporbulanan', $denda_lapor['t_idlaporbulanan'])->first();

        $data = [
            't_idlaporbulanan' => $denda_lapor['t_idlaporbulanan'],
            't_no_lapor' => $lap_ppat['t_no_lapor'],
            't_tgl_lapor' => Carbon::parse($lap_ppat['t_tgl_lapor'])->format('d-m-Y'),
            't_untuk_bulan' => date("F", mktime(0, 0, 0, $lap_ppat['t_untuk_bulan'], 10)) . ', ' . $lap_ppat['t_untuk_tahun'],
            't_idnotaris' => $ppat['s_idnotaris'],
            's_namanotaris' => $ppat['s_namanotaris'],
            't_tglpenetapan' => Carbon::parse($denda_lapor['t_tglpenetapan'])->format('d-m-Y'),
            't_id_pendenda' => $denda_lapor['t_id_pendenda']
        ];

        return view('denda-laporan-ajb.edit', [
            'data' => $data
        ]);
    }

    public function update(Request $request, DendaLapor $denda_lapor)
    {
        $update = $denda_lapor->update([
            't_tglpenetapan' => Carbon::parse($request->t_tglpenetapan)->format('Y-m-d'),
            't_iduser_buat' => Auth::user()->id
        ]);

        if ($update) {
            session()->flash('success', 'Penetapan denda berhasil diubah.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect()->route('denda-laporan-ajb');
    }

    public function destroy($id)
    {
        DendaLapor::where('t_id_pendenda', '=', $id)->delete();

        return response()->json([
            'success' => 'Data berhasil dihapus!',
            204,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        ]);
    }

    public function cetak_surat_tagihan(Request $request)
    {
        $detail = DendaLapor::from('t_pen_denda_lpor_notaris as a')
            ->leftJoin('t_laporbulanan_head as b', 'b.t_idlaporbulanan', '=', 'a.t_idlaporbulanan')
            ->leftJoin('s_notaris as c', 'c.s_idnotaris', '=', 'a.t_idnotaris')
            ->where('t_id_pendenda', '=', $request->query('t_id_pendenda'))
            ->first();

        $config = [
            'format' => 'A4-P', // Landscape
        ];

        $pdf = MPDF::loadView('denda-laporan-ajb.exports.cetak-surat-tagihan', [
            'data' => $detail,
            'pemda' => Pemda::first()
        ], $config);

        return $pdf->stream();
    }
}
