<?php

namespace App\Http\Controllers\Pelaporan;

use App\Exports\Pelaporan\InputAjbExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Pelaporan\PelaporanAjbRequst;
use App\Models\Pelaporan\InputAjb;
use App\Models\Setting\JenisTransaksi;
use App\Models\Setting\Notaris;
use App\Models\Spt\Spt;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class PelaporanAjbController extends Controller {

    public function __construct() {
        $this->middleware('auth');
    }

    public function index() {
        return view('pelaporan-ajb.index', [
            'jenis_hak' => JenisTransaksi::get(),
            'ppats' => Notaris::get(),
        ]);
    }

    public function datagridbelum(Request $request) {
        $response = (new InputAjb())->datagridBelumLapor(Auth::user()->s_idnotaris);
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_tgldaftar_spt');
        }

        if ($request['filter']['nodaftar'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['nodaftar'] . "%");
        }
        if ($request['filter']['tahun'] != null) {
            $response = $response->where('t_periodespt', 'like', "%" . $request['filter']['tahun'] . "%");
        }
        if ($request['filter']['notaris'] != null) {
            $response = $response->where('t_idnotaris_spt', '=', $request['filter']['notaris']);
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['namawp'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['namawp'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['status_persetujuan'] != null) {
            $response = ((($request['filter']['status_persetujuan'] == 1)) ? $response->where('t_idpersetujuan_bphtb', '=', $request['filter']['status_persetujuan']) : $response->whereNull('t_idpersetujuan_bphtb'));
        }
        if ($request['filter']['status_validasi_berkas'] != null) {
            if ($request['filter']['status_validasi_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['status_validasi_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['status_validasi_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }
        if ($request['filter']['status_validasi_kabid'] != null) {
            if ($request['filter']['status_validasi_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['status_validasi_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['status_validasi_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }
        if ($request['filter']['status_pembayaran'] != null) {
            if ($request['filter']['status_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['status_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];

        foreach ($response as $v) {
            if (!empty($v['s_id_status_berkas'])) {
                if ($v['s_id_status_berkas'] == 1) {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                } else {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button><small class="text-danger">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                }
            } else {
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-sudah btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['s_id_status_kabid'])) {
                if ($v['s_id_status_kabid'] == 1) {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SETUJU</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                } else {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK SETUJU</button><small class="text-danger">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                }
            } else {
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['t_tglpembayaran_pokok'])) {
                $status_bayar = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> LUNAS</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglpembayaran_pokok'])).'</small>';
            } else {
                $status_bayar = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';
            }

            if (!empty($v['t_idpersetujuan_bphtb'])) {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])).'</small>';
            } else {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>';
            }

            $perintah = '<a href="' . url('pelaporan-ajb') . '/' . $v['t_idspt'] . '/create" class="btn btn-warning btn-block btn-xs" title="PILIH"><i class="fas fa-fw fa-check-double"></i> <strong>LAPORKAN AKTA</strong></a>';
            $ketetapan = DB::connection('pgsql')
                    ->table('s_jenisketetapan')
                    ->select('s_namasingkatjenisketetapan')
                    ->where('s_jenisketetapan.s_idjenisketetapan', $v['jenis_ketetapan'])
                    ->first();
            $nilai_bphtb_fix = ($v['t_jmlh_totalskpdkb'] != null) ? $v['t_jmlh_totalskpdkb'] + $v['t_nilai_bphtb_fix'] : $v['t_nilai_bphtb_fix'];

            $dataArr[] = [
                't_idspt' => $perintah,
                'nodaftar' => $v['t_kohirspt'],
                'tglskpdkb' => ($v['t_tgldaftar_spt']) ? Carbon::parse($v['t_tgldaftar_spt'])->format('d-m-Y H:i:s') : '-',
                'tahun' => $v['t_periodespt'],
                'notaris' => $v['s_namanotaris'],
                'jenistransaksi' => $v['s_namajenistransaksi'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop_sppt' => $v['t_nop_sppt'],
                'nilai_bphtb_fix' => number_format($nilai_bphtb_fix, 0, ',', '.'),
                'status_persetujuan_bphtb' => $status_persetujuan,
                'status_validasi_berkas' => $status_validasi_berkas,
                'status_validasi_kabid' => $status_validasi_kabid,
                'status_pembayaran' => $status_bayar,
                'jenisketetapan' => $ketetapan->s_namasingkatjenisketetapan,
                'kodebayar' => $v['t_kodebayar_bphtb'],
                'ntpd' => $v['t_kodebayar_bphtb'],
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function datagrid(Request $request) {
        $response = (new InputAjb())->datagridSudahLapor();

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_tgldaftar_spt');
        }

        if ($request['filter']['nodaftar'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['nodaftar'] . "%");
        }
        if ($request['filter']['tahun'] != null) {
            $response = $response->where('t_periodespt', 'like', "%" . $request['filter']['tahun'] . "%");
        }
        if ($request['filter']['notaris'] != null) {
            $response = $response->where('t_idnotaris_spt', '=', $request['filter']['notaris']);
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['namawp'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['namawp'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }
        if ($request['filter']['status_persetujuan'] != null) {
            $response = ((($request['filter']['status_persetujuan'] == 1)) ? $response->where('t_idpersetujuan_bphtb', '=', $request['filter']['status_persetujuan']) : $response->whereNull('t_idpersetujuan_bphtb'));
        }
        if ($request['filter']['status_validasi_berkas'] != null) {
            if ($request['filter']['status_validasi_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['status_validasi_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['status_validasi_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }
        if ($request['filter']['status_validasi_kabid'] != null) {
            if ($request['filter']['status_validasi_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['status_validasi_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['status_validasi_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }
        if ($request['filter']['status_pembayaran'] != null) {
            if ($request['filter']['status_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['status_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {

            if (!empty($v['s_id_status_berkas'])) {
                if ($v['s_id_status_berkas'] == 1) {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> LENGKAP</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                } else {
                    $status_validasi_berkas = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK LENGKAP</button><small class="text-danger">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasi'])).'</small>';
                }
            } else {
                $status_validasi_berkas = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
            }

            if (!empty($v['s_id_status_kabid'])) {
                if ($v['s_id_status_kabid'] == 1) {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-square-o"></i> SETUJU</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                    $cetak_sspd = '<a href="/' . $v['t_idspt'] . '/cetaksspdbphtb" target="_blank" class="btn btn-primary btn-block btn-xs" title="CETAK SSPD"><i class="fa fa-fw fa-print"></i> CETAK SSPD</a>';
                } else {
                    $status_validasi_kabid = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-minus-square-o"></i> TIDAK SETUJU</button><small class="text-danger">'.date('d-m-Y H:i:s', strtotime($v['t_tglvalidasikabid'])).'</small>';
                    $cetak_sspd = '';
                }
            } else {
                $status_validasi_kabid = '<button type="button" class="btn btn-block btn-warning btn-xs" style="width: 100px;"><i class="fa fa-fw fa-eye"></i> BELUM</button>';
                $cetak_sspd = '';
            }

            if (!empty($v['t_tglpembayaran_pokok'])) {
                $edit_validasi = '';
                $status_bayar = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> LUNAS</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tglpembayaran_pokok'])).'</small>';
            } else {
                $edit_validasi = '<a href="' . url('validasi-kabid') . '/' . $v['t_idspt'] . '/formvalidasikabid" class="btn btn-warning btn-block btn-xs" title="EDIT PENDAFTARAN"><i class="fa fa-fw fa-edit"></i> EDIT</a>';
                $status_bayar = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-money"></i> BELUM</button>';
            }

            if (!empty($v['t_idpersetujuan_bphtb'])) {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-success btn-xs" style="width: 100px;"><i class="fa fa-fw fa-check-circle"></i> SUDAH</button><small class="text-green">'.date('d-m-Y H:i:s', strtotime($v['t_tgldaftar_spt'])).'</small>';
            } else {
                $status_persetujuan = '<button type="button" class="btn btn-block btn-danger btn-xs" style="width: 100px;"><i class="fa fa-fw fa-question"></i> BELUM</button>';
            }
            $lihat_data = '<a href="/lihatdata/' . $v['t_uuidspt'] . '" class="btn btn-block bg-gradient-info btn-sm" title="Lihat Data"><i class="fa fa-fw fa-eye"></i> Lihat Data</a>';

            $perintah = '<div class="dropdown">
                    <button onclick="myFunction(' . $v['t_idspt'] . ')" class="dropbtn btn-primary dropdown-toggle btn btn-xs">PERINTAH <span class="caret"></span>&nbsp;&nbsp;&nbsp;</button>
                    <div id="myDropdown' . $v['t_idspt'] . '" class="dropdown-content dropdown-menu" style="left:80px; border-color: blue;">
                            ' . $lihat_data . '
                            ' . $edit_validasi . '
                            ' . $cetak_sspd . '
                    </div>
                </div>';

            $ketetapan = DB::connection('pgsql')
                    ->table('s_jenisketetapan')
                    ->select('s_namasingkatjenisketetapan')
                    ->where('s_jenisketetapan.s_idjenisketetapan', $v['jenis_ketetapan'])
                    ->first();
            $nilai_bphtb_fix = ($v['t_jmlh_totalskpdkb'] != null) ? $v['t_jmlh_totalskpdkb'] + $v['t_nilai_bphtb_fix'] : $v['t_nilai_bphtb_fix'];

            $dataArr[] = [
                't_idspt' => $perintah,
                'nodaftar' => $v['t_kohirspt'],
                'tglskpdkb' => ($v['t_tgldaftar_spt']) ? Carbon::parse($v['t_tgldaftar_spt'])->format('d-m-Y H:i:s') : '-',
                'tahun' => $v['t_periodespt'],
                'notaris' => $v['s_namanotaris'],
                'jenistransaksi' => $v['s_namajenistransaksi'],
                'nama_pembeli' => $v['t_nama_pembeli'],
                'nop_sppt' => $v['t_nop_sppt'],
                // 'nilai_bphtb_fix' => number_format($v['t_nilai_bphtb_fix'], 0, ',', '.'),
                'nilai_bphtb_fix' => number_format($nilai_bphtb_fix, 0, ',', '.'),
                'status_persetujuan_bphtb' => $status_persetujuan,
                'status_validasi_berkas' => $status_validasi_berkas,
                'status_validasi_kabid' => $status_validasi_kabid,
                'status_pembayaran' => $status_bayar,
                // 'jenisketetapan' => $v['s_namasingkatjenisketetapan'],
                'jenisketetapan' => $ketetapan->s_namasingkatjenisketetapan,
                't_kodebayar_bphtb' => !empty($v['t_kodebayar_bphtb']) ? $v['t_kodebayar_bphtb'] : '',
                'ntpd' => !empty($v['t_ntpd']) ? $v['t_ntpd'] : ''
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function getDataSpt(Request $request) {
        $spt = Spt::find($request->query('id'));
        return response()->json(
                        $spt,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = InputAjb::with('spt')->where('t_idajb', '=', $request->query('id'))->first();

        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create(Spt $spt) {
        $data_fotoobjek = (new Spt())->cek_foto_objek($spt['t_idspt']);
        return view('pelaporan-ajb.create', [
            'jenis_hak' => JenisTransaksi::get(),
            'spt' => $spt,
            'dataspt' => $spt,
            'data_fotoobjek' => $data_fotoobjek,
        ]);
    }

    public function store(PelaporanAjbRequst $pelaporanAjbRequst, Spt $spt) {
        $attr = $pelaporanAjbRequst->except('_token');
        $attr['t_tgl_ajb'] = Carbon::parse($attr['t_tgl_ajb'])->format('Y-m-d');
        $attr['t_iduser_input'] = Auth::user()->id;
        $create = InputAjb::create($attr);
        if ($create) {
            session()->flash('success', 'Data Persyaratan berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect()->route('pelaporan-ajb');
    }

    public function edit(InputAjb $pelaporan_ajb) {
        $spt = Spt::where('t_idspt', $pelaporan_ajb->t_idspt)->first();

        return view('pelaporan-ajb.edit', [
            'ajb' => $pelaporan_ajb,
            'spt' => $spt,
        ]);
    }

    public function update(PelaporanAjbRequst $request, InputAjb $pelaporan_ajb) {
        $attr = $request->all();
        $attr['t_tgl_ajb'] = Carbon::parse($attr['t_tgl_ajb'])->format('Y-m-d');
        $attr['t_iduser_input'] = Auth::user()->id;

        try {
            $update = $pelaporan_ajb->update($attr);
        } catch (\Illuminate\Database\QueryException $ex) {
            return response()->json(array('message' => $ex->getMessage()));
        }

        if ($update) {
            session()->flash('success', 'Data Persyaratan berhasil diupdate!');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukan!');
        }

        return redirect()->route('pelaporan-ajb');
    }

    public function destroy($id) {
        InputAjb::where('t_idajb', '=', $id)->delete();

        return response()->json([
                    'success' => 'Data berhasil dihapus!',
                    204,
                    ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                    JSON_UNESCAPED_UNICODE
        ]);
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new InputAjbExport(
                                $request->query('no_daftar'),
                                $request->query('tgl_daftar'),
                                $request->query('jenis_peralihan'),
                                $request->query('nama_wp'),
                                $request->query('no_ajb'),
                                $request->query('tgl_ajb')
                        ),
                        'setting-target.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        return Excel::download(
                        new InputAjbExport(
                                $request->query('no_daftar'),
                                $request->query('tgl_daftar'),
                                $request->query('jenis_peralihan'),
                                $request->query('nama_wp'),
                                $request->query('no_ajb'),
                                $request->query('tgl_ajb')
                        ),
                        'setting-target.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
    }

}
