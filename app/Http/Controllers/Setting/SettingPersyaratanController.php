<?php

namespace App\Http\Controllers\Setting;

use App\Exports\Setting\SettingPersyaratanExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\PersyaratanRequest;
use App\Models\Dropdown\WajibUpload;
use App\Models\Setting\JenisTransaksi;
use Illuminate\Http\Request;
use App\Models\Setting\Persyaratan;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;

class SettingPersyaratanController extends Controller {

    private $menus = [
        array('s_idmenu' => 1, 's_nama_menu' => 'Pemdaftaran'),
        array('s_idmenu' => 2, 's_nama_menu' => 'Pemeriksaan')
    ];

    public function index() {
        return view('setting-persyaratan.index', [
            'persyaratan' => Persyaratan::orderBy('created_at', 'desc')->get(),
            'jenis_hak' => JenisTransaksi::get(),
        ]);
    }

    public function datagrid(Request $request) {
        $response = new Persyaratan();

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_idjenistransaksi');
        }

        if (!empty($request['filter']['jenis_peralihan'])) {
            $response = $response->where('s_idjenistransaksi', $request['filter']['jenis_peralihan']);
        }

        if (!empty($request['filter']['nama_persyaratan'])) {
            $response = $response->where('s_namapersyaratan', 'ilike', "%" . $request['filter']['nama_persyaratan'] . "%");
        }

        if (!empty($request['filter']['created_at'])) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));

            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        
        if (!empty($request['filter']['update_at'])) {
            $date = explode(' - ', $request['filter']['update_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));

            $response = $response->whereBetween('updated_at', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];

        foreach ($response as $v) {
            $dataArr[] = [
                'jenis_peralihan' => $v->jenisTransaksi['s_namajenistransaksi'],
                'nama_persyaratan' => $v['s_namapersyaratan'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'update_at' => date('d-m-Y', strtotime($v['updated_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-persyaratan/' . $v['s_idpersyaratan'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_idpersyaratan'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = Persyaratan::with('jenisTransaksi')->find($request->query('id'));

        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-persyaratan.create', [
            'persyaratan' => new Persyaratan(),
            'jenis_hak' => JenisTransaksi::get(),
            'wajib_upload' => WajibUpload::get(),
            'menus' => $this->menus,
        ]);
    }

    public function store(PersyaratanRequest $persyaratanRequest) {
        $attr = $persyaratanRequest->all();
        $attr['created_at'] = date('Y-m-d H:i:s');
        $create = Persyaratan::create($attr);
        if ($create) {
            session()->flash('success', 'Data Persyaratan berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-persyaratan');
    }

    public function edit(Persyaratan $setting_persyaratan) {
        return view('setting-persyaratan.edit', [
            'persyaratan' => $setting_persyaratan,
            'jenis_hak' => JenisTransaksi::get(),
            'wajib_upload' => WajibUpload::get(),
            'menus' => $this->menus,
        ]);
    }

    public function update(PersyaratanRequest $persyaratanRequest, Persyaratan $setting_persyaratan) {
        $attr = $persyaratanRequest->all();
        $attr['update_at'] = date('Y-m-d H:i:s');
        $update = $setting_persyaratan->update($attr);

        if ($update) {
            session()->flash('success', 'Data Persyaratan berhasil diupdate!');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukan!');
        }

        return redirect('setting-persyaratan');
    }

    public function destroy($setting_persyaratan) {
        Persyaratan::where('s_idpersyaratan', '=', $setting_persyaratan)->delete();

        return response()->json([
                    'success' => 'Data berhasil dihapus!',
                    204,
                    ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                    JSON_UNESCAPED_UNICODE
        ]);
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingPersyaratanExport(
                                $request->query('jenis_peralihan'),
                                $request->query('nama_persyaratan'),
                                $request->query('created_at')
                        ),
                        'setting-persyaratan.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        return Excel::download(
                        new SettingPersyaratanExport(
                                $request->query('jenis_peralihan'),
                                $request->query('nama_persyaratan'),
                                $request->query('created_at')
                        ),
                        'setting-persyaratan.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
    }

    public function cariMaxUrut(Request $request) {
        $detail = Persyaratan::select(
                        DB::raw('max(s_persyaratan.order) as max_urut')
                )->where('s_idjenistransaksi', $request->query('id'))->first();
        $detail['max_urut'] = $detail['max_urut'] + 1;
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

}
