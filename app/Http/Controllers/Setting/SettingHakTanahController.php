<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\HakTanahRequest;
use App\Models\Setting\HakTanah;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingHakTanahExport;

use function Ramsey\Uuid\v1;

class SettingHakTanahController extends Controller
{
    //
    public function index(){
        return view('setting-hak-tanah.index', [
            'haktanah' => HakTanah::OrderBy('s_idhaktanah', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request){
        $response = new HakTanah();

        if(!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('s_idhaktanah');
        }

        if($request['filter']['s_kodehaktanah'] != null) {
            $response = $response->where('s_kodehaktanah', 'like' , "%" . $request['filter']['s_kodehaktanah'] . "%");
        }
        if($request['filter']['s_namahaktanah'] != null) {
            $response = $response->where('s_namahaktanah', 'ilike' , "%" . $request['filter']['s_namahaktanah'] . "%" );
        }
        if ($request['filter']['created_at'] != null){
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'],['*'],'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach($response as $v){
            $dataArr[] = [
                'kodehaktanah'      => $v['s_kodehaktanah'],
                'namahaktanah'      => $v['s_namahaktanah'],
                'created_at'        => date('d-m-Y', strtotime($v['created_at'])),
                'actionList'        => [
                    [
                        'actionName'    => 'edit',
                        'actionUrl'     => 'setting-hak-tanah/' . $v['s_idhaktanah'] . '/edit',
                        'actionActive'  => true
                    ],
                    [
                        'actionName'    => 'delete',
                        'actionUrl'     => 'javascript:showDeleteDialog(' . $v['s_idhaktanah']. ')',
                        'actionActive'  => true
                    ]
                ]
            ];
        }

        $response = [
            'data'  => [
                'content'           => $dataArr,
                'number'            => $response->currentPage() - 1,
                'size'              => $response->perPage(),
                'first'             => $response->onFirstPage(),
                'last'              => $response->lastPage() == $response->currentPage() ? true : false ,
                'totalPages'        => $response->lastPage(),
                'numberOfElements'  => $response->count(),
                'totalElements'     => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request){
        $detail = HakTanah::where('s_idhaktanah', '=' , $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create(){
        return view('setting-hak-tanah.create',[
            'haktanah'  => new HakTanah()
        ]);
    }

    public function store(HakTanahRequest $hakTanahRequest){
        $attr = $hakTanahRequest->all();

        $create = HakTanah::create($attr);

        if($create){
            session()->flash('success', 'Data Jenis Hak Tanah baru berhasil ditambahkan.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-hak-tanah');
    }

    public function edit(HakTanah $hakTanah){
        return view('setting-hak-tanah.edit', [
            'haktanah' => $hakTanah
        ]);
    }

    public function update(HakTanahRequest $hakTanahRequest , HakTanah $hakTanah){
        $attr = $hakTanahRequest->all();
        $update = $hakTanah->update($attr);

        if($update){
            session()->flash('success', 'Data Jenis Hak Tanah berhasil diperbarui!.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-hak-tanah');
    }

    public function destroy(Request $request){
        HakTanah::where('s_idhaktanah', '=' , $request->query('id'))->delete();
    }

    public function export_xls(Request $request){
        return Excel::download(
            new SettingHakTanahExport($request->query('s_kodehaktanah'), $request->query('s_namahaktanah'), $request->query('created_at')),
            'jenis_hak_tanah.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request){
        $sheet = Excel::download(
            new SettingHakTanahExport($request->query('s_kodehaktanah'), $request->query('s_namahaktanah'), $request->query('created_at')),
            'jenis_hak_tanah.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }
}
