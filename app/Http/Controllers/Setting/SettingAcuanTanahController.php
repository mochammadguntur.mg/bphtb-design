<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Exports\Setting\SettingAcuanTanahExport;
use App\Http\Requests\Setting\AcuanTanahRequest;
use App\Models\Setting\AcuanTanah;
use App\Models\Setting\JenisTanah;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SettingAcuanTanahController extends Controller
{
    //
    public function index(){
        return view('setting-acuan-tanah.index', 
        [
            'acuantanah'    => AcuanTanah::orderBy('created_at', 'desc')->get(),
            'jenis_tanah'   => JenisTanah::get(),
        ]);
    }

    public function datagrid(Request $request)
    {
        $response = new AcuanTanah();
        if (!empty($response['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('s_idacuan_jenis');
        }

        if (!empty($request['filter']['kd_propinsi'])){
            $response = $response->where('s_kd_propinsi', 'like', "%" . $request['filter']['kd_propinsi'] . "%");
        }

        if (!empty($request['filter']['kd_dati2'])){
            $response = $response->where('s_kd_dati2', 'like', "%" . $request['filter']['kd_dati2'] . "%");
        }

        if (!empty($request['filter']['kd_kecamatan'])){
            $response = $response->where('s_kd_kecamatan', 'like', "%" . $request['filter']['kd_kecamatan'] . "%");
        }

        if (!empty($request['filter']['kd_kelurahan'])){
            $response = $response->where('s_kd_kelurahan', 'like', "%" . $request['filter']['kd_kelurahan'] . "%");
        }

        if (!empty($request['filter']['kd_blok'])){
            $response = $response->where('s_kd_blok', 'like', "%" . $request['filter']['kd_blok'] . "%");
        }

        if (!empty($request['filter']['permeter_tanah'])){
            $response = $response->where('s_permetertanah', 'like', "%" . $request['filter']['permeter_tanah'] . "%");
        }

        if (!empty($request['filter']['jenis_tanah'])){
            $response = $response->where('id_jenistanah', $request['filter']['jenis_tanah']);
        }

        if (!empty($request['filter']['created_at'])) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));

            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
      
        foreach ($response as $v) {
            // dd($v->jenisTanah); die;
            $dataArr[] = [
                'kd_propinsi'       => $v['s_kd_propinsi'],
                'kd_dati2'          => $v['s_kd_dati2'],
                'kd_kecamatan'      => $v['s_kd_kecamatan'],
                'kd_kelurahan'      => $v['s_kd_kelurahan'],
                'kd_blok'           => $v['s_kd_blok'],
                'permeter_tanah'    => number_format($v['s_permetertanah'], 0, '', '.'),
                'id_jenis_tanah'    => $v->jenisTanah['nama_jenistanah'],
                'created_at'        => date('d-m-Y', strtotime($v['created_at'])),
                'actionList'        => 
                [
                    [
                        'actionName' => 'edit',
                        'actionUrl'     =>'setting-acuan-tanah/'. $v['s_idacuan_jenis'] . '/edit',
                        'actionActive'  => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog('. $v['s_idacuan_jenis'] .')',
                        'actionActive' => true
                    ]
                ] 
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request)
    {
        $detail = AcuanTanah::with('jenisTanah')->find($request->query('id'));

        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create()
    {
        return view('setting-acuan-tanah.create', [
            'acuantanah'    => new AcuanTanah(),
            'jenistanah'    => JenisTanah::get(),
        ]);
    }

    public function store(AcuanTanahRequest $acuanTanahRequest)
    {
        $attr = $acuanTanahRequest->all();
        $attr['s_permetertanah'] = str_replace(".","",$attr['s_permetertanah']);
        $attr['s_idjenistanah'] = $attr['id_jenistanah']; // klo satu atau dua bisa map gini juga
        
        $create = AcuanTanah::create($attr);

        if($create){
            session()->flash('success', 'Data Acuan Tanah berhasil ditambahkan');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan');
        }

        return redirect('setting-acuan-tanah');
    }

    public function edit(AcuanTanah $acuantanah)
    {
        // dd($acuantanah);
        return view('setting-acuan-tanah.edit', [
            'acuantanah'    => $acuantanah,
            'jenistanah'    => JenisTanah::get(),
        ]);
    }

    public function update(AcuanTanahRequest $acuanTanahRequest, AcuanTanah $acuantanah)
    {
        $attr = $acuanTanahRequest->all();
        $attr['s_permetertanah'] = str_replace(".","",$attr['s_permetertanah']);
        $update = $acuantanah->update($attr);

        if($update){
            session()->flash('success', 'Data Acuan Tanah berhasil diupdate!');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan!');
        }

        return redirect('setting-acuan-tanah');
    }

    public function destroy($acuanTanah)
    {
        AcuanTanah::where('s_idacuan_jenis', '=', $acuanTanah)->delete();

        return response()->json([
            'success' => 'Data berhasil dihapus!',
            204,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        ]);
    }

    public function export_xls(Request $request)
    {
        return Excel::download(
            new SettingAcuanTanahExport(
			    $request->query('kd_propinsi'),
				$request->query('kd_dati2'),
				$request->query('kd_kecamatan'),
				$request->query('kd_kelurahan'),
				$request->query('kd_blok'),
                $request->query('permeter_tanah'), 
                $request->query('jenis_tanah'), 
                $request->query('created_at')
            ),
            'setting-acuan-tanah.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request)
    {
        return Excel::download(
            new SettingAcuanTanahExport(
			    $request->query('kd_propinsi'),
				$request->query('kd_dati2'),
				$request->query('kd_kecamatan'),
				$request->query('kd_kelurahan'),
				$request->query('kd_blok'),
                $request->query('permeter_tanah'), 
                $request->query('jenis_tanah'), 
                $request->query('created_at')
            ),
            'setting-acuan-tanah.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
    }
}
