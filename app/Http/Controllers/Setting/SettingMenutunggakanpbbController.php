<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\MenutunggakanpbbRequest;
use App\Models\Setting\Menutunggakanpbb;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingPejabatExport;

class SettingMenutunggakanpbbController extends Controller {

    public function index() {
        return view('setting-menutunggakanpbb.index', [
            'pejabats' => Menutunggakanpbb::orderBy('updated_at', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request) {
        $response = new Menutunggakanpbb();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_id_setmenutgkpbb');
        }

        /*
        if ($request['filter']['s_namapejabat'] != null) {
            $response = $response->where('s_namapejabat', 'ilike', "%" . $request['filter']['s_namapejabat'] . "%");
        }
        if ($request['filter']['s_nippejabat'] != null) {
            $response = $response->where('s_nippejabat', 'ilike', "%" . $request['filter']['s_nippejabat'] . "%");
        }
        if ($request['filter']['s_jabatanpejabat'] != null) {
            $response = $response->where('s_jabatanpejabat', 'ilike', "%" . $request['filter']['s_jabatanpejabat'] . "%");
        }
        if ($request['filter']['s_golonganpejabat'] != null) {
            $response = $response->where('s_golonganpejabat', 'ilike', "%" . $request['filter']['s_golonganpejabat'] . "%");
        } */

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            
            $dataArr[] = [
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-menutunggakanpbb/' . $v['s_id_setmenutgkpbb'] . '/edit',
                        'actionActive' => true
                    ]
                    /* [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_setmenutgkpbb'] . ')',
                        'actionActive' => true
                    ] */
                ],
                //'s_id_statustgkpbb' => $v['s_id_statustgkpbb'],
                's_ketmenutgkpbb' => $v['s_ketmenutgkpbb'],
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = Menutunggakanpbb('s_idpejabat', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-menutunggakanpbb.create', ['pejabat' => new Pejabat()]);
    }

    public function store(MenutunggakanpbbRequest $MenutunggakanpbbRequest) {
        $attr = $MenutunggakanpbbRequest->all();
        
        $create = Menutunggakanpbb::create($attr);
        if ($create) {
            session()->flash('success', 'Data baru berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-menutunggakanpbb');
    }

    public function edit(Menutunggakanpbb $menutunggakanpbb) {
        return view('setting-menutunggakanpbb.edit', ['menutunggakanpbb' => $menutunggakanpbb]);
    }

    public function update(MenutunggakanpbbRequest $MenutunggakanpbbRequest, Menutunggakanpbb $menutunggakanpbb) {
        
            $attr = $MenutunggakanpbbRequest->all();
        
            //var_dump($attr); exit();
            
            if($attr['s_id_statustgkpbb'] == 1){
                $attr['s_ketmenutgkpbb'] = 'AKTIF';
            }else{
                $attr['s_ketmenutgkpbb'] = 'TIDAK AKTIF';
            }
        
            //var_dump($attr); exit();
        $update = $menutunggakanpbb->update($attr);

        if ($update) {
            session()->flash('success', 'Data Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-menutunggakanpbb');
    }

    public function destroy(Request $request) {
        Menutunggakanpbb::where('s_idpejabat', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat')
                        ),
                        'setting_pejabat.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat')
                        ),
                        'setting_pejabat.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
    }

}
