<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\MenutunggakantahunpbbRequest;
use App\Models\Setting\Menutunggakantahunpbb;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingPejabatExport;

class SettingMenutunggakantahunpbbController extends Controller {

    public function index() {
        return view('setting-menutunggakantahunpbb.index', [
            'pejabats' => Menutunggakantahunpbb::orderBy('updated_at', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request) {
        $response = new Menutunggakantahunpbb();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_id_thntgkpbb');
        }

        /*
        if ($request['filter']['s_namapejabat'] != null) {
            $response = $response->where('s_namapejabat', 'ilike', "%" . $request['filter']['s_namapejabat'] . "%");
        }
        if ($request['filter']['s_nippejabat'] != null) {
            $response = $response->where('s_nippejabat', 'ilike', "%" . $request['filter']['s_nippejabat'] . "%");
        }
        if ($request['filter']['s_jabatanpejabat'] != null) {
            $response = $response->where('s_jabatanpejabat', 'ilike', "%" . $request['filter']['s_jabatanpejabat'] . "%");
        }
        if ($request['filter']['s_golonganpejabat'] != null) {
            $response = $response->where('s_golonganpejabat', 'ilike', "%" . $request['filter']['s_golonganpejabat'] . "%");
        } */

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            
            if(!empty($v['s_rentangthntgkpbb'])){
                $tahun_mulai = date('Y') - $v['s_rentangthntgkpbb'];
                $rentang_tahun = $v['s_rentangthntgkpbb'].' <b style="color: blue;"><i> ( Akan di Mulai dari tahun '.$tahun_mulai.' sampai tahun sekarang '.date('Y').' ) </i></b>';
            }else{
                $rentang_tahun = $v['s_rentangthntgkpbb'];
            }
            
            if(!empty($v['s_thntgkpbb'])){
                $tahun_mulai = $v['s_thntgkpbb'];
                $tahun_start = $v['s_thntgkpbb'].' <b style="color: blue;"><i> ( Akan di Mulai dari tahun '.$tahun_mulai.' sampai tahun sekarang '.date('Y').' ) </i></b>';
            }else{
                $tahun_start = $v['s_thntgkpbb'];
            }
            
            $dataArr[] = [
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-menutunggakantahunpbb/' . $v['s_id_thntgkpbb'] . '/edit',
                        'actionActive' => true
                    ]
                    /* [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_thntgkpbb'] . ')',
                        'actionActive' => true
                    ] */
                ],
                //'s_id_statustgkpbb' => $v['s_id_statustgkpbb'],
                's_thntgkpbb' => $tahun_start,
                's_rentangthntgkpbb' => $rentang_tahun,
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = Menutunggakantahunpbb('s_idpejabat', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-menutunggakantahunpbb.create', ['pejabat' => new Pejabat()]);
    }

    public function store(MenutunggakantahunpbbRequest $MenutunggakantahunpbbRequest) {
        $attr = $MenutunggakantahunpbbRequest->all();
        
        $create = Menutunggakantahunpbb::create($attr);
        if ($create) {
            session()->flash('success', 'Data baru berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-menutunggakantahunpbb');
    }

    public function edit(Menutunggakantahunpbb $menutunggakanpbb) {
        return view('setting-menutunggakantahunpbb.edit', ['menutunggakanpbb' => $menutunggakanpbb]);
    }

    public function update(MenutunggakantahunpbbRequest $MenutunggakantahunpbbRequest, Menutunggakantahunpbb $menutunggakanpbb) {
        
            $attr = $MenutunggakantahunpbbRequest->all();
        
            //var_dump($attr); exit();
            
            
        
            //var_dump($attr); exit();
        $update = $menutunggakanpbb->update($attr);

        if ($update) {
            session()->flash('success', 'Data Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-menutunggakantahunpbb');
    }

    public function destroy(Request $request) {
        Menutunggakantahunpbb::where('s_idpejabat', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat')
                        ),
                        'setting_pejabat.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat')
                        ),
                        'setting_pejabat.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
    }

}
