<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\PejabatRequest;
use App\Models\Setting\Pejabat;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingPejabatExport;

class SettingPejabatController extends Controller {

    public function index() {
        return view('setting-pejabat.index', [
            'pejabats' => Pejabat::orderBy('updated_at', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request) {
        $response = new Pejabat();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_idpejabat');
        }

        if ($request['filter']['s_namapejabat'] != null) {
            $response = $response->where('s_namapejabat', 'ilike', "%" . $request['filter']['s_namapejabat'] . "%");
        }
        if ($request['filter']['s_nippejabat'] != null) {
            $response = $response->where('s_nippejabat', 'ilike', "%" . $request['filter']['s_nippejabat'] . "%");
        }
        if ($request['filter']['s_jabatanpejabat'] != null) {
            $response = $response->where('s_jabatanpejabat', 'ilike', "%" . $request['filter']['s_jabatanpejabat'] . "%");
        }
        if ($request['filter']['s_golonganpejabat'] != null) {
            $response = $response->where('s_golonganpejabat', 'ilike', "%" . $request['filter']['s_golonganpejabat'] . "%");
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $lampiran_ttd = !empty($v['s_filettd']) ? "<img src='" . $v['s_filettd'] . "' style='width:100px'></img>" : '';
            $dataArr[] = [
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-pejabat/' . $v['s_idpejabat'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_idpejabat'] . ')',
                        'actionActive' => true
                    ]
                ],
                'nippejabat' => $v['s_nippejabat'],
                'namapejabat' => $v['s_namapejabat'],
                'jabatan' => $v['s_jabatanpejabat'],
                'golongan' => $v['s_golonganpejabat'],
                'filettd' => $lampiran_ttd,
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = Pejabat::where('s_idpejabat', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-pejabat.create', ['pejabat' => new Pejabat()]);
    }

    public function store(PejabatRequest $PejabatRequest) {
        $attr = $PejabatRequest->all();
        if ($PejabatRequest->hasFile('s_filettd')) {
            $files = $PejabatRequest->file('s_filettd');
            $destinationPath = 'upload/';
            $files->move($destinationPath, $files->getClientOriginalName());
            $attr['s_filettd'] = $destinationPath . $files->getClientOriginalName();
        }
        $create = Pejabat::create($attr);
        if ($create) {
            session()->flash('success', 'Data Pejabat baru berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-pejabat');
    }

    public function edit(Pejabat $pejabat) {
        return view('setting-pejabat.edit', ['pejabat' => $pejabat]);
    }

    public function update(PejabatRequest $pejabatRequest, Pejabat $pejabat) {
        if ($pejabatRequest->get('s_filettd') == null) {
            $attr = $pejabatRequest->except('s_filettd');
        } else {
            $attr = $pejabatRequest->all();
        }

        if ($pejabatRequest->hasFile('s_filettd')) {
            $files = $pejabatRequest->file('s_filettd');
            $destinationPath = 'upload/';
            $files->move($destinationPath, $files->getClientOriginalName());
            $attr['s_filettd'] = $destinationPath . $files->getClientOriginalName();
        }

        $update = $pejabat->update($attr);

        if ($update) {
            session()->flash('success', 'Data Pejabat Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-pejabat');
    }

    public function destroy(Request $request) {
        Pejabat::where('s_idpejabat', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat'),
                        ),
                        'setting_pejabat.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat'),
                        ),
                        'setting_pejabat.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
    }

}
