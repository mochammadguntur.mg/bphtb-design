<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Models\Setting\Tempo;
use App\Models\Dropdown\Status;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingTempoExport;
use App\Http\Requests\Setting\TempoRequest;

class SettingTempoController extends Controller
{
    public function index(){
        return view('setting-tempo.index', [
            'tempo' => Tempo::OrderBy('s_idtempo', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request){
        $response = new Tempo();

        if(!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('s_idtempo');
        }

        if($request['filter']['haritempo'] != null) {
            $response = $response->where('s_haritempo', 'ilike' , "%" . $request['filter']['haritempo'] . "%");
        }

        if ($request['filter']['tanggalawal'] != null){
            $date = explode(' - ', $request['filter']['tanggalawal']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response = $response->whereBetween('s_tglberlaku_awal', [$startDate, $endDate]);
        }

        if ($request['filter']['tanggalakhir'] != null){
            $date = explode(' - ', $request['filter']['tanggalakhir']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response = $response->whereBetween('s_tglberlaku_akhir', [$startDate, $endDate]);
        }
        
        if($request['filter']['status'] != null) {
            $response = $response->where('s_id_status', 'like' , "%" . $request['filter']['status'] . "%" );
        }

        if ($request['filter']['created_at'] != null){
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'],['*'],'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach($response as $v){
            $status = [1=>'Aktif',2=>'Tidak Aktif'];
            $dataArr[] = [
                'haritempo'      => $v['s_haritempo'],
                'tanggalawal'        => date('d-m-Y', strtotime($v['s_tglberlaku_awal'])),
                'tanggalakhir'        => date('d-m-Y', strtotime($v['s_tglberlaku_akhir'])),
                'status'                => $status[$v['s_id_status']],
                'created_at'        => date('d-m-Y', strtotime($v['created_at'])),
                'actionList'        => [
                    [
                        'actionName'    => 'edit',
                        'actionUrl'     => 'setting-tempo/' . $v['s_idtempo'] . '/edit',
                        'actionActive'  => true
                    ],
                    [
                        'actionName'    => 'delete',
                        'actionUrl'     => 'javascript:showDeleteDialog(' . $v['s_idtempo']. ')',
                        'actionActive'  => true
                    ]
                ]
            ];
        }

        $response = [
            'data'  => [
                'content'           => $dataArr,
                'number'            => $response->currentPage() - 1,
                'size'              => $response->perPage(),
                'first'             => $response->onFirstPage(),
                'last'              => $response->lastPage() == $response->currentPage() ? true : false ,
                'totalPages'        => $response->lastPage(),
                'numberOfElements'  => $response->count(),
                'totalElements'     => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request){
        $detail = Tempo::where('s_idtempo', '=' , $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create(){
        return view('setting-tempo.create', [
            'tempo' => new Tempo(),
            'status' => Status::get(),
        ]);
    }

    public function store(TempoRequest $TempoRequest){
        $attr = $TempoRequest->all();

        $create = Tempo::create($attr);

        if($create){
            session()->flash('success', 'Data Tempo baru berhasil ditamabahkan.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-tempo');

    }

    public function edit(Tempo $tempo){
        $tempo['s_tglberlaku_awal'] =  \Carbon\Carbon::parse($tempo['s_tglberlaku_awal'])->format('Y-m-d');
        $tempo['s_tglberlaku_akhir'] =  \Carbon\Carbon::parse($tempo['s_tglberlaku_akhir'])->format('Y-m-d');
        
        return view('setting-tempo.edit', [
            'tempo' => $tempo,
            'status' => Status::get(),
        ]);
    }

    public function update(TempoRequest $tempoRequest, Tempo $tempo){
        $attr = $tempoRequest->all();
        $update = $tempo->update($attr);
        if($update){
            session()->flash('success', 'Data Tempo Behasil diupdate.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-tempo');
    }


    public function destroy(Request $request){
        Tempo::where('s_idtempo', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request){
        return Excel::download(
            new SettingTempoExport($request->query('s_idtempo'),
            $request->query('s_haritempo'),
            $request->query('s_tglberlaku_awal'),
            $request->query('s_tglberlaku_akhir'),
            $request->query('s_status'),
            $request->query('created_at')),
            'setting-tempo.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request){
        $sheet = Excel::download(
            new SettingTempoExport($request->query('s_idtempo'),
            $request->query('s_haritempo'),
            $request->query('s_tglberlaku_awal'),
            $request->query('s_tglberlaku_akhir'),
            $request->query('s_status'),
            $request->query('created_at')),
            'setting-tempo.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }
}
