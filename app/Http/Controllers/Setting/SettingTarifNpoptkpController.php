<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\TarifNpoptkpRequest;
use App\Models\Dropdown\Status;
use App\Models\Setting\TarifNpoptkp;
use App\Models\Setting\JenisTransaksi;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingTarifNpoptkpExport;

class SettingTarifNpoptkpController extends Controller
{
    public function index(){
        return view('setting-tarif-npoptkp.index', [
            'tarifnpoptkp' => TarifNpoptkp::orderBy('updated_at', 'desc')->get(),
            'jenis_transaksi' => JenisTransaksi::get(),
        ]);
    }

    public function datagrid(Request $request){
        $response = new TarifNpoptkp();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('s_idtarifnpoptkp');
        }

        if ($request['filter']['jenistransaksinpoptkp'] != null) {
            $response = $response->where('s_idjenistransaksinpoptkp', '=', $request['filter']['jenistransaksinpoptkp'] );
        }
        if ($request['filter']['tarifnpoptkp'] != null) {
            $response = $response->where('s_tarifnpoptkp', 'like', "%" . $request['filter']['tarifnpoptkp'] . "%");
        }
        if ($request['filter']['tarifnpoptkptambahan'] != null) {
            $response = $response->where('s_tarifnpoptkptambahan', 'like', "%" . $request['filter']['tarifnpoptkptambahan'] . "%");
        }
        if ($request['filter']['dasarhukum'] != null) {
            $response = $response->where('s_dasarhukumnpoptkp', 'ilike', "%" . $request['filter']['dasarhukum'] . "%");
        }
        if ($request['filter']['status'] != null) {
            $response = $response->where('s_statusnpoptkp', 'like', "%" . $request['filter']['status'] . "%");
        }
        if ($request['filter']['tanggal_awal'] != null) {
            $date = explode(' - ', $request['filter']['tanggal_awal']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('s_tglberlaku_awal', [$startDate, $endDate]);
        }
        if ($request['filter']['tanggal_akhir'] != null) {
            $date = explode(' - ', $request['filter']['tanggal_akhir']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('s_tglberlaku_akhir', [$startDate, $endDate]);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $status = [1=>'Aktif',2=>'Tidak Aktif'];
            $dataArr[] = [
                'idjenistransaksinpoptkp' => $v->jenisTransaksi['s_namajenistransaksi'],
                'tarifnpoptkp' => number_format($v['s_tarifnpoptkp'], 0,',','.'),
                'tarifnpoptkptambahan' => number_format($v['s_tarifnpoptkptambahan'], 0,',','.'),
                'dasarhukumnpoptkp' => $v['s_dasarhukumnpoptkp'],
                'tglberlakuawal' => ($v['s_tglberlaku_awal']) ? date('d-m-Y', strtotime($v['s_tglberlaku_awal'])) : '-',
                'tglberlakuakhir' => ($v['s_tglberlaku_akhir']) ? date('d-m-Y', strtotime($v['s_tglberlaku_akhir'])) : '-',
                'statusnpoptkp' => $status[$v['s_statusnpoptkp']],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-tarif-npoptkp/' . $v['s_idtarifnpoptkp'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_idtarifnpoptkp'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request){
        $detail = TarifNpoptkp::where('s_idtarifnpoptkp', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create(){
        return view('setting-tarif-npoptkp.create', [
            'tarifnpoptkp' => new TarifNpoptkp(),
            'jenis_transaksi' => JenisTransaksi::get(),
            'status' => Status::get(),
        ]);
    }

    public function store(TarifNpoptkpRequest $TarifNpoptkpRequest){
        $attr = $TarifNpoptkpRequest->all();

        $create = TarifNpoptkp::create($attr);

        if($create){
            session()->flash('success', 'Data Tarif NPOPTKP baru berhasil ditamabahkan.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-tarif-npoptkp');

    }


    public function edit(TarifNpoptkp $tarifnpoptkp){
        return view('setting-tarif-npoptkp.edit', [
            'tarifnpoptkp' => $tarifnpoptkp,
            'jenis_transaksi' => JenisTransaksi::get(),
            'status' => Status::get(),
        ]);
    }

    public function update(TarifNpoptkpRequest $tarifNpoptkpRequest, TarifNpoptkp $tarifnpoptkp){
        $attr = $tarifNpoptkpRequest->all();

        $update = $tarifnpoptkp->update($attr);
        //dd($update);

        if($update){
            session()->flash('success', 'Data Tarif NPOPTKP Behasil diupdate.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-tarif-npoptkp');
    }

    public function destroy(Request $request){
        TarifNpoptkp::where('s_idtarifnpoptkp', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request){
        return Excel::download(
            new SettingTarifNpoptkpExport($request->query('s_idjenistransaksinpoptkp'),
            $request->query('s_tarifnpoptkp'),
            $request->query('s_tarifnpoptkptambahan'),
            $request->query('s_dasarhukumnpoptkp'),
            $request->query('s_tglberlaku_awal'),
            $request->query('s_tglberlaku_akhir'),
            $request->query('s_statusnpoptkp'),
            $request->query('created_at')),
            'setting_tarif_npoptkp.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request){
        $sheet = Excel::download(
            new SettingTarifNpoptkpExport($request->query('s_idjenistransaksinpoptkp'),
            $request->query('s_tarifnpoptkp'),
            $request->query('s_tarifnpoptkptambahan'),
            $request->query('s_dasarhukumnpoptkp'),
            $request->query('s_tglberlaku_awal'),
            $request->query('s_tglberlaku_akhir'),
            $request->query('s_statusnpoptkp'),
            $request->query('created_at')),
            'setting_tarif_npoptkp.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }

}
