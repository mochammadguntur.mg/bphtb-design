<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\JenisFasilitasRequest;
use App\Models\Setting\JenisFasilitas;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingJenisFasilitasExport;

class SettingJenisFasilitasController extends Controller
{
    //
    public function index(){
        return view('setting-jenis-fasilitas.index'
        , [
            'jenisfasilitas' => JenisFasilitas::OrderBy('s_idjenisfasilitas', 'desc')->get(),
        ]
    );
    }

    public function datagrid(Request $request){
        $response = new JenisFasilitas();
        //dd($request);
        if(!empty($request['sorting'])) {
            $response = $response->OrderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->OrderBy('s_idjenisfasilitas');
        }

        if($request['filter']['kodejenisfasilitas'] != null){
            $response = $response->where('s_kodejenisfasilitas', 'like', "%" . $request['filter']['kodejenisfasilitas'] . "%");
        }

        if($request['filter']['namajenisfasilitas'] != null){
            $response = $response->where('s_namajenisfasilitas', 'ilike', "%" . $request['filter']['namajenisfasilitas'] . "%");
        }

        if ($request['filter']['created_at'] != null ){
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v){
            $dataArr[] = [
                'kodejenisfasilitas'        => $v['s_kodejenisfasilitas'],
                'namajenisfasilitas'        => $v['s_namajenisfasilitas'],
                'created_at'                => date('d-m-Y', strtotime($v['created_at'])),
                'actionList'                => [
                    [
                        'actionName'        => 'edit',
                        'actionUrl'         => 'setting-jenis-fasilitas/' . $v['s_idjenisfasilitas']. '/edit',
                        'actionActive'      => true
                    ],
                    [
                        'actionName'        => 'delete',
                        'actionUrl'         => 'javascript:showDeleteDialog(' . $v['s_idjenisfasilitas']. ')',
                        'actionActive'      => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request){
        $detail = JenisFasilitas::where('s_idjenisfasilitas', '=' , $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create(){
        return view('setting-jenis-fasilitas.create', [
            'jenisfasilitas'    => new JenisFasilitas()
        ]);
    }

    public function store(JenisFasilitasRequest $JenisFasilitasRequest){
        $attr = $JenisFasilitasRequest->all();

        $create = JenisFasilitas::create($attr);

        if($create){
            session()->flash('success', 'Data Jenis Fasilitas baru berhasil ditamabahkan.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-jenis-fasilitas');

    }

    public function edit(JenisFasilitas $jenisFasilitas){
        return view('setting-jenis-fasilitas.edit', [
            'jenisfasilitas'    =>$jenisFasilitas
        ]);
    }

    public function update(JenisFasilitasRequest $jenisFasilitasRequest, JenisFasilitas $jenisFasilitas){
        $attr   = $jenisFasilitasRequest->all();
        $update = $jenisFasilitas->update($attr);
        if($update){
            session()->flash('success', 'Data Jenis Fasilitas berhasil di update. ');
        }else{
            session()->flash('error', 'Mohon di cek Kembai data yang anda masukkan.');
        }

        return redirect('setting-jenis-fasilitas');
    }

    public function destroy(Request $request){
        JenisFasilitas::where('s_idjenisfasilitas', '=' , $request->query('id'))->delete();
    }

    public function export_xls(Request $request){
        return Excel::download(
            new SettingJenisFasilitasExport($request->query('s_kodejenisfasilitas'), $request->query('s_namajenisfasilitas'), $request->query('created_at')),
            'setting-jenis-fasilitas.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request){
        $sheet = Excel::download(
            new SettingJenisFasilitasExport($request->query('s_kodejenisfasilitas'), $request->query('s_namajenisfasilitas'), $request->query('created_at')),
            'setting-jenis-fasilitas.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }
}
