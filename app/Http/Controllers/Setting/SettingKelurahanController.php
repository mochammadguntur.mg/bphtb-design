<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Models\Setting\Kecamatan;
use App\Models\Setting\Kelurahan;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingKelurahanExport;
use App\Http\Requests\Setting\KelurahanRequest;


class SettingKelurahanController extends Controller
{
    public function index()
    {
        return view('setting-kelurahan.index', [
            'kelurahans' => Kelurahan::orderBy('updated_at', 'desc')->get(),
            'kecamatan' => Kecamatan::get(),
        ]);
    }

    public function sync()
    {
        $kelurahan_oracle1 = DB::connection('oracle')->table("REF_KELURAHAN")->get();
        $kelurahan_postgre = DB::connection('pgsql')->table("s_kelurahan")->get();

        $unsetdarioracle = array();
        $offset = 0;
        $kodesama = false;
        
        $cek_data_propinsi = DB::connection('oracle')->table("REF_PROPINSI")->first();
        $cek_data_dati2 = DB::connection('oracle')->table("REF_DATI2")->first();
        
        $kelurahan_oracle = collect($kelurahan_oracle1);
        
        $kelurahan_oracle->push((object) ['kd_propinsi'=>$cek_data_propinsi->kd_propinsi, 'kd_dati2' => $cek_data_dati2->kd_dati2, 'kd_kecamatan' => '000', 'kd_kelurahan' => '000', 'nm_kelurahan' => 'LUAR DAERAH']);
        
        $kelurahan_oracle->all();
        
        //print_r($kelurahan_oracle); exit();

        foreach ($kelurahan_oracle as $o_kelurahan) {
            foreach ($kelurahan_postgre as $p_kelurahan) {
                if ($o_kelurahan->kd_kelurahan == $p_kelurahan->s_kd_kelurahan) {
                    $unsetdarioracle[$offset]['kodekecamatan'] = $o_kelurahan->kd_kelurahan;
                    $unsetdarioracle[$offset]['namakecamatan'] = $o_kelurahan->nm_kelurahan;
                    $kodesama = true;
                    continue;
                }
            }
            ++$offset;
        }

        foreach ($kelurahan_oracle as $key => $value) {
            foreach ($unsetdarioracle as $unset) {
                if ($value->kd_kelurahan == $unset['kodekecamatan']) {
                    unset($kelurahan_oracle[$key]);
                }
            }
        }

        //  insert data dari oracle ke postgre
        $kelurahan_oracle = $kelurahan_oracle->toArray();
        $kelurahan_postgre = $kelurahan_postgre->toArray();
        $kecamatan_postgre = Kecamatan::get();
        $kec_to_array = [];
        foreach ($kecamatan_postgre as $kec) {
            // $kec_to_array[$kec['s_kd_kecamatan']] = $kec['s_idkecamatan'];
            $kec_to_array[number_format($kec['s_kd_kecamatan'])] =  $kec['s_idkecamatan']; // simpan di array data kec dari bphtb
        }
        $final_data = array();
        $offset = 0;

        // dd($kec_to_array);
        foreach ($kelurahan_oracle as $value) {
            $final_data[$offset]['s_kd_propinsi'] = $value->kd_propinsi;
            $final_data[$offset]['s_kd_dati2'] = $value->kd_dati2;
            $final_data[$offset]['s_kd_kelurahan'] = $value->kd_kelurahan;
            $final_data[$offset]['s_namakelurahan'] = $value->nm_kelurahan;
            $final_data[$offset]['s_kd_kecamatan'] = $value->kd_kecamatan;
            // $final_data[$offset]['s_idkecamatan'] = $kec_to_array[$value->kd_kecamatan];
            $final_data[$offset]['s_idkecamatan'] = $kec_to_array[number_format($value->kd_kecamatan)];
            $final_data[$offset]['created_at'] = \Carbon\Carbon::now();
            $final_data[$offset]['updated_at'] = \Carbon\Carbon::now();
            ++$offset;
        }
        // dd($final_data);
        if (count($final_data) < 1) {
            session()->flash('success', 'Data sudah terupdate');
        } else {
            $insert = DB::connection('pgsql')->table('s_kelurahan')->insert($final_data);
            session()->flash('success', count($kelurahan_oracle) . ' Data Berhasil Sync ');
        }
        return redirect('setting-kelurahan');
    }

    public function datagrid(Request $request)
    {
        $response = new Kelurahan();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_kd_kecamatan');
        }
        if (!empty($request['filter']['kecamatan'])) {
            $response = $response->where('s_idkecamatan', $request['filter']['kecamatan']);
        }

        if ($request['filter']['kd_kelurahan'] != null) {
            $response = $response->where('s_kd_kelurahan', 'ilike', "%" . $request['filter']['kd_kelurahan'] . "%");
        }
        if ($request['filter']['namakelurahan'] != null) {
            $response = $response->where('s_namakelurahan', 'ilike', "%" . $request['filter']['namakelurahan'] . "%");
        }
        if ($request['filter']['latitude'] != null) {
            $response = $response->where('s_latitude', 'like', "%" . $request['filter']['latitude'] . "%");
        }
        if ($request['filter']['longitude'] != null) {
            $response = $response->where('s_longitude', 'like', "%" . $request['filter']['longitude'] . "%");
        }
        if ($request['filter']['updated_at'] != null) {
            $date = explode(' - ', $request['filter']['updated_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('updated_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        // dd($response);
        foreach ($response as $v) {

            $dataArr[] = [
                'kecamatan' => $v['s_kd_kecamatan'],
                'kd_kelurahan' => $v['s_kd_kelurahan'],
                'namakelurahan' => $v['s_namakelurahan'],
                'latitude' => (!empty($v['s_latitude'])) ? $v['s_latitude'] : '',
                'longitude' => (!empty($v['s_longitude'])) ? $v['s_longitude'] : '',
                'updated_at' => date('d-m-Y', strtotime($v['updated_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-kelurahan/' . $v['s_idkelurahan'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_idkelurahan'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request)
    {
        // $detail = Kelurahan::where('s_idkelurahan', '=', $request->query('id'))->get();
        $detail = Kelurahan::find($request->query('id'));
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create()
    {

        $kecamatan = Kecamatan::all();
        //dd($kecamatan);
        return view('setting-kelurahan.create', [
            'kelurahan' => new Kelurahan(), 'kecamatan' => $kecamatan, 'action' => 'create'
        ]);
    }

    public function store(Request $request, KelurahanRequest $kelurahanRequest)
    {

        $attr = $kelurahanRequest->all();
        $this->validate($request, [
            's_kd_kelurahan' => 'unique:s_kelurahan'
        ]);

        $kec = DB::table('s_kecamatan')->where('s_idkecamatan', $attr['s_idkecamatan'])->first();

        $attr['s_kd_propinsi'] = $kec->s_kd_propinsi;
        $attr['s_kd_dati2'] = $kec->s_kd_dati2;
        $attr['s_kd_kecamatan'] = $kec->s_kd_kecamatan;

        // save to s_kelurahan  
        DB::table('s_kelurahan')->insert([
            's_idkecamatan' => $attr['s_idkecamatan'],
            's_namakelurahan' => $attr['s_namakelurahan'],
            's_kd_propinsi' => $attr['s_kd_propinsi'],
            's_kd_dati2' => $attr['s_kd_dati2'],
            's_kd_kecamatan' => $attr['s_kd_kecamatan'],
            's_kd_kelurahan' => $attr['s_kd_kelurahan'],
            's_latitude' => $attr['s_latitude'],
            's_longitude' => $attr['s_longitude'],
            'created_at' => \Carbon\Carbon::now(),
            'updated_at' => \Carbon\Carbon::now()
        ]);


        return redirect('setting-kelurahan');
    }

    public function edit(Kelurahan $kelurahan)
    {
        $kecamatan_selected = Kecamatan::where('s_idkecamatan', $kelurahan['s_idkecamatan'])->get();
        $kecamatan = Kecamatan::all();

        return view('setting-kelurahan.edit', [
            'kelurahan' => $kelurahan, 'kecamatan_selected' => $kecamatan_selected,
            'kecamatan' => $kecamatan, 'action' => 'edit'
        ]);
    }

    public function update(KelurahanRequest $kelurahanRequest, Kelurahan $kelurahan)
    {

        $attr = $kelurahanRequest->all();

        $kec = DB::table('s_kecamatan')->where('s_idkecamatan', $attr['s_idkecamatan'])->first();

        $attr['s_kd_propinsi'] = $kec->s_kd_propinsi;
        $attr['s_kd_dati2'] = $kec->s_kd_dati2;
        $attr['s_kd_kecamatan'] = $kec->s_kd_kecamatan;

        $update = $kelurahan->update($attr);

        if ($update) {
            session()->flash('success', 'Data Kelurahan Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-kelurahan');
    }

    public function destroy(Request $request)
    {
        Kelurahan::where('s_idkelurahan', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request)
    {
        return Excel::download(
            new SettingKelurahanExport(
                // $request->query('kecamatan'),
                $request->query('s_kd_kelurahan'),
                $request->query('s_namakelurahan'),
                $request->query('updated_at')
            ),
            'setting_kelurahan.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request)
    {
        $pdf = Excel::download(
            new SettingKelurahanExport(
                $request->query('s_kd_kelurahan'),
                $request->query('s_namakelurahan'),
                $request->query('updated_at')
            ),
            'setting_kelurahan.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $pdf;
    }
}
