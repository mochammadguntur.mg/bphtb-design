<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\MenuntpdRequest;
use App\Models\Setting\Menuntpd;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingPejabatExport;

class SettingMenuntpdController extends Controller {

    public function index() {
        return view('setting-menuntpd.index', [
            'pejabats' => Menuntpd::orderBy('updated_at', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request) {
        $response = new Menuntpd();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_id_setmenuntpd');
        }

        /*
        if ($request['filter']['s_namapejabat'] != null) {
            $response = $response->where('s_namapejabat', 'ilike', "%" . $request['filter']['s_namapejabat'] . "%");
        }
        if ($request['filter']['s_nippejabat'] != null) {
            $response = $response->where('s_nippejabat', 'ilike', "%" . $request['filter']['s_nippejabat'] . "%");
        }
        if ($request['filter']['s_jabatanpejabat'] != null) {
            $response = $response->where('s_jabatanpejabat', 'ilike', "%" . $request['filter']['s_jabatanpejabat'] . "%");
        }
        if ($request['filter']['s_golonganpejabat'] != null) {
            $response = $response->where('s_golonganpejabat', 'ilike', "%" . $request['filter']['s_golonganpejabat'] . "%");
        } */

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            
            $dataArr[] = [
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-menuntpd/' . $v['s_id_setmenuntpd'] . '/edit',
                        'actionActive' => true
                    ]
                    /* [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_setmenuntpd'] . ')',
                        'actionActive' => true
                    ] */
                ],
                //'s_id_statusmenuntpd' => $v['s_id_statusmenuntpd'],
                's_ketmenuntpd' => $v['s_ketmenuntpd'],
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = Menuntpd('s_idpejabat', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-menuntpd.create', ['pejabat' => new Pejabat()]);
    }

    public function store(MenuntpdRequest $MenuntpdRequest) {
        $attr = $MenuntpdRequest->all();
        
        $create = Menuntpd::create($attr);
        if ($create) {
            session()->flash('success', 'Data baru berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-menuntpd');
    }

    public function edit(Menuntpd $menuntpd) {
        return view('setting-menuntpd.edit', ['menuntpd' => $menuntpd]);
    }

    public function update(MenuntpdRequest $MenuntpdRequest, Menuntpd $menuntpd) {
        
            $attr = $MenuntpdRequest->all();
        
            //var_dump($attr); exit();
            
            if($attr['s_id_statusmenuntpd'] == 1){
                $attr['s_ketmenuntpd'] = 'AKTIF';
            }else{
                $attr['s_ketmenuntpd'] = 'TIDAK AKTIF';
            }
        
            //var_dump($attr); exit();
        $update = $menuntpd->update($attr);

        if ($update) {
            session()->flash('success', 'Data Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-menuntpd');
    }

    public function destroy(Request $request) {
        Menuntpd::where('s_idpejabat', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat')
                        ),
                        'setting_pejabat.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat')
                        ),
                        'setting_pejabat.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
    }

}
