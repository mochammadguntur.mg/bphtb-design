<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\MenugmapRequest;
use App\Models\Setting\Menugmap;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingPejabatExport;

class SettingMenugmapController extends Controller {

    public function index() {
        return view('setting-menugmap.index', [
            'pejabats' => Menugmap::orderBy('updated_at', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request) {
        $response = new Menugmap();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_id_setmenugmap');
        }

        /*
        if ($request['filter']['s_namapejabat'] != null) {
            $response = $response->where('s_namapejabat', 'ilike', "%" . $request['filter']['s_namapejabat'] . "%");
        }
        if ($request['filter']['s_nippejabat'] != null) {
            $response = $response->where('s_nippejabat', 'ilike', "%" . $request['filter']['s_nippejabat'] . "%");
        }
        if ($request['filter']['s_jabatanpejabat'] != null) {
            $response = $response->where('s_jabatanpejabat', 'ilike', "%" . $request['filter']['s_jabatanpejabat'] . "%");
        }
        if ($request['filter']['s_golonganpejabat'] != null) {
            $response = $response->where('s_golonganpejabat', 'ilike', "%" . $request['filter']['s_golonganpejabat'] . "%");
        } */

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            
            $dataArr[] = [
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-menugmap/' . $v['s_id_setmenugmap'] . '/edit',
                        'actionActive' => true
                    ]
                    /* [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_setmenugmap'] . ')',
                        'actionActive' => true
                    ] */
                ],
                //'s_id_statusmenugmap' => $v['s_id_statusmenugmap'],
                's_ketmenugmap' => $v['s_ketmenugmap'],
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = Menugmap('s_idpejabat', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-menugmap.create', ['pejabat' => new Pejabat()]);
    }

    public function store(MenugmapRequest $MenugmapRequest) {
        $attr = $MenugmapRequest->all();
        
        $create = Menugmap::create($attr);
        if ($create) {
            session()->flash('success', 'Data baru berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-menugmap');
    }

    public function edit(Menugmap $menugmap) {
        return view('setting-menugmap.edit', ['menugmap' => $menugmap]);
    }

    public function update(MenugmapRequest $MenugmapRequest, Menugmap $menugmap) {
        
            $attr = $MenugmapRequest->all();
        
            //var_dump($attr); exit();
            
            if($attr['s_id_statusmenugmap'] == 1){
                $attr['s_ketmenugmap'] = 'AKTIF';
            }else{
                $attr['s_ketmenugmap'] = 'TIDAK AKTIF';
            }
        
            //var_dump($attr); exit();
        $update = $menugmap->update($attr);

        if ($update) {
            session()->flash('success', 'Data Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-menugmap');
    }

    public function destroy(Request $request) {
        Menugmap::where('s_idpejabat', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat')
                        ),
                        'setting_pejabat.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat')
                        ),
                        'setting_pejabat.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
    }

}
