<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\MenuuploadRequest;
use App\Models\Setting\Menuupload;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingPejabatExport;

class SettingMenuuploadController extends Controller {

    public function index() {
        return view('setting-menuupload.index', [
            'pejabats' => Menuupload::orderBy('updated_at', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request) {
        $response = new Menuupload();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_id_menuupload');
        }

        /*
        if ($request['filter']['s_namapejabat'] != null) {
            $response = $response->where('s_namapejabat', 'ilike', "%" . $request['filter']['s_namapejabat'] . "%");
        }
        if ($request['filter']['s_nippejabat'] != null) {
            $response = $response->where('s_nippejabat', 'ilike', "%" . $request['filter']['s_nippejabat'] . "%");
        }
        if ($request['filter']['s_jabatanpejabat'] != null) {
            $response = $response->where('s_jabatanpejabat', 'ilike', "%" . $request['filter']['s_jabatanpejabat'] . "%");
        }
        if ($request['filter']['s_golonganpejabat'] != null) {
            $response = $response->where('s_golonganpejabat', 'ilike', "%" . $request['filter']['s_golonganpejabat'] . "%");
        } */

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            
            $dataArr[] = [
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-menuupload/' . $v['s_id_menuupload'] . '/edit',
                        'actionActive' => true
                    ]
                    /* [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_menuupload'] . ')',
                        'actionActive' => true
                    ] */
                ],
                //'s_id_statusmenuupload' => $v['s_id_statusmenuupload'],
                's_ukuranfile' => $v['s_ukuranfile'],
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = Menuupload('s_idpejabat', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-menuupload.create', ['pejabat' => new Pejabat()]);
    }

    public function store(MenuuploadRequest $MenuuploadRequest) {
        $attr = $MenuuploadRequest->all();
        
        $create = Menuupload::create($attr);
        if ($create) {
            session()->flash('success', 'Data baru berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-menuupload');
    }

    public function edit(Menuupload $menuupload) {
        return view('setting-menuupload.edit', ['menuupload' => $menuupload]);
    }

    public function update(MenuuploadRequest $MenuuploadRequest, Menuupload $menuupload) {
        
            $attr = $MenuuploadRequest->all();
        
            
        
            //var_dump($attr); exit();
        $update = $menuupload->update($attr);

        if ($update) {
            session()->flash('success', 'Data Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-menuupload');
    }

    public function destroy(Request $request) {
        Menuupload::where('s_id_menuupload', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat')
                        ),
                        'setting_pejabat.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        return Excel::download(
                        new SettingPejabatExport(
                                $request->query('s_namapejabat'),
                                $request->query('s_jabatanpejabat')
                        ),
                        'setting_pejabat.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
    }

}
