<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Models\Setting\TargetStatus;
use GrahamCampbell\ResultType\Success;
use Illuminate\Http\Request;

class SettingTargetStatusController extends Controller
{
    public function index()
    {
        return view('setting-target-status.index');
    }

    public function datagrid(Request $request)
    {
        $response = new TargetStatus();

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_id_target_status');
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];

        foreach ($response as $v) {
            $dataArr[] = [
                'nama_persyaratan' => $v['s_nama_status'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'javascript:getDataStatus(' . $v['s_id_target_status'] . ', 1)',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:getDataStatus(' . $v['s_id_target_status'] . ', 2)',
                        'actionActive' => true
                    ]
                ]
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request)
    {
        $detail = TargetStatus::find($request->query('id'));

        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function store(Request $request)
    {
        $attr = $request->except('_token');
        
        $create = TargetStatus::create($attr);

        if ($create) {
            session()->flash('success', 'Data Status Target berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukan!');
        }

        return redirect('setting-target-status');
    }

    public function update(Request $request, TargetStatus $target_status)
    {
        $attr = $request->except('_token');
        $update = $target_status->update($attr);

        if ($update) {
            $message = 'Data Status Target berhasil diupdate!';
            $code = 201;
        } else {
            $message = 'Mohon cek kembali data yang anda masukan!';
            $code = 400;
        }

        return response()->json([
            $message,
            $code,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        ]);
    }

    public function destroy($target_status)
    {
        TargetStatus::where('s_id_target_status', '=', $target_status)->delete();

        return response()->json([
            'success' => 'Data berhasil dihapus!',
            204,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        ]);
    }
}
