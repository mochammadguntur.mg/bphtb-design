<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\NotarisRequest;
use App\Models\Dropdown\Status;
use App\Models\Setting\Notaris;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingNotarisExport;

class SettingNotarisController extends Controller
{
    public function index(){
        return view('setting-notaris.index', [
            'notariss' => Notaris::orderBy('updated_at', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request){
        $response = new Notaris();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('s_idnotaris');
        }

        if ($request['filter']['namanotaris'] != null) {
            $response = $response->where('s_namanotaris', 'ilike', "%" . $request['filter']['namanotaris'] . "%");
        }
        if ($request['filter']['kode'] != null) {
            $response = $response->where('s_kodenotaris', 'ilike', "%" . $request['filter']['kode'] . "%");
        }
        if ($request['filter']['status'] != null) {
            $response = $response->where('s_statusnotaris', 'like', "%" . $request['filter']['status'] . "%");
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $status = [1=>'Aktif',2=>'Tidak Aktif'];
            $dataArr[] = [
                'namanotaris' => $v['s_namanotaris'],
                'kodenotaris' => $v['s_kodenotaris'],
                'statusnotaris' => $status[$v['s_statusnotaris']],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-notaris/' . $v['s_idnotaris'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_idnotaris'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request){
        $detail = Notaris::where('s_idnotaris', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function create(){
        return view('setting-notaris.create', [
            'notaris' => new Notaris(),
            'status' => Status::get(),
        ]);
    }

    public function store(NotarisRequest $NotarisRequest){
        $attr = $NotarisRequest->all();

        $create = Notaris::create($attr);

        if($create){
            session()->flash('success', 'Data Notaris baru berhasil ditambahkan.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-notaris');

    }

    public function edit(Notaris $notaris){
        return view('setting-notaris.edit', [
            'notaris' => $notaris,
            'status' => Status::get(),
        ]);
    }

    public function update(NotarisRequest $notarisRequest, Notaris $notaris){
        $attr = $notarisRequest->all();
        $update = $notaris->update($attr);

        if($update){
            session()->flash('success', 'Data Notaris Behasil diupdate.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-notaris');
    }

    public function destroy(Request $request){
        Notaris::where('s_idnotaris', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request){
        return Excel::download(
            new SettingNotarisExport($request->query('s_namanotaris'), $request->query('s_kodenotaris'), $request->query('created_at')),
            'setting_notaris.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request){
        $pdf = Excel::download(
            new SettingNotarisExport($request->query('s_namanotaris'), $request->query('s_kodenotaris'), $request->query('created_at')),
            'setting_notaris.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $pdf;
    }

}
