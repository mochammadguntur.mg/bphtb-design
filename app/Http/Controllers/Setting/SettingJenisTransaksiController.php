<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Exports\Setting\SettingJenisTransaksiExport;
use App\Models\Setting\JenisTransaksi;
use App\Http\Requests\Setting\JenisTransaksiRequest;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SettingJenisTransaksiController extends Controller {

    public function index() {
        $data_statusperhitungan = (new JenisTransaksi())->cekdata_data_status_perhitungan();
        $data_statusdptnpoptkp = (new JenisTransaksi())->cekdata_data_status_dptnpoptkp();
        return view('setting-jenis-transaksi.index', [
            'data_statusperhitungan' => $data_statusperhitungan,
            'data_statusdptnpoptkp' => $data_statusdptnpoptkp
        ]);
    }

    public function datagrid(Request $request) {
        $response = (new JenisTransaksi())::select(
                        's_jenistransaksi.*',
                        's_status_perhitungan.s_nama',
                        's_status_dptnpoptkp.s_nama_dptnpoptkp'
                )->leftJoin('s_status_perhitungan', 's_status_perhitungan.s_idstatus_pht', '=', 's_jenistransaksi.s_idstatus_pht')
                ->leftJoin('s_status_dptnpoptkp', 's_status_dptnpoptkp.s_id_dptnpoptkp', '=', 's_jenistransaksi.s_id_dptnpoptkp');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_idjenistransaksi');
        }

        if ($request['filter']['s_kodejenistransaksi'] != null) {
            $response = $response->where('s_kodejenistransaksi', 'ilike', "%" . $request['filter']['s_kodejenistransaksi'] . "%");
        }

        if ($request['filter']['s_namajenistransaksi'] != null) {
            $response = $response->where('s_namajenistransaksi', 'ilike', "%" . $request['filter']['s_namajenistransaksi'] . "%");
        }

        if ($request['filter']['s_idstatus_pht'] != null) {
            $response = $response->where('s_jenistransaksi.s_idstatus_pht', '=', $request['filter']['s_idstatus_pht']);
        }

        if ($request['filter']['s_id_dptnpoptkp'] != null) {
            $response = $response->where('s_jenistransaksi.s_id_dptnpoptkp', '=', $request['filter']['s_id_dptnpoptkp']);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                's_kodejenistransaksi' => $v['s_kodejenistransaksi'],
                's_namajenistransaksi' => $v['s_namajenistransaksi'],
                's_nama' => !empty($v['s_nama']) ? $v['s_nama'] : '',
                's_nama_dptnpoptkp' => !empty($v['s_nama_dptnpoptkp']) ? $v['s_nama_dptnpoptkp'] : '',
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-jenis-transaksi/' . $v['s_idjenistransaksi'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_idjenistransaksi'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = JenisTransaksi::where('s_idjenistransaksi', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        $data_statusperhitungan = (new JenisTransaksi())->cekdata_data_status_perhitungan();
        $data_statusdptnpoptkp = (new JenisTransaksi())->cekdata_data_status_dptnpoptkp();
        return view('setting-jenis-transaksi.create', [
            'data_statusperhitungan' => $data_statusperhitungan,
            'data_statusdptnpoptkp' => $data_statusdptnpoptkp,
            'jenistransaksi' => new JenisTransaksi()
        ]);
    }

    public function store(JenisTransaksiRequest $JenisTransaksiRequest) {
        $attr = $JenisTransaksiRequest->all();

        $create = JenisTransaksi::create($attr);

        if ($create) {
            session()->flash('success', 'Data Jenis Transaksi baru berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-jenis-transaksi');
    }

    public function edit(JenisTransaksi $jenisTransaksi) {
        $data_statusperhitungan = (new JenisTransaksi())->cekdata_data_status_perhitungan();
        $data_statusdptnpoptkp = (new JenisTransaksi())->cekdata_data_status_dptnpoptkp();
        return view('setting-jenis-transaksi.edit', [
            'data_statusperhitungan' => $data_statusperhitungan,
            'data_statusdptnpoptkp' => $data_statusdptnpoptkp,
            'jenistransaksi' => $jenisTransaksi
        ]);
    }

    public function update(JenisTransaksiRequest $jenisTransaksiRequest, JenisTransaksi $jenisTransaksi) {
        $attr = $jenisTransaksiRequest->all();
        //dd($attr);
        $update = $jenisTransaksi->update($attr);
        if ($update) {
            session()->flash('success', 'Data Jenis Transaksi Berhasil diupdate.');
        } else {
            session()->flash('error', 'Mohon di cek kembali data yang anda masukkan.');
        }

        return redirect('setting-jenis-transaksi');
    }

    public function destroy(Request $request) {
        JenisTransaksi::where('s_idjenistransaksi', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingJenisTransaksiExport(
                                $request->query('s_kodejenistransaksi'),
                                $request->query('s_namajenistransaksi'),
                                $request->query('s_idstatus_pht'),
                                $request->query('s_id_dptnpoptkp'),
                        ),
                        'setting-jenis-transaksi.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        return Excel::download(
                        new SettingJenisTransaksiExport(
                                $request->query('s_kodejenistransaksi'),
                                $request->query('s_namajenistransaksi'),
                                $request->query('s_idstatus_pht'),
                                $request->query('s_id_dptnpoptkp'),
                        ),
                        'setting-jenis-transaksi.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
    }

}
