<?php

namespace App\Http\Controllers\Setting;

use App\Exports\UsersExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\UserRequest;
use App\Models\Setting\Notaris;
use App\Models\Setting\Pejabat;
use Spatie\Permission\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Support\Facades\DB;
use ZipArchive;

class SettingUserController extends Controller {

    public function index() {
        $submit = 'Create';
        $user = new User();
        return view('setting-user.index', compact('submit', 'user'));
    }

    public function tambah() {
        $submit = 'Create';
        $user = new User();
        $tipeHakAkses = Role::get();
        return view('setting-user.tambah', compact(
            'submit',
            'user',
            'tipeHakAkses'
    ));
    }

    public function backupdb() {
        // https://github.com/spatie/laravel-backup/issues/526
        $filename = Carbon::now()->format('Y-m-d-H-i-s');
        $tempPath = base_path('storage\app\backup-temp\\') . $filename;
        $backPath = base_path('storage\app\backups\\') . $filename;
        $command = 'pg_dump -U postgres ' . env('DB_DATABASE') . ' > ' . $tempPath . '.sql';
        exec($command);
        // zipping
        $zip = new ZipArchive;
        if ($zip->open($backPath . '.zip', ZipArchive::CREATE) === TRUE) {
            $zip->addFile($tempPath . '.sql', $filename . '.sql');
            $zip->close();
        }
        // clear temp folder
        $this->clearTempFolder();
        return response()->json(['success' => true]);
    }

    public function store(UserRequest $request) {
        $attr = $request->all();

        $attr['password'] = Hash::make($attr['password']);

        if ($attr['s_tipe_pejabat'] == 2) {
            $attr['s_idnotaris'] = $attr['s_idpejabat_idnotaris'];
        } else if ($attr['s_tipe_pejabat'] == 1) {
            $attr['s_idpejabat'] = $attr['s_idpejabat_idnotaris'];
        }

        User::create(Arr::except($attr, ['s_idpejabat_idnotaris']));
        $user = User::where('email', request('email'))->first();
        $user->assignRole(request('s_id_hakakses'));
        return redirect()->route('setting-user.index');
    }

    public function edit(User $user) {
        $dataPejabatAndNotaris = [];
        if ($user['s_tipe_pejabat'] == 1) {
            $data = Pejabat::get();
            foreach ($data as $v) {
                $dataPejabatAndNotaris[] = [
                    'id' => $v['s_idpejabat'],
                    'nama' => $v['s_namapejabat']
                ];
            }
        } else {
            $data = Notaris::get();
            foreach ($data as $v) {
                $dataPejabatAndNotaris[] = [
                    'id' => $v['s_idnotaris'],
                    'nama' => $v['s_namanotaris']
                ];
            }
        }
        $tipeHakAkses = Role::get();
        return view('setting-user.edit', [
            'user' => $user,
            'dataPejabatAndNotaris' => $dataPejabatAndNotaris,
            'tipeHakAkses' => $tipeHakAkses,
            'submit' => 'Update'
        ]);
    }

    public function update(User $user, UserRequest $request) {
        $attr = $request->all();
        $attr['password'] = Hash::make($attr['password']);

        if ($attr['s_tipe_pejabat'] == 2) {
            $attr['s_idnotaris'] = $attr['s_idpejabat_idnotaris'];
            $attr['s_idpejabat'] = null;

        } else if ($attr['s_tipe_pejabat'] == 1) {
            $attr['s_idpejabat'] = $attr['s_idpejabat_idnotaris'];
            $attr['s_idnotaris'] = null;
        }

        $user->update(Arr::except($attr, ['s_idpejabat_idnotaris']));
        $user->syncRoles($attr['s_id_hakakses']);
        return redirect()->route('setting-user.index');
    }

    public function datagrid(Request $request) {
        $response = new User();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('id');
        }

        if ($request['filter']['name'] != null) {
            $response = $response->where('name', 'ilike', "%" . $request['filter']['name'] . "%");
        }
        if ($request['filter']['username'] != null) {
            $response = $response->where('username', 'ilike', "%" . $request['filter']['username'] . "%");
        }
        if ($request['filter']['email'] != null) {
            $response = $response->where('email', 'ilike', "%" . $request['filter']['email'] . "%");
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'name' => $v['name'],
                'username' => $v['username'],
                'email' => $v['email'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => '/setting-user/' . $v['id'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['id'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $role = User::where('id', '=', $request->query('id'))->get();
        return response()->json(
                        $role,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function delete(Request $request) {
        User::where('id', '=', $request->query('id'))->delete();
    }

    // public function export(Request $request)
    // {
    //     return Excel::download(
    //         new UsersExport($request->query('name'), $request->query('email'), $request->query('created_at')),
    //         'users.xlsx',
    //         \Maatwebsite\Excel\Excel::XLSX
    //     );
    // }
    // public function export_pdf(Request $request)
    // {
    //     $sheet = Excel::download(
    //         new UsersExport($request->query('name'), $request->query('email'), $request->query('created_at')),
    //         'users.xlsx',
    //         \Maatwebsite\Excel\Excel::MPDF
    //     );
    //     // $sheet->setPaperSize(PHPExcel_Worksheet_PageSetup::PAPERSIZE_A3)
    //     return $sheet;
    // }


    public function listpejabatnotaris(Request $request) {
        $return_data = [];
        if ($request->query('s_tipe_pejabat') == 1) {
            $data = Pejabat::get();
            foreach ($data as $v) {
                $return_data[] = [
                    'id' => $v['s_idpejabat'],
                    'nama' => $v['s_namapejabat']
                ];
            }
        } else {
            $data = Notaris::get();
            foreach ($data as $v) {
                $return_data[] = [
                    'id' => $v['s_idnotaris'],
                    'nama' => $v['s_namanotaris']
                ];
            }
        }

        return response()->json(
                        $return_data,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

}
