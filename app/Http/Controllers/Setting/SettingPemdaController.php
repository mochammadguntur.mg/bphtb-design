<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\PemdaRequest;
use App\Models\Setting\Pemda;
use Illuminate\Support\Facades\Storage;

class SettingPemdaController extends Controller
{
    public function index()
    {
        return view('setting-pemda.index', ['pemda' => Pemda::orderBy('updated_at', 'desc')->first()]);
    }

    public function createOrUpdate(PemdaRequest $pemdaRequest)
    {
        $attr = $pemdaRequest->except(['_token']);
        if ($pemdaRequest->hasFile('s_logo')) {
            $files = $pemdaRequest->file('s_logo');
            $destinationPath = 'public/dist/img';
            $file = Storage::putFile(
                $destinationPath,
                $files
            );
            $nama_file = explode('/', $file);
            $files->move(public_path('/storage/public/dist/img'), $nama_file[3]);
            $attr['s_logo'] = $file;
        }

        if ($attr['s_idpemda'] == null) {
            $process = Pemda::create($attr);
        } else {
            $process = Pemda::where('s_idpemda', $attr['s_idpemda'])
                ->update($attr);
        }

        if ($process) {
            session()->flash('success', 'Data pemda berhasil di update.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-pemda');
    }
}
