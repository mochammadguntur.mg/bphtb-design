<?php

namespace App\Http\Controllers\Setting;

use App\Exports\Setting\SettingTargetExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\TargetPenerimaanRequest;
use App\Models\Setting\TargetPenerimaan;
use App\Models\Setting\TargetStatus;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SettingTargetController extends Controller {

    public function index() {
        return view('setting-target.index', [
            'target_status' => TargetStatus::get()
        ]);
    }

    public function datagrid(Request $request) {
        $response = new TargetPenerimaan();

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_tahun_target');
        }

        if (!empty($request['filter']['periode'])) {
            $response = $response->where('s_tahun_target', $request['filter']['periode']);
        }

        if (!empty($request['filter']['target_status'])) {
            $response = $response->where('s_id_target_status', $request['filter']['target_status']);
        }

        if (!empty($request['filter']['created_at'])) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));

            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];

        foreach ($response as $v) {
            $dataArr[] = [
                'nama_persyaratan' => $v['s_tahun_target'],
                'status' => $v->targetStatus['s_nama_status'],
                'jenis_peralihan' => number_format($v['s_nilai_target'], 0, ',', '.'),
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-target/' . $v['s_id_target_bphtb'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_id_target_bphtb'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }

        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];

        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = TargetPenerimaan::with('targetStatus')->find($request->query('id'));

        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-target.create', [
            'target' => new TargetPenerimaan(),
            'target_status' => TargetStatus::get(),
        ]);
    }

    public function store(TargetPenerimaanRequest $targetPenerimaanRequest) {
        $attr = $targetPenerimaanRequest->all();
        $attr['s_nilai_target'] = str_ireplace(".", '', $attr['s_nilai_target']);
        $target_cek = TargetPenerimaan::where([
                    ['s_id_target_status', '=', $attr['s_id_target_status']],
                    ['s_tahun_target', '=', $attr['s_tahun_target']],
                ])->get();

        if (count($target_cek) > 0) {
            session()->flash('error', 'Target untuk tahun ' . $attr['s_tahun_target'] . ' sudah di input!');
            return redirect('setting-target');
        }

        $create = TargetPenerimaan::create($attr);

        if ($create) {
            session()->flash('success', 'Data Target berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-target');
    }

    public function edit(TargetPenerimaan $setting_target) {
        return view('setting-target.edit', [
            'target' => $setting_target,
            'target_status' => TargetStatus::get(),
        ]);
    }

    public function update(TargetPenerimaanRequest $persyaratanRequest, TargetPenerimaan $setting_target) {
        $attr = $persyaratanRequest->all();
        $attr['s_nilai_target'] = str_ireplace(".", '', $attr['s_nilai_target']);

        $update = $setting_target->update($attr);

        if ($update) {
            session()->flash('success', 'Data Target berhasil diupdate!');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukan!');
        }

        return redirect('setting-target');
    }

    public function destroy($setting_target) {
        TargetPenerimaan::where('s_id_target_bphtb', '=', $setting_target)->delete();

        return response()->json([
                    'success' => 'Data berhasil dihapus!',
                    204,
                    ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                    JSON_UNESCAPED_UNICODE
        ]);
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingTargetExport(
                                $request->query('periode'),
                                $request->query('target_status'),
                                $request->query('created_at')
                        ),
                        'setting-target.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        return Excel::download(
                        new SettingTargetExport(
                                $request->query('periode'),
                                $request->query('target_status'),
                                $request->query('created_at')
                        ),
                        'setting-target.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
    }

}
