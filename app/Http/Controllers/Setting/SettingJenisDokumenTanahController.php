<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\JenisDokumenTanahRequest;
use App\Models\Setting\JenisDokumenTanah;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
//use App\Exports\Setting\SettingJenisDokumenTanahExport;
use DB;

class SettingJenisDokumenTanahController extends Controller
{
    public function index(){
        return view('setting-jenis-dokumen-tanah.index', [
            'JenisDokumenTanahs' => JenisDokumenTanah::orderBy('updated_at', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request){
        $response = new JenisDokumenTanah();
         if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('s_namadoktanah');
        }
        if ($request['filter']['namadoktanah'] != null) {
            $response = $response->where('s_namadoktanah', 'ilike', "%" . $request['filter']['namadoktanah'] . "%");
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        } 
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
           
            $dataArr[] = [
                'namadoktanah' => $v['s_namadoktanah'],
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-jenis-dokumen-tanah/' . $v['s_iddoktanah'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_iddoktanah'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }


    public function create(){
        $kecamatan = JenisDokumenTanah::all();
        return view('setting-jenis-dokumen-tanah.create', [
            'jenis_dokumen_tanah' => new JenisDokumenTanah()
        ]);
    }

    public function store(JenisDokumenTanahRequest $JenisDokumenTanahRequest){

        $attr = $JenisDokumenTanahRequest->all();
         $create = JenisDokumenTanah::create($attr);
        if($create){
            session()->flash('success', 'Dokumen Tanah berhasil di tambahkan.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('setting-jenis-dokumen-tanah'); 
    }

    public function edit(JenisDokumenTanah $JenisDokumenTanah){
        return view('setting-jenis-dokumen-tanah.edit', [
            'jenis_dokumen_tanah' => $JenisDokumenTanah
        ]);
    }

    
    public function update(JenisDokumenTanahRequest $JenisDokumenTanahRequest, JenisDokumenTanah $JenisDokumenTanah){
        $attr = $JenisDokumenTanahRequest->all();
        $update = $JenisDokumenTanah->update($attr);

        if($update){
            session()->flash('success', 'Data Jenis Dokumen Tanah Behasil diupdate.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-jenis-dokumen-tanah');
    }

    public function detail(Request $request){
        //dd($request);
        $detail = JenisDokumenTanah::where('s_iddoktanah', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function destroy(Request $request){
        //dd($request);
        JenisDokumenTanah::where('s_iddoktanah', '=', $request->query('id'))->delete();
    }

}
