<?php

namespace App\Http\Controllers\Setting;

use App\Exports\Setting\SettingTarifBphtbExport;
use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\TarifBphtbRequest;
use App\Models\Dropdown\Status;
use App\Models\Setting\TarifBphtb;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class SettingTarifBphtbController extends Controller {

    public function index() {
        return view('setting-tarif-bphtb.index', [
            'tarifs' => TarifBphtb::orderBy('updated_at', 'desc')->get()
        ]);
    }

    public function datagrid(Request $request) {
        $response = new TarifBphtb();
        // dd($response);
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_idtarifbphtb');
        }

        if ($request['filter']['s_tarif_bphtb'] != null) {
            $response = $response->where('s_tarif_bphtb', 'like', "%" . $request['filter']['s_tarif_bphtb'] . "%");
        }
        if ($request['filter']['s_dasar_hukum'] != null) {
            $response = $response->where('s_dasar_hukum', 'like', "%" . $request['filter']['s_dasar_hukum'] . "%");
        }
        if ($request['filter']['s_tgl_awal'] != null) {
            $date = explode(' - ', $request['filter']['s_tgl_awal']);
            $startDate = date('Y-m-d', strtotime($request['filter']['s_tgl_awal']));
            $response = $response->where('s_tgl_awal', $startDate);
        }
        if ($request['filter']['s_tgl_akhir'] != null) {
            $date = explode(' - ', $request['filter']['s_tgl_akhir']);
            $startDate = date('Y-m-d', strtotime($request['filter']['s_tgl_akhir']));
            $response = $response->where('s_tgl_akhir', $startDate);
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $dataArr[] = [
                'tarif' => $v['s_tarif_bphtb'],
                'dasar_hukum' => $v['s_dasar_hukum'],
                'tanggal_awal' => date('d-m-Y', strtotime($v['s_tgl_awal'])),
                'tanggal_akhir' => date('d-m-Y', strtotime($v['s_tgl_akhir'])),
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-tarif-bphtb/' . $v['s_idtarifbphtb'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_idtarifbphtb'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = TarifBphtb::where('s_idtarifbphtb', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-tarif-bphtb.create', [
            'tarif' => new TarifBphtb(),
            'status' => Status::get(),
        ]);
    }

    public function store(TarifBphtbRequest $tarifBphtbRequest) {
        $attr = $tarifBphtbRequest->all();
        $create = TarifBphtb::create($attr);
        if ($create) {
            session()->flash('success', 'Data baru telah ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('setting-tarif-bphtb');
    }

    public function edit(TarifBphtb $tarifBphtb) {
        return view('setting-tarif-bphtb.edit', [
            'tarif' => $tarifBphtb
        ]);
    }

    public function update(TarifBphtbRequest $tarifBphtbRequest, TarifBphtb $tarifBphtb) {
        $attr = $tarifBphtbRequest->all();
        $update = $tarifBphtb->update($attr);
        if ($update) {
            session()->flash('success', 'Data Tarif BPHTB Berhasil diupdate!.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('setting-tarif-bphtb');
    }

    public function destroy(Request $request) {
        TarifBphtb::where('s_idtarifbphtb', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingTarifBphtbExport($request->query('s_tarif_bphtb'), $request->query('s_dasar_hukum'), $request->query('s_tgl_awal'), $request->query('s_tgl_akhir'), $request->query('created_at')),
                        'setting_tarif_bphtb.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        $sheet = Excel::download(
                        new SettingTarifBphtbExport($request->query('s_tarif_bphtb'), $request->query('s_dasar_hukum'), $request->query('s_tgl_awal'), $request->query('s_tgl_akhir'), $request->query('created_at')),
                        'setting_tarif_bphtb.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
        return $sheet;
    }

}
