<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\PersentasewajarRequest;
use App\Models\Setting\Persentasewajar;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingPersentasewajarExport;

class SettingPersentasewajarController extends Controller {

    public function index() {
        return view('setting-persentasewajar.index', [
            'persentasewajars' => Persentasewajar::orderBy('updated_at', 'desc')->get(),
        ]);
    }

    public function datagrid(Request $request) {
        $response = new Persentasewajar();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_idpresentase_wajar');
        }

        if ($request['filter']['s_nilaimin'] != null) {
            $response = $response->where('s_nilaimin', 'ilike', "%" . $request['filter']['s_nilaimin'] . "%");
        }
        if ($request['filter']['s_ketpresentase_wajar'] != null) {
            $response = $response->where('s_ketpresentase_wajar', 'ilike', "%" . $request['filter']['s_ketpresentase_wajar'] . "%");
        }
        if ($request['filter']['s_nilaimax'] != null) {
            $response = $response->where('s_nilaimax', 'ilike', "%" . $request['filter']['s_nilaimax'] . "%");
        }
        if ($request['filter']['s_css_warna'] != null) {
            $response = $response->where('s_css_warna', 'ilike', "%" . $request['filter']['s_css_warna'] . "%");
        }

        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {

            $dataArr[] = [
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-persentasewajar/' . $v['s_idpresentase_wajar'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_idpresentase_wajar'] . ')',
                        'actionActive' => true
                    ]
                ],
                'namapejabat' => $v['s_nilaimin'],
                'jabatan' => $v['s_nilaimax'],
                'nippejabat' => $v['s_ketpresentase_wajar'],
                'golongan' => '<span style="background: ' . $v['s_css_warna'] . '; color: wheat; padding: 0.25em 0.75em; border-radius: 0.3em;"> warna indikator </span>'
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = Persentasewajar::where('s_idpresentase_wajar', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-persentasewajar.create', [
            'persentasewajar' => new Persentasewajar(),
        ]);
    }

    public function store(PersentasewajarRequest $PersentasewajarRequest) {
        $attr = $PersentasewajarRequest->all();
        $create = Persentasewajar::create($attr);
        if ($create) {
            session()->flash('success', 'Data Notaris baru berhasil ditamabahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('setting-persentasewajar');
    }

    public function edit(Persentasewajar $persentasewajar) {
        return view('setting-persentasewajar.edit', [
            'persentasewajar' => $persentasewajar
        ]);
    }

    public function update(PersentasewajarRequest $PersentasewajarRequest, Persentasewajar $persentasewajar) {
        $attr = $PersentasewajarRequest->all();
        $update = $persentasewajar->update($attr);
        if ($update) {
            session()->flash('success', 'Data Presentase Wajar Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('setting-persentasewajar');
    }

    public function destroy(Request $request) {
        Persentasewajar::where('s_idpresentase_wajar', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingPersentasewajarExport($request->query('s_nilaimin'), $request->query('s_nilaimax')),
                        'setting_persentasewajar.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        $pdf = Excel::download(
                        new SettingPersentasewajarExport($request->query('s_nilaimin'), $request->query('s_nilaimax')),
                        'setting_persentasewajar.pdf',
                        \Maatwebsite\Excel\Excel::MPDF
        );
        return $pdf;
    }

}
