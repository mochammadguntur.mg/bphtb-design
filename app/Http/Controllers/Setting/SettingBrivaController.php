<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Setting\BrivaRequest;
use App\Models\setting\Briva;
use App\Models\Dropdown\Status;
use App\Models\Dropdown\UrlAktif;

class SettingBrivaController extends Controller
{
    public function index() {
        return view('setting-briva.index', [
            'briva' => Briva::orderBy('updated_at', 'desc')->first(),
            'urlaktif' => UrlAktif::get(),
            'status' => Status::get(),
        ]);
    }

    public function createOrUpdate(BrivaRequest $brivaRequest) {
        $attr = $brivaRequest->except(['_token']);

        if ($attr['s_idbriva'] == null) {
            $process = Briva::create($attr);
        } else {
            $process = Briva::where('s_idbriva', $attr['s_idbriva'])
                    ->update($attr);
        }

        if ($process) {
            session()->flash('success', 'Data Briva berhasil di update.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-briva');
    }
}
