<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\AcuanRequest;
use App\Models\Setting\Acuan;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
//use App\Exports\Setting\SettingJenisDokumenTanahExport;
use App\Exports\Setting\SettingAcuanExport;
use DB;

class SettingHargaAcuanController extends Controller
{
    public function index(){
        return view('setting-harga-acuan.index', [
            'acuan' => Acuan::orderBy('updated_at', 'desc')->get(),
        ]);
    }

     public function datagrid(Request $request){
        $response = new Acuan();
         if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        }else{
            $response = $response->orderBy('s_kd_propinsi');
        }
        if ($request['filter']['kodepropinsi'] != null) {
            $response = $response->where('s_kd_propinsi', 'ilike', "%" . $request['filter']['kodepropinsi'] . "%");
        }
        if ($request['filter']['kodedati2'] != null) {
            $response = $response->where('s_kd_dati2', 'ilike', "%" . $request['filter']['kodedati2'] . "%");
        }
        if ($request['filter']['kodekecamatan'] != null) {
            $response = $response->where('s_kd_kecamatan', 'ilike', "%" . $request['filter']['kodekecamatan'] . "%");
        }
        if ($request['filter']['kodekelurahan'] != null) {
            $response = $response->where('s_kd_kelurahan', 'ilike', "%" . $request['filter']['kodekelurahan'] . "%");
        }
        if ($request['filter']['kodeblok'] != null) {
            $response = $response->where('s_kd_blok', 'ilike', "%" . $request['filter']['kodeblok'] . "%");
        }
        if ($request['filter']['permetertanah'] != null) {
            $response = $response->where('s_permetertanah', 'ilike', "%" . $request['filter']['permetertanah'] . "%");
        }
        if ($request['filter']['created_at'] != null) {
            $date = explode(' - ', $request['filter']['created_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        } 
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
           
            $dataArr[] = [
                'kodepropinsi' => $v['s_kd_propinsi'],
                'kodedati2' => $v['s_kd_dati2'],
                'kodekecamatan' => $v['s_kd_kecamatan'],
                'kodekelurahan' => $v['s_kd_kelurahan'],
                'kodeblok' => $v['s_kd_blok'],
                'permetertanah' => number_format($v['s_permetertanah'], 0,',','.'),
                'created_at' => date('d-m-Y', strtotime($v['created_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-harga-acuan/' . $v['s_idacuan'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_idacuan'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
            $response,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }


    public function create(){
        return view('setting-harga-acuan.create', [
            'acuan' => new Acuan()
        ]);
    }

    public function store(AcuanRequest $acuanRequest){

        $attr = $acuanRequest->all();
        $attr['s_permetertanah'] = str_replace(".","",$attr['s_permetertanah']);
        $create = Acuan::create($attr);
        if($create){
            session()->flash('success', 'Dokumen Tanah berhasil di tambahkan.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }
        return redirect('setting-harga-acuan'); 
    }

    public function edit(Acuan $acuan){
        // dd($acuan);  
        return view('setting-harga-acuan.edit', [
            'acuan' => $acuan
        ]);
    }

    
    public function update(AcuanRequest $AcuanRequest, Acuan $Acuan){
        $attr = $AcuanRequest->all();
        $attr['s_permetertanah'] = str_replace(".","",$attr['s_permetertanah']);
        $update = $Acuan->update($attr);

        if($update){
            session()->flash('success', 'Data Harga Acuan Behasil diupdate.');
        }else{
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-harga-acuan');
    }

    public function detail(Request $request){
        //dd($request);
        $detail = Acuan::where('s_idacuan', '=', $request->query('id'))->get();
        return response()->json(
            $detail,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function destroy(Request $request){
        //dd($request);
        Acuan::where('s_idacuan', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request){
        return Excel::download(
            new SettingAcuanExport(
                $request->query('s_idacuan'), 
                $request->query('s_permetertanah'), 
                $request->query('created_at')),
            'setting_harga_acuan.xlsx',
            \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request){
        $pdf = Excel::download(
            new SettingAcuanExport(
                $request->query('s_idacuan'), 
                $request->query('s_permetertanah'), 
                $request->query('created_at')),
            'setting_harga_acuan.pdf',
            \Maatwebsite\Excel\Excel::MPDF
        );
        return $pdf;
    } 

}
