<?php

namespace App\Http\Controllers\Setting;

use App\Http\Controllers\Controller;
use App\Http\Requests\Setting\KecamatanRequest;
use App\Models\Setting\Kecamatan;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Setting\SettingKecamatanExport;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class SettingKecamatanController extends Controller {

    public function index() {
        return view('setting-kecamatan.index', [
            'kecamatans' => Kecamatan::orderBy('updated_at', 'desc')->get(),
        ]);
    }
    

    public function sync() {
        $kecamatan_oracle1 = DB::connection('oracle')->table("REF_KECAMATAN")->get();
        $kecamatan_postgre = DB::connection('pgsql')->table("s_kecamatan")->get();

        $unsetdarioracle = array();
        $offset = 0;
        $kodesama = false;

        
        $cek_data_propinsi = DB::connection('oracle')->table("REF_PROPINSI")->first();
        $cek_data_dati2 = DB::connection('oracle')->table("REF_DATI2")->first();
        
        /*$collection = collect([
            (object) ['kd_propinsi'=>$cek_data_propinsi->kd_propinsi, 'kd_dati2' => $cek_data_dati2->kd_dati2, 'kd_kecamatan' => '000', 'nm_kecamatan' => 'LUAR DAERAH']
        ]);*/
        
        $kecamatan_oracle = collect($kecamatan_oracle1);
        //$kecamatan_oracle = collect($collection);
  
        $kecamatan_oracle->push((object) ['kd_propinsi'=>$cek_data_propinsi->kd_propinsi, 'kd_dati2' => $cek_data_dati2->kd_dati2, 'kd_kecamatan' => '000', 'nm_kecamatan' => 'LUAR DAERAH']);
        //$kecamatan_oracle->push($kecamatan_oracle1);
        
        $kecamatan_oracle->all();
        
        //print_r($kecamatan_oracle); exit();
        
        foreach ($kecamatan_oracle as $o_kecamatan) {
            foreach ($kecamatan_postgre as $p_kecamatan) {
                if ($o_kecamatan->kd_kecamatan == $p_kecamatan->s_kd_kecamatan) {
                    $unsetdarioracle[$offset]['kodekecamatan'] = $o_kecamatan->kd_kecamatan;
                    $unsetdarioracle[$offset]['namakecamatan'] = $o_kecamatan->nm_kecamatan;
                    $kodesama = true;
                    continue;
                }
            }
            ++$offset;
        }

        foreach ($kecamatan_oracle as $key => $value) {
            foreach ($unsetdarioracle as $unset) {
                if ($value->kd_kecamatan == $unset['kodekecamatan']) {
                    unset($kecamatan_oracle[$key]);
                }
            }
        }

        $kecamatan_oracle = $kecamatan_oracle->toArray();
        $final_data = array();
        $offset = 0;
        foreach ($kecamatan_oracle as $value) {
            $final_data[$offset]['s_kd_propinsi'] = $value->kd_propinsi;
            $final_data[$offset]['s_kd_dati2'] = $value->kd_dati2;
            $final_data[$offset]['s_kd_kecamatan'] = $value->kd_kecamatan;
            $final_data[$offset]['s_namakecamatan'] = $value->nm_kecamatan;
            $final_data[$offset]['created_at'] = \Carbon\Carbon::now();
            $final_data[$offset]['updated_at'] = \Carbon\Carbon::now();
            ++$offset;
        }

        if (count($final_data) < 1) {
            session()->flash('success', 'Data postgre sudah terupdate');
        } else {

            $insert = DB::table('s_kecamatan')->insert($final_data);
            //session()->flash('success', count($final_data) . 'Data Berhasil Sync ' . json_decode($final_data, true));
            session()->flash('success', count($final_data) . 'Data Berhasil Sync ');
        }

        // POSTGREE TO ORACLE---------------------------------------------------------------------------------------------------------';
        $kecamatan_oracle = NULL;
        $kecamatan_postgre = NULL;
        $kecamatan_oracle = DB::connection('oracle')->table("REF_KECAMATAN")->get();
        $kecamatan_postgre = DB::connection('pgsql')->table("s_kecamatan")->get();

        $unsetdaripostgre = array();
        $offset = 0;
        $kodesama = false;

        //  cek data yang sama
        foreach ($kecamatan_postgre as $p_kecamatan) {
            foreach ($kecamatan_oracle as $o_kecamatan) {
                if ($p_kecamatan->s_kd_kecamatan == $o_kecamatan->kd_kecamatan) {
                    $unsetdaripostgre[$offset]['kodekecamatan'] = $p_kecamatan->s_kd_kecamatan;
                    $unsetdaripostgre[$offset]['namakecamatan'] = $p_kecamatan->s_namakecamatan;
                    $kodesama = true;
                    continue;
                }
            }
            ++$offset;
        }

        // unset data dari postgre
        foreach ($kecamatan_postgre as $key => $value) {
            foreach ($unsetdaripostgre as $unset) {
                if ($value->s_kd_kecamatan == $unset['kodekecamatan']) {
                    unset($kecamatan_postgre[$key]);
                }
            }
        }

        $kecamatan_postgre = $kecamatan_postgre->toArray();

        if (count($kecamatan_postgre) > 1) {
            foreach ($kecamatan_postgre as $value) {
                $insert = DB::connection('oracle')->table('REF_KECAMATAN')->insert(
                        [
                            'kd_propinsi' => $value->s_kd_propinsi,
                            'kd_dati2' => $value->s_kd_dati2,
                            'kd_kecamatan' => $value->s_kd_kecamatan,
                            'nm_kecamatan' => $value->s_namakecamatan
                        ]
                );
            }

            session()->flash('success', count($kecamatan_postgre) . 'Data Berhasil Sync ');
        }
        return redirect('setting-kecamatan');
    }

    public function datagrid(Request $request) {
        $response = new Kecamatan();
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('s_idkecamatan');
        }
        if ($request['filter']['kd_propinsi'] != null) {
            $response = $response->where('s_kd_propinsi', 'ilike', "%" . $request['filter']['kd_propinsi'] . "%");
        }
        if ($request['filter']['kd_dati2'] != null) {
            $response = $response->where('s_kd_dati2', 'ilike', "%" . $request['filter']['kd_dati2'] . "%");
        }
        if ($request['filter']['kd_kecamatan'] != null) {
            $response = $response->where('s_kd_kecamatan', 'ilike', "%" . $request['filter']['kd_kecamatan'] . "%");
        }
        if ($request['filter']['namakecamatan'] != null) {
            $response = $response->where('s_namakecamatan', 'ilike', "%" . $request['filter']['namakecamatan'] . "%");
            //dd($response);
        }
        if ($request['filter']['latitude'] != null) {
            $response = $response->where('s_latitude', 'ilike', "%" . $request['filter']['latitude'] . "%");
        }
        if ($request['filter']['longitude'] != null) {
            $response = $response->where('s_longitude', 'ilike', "%" . $request['filter']['longitude'] . "%");
        }
        if (!empty($request['filter']['updated_at'])) {
            $date = explode(' - ', $request['filter']['updated_at']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('updated_at', [$startDate, $endDate]);
        }
        $response = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        $dataArr = [];
        foreach ($response as $v) {
            $status = [1 => 'Aktif', 2 => 'Tidak Aktif'];
            $dataArr[] = [
                'kd_propinsi' => $v['s_kd_propinsi'],
                'kd_dati2' => $v['s_kd_dati2'],
                'kd_kecamatan' => $v['s_kd_kecamatan'],
                'namakecamatan' => $v['s_namakecamatan'],
                'latitude' => (!empty($v['s_latitude'])) ? $v['s_latitude'] : '',
                'longitude' => (!empty($v['s_longitude'])) ? $v['s_longitude'] : '',
                'updated_at' => date('d-m-Y', strtotime($v['updated_at'])),
                'actionList' => [
                    [
                        'actionName' => 'edit',
                        'actionUrl' => 'setting-kecamatan/' . $v['s_idkecamatan'] . '/edit',
                        'actionActive' => true
                    ],
                    [
                        'actionName' => 'delete',
                        'actionUrl' => 'javascript:showDeleteDialog(' . $v['s_idkecamatan'] . ')',
                        'actionActive' => true
                    ]
                ]
            ];
        }
        $response = [
            'data' => [
                'content' => $dataArr,
                'number' => $response->currentPage() - 1,
                'size' => $response->perPage(),
                'first' => $response->onFirstPage(),
                'last' => $response->lastPage() == $response->currentPage() ? true : false,
                'totalPages' => $response->lastPage(),
                'numberOfElements' => $response->count(),
                'totalElements' => $response->total()
            ]
        ];
        return response()->json(
                        $response,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function detail(Request $request) {
        $detail = Kecamatan::where('s_idkecamatan', '=', $request->query('id'))->get();
        return response()->json(
                        $detail,
                        200,
                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
                        JSON_UNESCAPED_UNICODE
        );
    }

    public function create() {
        return view('setting-kecamatan.create', [
            'kecamatan' => new Kecamatan(),
        ]);
    }

    public function store(Request $request, KecamatanRequest $kecamatanRequest) {
        $attr = $kecamatanRequest->all();
        //dd($attr);
        $this->validate($request, [
            's_kd_kecamatan' => 'unique:s_kecamatan'
        ]);
        $create = Kecamatan::create($attr);
        //dd($create);
        if ($create) {
            session()->flash('success', 'Data Kecamatan berhasil di tambahkan.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-kecamatan');
    }

    public function edit(Kecamatan $kecamatan) {
        //dd($kecamatan);
        return view('setting-kecamatan.edit', [
            'kecamatan' => $kecamatan
        ]);
    }

    public function update(KecamatanRequest $kecamatanRequest, Kecamatan $kecamatan) {
        //dd($kecamatanRequest);
        $attr = $kecamatanRequest->all();
        // dd($attr);
        $update = $kecamatan->update($attr);

        if ($update) {
            session()->flash('success', 'Data Kecamatan Behasil diupdate.');
        } else {
            session()->flash('error', 'Mohon cek kembali data yang anda masukkan.');
        }

        return redirect('setting-kecamatan');
    }

    public function destroy(Request $request) {
        Kecamatan::where('s_idkecamatan', '=', $request->query('id'))->delete();
    }

    public function export_xls(Request $request) {
        return Excel::download(
                        new SettingKecamatanExport(
                                $request->query('s_kd_kecamatan'),
                                $request->query('s_namakecamatan'),
                                $request->query('updated_at')
                        ),
                        'setting_kecamatan.xlsx',
                        \Maatwebsite\Excel\Excel::XLSX
        );
    }

    public function export_pdf(Request $request) {
        $pdf = new SettingKecamatanExport($request->query('s_kd_kecamatan'), $request->query('s_namakecamatan'), $request->query('updated_at'));
        return Excel::download($pdf, 'setting_kecamatan.pdf', \Maatwebsite\Excel\Excel::MPDF);
//        $html = "<div><a href='http://localhost:8000/jadwal.pdf' class='media'></a></div>";
//        return $html;
//        return response()->json($html,
//                        200,
//                        ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
//                        JSON_UNESCAPED_UNICODE);
    }

}
