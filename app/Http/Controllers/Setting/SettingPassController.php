<?php

namespace App\Http\Controllers\Setting;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;
use App\Http\Requests\Setting\PassRequest;

class SettingPassController extends Controller
{
    //
    public function index()
    {
        $users = User::where('id',Auth::user()->id)->get();
        return view('ubah-pass.index',['datauser' => $users[0]]);
    }

    public function updatepass(Request $request){
        $oldUser = User::where('id',Auth::user()->id)->get()[0];
        $a = Auth::user()->id;
        $user = auth()->user();
        if ($request->s_gambar == null) {
            $request->s_gambar = $oldUser->s_gambar;
        }
        
        if ($request->hasFile('s_gambar') && Hash::check($request->old_password, $user->password )) {
            $files = $request->file('s_gambar');
            $destinationPath = 'upload/profil/';
            $files->move($destinationPath, $files->getClientOriginalName());
            $request->s_gambar = $destinationPath . $files->getClientOriginalName();
            $user->update([
                's_gambar' => $request->s_gambar
            ]);
        }

        if(Hash::check($request->old_password, $user->password )){
            $request->validate([
                'old_password' => 'required|min:8|max:100',
                'new_password' => 'required|min:8|max:100',
                'confirm_password' => 'required|same:new_password',
                ]);
            $user->update([
                'password' => bcrypt($request->new_password)
            ]);

            session()->flash('success','Password Berhasil di Perbarui');
        }else{
            session()->flash('error','Password lama tidak sama');
        }
        return redirect('ubah-pass/index');
    }
    
    /*
    public function updatepass(Request $request){
        $a = Auth::user()->id ;
        $request->validate([
            'old_password' => 'required|min:8|max:100',
            'new_password' => 'required|min:8|max:100',
            'confirm_password' => 'required|same:new_password',
            ]);
            // $attr = $request->all();
            $user = auth()->user();
            // dd(Hash::check($request->old_password, $user->password));
            // dd($user->getAuthPassword());
        if(Hash::check($request->old_password, $user->password )){
            $user->update([
                // $attr['password'] = Hash::make($attr['password']);
                'password' => bcrypt($request->new_password)
            ]);

            session()->flash('success','Password Berhasil di Perbarui');
        }else{
            session()->flash('error','Password lama tidak sama');
        }
        return redirect('ubah-pass/index');
    }*/
}
