<?php

namespace App\Http\Controllers\Informasiop;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Pbb\Sppt;
use App\Models\Spt\Spt;
use App\Models\Pbb\RefKecamatan;
use App\Models\Pbb\RefKelurahan;
use Maatwebsite\Excel\Facades\Excel;
use App\Exports\Informasiop\InformasiopExport;
use PDF;

class InformasiopController extends Controller
{
    public function index()
    {
        return view('informasiop.index');
    }

    public function caridata(Request $request)
    {
        $NOP = $request->query('nop');
        $KD_PROV    = substr($NOP, 0, 2);
        $kd_dati2   = substr($NOP, 3, 2);
        $KD_KEC     = substr($NOP, 6, 3);
        $KD_KEL     = substr($NOP, 10, 3);
        $KD_BLOK    = substr($NOP, 14, 3);
        $NO_URUT    = substr($NOP, 18, 4);
        $KD_JNS     = substr($NOP, 23, 1);
        $TAHUNPAJAK = substr($NOP, 25, 4);

        $query = Sppt::select()
            ->select(
                'sppt.*',
                'ref_kecamatan.nm_kecamatan',
                'ref_kelurahan.nm_kelurahan',
                'kelas_tanah.nilai_per_m2_tanah',
                'kelas_bangunan.nilai_per_m2_bng'
            )
            ->leftJoin('ref_kecamatan', [
                ['ref_kecamatan.kd_propinsi', '=', 'sppt.kd_propinsi'],
                ['ref_kecamatan.kd_dati2', '=', 'sppt.kd_dati2'],
                ['ref_kecamatan.kd_kecamatan', '=', 'sppt.kd_kecamatan']
            ])
            ->leftJoin('ref_kelurahan', [
                ['ref_kelurahan.kd_propinsi', '=', 'sppt.kd_propinsi'],
                ['ref_kelurahan.kd_dati2', '=', 'sppt.kd_dati2'],
                ['ref_kelurahan.kd_kecamatan', '=', 'sppt.kd_kecamatan'],
                ['ref_kelurahan.kd_kelurahan', '=', 'sppt.kd_kelurahan']
            ])
            ->leftJoin('kelas_tanah', [
                ['kelas_tanah.kd_kls_tanah', '=', 'sppt.kd_kls_tanah']
            ])
            ->leftJoin('kelas_bangunan', [
                ['kelas_bangunan.kd_kls_bng', '=', 'sppt.kd_kls_bng']
            ])
            ->where([
                ['sppt.kd_propinsi', '=', $KD_PROV],
                ['sppt.kd_dati2', '=', $kd_dati2],
                ['sppt.kd_kecamatan', '=', $KD_KEC],
                ['sppt.kd_kelurahan', '=', $KD_KEL],
                ['sppt.kd_blok', '=', $KD_BLOK],
                ['sppt.no_urut', '=', $NO_URUT],
                ['sppt.kd_jns_op', '=', $KD_JNS],
                ['sppt.thn_pajak_sppt', '=', $TAHUNPAJAK],
            ])->get();

        $sql_tunggakan = Sppt::select(
            'sppt.thn_pajak_sppt',
            'sppt.pbb_yg_harus_dibayar_sppt',
            'sppt.tgl_jatuh_tempo_sppt'
        )
            ->where([
                ['sppt.kd_propinsi', '=', $KD_PROV],
                ['sppt.kd_dati2', '=', $kd_dati2],
                ['sppt.kd_kecamatan', '=', $KD_KEC],
                ['sppt.kd_kelurahan', '=', $KD_KEL],
                ['sppt.kd_blok', '=', $KD_BLOK],
                ['sppt.no_urut', '=', $NO_URUT],
                ['sppt.kd_jns_op', '=', $KD_JNS],
                ['sppt.thn_pajak_sppt', '<=', $TAHUNPAJAK],
                ['sppt.status_pembayaran_sppt', '=', '0'],
            ])
            ->orderBy('thn_pajak_sppt', 'ASC')->get();
        if ($query->count() == 0) {
            $response = '<span class="text-red"><i class="fas fa-warning"></i> Data Tidak Ditemukan</span>';
        } else {

            $response = '<div class="col-12">
                <div class="input-group input-group-sm">
                <label class="col-2">NOP/TAHUN</label>
                <div class="col-4">: <strong>' . $NOP . '</strong></div>
                </div>
        </div>';
            $response .= '<div class="col-12">
                <div class="input-group input-group-sm">
                <label class="col-2">Nama WP SPPT</label>
                <div class="col-4">: ' . $query[0]->nm_wp_sppt . '</div>
                </div>
        </div>';
            $response .= '<div class="col-12">
                <div class="input-group input-group-sm">
                <label class="col-2">Letak</label> <div class="col-4">: ' . $query[0]->jln_wp_sppt . '</div>
                <label class="col-2">RT/RW</label> <div class="col-4">: ' . $query[0]->rt_wp_sppt . '/' . $query[0]->rw_wp_sppt . '</div>
                </div>
        </div>';
            $response .= '<div class="col-12">
                <div class="input-group input-group-sm">
                <label class="col-2">Kelurahan</label> <div class="col-4">: ' . $query[0]->nm_kelurahan . '</div>
                <label class="col-2">Kecamatan</label> <div class="col-4">: ' . $query[0]->nm_kecamatan . '</div>
                </div>
        </div>';
            if($query[0]->luas_bumi_sppt > 0){
                $njop_tanah = $query[0]->njop_bumi_sppt / $query[0]->luas_bumi_sppt;
            }else{
                $njop_tanah = 0;
            }
            $response .= '<div class="col-12">
                <div class="input-group input-group-sm">
                <label class="col-2">Luas Tanah (M<sup>2</sup>)</label> <div class="col-4">: ' . number_format($query[0]->luas_bumi_sppt, 0, ',', '.') . '</div>
                <label class="col-2">NJOP Tanah</label> <div class="col-4">: ' . number_format($njop_tanah, 0, ',', '.') . '</div>
                </div>
        </div>';
            if($query[0]->luas_bng_sppt > 0){
                $njop_bangunan = $query[0]->njop_bng_sppt / $query[0]->luas_bng_sppt;
            }else{
                $njop_bangunan = 0;
            }
            
            $response .= '<div class="col-12">
                <div class="input-group input-group-sm">
                <label class="col-2">Luas Bangunan (M<sup>2</sup>)</label> <div class="col-4">: ' . number_format($query[0]->luas_bng_sppt, 0, ',', '.') . '</div>
                <label class="col-2">NJOP Bangunan</label> <div class="col-4">: ' . number_format($njop_bangunan, 0, ',', '.') . '</div>
                </div>
        </div>';
            $response .= '<div class="col-12 table-responsive" style="border:1px solid #ccc;">
            <table class="table table-hover table-striped">
                <thead>
                    <tr class="bg-warning">
                        <th colspan="5" style="font-size:12pt;">TUNGGAKAN SPPT-PBB</th>
                    </tr>
                    <tr>
                        <th>NO.</th>
                        <th>TAHUN</th>
                        <th>JMLH TUNGGAKAN (Rp)</th>
                        <th>TGL JATUH TEMPO</th>
                        <th>JMLH DENDA (Rp)</th>
                    </tr>
                </thead>
                <tbody>';
            if ($sql_tunggakan->count() == 0) {
                $response .= '<tr><td colspan="5">Tidak Ada Tunggakan!</td></tr>';
            }
            $counter = 1;
            $jmlh_tunggakan = 0;
            $jmlh_denda = 0;
            foreach ($sql_tunggakan as $v) {
                $tgljatuhtempo = date('Y-m-d', strtotime($v['tgl_jatuh_tempo_sppt']));
                $tglsekarang = date('Y-m-d');
                $ts1 = strtotime($tgljatuhtempo);
                $ts2 = strtotime($tglsekarang);

                $year1 = date('Y', $ts1);
                $year2 = date('Y', $ts2);

                $month1 = date('m', $ts1);
                $month2 = date('m', $ts2);

                $day1 = date('d', $ts1);
                $day2 = date('d', $ts2);
                if ($day1 < $day2) {
                    $tambahanbulan = 1;
                } else {
                    $tambahanbulan = 0;
                }

                $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
                if ($jmlbulan > 24) {
                    $jmlbulan = 24;
                    $jmldenda = $jmlbulan * 2 / 100 * $v['pbb_yg_harus_dibayar_sppt'];
                } elseif ($jmlbulan <= 0) {
                    $jmlbulan = 0;
                    $jmldenda = $jmlbulan * 2 / 100 * $v['pbb_yg_harus_dibayar_sppt'];
                } else {
                    $jmldenda = $jmlbulan * 2 / 100 * $v['pbb_yg_harus_dibayar_sppt'];
                }
                $response .= '<tr>
                        <td>' . $counter++ . '</td>
                        <td class="text-center">' . $v['thn_pajak_sppt'] . '</td>
                        <td class="text-right">' . number_format($v['pbb_yg_harus_dibayar_sppt'], 0, ',', '.') . '</td>
                        <td class="text-center">' . date('d-m-Y', strtotime($v['tgl_jatuh_tempo_sppt'])) . '</td>
                        <td class="text-right">' . number_format($jmldenda, 0, ',', '.') . '</td>
                    </tr>';
                $jmlh_tunggakan += $v['pbb_yg_harus_dibayar_sppt'];
                $jmlh_denda += $jmldenda;
            }

            $total_tunggakan = ($jmlh_tunggakan + $jmlh_denda);
            $response .= '</tbody>
                <tfoot>
                    <tr class="bg-gray">
                        <th colspan="2">JUMLAH TUNGGAKAN (Rp)</th>
                        <th class="text-right">' . number_format($jmlh_tunggakan, 0, ',', '.') . '</th>
                        <th class="text-center">JUMLAH DENDA (Rp)</th>
                        <th class="text-right">' . number_format($jmlh_denda, 0, ',', '.') . '</th>
                    </tr>
                    <tr class="bg-danger">
                        <th colspan="3">TOTAL TUNGGAKAN (Rp)</th>
                        <th colspan="2" class="text-center" style="font-size:14pt;">' . number_format($total_tunggakan, 0, ',', '.') . '</th>
                    </tr>
                </tfoot>
            </table>
        </div>';
        }

        $queryBphtb = Spt::select()
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->where('t_nik_pembeli', '=', $request->query('nik'))
            ->orWhere('t_npwpd', '=', $request->query('npwpd'))->get();

        if(count($queryBphtb) != 0){

            $bphtb = '<div class="col-12 table-responsive" style="border:1px solid #ccc;">
            <table class="table table-hover table-striped">
                <thead>
                    <tr>
                        <th>NO.</th>
                        <th>TGL DAFTAR</th>
                        <th>NO KOHIR</th>
                        <th>NIK</th>
                        <th>NPWPD</th>
                        <th>NAMA WP</th>
                        <th>NOP</th>
                        <th>JMLH PAJAK (Rp)</th>
                        <th>JENIS TRANSAKSI</th>
                    </tr>
                </thead>
                <tbody>';
            $counter = 1;
            foreach($queryBphtb as $v){
                $bphtb .= '<tr>
                        <td>' . $counter++ . '</td>
                        <td class="text-center">' . date('d-m-Y', strtotime($v['t_tgldaftar_spt'])) . '</td>
                        <td class="text-center">' . $v['t_kohirspt'] . '</td>
                        <td class="text-center">' . $v['t_nik_pembeli'] . '</td>
                        <td class="text-center">' . $v['t_npwpd'] . '</td>
                        <td class="">' . $v['t_p_nama_pembeli'] . '</td>
                        <td class="text-center">' . $v['t_nop_sppt'] . '</td>
                        <td class="text-right">' . number_format($v['t_nilai_bphtb_fix'], 0, ',', '.') . '</td>
                        <td class="">' . $v['s_namajenistransaksi'] . '</td>
                    </tr>';
                $jmlh_tunggakan += $v['t_nilai_bphtb_fix'];
            }
            $bphtb .= '</tbody>
                <tfoot>
                    <tr class="bg-danger">
                        <th colspan="7">JUMLAH TUNGGAKAN (Rp)</th>
                        <th class="text-right" style="font-size:14pt;">' . number_format($jmlh_tunggakan, 0, ',', '.') . '</th>
                        <th class="text-center"></th>
                    </tr>
                </tfoot>
            </table>';

        }else{
            $bphtb = '<span class="text-red"><i class="fas fa-warning"></i> Data Tidak Ditemukan</span>';

        }

        $pdl = '<span class="text-red"><i class="fas fa-warning"></i> Data Tidak Ditemukan</span>';

        $res = [
            'data' => [
                'pbb' => $response,
                'bphtb' => $bphtb,
                'pdl' => $pdl
            ]
        ];

        return response()->json(
            $res,
            200,
            ['Content-Type' => 'application/json;charset=UTF-8', 'Charset' => 'utf-8'],
            JSON_UNESCAPED_UNICODE
        );
    }

    public function export_pdf(Request $request){
            $NOP = $request->query('t_nop');
            $KD_PROV    = substr($NOP, 0, 2);
            $kd_dati2   = substr($NOP, 3, 2);
            $KD_KEC     = substr($NOP, 6, 3);
            $KD_KEL     = substr($NOP, 10, 3);
            $KD_BLOK    = substr($NOP, 14, 3);
            $NO_URUT    = substr($NOP, 18, 4);
            $KD_JNS     = substr($NOP, 23, 1);
            $TAHUNPAJAK = substr($NOP, 25, 4);

            $query = Sppt::select()
                ->select(
                    'sppt.*',
                    'ref_kecamatan.nm_kecamatan',
                    'ref_kelurahan.nm_kelurahan',
                    'kelas_tanah.nilai_per_m2_tanah',
                    'kelas_bangunan.nilai_per_m2_bng'
                )
                ->leftJoin('ref_kecamatan', [
                    ['ref_kecamatan.kd_propinsi', '=', 'sppt.kd_propinsi'],
                    ['ref_kecamatan.kd_dati2', '=', 'sppt.kd_dati2'],
                    ['ref_kecamatan.kd_kecamatan', '=', 'sppt.kd_kecamatan']
                ])
                ->leftJoin('ref_kelurahan', [
                    ['ref_kelurahan.kd_propinsi', '=', 'sppt.kd_propinsi'],
                    ['ref_kelurahan.kd_dati2', '=', 'sppt.kd_dati2'],
                    ['ref_kelurahan.kd_kecamatan', '=', 'sppt.kd_kecamatan'],
                    ['ref_kelurahan.kd_kelurahan', '=', 'sppt.kd_kelurahan']
                ])
                ->leftJoin('kelas_tanah', [
                    ['kelas_tanah.kd_kls_tanah', '=', 'sppt.kd_kls_tanah']
                ])
                ->leftJoin('kelas_bangunan', [
                    ['kelas_bangunan.kd_kls_bng', '=', 'sppt.kd_kls_bng']
                ])
                ->where([
                    ['sppt.kd_propinsi', '=', $KD_PROV],
                    ['sppt.kd_dati2', '=', $kd_dati2],
                    ['sppt.kd_kecamatan', '=', $KD_KEC],
                    ['sppt.kd_kelurahan', '=', $KD_KEL],
                    ['sppt.kd_blok', '=', $KD_BLOK],
                    ['sppt.no_urut', '=', $NO_URUT],
                    ['sppt.kd_jns_op', '=', $KD_JNS],
                    ['sppt.thn_pajak_sppt', '=', $TAHUNPAJAK],
                ])->get();

            $sql_tunggakan = Sppt::select(
                'sppt.thn_pajak_sppt',
                'sppt.pbb_yg_harus_dibayar_sppt',
                'sppt.tgl_jatuh_tempo_sppt'
            )
                ->where([
                    ['sppt.kd_propinsi', '=', $KD_PROV],
                    ['sppt.kd_dati2', '=', $kd_dati2],
                    ['sppt.kd_kecamatan', '=', $KD_KEC],
                    ['sppt.kd_kelurahan', '=', $KD_KEL],
                    ['sppt.kd_blok', '=', $KD_BLOK],
                    ['sppt.no_urut', '=', $NO_URUT],
                    ['sppt.kd_jns_op', '=', $KD_JNS],
                    ['sppt.thn_pajak_sppt', '<=', $TAHUNPAJAK],
                    ['sppt.status_pembayaran_sppt', '=', '0'],
                ])
                ->orderBy('thn_pajak_sppt', 'ASC')->get();

        $queryBphtb = Spt::select()
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->where('t_nik_pembeli', '=', $request->query('t_nik'))
            ->orWhere('t_npwpd', '=', $request->query('t_npwpd'))->get();

        $pdf = PDF::loadView('informasiop.exports.export', [
            'NOP' => $NOP,
            'request' => $request,
            'query' => $query,
            'sql_tunggakan' => $sql_tunggakan,
            'queryBphtb' => $queryBphtb
        ]);
        return $pdf->stream();
        // return $pdf->download('informasiop_'. date('d-m-Y H:i:s').'.pdf');

    }
}