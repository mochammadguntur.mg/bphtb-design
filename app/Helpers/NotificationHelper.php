<?php

class NotificationHelper {
    
    public function getPendaftaranNotifications() {
        $return = \App\Models\Notification\Notification::whereNull('read_at')->where('type', '=', 'App\Notifications\PendaftaranNotification')->count();
        return $return;
    }

}
