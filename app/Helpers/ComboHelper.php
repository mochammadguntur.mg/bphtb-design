<?php

use App\Models\Spt\Spt;

class ComboHelper {

    public function getDataNotaris() {
        $return = (new Spt())->cekdata_notaris();
        return $return;
    }

    public function getDataJenisTransaksi() {
        $return = (new Spt())->cekdata_jenis_transaksi();
        return $return;
    }
    
    public function getDataStatusBerkas() {
        $return = (new Spt())->cekdata_data_status_berkas();
        return $return;
    }
    
    public function getDataStatusKabid() {
        $return = (new Spt())->cekdata_data_status_kabid();
        return $return;
    }
    
    public function getDataStatusBayar() {
        $return = (new Spt())->cekdata_data_status_bayar();
        return $return;
    }
    
    public function cekmenukaban() {
        $return = (new Spt())->cek_setmenukaban();
        return $return;
    }
    
    public function cek_setmenutunggakanpbb() {
        $return = (new Spt())->cek_setmenutunggakanpbb();
        return $return;
    }
    
    public function cek_setmenubriva() {
        $return = (new Spt())->cek_setmenubriva();
        return $return;
    }
    
    public function cek_setkonekpbb() {
        $return = (new Spt())->cek_setkonekpbb();
        return $return;
    }
    
    public function cek_ukuranfileupload() {
        $return = (new Spt())->cek_ukuranfileupload();
        return $return;
    }
    
    
    public function cekdata_jenis_ketetapan() {
        $return = (new Spt())->cekdata_jenis_ketetapan();
        return $return;
    }
    
    public function cek_setgmap() {
        $return = (new Spt())->cek_setgmap();
        return $return;
    }
    
    public function cek_setcaptcha() {
        $return = (new Spt())->cek_setcaptcha();
        return $return;
    }
    
    public function cek_setpelayanan($id) {
        $return = (new Spt())->cek_setpelayanan($id);
        return $return;
    }
}
