<?php
function terbilang($number) {
    $x = abs($number);
    $angka = array(
        "", "Satu", "Dua", "Tiga", "Empat", "Lima",
        "Enam", "Tujuh", "Delapan", "Sembilan", "Sepuluh", "Sebelas"
    );
    $temp = "";
    if ($x < 12) {
        $temp = " " . $angka[$x];
    } else if ($x < 20) {
        $temp = terbilang($x - 10) . " Belas";
    } else if ($x < 100) {
        $temp = terbilang($x / 10) . " Puluh" . terbilang($x % 10);
    } else if ($x < 200) {
        $temp = " Seratus" . terbilang($x - 100);
    } else if ($x < 1000) {
        $temp = terbilang($x / 100) . " Ratus" . terbilang($x % 100);
    } else if ($x < 2000) {
        $temp = " Seribu" . terbilang($x - 1000);
    } else if ($x < 1000000) {
        $temp = terbilang($x / 1000) . " Ribu" . terbilang($x % 1000);
    } else if ($x < 1000000000) {
        $temp = terbilang($x / 1000000) . " Juta" . terbilang($x % 1000000);
    } else if ($x < 1000000000000) {
        $temp = terbilang($x / 1000000000) . " Milyar" . terbilang(fmod($x, 1000000000));
    } else if ($x < 1000000000000000) {
        $temp = terbilang($x / 1000000000000) . " Trilyun" . terbilang(fmod($x, 1000000000000));
    }
    return $temp;
}

function hitungDenda($jmlhpajak, $tgljatuhtempo) {
    $jatuhtempo = date('Y-m-d', strtotime($tgljatuhtempo));
    $tglsekarang = date('Y-m-d');
    $ts1 = strtotime($jatuhtempo);
    $ts2 = strtotime($tglsekarang);

    $year1 = date('Y', $ts1);
    $year2 = date('Y', $ts2);

    $month1 = date('m', $ts1);
    $month2 = date('m', $ts2);

    $day1 = date('d', $ts1);
    $day2 = date('d', $ts2);
    if ($day1 < $day2) {
        $tambahanbulan = 1;
    } else {
        $tambahanbulan = 0;
    }

    $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
    if ($jmlbulan > 24) {
        $jmlbulan = 24;
        $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
    } elseif ($jmlbulan <= 0) {
        $jmlbulan = 0;
        $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
    } else {
        $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
    }

    $data = [
        'jmlbulan' => $jmlbulan,
        'jmldenda' => round($jmldenda)
    ];

    return $data;
}
