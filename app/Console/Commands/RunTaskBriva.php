<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Models\Logs\logPayment;
use App\Models\setting\Briva;
use App\Models\Spt\Spt;
use App\Models\Pembayaran\PembayaranBphtb;

class RunTaskBriva extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'runtask';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update Data Payment to Database BPHTB';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() {

        $briva = Briva::orderBy('updated_at', 'desc')->first();
        $date = date_create(date('d-m-Y H:i:s'));
        date_add($date, date_interval_create_from_date_string('-1 hours'));

        $waktuAwal = date_format($date, 'H:i');
        $waktuAkhir = date('H:i');

        $client_id = $briva->client_id;
        $secret_id = $briva->secret_id;
        $timestamp = gmdate("Y-m-d\TH:i:s.000\Z");
        $secret = $secret_id;
        $token = $this->BRIVAgenerateToken($client_id, $secret_id);

        $institutionCode = $briva->institution_code;
        $brivaNo = $briva->briva_no;

        $startDate = date('Y-m-d');
        $endDate = date('Y-m-d');
        $startTime = $waktuAwal; //"10:00";
        $endTime = $waktuAkhir; //"12:30";

        $payload = null;
        $path = "/v1/briva/report_time/" . $institutionCode . "/" . $brivaNo . "/" . $startDate . "/" . $startTime . "/" . $endDate . "/" . $endTime;
        $verb = "GET";
        $base64sign = $this->BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret);

        $request_headers = array(
            "Authorization:Bearer " . $token,
            "BRI-Timestamp:" . $timestamp,
            "BRI-Signature:" . $base64sign,
        );

        if ($briva->url_aktif == 1) {
            $urlPost = $briva->url_development . "/v1/briva/report_time/" . $institutionCode . "/" . $brivaNo . "/" . $startDate . "/" . $startTime . "/" . $endDate . "/" . $endTime;
        } else {
            $urlPost = $briva->url_production . "/v1/briva/report_time/" . $institutionCode . "/" . $brivaNo . "/" . $startDate . "/" . $startTime . "/" . $endDate . "/" . $endTime;
        }

        $chPost = curl_init();
        curl_setopt($chPost, CURLOPT_URL, $urlPost);
        curl_setopt($chPost, CURLOPT_HTTPHEADER, $request_headers);
        curl_setopt($chPost, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($chPost, CURLOPT_POSTFIELDS, $payload);
        curl_setopt($chPost, CURLINFO_HEADER_OUT, true);
        curl_setopt($chPost, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($chPost, CURLOPT_SSL_VERIFYPEER, false);

        $resultPost = curl_exec($chPost);
        $httpCodePost = curl_getinfo($chPost, CURLINFO_HTTP_CODE);
        curl_close($chPost);

        $decode = json_decode($resultPost, true);

        if ($decode['status'] == true) {
            foreach ($decode['data'] as $row) {
                $spt = Spt::select()->where([
                            ['t_kodebayar_bphtb', '=', $row['brivaNo'] . $row['custCode']],
                        ])->get();

                if ($spt->count() > 0) {

                    PembayaranBphtb::insert([
                        't_idspt' => $spt[0]->t_idspt,
                        's_id_status_bayar' => 1,
                        't_iduser_bayar' => 12, //BRI
                        't_tglpembayaran_pokok' => \Carbon\Carbon::now(),
                        't_jmlhbayar_pokok' => $row['amount'],
                        't_jmlhbayar_total' => $row['amount'],
                        'created_at' => date('Y-m-d H:i:s', strtotime($row['paymentDate'])),
                        'updated_at' => date('Y-m-d H:i:s', strtotime($row['paymentDate'])),
                    ]);

                    $logs = new logPayment();
                    $logs->briva_no = $row['brivaNo'];
                    $logs->cust_code = $row['custCode'];
                    $logs->nama = $row['nama'];
                    $logs->amount = $row['amount'];
                    $logs->keterangan = 'Update Payment BRIVA { 00 - Success }';
                    $logs->save();
                }
            }
        }
    }

    function BRIVAgenerateToken($client_id, $secret_id) {
        $briva = Briva::orderBy('updated_at', 'desc')->first();
        if ($briva->url_aktif == 1) {
            $url = $briva->url_development . "/oauth/client_credential/accesstoken?grant_type=client_credentials";
        } else {
            $url = $briva->url_production . "/oauth/client_credential/accesstoken?grant_type=client_credentials";
        }
        $data = "client_id=" . $client_id . "&client_secret=" . $secret_id;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

        $result = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        $json = json_decode($result, true);
        $accesstoken = $json['access_token'];

        return $accesstoken;
    }

    /* Generate signature */

    function BRIVAgenerateSignature($path, $verb, $token, $timestamp, $payload, $secret) {
        $payloads = "path=$path&verb=$verb&token=Bearer $token&timestamp=$timestamp&body=$payload";
        $signPayload = hash_hmac('sha256', $payloads, $secret, true);
        return base64_encode($signPayload);
    }

    function hitungDenda($tglSekarang, $tgltempo, $jmlhpajak) {
        $tgljatuhtempo = date('Y-m-d', strtotime($tgltempo));
        $tglsekarang = date('Y-m-d', strtotime($tglSekarang));
        $ts1 = strtotime($tgljatuhtempo);
        $ts2 = strtotime($tglsekarang);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $day1 = date('d', $ts1);
        $day2 = date('d', $ts2);
        if ($day1 < $day2) {
            $tambahanbulan = 1;
        } else {
            $tambahanbulan = 0;
        }

        $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
        if ($jmlbulan > 24) {
            $jmlbulan = 24;
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        } elseif ($jmlbulan <= 0) {
            $jmlbulan = 0;
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        } else {
            $jmldenda = $jmlbulan * 2 / 100 * $jmlhpajak;
        }

        return round($jmldenda);
    }

}
