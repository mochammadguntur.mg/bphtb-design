<?php

namespace App\Models\Pelayanan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PelayananPenundaan extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_pelayanan_penundaan';
    protected $primaryKey = 't_idpenundaan';
    protected $fillable = ['t_idspt', 't_nopenundaan', 't_tglpengajuan', 't_keterangan_permohoanan', 't_iduser_pengajuan', 't_nosk_penundaan', 't_idoperator', 't_tglpersetujuan', 't_keterangan_disetujui'];
    protected static $logAttributes = ['t_idspt', 't_nopenundaan', 't_tglpengajuan', 't_keterangan_permohoanan', 't_iduser_pengajuan', 't_nosk_penundaan', 't_idoperator', 't_tglpersetujuan', 't_keterangan_disetujui'];
}
