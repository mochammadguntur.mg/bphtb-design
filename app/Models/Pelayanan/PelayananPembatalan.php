<?php

namespace App\Models\Pelayanan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PelayananPembatalan extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_pelayanan_pembatalan';
    protected $primaryKey = 't_idpembatalan';
    protected $fillable = ['t_idspt', 't_nopembatalan', 't_tglpengajuan', 't_keterangan_pembatalan', 't_iduser_pengajuan', 't_nosk_pembatalan', 't_idoperator', 't_tglpersetujuan', 't_keterangan_disetujui'];
    protected static $logAttributes = ['t_idspt', 't_nopembatalan', 't_tglpengajuan', 't_keterangan_pembatalan', 't_iduser_pengajuan', 't_nosk_pembatalan', 't_idoperator', 't_tglpersetujuan', 't_keterangan_disetujui'];
}
