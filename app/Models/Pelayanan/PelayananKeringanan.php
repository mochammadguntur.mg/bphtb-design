<?php

namespace App\Models\Pelayanan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PelayananKeringanan extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_pelayanan_keringanan';
    protected $primaryKey = 't_idkeringanan';
    protected $fillable = ['t_idspt', 't_nokeringanan', 't_tglpengajuan', 't_iduser_pengajuan', 't_jmlhpotongan', 't_keterangan_permohoanan', 't_nosk_keringanan', 't_idoperator', 't_tglpersetujuan', 't_jmlhpotongan_disetujui', 't_persentase_disetujui','t_jmlh_spt_sebenarnya', 't_jmlh_spt_hasilpot', 't_keterangan_disetujui'];
    protected static $logAttributes = ['t_idspt', 't_nokeringanan', 't_tglpengajuan', 't_iduser_pengajuan', 't_jmlhpotongan', 't_keterangan_permohoanan', 't_nosk_keringanan', 't_idoperator', 't_tglpersetujuan', 't_jmlhpotongan_disetujui', 't_persentase_disetujui','t_jmlh_spt_sebenarnya', 't_jmlh_spt_hasilpot', 't_keterangan_disetujui'];
}
