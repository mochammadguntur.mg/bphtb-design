<?php

namespace App\Models\Pelayanan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PelayananAngsuran extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_pelayanan_angsuran';
    protected $primaryKey = 't_idangsuran';
    protected $fillable = ['t_idspt', 't_noangsuran', 't_tglpengajuan', 't_keterangan_permohoanan', 't_iduser_pengajuan', 't_nosk_angsuran', 't_idoperator', 't_tglpersetujuan', 't_keterangan_disetujui'];
    protected static $logAttributes = ['t_idspt', 't_noangsuran', 't_tglpengajuan', 't_keterangan_permohoanan', 't_iduser_pengajuan', 't_nosk_angsuran', 't_idoperator', 't_tglpersetujuan', 't_keterangan_disetujui'];
}
