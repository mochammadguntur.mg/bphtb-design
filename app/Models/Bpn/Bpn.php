<?php

namespace App\Models\Bpn;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Bpn extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_bpn';
    protected $primaryKey = 't_idbpn';
    protected $fillable = ['t_noakta', 't_tglakta', 't_namappat', 't_nop', 't_nib', 't_ntpd', 't_nik', 't_npwp', 't_namawp', 't_kelurahanop', 't_kecamatanop', 't_kotaop','t_luastanahop', 't_jenishak', 't_koordinat_x', 't_koordinat_y', 't_tgltransaksi'];
    protected static $logAttributes = ['t_noakta', 't_tglakta', 't_namappat', 't_nop', 't_nib', 't_ntpd', 't_nik', 't_npwp', 't_namawp', 't_kelurahanop', 't_kecamatanop', 't_kotaop','t_luastanahop', 't_jenishak', 't_koordinat_x', 't_koordinat_y', 't_tgltransaksi'];
}
