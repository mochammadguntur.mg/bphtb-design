<?php

namespace App\Models\Bpn;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class BpnApi extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_users_api';
    protected $primaryKey = 's_idusers_api';
    protected $fillable = ['s_username', 's_password'];
    protected static $logAttributes = ['s_username', 's_password'];
}
