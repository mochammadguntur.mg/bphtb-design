<?php

namespace App\Models\ValidasiBerkas;

use App\Models\Spt\Spt;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class ValidasiBerkas extends Model {

    use HasFactory,
        LogsActivity;

    protected $table = 't_validasi_berkas';
    protected $primaryKey = 't_id_validasi_berkas';
    protected $fillable = ['t_idspt', 's_id_status_berkas', 't_tglvalidasi', 't_iduser_validasi', 't_keterangan_berkas', 't_persyaratan_validasi_berkas'];
    protected static $logAttributes = ['t_idspt', 's_id_status_berkas', 't_tglvalidasi', 't_iduser_validasi', 't_keterangan_berkas', 't_persyaratan_validasi_berkas'];

    public function dataGridValidasiBerkasSudah() {
        $response = (new Spt())::select('t_spt.*',
                        's_jenistransaksi.s_namajenistransaksi',
                        's_notaris.s_namanotaris',
                        's_jenis_bidangusaha.s_nama_bidangusaha',
                        't_validasi_berkas.t_id_validasi_berkas', 
                        't_validasi_berkas.s_id_status_berkas',
                        't_validasi_berkas.t_tglvalidasi',
                        't_validasi_kabid.t_id_validasi_kabid', 
                        't_validasi_kabid.s_id_status_kabid',
                        't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                        't_pembayaran_bphtb.t_id_pembayaran', 
                        't_pembayaran_bphtb.t_tglpembayaran_pokok',
                        's_jenisketetapan.s_namajenisketetapan', 
                        's_jenisketetapan.s_namasingkatjenisketetapan',
                        's_jenisfasilitas.s_namajenisfasilitas',
                        's_jenishaktanah.s_namahaktanah',
                        's_jenisdoktanah.s_namadoktanah',
                        's_jenispengurangan.s_namapengurangan',
                        's_jenistanah.nama_jenistanah'
                )
                ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
                ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
                ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
                ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
                ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
                ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
                ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
                ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
                ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
                ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
                ->orderBy('t_spt.t_idspt', 'desc');
        return $response;
    }

    public function dataGridBelum() {
        $response = (new Spt())::select(
                        't_spt.*',
                        's_jenistransaksi.s_namajenistransaksi',
                        's_notaris.s_namanotaris',
                        's_jenis_bidangusaha.s_nama_bidangusaha',
                        't_validasi_berkas.t_id_validasi_berkas', 
                        't_validasi_berkas.s_id_status_berkas',
                        't_validasi_berkas.t_tglvalidasi',
                        't_validasi_kabid.t_id_validasi_kabid', 
                        't_validasi_kabid.s_id_status_kabid',
                        't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                        't_pembayaran_bphtb.t_id_pembayaran', 
                        't_pembayaran_bphtb.t_tglpembayaran_pokok',
                        's_jenisketetapan.s_namajenisketetapan', 
                        's_jenisketetapan.s_namasingkatjenisketetapan',
                        's_jenisfasilitas.s_namajenisfasilitas',
                        's_jenishaktanah.s_namahaktanah',
                        's_jenisdoktanah.s_namadoktanah',
                        's_jenispengurangan.s_namapengurangan',
                        's_jenistanah.nama_jenistanah')
                ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
                ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
                ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
                ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
                ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
                ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
                ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
                ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
                ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
                ->whereNull('t_validasi_berkas.t_id_validasi_berkas')
                ->where('t_spt.t_idpersetujuan_bphtb', '=', '1')
                ->orderBy('t_spt.t_idspt', 'desc');
        return $response;
    }

}
