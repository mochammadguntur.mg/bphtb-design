<?php

namespace App\Models\Pemeriksaan;

use App\Models\Spt\Spt;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Pemeriksaan extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_pemeriksaan';
    protected $primaryKey = 't_idpemeriksa';
    protected $fillable = [
        't_idpemeriksa',
        't_idspt',
        't_tglpenetapan',
        't_tglby_system',
        't_iduser_buat',
        't_idpejabat1',
        't_idpejabat2',
        't_idpejabat3',
        't_idpejabat4',
        't_noperiksa',
        't_keterangan',
        't_nop_sppt',
        't_tahun_sppt',
        't_luastanah_pemeriksa',
        't_njoptanah_pemeriksa',
        't_totalnjoptanah_pemeriksa',
        't_luasbangunan_pemeriksa',
        't_njopbangunan_pemeriksa',
        't_totalnjopbangunan_pemeriksa',
        't_grandtotalnjop_pemeriksa',
        't_nilaitransaksispt_pemeriksa',
        't_trf_aphb_kali_pemeriksa',
        't_trf_aphb_bagi_pemeriksa',
        't_permeter_tanah_pemeriksa',
        't_persenbphtb_pemeriksa',
        't_npop_bphtb_pemeriksa',
        't_npoptkp_bphtb_pemeriksa',
        't_npopkp_bphtb_pemeriksa',
        't_nilaibphtb_pemeriksa',
        't_idtarifbphtb_pemeriksa',
        't_idpersetujuan_pemeriksa',
        'isdeleted',
        't_idoperator_deleted'
    ];

    protected static $logAttributes = [
        't_idpemeriksa',
        't_idspt',
        't_tglpenetapan',
        't_tglby_system',
        't_iduser_buat',
        't_idpejabat1',
        't_idpejabat2',
        't_idpejabat3',
        't_idpejabat4',
        't_noperiksa',
        't_keterangan',
        't_nop_sppt',
        't_tahun_sppt',
        't_luastanah_pemeriksa',
        't_njoptanah_pemeriksa',
        't_totalnjoptanah_pemeriksa',
        't_luasbangunan_pemeriksa',
        't_njopbangunan_pemeriksa',
        't_totalnjopbangunan_pemeriksa',
        't_grandtotalnjop_pemeriksa',
        't_nilaitransaksispt_pemeriksa',
        't_trf_aphb_kali_pemeriksa',
        't_trf_aphb_bagi_pemeriksa',
        't_permeter_tanah_pemeriksa',
        't_persenbphtb_pemeriksa',
        't_npop_bphtb_pemeriksa',
        't_npoptkp_bphtb_pemeriksa',
        't_npopkp_bphtb_pemeriksa',
        't_nilaibphtb_pemeriksa',
        't_idtarifbphtb_pemeriksa',
        't_idpersetujuan_pemeriksa',
        'isdeleted',
        't_idoperator_deleted'
    ];

    public function datagridPemeriksaanBelum($request)
    {
        $response = (new Spt())::select(
            't_spt.*',
            's_jenistransaksi.s_namajenistransaksi',
            's_notaris.s_namanotaris',
            's_jenis_bidangusaha.s_nama_bidangusaha',
            't_validasi_berkas.t_id_validasi_berkas',
            't_validasi_berkas.s_id_status_berkas',
            't_validasi_berkas.t_tglvalidasi',
            't_validasi_kabid.t_id_validasi_kabid',
            't_validasi_kabid.s_id_status_kabid',
            't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
            't_pembayaran_bphtb.t_id_pembayaran',
            't_pembayaran_bphtb.t_tglpembayaran_pokok',
            't_pemeriksaan.t_idpemeriksa',
            't_pemeriksaan.t_tglpenetapan as t_tglpemeriksaan',
            's_jenisketetapan.s_namajenisketetapan',
            's_jenisketetapan.s_namasingkatjenisketetapan',
            's_jenisfasilitas.s_namajenisfasilitas',
            's_jenishaktanah.s_namahaktanah',
            's_jenisdoktanah.s_namadoktanah',
            's_jenispengurangan.s_namapengurangan',
            's_jenistanah.nama_jenistanah'
        )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
            ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
            ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
            ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
            ->whereNull('t_pembayaran_bphtb.t_idspt')
            ->whereNull('t_pemeriksaan.t_idspt')
            ->where('t_validasi_kabid.s_id_status_kabid', '=', 2)
            ->orderBy('t_spt.t_idspt', 'desc');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_tgldaftar_spt');
        }

        if ($request['filter']['nodaftar'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['nodaftar'] . "%");
        }

        if ($request['filter']['tahun'] != null) {
            $response = $response->where('t_periodespt', 'like', "%" . $request['filter']['tahun'] . "%");
        }

        if ($request['filter']['notaris'] != null) {
            $response = $response->where('t_idnotaris_spt', '=', $request['filter']['notaris']);
        }

        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }

        if ($request['filter']['namawp'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['namawp'] . "%");
        }

        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }

        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }

        if ($request['filter']['status_persetujuan'] != null) {
            $response = (($request['filter']['status_persetujuan'] == 1)
                ? $response->where('t_idpersetujuan_bphtb', '=', $request['filter']['status_persetujuan'])
                : $response->whereNull('t_idpersetujuan_bphtb'));
        }

        if ($request['filter']['status_validasi_berkas'] != null) {
            if ($request['filter']['status_validasi_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['status_validasi_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['status_validasi_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }

        if ($request['filter']['status_validasi_kabid'] != null) {
            if ($request['filter']['status_validasi_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['status_validasi_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['status_validasi_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }
        if ($request['filter']['status_pembayaran'] != null) {
            if ($request['filter']['status_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['status_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }

        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }

    public function datagridPemeriksaanSudah($request)
    {
        $response = (new Spt())::select(
            't_spt.*',
            's_jenistransaksi.s_namajenistransaksi',
            's_notaris.s_namanotaris',
            't_pemeriksaan.t_idpemeriksa',
            's_jenis_bidangusaha.s_nama_bidangusaha',
            't_validasi_berkas.t_id_validasi_berkas',
            't_validasi_berkas.s_id_status_berkas',
            't_validasi_berkas.t_tglvalidasi',
            't_validasi_kabid.t_id_validasi_kabid',
            't_validasi_kabid.s_id_status_kabid',
            't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
            't_pembayaran_bphtb.t_id_pembayaran',
            't_pembayaran_bphtb.t_tglpembayaran_pokok',
            't_pemeriksaan.t_idpemeriksa',
            't_pemeriksaan.t_tglpenetapan as t_tglpemeriksaan',
            's_jenisketetapan.s_namajenisketetapan',
            's_jenisketetapan.s_namasingkatjenisketetapan',
            's_jenisfasilitas.s_namajenisfasilitas',
            's_jenishaktanah.s_namahaktanah',
            's_jenisdoktanah.s_namadoktanah',
            's_jenispengurangan.s_namapengurangan',
            's_jenistanah.nama_jenistanah'
        )
            ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
            ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
            ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
            ->whereNotNull('t_pemeriksaan.t_idpemeriksa')
            ->orderBy('t_spt.t_idspt', 'desc');

        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_tgldaftar_spt');
        }

        if ($request['filter']['nodaftar'] != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $request['filter']['nodaftar'] . "%");
        }

        if ($request['filter']['tahun'] != null) {
            $response = $response->where('t_periodespt', 'like', "%" . $request['filter']['tahun'] . "%");
        }

        if ($request['filter']['notaris'] != null) {
            $response = $response->where('t_idnotaris_spt', '=', $request['filter']['notaris']);
        }

        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }

        if ($request['filter']['namawp'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['namawp'] . "%");
        }

        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $request['filter']['nop'] . "%");
        }

        if ($request['filter']['jenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['jenistransaksi']);
        }

        if ($request['filter']['status_persetujuan'] != null) {
            $response = (($request['filter']['status_persetujuan'] == 1)
                ? $response->where('t_idpersetujuan_bphtb', '=', $request['filter']['status_persetujuan'])
                : $response->whereNull('t_idpersetujuan_bphtb'));
        }

        if ($request['filter']['status_validasi_berkas'] != null) {
            if ($request['filter']['status_validasi_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['status_validasi_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['status_validasi_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }

        if ($request['filter']['status_validasi_kabid'] != null) {
            if ($request['filter']['status_validasi_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['status_validasi_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['status_validasi_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }
        if ($request['filter']['status_pembayaran'] != null) {
            if ($request['filter']['status_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['status_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        if ($request['filter']['kodebayar'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['kodebayar'] . "%");
        }
        if ($request['filter']['nop'] != null) {
            $response = $response->where('t_kodebayar_bphtb', 'like', "%" . $request['filter']['nop'] . "%");
        }
        if ($request['filter']['tgldaftar'] != null) {
            $date = explode(' - ', $request['filter']['tgldaftar']);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response = $response->whereBetween('t_tgldaftar_spt', [$startDate, $endDate]);
        }

        return $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
    }
}
