<?php

namespace App\Models\Skpdkb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Skpdkb extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_skpdkb';
    protected $primaryKey = 't_idskpdkb';
    protected $fillable = ['t_idspt', 't_tglpenetapan', 't_tglby_system', 't_iduser_buat', 't_nop_sppt', 't_tahun_sppt', 't_luastanah_skpdkb', 't_njoptanah_skpdkb', 't_totalnjoptanah_skpdkb', 't_luasbangunan_skpdkb', 't_njopbangunan_skpdkb', 't_totalnjopbangunan_skpdkb', 't_grandtotalnjop_skpdkb', 't_nilaitransaksispt_skpdkb', 't_trf_aphb_kali_skpdkb', 't_trf_aphb_bagi_skpdkb', 't_permeter_tanah_skpdkb', 't_idtarifbphtb_skpdkb', 't_persenbphtb_skpdkb', 't_npop_bphtb_skpdkb', 't_npoptkp_bphtb_skpdkb', 't_npopkp_bphtb_skpdkb', 't_nilai_bayar_sblumnya', 't_nilai_pokok_skpdkb', 't_jmlh_blndenda', 't_jmlh_dendaskpdkb', 't_jmlh_totalskpdkb', 't_tglbuat_kodebyar', 't_nourut_kodebayar', 't_kodebayar_skpdkb', 't_tgljatuhtempo_skpdkb', 't_idjenisketetapan', 't_idpersetujuan_skpdkb', 'isdeleted', 't_idoperator_deleted'];
    protected static $logAttributes = ['t_idspt', 't_tglpenetapan', 't_tglby_system', 't_iduser_buat', 't_nop_sppt', 't_tahun_sppt', 't_luastanah_skpdkb', 't_njoptanah_skpdkb', 't_totalnjoptanah_skpdkb', 't_luasbangunan_skpdkb', 't_njopbangunan_skpdkb', 't_totalnjopbangunan_skpdkb', 't_grandtotalnjop_skpdkb', 't_nilaitransaksispt_skpdkb', 't_trf_aphb_kali_skpdkb', 't_trf_aphb_bagi_skpdkb', 't_permeter_tanah_skpdkb', 't_idtarifbphtb_skpdkb', 't_persenbphtb_skpdkb', 't_npop_bphtb_skpdkb', 't_npoptkp_bphtb_skpdkb', 't_npopkp_bphtb_skpdkb', 't_nilai_bayar_sblumnya', 't_nilai_pokok_skpdkb', 't_jmlh_blndenda', 't_jmlh_dendaskpdkb', 't_jmlh_totalskpdkb', 't_tglbuat_kodebyar', 't_nourut_kodebayar', 't_kodebayar_skpdkb', 't_tgljatuhtempo_skpdkb', 't_idjenisketetapan', 't_idpersetujuan_skpdkb', 'isdeleted', 't_idoperator_deleted'];
}
