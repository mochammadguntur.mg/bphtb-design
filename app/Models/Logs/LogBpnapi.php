<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class LogBpnapi extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 'apibpn_log';
    protected $primaryKey = 'log_id';
    protected $fillable = ['timestamp', 'message'];
    protected static $logAttributes = ['timestamp', 'message'];
    
    
    
}
