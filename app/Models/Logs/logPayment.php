<?php

namespace App\Models\Logs;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class logPayment extends Model
{
    use HasFactory, LogsActivity;
    
    protected $table = 'log_payment';
    protected $primaryKey = 'log_id';
    protected $fillable = [
        'briva_no',
        'cust_code',
        'nama',
        'amount',
        'keterangan'
    ];

    protected static $logAttributes = [
        'briva_no',
        'cust_code',
        'nama',
        'amount',
        'keterangan'
    ];
}
