<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RefKecamatan extends Model
{
    // protected $connection = 'oracle';
    // protected $table = 'REF_KECAMATAN';

    protected $connection = 'pgsql2';
    protected $table = 'ref_kecamatan';
}
