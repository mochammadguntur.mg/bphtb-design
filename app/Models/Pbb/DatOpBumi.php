<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatOpBumi extends Model
{
    use HasFactory;
    // protected $connection = 'oracle';
    // protected $table = 'DAT_OP_BUMI';

    protected $connection = 'pgsql2';
    protected $table = 'dat_op_bumi';
    protected $fillable = [
        'KD_PROPINSI', 
        'KD_DATI2', 
        'KD_KECAMATAN', 
        'KD_KELURAHAN', 
        'KD_BLOK', 
        'NO_URUT', 
        'KD_JNS_OP',
        'NO_BUMI', 
        'KD_ZNT', 
        'LUAS_BUMI', 
        'JNS_BUMI', 
        'NILAI_SISTEM_BUMI'
    ];
}
