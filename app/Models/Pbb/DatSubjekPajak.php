<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatSubjekPajak extends Model
{
    use HasFactory;
    // protected $connection = 'oracle';
    // protected $table = 'DAT_SUBJEK_PAJAK';

    protected $connection = 'pgsql2';
    protected $table = 'dat_subjek_pajak';
    protected $fillable = [
        'SUBJEK_PAJAK_ID', 
        'NM_WP', 
        'JALAN_WP', 
        'BLOK_KAV_NO_WP', 
        'RT_WP', 
        'RW_WP',
        'KELURAHAN_WP', 
        'KOTA_WP', 
        'KD_POS_WP', 
        'TELP_WP', 
        'NPWP', 
        'STATUS_PEKERJAAN_WP'
    ];
}
