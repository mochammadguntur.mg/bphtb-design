<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatObjekPajak extends Model
{
    use HasFactory;
    // protected $connection = 'oracle';
    // protected $table = 'DAT_OBJEK_PAJAK';

    protected $connection = 'pgsql2';
    protected $table = 'dat_objek_pajak';
    protected $fillable = [
        'KD_PROPINSI', 
        'KD_DATI2', 
        'KD_KECAMATAN', 
        'KD_KELURAHAN', 
        'KD_BLOK', 
        'NO_URUT', 
        'KD_JNS_OP',
        'SUBJEK_PAJAK_ID', 
        'NO_FORMULIR_SPOP', 
        'NO_PERSIL', 
        'JALAN_OP', 
        'BLOK_KAV_NO_OP', 
        'RW_OP', 
        'RT_OP',
        'KD_STATUS_CABANG', 
        'KD_STATUS_WP', 
        'TOTAL_LUAS_BUMI', 
        'TOTAL_LUAS_BNG', 
        'NJOP_BUMI', 
        'NJOP_BNG',
        'STATUS_PETA_OP', 
        'JNS_TRANSAKSI_OP'
    ];
}
