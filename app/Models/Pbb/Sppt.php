<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sppt extends Model
{
    // protected $connection = 'oracle';
    // protected $table = 'SPPT';

    protected $connection = 'pgsql2';
    protected $table = 'sppt';

    public function getDataNOP($t_nop_sppt, $t_tahun_sppt)
    {
        $nop = explode('.', $t_nop_sppt);
        $KD_PROPINSI = $nop[0];
        $KD_DATI2 = $nop[1];
        $KD_KECAMATAN = $nop[2];
        $KD_KELURAHAN = $nop[3];
        $KD_BLOK = $nop[4];
        $NO_URUT = $nop[5];
        $KD_JNS_OP = $nop[6];
        $TAHUNPAJAK = $t_tahun_sppt;
        // $cek_data = Sppt::select(
        //     'SPPT.*',
        //     'REF_DATI2.NM_DATI2',
        //     'REF_KECAMATAN.NM_KECAMATAN',
        //     'REF_KELURAHAN.NM_KELURAHAN',
        //     'KELAS_TANAH.NILAI_PER_M2_TANAH',
        //     'KELAS_BANGUNAN.NILAI_PER_M2_BNG'
        // )
        //     ->leftJoin('REF_DATI2', [
        //         ['REF_DATI2.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
        //         ['REF_DATI2.KD_DATI2', '=', 'SPPT.KD_DATI2']
        //     ])
        //     ->leftJoin('REF_KECAMATAN', [
        //         ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
        //         ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
        //         ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
        //     ])
        //     ->leftJoin('REF_KELURAHAN', [
        //         ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
        //         ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
        //         ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
        //         ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
        //     ])
        //     ->leftJoin('KELAS_TANAH', [
        //         ['KELAS_TANAH.KD_KLS_TANAH', '=', 'SPPT.KD_KLS_TANAH']
        //     ])
        //     ->leftJoin('KELAS_BANGUNAN', [
        //         ['KELAS_BANGUNAN.KD_KLS_BNG', '=', 'SPPT.KD_KLS_BNG']
        //     ])
        //     ->where([
        //         ['SPPT.KD_PROPINSI', '=', $KD_PROPINSI],
        //         ['SPPT.KD_DATI2', '=', $KD_DATI2],
        //         ['SPPT.KD_KECAMATAN', '=', $KD_KECAMATAN],
        //         ['SPPT.KD_KELURAHAN', '=', $KD_KELURAHAN],
        //         ['SPPT.KD_BLOK', '=', $KD_BLOK],
        //         ['SPPT.NO_URUT', '=', $NO_URUT],
        //         ['SPPT.KD_JNS_OP', '=', $KD_JNS_OP],
        //         ['SPPT.THN_PAJAK_SPPT', '=', $TAHUNPAJAK],
        //     ])->first();
        // $cek_data->dd();

        $cek_data = Sppt::select(
            'sppt.*',
            'ref_dati2.nm_dati2',
            'ref_kecamatan.nm_kecamatan',
            'ref_kelurahan.nm_kelurahan',
            'kelas_tanah.nilai_per_m2_tanah',
            'kelas_bangunan.nilai_per_m2_bng'
        )
            ->leftJoin('ref_dati2', [
                ['ref_dati2.kd_propinsi', '=', 'sppt.kd_propinsi'],
                ['ref_dati2.kd_dati2', '=', 'sppt.kd_dati2']
            ])
            ->leftJoin('ref_kecamatan', [
                ['ref_kecamatan.kd_propinsi', '=', 'sppt.kd_propinsi'],
                ['ref_kecamatan.kd_dati2', '=', 'sppt.kd_dati2'],
                ['ref_kecamatan.kd_kecamatan', '=', 'sppt.kd_kecamatan']
            ])
            ->leftJoin('ref_kelurahan', [
                ['ref_kelurahan.kd_propinsi', '=', 'sppt.kd_propinsi'],
                ['ref_kelurahan.kd_dati2', '=', 'sppt.kd_dati2'],
                ['ref_kelurahan.kd_kecamatan', '=', 'sppt.kd_kecamatan'],
                ['ref_kelurahan.kd_kelurahan', '=', 'sppt.kd_kelurahan']
            ])
            ->leftJoin('kelas_tanah', [
                ['kelas_tanah.kd_kls_tanah', '=', 'sppt.kd_kls_tanah']
            ])
            ->leftJoin('kelas_bangunan', [
                ['kelas_bangunan.kd_kls_bng', '=', 'sppt.kd_kls_bng']
            ])
            ->where([
                ['sppt.kd_propinsi', '=', $KD_PROPINSI],
                ['sppt.kd_dati2', '=', $KD_DATI2],
                ['sppt.kd_kecamatan', '=', $KD_KECAMATAN],
                ['sppt.kd_kelurahan', '=', $KD_KELURAHAN],
                ['sppt.kd_blok', '=', $KD_BLOK],
                ['sppt.no_urut', '=', $NO_URUT],
                ['sppt.kd_jns_op', '=', $KD_JNS_OP],
                ['sppt.thn_pajak_sppt', '=', $TAHUNPAJAK],
            ])->first();
        return $cek_data;
    }
}
