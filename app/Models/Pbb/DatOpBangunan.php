<?php

namespace App\Models\Pbb;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DatOpBangunan extends Model
{
    use HasFactory;
    // protected $connection = 'oracle';
    // protected $table = 'DAT_OP_BANGUNAN';

    protected $connection = 'pgsql2';
    protected $table = 'dat_op_bangunan';
    protected $fillable = [
        'KD_PROPINSI', 
        'KD_DATI2', 
        'KD_KECAMATAN', 
        'KD_KELURAHAN', 
        'KD_BLOK', 
        'NO_URUT', 
        'KD_JNS_OP',
        'NO_BNG', 
        'KD_JPB', 
        'NO_FORMULIR_LSPOP', 
        'THN_DIBANGUN_BNG', 
        'THN_RENOVASI_BNG', 
        'LUAS_BNG',
        'JML_LANTAI_BNG', 
        'KONDISI_BNG', 
        'JNS_KONSTRUKSI_BNG', 
        'JNS_ATAP_BNG', 
        'KD_DINDING', 
        'KD_LANTAI',
        'KD_LANGIT_LANGIT', 
        'NILAI_SISTEM_BNG', 
        'JNS_TRANSAKSI_BNG', 
        'TGL_PENDATAAN_BNG', 
        'NIP_PENDATA_BNG',
        'TGL_PEMERIKSAAN_BNG', 
        'NIP_PEMERIKSA_BNG', 
        'TGL_PEREKAMAN_BNG', 
        'NIP_PEREKAM_BNG'
    ];
}
