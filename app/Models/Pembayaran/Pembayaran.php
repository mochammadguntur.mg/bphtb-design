<?php

namespace App\Models\Pembayaran;

use App\Models\Spt\Spt;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Spatie\Activitylog\Traits\LogsActivity;

class Pembayaran extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_validasi_kabid';
    protected $primaryKey = 't_id_validasi_kabid';
    protected $fillable = ['t_idspt', 's_id_status_kabid', 't_tglvalidasi', 't_iduser_validasi', 't_keterangan_kabid'];
    protected static $logAttributes = ['t_idspt', 's_id_status_kabid', 't_tglvalidasi', 't_iduser_validasi', 't_keterangan_kabid'];

    public function datagridSudahBayar()
    {
        $response = new Spt();
        $response = $response::select(
            't_spt.*',
            's_jenistransaksi.*',
            's_notaris.s_namanotaris',
            's_jenis_bidangusaha.s_nama_bidangusaha',
            't_validasi_berkas.t_id_validasi_berkas',
            't_validasi_berkas.s_id_status_berkas',
            't_validasi_berkas.t_tglvalidasi',
            't_validasi_berkas.t_keterangan_berkas',
            't_validasi_kabid.t_id_validasi_kabid',
            't_validasi_kabid.s_id_status_kabid',
            't_validasi_kabid.t_keterangan_kabid',
            't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
            't_validasi_kaban.t_id_validasi_kaban',
            't_validasi_kaban.s_id_status_kaban',
            't_validasi_kaban.t_keterangan_kaban',
            't_validasi_kaban.t_tglvalidasikaban',
            't_pembayaran_bphtb.t_id_pembayaran',
            't_pembayaran_bphtb.t_nourut_bayar',
            't_pembayaran_bphtb.t_tglpembayaran_pokok',
            't_pembayaran_bphtb.t_jmlhbayar_pokok',
            't_pembayaran_bphtb.t_tglpembayaran_denda',
            't_pembayaran_bphtb.t_jmlhbayar_denda',
            't_pembayaran_bphtb.t_jmlhbayar_total',
            's_jenisketetapan.s_namajenisketetapan',
            's_jenisketetapan.s_namasingkatjenisketetapan',
            's_jenisfasilitas.s_namajenisfasilitas',
            's_jenishaktanah.s_namahaktanah',
            's_jenisdoktanah.s_namadoktanah',
            's_jenispengurangan.s_namapengurangan',
            's_jenistanah.nama_jenistanah',
            't_skpdkb.t_idskpdkb',
            't_skpdkb.t_jmlh_totalskpdkb',
            DB::raw('( CASE WHEN t_skpdkb.t_idskpdkb IS NOT NULL THEN t_skpdkb.t_idjenisketetapan ELSE t_spt.t_idjenisketetapan END ) AS jenis_ketetapan')
        )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_validasi_kaban', 't_spt.t_idspt', '=', 't_validasi_kaban.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
            ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
            ->leftJoin('t_skpdkb', 't_spt.t_idspt', '=', 't_skpdkb.t_idspt')
            ->leftJoin('t_pembayaran_skpdkb', 't_pembayaran_skpdkb.t_idskpdkb', '=', 't_skpdkb.t_idskpdkb')
            ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
            ->whereNotNull('t_validasi_kabid.t_id_validasi_kabid')
            // ->whereNotNull('t_pembayaran_bphtb.t_id_pembayaran')
            ->whereRaw('( CASE WHEN t_skpdkb.t_idskpdkb IS NOT NULL THEN t_pembayaran_skpdkb.t_tglpembayaran_skpdkb IS NOT NULL ELSE t_pembayaran_bphtb.t_tglpembayaran_pokok IS NOT NULL END )')
            ->orderBy('t_pembayaran_bphtb.t_tglpembayaran_pokok', 'desc');
        
        
        return $response;
    }

    public function datagridBelumBayar()
    {
        
        $cek_setmenukaban = $this->cek_setmenukaban();
        if($cek_setmenukaban->s_id_statusmenukaban == 1){   // kalau 1 aktif    
             $where_kaban = ' AND t_validasi_kaban.s_id_status_kaban = 1 and t_validasi_kaban.t_id_validasi_kaban is not null';
        }else{
            $where_kaban = '';
        }
        $response = new Spt();
        $response = $response->select(
            't_spt.*',
            's_jenistransaksi.*',
            's_notaris.s_namanotaris',
            's_jenis_bidangusaha.s_nama_bidangusaha',
            't_validasi_berkas.t_id_validasi_berkas',
            't_validasi_berkas.s_id_status_berkas',
            't_validasi_berkas.t_tglvalidasi',
            't_validasi_berkas.t_keterangan_berkas',
            't_validasi_kabid.t_id_validasi_kabid',
            't_validasi_kabid.s_id_status_kabid',
            't_validasi_kabid.t_keterangan_kabid',
            't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
            't_validasi_kaban.t_id_validasi_kaban',
            't_validasi_kaban.s_id_status_kaban',
            't_validasi_kaban.t_keterangan_kaban',
            't_validasi_kaban.t_tglvalidasikaban',
            't_pembayaran_bphtb.t_id_pembayaran',
            't_pembayaran_bphtb.t_nourut_bayar',
            't_pembayaran_bphtb.t_tglpembayaran_pokok',
            't_pembayaran_bphtb.t_jmlhbayar_pokok',
            't_pembayaran_bphtb.t_tglpembayaran_denda',
            't_pembayaran_bphtb.t_jmlhbayar_denda',
            't_pembayaran_bphtb.t_jmlhbayar_total',
            's_jenisfasilitas.s_namajenisfasilitas',
            's_jenishaktanah.s_namahaktanah',
            's_jenisdoktanah.s_namadoktanah',
            's_jenispengurangan.s_namapengurangan',
            's_jenistanah.nama_jenistanah',
            't_skpdkb.t_idskpdkb',
            't_skpdkb.t_jmlh_totalskpdkb',
            't_skpdkb.t_kodebayar_skpdkb',
            DB::raw('( CASE WHEN t_skpdkb.t_idskpdkb IS NOT NULL THEN t_skpdkb.t_idjenisketetapan ELSE t_spt.t_idjenisketetapan END ) AS jenis_ketetapan')
        )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_validasi_kaban', 't_spt.t_idspt', '=', 't_validasi_kaban.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
            ->leftJoin('t_skpdkb', 't_spt.t_idspt', '=', 't_skpdkb.t_idspt')
            ->leftJoin('t_pembayaran_skpdkb', 't_pembayaran_skpdkb.t_idskpdkb', '=', 't_skpdkb.t_idskpdkb')
            ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
            ->whereNotNull('t_validasi_kabid.t_id_validasi_kabid')
                
                    
            ->whereRaw('( CASE WHEN t_skpdkb.t_idskpdkb IS NOT NULL THEN t_pembayaran_skpdkb.t_tglpembayaran_skpdkb IS NULL ELSE t_pembayaran_bphtb.t_tglpembayaran_pokok IS NULL END ) '.$where_kaban.' ')
            ->orderBy('t_spt.t_idspt', 'desc');
         //$response->dd();
        return $response;
    }
    
    
    public function cek_setmenukaban()
    {
        $data = DB::table('s_setmenukaban')->where('s_id_setmenukaban', '=', 1)->first();
        return $data;
    }
}
