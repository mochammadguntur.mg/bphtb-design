<?php

namespace App\Models\Pembayaran;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PembayaranSkpdkb extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_pembayaran_skpdkb';
    protected $primaryKey = 't_id_bayar_skpdkb';
    protected $fillable = [
        't_idspt',
        't_idskpdkb',
        't_nourut_bayar',
        's_id_status_bayar',
        't_iduser_bayar',
        't_tglpembayaran_skpdkb',
        't_jmlhbayar_skpdkb',
        't_tglpembayaran_denda',
        't_jmlhbayar_denda',
        't_jmlhbayar_total'
    ];
    protected static $logAttributes = [
        't_idskpdkb',
        't_nourut_bayar',
        's_id_status_bayar',
        't_iduser_bayar',
        't_tglpembayaran_skpdkb',
        't_jmlhbayar_skpdkb',
        't_tglpembayaran_denda',
        't_jmlhbayar_denda',
        't_jmlhbayar_total'
    ];
}
