<?php

namespace App\Models\Pembayaran;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class PembayaranBphtb extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_pembayaran_bphtb';
    protected $primaryKey = 't_id_pembayaran';
    protected $fillable = ['t_idspt', 't_nourut_bayar', 's_id_status_bayar', 't_iduser_bayar', 't_tglpembayaran_pokok', 't_jmlhbayar_pokok', 't_tglpembayaran_denda', 't_jmlhbayar_denda', 't_jmlhbayar_total'];
    protected static $logAttributes = ['t_idspt', 't_nourut_bayar', 's_id_status_bayar', 't_iduser_bayar', 't_tglpembayaran_pokok', 't_jmlhbayar_pokok', 't_tglpembayaran_denda', 't_jmlhbayar_denda', 't_jmlhbayar_total'];
}
