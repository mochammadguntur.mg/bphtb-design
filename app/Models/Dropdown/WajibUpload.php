<?php

namespace App\Models\Dropdown;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WajibUpload extends Model
{
    use HasFactory;

    protected $table = 's_wajib_up';

    protected $primaryKey = 's_idwajib_up';

    protected $fillable = ['s_ket_wjb'];
}
