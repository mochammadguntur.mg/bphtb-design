<?php

namespace App\Models\Dropdown;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class UrlAktif extends Model
{
    use HasFactory;
    
    protected $table = 's_urlaktif';
}
