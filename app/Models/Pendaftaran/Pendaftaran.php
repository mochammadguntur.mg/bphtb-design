<?php

namespace App\Models\Pendaftaran;

use App\Models\Spt\Spt;
use App\Models\Skpdkb\Skpdkb;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pendaftaran extends Model
{
    use HasFactory;

    public function datagridPendaftaran($request, $all_session)
    {
        $response = (new Spt())::select(
            't_spt.t_idspt',  
            't_spt.t_idspt as t_idspt_ori',    
            't_spt.t_kohirspt',    
            't_spt.t_tgldaftar_spt',    
            't_spt.t_periodespt',   
            't_spt.t_idnotaris_spt', 
            't_spt.t_idjenistransaksi',    
            't_spt.t_nama_pembeli',
            't_spt.t_nop_sppt',    
            't_spt.t_nilai_bphtb_fix',      
            't_spt.t_idpersetujuan_bphtb',     
            't_spt.t_kodebayar_bphtb',   
            't_spt.t_ntpd', 
            't_spt.t_idjenisketetapan',
            't_spt.t_uuidspt',    
                
            's_jenistransaksi.s_namajenistransaksi',
            's_notaris.s_namanotaris',
            's_jenis_bidangusaha.s_nama_bidangusaha',
            't_validasi_berkas.t_id_validasi_berkas',
            't_validasi_berkas.s_id_status_berkas',
            't_validasi_berkas.t_tglvalidasi',
            't_validasi_kabid.t_id_validasi_kabid',
            't_validasi_kabid.s_id_status_kabid',
            't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
            't_pembayaran_bphtb.t_id_pembayaran',
            't_pembayaran_bphtb.t_tglpembayaran_pokok',
            's_jenisketetapan.s_namajenisketetapan',
            's_jenisketetapan.s_namasingkatjenisketetapan',
            's_jenisfasilitas.s_namajenisfasilitas',
            's_jenishaktanah.s_namahaktanah',
            's_jenisdoktanah.s_namadoktanah',
            's_jenispengurangan.s_namapengurangan',
            's_jenistanah.nama_jenistanah',
            't_pemeriksaan.t_idpemeriksa'
        )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
            ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
            ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
            ->whereRaw('t_spt.isdeleted IS NULL')
            ->orderBy('t_spt.t_idspt', 'desc')    ;

        
        if (!empty($request['sorting'])) {
            $response = $response->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response = $response->orderBy('t_idspt', 'desc');
        }

        if ($all_session->s_id_hakakses == 4) {
            $response = $response->where('t_idnotaris_spt', '=', $all_session->s_idnotaris);
        }

        if ($request['filter']['t_kohirspt'] != null) {
            $response = $response->where('t_kohirspt', 'ilike', "%" . $request['filter']['t_kohirspt'] . "%");
        }

        if ($request['filter']['t_tgldaftar_spt'] != null) {
            $response = $response->where('t_tgldaftar_spt', 'ilike', "%" . $request['filter']['t_tgldaftar_spt'] . "%");
        }

        if ($request['filter']['t_periodespt'] != null) {
            $response = $response->where('t_periodespt', 'ilike', "%" . $request['filter']['t_periodespt'] . "%");
        }

        if ($request['filter']['t_idnotaris_spt'] != null) {
            $response = $response->where('t_idnotaris_spt', '=', $request['filter']['t_idnotaris_spt']);
        }

        if ($request['filter']['t_idjenistransaksi'] != null) {
            $response = $response->where('t_idjenistransaksi', '=', $request['filter']['t_idjenistransaksi']);
        }

        if ($request['filter']['t_nama_pembeli'] != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $request['filter']['t_nama_pembeli'] . "%");
        }

        if ($request['filter']['t_nop_sppt'] != null) {
            $response = $response->where('t_nop_sppt', 'ilike', "%" . $request['filter']['t_nop_sppt'] . "%");
        }

        if ($request['filter']['t_idpersetujuan_bphtb'] != null) {
            if ($request['filter']['t_idpersetujuan_bphtb'] == 1) {
                $response = $response->where('t_idpersetujuan_bphtb', '=', 1);
            } elseif ($request['filter']['t_idpersetujuan_bphtb'] == 2) {
                $response = $response->whereNull('t_idpersetujuan_bphtb');
            }
        }

        if ($request['filter']['s_id_status_berkas'] != null) {
            if ($request['filter']['s_id_status_berkas'] == 1) {
                $response = $response->where('s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['s_id_status_berkas'] == 2) {
                $response = $response->where('s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['s_id_status_berkas'] == 3) {
                $response = $response->whereNull('s_id_status_berkas');
            }
        }

        if ($request['filter']['s_id_status_kabid'] != null) {
            if ($request['filter']['s_id_status_kabid'] == 1) {
                $response = $response->where('s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['s_id_status_kabid'] == 2) {
                $response = $response->where('s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['s_id_status_kabid'] == 3) {
                $response = $response->whereNull('s_id_status_kabid');
            }
        }

        if ($request['filter']['t_id_pembayaran'] != null) {
            if ($request['filter']['t_id_pembayaran'] == 1) {
                $response = $response->whereNotNull('t_id_pembayaran');
            } elseif ($request['filter']['t_id_pembayaran'] == 2) {
                $response = $response->whereNull('t_id_pembayaran');
            }
        }
        
        if ($request['filter']['t_idjenisketetapan'] != null) {
            $response = $response->where('t_idjenisketetapan', '=', "" . $request['filter']['t_idjenisketetapan'] . "");
        }
        
        
        //====================================================================== DATA SKPDKB
        
        $response2 = (new Skpdkb())::select(
            't_skpdkb.t_idskpdkb as t_idspt',  
            't_skpdkb.t_idspt as t_idspt_ori',    
            't_spt.t_kohirspt',    
            't_skpdkb.t_tglpenetapan as t_tgldaftar_spt',    
            't_skpdkb.t_tahun_sppt as t_periodespt',   
            't_spt.t_idnotaris_spt', 
            't_spt.t_idjenistransaksi',    
            't_spt.t_nama_pembeli',
            't_skpdkb.t_nop_sppt',    
            't_skpdkb.t_jmlh_totalskpdkb as t_nilai_bphtb_fix',      
            't_spt.t_idpersetujuan_bphtb',     
            't_skpdkb.t_kodebayar_skpdkb as t_kodebayar_bphtb',   
            't_skpdkb.t_ntpd_skpdkb as t_ntpd', 
            't_skpdkb.t_idjenisketetapan',    
            't_spt.t_uuidspt',    
                
            's_jenistransaksi.s_namajenistransaksi',
            's_notaris.s_namanotaris',
            's_jenis_bidangusaha.s_nama_bidangusaha',
            't_validasi_berkas.t_id_validasi_berkas',
            't_validasi_berkas.s_id_status_berkas',
            't_validasi_berkas.t_tglvalidasi',
            't_validasi_kabid.t_id_validasi_kabid',
            't_validasi_kabid.s_id_status_kabid',
            't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
            't_pembayaran_skpdkb.t_id_bayar_skpdkb as t_id_pembayaran',
            't_pembayaran_skpdkb.t_tglpembayaran_skpdkb as t_tglpembayaran_pokok',
            's_jenisketetapan.s_namajenisketetapan',
            's_jenisketetapan.s_namasingkatjenisketetapan',
            's_jenisfasilitas.s_namajenisfasilitas',
            's_jenishaktanah.s_namahaktanah',
            's_jenisdoktanah.s_namadoktanah',
            's_jenispengurangan.s_namapengurangan',
            's_jenistanah.nama_jenistanah',
            't_pemeriksaan.t_idpemeriksa'
        )
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_skpdkb.t_idspt')     
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_skpdkb', 't_skpdkb.t_idskpdkb', '=', 't_pembayaran_skpdkb.t_idskpdkb')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
            ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
            ->leftJoin('s_jenisketetapan', 't_skpdkb.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
            ->whereRaw('t_spt.isdeleted IS NULL')
            ->orderBy('t_spt.t_idspt', 'desc')   
                ;

        
        if (!empty($request['sorting'])) {
            $response2 = $response2->orderBy($request['sorting']['key'], $request['sorting']['order']);
        } else {
            $response2 = $response2->orderBy('t_spt.t_idspt', 'desc');
        }

        if ($all_session->s_id_hakakses == 4) {
            $response2 = $response2->where('t_spt.t_idnotaris_spt', '=', $all_session->s_idnotaris);
        }

        if ($request['filter']['t_kohirspt'] != null) {
            $response2 = $response2->where('t_spt.t_kohirspt', 'ilike', "%" . $request['filter']['t_kohirspt'] . "%");
        }

        if ($request['filter']['t_tgldaftar_spt'] != null) {
            $response2 = $response2->where('t_spt.t_tgldaftar_spt', 'ilike', "%" . $request['filter']['t_tgldaftar_spt'] . "%");
        }

        if ($request['filter']['t_periodespt'] != null) {
            $response2 = $response2->where('t_spt.t_periodespt', 'ilike', "%" . $request['filter']['t_periodespt'] . "%");
        }

        if ($request['filter']['t_idnotaris_spt'] != null) {
            $response2 = $response2->where('t_spt.t_idnotaris_spt', '=', $request['filter']['t_idnotaris_spt']);
        }

        if ($request['filter']['t_idjenistransaksi'] != null) {
            $response2 = $response2->where('t_spt.t_idjenistransaksi', '=', $request['filter']['t_idjenistransaksi']);
        }

        if ($request['filter']['t_nama_pembeli'] != null) {
            $response2 = $response2->where('t_spt.t_nama_pembeli', 'ilike', "%" . $request['filter']['t_nama_pembeli'] . "%");
        }

        if ($request['filter']['t_nop_sppt'] != null) {
            $response2 = $response2->where('t_spt.t_nop_sppt', 'ilike', "%" . $request['filter']['t_nop_sppt'] . "%");
        }

        if ($request['filter']['t_idpersetujuan_bphtb'] != null) {
            if ($request['filter']['t_idpersetujuan_bphtb'] == 1) {
                $response2 = $response2->where('t_spt.t_idpersetujuan_bphtb', '=', 1);
            } elseif ($request['filter']['t_idpersetujuan_bphtb'] == 2) {
                $response2 = $response2->whereNull('t_spt.t_idpersetujuan_bphtb');
            }
        }

        if ($request['filter']['s_id_status_berkas'] != null) {
            if ($request['filter']['s_id_status_berkas'] == 1) {
                $response2 = $response2->where('t_validasi_berkas.s_id_status_berkas', '=', 1);
            } elseif ($request['filter']['s_id_status_berkas'] == 2) {
                $response2 = $response2->where('t_validasi_berkas.s_id_status_berkas', '=', 2);
            } elseif ($request['filter']['s_id_status_berkas'] == 3) {
                $response2 = $response2->whereNull('t_validasi_berkas.s_id_status_berkas');
            }
        }

        if ($request['filter']['s_id_status_kabid'] != null) {
            if ($request['filter']['s_id_status_kabid'] == 1) {
                $response2 = $response2->where('t_validasi_kabid.s_id_status_kabid', '=', 1);
            } elseif ($request['filter']['s_id_status_kabid'] == 2) {
                $response2 = $response2->where('t_validasi_kabid.s_id_status_kabid', '=', 2);
            } elseif ($request['filter']['s_id_status_kabid'] == 3) {
                $response2 = $response2->whereNull('t_validasi_kabid.s_id_status_kabid');
            }
        }

        if ($request['filter']['t_id_pembayaran'] != null) {
            if ($request['filter']['t_id_pembayaran'] == 1) {
                $response2 = $response2->whereNotNull('t_pembayaran_bphtb.t_id_pembayaran');
            } elseif ($request['filter']['t_id_pembayaran'] == 2) {
                $response2 = $response2->whereNull('t_pembayaran_bphtb.t_id_pembayaran');
            }
        }
        
        if ($request['filter']['t_idjenisketetapan'] != null) {
            $response2 = $response2->where('t_spt.t_idjenisketetapan', '=', "" . $request['filter']['t_idjenisketetapan'] . "");
        }
        
        //$response2->union($response)->get();

        $data = $response->union($response2)->orderBy('t_idspt', 'desc')->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        //$data = $response->paginate($request['pagination']['pageSize'], ['*'], 'page', $request['pagination']['pageNumber'] + 1);
        return $data;
    }
}
