<?php

namespace App\Models\Pendaftaran;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class NikBersama extends Model {

    use HasFactory;
    
    protected $table = 't_nik_bersama';
    protected $primaryKey = 't_id_nik_bersama';
    protected $fillable = ['t_idspt', 't_nik_bersama', 't_nama_bersama', 't_nama_jalan', 't_rt_bersama', 't_rw_bersama', 't_kecamatan_bersama', 't_kelurahan_bersama', 't_kabkota_bersama', 't_nohp_bersama', 'created_at', 'updated_at'];

}
