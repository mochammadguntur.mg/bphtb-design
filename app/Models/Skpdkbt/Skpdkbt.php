<?php

namespace App\Models\Skpdkbt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Skpdkbt extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_skpdkbt';
    protected $primaryKey = 't_idskpdkbt';
    protected $fillable = ['t_idspt', 't_idskpdkb', 't_tglpenetapan', 't_tglby_system', 't_iduser_buat', 't_nop_sppt', 't_tahun_sppt', 't_luastanah_skpdkbt', 't_njoptanah_skpdkbt', 't_totalnjoptanah_skpdkbt', 't_luasbangunan_skpdkbt', 't_njopbangunan_skpdkbt', 't_totalnjopbangunan_skpdkbt', 't_grandtotalnjop_skpdkbt', 't_nilaitransaksispt_skpdkbt', 't_trf_aphb_kali_skpdkbt', 't_trf_aphb_bagi_skpdkbt', 't_permeter_tanah_skpdkbt', 't_idtarifbphtb_skpdkbt', 't_persenbphtb_skpdkbt', 't_npop_bphtb_skpdkbt', 't_npoptkp_bphtb_skpdkbt', 't_npopkp_bphtb_skpdkbt', 't_nilai_bayar_sblumnya', 't_nilai_pokok_skpdkbt', 't_jmlh_blndenda', 't_jmlh_dendaskpdkbt', 't_jmlh_totalskpdkbt', 't_tglbuat_kodebyar', 't_nourut_kodebayar', 't_kodebayar_skpdkbt', 't_tgljatuhtempo_skpdkbt', 't_idjenisketetapan', 't_idpersetujuan_skpdkbt', 'isdeleted', 't_idoperator_deleted'];
    protected static $logAttributes = ['t_idspt', 't_idskpdkb', 't_tglpenetapan', 't_tglby_system', 't_iduser_buat', 't_nop_sppt', 't_tahun_sppt', 't_luastanah_skpdkbt', 't_njoptanah_skpdkbt', 't_totalnjoptanah_skpdkbt', 't_luasbangunan_skpdkbt', 't_njopbangunan_skpdkbt', 't_totalnjopbangunan_skpdkbt', 't_grandtotalnjop_skpdkbt', 't_nilaitransaksispt_skpdkbt', 't_trf_aphb_kali_skpdkbt', 't_trf_aphb_bagi_skpdkbt', 't_permeter_tanah_skpdkbt', 't_idtarifbphtb_skpdkbt', 't_persenbphtb_skpdkbt', 't_npop_bphtb_skpdkbt', 't_npoptkp_bphtb_skpdkbt', 't_npopkp_bphtb_skpdkbt', 't_nilai_bayar_sblumnya', 't_nilai_pokok_skpdkbt', 't_jmlh_blndenda', 't_jmlh_dendaskpdkbt', 't_jmlh_totalskpdkbt', 't_tglbuat_kodebyar', 't_nourut_kodebayar', 't_kodebayar_skpdkbt', 't_tgljatuhtempo_skpdkbt', 't_idjenisketetapan', 't_idpersetujuan_skpdkbt', 'isdeleted', 't_idoperator_deleted'];
}