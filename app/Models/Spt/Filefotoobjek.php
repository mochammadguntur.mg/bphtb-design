<?php

namespace App\Models\Spt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;

class Filefotoobjek extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_file_objek';
    protected $primaryKey = 't_idfile_objek';
    protected $fillable = [
                't_idspt',
                't_iduser_upload',
                'letak_file',
                'nama_file',
                't_tgl_upload',
                't_keterangan_file',
                'created_at',
                'updated_at'
        
        ];
    protected static $logAttributes = [
                 't_idspt',
                't_iduser_upload',
                'letak_file',
                'nama_file',
                't_tgl_upload',
                't_keterangan_file',
                'created_at',
                'updated_at'
    ];
    
}
