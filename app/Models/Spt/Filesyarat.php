<?php

namespace App\Models\Spt;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;

class Filesyarat extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_filesyarat_bphtb';
    protected $primaryKey = 't_id_filesyarat';
    protected $fillable = [
                't_idspt',
                's_idpersyaratan',
                's_idjenistransaksi',
                't_iduser_upload',
                'letak_file',
                'nama_file',
                't_tgl_upload',
                't_keterangan_file',
                'created_at',
                'updated_at'
        
        ];
    protected static $logAttributes = [
                    't_idspt',
                's_idpersyaratan',
                's_idjenistransaksi',
                't_iduser_upload',
                'letak_file',
                'nama_file',
                't_tgl_upload',
                't_keterangan_file',
                'created_at',
                'updated_at'
    ];
    
}
