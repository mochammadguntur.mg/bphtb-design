<?php

namespace App\Models\Spt;

use App\Models\Skpdkb\Skpdkb;
use App\Models\Setting\Notaris;
use App\Models\Setting\HakTanah;
use App\Models\Pelaporan\InputAjb;
use App\Models\Setting\JenisTanah;
use Illuminate\Support\Facades\DB;
use App\Models\Setting\JenisTransaksi;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use App\Models\Setting\JenisDokumenTanah;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Spt extends Model
{

    use HasFactory,
        LogsActivity,
        Notifiable;

    protected $table = 't_spt';
    protected $primaryKey = 't_idspt';
    protected $fillable = [
        't_uuidspt',
        't_kohirspt',
        't_tgldaftar_spt',
        't_tglby_system',
        't_periodespt',
        't_idjenistransaksi',
        't_idnotaris_spt',
        't_npwpd',
        't_idstatus_npwpd',
        't_idbidang_usaha',
        't_nama_pembeli',
        't_nik_pembeli',
        't_npwp_pembeli',
        't_jalan_pembeli',
        't_kabkota_pembeli',
        't_idkec_pembeli',
        't_namakecamatan_pembeli',
        't_idkel_pembeli',
        't_namakelurahan_pembeli',
        't_rt_pembeli',
        't_rw_pembeli',
        't_nohp_pembeli',
        't_notelp_pembeli',
        't_email_pembeli',
        't_kodepos_pembeli',
        't_siup_pembeli',
        't_nib_pembeli',
        't_ketdomisili_pembeli',
        't_b_nama_pngjwb_pembeli',
        't_b_nik_pngjwb_pembeli',
        't_b_npwp_pngjwb_pembeli',
        't_b_statusjab_pngjwb_pembeli',
        't_b_jalan_pngjwb_pembeli',
        't_b_kabkota_pngjwb_pembeli',
        't_b_idkec_pngjwb_pembeli',
        't_b_namakec_pngjwb_pembeli',
        't_b_idkel_pngjwb_pembeli',
        't_b_namakel_pngjwb_pembeli',
        't_b_rt_pngjwb_pembeli',
        't_b_rw_pngjwb_pembeli',
        't_b_nohp_pngjwb_pembeli',
        't_b_notelp_pngjwb_pembeli',
        't_b_email_pngjwb_pembeli',
        't_b_kodepos_pngjwb_pembeli',
        't_nop_sppt',
        't_tahun_sppt',
        't_nama_sppt',
        't_jalan_sppt',
        't_kabkota_sppt',
        't_kecamatan_sppt',
        't_kelurahan_sppt',
        't_rt_sppt',
        't_rw_sppt',
        't_luastanah_sismiop',
        't_luasbangunan_sismiop',
        't_njoptanah_sismiop',
        't_njopbangunan_sismiop',
        't_luastanah',
        't_njoptanah',
        't_totalnjoptanah',
        't_luasbangunan',
        't_njopbangunan',
        't_totalnjopbangunan',
        't_grandtotalnjop',
        't_nilaitransaksispt',
        't_tarif_pembagian_aphb_kali',
        't_tarif_pembagian_aphb_bagi',
        't_permeter_tanah',
        't_idjenisfasilitas',
        't_idjenishaktanah',
        't_idjenisdoktanah',
        't_idpengurangan',
        't_id_jenistanah',
        't_nosertifikathaktanah',
        't_tgldok_tanah',
        's_latitude',
        's_longitude',
        't_idtarifbphtb',
        't_persenbphtb',
        't_npop_bphtb',
        't_npoptkp_bphtb',
        't_npopkp_bphtb',
        't_nilai_bphtb_fix',
        't_nilai_bphtb_real',
        't_nama_penjual',
        't_nik_penjual',
        't_npwp_penjual',
        't_jalan_penjual',
        't_kabkota_penjual',
        't_idkec_penjual',
        't_namakec_penjual',
        't_idkel_penjual',
        't_namakel_penjual',
        't_rt_penjual',
        't_rw_penjual',
        't_nohp_penjual',
        't_notelp_penjual',
        't_email_penjual',
        't_kodepos_penjual',
        't_siup_penjual',
        't_nib_penjual',
        't_ketdomisili_penjual',
        't_b_nama_pngjwb_penjual',
        't_b_nik_pngjwb_penjual',
        't_b_npwp_pngjwb_penjual',
        't_b_statusjab_pngjwb_penjual',
        't_b_jalan_pngjwb_penjual',
        't_b_kabkota_pngjwb_penjual',
        't_b_idkec_pngjwb_penjual',
        't_b_namakec_pngjwb_penjual',
        't_b_idkel_pngjwb_penjual',
        't_b_namakel_pngjwb_penjual',
        't_b_rt_pngjwb_penjual',
        't_b_rw_pngjwb_penjual',
        't_b_nohp_pngjwb_penjual',
        't_b_notelp_pngjwb_penjual',
        't_b_email_pngjwb_penjual',
        't_b_kodepos_pngjwb_penjual',
        't_tglbuat_kodebyar',
        't_nourut_kodebayar',
        't_kodebayar_bphtb',
        't_tglketetapan_spt',
        't_tgljatuhtempo_spt',
        't_idjenisketetapan',
        't_idpersetujuan_bphtb'
    ];
    protected static $logAttributes = [
        't_uuidspt',
        't_kohirspt',
        't_tgldaftar_spt',
        't_tglby_system',
        't_periodespt',
        't_idjenistransaksi',
        't_idnotaris_spt',
        't_npwpd',
        't_idstatus_npwpd',
        't_idbidang_usaha',
        't_nama_pembeli',
        't_nik_pembeli',
        't_npwp_pembeli',
        't_jalan_pembeli',
        't_kabkota_pembeli',
        't_idkec_pembeli',
        't_namakecamatan_pembeli',
        't_idkel_pembeli',
        't_namakelurahan_pembeli',
        't_rt_pembeli',
        't_rw_pembeli',
        't_nohp_pembeli',
        't_notelp_pembeli',
        't_email_pembeli',
        't_kodepos_pembeli',
        't_siup_pembeli',
        't_nib_pembeli',
        't_ketdomisili_pembeli',
        't_b_nama_pngjwb_pembeli',
        't_b_nik_pngjwb_pembeli',
        't_b_npwp_pngjwb_pembeli',
        't_b_statusjab_pngjwb_pembeli',
        't_b_jalan_pngjwb_pembeli',
        't_b_kabkota_pngjwb_pembeli',
        't_b_idkec_pngjwb_pembeli',
        't_b_namakec_pngjwb_pembeli',
        't_b_idkel_pngjwb_pembeli',
        't_b_namakel_pngjwb_pembeli',
        't_b_rt_pngjwb_pembeli',
        't_b_rw_pngjwb_pembeli',
        't_b_nohp_pngjwb_pembeli',
        't_b_notelp_pngjwb_pembeli',
        't_b_email_pngjwb_pembeli',
        't_b_kodepos_pngjwb_pembeli',
        't_nop_sppt',
        't_tahun_sppt',
        't_nama_sppt',
        't_jalan_sppt',
        't_kabkota_sppt',
        't_kecamatan_sppt',
        't_kelurahan_sppt',
        't_rt_sppt',
        't_rw_sppt',
        't_luastanah_sismiop',
        't_luasbangunan_sismiop',
        't_njoptanah_sismiop',
        't_njopbangunan_sismiop',
        't_luastanah',
        't_njoptanah',
        't_totalnjoptanah',
        't_luasbangunan',
        't_njopbangunan',
        't_totalnjopbangunan',
        't_grandtotalnjop',
        't_nilaitransaksispt',
        't_tarif_pembagian_aphb_kali',
        't_tarif_pembagian_aphb_bagi',
        't_permeter_tanah',
        't_idjenisfasilitas',
        't_idjenishaktanah',
        't_idjenisdoktanah',
        't_idpengurangan',
        't_id_jenistanah',
        't_nosertifikathaktanah',
        't_tgldok_tanah',
        's_latitude',
        's_longitude',
        't_idtarifbphtb',
        't_persenbphtb',
        't_npop_bphtb',
        't_npoptkp_bphtb',
        't_npopkp_bphtb',
        't_nilai_bphtb_fix',
        't_nilai_bphtb_real',
        't_nama_penjual',
        't_nik_penjual',
        't_npwp_penjual',
        't_jalan_penjual',
        't_kabkota_penjual',
        't_idkec_penjual',
        't_namakec_penjual',
        't_idkel_penjual',
        't_namakel_penjual',
        't_rt_penjual',
        't_rw_penjual',
        't_nohp_penjual',
        't_notelp_penjual',
        't_email_penjual',
        't_kodepos_penjual',
        't_siup_penjual',
        't_nib_penjual',
        't_ketdomisili_penjual',
        't_b_nama_pngjwb_penjual',
        't_b_nik_pngjwb_penjual',
        't_b_npwp_pngjwb_penjual',
        't_b_statusjab_pngjwb_penjual',
        't_b_jalan_pngjwb_penjual',
        't_b_kabkota_pngjwb_penjual',
        't_b_idkec_pngjwb_penjual',
        't_b_namakec_pngjwb_penjual',
        't_b_idkel_pngjwb_penjual',
        't_b_namakel_pngjwb_penjual',
        't_b_rt_pngjwb_penjual',
        't_b_rw_pngjwb_penjual',
        't_b_nohp_pngjwb_penjual',
        't_b_notelp_pngjwb_penjual',
        't_b_email_pngjwb_penjual',
        't_b_kodepos_pngjwb_penjual',
        't_tglbuat_kodebyar',
        't_nourut_kodebayar',
        't_kodebayar_bphtb',
        't_tglketetapan_spt',
        't_tgljatuhtempo_spt',
        't_idjenisketetapan',
        't_idpersetujuan_bphtb'
    ];

    public function jenisTransaksi()
    {
        return $this->belongsTo(JenisTransaksi::class, 't_idjenistransaksi', 's_idjenistransaksi');
    }

    public function notaris()
    {
        return $this->belongsTo(Notaris::class, 't_idnotaris_spt', 's_idnotaris');
    }

    public function inputAjb()
    {
        return $this->hasOne(InputAjb::class, 't_idspt');
    }

    public function cekdata_pendaftaran_bphtb()
    {
        $data = DB::table('t_spt')->get();
        return $data;
    }

    public function cekdata_notaris()
    {
        $data = DB::table('s_notaris')->where('s_statusnotaris', '=', 1)->get();
        return $data;
    }

    public function jenisHak()
    {
        return $this->belongsTo(
            HakTanah::class,
            't_idjenishaktanah',
            's_idhaktanah'
        );
    }

    public function cekdata_jenis_transaksi()
    {
        $data = DB::table('s_jenistransaksi')->orderBy('s_kodejenistransaksi', 'asc')->get();
        return $data;
    }

    public function cekdata_jenis_bidangusaha()
    {
        $data = DB::table('s_jenis_bidangusaha')->get();
        return $data;
    }

    public function cekdata_kecamatan()
    {
        $data = DB::table('s_kecamatan')->get();
        return $data;
    }

    public function cek_id_kecamatan($idkec)
    {
        $data = DB::table('s_kecamatan')->where('s_idkecamatan', '=', $idkec)->first();
        return $data;
    }

    public function cek_id_datakelurahan($idkec)
    {
        $data = DB::table('s_kelurahan')->where('s_idkecamatan', '=', $idkec)->get();
        return $data;
    }

    public function cek_id_kelurahan($id)
    {
        $data = DB::table('s_kelurahan')->where('s_idkelurahan', '=', $id)->first();
        return $data;
    }

    public function cek_data_nik($idspt){
        $data = DB::table('t_nik_bersama')->where('t_idspt', '=', $idspt)->get();
        return $data;
    }

    public function cek_id_spt($id)
    {
        $data = DB::table('t_spt')
            ->select(
                't_spt.*',
                's_jenistransaksi.*',
                't_validasi_berkas.t_id_validasi_berkas',
                't_validasi_berkas.s_id_status_berkas',
                't_validasi_berkas.t_tglvalidasi',
                't_validasi_berkas.t_keterangan_berkas',
                't_validasi_kabid.t_id_validasi_kabid',
                't_validasi_kabid.s_id_status_kabid',
                't_validasi_kabid.t_keterangan_kabid',
                't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                't_validasi_kaban.t_id_validasi_kaban',
                't_validasi_kaban.s_id_status_kaban',
                't_validasi_kaban.t_keterangan_kaban',
                't_validasi_kaban.t_tglvalidasikaban',
                't_pembayaran_bphtb.t_id_pembayaran',
                't_pembayaran_bphtb.t_nourut_bayar',
                't_pembayaran_bphtb.t_tglpembayaran_pokok',
                't_pembayaran_bphtb.t_jmlhbayar_pokok',
                't_pembayaran_bphtb.t_tglpembayaran_denda',
                't_pembayaran_bphtb.t_jmlhbayar_denda',
                't_pembayaran_bphtb.t_jmlhbayar_total',
                's_jenisfasilitas.s_namajenisfasilitas',
                's_jenishaktanah.s_namahaktanah',
                's_jenisdoktanah.s_namadoktanah',
                's_jenispengurangan.s_namapengurangan',
                's_jenistanah.nama_jenistanah',
                's_notaris.s_namanotaris',
                't_pemeriksaan.t_idpemeriksa',
                't_pemeriksaan.t_noperiksa',
                't_pemeriksaan.t_idpejabat1',
                't_pemeriksaan.t_idpejabat2',
                't_pemeriksaan.t_idpejabat3',
                't_pemeriksaan.t_idpejabat4',
                't_pemeriksaan.t_keterangan',
                't_pemeriksaan.t_luastanah_pemeriksa',
                't_pemeriksaan.t_njoptanah_pemeriksa',
                't_pemeriksaan.t_totalnjoptanah_pemeriksa',
                't_pemeriksaan.t_luasbangunan_pemeriksa',
                't_pemeriksaan.t_njopbangunan_pemeriksa',
                't_pemeriksaan.t_totalnjopbangunan_pemeriksa',
                't_pemeriksaan.t_grandtotalnjop_pemeriksa',
                't_pemeriksaan.t_nilaitransaksispt_pemeriksa',
                't_pemeriksaan.t_permeter_tanah_pemeriksa',
                't_pemeriksaan.t_idtarifbphtb_pemeriksa',
                't_pemeriksaan.t_persenbphtb_pemeriksa',
                't_pemeriksaan.t_npop_bphtb_pemeriksa',
                't_pemeriksaan.t_npoptkp_bphtb_pemeriksa',
                't_pemeriksaan.t_npopkp_bphtb_pemeriksa',
                't_pemeriksaan.t_nilaibphtb_pemeriksa',
                't_pemeriksaan.t_idpersetujuan_pemeriksa',
                's_status_berkas.s_nama_status_berkas',
                's_status_kabid.s_nama_status_kabid',
                't_validasi_berkas.t_persyaratan_validasi_berkas',
                'users.username as username_kabid',
                's_pejabat.s_namapejabat as namapejabat_kabid',
                's_pejabat.s_nippejabat as nippejabat_kabid',
                'b.s_namapejabat as namapejabat_kaban',
                'b.s_nippejabat as nippejabat_kaban'   
            )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_validasi_kaban', 't_spt.t_idspt', '=', 't_validasi_kaban.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
            ->leftJoin('s_status_berkas', 't_validasi_berkas.s_id_status_berkas', '=', 's_status_berkas.s_id_status_berkas')
            ->leftJoin('s_status_kabid', 't_validasi_kabid.s_id_status_kabid', '=', 's_status_kabid.s_id_status_kabid')
            ->leftJoin('users', 'users.id', '=', 't_validasi_kabid.t_iduser_validasi')    
            ->leftJoin('s_pejabat', 's_pejabat.s_idpejabat', '=', 'users.s_idpejabat')      
            ->leftJoin('users as a', 'a.id', '=', 't_validasi_kaban.t_iduser_validasikaban')    
            ->leftJoin('s_pejabat as b', 'b.s_idpejabat', '=', 'a.s_idpejabat')          
        ->where('t_spt.t_idspt', '=', $id)->first();

        return $data;
    }
    
    public function cek_uuid_spt($id)
    {
        $data = DB::table('t_spt')
            ->select(
                't_spt.*',
                's_jenistransaksi.*',
                't_validasi_berkas.t_id_validasi_berkas',
                't_validasi_berkas.s_id_status_berkas',
                't_validasi_berkas.t_tglvalidasi',
                't_validasi_berkas.t_keterangan_berkas',
                't_validasi_kabid.t_id_validasi_kabid',
                't_validasi_kabid.s_id_status_kabid',
                't_validasi_kabid.t_keterangan_kabid',
                't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                't_validasi_kaban.t_id_validasi_kaban',
                't_validasi_kaban.s_id_status_kaban',
                't_validasi_kaban.t_keterangan_kaban',
                't_validasi_kaban.t_tglvalidasikaban',
                't_pembayaran_bphtb.t_id_pembayaran',
                't_pembayaran_bphtb.t_nourut_bayar',
                't_pembayaran_bphtb.t_tglpembayaran_pokok',
                't_pembayaran_bphtb.t_jmlhbayar_pokok',
                't_pembayaran_bphtb.t_tglpembayaran_denda',
                't_pembayaran_bphtb.t_jmlhbayar_denda',
                't_pembayaran_bphtb.t_jmlhbayar_total',
                's_jenisfasilitas.s_namajenisfasilitas',
                's_jenishaktanah.s_namahaktanah',
                's_jenisdoktanah.s_namadoktanah',
                's_jenispengurangan.s_namapengurangan',
                's_jenistanah.nama_jenistanah',
                's_notaris.s_namanotaris',
                't_pemeriksaan.t_idpemeriksa',
                't_pemeriksaan.t_noperiksa',
                't_pemeriksaan.t_idpejabat1',
                't_pemeriksaan.t_idpejabat2',
                't_pemeriksaan.t_idpejabat3',
                't_pemeriksaan.t_idpejabat4',
                't_pemeriksaan.t_keterangan',
                't_pemeriksaan.t_luastanah_pemeriksa',
                't_pemeriksaan.t_njoptanah_pemeriksa',
                't_pemeriksaan.t_totalnjoptanah_pemeriksa',
                't_pemeriksaan.t_luasbangunan_pemeriksa',
                't_pemeriksaan.t_njopbangunan_pemeriksa',
                't_pemeriksaan.t_totalnjopbangunan_pemeriksa',
                't_pemeriksaan.t_grandtotalnjop_pemeriksa',
                't_pemeriksaan.t_nilaitransaksispt_pemeriksa',
                't_pemeriksaan.t_permeter_tanah_pemeriksa',
                't_pemeriksaan.t_idtarifbphtb_pemeriksa',
                't_pemeriksaan.t_persenbphtb_pemeriksa',
                't_pemeriksaan.t_npop_bphtb_pemeriksa',
                't_pemeriksaan.t_npoptkp_bphtb_pemeriksa',
                't_pemeriksaan.t_npopkp_bphtb_pemeriksa',
                't_pemeriksaan.t_nilaibphtb_pemeriksa',
                't_pemeriksaan.t_idpersetujuan_pemeriksa',
                's_status_berkas.s_nama_status_berkas',
                's_status_kabid.s_nama_status_kabid',
                't_validasi_berkas.t_persyaratan_validasi_berkas'
            )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_validasi_kaban', 't_spt.t_idspt', '=', 't_validasi_kaban.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
            ->leftJoin('s_status_berkas', 't_validasi_berkas.s_id_status_berkas', '=', 's_status_berkas.s_id_status_berkas')
            ->leftJoin('s_status_kabid', 't_validasi_kabid.s_id_status_kabid', '=', 's_status_kabid.s_id_status_kabid')
            ->where('t_spt.t_uuidspt', '=', $id)->first();

        return $data;
    }

    public function cek_id_spt_pemeriksaan($id)
    {
        $data = DB::table('t_spt')
            ->select(
                't_spt.*',
                's_jenistransaksi.*',
                't_validasi_berkas.t_id_validasi_berkas',
                't_validasi_berkas.s_id_status_berkas',
                't_validasi_berkas.t_tglvalidasi',
                't_validasi_berkas.t_keterangan_berkas',
                't_validasi_kabid.t_id_validasi_kabid',
                't_validasi_kabid.s_id_status_kabid',
                't_validasi_kabid.t_keterangan_kabid',
                't_pembayaran_bphtb.t_id_pembayaran',
                't_pembayaran_bphtb.t_nourut_bayar',
                't_pembayaran_bphtb.t_tglpembayaran_pokok',
                't_pembayaran_bphtb.t_jmlhbayar_pokok',
                't_pembayaran_bphtb.t_tglpembayaran_denda',
                't_pembayaran_bphtb.t_jmlhbayar_denda',
                't_pembayaran_bphtb.t_jmlhbayar_total',
                's_jenisfasilitas.s_namajenisfasilitas',
                's_jenishaktanah.s_namahaktanah',
                's_jenisdoktanah.s_namadoktanah',
                's_jenispengurangan.s_namapengurangan',
                's_jenistanah.nama_jenistanah',
                's_notaris.s_namanotaris',
                't_pemeriksaan.t_idpemeriksa',
                't_pemeriksaan.t_noperiksa',
                't_pemeriksaan.t_tglpenetapan',
                't_pemeriksaan.t_idpejabat1',
                't_pemeriksaan.t_idpejabat2',
                't_pemeriksaan.t_idpejabat3',
                't_pemeriksaan.t_idpejabat4',
                't_pemeriksaan.t_keterangan',
                't_pemeriksaan.t_luastanah_pemeriksa',
                't_pemeriksaan.t_njoptanah_pemeriksa',
                't_pemeriksaan.t_totalnjoptanah_pemeriksa',
                't_pemeriksaan.t_luasbangunan_pemeriksa',
                't_pemeriksaan.t_njopbangunan_pemeriksa',
                't_pemeriksaan.t_totalnjopbangunan_pemeriksa',
                't_pemeriksaan.t_grandtotalnjop_pemeriksa',
                't_pemeriksaan.t_nilaitransaksispt_pemeriksa',
                't_pemeriksaan.t_permeter_tanah_pemeriksa',
                't_pemeriksaan.t_idtarifbphtb_pemeriksa',
                't_pemeriksaan.t_persenbphtb_pemeriksa',
                't_pemeriksaan.t_npop_bphtb_pemeriksa',
                't_pemeriksaan.t_npoptkp_bphtb_pemeriksa',
                't_pemeriksaan.t_npopkp_bphtb_pemeriksa',
                't_pemeriksaan.t_nilaibphtb_pemeriksa',
                't_pemeriksaan.t_idpersetujuan_pemeriksa',
                's_status_berkas.s_nama_status_berkas',
                's_status_kabid.s_nama_status_kabid',
                't_validasi_berkas.t_persyaratan_validasi_berkas'
            )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
            ->leftJoin('s_status_berkas', 't_validasi_berkas.s_id_status_berkas', '=', 's_status_berkas.s_id_status_berkas')
            ->leftJoin('s_status_kabid', 't_validasi_kabid.s_id_status_kabid', '=', 's_status_kabid.s_id_status_kabid')
            ->where('t_spt.t_idspt', '=', $id)->first();
        return $data;
    }

    public function cekdata_jenis_haktanah()
    {
        $data = DB::table('s_jenishaktanah')->get();
        return $data;
    }

    public function cekdata_jenis_doktanah()
    {
        $data = DB::table('s_jenisdoktanah')->get();
        return $data;
    }

    public function cekdata_jenis_pengurangan()
    {
        $data = DB::table('s_jenispengurangan')->get();
        return $data;
    }

    public function cekdata_jenis_perumahan_tanah()
    {
        $data = DB::table('s_jenistanah')->get();
        return $data;
    }

    public function cekdata_jenis_fasilitas()
    {
        $data = DB::table('s_jenisfasilitas')->get();
        return $data;
    }

    public function cekdata_jenis_ketetapan()
    {
        $data = DB::table('s_jenisketetapan')->get();
        return $data;
    }

    public function cekdata_id_syaratupload($id)
    {
        $data = DB::table('s_persyaratan')->where('s_persyaratan.s_idjenistransaksi', '=', $id)->orderBy('s_idpersyaratan')->get();
        return $data;
    }

    public function cekdata_data_status_berkas()
    {
        $data = DB::table('s_status_berkas')->get();
        return $data;
    }

    public function cekdata_data_status_kabid()
    {
        $data = DB::table('s_status_kabid')->get();
        return $data;
    }

    public function cekdata_data_status_bayar()
    {
        $data = DB::table('s_status_bayar')->get();
        return $data;
    }

    public function cari_history_transaksi($q)
    {

        $data = DB::table('t_spt')
            ->select(
                't_spt.t_idspt',
                't_spt.t_kohirspt',
                't_spt.t_nilai_bphtb_fix',
                's_jenistransaksi.s_namajenistransaksi',
                's_notaris.s_namanotaris',
                't_spt.t_nop_sppt',
                't_spt.t_nik_pembeli',
                't_spt.t_nib_pembeli',
                't_spt.t_npwp_pembeli',
                't_spt.t_namakecamatan_pembeli',
                't_spt.t_namakelurahan_pembeli',
                't_spt.t_jalan_pembeli',
                't_spt.t_nohp_pembeli',
                't_spt.t_nama_pembeli',
                't_spt.t_tgldaftar_spt',
                't_pembayaran_bphtb.t_jmlhbayar_total',
                't_pembayaran_bphtb.t_id_pembayaran',
                's_status_berkas.s_nama_status_berkas',
                't_validasi_berkas.t_tglvalidasi',
                't_validasi_berkas.t_keterangan_berkas',
                's_status_bayar.s_nama_status_bayar'
            )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_status_bayar', 't_pembayaran_bphtb.s_id_status_bayar', '=', 's_status_bayar.s_id_status_bayar')
            ->leftJoin('s_status_berkas', 't_validasi_berkas.s_id_status_berkas', '=', 's_status_berkas.s_id_status_berkas')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->Where('t_spt.t_nik_pembeli', '=', $q)
            ->orWhere('t_spt.t_nib_pembeli', '=', $q)
            ->orderBy('t_spt.t_tgldaftar_spt', 'desc')
            ->get();
        return $data;
    }

    public function cek_history_tahun_berjalan($id)
    {
        $nik = DB::table('t_spt')->select('t_nik_pembeli')->where('t_spt.t_idspt', '=', $id)->first();
        $string_nik = (string) $nik->t_nik_pembeli;
        $data = DB::table('t_spt')->select(
            't_spt.t_idspt',
            't_spt.t_tgldaftar_spt',
            't_spt.t_nik_pembeli',
            't_spt.t_nama_pembeli',
            't_spt.t_kohirspt',
            's_jenistransaksi.s_namajenistransaksi',
            't_spt.t_periodespt',
            's_notaris.s_namanotaris',
            't_spt.t_nop_sppt',
            't_spt.t_nilai_bphtb_fix',
        )->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->Where('t_spt.t_nik_pembeli', '=', $string_nik)
            ->whereRaw("extract(year from t_spt.t_tgldaftar_spt) = '" . date('Y') . "'")
            ->where('t_idspt', '<>', $id)
            ->whereNotNull('t_nop_sppt')
            ->orderBy('t_spt.t_tgldaftar_spt', 'desc')
            ->get();
        return $data;
    }

    public function cek_detail_history_transaksi($id)
    {
        $data = DB::table('t_spt')->select(
            't_spt.t_idspt',
            't_spt.t_nama_pembeli',
            't_spt.t_nik_pembeli',
            't_spt.t_npwp_pembeli',
            't_spt.t_namakecamatan_pembeli',
            't_spt.t_namakelurahan_pembeli',
            't_spt.t_jalan_pembeli',
            't_spt.t_nohp_pembeli',
            's_status_berkas.s_nama_status_berkas',
            't_validasi_berkas.t_tglvalidasi',
            't_validasi_berkas.t_keterangan_berkas',
            's_status_kabid.s_nama_status_kabid',
            't_validasi_kabid.t_tglvalidasi',
            't_validasi_kabid.t_keterangan_kabid',
            't_pembayaran_bphtb.t_jmlhbayar_total',
            't_pembayaran_bphtb.t_tglpembayaran_pokok',
            's_status_bayar.s_nama_status_bayar',
            's_notaris.s_namanotaris'
        )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_status_bayar', 't_pembayaran_bphtb.s_id_status_bayar', '=', 's_status_bayar.s_id_status_bayar')
            ->leftJoin('s_status_berkas', 't_validasi_berkas.s_id_status_berkas', '=', 's_status_berkas.s_id_status_berkas')
            ->leftJoin('s_status_kabid', 't_validasi_kabid.s_id_status_kabid', '=', 's_status_kabid.s_id_status_kabid')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->where('t_spt.t_idspt', '=', $id)
            // ->whereRaw("extract(year from t_spt.t_tgldaftar_spt) = '".date('Y')."'")
            ->orderBy('t_spt.t_tgldaftar_spt', 'desc')
            ->get();
        return $data;
    }

    //========================================================================== CEK JUMLAH DATA PENDAFTARAN SSPD BPHTB

    public function cek_jumlah_pendaftaran_sspd($all_session)
    {
        if ($all_session->s_id_hakakses == 4 || $all_session->s_idnotaris != null) {
            $where_idnotaris = ' and t_spt.t_idnotaris_spt = ' . $all_session->s_idnotaris . ' ';
        } else {
            $where_idnotaris = '';
        }


        $data = DB::table('t_spt')
            ->select(
                DB::raw('count(t_spt.t_idspt) as jml_spt')
            )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            //->whereRaw("extract(year from t_spt.t_tgldaftar_spt) = '" . date('Y') . "' " . $where_idnotaris . " ")
            ->whereRaw("t_idpersetujuan_bphtb=1 and isdeleted IS NULL and extract(year from t_spt.t_tgldaftar_spt) = '" . date('Y') . "' " . $where_idnotaris . " ")    
            //->groupBy('t_spt.t_idspt')
            ->first();
        return $data;
    }

    //========================================================================== END CEK JUMLAH DATA PENDAFTARAN SSPD BPHTB
    //========================================================================== CEK JUMLAH DATA PENDAFTARAN SSPD BPHTB

    public function cek_jumlah_pendaftaran_sspd_terbaru($all_session)
    {
        if ($all_session->s_id_hakakses == 4) {
            $where_idnotaris = ' and t_spt.t_idnotaris_spt = ' . $all_session->s_idnotaris . ' ';
        } else {
            $where_idnotaris = '';
        }
        $data = DB::table('t_spt')
            ->select(
                't_spt.t_idspt',
                't_spt.t_nama_pembeli',
                't_spt.t_tgldaftar_spt',
                's_jenistransaksi.s_namajenistransaksi',
                't_pembayaran_bphtb.t_jmlhbayar_total',
                't_pembayaran_bphtb.t_id_pembayaran',
                's_status_bayar.s_nama_status_bayar',
                's_notaris.s_namanotaris',
                't_spt.t_nilai_bphtb_fix'    
            )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_status_bayar', 't_pembayaran_bphtb.s_id_status_bayar', '=', 's_status_bayar.s_id_status_bayar')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->whereRaw("t_idpersetujuan_bphtb=1 and isdeleted IS NULL and extract(year from t_spt.t_tgldaftar_spt) = '" . date('Y') . "' " . $where_idnotaris . " ") //and t_nilai_bphtb_fix > 0 
            ->orderBy('t_spt.t_tgldaftar_spt', 'desc')
            //->groupBy('t_spt.t_idspt')
            ->get();
        return $data;
    }

    //========================================================================== CEK JUMLAH DATA VALIDASI BERKAS SSPD BPHTB

    public function cek_jumlah_validasi_berkas($all_session)
    {
        if ($all_session->s_id_hakakses == 4) {
            $where_idnotaris = ' and t_spt.t_idnotaris_spt = ' . $all_session->s_idnotaris . ' ';
        } else {
            $where_idnotaris = '';
        }

        $data = DB::table('t_spt')
            ->select(
                DB::raw('count(t_validasi_berkas.t_id_validasi_berkas) as jml_spt')
            )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->whereRaw("extract(year from t_validasi_berkas.t_tglvalidasi) = '" . date('Y') . "' " . $where_idnotaris . " ")
            ->first();
        return $data;
    }

    public function cek_jumlah_validasi_kabid($all_session)
    {
        if ($all_session->s_id_hakakses == 4) {
            $where_idnotaris = ' and t_spt.t_idnotaris_spt = ' . $all_session->s_idnotaris . ' ';
        } else {
            $where_idnotaris = '';
        }

        $data = DB::table('t_spt')
            ->select(DB::raw('count(t_validasi_kabid.t_id_validasi_kabid) as jml_spt'))
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->whereRaw("extract(year from t_validasi_kabid.t_tglvalidasi) = '" . date('Y') . "' " . $where_idnotaris . " ")
            //->groupBy('t_validasi_berkas.t_id_validasi_berkas')
            ->first();
        return $data;
    }

    public function cek_jumlah_pembayaran($all_session)
    {
        if ($all_session->s_id_hakakses == 4) {
            $where_idnotaris = ' and t_spt.t_idnotaris_spt = ' . $all_session->s_idnotaris . ' ';
        } else {
            $where_idnotaris = '';
        }

        $data = DB::table('t_spt')
            ->select(
                DB::raw('count(t_pembayaran_bphtb.t_id_pembayaran) as jml_spt')
            )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->whereRaw("extract(year from t_pembayaran_bphtb.t_tglpembayaran_pokok) = '" . date('Y') . "' " . $where_idnotaris . " ")
            //->groupBy('t_validasi_berkas.t_id_validasi_berkas')
            ->first();
        return $data;
    }

    public function cek_jumlah_ajb($all_session)
    {
        if ($all_session->s_id_hakakses == 4) {
            $where_idnotaris = ' and t_spt.t_idnotaris_spt = ' . $all_session->s_idnotaris . ' ';
        } else {
            $where_idnotaris = '';
        }

        $data = DB::table('t_spt')
            ->select(DB::raw('count(t_inputajb.t_idajb) as jml_spt'))
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_inputajb', 't_spt.t_idspt', '=', 't_inputajb.t_idspt')
            ->whereRaw("extract(year from t_pembayaran_bphtb.t_tglpembayaran_pokok) = '" . date('Y') . "' " . $where_idnotaris . " ")
            ->first();
        return $data;
    }

    public function cek_jumlah_lapor_bulanan($all_session)
    {
        if ($all_session->s_id_hakakses == 4) {
            $where_idnotaris = ' and t_spt.t_idnotaris_spt = ' . $all_session->s_idnotaris . ' ';
        } else {
            $where_idnotaris = '';
        }

        $data = DB::table('t_spt')->select(DB::raw('count(t_laporbulanan_head.t_idlaporbulanan) as jml_spt'))
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_inputajb', 't_spt.t_idspt', '=', 't_inputajb.t_idspt')
            ->leftJoin('t_laporbulanan_detail', 't_inputajb.t_idajb', '=', 't_laporbulanan_detail.t_idajb')
            ->leftJoin('t_laporbulanan_head', 't_laporbulanan_detail.t_idlaporbulanan', '=', 't_laporbulanan_head.t_idlaporbulanan')
            ->whereRaw("extract(year from t_pembayaran_bphtb.t_tglpembayaran_pokok) = '" . date('Y') . "' " . $where_idnotaris . " ")
            ->first();
        return $data;
    }

    public function cek_jumlah_skpdkb($all_session)
    {
        if ($all_session->s_id_hakakses == 4) {
            $where_idnotaris = ' and t_spt.t_idnotaris_spt = ' . $all_session->s_idnotaris . ' ';
        } else {
            $where_idnotaris = '';
        }

        $data = DB::table('t_spt')
            ->select(DB::raw('count(t_skpdkb.t_idskpdkb) as jml_skpdkb'))
            ->leftJoin('t_skpdkb', 't_spt.t_idspt', '=', 't_skpdkb.t_idspt')
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->whereRaw("extract(year from t_skpdkb.t_tglpenetapan) = '" . date('Y') . "' " . $where_idnotaris . " ")
            //->groupBy('t_validasi_berkas.t_id_validasi_berkas')
            ->first();
        return $data;
    }

    public function cek_nourut_pertahun($tahun)
    {

        $data = DB::table('t_spt')->select(DB::raw('COALESCE((max(t_spt.t_kohirspt) + 1),1) as no_urut'))->whereYear('t_spt.t_tgldaftar_spt', '=', $tahun)->first();

        return $data;
    }

    public function cek_npoptkp($id)
    {
        $data = DB::table('s_tarifnpoptkp')->where('s_idjenistransaksinpoptkp', '=', $id)->first();
        return $data;
    }

    //========================================================================== CEK TARIF BPHTB

    public function cek_tarifbphtb($date)
    {
        $data = DB::table('s_tarif_bphtb')->whereRaw("s_tgl_awal::date <= '" . date('Y-m-d', strtotime($date)) . "' and s_tgl_akhir >= '" . date('Y-m-d', strtotime($date)) . "' ")->first();
        return $data;
    }

    //========================================================================== END TARIF BPHTB


    public function cek_fileupload_syarat($data_get)
    {
        $data = DB::table('t_filesyarat_bphtb')->where('t_idspt', '=', $data_get['t_idspt'])->where('s_idpersyaratan', '=', $data_get['s_idpersyaratan'])->where('s_idjenistransaksi', '=', $data_get['s_idjenistransaksi'])->get();
        return $data;
    }

    public function cek_imageupload_persyaratan($file_name)
    {
        $data = DB::table('t_filesyarat_bphtb')->where('nama_file', '=', $file_name)->get()->count();
        return $data;
    }

    public function cek_idimage_persyaratan($id)
    {
        $data = DB::table('t_filesyarat_bphtb')->where('t_id_filesyarat', '=', $id)->first();
        return $data;
    }

    public function cek_fileupload_fotoobjek($data_get)
    {
        $data = DB::table('t_file_objek')->where('t_idspt', '=', $data_get['t_idspt'])->get();
        return $data;
    }

    public function cek_imageupload_fotoobjek($file_name)
    {
        $data = DB::table('t_file_objek')->where('nama_file', '=', $file_name)->get()->count();
        return $data;
    }

    public function cek_idimage_fotoobjek($id)
    {
        $data = DB::table('t_file_objek')->where('t_idfile_objek', '=', $id)->first();
        return $data;
    }

    public function cek_all_data_syarat_dan_file_upload($idspt, $id)
    {
        $data = DB::table("s_persyaratan as a")
            ->select("a.*", "b.letak_file", "b.nama_file")
            ->leftJoin("t_filesyarat_bphtb as b", function ($join) use ($idspt) {
                $join->on("a.s_idpersyaratan", "=", "b.s_idpersyaratan");
                $join->where("b.t_idspt", "=", $idspt);
            })
            ->where("a.s_idjenistransaksi", "=", $id)->orderBy("s_idpersyaratan")->get();
        return $data;
    }

    public function cek_foto_objek($id)
    {
        $data = DB::table('t_file_objek')->where('t_file_objek.t_idspt', '=', $id)->get();
        return $data;
    }

    public function cek_jenis_tanah($id){
        $data = DB::table('s_jenistanah')
            ->where('id_jenistanah', '=', $id)
            ->first();
        return $data;
    }

    public function cek_jenis_transaksi($id)
    {
        $data = DB::table('s_jenistransaksi')->where('s_idjenistransaksi', '=', $id)->first();
        return $data;
    }

    public function cek_jenis_pengurangan($id)
    {
        $data = DB::table('s_jenispengurangan')->where('s_idpengurangan', '=', $id)->first();
        return $data;
    }

    public function get_nikbersama($idspt)
    {
        $data = DB::table('t_nik_bersama')->where('t_idspt', '=', $idspt)->get();
        return $data;
    }

    public function get_nikbersama_count($idspt)
    {
        $data = DB::table('t_nik_bersama')->where('t_idspt', '=', $idspt)->get()->count();
        return $data;
    }

    public function ceknikbersama($tahunproses, $nik = null)
    {
        if (!empty($nik)) {
            $kesatu = DB::table('t_spt')->select(DB::raw('t_spt.t_nik_pembeli as t_nikwppembeli'))->whereRaw("extract(year from t_spt.t_tgldaftar_spt) = '" . $tahunproses . "' and t_spt.t_nik_pembeli in (" . $nik . ") ");

            $kedua = DB::table('t_nik_bersama')
                ->select(DB::raw('t_nik_bersama.t_nik_bersama as t_nikwppembeli'))
                ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_nik_bersama.t_idspt')
                ->whereRaw("extract(year from t_spt.t_tgldaftar_spt) = '" . $tahunproses . "' and t_nik_bersama.t_nik_bersama in (" . $nik . ") ")
                ->union($kesatu)
                ->groupBy('t_nikwppembeli')
                ->get();
            return $kedua;
        } else {
            return 0;
        }
    }

    public function ceknikbersama_pertransaksi($tahunproses, $nik = null, $jenistransaksi = null)
    {
        if (!empty($jenistransaksi)) {
            if ($jenistransaksi == 1) {   //jual beli dkk 60jt pokoke
                $where = ' AND t_idjenistransaksi NOT IN (4,5)';
            } elseif ($jenistransaksi == 2) {   //jual beli dkk 60jt pokoke
                $where = ' AND t_idjenistransaksi IN (4,5)';
            } else {
                $where = '';
            }
        } else {
            $where = '';
        }

        if (!empty($nik)) {
            $kesatu = DB::table('t_spt')->select(
                't_spt.t_idspt',
                't_spt.t_kohirspt',
                't_spt.t_tgldaftar_spt',
                't_spt.t_periodespt',
                't_spt.t_idjenistransaksi',
                't_spt.t_idnotaris_spt',
                't_spt.t_npwpd',
                't_spt.t_idbidang_usaha',
                't_spt.t_nama_pembeli',
                't_spt.t_nik_pembeli',
                't_spt.t_npwp_pembeli',
                't_spt.t_jalan_pembeli',
                't_spt.t_kabkota_pembeli',
                't_spt.t_namakecamatan_pembeli',
                't_spt.t_namakelurahan_pembeli',
                't_spt.t_rt_pembeli',
                't_spt.t_rw_pembeli',
                't_spt.t_nohp_pembeli',
                't_spt.t_email_pembeli',
                't_spt.t_kodepos_pembeli'
            )->whereRaw("extract(year from t_spt.t_tgldaftar_spt) = '" . $tahunproses . "' and t_spt.t_nik_pembeli in (" . $nik . ") ");

            $kedua = DB::table('t_nik_bersama')->select(
                't_spt.t_idspt',
                't_spt.t_kohirspt',
                't_spt.t_tgldaftar_spt',
                't_spt.t_periodespt',
                't_spt.t_idjenistransaksi',
                't_spt.t_idnotaris_spt',
                't_spt.t_npwpd',
                't_spt.t_idbidang_usaha',
                't_spt.t_nama_pembeli',
                DB::raw('t_nik_bersama.t_nik_bersama as t_nik_pembeli'),
                't_spt.t_npwp_pembeli',
                't_spt.t_jalan_pembeli',
                't_spt.t_kabkota_pembeli',
                't_spt.t_namakecamatan_pembeli',
                't_spt.t_namakelurahan_pembeli',
                't_spt.t_rt_pembeli',
                't_spt.t_rw_pembeli',
                't_spt.t_nohp_pembeli',
                't_spt.t_email_pembeli',
                't_spt.t_kodepos_pembeli'
            )
                ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_nik_bersama.t_idspt')
                ->whereRaw("extract(year from t_spt.t_tgldaftar_spt) = '" . $tahunproses . "' and t_nik_bersama.t_nik_bersama in (" . $nik . ") " . $where . " ")
                ->union($kesatu)
                ->get();
            return $kedua;
        } else {
            return 0;
        }
    }

    public function ceknik($nik, $tahunproses)
    {
        $kesatu = DB::table('t_spt')
            ->select(
                't_spt.t_idspt',
                't_spt.t_kohirspt',
                't_spt.t_tgldaftar_spt',
                't_spt.t_periodespt',
                't_spt.t_idjenistransaksi',
                't_spt.t_idnotaris_spt',
                't_spt.t_npwpd',
                't_spt.t_idbidang_usaha',
                't_spt.t_nama_pembeli',
                't_spt.t_nik_pembeli',
                't_spt.t_npwp_pembeli',
                't_spt.t_jalan_pembeli',
                't_spt.t_kabkota_pembeli',
                't_spt.t_namakecamatan_pembeli',
                't_spt.t_namakelurahan_pembeli',
                't_spt.t_rt_pembeli',
                't_spt.t_rw_pembeli',
                't_spt.t_nohp_pembeli',
                't_spt.t_email_pembeli',
                't_spt.t_kodepos_pembeli'
            )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_inputajb', 't_spt.t_idspt', '=', 't_inputajb.t_idspt')
            ->leftJoin('t_laporbulanan_detail', 't_inputajb.t_idajb', '=', 't_laporbulanan_detail.t_idajb')
            ->leftJoin('t_laporbulanan_head', 't_laporbulanan_detail.t_idlaporbulanan', '=', 't_laporbulanan_head.t_idlaporbulanan')
            ->whereRaw("extract(year from t_spt.t_tgldaftar_spt) = '" . $tahunproses . "' and t_spt.t_nik_pembeli = '" . $nik . "' ")
            ->orderBy('t_idspt', 'asc');
        $kedua = DB::table('t_nik_bersama')->select(
            't_spt.t_idspt',
            't_spt.t_kohirspt',
            't_spt.t_tgldaftar_spt',
            't_spt.t_periodespt',
            't_spt.t_idjenistransaksi',
            't_spt.t_idnotaris_spt',
            't_spt.t_npwpd',
            't_spt.t_idbidang_usaha',
            't_spt.t_nama_pembeli',
            DB::raw('t_nik_bersama.t_nik_bersama as t_nik_pembeli'),
            't_spt.t_npwp_pembeli',
            't_spt.t_jalan_pembeli',
            't_spt.t_kabkota_pembeli',
            't_spt.t_namakecamatan_pembeli',
            't_spt.t_namakelurahan_pembeli',
            't_spt.t_rt_pembeli',
            't_spt.t_rw_pembeli',
            't_spt.t_nohp_pembeli',
            't_spt.t_email_pembeli',
            't_spt.t_kodepos_pembeli'
        )
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_nik_bersama.t_idspt')
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_inputajb', 't_spt.t_idspt', '=', 't_inputajb.t_idspt')
            ->leftJoin('t_laporbulanan_detail', 't_inputajb.t_idajb', '=', 't_laporbulanan_detail.t_idajb')
            ->leftJoin('t_laporbulanan_head', 't_laporbulanan_detail.t_idlaporbulanan', '=', 't_laporbulanan_head.t_idlaporbulanan')
            ->whereRaw("extract(year from t_spt.t_tgldaftar_spt) = '" . $tahunproses . "' and t_nik_bersama.t_nik_bersama = '" . $nik . "' ")
            ->union($kesatu)
            ->orderBy('t_idspt', 'asc')
            ->get();
        return $kedua;
    }

    public function cek_kd_kecamatan($KD_PROPINSI, $KD_DATI2, $KD_KECAMATAN)
    {
        $data = DB::table('s_kecamatan')->where('s_kd_propinsi', '=', $KD_PROPINSI)->where('s_kd_dati2', '=', $KD_DATI2)->where('s_kd_kecamatan', '=', $KD_KECAMATAN)->first();
        return $data;
    }

    public function cek_kd_kelurahan($KD_PROPINSI, $KD_DATI2, $KD_KECAMATAN, $KD_KELURAHAN)
    {
        $data = DB::table('s_kelurahan')->where('s_kd_propinsi', '=', $KD_PROPINSI)->where('s_kd_dati2', '=', $KD_DATI2)->where('s_kd_kecamatan', '=', $KD_KECAMATAN)->where('s_kd_kelurahan', '=', $KD_KELURAHAN)->first();
        return $data;
    }

    public function cek_id_validasi_kabid($id)
    {
        $data = DB::table('t_validasi_kabid')->where('t_id_validasi_kabid', '=', $id)->first();
        return $data;
    }

    public function cek_idspt_validasi_kabid($id)
    {
        $data = DB::table('t_validasi_kabid')->where('t_idspt', '=', $id)->first();
        return $data;
    }
    
    public function cek_idspt_validasi_kaban($id)
    {
        $data = DB::table('t_validasi_kaban')->where('t_idspt', '=', $id)->first();
        return $data;
    }

    public function data_pemda()
    {
        $data = DB::table('s_pemda')->where('s_idpemda', '=', 1)->first();
        return $data;
    }

    public function bikin_kodebayar($tahun)
    {
        $data = DB::table('t_spt')->select(DB::raw('COALESCE(max(t_nourut_kodebayar)+1,1) as t_nourut_kodebayar'))->whereYear('t_tglbuat_kodebyar', '=', $tahun)->first();
        return $data;
    }

    public function cek_kodebayar_all($kodebayar)
    {
        $data = DB::table('t_spt')->select(DB::raw('count(t_spt.t_idspt) as jml_kd_bayar'))->where('t_kodebayar_bphtb', '=', $kodebayar)->first();
        return $data;
    }

    public function cek_kodebayar_all_bikin($kodebayar, $tahun)
    {
        $data = DB::table('t_spt')->select('t_idspt', DB::raw("(ROW_NUMBER () OVER (ORDER BY t_nourut_kodebayar) + (select max(t_nourut_kodebayar) from t_spt where extract(year from t_tglbuat_kodebyar) = '" . $tahun . "')) as row_urut"))->where('t_kodebayar_bphtb', '=', $kodebayar)->get(); //->whereYear('t_tglbuat_kodebyar', '=', $tahun)->get();
        return $data;
    }

    public function cek_transaksi_simpan_wp($kodebayar)
    {
        $data = DB::table('t_spt')->where('t_kodebayar_bphtb', '=', $kodebayar)->orderBy('t_spt.t_idspt', 'asc')->first();
        return $data;
    }

    public function looping_cek_kodebayar_all($kodebayar, $tahun, $jenisketetapan)
    {
        $cek_pemda = $this->data_pemda();
        $res2 = $this->cek_transaksi_simpan_wp($kodebayar);


        $cek_data_all_kodebayar = $this->cek_kodebayar_all($kodebayar);
        //var_dump($cek_data_all_kodebayar); exit();
        if (($cek_data_all_kodebayar->jml_kd_bayar) > 1) {

            $tahunpendataan = date('Y');
            $data_all_update_kodebayar = $this->cek_kodebayar_all_bikin($kodebayar, $tahun);

            
            foreach ($data_all_update_kodebayar as $key => $v) {
                if ($v->t_idspt == $res2->t_idspt) {
                } else {

                    $idbillingnya = $this->format_idbilling($tahun, $v->row_urut, $jenisketetapan);  
                    //$idbillingnya = $cek_pemda->s_kodekabkot . date('Y') . "10" . str_pad($v->row_urut, 5, "0", STR_PAD_LEFT);

                    //$this->update_kodebayar_all($idbillingnya, $v['row_urut'], $v['t_idtransaksi']);
                    $affected = DB::table('t_spt')
                        ->where('t_idspt', $v->t_idspt)
                        ->update([
                            't_nourut_kodebayar' => $v->row_urut,
                            't_kodebayar_bphtb' => $idbillingnya
                        ]);
                }
            }
        }

        return $res2;
    }
    
    public function format_idbilling($tahun, $nourutKodeBayar, $jenisketetapan){
        $cek_pemda = $this->data_pemda();
        
        $format_billing = $cek_pemda->s_kodekabkot . $tahun . "11" . str_pad($jenisketetapan, 2, "0", STR_PAD_LEFT). str_pad($nourutKodeBayar, 5, "0", STR_PAD_LEFT);
        
        return $format_billing;
    }
    
    public function format_ntpd($tgl, $nourutntpd, $jenisketetapan){
        $cek_pemda = $this->data_pemda();
        
        $format_ntpd = $cek_pemda->s_kodekabkot . date('Ymd', strtotime($tgl)) . str_pad($jenisketetapan, 2, "0", STR_PAD_LEFT). str_pad($nourutntpd, 3, "0", STR_PAD_LEFT);
        
        return $format_ntpd;
    }

    public function cek_idspt_pemeriksaan($id)
    {
        $data = DB::table('t_pemeriksaan')->where('t_idspt', '=', $id)->first();
        return $data;
    }

    //========================================================================== CEK MAX NO URUT PERTAHUN

    public function cek_nourut_pemeriksa_pertahun($tahun)
    {
        $data = DB::table('t_pemeriksaan')->select(DB::raw('COALESCE((max(t_pemeriksaan.t_noperiksa) + 1),1) as no_urut'))->whereYear('t_pemeriksaan.t_tglpenetapan', '=', $tahun)->first();
        return $data;
    }

    //========================================================================== END CEK MAX NO URUT PERTAHUN

    public function cekdata_notaris_all()
    {
        $data = DB::table('s_notaris')->get();
        return $data;
    }

    public function cekdata_pejabat_all()
    {
        $data = DB::table('s_pejabat')->get();
        return $data;
    }

    public function cek_id_pejabat($idkec)
    {
        $data = DB::table('s_pejabat')->where('s_idpejabat', '=', $idkec)->first();
        return $data;
    }

    public function skpdkb()
    {
        return $this->hasOne(Skpdkb::class, 't_idspt', 't_idspt');
    }
    
    public function cek_setmenukaban()
    {
        $data = DB::table('s_setmenukaban')->where('s_id_setmenukaban', '=', 1)->first();
        return $data;
    }
    
    public function cek_tahuntunggakanpbb()
    {
        $data = DB::table('s_tahuntunggakanpbb')->where('s_id_thntgkpbb', '=', 1)->first();
        return $data;
    }
    
    public function cek_setmenutunggakanpbb()
    {
        $data = DB::table('s_setmenutgkpbb')->where('s_id_setmenutgkpbb', '=', 1)->first();
        return $data;
    }
    
    public function cek_setmenubriva()
    {
        $data = DB::table('s_setmenubriva')->where('s_id_setmenubriva', '=', 1)->first();
        return $data;
    }
    
    public function cek_setkonekpbb()
    {
        $data = DB::table('s_setkonekpbb')->where('s_id_setkonekpbb', '=', 1)->first();
        return $data;
    }
    
    public function cek_ukuranfileupload()
    {
        $data = DB::table('s_menuupload')->where('s_id_menuupload', '=', 1)->first();
        return $data;
    }
    
    public function cek_setgmap()
    {
        $data = DB::table('s_setmenugmap')->where('s_id_setmenugmap', '=', 1)->first();
        return $data;
    }
    
    public function cek_setcaptcha()
    {
        $data = DB::table('s_setmenucaptcha')->where('s_id_setmenucaptcha', '=', 1)->first();
        return $data;
    }
    
    public function cek_setpelayanan($id)
    {
        $data = DB::table('s_setmenupelayanan')->where('s_id_setmenupelayanan', '=', $id)->first();
        return $data;
    }
    
   
    public function cetak_sspd_bphtb($id, $idketetapan)
    {
        if($idketetapan == 1){ //============= SSPD
            $data = DB::table('t_spt')
                ->select(
                    't_spt.*',
                    's_jenistransaksi.*',
                    't_validasi_berkas.t_id_validasi_berkas',
                    't_validasi_berkas.s_id_status_berkas',
                    't_validasi_berkas.t_tglvalidasi',
                    't_validasi_berkas.t_keterangan_berkas',
                    't_validasi_kabid.t_id_validasi_kabid',
                    't_validasi_kabid.s_id_status_kabid',
                    't_validasi_kabid.t_keterangan_kabid',
                    't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                    't_validasi_kaban.t_id_validasi_kaban',
                    't_validasi_kaban.s_id_status_kaban',
                    't_validasi_kaban.t_keterangan_kaban',
                    't_validasi_kaban.t_tglvalidasikaban',
                    't_pembayaran_bphtb.t_id_pembayaran',
                    't_pembayaran_bphtb.t_nourut_bayar',
                    't_pembayaran_bphtb.t_tglpembayaran_pokok',
                    't_pembayaran_bphtb.t_jmlhbayar_pokok',
                    't_pembayaran_bphtb.t_tglpembayaran_denda',
                    't_pembayaran_bphtb.t_jmlhbayar_denda',
                    't_pembayaran_bphtb.t_jmlhbayar_total',
                    's_jenisfasilitas.s_namajenisfasilitas',
                    's_jenishaktanah.s_namahaktanah',
                    's_jenisdoktanah.s_namadoktanah',
                    's_jenispengurangan.s_namapengurangan',
                    's_jenistanah.nama_jenistanah',
                    's_notaris.s_namanotaris',
                    't_pemeriksaan.t_idpemeriksa',
                    't_pemeriksaan.t_noperiksa',
                    't_pemeriksaan.t_idpejabat1',
                    't_pemeriksaan.t_idpejabat2',
                    't_pemeriksaan.t_idpejabat3',
                    't_pemeriksaan.t_idpejabat4',
                    't_pemeriksaan.t_keterangan',
                    't_pemeriksaan.t_luastanah_pemeriksa',
                    't_pemeriksaan.t_njoptanah_pemeriksa',
                    't_pemeriksaan.t_totalnjoptanah_pemeriksa',
                    't_pemeriksaan.t_luasbangunan_pemeriksa',
                    't_pemeriksaan.t_njopbangunan_pemeriksa',
                    't_pemeriksaan.t_totalnjopbangunan_pemeriksa',
                    't_pemeriksaan.t_grandtotalnjop_pemeriksa',
                    't_pemeriksaan.t_nilaitransaksispt_pemeriksa',
                    't_pemeriksaan.t_permeter_tanah_pemeriksa',
                    't_pemeriksaan.t_idtarifbphtb_pemeriksa',
                    't_pemeriksaan.t_persenbphtb_pemeriksa',
                    't_pemeriksaan.t_npop_bphtb_pemeriksa',
                    't_pemeriksaan.t_npoptkp_bphtb_pemeriksa',
                    't_pemeriksaan.t_npopkp_bphtb_pemeriksa',
                    't_pemeriksaan.t_nilaibphtb_pemeriksa',
                    't_pemeriksaan.t_idpersetujuan_pemeriksa',
                    's_status_berkas.s_nama_status_berkas',
                    's_status_kabid.s_nama_status_kabid',
                    't_validasi_berkas.t_persyaratan_validasi_berkas',
                    'users.username as username_kabid',
                    's_pejabat.s_namapejabat as namapejabat_kabid',
                    's_pejabat.s_nippejabat as nippejabat_kabid',
                    'b.s_namapejabat as namapejabat_kaban',
                    'b.s_nippejabat as nippejabat_kaban'   
                )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_validasi_kaban', 't_spt.t_idspt', '=', 't_validasi_kaban.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
            ->leftJoin('s_status_berkas', 't_validasi_berkas.s_id_status_berkas', '=', 's_status_berkas.s_id_status_berkas')
            ->leftJoin('s_status_kabid', 't_validasi_kabid.s_id_status_kabid', '=', 's_status_kabid.s_id_status_kabid')
            ->leftJoin('users', 'users.id', '=', 't_validasi_kabid.t_iduser_validasi')    
            ->leftJoin('s_pejabat', 's_pejabat.s_idpejabat', '=', 'users.s_idpejabat')      
            ->leftJoin('users as a', 'a.id', '=', 't_validasi_kaban.t_iduser_validasikaban')    
            ->leftJoin('s_pejabat as b', 'b.s_idpejabat', '=', 'a.s_idpejabat')
            ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')           
            ->where('t_spt.t_idspt', '=', $id)->first();

        }elseif($idketetapan == 2){ //============= SKPDKB
            $data = DB::table('t_skpdkb')
                ->select(
                    't_skpdkb.t_kodebayar_skpdkb as t_kodebayar_bphtb',
                    't_spt.t_nama_pembeli',    
                    't_spt.t_npwp_pembeli',   
                    't_spt.t_jalan_pembeli',    
                    't_spt.t_namakelurahan_pembeli', 
                    't_spt.t_rt_pembeli', 
                    't_spt.t_rw_pembeli', 
                    't_spt.t_namakecamatan_pembeli', 
                    't_spt.t_kabkota_pembeli', 
                    't_spt.t_kodepos_pembeli', 
                    't_skpdkb.t_nop_sppt',   
                    't_spt.t_jalan_sppt', 
                    't_spt.t_kelurahan_sppt', 
                    't_spt.t_rt_sppt',     
                    't_spt.t_rw_sppt',
                    't_spt.t_kecamatan_sppt',
                    't_spt.t_kabkota_sppt',
                    't_skpdkb.t_tahun_sppt as t_periodespt', 
                    't_skpdkb.t_luastanah_skpdkb as t_luastanah',
                    't_skpdkb.t_njoptanah_skpdkb as t_njoptanah',
                    't_skpdkb.t_totalnjoptanah_skpdkb as t_totalnjoptanah',    
                    't_skpdkb.t_luasbangunan_skpdkb as t_luasbangunan',    
                    't_skpdkb.t_njopbangunan_skpdkb as t_njopbangunan',    
                    't_skpdkb.t_totalnjopbangunan_skpdkb as t_totalnjopbangunan',    
                    't_skpdkb.t_grandtotalnjop_skpdkb as t_grandtotalnjop',    
                    't_skpdkb.t_nilaitransaksispt_skpdkb as t_nilaitransaksispt',    
                    't_spt.t_idjenistransaksi',    
                    't_skpdkb.t_trf_aphb_kali_skpdkb as t_tarif_pembagian_aphb_kali',        
                    't_skpdkb.t_trf_aphb_bagi_skpdkb as t_tarif_pembagian_aphb_bagi',    
                    't_skpdkb.t_npop_bphtb_skpdkb as t_npop_bphtb',    
                    't_spt.t_nosertifikathaktanah',    
                    't_skpdkb.t_npoptkp_bphtb_skpdkb as t_npoptkp_bphtb',    
                    't_skpdkb.t_npopkp_bphtb_skpdkb as t_npopkp_bphtb',    
                    't_skpdkb.t_persenbphtb_skpdkb as t_persenbphtb',    
                    't_skpdkb.t_jmlh_totalskpdkb as t_nilai_bphtb_fix',    
                    't_skpdkb.t_tglpenetapan as t_tgldaftar_spt',    
                        
                    't_spt.t_kohirspt',    
                    't_skpdkb.t_ntpd_skpdkb as t_ntpd',   
                    't_skpdkb.t_idjenisketetapan',     
                        
                    's_jenistransaksi.*',
                    't_validasi_berkas.t_id_validasi_berkas',
                    't_validasi_berkas.s_id_status_berkas',
                    't_validasi_berkas.t_tglvalidasi',
                    't_validasi_berkas.t_keterangan_berkas',
                    't_validasi_kabid.t_id_validasi_kabid',
                    't_validasi_kabid.s_id_status_kabid',
                    't_validasi_kabid.t_keterangan_kabid',
                    't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                    't_validasi_kaban.t_id_validasi_kaban',
                    't_validasi_kaban.s_id_status_kaban',
                    't_validasi_kaban.t_keterangan_kaban',
                    't_validasi_kaban.t_tglvalidasikaban',
                    't_pembayaran_bphtb.t_id_pembayaran',
                    't_pembayaran_bphtb.t_nourut_bayar',
                    't_pembayaran_bphtb.t_tglpembayaran_pokok',
                    't_pembayaran_bphtb.t_jmlhbayar_pokok',
                    't_pembayaran_bphtb.t_tglpembayaran_denda',
                    't_pembayaran_bphtb.t_jmlhbayar_denda',
                    't_pembayaran_bphtb.t_jmlhbayar_total',
                    's_jenisfasilitas.s_namajenisfasilitas',
                    's_jenishaktanah.s_namahaktanah',
                    's_jenisdoktanah.s_namadoktanah',
                    's_jenispengurangan.s_namapengurangan',
                    's_jenistanah.nama_jenistanah',
                    's_notaris.s_namanotaris',
                    't_pemeriksaan.t_idpemeriksa',
                    't_pemeriksaan.t_noperiksa',
                    't_pemeriksaan.t_idpejabat1',
                    't_pemeriksaan.t_idpejabat2',
                    't_pemeriksaan.t_idpejabat3',
                    't_pemeriksaan.t_idpejabat4',
                    't_pemeriksaan.t_keterangan',
                    't_pemeriksaan.t_luastanah_pemeriksa',
                    't_pemeriksaan.t_njoptanah_pemeriksa',
                    't_pemeriksaan.t_totalnjoptanah_pemeriksa',
                    't_pemeriksaan.t_luasbangunan_pemeriksa',
                    't_pemeriksaan.t_njopbangunan_pemeriksa',
                    't_pemeriksaan.t_totalnjopbangunan_pemeriksa',
                    't_pemeriksaan.t_grandtotalnjop_pemeriksa',
                    't_pemeriksaan.t_nilaitransaksispt_pemeriksa',
                    't_pemeriksaan.t_permeter_tanah_pemeriksa',
                    't_pemeriksaan.t_idtarifbphtb_pemeriksa',
                    't_pemeriksaan.t_persenbphtb_pemeriksa',
                    't_pemeriksaan.t_npop_bphtb_pemeriksa',
                    't_pemeriksaan.t_npoptkp_bphtb_pemeriksa',
                    't_pemeriksaan.t_npopkp_bphtb_pemeriksa',
                    't_pemeriksaan.t_nilaibphtb_pemeriksa',
                    't_pemeriksaan.t_idpersetujuan_pemeriksa',
                    's_status_berkas.s_nama_status_berkas',
                    's_status_kabid.s_nama_status_kabid',
                    't_validasi_berkas.t_persyaratan_validasi_berkas',
                    'users.username as username_kabid',
                    's_pejabat.s_namapejabat as namapejabat_kabid',
                    's_pejabat.s_nippejabat as nippejabat_kabid',
                    'b.s_namapejabat as namapejabat_kaban',
                    'b.s_nippejabat as nippejabat_kaban'   
                )
            ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_skpdkb.t_idspt')        
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_validasi_kaban', 't_spt.t_idspt', '=', 't_validasi_kaban.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
            ->leftJoin('s_status_berkas', 't_validasi_berkas.s_id_status_berkas', '=', 's_status_berkas.s_id_status_berkas')
            ->leftJoin('s_status_kabid', 't_validasi_kabid.s_id_status_kabid', '=', 's_status_kabid.s_id_status_kabid')
            ->leftJoin('users', 'users.id', '=', 't_validasi_kabid.t_iduser_validasi')    
            ->leftJoin('s_pejabat', 's_pejabat.s_idpejabat', '=', 'users.s_idpejabat')      
            ->leftJoin('users as a', 'a.id', '=', 't_validasi_kaban.t_iduser_validasikaban')    
            ->leftJoin('s_pejabat as b', 'b.s_idpejabat', '=', 'a.s_idpejabat')  
            ->leftJoin('s_jenisketetapan', 't_skpdkb.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')        
            ->where('t_spt.t_idspt', '=', $id)->first();
        }else{
            $data = DB::table('t_spt')
                ->select(
                    't_spt.*',
                    's_jenistransaksi.*',
                    't_validasi_berkas.t_id_validasi_berkas',
                    't_validasi_berkas.s_id_status_berkas',
                    't_validasi_berkas.t_tglvalidasi',
                    't_validasi_berkas.t_keterangan_berkas',
                    't_validasi_kabid.t_id_validasi_kabid',
                    't_validasi_kabid.s_id_status_kabid',
                    't_validasi_kabid.t_keterangan_kabid',
                    't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                    't_validasi_kaban.t_id_validasi_kaban',
                    't_validasi_kaban.s_id_status_kaban',
                    't_validasi_kaban.t_keterangan_kaban',
                    't_validasi_kaban.t_tglvalidasikaban',
                    't_pembayaran_bphtb.t_id_pembayaran',
                    't_pembayaran_bphtb.t_nourut_bayar',
                    't_pembayaran_bphtb.t_tglpembayaran_pokok',
                    't_pembayaran_bphtb.t_jmlhbayar_pokok',
                    't_pembayaran_bphtb.t_tglpembayaran_denda',
                    't_pembayaran_bphtb.t_jmlhbayar_denda',
                    't_pembayaran_bphtb.t_jmlhbayar_total',
                    's_jenisfasilitas.s_namajenisfasilitas',
                    's_jenishaktanah.s_namahaktanah',
                    's_jenisdoktanah.s_namadoktanah',
                    's_jenispengurangan.s_namapengurangan',
                    's_jenistanah.nama_jenistanah',
                    's_notaris.s_namanotaris',
                    't_pemeriksaan.t_idpemeriksa',
                    't_pemeriksaan.t_noperiksa',
                    't_pemeriksaan.t_idpejabat1',
                    't_pemeriksaan.t_idpejabat2',
                    't_pemeriksaan.t_idpejabat3',
                    't_pemeriksaan.t_idpejabat4',
                    't_pemeriksaan.t_keterangan',
                    't_pemeriksaan.t_luastanah_pemeriksa',
                    't_pemeriksaan.t_njoptanah_pemeriksa',
                    't_pemeriksaan.t_totalnjoptanah_pemeriksa',
                    't_pemeriksaan.t_luasbangunan_pemeriksa',
                    't_pemeriksaan.t_njopbangunan_pemeriksa',
                    't_pemeriksaan.t_totalnjopbangunan_pemeriksa',
                    't_pemeriksaan.t_grandtotalnjop_pemeriksa',
                    't_pemeriksaan.t_nilaitransaksispt_pemeriksa',
                    't_pemeriksaan.t_permeter_tanah_pemeriksa',
                    't_pemeriksaan.t_idtarifbphtb_pemeriksa',
                    't_pemeriksaan.t_persenbphtb_pemeriksa',
                    't_pemeriksaan.t_npop_bphtb_pemeriksa',
                    't_pemeriksaan.t_npoptkp_bphtb_pemeriksa',
                    't_pemeriksaan.t_npopkp_bphtb_pemeriksa',
                    't_pemeriksaan.t_nilaibphtb_pemeriksa',
                    't_pemeriksaan.t_idpersetujuan_pemeriksa',
                    's_status_berkas.s_nama_status_berkas',
                    's_status_kabid.s_nama_status_kabid',
                    't_validasi_berkas.t_persyaratan_validasi_berkas',
                    'users.username as username_kabid',
                    's_pejabat.s_namapejabat as namapejabat_kabid',
                    's_pejabat.s_nippejabat as nippejabat_kabid',
                    'b.s_namapejabat as namapejabat_kaban',
                    'b.s_nippejabat as nippejabat_kaban'   
                )
            ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
            ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
            ->leftJoin('t_validasi_kaban', 't_spt.t_idspt', '=', 't_validasi_kaban.t_idspt')
            ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
            ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
            ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
            ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
            ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
            ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
            ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
            ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
            ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
            ->leftJoin('s_status_berkas', 't_validasi_berkas.s_id_status_berkas', '=', 's_status_berkas.s_id_status_berkas')
            ->leftJoin('s_status_kabid', 't_validasi_kabid.s_id_status_kabid', '=', 's_status_kabid.s_id_status_kabid')
            ->leftJoin('users', 'users.id', '=', 't_validasi_kabid.t_iduser_validasi')    
            ->leftJoin('s_pejabat', 's_pejabat.s_idpejabat', '=', 'users.s_idpejabat')      
            ->leftJoin('users as a', 'a.id', '=', 't_validasi_kaban.t_iduser_validasikaban')    
            ->leftJoin('s_pejabat as b', 'b.s_idpejabat', '=', 'a.s_idpejabat')          
            ->where('t_spt.t_idspt', '=', $id)->first();
        }
        
        
        return $data;
    }
    
}
