<?php

namespace App\Models\Roles;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Roles extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 'roles';
    protected $primaryKey = 'id';
    protected $fillable = ['name', 'guard_name'];
    protected static $logAttributes = ['name', 'guard_name'];
}
