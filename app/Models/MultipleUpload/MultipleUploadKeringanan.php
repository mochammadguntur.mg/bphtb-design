<?php

namespace App\Models\MultipleUpload;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class MultipleUploadKeringanan extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_file_keringanan';
    protected $primaryKey = 't_idfile_keringanan';
    protected $fillable = ['t_idspt', 't_idkeringanan', 't_iduser_upload', 'letak_file', 'nama_file', 't_tgl_upload', 't_keterangan_file'];
    protected static $logAttributes = ['t_idspt', 't_idkeringanan', 't_iduser_upload', 'letak_file', 'nama_file', 't_tgl_upload', 't_keterangan_file'];
}
