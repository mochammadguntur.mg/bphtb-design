<?php

namespace App\Models\ValidasiKaban;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use App\Models\Spt\Spt;

class ValidasiKaban extends Model {

    use HasFactory,
        LogsActivity;

    protected $table = 't_validasi_kaban';
    protected $primaryKey = 't_id_validasi_kaban';
    protected $fillable = ['t_idspt', 's_id_status_kaban', 't_tglvalidasikaban', 't_iduser_validasikaban', 't_keterangan_kaban'];
    protected static $logAttributes = ['t_idspt', 's_id_status_kaban', 't_tglvalidasikaban', 't_iduser_validasikaban', 't_keterangan_kaban'];

    public function datagridValidasiKabanBelum() {
        $response = (new Spt())::select(
                        't_spt.*',
                        's_jenistransaksi.s_namajenistransaksi',
                        's_notaris.s_namanotaris',
                        's_jenis_bidangusaha.s_nama_bidangusaha',
                        't_validasi_berkas.t_id_validasi_berkas',
                        't_validasi_berkas.s_id_status_berkas',
                        't_validasi_berkas.t_tglvalidasi',
                        't_validasi_kabid.t_id_validasi_kabid',
                        't_validasi_kabid.s_id_status_kabid',
                        't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                        't_validasi_kaban.t_tglvalidasikaban',
                        't_pembayaran_bphtb.t_id_pembayaran',
                        't_pembayaran_bphtb.t_tglpembayaran_pokok',
                        't_pemeriksaan.t_idpemeriksa',
                        't_pemeriksaan.t_tglpenetapan as t_tglpemeriksaan',
                        's_jenisketetapan.s_namajenisketetapan',
                        's_jenisketetapan.s_namasingkatjenisketetapan',
                        's_jenisfasilitas.s_namajenisfasilitas',
                        's_jenishaktanah.s_namahaktanah',
                        's_jenisdoktanah.s_namadoktanah',
                        's_jenispengurangan.s_namapengurangan',
                        's_jenistanah.nama_jenistanah'
                )
                ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                ->leftJoin('t_validasi_kaban', 't_spt.t_idspt', '=', 't_validasi_kaban.t_idspt')
                ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
                ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
                ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
                ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
                ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
                ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
                ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
                ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
                ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
                ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
                ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
                ->whereNotNull('t_validasi_kabid.t_id_validasi_kabid')
                ->whereNull('t_validasi_kaban.t_id_validasi_kaban')
                ->where('s_id_status_kabid', '=', 1)
                ->orderBy('t_spt.t_idspt', 'desc')
        ;
        return $response;
    }

    public function datagridValidasiKabanSudah() {
        $response = (new Spt())::select('t_spt.*',
                        's_jenistransaksi.s_namajenistransaksi',
                        's_notaris.s_namanotaris',
                        's_jenis_bidangusaha.s_nama_bidangusaha',
                        't_validasi_berkas.t_id_validasi_berkas',
                        't_validasi_berkas.s_id_status_berkas',
                        't_validasi_berkas.t_tglvalidasi',
                        't_validasi_kabid.t_id_validasi_kabid',
                        't_validasi_kabid.s_id_status_kabid',
                        't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                        't_validasi_kaban.t_tglvalidasikaban',
                        't_pemeriksaan.t_idpemeriksa',
                        't_pemeriksaan.t_tglpenetapan as t_tglpemeriksaan',
                        't_pembayaran_bphtb.t_id_pembayaran',
                        't_pembayaran_bphtb.t_tglpembayaran_pokok',
                        's_jenisketetapan.s_namajenisketetapan',
                        's_jenisketetapan.s_namasingkatjenisketetapan',
                        's_jenisfasilitas.s_namajenisfasilitas',
                        's_jenishaktanah.s_namahaktanah',
                        's_jenisdoktanah.s_namadoktanah',
                        's_jenispengurangan.s_namapengurangan',
                        's_jenistanah.nama_jenistanah'
                )
                ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                ->leftJoin('t_validasi_kaban', 't_spt.t_idspt', '=', 't_validasi_kaban.t_idspt')
                ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
                ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
                ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
                ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
                ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
                ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
                ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
                ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
                ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
                ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
                ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
                ->whereNotNull('t_validasi_kabid.t_id_validasi_kabid')
                ->whereNotNull('t_validasi_kaban.t_id_validasi_kaban')
                ->orderBy('t_spt.t_idspt', 'desc')
        ;
        return $response;
    }

    public function datagridValidasiKabanStatus($idstatus) {
        $response = (new Spt())::select('t_spt.*',
                        's_jenistransaksi.s_namajenistransaksi',
                        's_notaris.s_namanotaris',
                        's_jenis_bidangusaha.s_nama_bidangusaha',
                        't_validasi_berkas.t_id_validasi_berkas', 't_validasi_berkas.s_id_status_berkas',
                        't_validasi_kabid.t_id_validasi_kabid', 't_validasi_kabid.s_id_status_kabid',
                        't_pembayaran_bphtb.t_id_pembayaran', 't_pembayaran_bphtb.t_tglpembayaran_pokok',
                        's_jenisketetapan.s_namajenisketetapan', 's_jenisketetapan.s_namasingkatjenisketetapan',
                        's_jenisfasilitas.s_namajenisfasilitas',
                        's_jenishaktanah.s_namahaktanah',
                        's_jenisdoktanah.s_namadoktanah',
                        's_jenispengurangan.s_namapengurangan',
                        's_jenistanah.nama_jenistanah'
                )
                ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                ->leftJoin('t_validasi_kaban', 't_spt.t_idspt', '=', 't_validasi_kaban.t_idspt')
                ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
                ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
                ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
                ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
                ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
                ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
                ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
                ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
                ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
                ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
                ->whereNotNull('t_validasi_kabid.t_id_validasi_kabid')
                ->whereNotNull('t_validasi_kaban.t_id_validasi_kaban')
                ->where('s_id_status_kaban', '=', $idstatus)
                ->orderBy('t_spt.t_idspt', 'desc')
        ;
        return $response;
    }

}
