<?php

namespace App\Models\ValidasiKabid;

use App\Models\Pemeriksaan\Pemeriksaan;
use App\Models\Spt\Spt;
use App\Models\Setting\Acuan;
use App\Models\Setting\AcuanTanah;
use App\Models\Setting\JenisTanah;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Stringable;

class ValidasiKabid extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_validasi_kabid';
    protected $primaryKey = 't_id_validasi_kabid';
    protected $fillable = ['t_idspt', 's_id_status_kabid', 't_tglvalidasi', 't_iduser_validasi', 't_keterangan_kabid'];
    protected static $logAttributes = ['t_idspt', 's_id_status_kabid', 't_tglvalidasi', 't_iduser_validasi', 't_keterangan_kabid'];

    public function datagridValidasiKabidBelum(){
        $response = (new Spt())::select(
            't_spt.*',
            's_jenistransaksi.s_namajenistransaksi',
            's_notaris.s_namanotaris',
            's_jenis_bidangusaha.s_nama_bidangusaha',
            't_validasi_berkas.t_id_validasi_berkas',
            't_validasi_berkas.s_id_status_berkas',
            't_validasi_berkas.t_tglvalidasi',
            't_validasi_kabid.t_id_validasi_kabid',
            't_validasi_kabid.s_id_status_kabid',
            't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
            't_pembayaran_bphtb.t_id_pembayaran',
            't_pembayaran_bphtb.t_tglpembayaran_pokok',
            't_pemeriksaan.t_idpemeriksa',
            't_pemeriksaan.t_tglpenetapan as t_tglpemeriksaan',
            's_jenisketetapan.s_namajenisketetapan',
            's_jenisketetapan.s_namasingkatjenisketetapan',
            's_jenisfasilitas.s_namajenisfasilitas',
            's_jenishaktanah.s_namahaktanah',
            's_jenisdoktanah.s_namadoktanah',
            's_jenispengurangan.s_namapengurangan',
            's_jenistanah.nama_jenistanah'
        )
        ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
        ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
        ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
        ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
        ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
        ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
        ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
        ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
        ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
        ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
        ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
        ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
        ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
        ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')->whereNull('t_validasi_kabid.t_id_validasi_kabid')
        ->where('s_id_status_berkas', '=', 1)
        ->orderBy('t_spt.t_idspt', 'desc')
        ;
        return $response;
    }

    public function datagridValidasiKabidSudah(){
        $response = (new Spt())::select('t_spt.*',
                        's_jenistransaksi.s_namajenistransaksi',
                        's_notaris.s_namanotaris',
                        's_jenis_bidangusaha.s_nama_bidangusaha',
                        't_validasi_berkas.t_id_validasi_berkas',
                        't_validasi_berkas.s_id_status_berkas',
                        't_validasi_berkas.t_tglvalidasi',
                        't_validasi_kabid.t_id_validasi_kabid',
                        't_validasi_kabid.s_id_status_kabid',
                        't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                        't_pemeriksaan.t_idpemeriksa',
                        't_pemeriksaan.t_tglpenetapan as t_tglpemeriksaan',
                        't_pembayaran_bphtb.t_id_pembayaran',
                        't_pembayaran_bphtb.t_tglpembayaran_pokok',
                        's_jenisketetapan.s_namajenisketetapan',
                        's_jenisketetapan.s_namasingkatjenisketetapan',
                        's_jenisfasilitas.s_namajenisfasilitas',
                        's_jenishaktanah.s_namahaktanah',
                        's_jenisdoktanah.s_namadoktanah',
                        's_jenispengurangan.s_namapengurangan',
                        's_jenistanah.nama_jenistanah'
                    )
                    ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                    ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                    ->leftJoin('t_pemeriksaan', 't_spt.t_idspt', '=', 't_pemeriksaan.t_idspt')
                    ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                    ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
                    ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
                    ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
                    ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
                    ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
                    ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
                    ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
                    ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
                    ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
                    ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')->whereNotNull('t_validasi_kabid.t_id_validasi_kabid')
                    ->orderBy('t_spt.t_idspt', 'desc')
                ;
        return $response;
    }

    public function datagridValidasiKabidStatus($idstatus){
        $response = (new Spt())::select('t_spt.*',
                        's_jenistransaksi.s_namajenistransaksi',
                        's_notaris.s_namanotaris',
                        's_jenis_bidangusaha.s_nama_bidangusaha',
                        't_validasi_berkas.t_id_validasi_berkas','t_validasi_berkas.s_id_status_berkas',
                        't_validasi_kabid.t_id_validasi_kabid','t_validasi_kabid.s_id_status_kabid',
                        't_pembayaran_bphtb.t_id_pembayaran','t_pembayaran_bphtb.t_tglpembayaran_pokok',
                        's_jenisketetapan.s_namajenisketetapan','s_jenisketetapan.s_namasingkatjenisketetapan',
                        's_jenisfasilitas.s_namajenisfasilitas',
                        's_jenishaktanah.s_namahaktanah',
                        's_jenisdoktanah.s_namadoktanah',
                        's_jenispengurangan.s_namapengurangan',
                        's_jenistanah.nama_jenistanah'
                    )
                    ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                    ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                    ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                    ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
                    ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
                    ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
                    ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
                    ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
                    ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
                    ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
                    ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
                    ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
                    ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')->whereNotNull('t_validasi_kabid.t_id_validasi_kabid')
                    ->where('s_id_status_kabid', '=', $idstatus)
                    ->orderBy('t_spt.t_idspt', 'desc')
                ;
        return $response;
    }

    public function getPermeter(){
        $res = (new Spt())::select('t_spt.t_idspt as t_idspt,
            t_spt.t_nop_sppt as t_nop_sppt,
            CASE
                WHEN t_pemeriksaan.t_permeter_tanah_pemeriksa is null THEN t_spt.t_permeter_tanah
                ELSE t_pemeriksaan.t_permeter_tanah_pemeriksa
            END as t_permeter_tanah')
            ->leftjoin('t_pemeriksaan', 't_pemeriksaan.t_idspt', '=', 't_spt.t_idspt');
        return $res;
    }

    public function analisaSistemTransaksiMin($nop){
        // $res = (new Spt())::select('t_spt.t_idspt as t_idspt,
        // t_spt.t_nop_sppt as t_nop_sppt,
        // CASE
        //     WHEN t_pemeriksaan.t_permeter_tanah_pemeriksa is null THEN t_spt.t_permeter_tanah
        //     ELSE t_pemeriksaan.t_permeter_tanah_pemeriksa
        // END as t_permeter_tanah')
        // ->leftjoin('t_pemeriksaan', 't_pemeriksaan.t_idspt', '=', 't_spt.t_idspt')
        $res = $this->getPermeter()
        ->where('t_spt.t_nop_sppt', 'like', substr($nop, 0, 17) . '%')->min('t_permeter_tanah'); //t_nilaitransaksispt //t_npop_bphtb
        return $res;
    }
    
    public function analisaSistemTransaksiAvg($nop){
        $res = $this->getPermeter()->where('t_spt.t_nop_sppt', 'like', substr($nop, 0, 17) . '%')->avg('t_permeter_tanah');
        // $res = Spt::where('t_nop_sppt', 'like', substr($nop, 0, 17) . '%')->avg('t_npop_bphtb');
        return $res;
    }
    
    public function analisaSistemTransaksiMax($nop){
        $res =  $this->getPermeter()->where('t_spt.t_nop_sppt', 'like', substr($nop, 0, 17) . '%')->max('t_permeter_tanah');
        // $res = Spt::where('t_nop_sppt', 'like', substr($nop, 0, 17) . '%')->max('t_npop_bphtb');
        return $res;
    }

    public function analisaSistemAcuanBiasa($nop){
        $KD_PROV    = substr($nop, 0, 2);
        $KD_DATI2   = substr($nop, 3, 2);
        $KD_KEC     = substr($nop, 6, 3);
        $KD_KEL     = substr($nop, 10, 3);
        $KD_BLOK    = substr($nop, 14, 3);
        $res = Acuan::select()
            ->where('s_kd_propinsi', 'like', $KD_PROV)
            ->where('s_kd_dati2', 'like', $KD_DATI2)
            ->where('s_kd_kecamatan', 'like', $KD_KEC)
            ->where('s_kd_kelurahan', 'like', $KD_KEL)
            ->where('s_kd_blok', 'like', $KD_BLOK)->get();
        return $res;
    }

    public function analisaSistemAcuan($nop, $jenistanah){
        // dd($jenistanah);
        $KD_PROV    = substr($nop, 0, 2);
        $KD_DATI2   = substr($nop, 3, 2);
        $KD_KEC     = substr($nop, 6, 3);
        $KD_KEL     = substr($nop, 10, 3);
        $KD_BLOK    = substr($nop, 14, 3);
        $jenistanahe = (array)$jenistanah;
        $res = AcuanTanah::select()
            ->where('s_kd_propinsi', 'like', $KD_PROV)
            ->where('s_kd_dati2', 'like', $KD_DATI2)
            ->where('s_kd_kecamatan', 'like', $KD_KEC)
            ->where('s_kd_kelurahan', 'like', $KD_KEL)
            ->where('s_kd_blok', 'like', $KD_BLOK)
            ->where('id_jenistanah', '=', $jenistanahe)
            ->get();
        return $res;
    }

    public function prasentaseMin($prasmin){
        $res = DB::table('s_presentase_wajar')
        ->where('s_nilaimin','<=',$prasmin)
        ->where('s_nilaimax','>=',$prasmin)
        ->get();
        return $res;
    }
    public function prasentaseAvg($prasavg){
        $res = DB::table('s_presentase_wajar')
        ->where('s_nilaimin','<=',$prasavg)
        ->where('s_nilaimax','>=',$prasavg)
        ->get();
        return $res;
    }
    public function prasentaseMax($prasmax){
        $res = DB::table('s_presentase_wajar')
        ->where('s_nilaimin','<=',$prasmax)
        ->where('s_nilaimax','>=',$prasmax)
        ->get();
        return $res;
    }
    public function prasentaseAcuan($prasacuan){
        $res = DB::table('s_presentase_wajar')
        ->where('s_nilaimin','<=',$prasacuan)
        ->where('s_nilaimax','>=',$prasacuan)
        ->get();
        return $res;
    }

}