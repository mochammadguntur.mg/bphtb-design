<?php

namespace App\Models\Pelaporan;

use App\Models\Spt\Spt;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;

class InputAjb extends Model {

    use HasFactory,
        LogsActivity;

    protected $table = 't_inputajb';
    protected $primaryKey = 't_idajb';
    protected $fillable = ['t_idspt', 't_tgl_ajb', 't_no_ajb', 't_iduser_input'];
    protected static $logAttributes = ['t_idspt', 't_tgl_ajb', 't_no_ajb', 't_iduser_input'];

    public function spt() {
        return $this->belongsTo(Spt::class, 't_idspt');
    }

    public function laporBulanans() {
        return $this->belongsToMany(LaporBulananHead::class, 't_laporbulanan_detail', 't_idajb', 't_idlaporbulanan')->withTimestamps();
    }

    public function datagridBelumLapor($id_notaris) {
        $response = new Spt();
        $response = $response->select(
                        't_spt.*',
                        's_jenistransaksi.*',
                        's_notaris.s_namanotaris',
                        's_jenis_bidangusaha.s_nama_bidangusaha',
                        't_validasi_berkas.t_id_validasi_berkas',
                        't_validasi_berkas.s_id_status_berkas',
                        't_validasi_berkas.t_tglvalidasi',
                        't_validasi_berkas.t_keterangan_berkas',
                        't_validasi_kabid.t_id_validasi_kabid',
                        't_validasi_kabid.s_id_status_kabid',
                        't_validasi_kabid.t_keterangan_kabid',
                        't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                        't_pembayaran_bphtb.t_id_pembayaran',
                        't_pembayaran_bphtb.t_nourut_bayar',
                        't_pembayaran_bphtb.t_tglpembayaran_pokok',
                        't_pembayaran_bphtb.t_jmlhbayar_pokok',
                        't_pembayaran_bphtb.t_tglpembayaran_denda',
                        't_pembayaran_bphtb.t_jmlhbayar_denda',
                        't_pembayaran_bphtb.t_jmlhbayar_total',
                        's_jenisfasilitas.s_namajenisfasilitas',
                        's_jenishaktanah.s_namahaktanah',
                        's_jenisdoktanah.s_namadoktanah',
                        's_jenispengurangan.s_namapengurangan',
                        's_jenistanah.nama_jenistanah',
                        't_skpdkb.t_idskpdkb',
                        't_skpdkb.t_jmlh_totalskpdkb',
                        DB::raw('( CASE WHEN t_skpdkb.t_idskpdkb IS NOT NULL THEN t_skpdkb.t_idjenisketetapan ELSE t_spt.t_idjenisketetapan END ) AS jenis_ketetapan')
                )
                ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
                ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
                ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
                ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
                ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
                ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
                ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
                ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
                ->leftJoin('t_skpdkb', 't_spt.t_idspt', '=', 't_skpdkb.t_idspt')
                ->leftJoin('t_pembayaran_skpdkb', 't_pembayaran_skpdkb.t_idskpdkb', '=', 't_skpdkb.t_idskpdkb')
                ->leftJoin('t_inputajb', 't_spt.t_idspt', '=', 't_inputajb.t_idspt')
                ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
                ->whereNotNull('t_validasi_kabid.t_id_validasi_kabid')
                ->where('t_validasi_kabid.s_id_status_kabid', 1)
                ->whereNull('t_idajb')
                ->whereRaw('( CASE WHEN t_skpdkb.t_idskpdkb IS NOT NULL THEN t_pembayaran_skpdkb.t_tglpembayaran_skpdkb IS NOT NULL ELSE t_pembayaran_bphtb.t_tglpembayaran_pokok IS NOT NULL END )')
                ->orderBy('t_spt.t_idspt', 'desc');

                if ($id_notaris != null) {
                    $response = $response->where('t_idnotaris_spt', '=', $id_notaris);
                }

        return $response;
    }

    public function datagridSudahLapor() {
        $response = (new Spt())::select(
                        't_spt.*',
                        's_jenistransaksi.*',
                        's_notaris.s_namanotaris',
                        's_jenis_bidangusaha.s_nama_bidangusaha',
                        't_validasi_berkas.t_id_validasi_berkas',
                        't_validasi_berkas.s_id_status_berkas',
                        't_validasi_berkas.t_tglvalidasi',
                        't_validasi_berkas.t_keterangan_berkas',
                        't_validasi_kabid.t_id_validasi_kabid',
                        't_validasi_kabid.s_id_status_kabid',
                        't_validasi_kabid.t_keterangan_kabid',
                        't_validasi_kabid.t_tglvalidasi as t_tglvalidasikabid',
                        't_pembayaran_bphtb.t_id_pembayaran',
                        't_pembayaran_bphtb.t_nourut_bayar',
                        't_pembayaran_bphtb.t_tglpembayaran_pokok',
                        't_pembayaran_bphtb.t_jmlhbayar_pokok',
                        't_pembayaran_bphtb.t_tglpembayaran_denda',
                        't_pembayaran_bphtb.t_jmlhbayar_denda',
                        't_pembayaran_bphtb.t_jmlhbayar_total',
                        's_jenisketetapan.s_namajenisketetapan',
                        's_jenisketetapan.s_namasingkatjenisketetapan',
                        's_jenisfasilitas.s_namajenisfasilitas',
                        's_jenishaktanah.s_namahaktanah',
                        's_jenisdoktanah.s_namadoktanah',
                        's_jenispengurangan.s_namapengurangan',
                        's_jenistanah.nama_jenistanah',
                        't_skpdkb.t_idskpdkb',
                        't_skpdkb.t_jmlh_totalskpdkb',
                        DB::raw('( CASE WHEN t_skpdkb.t_idskpdkb IS NOT NULL THEN t_skpdkb.t_idjenisketetapan ELSE t_spt.t_idjenisketetapan END ) AS jenis_ketetapan')
                )
                ->leftJoin('t_validasi_berkas', 't_spt.t_idspt', '=', 't_validasi_berkas.t_idspt')
                ->leftJoin('t_validasi_kabid', 't_spt.t_idspt', '=', 't_validasi_kabid.t_idspt')
                ->leftJoin('t_pembayaran_bphtb', 't_spt.t_idspt', '=', 't_pembayaran_bphtb.t_idspt')
                ->leftJoin('s_jenistransaksi', 't_spt.t_idjenistransaksi', '=', 's_jenistransaksi.s_idjenistransaksi')
                ->leftJoin('s_notaris', 't_spt.t_idnotaris_spt', '=', 's_notaris.s_idnotaris')
                ->leftJoin('s_jenisfasilitas', 't_spt.t_idjenisfasilitas', '=', 's_jenisfasilitas.s_idjenisfasilitas')
                ->leftJoin('s_jenishaktanah', 't_spt.t_idjenishaktanah', '=', 's_jenishaktanah.s_idhaktanah')
                ->leftJoin('s_jenisdoktanah', 't_spt.t_idjenisdoktanah', '=', 's_jenisdoktanah.s_iddoktanah')
                ->leftJoin('s_jenispengurangan', 't_spt.t_idpengurangan', '=', 's_jenispengurangan.s_idpengurangan')
                ->leftJoin('s_jenistanah', 't_spt.t_id_jenistanah', '=', 's_jenistanah.id_jenistanah')
                ->leftJoin('s_jenis_bidangusaha', 't_spt.t_idbidang_usaha', '=', 's_jenis_bidangusaha.s_idbidang_usaha')
                ->leftJoin('s_jenisketetapan', 't_spt.t_idjenisketetapan', '=', 's_jenisketetapan.s_idjenisketetapan')
                ->leftJoin('t_skpdkb', 't_spt.t_idspt', '=', 't_skpdkb.t_idspt')
                ->leftJoin('t_pembayaran_skpdkb', 't_pembayaran_skpdkb.t_idskpdkb', '=', 't_skpdkb.t_idskpdkb')
                ->leftJoin('t_inputajb', 't_spt.t_idspt', '=', 't_inputajb.t_idspt')
                ->whereNotNull('t_validasi_berkas.t_id_validasi_berkas')
                ->whereNotNull('t_validasi_kabid.t_id_validasi_kabid')
                ->whereNotNull('t_idajb')
                // ->whereNotNull('t_pembayaran_bphtb.t_id_pembayaran')
                ->whereRaw('( CASE WHEN t_skpdkb.t_idskpdkb IS NOT NULL THEN t_pembayaran_skpdkb.t_tglpembayaran_skpdkb IS NOT NULL ELSE t_pembayaran_bphtb.t_tglpembayaran_pokok IS NOT NULL END )')
                ->orderBy('t_pembayaran_bphtb.t_tglpembayaran_pokok', 'desc');
        return $response;
    }

}
