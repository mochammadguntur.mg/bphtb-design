<?php

namespace App\Models\Pelaporan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DendaAjb extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_pen_denda_ajb_notaris';
    protected $primaryKey = 't_id_ajbdenda';
    protected $fillable = ['t_idspt', 't_idnotaris', 't_tglpenetapan', 't_nourut_ajbdenda', 't_nilai_ajbdenda', 't_iduser_buat', 's_id_status_bayar', 't_tglbayar_ajbdenda', 't_jmlhbayar_ajbdenda'];
    protected static $logAttributes = ['t_idspt', 't_idnotaris', 't_tglpenetapan', 't_nourut_ajbdenda', 't_nilai_ajbdenda', 't_iduser_buat', 's_id_status_bayar', 't_tglbayar_ajbdenda', 't_jmlhbayar_ajbdenda'];
}
