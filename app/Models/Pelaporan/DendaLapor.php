<?php

namespace App\Models\Pelaporan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class DendaLapor extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 't_pen_denda_lpor_notaris';
    protected $primaryKey = 't_id_pendenda';
    protected $fillable = ['t_idlaporbulanan', 't_idnotaris', 't_tglpenetapan', 't_nourut_pendenda', 't_nilai_pendenda', 't_iduser_buat', 's_id_status_bayar', 't_tglbayar_pendenda', 't_jmlhbayar_pendenda'];
    protected static $logAttributes = [
        't_idlaporbulanan', 
        't_idnotaris', 
        't_tglpenetapan', 
        't_nourut_pendenda', 
        't_nilai_pendenda', 
        't_iduser_buat', 
        's_id_status_bayar', 
        't_tglbayar_pendenda', 
        't_jmlhbayar_pendenda'];
}
