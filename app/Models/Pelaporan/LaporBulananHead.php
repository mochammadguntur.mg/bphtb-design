<?php

namespace App\Models\Pelaporan;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class LaporBulananHead extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 't_laporbulanan_head';
    protected $primaryKey = 't_idlaporbulanan';
    protected $fillable = ['t_no_lapor', 't_tgl_lapor', 't_untuk_bulan', 't_untuk_tahun', 't_keterangan', 't_iduser_input'];
    protected static $logAttributes = ['t_no_lapor', 't_tgl_lapor', 't_untuk_bulan', 't_untuk_tahun', 't_keterangan', 't_iduser_input'];

    public function inputAjbs()
    {
        return $this->belongsToMany(InputAjb::class, 't_laporbulanan_detail', 't_idlaporbulanan', 't_idajb')
            ->withTimestamps();
    }

    public function dendaLaporan()
    {
        return $this->hasOne(DendaLapor::class, 't_idlaporbulanan', 't_idlaporbulanan');
    }
}
