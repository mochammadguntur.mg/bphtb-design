<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Menupelayanan extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_setmenupelayanan';
    protected $primaryKey = 's_id_setmenupelayanan';

    protected $fillable = ['s_id_statusmenupelayanan', 's_ketmenupelayanan','s_namamenupelayanan'];
    protected static $logAttributes = ['s_id_statusmenupelayanan', 's_ketmenupelayanan','s_namamenupelayanan'];
}
