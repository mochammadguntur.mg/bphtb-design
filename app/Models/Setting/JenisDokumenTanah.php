<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class JenisDokumenTanah extends Model
{
    protected $table        = 's_jenisdoktanah';
    protected $primaryKey   = 's_iddoktanah';
    use HasFactory;

    protected $fillable = ['s_namadoktanah'];
}
