<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Menukonekpbb extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_setkonekpbb';
    protected $primaryKey = 's_id_setkonekpbb';

    protected $fillable = ['s_id_statuskonekpbb', 's_ketkonekpbb'];
    protected static $logAttributes = ['s_id_statuskonekpbb', 's_ketkonekpbb'];
}
