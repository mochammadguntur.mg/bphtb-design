<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Kelurahan extends Model
{
    use HasFactory, LogsActivity;
    
    protected $table    = 's_kelurahan';
    protected $primaryKey = 's_idkelurahan';
    protected $fillable = ['s_idkecamatan','s_kd_kelurahan', 's_namakelurahan', 's_kd_propinsi','s_kd_dati2','s_kd_kecamatan', 's_latitude', 's_longitude'];
    protected static $logAttributes = ['s_idkecamatan','s_kd_kelurahan', 's_namakelurahan', 's_latitude', 's_longitude'];
}
