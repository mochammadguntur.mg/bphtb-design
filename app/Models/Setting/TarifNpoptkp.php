<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TarifNpoptkp extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_tarifnpoptkp';
    protected $primaryKey = 's_idtarifnpoptkp';
    protected $fillable = ['s_idjenistransaksinpoptkp', 's_tarifnpoptkp', 's_tarifnpoptkptambahan', 's_dasarhukumnpoptkp', 's_statusnpoptkp', 's_tglberlaku_awal', 's_tglberlaku_akhir'];
    protected static $logAttributes = ['s_idjenistransaksinpoptkp', 's_tarifnpoptkp', 's_tarifnpoptkptambahan', 's_dasarhukumnpoptkp', 's_statusnpoptkp', 's_tglberlaku_awal', 's_tglberlaku_akhir'];

    public function jenisTransaksi()
    {
        return $this->belongsTo(
            JenisTransaksi::class,
            's_idjenistransaksinpoptkp',
            's_idjenistransaksi'
        );
    }
}
