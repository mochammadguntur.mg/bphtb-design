<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Persentasewajar extends Model
{
    use HasFactory, LogsActivity;
    
    protected $table    = 's_presentase_wajar';
    protected $primaryKey = 's_idpresentase_wajar';
    protected $fillable = ['s_nilaimin', 's_nilaimax', 's_ketpresentase_wajar', 's_css_warna'];
    protected static $logAttributes = ['s_nilaimin', 's_nilaimax', 's_ketpresentase_wajar', 's_css_warna'];
}
