<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Menukaban extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_setmenukaban';
    protected $primaryKey = 's_id_setmenukaban';

    protected $fillable = ['s_id_statusmenukaban', 's_ketmenukaban'];
    protected static $logAttributes = ['s_id_statusmenukaban', 's_ketmenukaban'];
}
