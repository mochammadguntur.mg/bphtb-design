<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Menuntpd extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_setmenuntpd';
    protected $primaryKey = 's_id_setmenuntpd';

    protected $fillable = ['s_id_statusmenuntpd', 's_ketmenuntpd'];
    protected static $logAttributes = ['s_id_statusmenuntpd', 's_ketmenuntpd'];
}
