<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class JenisTanah extends Model
{
    use HasFactory, LogsActivity;

    protected $table        = 's_jenistanah';
    protected $primaryKey   = 'id_jenistanah';
    protected $fillable     = ['nama_jenistanah'];
    // protected static $logAttibutes = 'nama_jenistanah';
}
