<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Menucaptcha extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_setmenucaptcha';
    protected $primaryKey = 's_id_setmenucaptcha';

    protected $fillable = ['s_id_statusmenucaptcha', 's_ketmenucaptcha'];
    protected static $logAttributes = ['s_id_statusmenucaptcha', 's_ketmenucaptcha'];
}
