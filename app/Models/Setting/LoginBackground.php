<?php

namespace App\Models\Setting;

use App\Models\Dropdown\Status;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;


class LoginBackground extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 's_background';
    protected $fillable = ['s_thumbnail', 's_id_status'];
    protected static $logAttributes = ['s_thumbnail', 's_id_status'];

    public function Status()
    {
        return $this->belongsTo(Status::class, 's_id_status', 's_id_status');
    }

    public function aktifUpdate($data){
        $data = DB::table('s_background')->where('id', '=', $data['id'])->update(['s_id_status' => $data['s_id_status']]);
        return $data;
    }
}
