<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Menubriva extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_setmenubriva';
    protected $primaryKey = 's_id_setmenubriva';

    protected $fillable = ['s_id_statusmenubriva', 's_ketmenubriva'];
    protected static $logAttributes = ['s_id_statusmenubriva', 's_ketmenubriva'];
}
