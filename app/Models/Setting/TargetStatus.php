<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TargetStatus extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 's_target_status';
    protected $primaryKey = 's_id_target_status';
    protected $fillable = ['s_nama_status'];
    protected static $logAttributes = ['s_nama_status'];

    public function targetPenerimaan()
    {
        return $this->hasMany(TargetPenerimaan::class);
    }
}
