<?php

namespace App\Models\Setting;

use App\Models\Dropdown\WajibUpload;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Persyaratan extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 's_persyaratan';
    protected $primaryKey = 's_idpersyaratan';
    protected $fillable = ['s_idjenistransaksi', 's_namapersyaratan', 'order', 's_idwajib_up', 's_lokasimenu'];
    protected static $logAttributes = ['s_idjenistransaksi', 's_namapersyaratan', 'order', 's_idwajib_up', 's_lokasimenu'];

    public function jenisTransaksi()
    {
        return $this->belongsTo(
            JenisTransaksi::class,
            's_idjenistransaksi',
            's_idjenistransaksi'
        );
    }

    public function wajibUp()
    {
        return $this->belongsTo(
            WajibUpload::class,
            's_idwajib_up',
            's_idwajib_up'
        );
    }
}
