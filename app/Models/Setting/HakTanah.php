<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class HakTanah extends Model
{
    use HasFactory, LogsActivity;
    
    protected $table        = 's_jenishaktanah';
    protected $primaryKey   = 's_idhaktanah';
    protected $fillable = ['s_kodehaktanah', 's_namahaktanah'];
    protected static $logAttributes = ['s_kodehaktanah', 's_namahaktanah'];
}
