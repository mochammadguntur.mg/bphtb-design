<?php

namespace App\Models\Setting;

use App\Models\Dropdown\Status;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TarifBphtb extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 's_tarif_bphtb';
    protected $primaryKey = 's_idtarifbphtb';
    protected $fillable = ['s_tarif_bphtb', 's_dasar_hukum', 's_tgl_awal', 's_tgl_akhir'];
    protected static $logAttributes = ['s_tarif_bphtb', 's_dasar_hukum', 's_tgl_awal', 's_tgl_akhir'];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }
}
