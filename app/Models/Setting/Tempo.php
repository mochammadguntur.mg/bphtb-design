<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Tempo extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 's_tempo';
    protected $primaryKey = 's_idtempo';
    protected $fillable = ['s_haritempo', 's_tglberlaku_awal', 's_tglberlaku_akhir', 's_id_status'];
    protected static $LogAttributes = ['s_haritempo', 's_tglberlaku_awal', 's_tglberlaku_akhir', 's_id_status'];
}
