<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Pejabat extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_pejabat';
    protected $primaryKey = 's_idpejabat';

    protected $fillable = ['s_namapejabat', 's_jabatanpejabat', 's_nippejabat', 's_golonganpejabat', 's_filettd', 's_alamat'];
    protected static $logAttributes = ['s_namapejabat', 's_jabatanpejabat', 's_nippejabat', 's_golonganpejabat', 's_filettd', 's_alamat'];
}
