<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Menutunggakantahunpbb extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_tahuntunggakanpbb';
    protected $primaryKey = 's_id_thntgkpbb';

    protected $fillable = ['s_thntgkpbb','s_rentangthntgkpbb'];
    protected static $logAttributes = ['s_thntgkpbb','s_rentangthntgkpbb'];
}
