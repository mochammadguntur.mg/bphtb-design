<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Pemda extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 's_pemda';
    protected $primaryKey = 's_idpemda';
    protected $fillable = [
        's_namaprov',
        's_namakabkot',
        's_namaibukotakabkot',
        's_kodeprovinsi',
        's_kodekabkot',
        's_namainstansi',
        's_namasingkatinstansi',
        's_alamatinstansi',
        's_notelinstansi',
        's_namabank',
        's_norekbank',
        's_logo',
        's_namacabang',
        's_kodecabang',
        's_kodepos',
        's_email',
        's_urlbphtb',
        's_latitude_pemda',
        's_longitude_pemda'
    ];

    protected static $logAttributes = [
        's_namaprov',
        's_namakabkot',
        's_namaibukotakabkot',
        's_kodeprovinsi',
        's_kodekabkot',
        's_namainstansi',
        's_namasingkatinstansi',
        's_alamatinstansi',
        's_notelinstansi',
        's_namabank',
        's_norekbank',
        's_logo',
        's_namacabang',
        's_kodecabang',
        's_kodepos',
        's_email',
        's_urlbphtb',
        's_latitude_pemda',
        's_longitude_pemda'
    ];
}
