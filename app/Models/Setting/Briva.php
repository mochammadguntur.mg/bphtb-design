<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Briva extends Model
{
    use HasFactory, LogsActivity;
    
    protected $table = 's_briva';
    protected $primaryKey = 's_idbriva';
    protected $fillable = [
        'client_id',
        'secret_id',
        'institution_code',
        'briva_no',
        'url_development',
        'url_production',
        'url_aktif'
    ];

    protected static $logAttributes = [
        'client_id',
        'secret_id',
        'institution_code',
        'briva_no',
        'url_development',
        'url_production',
        'url_aktif'
    ];
}
