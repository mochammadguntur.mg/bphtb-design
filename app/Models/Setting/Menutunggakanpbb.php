<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Menutunggakanpbb extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_setmenutgkpbb';
    protected $primaryKey = 's_id_setmenutgkpbb';

    protected $fillable = ['s_id_statustgkpbb', 's_ketmenutgkpbb'];
    protected static $logAttributes = ['s_id_statustgkpbb', 's_ketmenutgkpbb'];
}
