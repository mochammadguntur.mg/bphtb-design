<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Menuupload extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_menuupload';
    protected $primaryKey = 's_id_menuupload';

    protected $fillable = ['s_ukuranfile'];
    protected static $logAttributes = ['s_ukuranfile'];
}
