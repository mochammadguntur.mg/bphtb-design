<?php

namespace App\Models\Setting;

use App\Models\Spt\Spt;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;
use Illuminate\Support\Facades\DB;

class JenisTransaksi extends Model
{
    use HasFactory, LogsActivity;

    protected $table        =   's_jenistransaksi';
    protected $primaryKey   =   's_idjenistransaksi';
    protected $fillable = ['s_kodejenistransaksi', 's_namajenistransaksi', 's_idstatus_pht', 's_id_dptnpoptkp', 'created_at', 'updated_at'];
    protected static $logAttributes = ['s_kodejenistransaksi', 's_namajenistransaksi', 's_idstatus_pht', 's_id_dptnpoptkp',  'created_at', 'updated_at'];

    public function spt()
    {
        return $this->hasMany(Spt::class, 't_idjenistransaksi', 's_idjenistransaksi');
    }
    
    public function cekdata_data_status_perhitungan(){
        $data = DB::table('s_status_perhitungan')->get();
        
        return $data;
    }
    
    public function cekdata_data_status_dptnpoptkp(){
        $data = DB::table('s_status_dptnpoptkp')->get();
        
        return $data;
    }
}
