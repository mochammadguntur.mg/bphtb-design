<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Menugmap extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_setmenugmap';
    protected $primaryKey = 's_id_setmenugmap';

    protected $fillable = ['s_id_statusmenugmap', 's_ketmenugmap'];
    protected static $logAttributes = ['s_id_statusmenugmap', 's_ketmenugmap'];
}
