<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Notaris extends Model
{
    use HasFactory, LogsActivity;

    protected $table    = 's_notaris';
    protected $primaryKey = 's_idnotaris';
    protected $fillable = ['s_namanotaris', 's_alamatnotaris', 's_kodenotaris', 's_sknotaris', 's_statusnotaris'];
    protected static $logAttributes = ['s_namanotaris', 's_alamatnotaris', 's_kodenotaris', 's_sknotaris', 's_statusnotaris'];
}
