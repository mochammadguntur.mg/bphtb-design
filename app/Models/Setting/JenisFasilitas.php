<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class JenisFasilitas extends Model
{
    use HasFactory, LogsActivity;

    protected $table        = 's_jenisfasilitas';
    protected $primaryKey   = 's_idjenisfasilitas';
    protected $fillable     = ['s_kodejenisfasilitas', 's_namajenisfasilitas'];
    protected static $logAttibutes  = ['s_kodeejnisfasilitas', 's_namajenisfasilitas'];
}
