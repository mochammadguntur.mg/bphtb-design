<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class TargetPenerimaan extends Model
{
    use HasFactory, LogsActivity;

    protected $table = 's_target_bphtb';
    protected $primaryKey = 's_id_target_bphtb';
    protected $fillable = ['s_id_target_status', 's_nilai_target', 's_tahun_target', 's_keterangan'];
    protected static $logAttributes = ['s_id_target_status', 's_nilai_target', 's_tahun_target', 's_keterangan'];

    public function targetStatus()
    {
        return $this->belongsTo(TargetStatus::class, 's_id_target_status');
    }
}
