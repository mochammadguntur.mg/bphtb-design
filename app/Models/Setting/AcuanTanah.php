<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class AcuanTanah extends Model
{
    use HasFactory, LogsActivity;

    protected $table                = 's_acuan_jenistanah';
    protected $primaryKey           = 's_idacuan_jenis';
    protected $fillable             = ['s_kd_propinsi','s_kd_dati2','s_kd_kecamatan','s_kd_kelurahan','s_kd_blok','id_jenistanah', 's_permetertanah'];
    protected static $logAttributes = ['s_kd_propinsi','s_kd_dati2','s_kd_kecamatan','s_kd_kelurahan','s_kd_blok','id_jenistanah', 's_permetertanah'];

    public function jenisTanah()
    {
        return $this->belongsTo(
            JenisTanah::class,
            'id_jenistanah',
            // 'id_jenistanah'
        );
    }
}
