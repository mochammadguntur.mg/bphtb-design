<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Acuan extends Model
{
    use HasFactory, LogsActivity;
    
    protected $table    = 's_acuan';
    protected $primaryKey = 's_idacuan';
    protected $fillable = ['s_kd_propinsi','s_kd_dati2','s_kd_kecamatan','s_kd_kelurahan', 's_kd_blok', 's_permetertanah'];
    protected static $logAttributes = ['s_kd_propinsi','s_kd_dati2','s_kd_kecamatan','s_kd_kelurahan', 's_kd_blok', 's_permetertanah'];
}
