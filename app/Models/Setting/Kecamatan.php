<?php

namespace App\Models\Setting;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\Activitylog\Traits\LogsActivity;

class Kecamatan extends Model
{
    use HasFactory, LogsActivity;
    
    protected $table    = 's_kecamatan';
    protected $primaryKey = 's_idkecamatan';
    protected $fillable = ['s_kd_propinsi','s_kd_dati2','s_kd_kecamatan', 's_namakecamatan', 's_latitude', 's_longitude'];
    protected static $logAttributes = ['s_kd_propinsi', 's_kd_dati2', 's_kd_kecamatan', 's_namakecamatan', 's_latitude', 's_longitude'];
}
