<?php

namespace App\Exports\Pelayanan;

use App\Models\Pelayanan\PelayananMutasi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class PelayananMutasiExport implements FromView, WithEvents
{
    protected $t_nomutasi;
    protected $t_tglpengajuan;
    protected $t_kohirspt;
    protected $t_nama_pembeli;
    protected $t_tglpersetujuan;
    protected $created_at;
    function __construct($t_nomutasi,$t_tglpengajuan,$t_kohirspt,$t_nama_pembeli,$t_tglpersetujuan,$created_at)
    {
        $this->t_nomutasi = $t_nomutasi;
        $this->t_tglpengajuan = $t_tglpengajuan;
        $this->t_kohirspt = $t_kohirspt;
        $this->t_nama_pembeli = $t_nama_pembeli;
        $this->t_tglpersetujuan = $t_tglpersetujuan;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array{
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View{
        $response = PelayananMutasi::select()
        ->select(
            't_idmutasi',
            't_nomutasi',
            't_tglpengajuan',
            't_kohirspt',
            't_nama_pembeli',
            't_tglpersetujuan',
            't_pelayanan_mutasi.created_at')
        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_pelayanan_mutasi.t_idspt')
        ->orderBy('t_tglpengajuan');

        if ($this->t_nomutasi != null) {
            $response = $response->where('t_nomutasi', 'like', "%" . $this->t_nomutasi . "%");
        }
        if ($this->t_kohirspt != null) {
            $response = $response->where('t_kohirspt', 'like', "%" . $this->t_kohirspt . "%");
        }
        if ($this->t_nama_pembeli != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $this->t_nama_pembeli . "%");
        }
        if ($this->t_tglpengajuan != null) {
            $date = explode(' - ', $this->t_tglpengajuan);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpengajuan', [$startDate, $endDate]);
        }
        if ($this->t_tglpersetujuan != null) {
            $date = explode(' - ', $this->t_tglpersetujuan);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpersetujuan', [$startDate, $endDate]);
        }
        if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->get();
        return view('pelayanan-mutasi.exports.export', [
            'mutasi' => $response
        ]);
    }
}