<?php

namespace App\Exports\Skpdkb;

use App\Models\Skpdkb\Skpdkb;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class SkpdkbExport implements FromView, WithEvents
{
    protected $noskpdkb;
    protected $tglskpdkb;
    protected $nop;
    protected $nama_pembeli;
    protected $kodebayar;
    protected $status;
    protected $tglbayar;
    protected $created_at;
    function __construct($noskpdkb,$tglskpdkb,$nop,$nama_pembeli,$kodebayar,$status,$tglbayar,$created_at)
    {
        $this->noskpdkb = $noskpdkb;
        $this->tglskpdkb = $tglskpdkb;
        $this->nop = $nop;
        $this->nama_pembeli = $nama_pembeli;
        $this->kodebayar = $kodebayar;
        $this->status = $status;
        $this->tglbayar = $tglbayar;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array{
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View{
        $response = Skpdkb::select(
            't_skpdkb.t_idskpdkb',
            't_skpdkb.t_nourut_kodebayar',
            't_tglpenetapan',
            't_nama_pembeli',
            't_skpdkb.t_nop_sppt',
            't_jmlh_totalskpdkb',
            't_kodebayar_skpdkb',
            't_tglpembayaran_skpdkb',
            't_skpdkb.created_at')
        ->leftJoin('t_pembayaran_skpdkb', 't_pembayaran_skpdkb.t_idskpdkb', '=', 't_skpdkb.t_idskpdkb')
        ->leftJoin('t_spt', 't_spt.t_idspt', '=', 't_skpdkb.t_idspt')
        ->orderBy('t_idskpdkb');

        if ($this->noskpdkb != null) {
            $response = $response->where('t_nourut_kodebayar', 'like', "%" . $this->noskpdkb . "%");
        }
        if ($this->nop != null) {
            $response = $response->where('t_nop_sppt', 'like', "%" . $this->nop . "%");
        }
        if ($this->nama_pembeli != null) {
            $response = $response->where('t_nama_pembeli', 'ilike', "%" . $this->nama_pembeli . "%");
        }
        if ($this->kodebayar != null) {
            $response = $response->where('t_kodebayar_skpdkb', 'ilike', "%" . $this->kodebayar . "%");
        }
        if ($this->status != null) {
            $response = ($this->status == 1) ? $response->whereNotNull('t_tglpembayaran_skpdkb') : $response->whereNull('t_tglpembayaran_skpdkb');
        }
        if ($this->tglskpdkb != null) {
            $date = explode(' - ', $this->tglskpdkb);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpenetapan', [$startDate, $endDate]);
        }
        if ($this->tglbayar != null) {
            $date = explode(' - ', $this->tglbayar);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tglpembayaran_skpdkb', [$startDate, $endDate]);
        }
        if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->get();
        return view('skpdkb.exports.export', [
            'skpdkb' => $response
        ]);
    }
}