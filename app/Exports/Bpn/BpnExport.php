<?php

namespace App\Exports\Bpn;

use App\Models\Bpn\BpnApi;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class BpnExport implements FromView, WithEvents
{
    protected $t_tgltransaksi;
    function __construct($t_tgltransaksi)
    {
        $this->t_tgltransaksi = $t_tgltransaksi;
    }

    public function registerEvents(): array{
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View{
        $tgltransaksi = date('d/m/Y', strtotime($this->t_tgltransaksi));
        // dd($tgltransaksi);
        $userApi = BpnApi::select()->where('s_idusers_api','=',1)->get();

        $curl = curl_init();

        curl_setopt_array($curl, [
            CURLOPT_URL => "https://services.atrbpn.go.id/BPNApiService/Api/BPHTB/getDataATRBPN",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => "{\n\t\"username\": \"".$userApi[0]->s_username."\",\n\t\"password\": \"".$userApi[0]->s_password."\",\n\t\"TANGGAL\":\"".$tgltransaksi."\"\n}",
            CURLOPT_HTTPHEADER => [
                "Content-Type: application/json"
            ],
        ]);

        $response = curl_exec($curl);
        @$decode = json_decode($response, true);

        return view('bpn.exports.export', [
            'databpn' => $decode
        ]);
    }
}
