<?php

namespace App\Exports\Bpn;

use App\Models\Bpn\Bpn;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class BpnApiExport implements FromView, WithEvents
{
    protected $tgltransaksi;
    protected $akta;
    protected $ppat;
    protected $nop;
    protected $ntpd;
    protected $nib;
    protected $nik;
    protected $npwp;
    protected $namawp;
    protected $alamatop;
    protected $jenishak;
    protected $created_at;
    function __construct($tgltransaksi,$akta,$ppat,$nop,$ntpd,$nib,$nik,$npwp,$namawp,$alamatop,$jenishak,$created_at)
    {
        $this->tgltransaksi = $tgltransaksi;
        $this->akta = $akta;
        $this->ppat = $ppat;
        $this->nop = $nop;
        $this->ntpd = $ntpd;
        $this->nib = $nib;
        $this->nik = $nik;
        $this->npwp = $npwp;
        $this->namawp = $namawp;
        $this->alamatop = $alamatop;
        $this->jenishak = $jenishak;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array{
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View{
        $response = Bpn::orderBy('t_idbpn');
        if ($this->akta != null) {
            $response = $response->where('t_noakta', 'like', "%" . $this->akta. "%")
                                            ->orWhere('t_tglakta', 'like', "%" . $this->akta. "%");
        }
        if ($this->ppat != null) {
            $response = $response->where('t_namappat', 'ilike', "%" . $this->ppat . "%");
        }
        if ($this->nop != null) {
            $response = $response->where('t_nop', 'like', "%" . $this->nop . "%");
        }
        if ($this->ntpd != null) {
            $response = $response->where('t_ntpd', 'like', "%" . $this->ntpd . "%");
        }
        if ($this->nib != null) {
            $response = $response->where('t_nib', 'like', "%" . $this->nib . "%");
        }
        if ($this->nik != null) {
            $response = $response->where('t_nik', 'like', "%" . $this->nik . "%");
        }
        if ($this->npwp != null) {
            $response = $response->where('t_npwp', 'like', "%" . $this->npwp . "%");
        }
        if ($this->namawp != null) {
            $response = $response->where('t_namawp', 'ilike', "%" . $this->namawp . "%");
        }
        if ($this->alamatop != null) {
            $response = $response->where('t_kelurahanop', 'ilike', "%" . $this->alamatop . "%")
                    ->orWhere('t_kecamatanop', 'ilike', "%" . $this->alamatop . "%")
                    ->orWhere('t_kotaop', 'ilike', "%" . $this->alamatop . "%");
        }
        if ($this->jenishak != null) {
            $response = $response->where('t_jenishak', 'like', "%" . $this->jenishak . "%");
        }
        if ($this->tgltransaksi != null) {
            $date = explode(' - ', $this->tgltransaksi);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('t_tgltransaksi', [$startDate, $endDate]);
        }
        if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->get();
        return view('bpn.exports.exportatr', [
            'databpnatr' => $response
        ]);
    }
}
