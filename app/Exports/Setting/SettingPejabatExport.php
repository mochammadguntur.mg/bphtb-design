<?php

namespace App\Exports\Setting;

use App\Models\Setting\Pejabat;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingPejabatExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $namapejabat;
    protected $jabatanpejabat;
    //protected $created_at;
    function __construct($namapejabat, $jabatanpejabat) //$created_at
     {
        $this->namapejabat = $namapejabat;
        $this->jabatanpejabat = $jabatanpejabat;
        //$this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = Pejabat::orderBy('s_idpejabat');
        if ($this->namapejabat != null) {
            $response = $response->where('s_namapejabat', 'like', "%" . $this->namapejabat . "%");
        }
        if ($this->jabatanpejabat != null) {
            $response = $response->where('s_jabatanpejabat', 'like', "%" . $this->jabatanpejabat . "%");
        }
        /*if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }*/
        $response = $response->get();
        return view('setting-pejabat.exports.export', [
            'notariss' => $response
        ]);
    }
}
