<?php

namespace App\Exports\Setting;

use App\Models\Setting\Persyaratan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class SettingPersyaratanExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $jenis_peralihan;
    protected $nama_persyaratan;
    protected $created_at;

    function __construct($jenis_peralihan, $nama_persyaratan, $created_at)
    {
        $this->jenis_peralihan = $jenis_peralihan;
        $this->nama_persyaratan = $nama_persyaratan;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ]; 
    }

    public function view(): View
    {
        $response = Persyaratan::with(['jenisTransaksi', 'wajibUp'])->orderBy('s_idjenistransaksi');

        if (!empty($this->jenis_peralihan)) {
            $response = $response->where('s_idjenistransaksi', $this->jenis_peralihan);
        }

        if (!empty($this->nama_persyaratan)) {
            $response = $response->where('s_namapersyaratan', 'ilike', "%" . $this->nama_persyaratan . "%");
        }

        if (!empty($this->created_at)) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));

            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->get();

        return view('setting-persyaratan.exports.export', [
            'persyaratans' => $response
        ]);
    }
}
