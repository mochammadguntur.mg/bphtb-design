<?php

namespace App\Exports\Setting;

use Illuminate\Contracts\View\View;
use App\Models\Setting\Persentasewajar;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingPersentasewajarExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $nilaimin;
    protected $nilaimax;
    //protected $created_at;
    function __construct($nilaimin, $nilaimax) //$created_at
     {
        $this->nilaimin = $nilaimin;
        $this->nilaimax = $nilaimax;
        //$this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = Persentasewajar::orderBy('s_idpresentase_wajar');
        if ($this->nilaimin != null) {
            $response = $response->where('s_nilaimin', 'like', "%" . $this->nilaimin . "%");
        }
        if ($this->nilaimax != null) {
            $response = $response->where('s_nilaimax', 'like', "%" . $this->nilaimax . "%");
        }
        /*if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }*/
        $response = $response->get();
        return view('setting-persentasewajar.exports.export', [
            'data_persentasewajar' => $response
        ]);
    }
}
