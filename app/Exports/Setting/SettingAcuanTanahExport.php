<?php

namespace App\Exports\Setting;

use App\Models\Setting\AcuanTanah;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class SettingAcuanTanahExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $kd_propinsi;
	protected $kd_dati2;
	protected $kd_kecamatan;
	protected $kd_kelurahan;
	protected $kd_blok;
	protected $permeter_tanah;
	protected $jenis_tanah;
    protected $created_at;

    function __construct($kd_propinsi, $kd_dati2, $kd_kecamatan, $kd_kelurahan, $kd_blok, $permeter_tanah, $jenis_tanah, $created_at)
    {
		$this->kd_propinsi = $kd_propinsi;
		$this->kd_dati2 = $kd_dati2;
		$this->kd_kecamatan = $kd_kecamatan;
		$this->kd_kelurahan = $kd_kelurahan;
		$this->kd_blok = $kd_blok;
		$this->permeter_tanah = $permeter_tanah;
		$this->jenis_tanah = $jenis_tanah;
		$this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ]; 
    }

    public function view(): View
    {
        $response = AcuanTanah::with('jenisTanah')->orderBy('s_idacuan_jenis');

        if (!empty($this->kd_propinsi)) {
            $response = $response->where('s_kd_propinsi', 'ilike', "%" . $this->kd_propinsi . "%");
        }
		
		if (!empty($this->kd_dati2)) {
            $response = $response->where('s_kd_dati2', 'ilike', "%" . $this->kd_dati2 . "%");
        }
		
		if (!empty($this->kd_kecamatan)) {
            $response = $response->where('s_kd_kecamatan', 'ilike', "%" . $this->kd_kecamatan . "%");
        }
		
		if (!empty($this->kd_kelurahan)) {
            $response = $response->where('s_kd_kelurahan', 'ilike', "%" . $this->kd_kelurahan . "%");
        }
		
		if (!empty($this->kd_blok)) {
            $response = $response->where('s_kd_blok', 'ilike', "%" . $this->kd_blok . "%");
        }
		
		if (!empty($this->permeter_tanah)) {
            $response = $response->where('s_permetertanah', 'ilike', "%" . $this->permeter_tanah . "%");
        }
		
		if (!empty($this->jenis_tanah)) {
            $response = $response->where('id_jenistanah', $this->jenis_tanah);
        }

        if (!empty($this->created_at)) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));

            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->get();

        return view('setting-acuan-tanah.exports.export', [
            'acuantanah' => $response
        ]);
    }
}
