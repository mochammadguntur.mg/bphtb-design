<?php

namespace App\Exports\Setting;

use App\Models\Setting\Notaris;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingNotarisExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $namanotaris;
    protected $kodenotaris;
    protected $created_at;
    function __construct($namanotaris, $kodenotaris, $created_at)
    {
        $this->namanotaris = $namanotaris;
        $this->kodenotaris = $kodenotaris;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = Notaris::orderBy('s_idnotaris');
        if ($this->namanotaris != null) {
            $response = $response->where('s_namanotaris', 'like', "%" . $this->namanotaris . "%");
        }
        if ($this->kodenotaris != null) {
            $response = $response->where('s_kodenotaris', 'like', "%" . $this->kodenotaris . "%");
        }
        if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->get();
        return view('setting-notaris.exports.export', [
            'notariss' => $response
        ]);
    }
}
