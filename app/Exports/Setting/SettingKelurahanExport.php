<?php

namespace App\Exports\Setting;

use App\Models\Setting\kelurahan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingKelurahanExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $namakelurahan;
    protected $kodekelurahan;
    protected $created_at;
    function __construct($namakelurahan, $kodekelurahan, $created_at)
    {
        $this->namakelurahan = $namakelurahan;
        $this->kodekelurahan = $kodekelurahan;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = Kelurahan::orderBy('s_idkelurahan');

        if ($this->namakelurahan != null) {
            $response = $response->where('s_namakelurahan', 'like', "%" . $this->namakelurahan . "%");
        }
        if ($this->kodekelurahan != null) {
            $response = $response->where('s_kodekelurahan', 'like', "%" . $this->kodekelurahan . "%");
        }
        if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->get();
        return view('setting-kelurahan.exports.export', [
            'kelurahans' => $response
        ]);
    }
}
