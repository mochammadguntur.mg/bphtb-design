<?php

namespace App\Exports\Setting;

use Illuminate\Contracts\View\View;
use App\Models\Setting\Tempo;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingTempoExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $s_idtempo;
    protected $s_haritempo;
    protected $s_tglberlaku_awal;
    protected $s_tglberlaku_akhir;
    protected $s_status;
    protected $created_at;
    function __construct($s_idtempo,$s_haritempo,$s_tglberlaku_awal,$s_tglberlaku_akhir,$s_status,$created_at)
    {
        $this->s_idtempo = $s_idtempo;
        $this->s_haritempo = $s_haritempo;
        $this->s_tglberlaku_awal = $s_tglberlaku_awal;
        $this->s_tglberlaku_akhir = $s_tglberlaku_akhir;
        $this->s_status = $s_status;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array{
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View{
        $response = Tempo::orderBy('s_idtempo');
        if ($this->s_idtempo != null) {
            $response = $response->where('s_idtempo', '=', $this->s_idtempo );
        }
        if ($this->s_haritempo != null) {
            $response = $response->where('s_haritempo', 'like', "%" . $this->s_haritempo . "%");
        }
        if ($this->s_tglberlaku_awal != null) {
            $date = explode(' - ', $this->s_tglberlaku_awal);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('s_tglberlaku_awal', [$startDate, $endDate]);
        }
        if ($this->s_tglberlaku_akhir != null) {
            $date = explode(' - ', $this->s_tglberlaku_akhir);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('s_tglberlaku_akhir', [$startDate, $endDate]);
        }
        if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        if ($this->s_status != null) {
            $response = $response->where('s_status', '=', $this->s_status );
        }
        $response = $response->get();
        return view('setting-tempo.exports.export', [
            'tempo' => $response
        ]);
    }
}
