<?php

namespace App\Exports\Setting;

use Illuminate\Contracts\View\View;
use App\Models\Setting\JenisTransaksi;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingJenisTransaksiExport implements FromView, WithEvents, ShouldAutoSize {

    protected $s_kodejenistransaksi;
    protected $s_namajenistransaksi;
    protected $s_idstatus_pht;
    protected $s_id_dptnpoptkp;

    function __construct($s_kodejenistransaksi, $s_namajenistransaksi, $s_idstatus_pht, $s_id_dptnpoptkp) {
        $this->s_kodejenistransaksi = $s_kodejenistransaksi;
        $this->s_namajenistransaksi = $s_namajenistransaksi;
        $this->s_idstatus_pht = $s_idstatus_pht;
        $this->s_id_dptnpoptkp = $s_id_dptnpoptkp;
    }

    public function registerEvents(): array {
        return [
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            }
        ];
    }

    public function view(): View {
        $response = (new JenisTransaksi())::select('s_jenistransaksi.*', 's_status_perhitungan.s_nama', 's_status_dptnpoptkp.s_nama_dptnpoptkp')
                ->leftJoin('s_status_perhitungan', 's_status_perhitungan.s_idstatus_pht', '=', 's_jenistransaksi.s_idstatus_pht')
                ->leftJoin('s_status_dptnpoptkp', 's_status_dptnpoptkp.s_id_dptnpoptkp', '=', 's_jenistransaksi.s_id_dptnpoptkp');

        if ($this->s_kodejenistransaksi != null) {
            $response = $response->where('s_kodejenistransaksi', 'ilike', "%" . $this->s_kodejenistransaksi . "%");
        }

        if ($this->s_namajenistransaksi != null) {
            $response = $response->where('s_namajenistransaksi', 'ilike', "%" . $this->s_namajenistransaksi . "%");
        }

        if ($this->s_idstatus_pht != null) {
            $response = $response->where('s_jenistransaksi.s_idstatus_pht', '=', $this->s_idstatus_pht);
        }

        if ($this->s_id_dptnpoptkp != null) {
            $response = $response->where('s_jenistransaksi.s_id_dptnpoptkp', '=', $this->s_id_dptnpoptkp);
        }
        $response = $response->get();
        return view('setting-jenis-transaksi.exports.export', [
            'jenistransaksi' => $response
        ]);
    }

}
