<?php

namespace App\Exports\Setting;

use App\Models\Setting\HakTanah;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingHakTanahExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $s_kodehaktanah;
    protected $s_namahaktanah;
    protected $created_at;
    function __construct($s_kodehaktanah, $s_namahaktanah, $created_at)
    {
        $this->s_kodehaktanah = $s_kodehaktanah;
        $this->s_namahaktanah = $s_namahaktanah;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function (BeforeExport $event){
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class =>function (AfterSheet $event){
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            }
        ];
    }

    public function view(): View
    {
        $response = HakTanah::orderBy('s_idhaktanah');
        if($this->s_kodehaktanah != null) {
            $response = $response->where('s_kodehaktanah' , 'ilike', "%". $this->s_kodehaktanah."%");
        }

        if($this->s_namahaktanah != null){
            $response = $response->where('s_namahaktanah', 'ilike', "%". $this->s_namahaktanah."%");
        }

        if($this->created_at != null){
            $date = explode('-', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->get();
        return view('setting-hak-tanah.exports.export', [
            'haktanah' => $response
        ]);
    }


}
