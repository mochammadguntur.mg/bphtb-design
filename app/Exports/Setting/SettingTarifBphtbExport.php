<?php

namespace App\Exports\Setting;

use App\Models\Setting\TarifBphtb;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingTarifBphtbExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $s_tarif_bphtb;
    protected $s_dasar_hukum;
    protected $s_tgl_awal;
    protected $s_tgl_akhir;
    protected $created_at;
    function __construct($s_tarif_bphtb, $s_dasar_hukum, $s_tgl_awal, $s_tgl_akhir, $created_at)
    {
        $this->s_tarif_bphtb = $s_tarif_bphtb;
        $this->s_dasar_hukum = $s_dasar_hukum;
        $this->s_tgl_awal = $s_tgl_awal;
        $this->s_tgl_akhir = $s_tgl_akhir;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = TarifBphtb::orderBy('s_idtarifbphtb');
        if ($this->s_tarif_bphtb != null) {
            $response = $response->where('s_tarif_bphtb', 'like', "%" . $this->s_tarif_bphtb . "%");
        }
        if ($this->s_dasar_hukum != null) {
            $response = $response->where('s_dasar_hukum', 'like', "%" . $this->s_dasar_hukum . "%");
        }
        if ($this->s_tgl_awal != null) {
            $startDate = date('Y-m-d', strtotime($this->s_tgl_awal));
            $response = $response->where('s_tgl_awal', $startDate);
        }
        if ($this->s_tgl_akhir != null) {
            $startDate = date('Y-m-d', strtotime($this->s_tgl_akhir));
            $response = $response->where('s_tgl_akhir', $startDate);
        }
        if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->get();
        return view('setting-tarif-bphtb.exports.export', [
            'tarifs' => $response
        ]);
    }
}
