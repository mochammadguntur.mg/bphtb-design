<?php

namespace App\Exports\Setting;

use Illuminate\Contracts\View\View;
use App\Models\Setting\JenisFasilitas;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingJenisFasilitasExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $s_kodejenisfasilitas;
    protected $s_namajenisfasilitas;
    protected $created_at;
    function __construct($s_kodejenisfasilitas, $s_namajenisfasilitas, $created_at)
    {
        $this->s_kodejenisfasilitas = $s_kodejenisfasilitas;
        $this->s_namajenisfasilitas = $s_namajenisfasilitas;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class =>function(BeforeExport $event){
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class => function(AfterSheet $event){
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            }
        ];
    }

    public function view(): View{
        $response = JenisFasilitas::orderBy('s_idjenisfasilitas');
        if($this->s_kodejenisfasilitas != null){
            $response = $response->where('s_kodejenisfasilitas', 'like', "%". $this->s_kodejenisfasilitas . "%");
        }

        if($this->s_namajenisfasilitas != null){
            $response = $response->where('s_namajenisfasilitas', 'ilike', "%". $this->s_namajenisfasilitas . "%");
        }

        if($this->created_at != null){
            $date = explode('-', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate    = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response   = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->get();
        return view('setting-jenis-fasilitas.exports.export',[
            'jenisfasilitas'    => $response
        ]);
    }
}