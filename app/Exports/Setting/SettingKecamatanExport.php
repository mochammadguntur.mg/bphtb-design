<?php

namespace App\Exports\Setting;

use App\Models\Setting\Kecamatan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingKecamatanExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $namakecamatan;
    protected $kodekecamatan;
    protected $created_at;
    function __construct($namakecamatan, $kodekecamatan, $created_at)
    {
        $this->namakecamatan = $namakecamatan;
        $this->kodekecamatan = $kodekecamatan;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = Kecamatan::orderBy('s_idkecamatan');
        if ($this->namakecamatan != null) {
            $response = $response->where('s_namakecamatan', 'like', "%" . $this->namakecamatan . "%");
        }
        if ($this->kodekecamatan != null) {
            $response = $response->where('s_kodekecamatan', 'like', "%" . $this->kodekecamatan . "%");
        }
        if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        $response = $response->get();
        return view('setting-kecamatan.exports.export', [
            'kecamatans' => $response
        ]);
    }
}
