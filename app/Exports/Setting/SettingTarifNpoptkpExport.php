<?php

namespace App\Exports\Setting;

use Illuminate\Contracts\View\View;
use App\Models\Setting\TarifNpoptkp;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingTarifNpoptkpExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $s_idjenistransaksinpoptkp;
    protected $s_tarifnpoptkp;
    protected $s_tarifnpoptkptambahan;
    protected $s_dasarhukumnpoptkp;
    protected $s_tglberlaku_awal;
    protected $s_tglberlaku_akhir;
    protected $s_statusnpoptkp;
    protected $created_at;
    function __construct($s_idjenistransaksinpoptkp,$s_tarifnpoptkp,$s_tarifnpoptkptambahan,$s_dasarhukumnpoptkp,$s_tglberlaku_awal,$s_tglberlaku_akhir,$s_statusnpoptkp,$created_at)
    {
        $this->s_idjenistransaksinpoptkp = $s_idjenistransaksinpoptkp;
        $this->s_tarifnpoptkp = $s_tarifnpoptkp;
        $this->s_tarifnpoptkptambahan = $s_tarifnpoptkptambahan;
        $this->s_dasarhukumnpoptkp = $s_dasarhukumnpoptkp;
        $this->s_tglberlaku_awal = $s_tglberlaku_awal;
        $this->s_tglberlaku_akhir = $s_tglberlaku_akhir;
        $this->s_statusnpoptkp = $s_statusnpoptkp;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array{
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View{
        $response = TarifNpoptkp::orderBy('s_idtarifnpoptkp');
        if ($this->s_idjenistransaksinpoptkp != null) {
            $response = $response->where('s_idjenistransaksinpoptkp', '=', $this->s_idjenistransaksinpoptkp );
        }
        if ($this->s_tarifnpoptkp != null) {
            $response = $response->where('s_tarifnpoptkp', 'like', "%" . $this->s_tarifnpoptkp . "%");
        }
        if ($this->s_tarifnpoptkptambahan != null) {
            $response = $response->where('s_tarifnpoptkptambahan', 'like', "%" . $this->s_tarifnpoptkptambahan . "%");
        }
        if ($this->s_dasarhukumnpoptkp != null) {
            $response = $response->where('s_dasarhukumnpoptkp', 'ilike', "%" . $this->s_dasarhukumnpoptkp . "%");
        }
        if ($this->s_tglberlaku_awal != null) {
            $date = explode(' - ', $this->s_tglberlaku_awal);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('s_tglberlaku_awal', [$startDate, $endDate]);
        }
        if ($this->s_tglberlaku_akhir != null) {
            $date = explode(' - ', $this->s_tglberlaku_akhir);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('s_tglberlaku_akhir', [$startDate, $endDate]);
        }
        if ($this->created_at != null) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day")); // karena di db pakai date time maka tambah 1 hari
            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }
        if ($this->s_statusnpoptkp != null) {
            $response = $response->where('s_statusnpoptkp', '=', $this->s_statusnpoptkp );
        }
        $response = $response->get();
        return view('setting-tarif-npoptkp.exports.export', [
            'tarifnpoptkp' => $response
        ]);
    }
}
