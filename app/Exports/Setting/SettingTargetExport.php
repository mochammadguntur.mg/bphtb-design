<?php

namespace App\Exports\Setting;

use App\Models\Setting\TargetPenerimaan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class SettingTargetExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $periode;
    protected $created_at;

    function __construct($periode, $target_status, $created_at)
    {
        $this->periode = $periode;
        $this->target_status = $target_status;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = TargetPenerimaan::with('targetStatus')->orderBy('s_tahun_target');

        if (!empty($this->periode)) {
            $response = $response->where('s_tahun_target', $this->periode);
        }

        if (!empty($this->target_status)) {
            $response = $response->where('s_id_target_status', $this->target_status);
        }

        if (!empty($this->created_at)) {
            $date = explode(' - ', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate = date('Y-m-d', strtotime($date[1] . "+1day"));

            $response = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->get();

        return view('setting-target.exports.export', [
            'target' => $response
        ]);
    }
}
