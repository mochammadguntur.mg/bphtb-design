<?php

namespace App\Exports\Setting;

use App\Models\Setting\Acuan;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;

class SettingAcuanExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $s_idacuan;
    protected $s_permetertanah;
    protected $created_at;
    function __construct($s_idacuan, $s_permetertanah, $created_at)
    {
        $this->s_idacuan = $s_idacuan;
        $this->s_permetertanah = $s_permetertanah;
        $this->created_at = $created_at;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class =>function(BeforeExport $event){
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class => function(AfterSheet $event){
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            }
        ];
    }

    public function view(): View{
        $response = Acuan::orderBy('s_idacuan');
        if($this->s_idacuan != null){
            $response = $response->where('s_idacuan', 'like', "%". $this->s_idacuan . "%");
        }

        if($this->s_permetertanah != null){
            $response = $response->where('s_permetertanah', 'ilike', "%". $this->s_permetertanah . "%");
        }

        if($this->created_at != null){
            $date = explode('-', $this->created_at);
            $startDate = date('Y-m-d', strtotime($date[0]));
            $endDate    = date('Y-m-d', strtotime($date[1] . "+1day"));
            $response   = $response->whereBetween('created_at', [$startDate, $endDate]);
        }

        $response = $response->get();
        return view('setting-harga-acuan.exports.export',[
            'acuans'    => $response
        ]);
    }
}