<?php

namespace App\Exports\CetakLaporan;

use App\Models\Setting\Pejabat;
use App\Models\Setting\Pemda;
use App\Models\Spt\Spt;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Illuminate\Contracts\View\View;
use MPDF;

class PenerimaanExport implements FromView, WithEvents
{
    protected $param;

    function __construct($param)
    {
        $this->param = $param;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }
    
    public function cetak_data_penerimaan($data_get)
    {
        $response = Spt::from('t_spt as a')
            ->leftJoin('s_jenistransaksi as b', 'b.s_idjenistransaksi', '=', 'a.t_idjenistransaksi')
            ->leftJoin('s_notaris as c', 'c.s_idnotaris', '=', 'a.t_idnotaris_spt')
            ->leftJoin('t_pembayaran_bphtb as d', 'd.t_idspt', '=', 'a.t_idspt')
            ->leftJoin('s_jenistanah', 's_jenistanah.id_jenistanah', '=', 'a.t_id_jenistanah')
            ->select(
                'a.*',    
                
                'b.s_namajenistransaksi',
                'c.s_namanotaris',
                'd.t_jmlhbayar_pokok as jml_bayar',
                's_jenistanah.nama_jenistanah',
                'd.t_jmlhbayar_total',
                'd.t_tglpembayaran_pokok'
            )
            ->orderBy('t_tgldaftar_spt');

        $response->whereNotNull('d.t_tglpembayaran_pokok');

        // $tgl_bayar = explode(' - ', $this->param['tgl_penerimaan']);
        $tgl_awal = Carbon::parse($data_get['tgl_penerimaan_awal'])->format('Y-m-d');
        $tgl_akhir = Carbon::parse($data_get['tgl_penerimaan_akhir'])->format('Y-m-d');
        $response = $response->whereRaw("d.t_tglpembayaran_pokok::date >= '".$tgl_awal."' and d.t_tglpembayaran_pokok::date <= '".$tgl_akhir."' ");
        //$response = $response->whereBetween('d.t_tglpembayaran_pokok', [$tgl_awal, $tgl_akhir]);

        if (!empty($data_get['jenis_peralihan'])) {
            $response = $response->where('a.t_idjenistransaksi', $data_get['jenis_peralihan']);
        }

        if (!empty($data_get['notaris'])) {
            $response = $response->where('a.t_idnotaris_spt', $data_get['notaris']);
        }

        if (!empty($data_get['kecamatan'])) {
            $response = $response->where('t_kecamatan_sppt', 'ilike', '%' . $data_get['kecamatan'] . '%');
        }

        if (!empty($data_get['kelurahan'])) {
            $response = $response->where('t_kelurahan_sppt', 'ilike', '%' . $data_get['kelurahan'] . '%');
        }

        $response = $response->get();
        return $response;
    }

    public function view(): View
    {
        $data_get = $this->param;
        
        $ttd_megetahui = Pejabat::find($data_get['mengetahui']);
        $ttd_diproses = Pejabat::find($data_get['diproses']);
        
        $response = $this->cetak_data_penerimaan($data_get);
        
        $data_cetak = array(
                    'penerimaan' => $response,
                    'tgl_awal' => $data_get['tgl_penerimaan_awal'],
                    'tgl_akhir' => $data_get['tgl_penerimaan_akhir'],
                    'tgl_cetak' => $data_get['tgl_cetak'],
                    'mengetahui' => $ttd_megetahui,
                    'diproses' => $ttd_diproses,
                    'pemda' => Pemda::first(),
                    'tipecetak' => $data_get['tipecetak'],
                    'data_get' => $data_get
        );

        return view('cetak-laporan.exports.cetak-penerimaan',$data_cetak);
        
    }
}
