<?php

namespace App\Exports\CetakLaporan;

use App\Models\Pelaporan\InputAjb;
use App\Models\Setting\Pejabat;
use App\Models\Setting\Pemda;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Illuminate\Contracts\View\View;

class AjbExport implements FromView, WithEvents
{
    protected $tgl_pendaftaran;
    protected $jenis_peralihan;
    protected $notaris;
    protected $kecamatan;
    protected $kelurahan;
    protected $tgl_cetak;
    protected $mengetahui;
    protected $diproses;

    function __construct($param)
    {
        $this->tgl_pendaftaran = $param['tgl_penerimaan'];
        $this->jenis_peralihan = $param['jenis_peralihan'];
        $this->notaris = $param['notaris'];
        $this->kecamatan = $param['kecamatan'];
        $this->kelurahan = $param['kelurahan'];
        $this->tgl_cetak = $param['tgl_cetak'];
        $this->mengetahui = $param['mengetahui'];
        $this->diproses = $param['diproses'];
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $ttd_megetahui = Pejabat::find($this->mengetahui);
        $ttd_diproses = Pejabat::find($this->diproses);
        $response = InputAjb::with('spt')->orderBy('t_idajb');

        if (!empty($this->notaris)) {
            $ppat = $this->notaris;
            $response = $response->with(['spt' => function ($q) use ($ppat) {
                $q->where('t_idnotaris_spt', $ppat);
            }]);
        }

        if (!empty($this->tgl_pendaftaran)) {
            $tgl_daftar = explode(' - ', $this->tgl_pendaftaran);
            $tgl_awal = Carbon::parse($tgl_daftar[0])->format('Y-m-d');
            $tgl_akhir = Carbon::parse($tgl_daftar[1])->format('Y-m-d');

            $response = $response->whereBetween('t_tgl_ajb', [$tgl_awal, $tgl_akhir]);
        }

        if (!empty($this->jenis_peralihan)) {
            $t_idjenistransaksi = $this->jenis_peralihan;
            $response = $response->with(['spt' => function ($query) use ($t_idjenistransaksi) {
                $query->where('t_idjenistransaksi', $t_idjenistransaksi);
            }]);
        }

        if (!empty($this->kecamatan)) {
            $t_kecamatan = $this->kecamatan;
            $response = $response->with(['spt' => function ($query) use ($t_kecamatan) {
                $query->where('t_kecamatan_sppt', 'ilike', '%' . $t_kecamatan . '%');
            }]);
        }

        if (!empty($this->kelurahan)) {
            $t_kelurahan = $this->kelurahan;
            $response = $response->with(['spt' => function ($query) use ($t_kelurahan) {
                $query->where('t_kelurahan_sppt', 'ilike', '%' . $t_kelurahan . '%');
            }]);
        }

        $response = $response->get();

        return view('cetak-laporan.exports.cetak-ajb', [
            'ajb' => $response,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
            'tgl_cetak' => $this->tgl_cetak,
            'mengetahui' => $ttd_megetahui,
            'diproses' => $ttd_diproses,
            'pemda' => Pemda::first()
        ]);
    }
}
