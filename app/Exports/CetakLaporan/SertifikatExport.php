<?php

namespace App\Exports\CetakLaporan;

use App\Models\Bpn\Bpn;
use App\Models\Setting\Pejabat;
use App\Models\Setting\Pemda;
use App\Models\Spt\Spt;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Illuminate\Contracts\View\View;

class SertifikatExport implements FromView, WithEvents
{
    protected $tgl_sertipikat;
    protected $kecamatan;
    protected $kelurahan;
    protected $tgl_cetak;
    protected $mengetahui;
    protected $diproses;

    function __construct($param)
    {
        $this->tgl_sertipikat = $param['tgl_sertipikat'];
        $this->kecamatan = $param['kecamatan'];
        $this->kelurahan = $param['kelurahan'];
        $this->tgl_cetak = $param['tgl_cetak'];
        $this->mengetahui = $param['mengetahui'];
        $this->diproses = $param['diproses'];
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_LEGAL);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $ttd_megetahui = Pejabat::find($this->mengetahui);
        $ttd_diproses = Pejabat::find($this->diproses);
        $response = Bpn::orderBy('t_tglakta');

        if (!empty($this->tgl_sertipikat)) {
            $tgl_bayar = explode(' - ', $this->tgl_sertipikat);
            $tgl_awal = Carbon::parse($tgl_bayar[0])->format('Y-m-d');
            $tgl_akhir = Carbon::parse($tgl_bayar[1])->format('Y-m-d');
            $response = $response->whereBetween('t_tgltransaksi', [$tgl_awal, $tgl_akhir]);
        }

        if (!empty($this->kecamatan)) {
            $response = $response->where('t_kecamatanop', 'ilike', '%' . $this->kecamatan . '%');
        }

        if (!empty($this->kelurahan)) {
            $response = $response->where('t_kelurahanop', 'ilike', '%' . $this->kelurahan . '%');
        }

        $response = $response->get();

        return view('cetak-laporan.exports.cetak-sertifikat', [
            'sertifikat' => $response,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
            'tgl_cetak' => $this->tgl_cetak,
            'mengetahui' => $ttd_megetahui,
            'diproses' => $ttd_diproses,
            'pemda' => Pemda::first()
        ]);
    }
}
