<?php

namespace App\Exports\CetakLaporan;

use App\Models\Setting\JenisTransaksi;
use App\Models\Setting\Pejabat;
use App\Models\Setting\Pemda;
use App\Models\Spt\Spt;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Illuminate\Contracts\View\View;

class RekapPenerimaanExport implements FromView, WithEvents
{
    protected $tgl_penerimaan_awal;
    protected $tgl_penerimaan_akhir;
    protected $kecamatan;
    protected $kelurahan;
    protected $tgl_cetak;
    protected $mengetahui;
    protected $diproses;


    function __construct($tgl_penerimaan_awal, $tgl_penerimaan_akhir, $kecamatan, $kelurahan, $tgl_cetak, $mengetahui, $diproses)
    {
        $this->tgl_penerimaan_awal = $tgl_penerimaan_awal;
        $this->tgl_penerimaan_akhir = $tgl_penerimaan_akhir;
        $this->kecamatan = $kecamatan;
        $this->kelurahan = $kelurahan;
        $this->tgl_cetak = $tgl_cetak;
        $this->mengetahui = $mengetahui;
        $this->diproses = $diproses;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_PORTRAIT);
            },
        ];
    }

    public function view(): View
    {
        $ttd_megetahui = Pejabat::find($this->mengetahui);
        $ttd_diproses = Pejabat::find($this->diproses);

        $jenis_transaksi = JenisTransaksi::get();
        $spt = Spt::groupBy('t_idjenistransaksi')
                ->leftJoin('t_pembayaran_bphtb as d', 'd.t_idspt', '=', 't_spt.t_idspt')
                ->selectRaw('t_idjenistransaksi, sum(t_nilai_bphtb_fix) as total_terima')
                ->whereNotNull('d.t_tglpembayaran_pokok');

        if (!empty($this->tgl_penerimaan)) {
            $tgl_awal = Carbon::parse($this->tgl_penerimaan_awal)->format('Y-m-d');
            $tgl_akhir = Carbon::parse($this->tgl_penerimaan_akhir)->format('Y-m-d');
            $spt = $spt->whereBetween('d.t_tglpembayaran_pokok', [$tgl_awal, $tgl_akhir]);
        }

        if (!empty($this->kecamatan)) {
            $spt = $spt->where('t_kecamatan_sppt', 'ilike', '%' . $this->kecamatan . '%');
        }

        if (!empty($this->kelurahan)) {
            $spt = $spt->where('t_kelurahan_sppt', 'ilike', '%' . $this->kelurahan . '%');
        }

        $response = [];
        $tot_terima = [];

        foreach($spt->get() as $s) {
            $tot_terima[$s->t_idjenistransaksi] = $s->total_terima;
        }

        foreach ($jenis_transaksi as $row) {
                $response[] = [
                    's_namajenistransaksi' => $row->s_namajenistransaksi,
                    'total_penerimaan' => isset($tot_terima[$row->s_idjenistransaksi]) ? $tot_terima[$row->s_idjenistransaksi] : 0
                ];
        }

        return view('cetak-laporan.exports.cetak-rekap-penerimaan', [
            'penerimaan' => $response,
            'tgl_awal' => $this->tgl_penerimaan_awal,
            'tgl_akhir' => $this->tgl_penerimaan_akhir,
            'tgl_cetak' => $this->tgl_cetak,
            'mengetahui' => $ttd_megetahui,
            'diproses' => $ttd_diproses,
            'pemda' => Pemda::first()
        ]);
    }
}
