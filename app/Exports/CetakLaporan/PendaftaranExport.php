<?php

namespace App\Exports\CetakLaporan;

use App\Models\Setting\Pejabat;
use App\Models\Setting\Pemda;
use App\Models\Spt\Spt;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Illuminate\Contracts\View\View;

class PendaftaranExport implements FromView, WithEvents
{
    protected $param;
    protected $pemda;

    function __construct($param)
    {
        $this->param = $param;
        $this->pemda = Pemda::first();
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = Spt::from('t_spt as a')
            ->leftJoin('s_jenistransaksi as b', 'b.s_idjenistransaksi', '=', 'a.t_idjenistransaksi')
            ->leftJoin('s_notaris as c', 'c.s_idnotaris', '=', 'a.t_idnotaris_spt')
            ->select(
                'a.t_nama_pembeli',
                'a.t_nik_pembeli',
                'a.t_jalan_pembeli',
                'a.t_nama_penjual',
                'a.t_namakec_penjual',
                'a.t_namakel_penjual',
                'a.t_kabkota_penjual',
                'a.t_nop_sppt',
                'a.t_tahun_sppt',
                'a.t_nama_sppt',
                'a.t_jalan_sppt',
                'a.t_npop_bphtb',
                'a.t_luastanah',
                'a.t_luasbangunan',
                'b.s_namajenistransaksi',
                'c.s_namanotaris'
            )
            ->orderBy('t_tgldaftar_spt');

        if (!empty($this->param['tgl_pendaftaran'])) {
            $tgl_daftar = explode(' - ', $this->param['tgl_pendaftaran']);
            $tgl_awal = Carbon::parse($tgl_daftar[0])->format('Y-m-d');
            $tgl_akhir = Carbon::parse($tgl_daftar[1])->format('Y-m-d');
            $response = $response->whereBetween('t_tgldaftar_spt', [$tgl_awal, $tgl_akhir]);
        }

        if (!empty($this->param['jenis_peralihan'])) {
            $response = $response->where('t_idjenistransaksi', $this->param['jenis_peralihan']);
        }

        if (!empty($this->param['notaris'])) {
            $response = $response->where('t_idnotaris_spt', $this->param['notaris']);
        }

        if (!empty($this->param['kecamatan'])) {
            $response = $response->where('t_kecamatan_sppt', 'ilike', '%'. $this->param['kecamatan'] .'%');
        }

        if (!empty($this->param['kelurahan'])) {
            $response = $response->where('t_kelurahan_sppt', 'ilike', '%'. $this->param['kelurahan'] .'%');
        }

        $response = $response->get();
        $ttd_megetahui = Pejabat::find($this->param['mengetahui']);
        $ttd_diproses = Pejabat::find($this->param['diproses']);

        return view('cetak-laporan.exports.cetak-pendaftaran', [
            'pendaftaran' => $response,
            'tgl_awal' => $tgl_awal,
            'tgl_akhir' => $tgl_akhir,
            'tgl_cetak' => $this->param['tgl_cetak'],
            'mengetahui' => $ttd_megetahui,
            'diproses' => $ttd_diproses,
            'pemda' => $this->pemda,
            'image' => false
        ]);
    }
}
