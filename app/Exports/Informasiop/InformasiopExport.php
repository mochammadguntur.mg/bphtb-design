<?php

namespace App\Exports\Informasiop;

use App\Models\Pbb\Sppt;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class InformasiopExport implements FromView, WithEvents
{
    protected $t_cariberdasarkan;
    protected $t_nop;
    protected $t_nik;
    protected $t_npwpd;
    function __construct($t_cariberdasarkan,$t_nop,$t_nik,$t_npwpd)
    {
        $this->t_cariberdasarkan = $t_cariberdasarkan;
        $this->t_nop = $t_nop;
        $this->t_nik = $t_nik;
        $this->t_npwpd = $t_npwpd;
    }

    public function registerEvents(): array{
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                // dd($event->writer);
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                // dd($event->sheet->worksheet);
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View{
        if ($this->t_cariberdasarkan == 'NOP') {
            $NOP = $this->t_nop;
            $KD_PROV    = substr($NOP, 0, 2);
            $KD_DATI2   = substr($NOP, 3, 2);
            $KD_KEC     = substr($NOP, 6, 3);
            $KD_KEL     = substr($NOP, 10, 3);
            $KD_BLOK    = substr($NOP, 14, 3);
            $NO_URUT    = substr($NOP, 18, 4);
            $KD_JNS     = substr($NOP, 23, 1);
            $TAHUNPAJAK = substr($NOP, 25, 4);

            $query = Sppt::select()
                ->select(
                    'SPPT.*',
                    'REF_KECAMATAN.NM_KECAMATAN',
                    'REF_KELURAHAN.NM_KELURAHAN',
                    'KELAS_TANAH.NILAI_PER_M2_TANAH',
                    'KELAS_BANGUNAN.NILAI_PER_M2_BNG'
                )
                ->leftJoin('REF_KECAMATAN', [
                    ['REF_KECAMATAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                    ['REF_KECAMATAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                    ['REF_KECAMATAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN']
                ])
                ->leftJoin('REF_KELURAHAN', [
                    ['REF_KELURAHAN.KD_PROPINSI', '=', 'SPPT.KD_PROPINSI'],
                    ['REF_KELURAHAN.KD_DATI2', '=', 'SPPT.KD_DATI2'],
                    ['REF_KELURAHAN.KD_KECAMATAN', '=', 'SPPT.KD_KECAMATAN'],
                    ['REF_KELURAHAN.KD_KELURAHAN', '=', 'SPPT.KD_KELURAHAN']
                ])
                ->leftJoin('KELAS_TANAH', [
                    ['KELAS_TANAH.KD_KLS_TANAH', '=', 'SPPT.KD_KLS_TANAH']
                ])
                ->leftJoin('KELAS_BANGUNAN', [
                    ['KELAS_BANGUNAN.KD_KLS_BNG', '=', 'SPPT.KD_KLS_BNG']
                ])
                ->where([
                    ['SPPT.KD_PROPINSI', '=', $KD_PROV],
                    ['SPPT.KD_DATI2', '=', $KD_DATI2],
                    ['SPPT.KD_KECAMATAN', '=', $KD_KEC],
                    ['SPPT.KD_KELURAHAN', '=', $KD_KEL],
                    ['SPPT.KD_BLOK', '=', $KD_BLOK],
                    ['SPPT.NO_URUT', '=', $NO_URUT],
                    ['SPPT.KD_JNS_OP', '=', $KD_JNS],
                    ['SPPT.THN_PAJAK_SPPT', '=', $TAHUNPAJAK],
                ])->get();

            $sql_tunggakan = Sppt::select(
                'SPPT.THN_PAJAK_SPPT',
                'SPPT.PBB_YG_HARUS_DIBAYAR_SPPT',
                'SPPT.TGL_JATUH_TEMPO_SPPT'
            )
                ->where([
                    ['SPPT.KD_PROPINSI', '=', $KD_PROV],
                    ['SPPT.KD_DATI2', '=', $KD_DATI2],
                    ['SPPT.KD_KECAMATAN', '=', $KD_KEC],
                    ['SPPT.KD_KELURAHAN', '=', $KD_KEL],
                    ['SPPT.KD_BLOK', '=', $KD_BLOK],
                    ['SPPT.NO_URUT', '=', $NO_URUT],
                    ['SPPT.KD_JNS_OP', '=', $KD_JNS],
                    ['SPPT.THN_PAJAK_SPPT', '<=', $TAHUNPAJAK],
                    ['SPPT.STATUS_PEMBAYARAN_SPPT', '=', '0'],
                ])
                ->orderBy('THN_PAJAK_SPPT', 'ASC')->get();
            if ($query->count() == 0) {
                $response = 'Data Tidak Ditemukan';
            } else {

                $response = '<div class="col-12">
                 <div class="input-group input-group-sm">
                   <label class="col-2">Nama WP SPPT</label>
                   <div class="col-4">: ' . $query[0]->nm_wp_sppt . '</div>
                 </div>
            </div>';
                $response .= '<div class="col-12">
                 <div class="input-group input-group-sm">
                   <label class="col-2">Letak</label> <div class="col-4">: ' . $query[0]->jln_wp_sppt . '</div>
                   <label class="col-2">RT/RW</label> <div class="col-4">: ' . $query[0]->rt_wp_sppt . '/' . $query[0]->rw_wp_sppt . '</div>
                 </div>
            </div>';
                $response .= '<div class="col-12">
                 <div class="input-group input-group-sm">
                   <label class="col-2">Kelurahan</label> <div class="col-4">: ' . $query[0]->nm_kelurahan . '</div>
                   <label class="col-2">Kecamatan</label> <div class="col-4">: ' . $query[0]->nm_kecamatan . '</div>
                 </div>
            </div>';
                $response .= '<div class="col-12">
                 <div class="input-group input-group-sm">
                   <label class="col-2">Luas Tanah (M<sup>2</sup>)</label> <div class="col-4">: ' . number_format($query[0]->luas_bumi_sppt, 0, ',', '.') . '</div>
                   <label class="col-2">NJOP Tanah</label> <div class="col-4">: ' . number_format($query[0]->njop_bumi_sppt / $query[0]->luas_bumi_sppt, 0, ',', '.') . '</div>
                 </div>
            </div>';
                $response .= '<div class="col-12">
                 <div class="input-group input-group-sm">
                   <label class="col-2">Luas Bangunan (M<sup>2</sup>)</label> <div class="col-4">: ' . number_format($query[0]->luas_bng_sppt, 0, ',', '.') . '</div>
                   <label class="col-2">NJOP Bangunan</label> <div class="col-4">: ' . number_format($query[0]->njop_bng_sppt / $query[0]->luas_bng_sppt, 0, ',', '.') . '</div>
                 </div>
            </div>';
                $response .= '<div class="col-12 table-responsive" style="border:1px solid #ccc;">
                <table class="table table-hover table-striped">
                    <thead>
                        <tr class="bg-primary">
                            <th colspan="5" style="font-size:12pt;">TUNGGAKAN SPPT-PBB</th>
                        </tr>
                        <tr>
                            <th>NO.</th>
                            <th>TAHUN</th>
                            <th>JMLH TUNGGAKAN (Rp)</th>
                            <th>TGL JATUH TEMPO</th>
                            <th>JMLH DENDA (Rp)</th>
                        </tr>
                    </thead>
                    <tbody>';
                if ($sql_tunggakan->count() == 0) {
                    $response .= '<tr><td colspan="5">Tidak Ada Tunggakan!</td></tr>';
                }
                $counter = 1;
                $jmlh_tunggakan = 0;
                $jmlh_denda = 0;
                foreach ($sql_tunggakan as $v) {
                    $tgljatuhtempo = date('Y-m-d', strtotime($v['tgl_jatuh_tempo_sppt']));
                    $tglsekarang = date('Y-m-d');
                    $ts1 = strtotime($tgljatuhtempo);
                    $ts2 = strtotime($tglsekarang);

                    $year1 = date('Y', $ts1);
                    $year2 = date('Y', $ts2);

                    $month1 = date('m', $ts1);
                    $month2 = date('m', $ts2);

                    $day1 = date('d', $ts1);
                    $day2 = date('d', $ts2);
                    if ($day1 < $day2) {
                        $tambahanbulan = 1;
                    } else {
                        $tambahanbulan = 0;
                    }

                    $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
                    if ($jmlbulan > 24) {
                        $jmlbulan = 24;
                        $jmldenda = $jmlbulan * 2 / 100 * $v['pbb_yg_harus_dibayar_sppt'];
                    } elseif ($jmlbulan <= 0) {
                        $jmlbulan = 0;
                        $jmldenda = $jmlbulan * 2 / 100 * $v['pbb_yg_harus_dibayar_sppt'];
                    } else {
                        $jmldenda = $jmlbulan * 2 / 100 * $v['pbb_yg_harus_dibayar_sppt'];
                    }
                    $response .= '<tr>
                            <td>' . $counter++ . '</td>
                            <td class="text-center">' . $v['thn_pajak_sppt'] . '</td>
                            <td class="text-right">' . number_format($v['pbb_yg_harus_dibayar_sppt'], 0, ',', '.') . '</td>
                            <td class="text-center">' . date('d-m-Y', strtotime($v['tgl_jatuh_tempo_sppt'])) . '</td>
                            <td class="text-right">' . number_format($jmldenda, 0, ',', '.') . '</td>
                        </tr>';
                    $jmlh_tunggakan += $v['pbb_yg_harus_dibayar_sppt'];
                    $jmlh_denda += $jmldenda;
                }

                $total_tunggakan = ($jmlh_tunggakan + $jmlh_denda);
                $response .= '</tbody>
                    <tfoot>
                        <tr class="bg-gray">
                            <th colspan="2">JUMLAH TUNGGAKAN (Rp)</th>
                            <th class="text-right">' . number_format($jmlh_tunggakan, 0, ',', '.') . '</th>
                            <th class="text-center">JUMLAH DENDA (Rp)</th>
                            <th class="text-right">' . number_format($jmlh_denda, 0, ',', '.') . '</th>
                        </tr>
                        <tr class="bg-danger">
                            <th colspan="3">TOTAL TUNGGAKAN (Rp)</th>
                            <th colspan="2" class="text-center" style="font-size:14pt;">' . number_format($total_tunggakan, 0, ',', '.') . '</th>
                        </tr>
                    </tfoot>
                </table>
            </div>';
            }
        } elseif ($this->t_cariberdasarkan == 'NIK') {
            $response = 'OKE NIK BERHASIL DI AMBIL';
        } elseif ($this->t_cariberdasarkan == 'NPWPD') {
            $response = 'OKE NPWPD BERHASIL DI AMBIL';
        }
        return view('informasiop.exports.export', [
            'infomasiop' => $response
        ]);
    }
}
