<?php

namespace App\Exports\Pelaporan;

use App\Models\Pelaporan\InputAjb;
use Carbon\Carbon;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Illuminate\Contracts\View\View;

class InputAjbExport implements FromView, WithEvents
{
    protected $no_daftar;
    protected $tgl_daftar;
    protected $jenis_peralihan;
    protected $nama_wp;
    protected $no_ajb;
    protected $tgl_ajb;

    function __construct($no_daftar, $tgl_daftar, $jenis_peralihan, $nama_wp, $no_ajb, $tgl_ajb)
    {
        $this->no_daftar = $no_daftar;
        $this->tgl_daftar = $tgl_daftar;
        $this->nama_wp = $nama_wp;
        $this->jenis_peralihan = $jenis_peralihan;
        $this->no_ajb = $no_ajb;
        $this->tgl_ajb = $tgl_ajb;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class    => function (AfterSheet $event) {
                $event->sheet->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = InputAjb::with('spt')->orderBy('t_idajb');

        if (!empty($this->no_daftar)) {
            $nodaftar = $this->no_daftar;
            $response = $response->with(['spt' => function ($q) use ($nodaftar) {
                $q->where('t_kohirspt', $nodaftar);
            }]);
        }

        if (!empty($this->tgl_daftar)) {
            $tgl_daftars = Carbon::parse($this->tgl_daftar)->format('Y-m-d');
            $response = $response->with(['spt' => function ($query) use ($tgl_daftars) {
                $query->where('t_tgldaftar_spt', $tgl_daftars);
            }]);
        }
        
        if (!empty($this->jenis_peralihan)) {
            $t_idjenistransaksi = $this->jenis_peralihan;
            $response = $response->with(['spt' => function ($query) use ($t_idjenistransaksi) {
                $query->where('t_idjenistransaksi', $t_idjenistransaksi);
            }]);
        }
        
        if (!empty($this->nama_wp)) {
            $wp_nama = $this->nama_wp;
            $response = $response->with(['spt' => function ($query) use ($wp_nama) {
                $query->where('t_nama_pembeli', 'ilike', '%'. $wp_nama .'%');
            }]);
        }

        if (!empty($this->no_ajb)) {
            $response = $response->where('t_no_ajb', 'ilike', '%' . $this->no_ajb . '%');
        }

        if (!empty($this->tgl_ajb)) {
            $tgl_ajbs = Carbon::parse($this->tgl_ajb)->format('Y-m-d');
            $response = $response->where('t_tgl_ajb', $tgl_ajbs);
        }

        $response = $response->get();

        return view('pelaporan-ajb.exports.export', [
            'ajb' => $response
        ]);
    }
}
