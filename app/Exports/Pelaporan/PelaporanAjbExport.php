<?php

namespace App\Exports\Pelaporan;

use App\Models\Pelaporan\LaporBulananHead;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;
use Maatwebsite\Excel\Concerns\WithEvents;

;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;

class PelaporanAjbExport implements FromView, WithEvents, ShouldAutoSize
{
    protected $tgl_lapor;
    protected $nama_wp;
    protected $lapor_untuk;
    protected $jenis_peralihan;

    function __construct($tgl_lapor, $nama_wp, $lapor_untuk, $jenis_peralihan)
    {
        $this->tgl_lapor = $tgl_lapor;
        $this->nama_wp = $nama_wp;
        $this->lapor_untuk = $lapor_untuk;
        $this->jenis_peralihan = $jenis_peralihan;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class  => function (BeforeExport $event) {
                $event->writer->setCreator('Patrick');
            },
            AfterSheet::class => function (AfterSheet $event) {
                $event->sheet->setPaperSize(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::PAPERSIZE_A4);
                $event->sheet->getPageSetup()->setOrientation(\PhpOffice\PhpSpreadsheet\Worksheet\PageSetup::ORIENTATION_LANDSCAPE);
            },
        ];
    }

    public function view(): View
    {
        $response = LaporBulananHead::with('inputAjbs')->orderBy('t_idlaporbulanan');

        if (!empty($this->tgl_lapor)) {
            $tgl = Carbon::parse($this->tgl_lapor)->format('Y-m-d');
            $response = $response->where('t_tgl_lapor', $tgl);
        }

        if (!empty($this->nama_wp)) {
            $nama = $this->nama_wp;
            $response = $response->with(['inputAjbs' => function ($query) use ($nama) {
                $query->with(['spt' => function ($q) use ($nama) {
                    $q->where('t_nama_pembeli', 'ilike', '%'. $nama .'%');
                }]);
            }]);
        }

        if (!empty($this->jenis_peralihan)) {
            $jenis_peralihan_hak = $this->jenis_peralihan;
            $response = $response->with(['inputAjbs' => function ($query) use ($jenis_peralihan_hak) {
                $query->with(['spt' => function ($q) use ($jenis_peralihan_hak) {
                    $q->where('t_idjenistransaksi', $jenis_peralihan_hak);
                }]);
            }]);
        }

        if (!empty($this->lapor_untuk)) {
            $exploded = explode("-", $this->lapor_untuk);
            $bulan = str_replace("0", "", $exploded[0]);
            $tahun = $exploded[1];

            $response = $response->where('t_untuk_bulan', $bulan);
            $response = $response->where('t_untuk_tahun', $tahun);
        }

        $response = $response->get();

        return view('pelaporan-bulanan-ajb.exports.export', [
            'laporan_ajb' => $response
        ]);
    }
}
