<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTinputajbTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_inputajb', function (Blueprint $table) {
            $table->increments('t_idajb');
			$table->integer('t_idspt')->nullable();
			$table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
			$table->timestamp('t_tgl_ajb')->nullable();
            $table->string('t_no_ajb');
			$table->integer('t_iduser_input')->nullable();
			$table->foreign('t_iduser_input')->references('id')->on('users');
            $table->timestamps();
        });
		
		
		Schema::create('t_laporbulanan_head', function (Blueprint $table) {
            $table->increments('t_idlaporbulanan');
			$table->integer('t_no_lapor')->nullable();
			$table->timestamp('t_tgl_lapor')->nullable();
			$table->integer('t_untuk_bulan')->nullable();
			$table->integer('t_untuk_tahun')->nullable();
            $table->text('t_keterangan');
			$table->integer('t_iduser_input')->nullable();
			$table->foreign('t_iduser_input')->references('id')->on('users');
            $table->timestamps();
        });
		
		
		Schema::create('t_laporbulanan_detail', function (Blueprint $table) {
           
			$table->integer('t_idlaporbulanan')->nullable();
			$table->foreign('t_idlaporbulanan')->references('t_idlaporbulanan')->on('t_laporbulanan_head');
			$table->integer('t_idajb')->nullable();
			$table->foreign('t_idajb')->references('t_idajb')->on('t_inputajb');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_inputajb');
		Schema::dropIfExists('t_laporbulanan_notaris');
		Schema::dropIfExists('t_laporbulanan_detail');
    }
}
