<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTvalidasiberkasTable extends Migration {

    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up() {

        /* MENU VALIDASI BERKAS */

        Schema::create('s_status_berkas', function (Blueprint $table) {
            $table->increments('s_id_status_berkas');
            $table->string('s_nama_status_berkas')->nullable();
            $table->timestamps();
        });

        Schema::create('s_status_lihat', function (Blueprint $table) {
            $table->increments('s_id_status_lihat');
            $table->string('s_nama_status_lihat')->nullable();
            $table->timestamps();
        });

        Schema::create('t_validasi_berkas', function (Blueprint $table) {
            $table->increments('t_id_validasi_berkas');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('s_id_status_berkas')->nullable();
            $table->foreign('s_id_status_berkas')->references('s_id_status_berkas')->on('s_status_berkas');
            $table->timestamp('t_tglvalidasi')->nullable();
            $table->integer('t_iduser_validasi')->nullable();
            $table->foreign('t_iduser_validasi')->references('id')->on('users');
            $table->text('t_keterangan_berkas')->nullable();
            $table->timestamps();
        });

        Schema::create('t_notif_validasi_berkas', function (Blueprint $table) {
            $table->increments('t_id_notif_berkas');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->text('t_isipesan')->nullable();
            $table->integer('t_dari')->nullable();
            $table->foreign('t_dari')->references('id')->on('users');
            $table->timestamp('t_tglkirim')->nullable();
            $table->integer('t_untuk')->nullable();
            $table->foreign('t_untuk')->references('id')->on('users');
            $table->integer('s_id_status_lihat')->nullable();
            $table->foreign('s_id_status_lihat')->references('id')->on('users');
            $table->timestamps();
        });

        /* END MENU VALIDASI BERKAS */

        /*  MENU VALIDASI KABID */

        Schema::create('s_status_kabid', function (Blueprint $table) {
            $table->increments('s_id_status_kabid');
            $table->string('s_nama_status_kabid')->nullable();
            $table->timestamps();
        });

        Schema::create('t_validasi_kabid', function (Blueprint $table) {
            $table->increments('t_id_validasi_kabid');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('s_id_status_kabid')->nullable();
            $table->foreign('s_id_status_kabid')->references('s_id_status_kabid')->on('s_status_kabid');
            $table->timestamp('t_tglvalidasi')->nullable();
            $table->integer('t_iduser_validasi')->nullable();
            $table->foreign('t_iduser_validasi')->references('id')->on('users');
            $table->text('t_keterangan_kabid')->nullable();
            $table->timestamps();
        });

        Schema::create('t_notif_validasi_kabid', function (Blueprint $table) {
            $table->increments('t_id_notif_kabid');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->text('t_isipesan')->nullable();
            $table->integer('t_dari')->nullable();
            $table->foreign('t_dari')->references('id')->on('users');
            $table->timestamp('t_tglkirim')->nullable();
            $table->integer('t_untuk')->nullable();
            $table->foreign('t_untuk')->references('id')->on('users');
            $table->integer('s_id_status_lihat')->nullable();
            $table->foreign('s_id_status_lihat')->references('id')->on('users');
            $table->timestamps();
        });

        /* END MENU VALIDASI KABID */

        /* MENU PEMBAYARAN */

        Schema::create('s_status_bayar', function (Blueprint $table) {
            $table->increments('s_id_status_bayar');
            $table->string('s_nama_status_bayar')->nullable();
            $table->timestamps();
        });

        Schema::create('t_pembayaran_bphtb', function (Blueprint $table) {
            $table->increments('t_id_pembayaran');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_nourut_bayar')->nullable();
            $table->integer('s_id_status_bayar')->nullable();
            $table->foreign('s_id_status_bayar')->references('s_id_status_bayar')->on('s_status_bayar');

            $table->integer('t_iduser_bayar')->nullable();
            $table->foreign('t_iduser_bayar')->references('id')->on('users');
            $table->timestamp('t_tglpembayaran_pokok')->nullable();
            $table->float('t_jmlhbayar_pokok')->nullable();
            $table->timestamp('t_tglpembayaran_denda')->nullable();
            $table->float('t_jmlhbayar_denda')->nullable();
            $table->float('t_jmlhbayar_total')->nullable();
            $table->timestamps();
        });

        /* END MENU PEMBAYARAN */

        /* MENU PEMBAYARAN SKPDKB */

        Schema::create('t_pembayaran_skpdkb', function (Blueprint $table) {
            $table->increments('t_id_bayar_skpdkb');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_idskpdkb')->nullable();
            $table->foreign('t_idskpdkb')->references('t_idskpdkb')->on('t_skpdkb');
            $table->integer('t_nourut_bayar')->nullable();
            $table->integer('s_id_status_bayar')->nullable();
            $table->foreign('s_id_status_bayar')->references('s_id_status_bayar')->on('s_status_bayar');

            $table->integer('t_iduser_bayar')->nullable();
            $table->foreign('t_iduser_bayar')->references('id')->on('users');
            $table->timestamp('t_tglpembayaran_skpdkb')->nullable();
            $table->float('t_jmlhbayar_skpdkb')->nullable();
            $table->timestamp('t_tglpembayaran_denda')->nullable();
            $table->float('t_jmlhbayar_denda')->nullable();
            $table->float('t_jmlhbayar_total')->nullable();
            $table->timestamps();
        });

        /* END MENU PEMBAYARAN SKPDKB */


        /* MENU PEMBAYARAN SKPDKB */

        Schema::create('t_pembayaran_skpdkbt', function (Blueprint $table) {
            $table->increments('t_id_bayar_skpdkb');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_idskpdkbt')->nullable();
            $table->foreign('t_idskpdkbt')->references('t_idskpdkb')->on('t_skpdkb');
            $table->integer('t_nourut_bayar')->nullable();
            $table->integer('s_id_status_bayar')->nullable();
            $table->foreign('s_id_status_bayar')->references('s_id_status_bayar')->on('s_status_bayar');

            $table->integer('t_iduser_bayar')->nullable();
            $table->foreign('t_iduser_bayar')->references('id')->on('users');
            $table->timestamp('t_tglpembayaran_skpdkbt')->nullable();
            $table->float('t_jmlhbayar_skpdkbt')->nullable();
            $table->timestamp('t_tglpembayaran_denda')->nullable();
            $table->float('t_jmlhbayar_denda')->nullable();
            $table->float('t_jmlhbayar_total')->nullable();
            $table->timestamps();
        });

        /* END MENU PEMBAYARAN SKPDKB */

        /* MENU PENETAPAN DENDA NOTARIS 250rb */

        Schema::create('t_pen_denda_lpor_notaris', function (Blueprint $table) {
            $table->increments('t_id_pendenda');
            $table->integer('t_idlaporbulanan')->nullable();
            $table->foreign('t_idlaporbulanan')->references('t_idlaporbulanan')->on('t_laporbulanan_head');
            $table->integer('t_idnotaris')->nullable();
            $table->foreign('t_idnotaris')->references('s_idnotaris')->on('s_notaris');
            $table->timestamp('t_tglpenetapan')->nullable();
            $table->integer('t_nourut_pendenda')->nullable();
            $table->float('t_nilai_pendenda')->nullable();
            $table->integer('t_iduser_buat')->nullable();
            $table->foreign('t_iduser_buat')->references('id')->on('users');


            $table->integer('s_id_status_bayar')->nullable();
            $table->foreign('s_id_status_bayar')->references('s_id_status_bayar')->on('s_status_bayar');
            $table->timestamp('t_tglbayar_pendenda')->nullable();
            $table->float('t_jmlhbayar_pendenda')->nullable();

            $table->timestamps();
        });

        /* END MENU PENETAPAN DENDA NOTARIS 250rb */


        /* MENU PENETAPAN DENDA TTD AJB NOTARIS LEBIH AWAL DARI TGL BAYAR */

        Schema::create('t_pen_denda_ajb_notaris', function (Blueprint $table) {
            $table->increments('t_id_ajbdenda');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_idnotaris')->nullable();
            $table->foreign('t_idnotaris')->references('s_idnotaris')->on('s_notaris');
            $table->timestamp('t_tglpenetapan')->nullable();
            $table->integer('t_nourut_ajbdenda')->nullable();
            $table->float('t_nilai_ajbdenda')->nullable();
            $table->integer('t_iduser_buat')->nullable();
            $table->foreign('t_iduser_buat')->references('id')->on('users');


            $table->integer('s_id_status_bayar')->nullable();
            $table->foreign('s_id_status_bayar')->references('s_id_status_bayar')->on('s_status_bayar');
            $table->timestamp('t_tglbayar_ajbdenda')->nullable();
            $table->float('t_jmlhbayar_ajbdenda')->nullable();

            $table->timestamps();
        });

        /* END MENU PENETAPAN DENDA TTD AJB NOTARIS LEBIH AWAL DARI TGL BAYAR */
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('s_status_berkas');
        Schema::dropIfExists('s_status_lihat');
        Schema::dropIfExists('t_validasi_berkas');
        Schema::dropIfExists('t_notif_validasi_berkas');

        Schema::dropIfExists('s_status_kabid');
        Schema::dropIfExists('t_validasi_kabid');
        Schema::dropIfExists('t_notif_validasi_kabid');

        Schema::dropIfExists('s_status_bayar');
        Schema::dropIfExists('t_pembayaran_bphtb');

        Schema::dropIfExists('t_pembayaran_skpdkb');
        Schema::dropIfExists('t_pembayaran_skpdkbt');

        Schema::dropIfExists('t_pen_denda_lpor_notaris');
        Schema::dropIfExists('t_pen_denda_ajb_notaris');
    }

}
