<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSBackgroundTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_background', function (Blueprint $table) {
            $table->id();
            $table->text('s_thumbnail')->nullable();
			$table->integer('s_id_status');	
			$table->foreign('s_id_status')->references('s_id_status')->on('s_status');
			$table->integer('s_iduser')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_background');
    }
}
