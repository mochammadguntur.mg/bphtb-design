<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateIntegrasisertifikatbpnTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('integrasi_sertifikat_bpn', function (Blueprint $table) {
            $table->increments('t_idsertifikat');
            $table->string('aktaid');
            $table->timestamp('tgl_akta');
            $table->string('nop');
            $table->string('nib');
            $table->string('nik');
			$table->string('npwp');
			$table->string('nama_wp');
			$table->string('alamat_op');
			$table->string('kelurahan_op');
			$table->string('kecamatan_op');
			$table->string('kota_op');
			$table->float('luastanah_op', 8, 2)->nullable();
			$table->float('luasbangunan_op', 8, 2)->nullable();
			$table->string('ppat');
			$table->string('no_sertipikat');
			$table->string('no_akta');
			$table->integer('t_iduser')->nullable();
			$table->timestamp('t_tgltransaksi');
			
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('integrasi_sertifikat_bpn');
    }
}
