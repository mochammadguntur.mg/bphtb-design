<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStargetTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_target_status', function (Blueprint $table) {
            $table->increments('s_id_target_status');
            $table->string('s_nama_status');
            
            $table->timestamps();
        });
		
		Schema::create('s_target_bphtb', function (Blueprint $table) {
            $table->increments('s_id_target_bphtb');
			$table->integer('s_id_target_status')->nullable();
			$table->foreign('s_id_target_status')->references('s_id_target_status')->on('s_target_status');
			$table->integer('s_tahun_target')->nullable();
			$table->float('s_nilai_target', 8, 2)->nullable();
            $table->string('s_keterangan');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_target_status');
		Schema::dropIfExists('s_target_bphtb');
    }
}
