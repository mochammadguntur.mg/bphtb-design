<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHisgetpbbbpnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('his_getpbb_bpn', function (Blueprint $table) {
            $table->increments('t_idhis_pbbbpn');
            $table->string('t_username');
			$table->string('t_password');
            $table->string('t_nop');
            $table->string('t_nik');
			$table->string('t_nama_wp');
			$table->text('t_alamat_op');
			$table->string('t_kecamatan_op');
			$table->string('t_kelurahan_op');
			$table->string('t_kota_kab_op');
			$table->float('t_luas_tanah_op', 8, 2)->nullable();
			$table->float('t_luas_bangunan_op', 8, 2)->nullable();
			$table->float('t_njop_tanah_op', 8, 2)->nullable();
			$table->float('t_njop_bangunan_op', 8, 2)->nullable();
			$table->string('t_status_tunggakan');
			$table->integer('t_iduser')->nullable();
			$table->timestamp('t_tglby_system');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('his_getpbb_bpn');
    }
}
