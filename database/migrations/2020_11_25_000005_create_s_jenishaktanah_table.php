<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSJenishaktanahTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('s_jenishaktanah', function (Blueprint $table) {
            $table->increments('s_idhaktanah');
            $table->string('s_kodehaktanah', 10)->nullable();
            $table->text('s_namahaktanah')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_jenishaktanah');
    }
}
