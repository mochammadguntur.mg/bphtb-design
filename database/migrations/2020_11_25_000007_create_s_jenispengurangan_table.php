<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSJenispenguranganTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('s_jenispengurangan', function (Blueprint $table) {
            $table->increments('s_idpengurangan');
            $table->integer('s_persentase')->nullable();
            $table->text('s_namapengurangan')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_jenispengurangan');
    }
}
