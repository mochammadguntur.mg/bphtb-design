<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnToValidasiBerkas extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('t_validasi_berkas', function (Blueprint $table) {
            $table->string("t_persyaratan_validasi_berkas")->nullable()->after("t_iduser_validasi");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('t_validasi_berkas', function (Blueprint $table) {
            $table->dropColumn("t_persyaratan_validasi_berkas");
        });
    }
}
