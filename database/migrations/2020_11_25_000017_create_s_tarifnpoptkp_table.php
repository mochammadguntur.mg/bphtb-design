<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSTarifnpoptkpTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('s_tarifnpoptkp', function (Blueprint $table) {
            $table->increments('s_idtarifnpoptkp');
			$table->integer('s_idjenistransaksinpoptkp')->nullable();
			$table->foreign('s_idjenistransaksinpoptkp')->references('s_idjenistransaksi')->on('s_jenistransaksi');
            $table->float('s_tarifnpoptkp', 8, 2)->nullable();
            $table->float('s_tarifnpoptkptambahan', 8, 2)->nullable();
			$table->text('s_dasarhukumnpoptkp')->nullable();
			$table->integer('s_statusnpoptkp')->nullable();
			$table->foreign('s_statusnpoptkp')->references('s_id_status')->on('s_status');
			$table->date('s_tglberlaku_awal')->nullable();
			$table->date('s_tglberlaku_akhir')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_tarifnpoptkp');
    }
}
