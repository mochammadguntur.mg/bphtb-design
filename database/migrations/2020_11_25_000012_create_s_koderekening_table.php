<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSKoderekeningTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('s_koderekening', function (Blueprint $table) {
            $table->increments('s_korekid');
            $table->string('s_korektipe', 2);
            $table->string('s_korekkelompok', 2);
			$table->string('s_korekjenis', 2);
			$table->string('s_korekobjek', 4);
			$table->string('s_korekrincian', 4);
			$table->string('s_korekrinciansub', 10);
			$table->string('s_koreknama');
			$table->text('s_korekketerangan')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_koderekening');
    }
}
