<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSAcuanTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_acuan', function (Blueprint $table) {
            $table->increments('s_idacuan');
            $table->string('s_kd_propinsi', 2)->nullable();
            $table->string('s_kd_dati2', 2)->nullable();
            $table->string('s_kd_kecamatan', 3)->nullable();
            $table->string('s_kd_kelurahan', 3)->nullable();
            $table->string('s_kd_blok', 3)->nullable();
			$table->float('s_permetertanah', 8, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_acuan');
    }
}
