<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTSptTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('s_jenis_bidangusaha', function (Blueprint $table) {
            $table->increments('s_idbidang_usaha');
            $table->string('s_nama_bidangusaha')->nullable();
            $table->timestamps();
        });

        Schema::create('s_status_npwpd', function (Blueprint $table) {
            $table->increments('s_idstatus_npwpd');
            $table->string('s_nama_status')->nullable();
            $table->timestamps();
        });

        Schema::create('t_spt', function (Blueprint $table) {
            $table->increments('t_idspt');
            $table->uuid('t_uuidspt');
            $table->integer('t_kohirspt')->nullable();
            $table->timestamp('t_tgldaftar_spt')->nullable();
            $table->timestamp('t_tglby_system')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('t_periodespt')->nullable();
            $table->integer('t_iduser_buat')->nullable();
            $table->foreign('t_iduser_buat')->references('id')->on('users');

            $table->integer('t_idjenistransaksi')->nullable();
            $table->foreign('t_idjenistransaksi')->references('s_idjenistransaksi')->on('s_jenistransaksi');
            $table->integer('t_idnotaris_spt')->nullable();
            $table->foreign('t_idnotaris_spt')->references('s_idnotaris')->on('s_notaris');
            $table->string('t_npwpd')->nullable();
            $table->integer('t_idstatus_npwpd')->nullable();
            $table->foreign('t_idstatus_npwpd')->references('s_idstatus_npwpd')->on('s_status_npwpd');
            $table->integer('t_idbidang_usaha')->nullable();
            $table->foreign('t_idbidang_usaha')->references('s_idbidang_usaha')->on('s_jenis_bidangusaha');

            $table->string('t_nama_pembeli')->nullable();
            $table->string('t_nik_pembeli')->nullable();
            $table->string('t_npwp_pembeli')->nullable();
            $table->text('t_jalan_pembeli')->nullable();
            $table->string('t_kabkota_pembeli')->nullable();

            $table->integer('t_idkec_pembeli')->nullable();
            $table->foreign('t_idkec_pembeli')->references('s_idkecamatan')->on('s_kecamatan');
            $table->string('t_namakecamatan_pembeli')->nullable();

            $table->integer('t_idkel_pembeli')->nullable();
            $table->foreign('t_idkel_pembeli')->references('s_idkelurahan')->on('s_kelurahan');
            $table->string('t_namakelurahan_pembeli')->nullable();

            $table->string('t_rt_pembeli')->nullable();
            $table->string('t_rw_pembeli')->nullable();
            $table->string('t_nohp_pembeli')->nullable();
            $table->string('t_notelp_pembeli')->nullable();
            $table->string('t_email_pembeli')->nullable();
            $table->string('t_kodepos_pembeli')->nullable();

            $table->string('t_siup_pembeli')->nullable();
            $table->string('t_nib_pembeli')->nullable();
            $table->string('t_ketdomisili_pembeli')->nullable();

            //$table->string('t_b_nama_pembeli')->nullable();
            //$table->string('t_b_npwp_pembeli')->nullable();
            //$table->text('t_b_jalan_pembeli')->nullable();
            //$table->string('t_b_kabkota_pembeli')->nullable();
            //$table->integer('t_b_idkec_pembeli')->nullable();
            //$table->foreign('t_b_idkec_pembeli')->references('s_idkecamatan')->on('s_kecamatan');
            //$table->string('t_b_namakecamatan_pembeli')->nullable();
            //$table->integer('t_b_idkel_pembeli')->nullable();
            //$table->foreign('t_b_idkel_pembeli')->references('s_idkelurahan')->on('s_kelurahan');
            //$table->string('t_b_namakelurahan_pembeli')->nullable();
            //$table->string('t_b_rt_pembeli')->nullable();
            //$table->string('t_b_rw_pembeli')->nullable();
            //$table->string('t_b_nohp_pembeli')->nullable();
            //$table->string('t_b_email_pembeli')->nullable();
            //$table->string('t_b_kodepos_pembeli')->nullable();


            $table->string('t_b_nama_pngjwb_pembeli')->nullable();
            $table->string('t_b_nik_pngjwb_pembeli')->nullable();
            $table->string('t_b_npwp_pngjwb_pembeli')->nullable();
            $table->string('t_b_statusjab_pngjwb_pembeli')->nullable();
            $table->text('t_b_jalan_pngjwb_pembeli')->nullable();
            $table->string('t_b_kabkota_pngjwb_pembeli')->nullable();

            $table->integer('t_b_idkec_pngjwb_pembeli')->nullable();
            $table->foreign('t_b_idkec_pngjwb_pembeli')->references('s_idkecamatan')->on('s_kecamatan');
            $table->string('t_b_namakec_pngjwb_pembeli')->nullable();

            $table->integer('t_b_idkel_pngjwb_pembeli')->nullable();
            $table->foreign('t_b_idkel_pngjwb_pembeli')->references('s_idkelurahan')->on('s_kelurahan');
            $table->string('t_b_namakel_pngjwb_pembeli')->nullable();

            $table->string('t_b_rt_pngjwb_pembeli')->nullable();
            $table->string('t_b_rw_pngjwb_pembeli')->nullable();
            $table->string('t_b_nohp_pngjwb_pembeli')->nullable();
            $table->string('t_b_notelp_pngjwb_pembeli')->nullable();
            $table->string('t_b_email_pngjwb_pembeli')->nullable();
            $table->string('t_b_kodepos_pngjwb_pembeli')->nullable();


            $table->string('t_nop_sppt')->nullable();
            $table->string('t_tahun_sppt')->nullable();
            $table->string('t_nama_sppt')->nullable();
            $table->string('t_jalan_sppt')->nullable();
            $table->string('t_kabkota_sppt')->nullable();
            $table->string('t_kecamatan_sppt')->nullable();
            $table->string('t_kelurahan_sppt')->nullable();
            $table->string('t_rt_sppt')->nullable();
            $table->string('t_rw_sppt')->nullable();

            $table->float('t_luastanah_sismiop')->nullable();
            $table->float('t_luasbangunan_sismiop')->nullable();
            $table->float('t_njoptanah_sismiop')->nullable();
            $table->float('t_njopbangunan_sismiop')->nullable();

            $table->float('t_luastanah')->nullable();
            $table->float('t_njoptanah')->nullable();
            $table->float('t_totalnjoptanah')->nullable();

            $table->float('t_luasbangunan')->nullable();
            $table->float('t_njopbangunan')->nullable();
            $table->float('t_totalnjopbangunan')->nullable();

            $table->float('t_grandtotalnjop')->nullable();
            $table->float('t_nilaitransaksispt')->nullable();
            $table->float('t_tarif_pembagian_aphb_kali', 8, 2)->nullable();
            $table->float('t_tarif_pembagian_aphb_bagi', 8, 2)->nullable();
            $table->float('t_permeter_tanah')->nullable();

            $table->integer('t_idjenisfasilitas')->nullable();
            $table->foreign('t_idjenisfasilitas')->references('s_idjenisfasilitas')->on('s_jenisfasilitas');
            $table->integer('t_idjenishaktanah')->nullable();
            $table->foreign('t_idjenishaktanah')->references('s_idhaktanah')->on('s_jenishaktanah');
            $table->integer('t_idjenisdoktanah')->nullable();
            $table->foreign('t_idjenisdoktanah')->references('s_iddoktanah')->on('s_jenisdoktanah');
            $table->integer('t_idpengurangan')->nullable();
            $table->foreign('t_idpengurangan')->references('s_idpengurangan')->on('s_jenispengurangan');
            $table->integer('t_id_jenistanah')->nullable();
            $table->foreign('t_id_jenistanah')->references('id_jenistanah')->on('s_jenistanah');
            $table->string('t_nosertifikathaktanah')->nullable();
            $table->timestamp('t_tgldok_tanah')->nullable();

            $table->string('s_latitude')->nullable();
            $table->string('s_longitude')->nullable();

            $table->integer('t_idtarifbphtb')->nullable();
            $table->foreign('t_idtarifbphtb')->references('s_idtarifbphtb')->on('s_tarif_bphtb');
            $table->float('t_persenbphtb', 8, 2)->nullable();
            $table->float('t_npop_bphtb', 8, 2)->nullable();
            $table->float('t_npoptkp_bphtb', 8, 2)->nullable();
            $table->float('t_npopkp_bphtb', 8, 2)->nullable();
            $table->float('t_nilai_bphtb_fix', 8, 2)->nullable();
            $table->float('t_nilai_bphtb_real', 8, 2)->nullable();

            $table->integer('t_idbidang_penjual')->nullable();
            $table->foreign('t_idbidang_penjual')->references('s_idbidang_usaha')->on('s_jenis_bidangusaha');
            $table->string('t_nama_penjual')->nullable();
            $table->string('t_nik_penjual')->nullable();
            $table->string('t_npwp_penjual')->nullable();
            $table->text('t_jalan_penjual')->nullable();
            $table->string('t_kabkota_penjual')->nullable();

            $table->integer('t_idkec_penjual')->nullable();
            $table->foreign('t_idkec_penjual')->references('s_idkecamatan')->on('s_kecamatan');
            $table->string('t_namakec_penjual')->nullable();

            $table->integer('t_idkel_penjual')->nullable();
            $table->foreign('t_idkel_penjual')->references('s_idkelurahan')->on('s_kelurahan');
            $table->string('t_namakel_penjual')->nullable();

            $table->string('t_rt_penjual')->nullable();
            $table->string('t_rw_penjual')->nullable();
            $table->string('t_nohp_penjual')->nullable();
            $table->string('t_notelp_penjual')->nullable();
            $table->string('t_email_penjual')->nullable();
            $table->string('t_kodepos_penjual')->nullable();

            $table->string('t_siup_penjual')->nullable();
            $table->string('t_nib_penjual')->nullable();
            $table->string('t_ketdomisili_penjual')->nullable();


            //$table->string('t_b_nama_penjual')->nullable();
            //$table->string('t_b_siup_penjual')->nullable();
            //$table->string('t_b_npwp_penjual')->nullable();
            //$table->string('t_b_nib_penjual')->nullable();
            //$table->text('t_b_jalan_penjual')->nullable();
            //$table->string('t_b_kabkota_penjual')->nullable();
            //$table->integer('t_b_idkec_penjual')->nullable();
            //$table->foreign('t_b_idkec_penjual')->references('s_idkecamatan')->on('s_kecamatan');
            //$table->string('t_b_namakec_penjual')->nullable();
            //$table->integer('t_b_idkel_penjual')->nullable();
            //$table->foreign('t_b_idkel_penjual')->references('s_idkelurahan')->on('s_kelurahan');
            //$table->string('t_b_namakel_penjual')->nullable();
            //$table->string('t_b_rt_penjual')->nullable();
            //$table->string('t_b_rw_penjual')->nullable();
            //$table->string('t_b_ketdomisili_penjual')->nullable();
            //$table->string('t_b_nohp_penjual')->nullable();
            //$table->string('t_b_email_penjual')->nullable();
            //$table->string('t_b_kodepos_penjual')->nullable();


            $table->string('t_b_nama_pngjwb_penjual')->nullable();
            $table->string('t_b_nik_pngjwb_penjual')->nullable();
            $table->string('t_b_npwp_pngjwb_penjual')->nullable();
            $table->string('t_b_statusjab_pngjwb_penjual')->nullable();
            $table->text('t_b_jalan_pngjwb_penjual')->nullable();
            $table->string('t_b_kabkota_pngjwb_penjual')->nullable();

            $table->integer('t_b_idkec_pngjwb_penjual')->nullable();
            $table->foreign('t_b_idkec_pngjwb_penjual')->references('s_idkecamatan')->on('s_kecamatan');
            $table->string('t_b_namakec_pngjwb_penjual')->nullable();

            $table->integer('t_b_idkel_pngjwb_penjual')->nullable();
            $table->foreign('t_b_idkel_pngjwb_penjual')->references('s_idkelurahan')->on('s_kelurahan');
            $table->string('t_b_namakel_pngjwb_penjual')->nullable();

            $table->string('t_b_rt_pngjwb_penjual')->nullable();
            $table->string('t_b_rw_pngjwb_penjual')->nullable();
            $table->string('t_b_nohp_pngjwb_penjual')->nullable();
            $table->string('t_b_notelp_pngjwb_penjual')->nullable();
            $table->string('t_b_email_pngjwb_penjual')->nullable();
            $table->string('t_b_kodepos_pngjwb_penjual')->nullable();

            $table->timestamp('t_tglbuat_kodebyar')->nullable();
            $table->integer('t_nourut_kodebayar')->nullable();
            $table->string('t_kodebayar_bphtb')->nullable();
            $table->timestamp('t_tglketetapan_spt')->nullable();
            $table->timestamp('t_tgljatuhtempo_spt')->nullable();

            $table->integer('t_idjenisketetapan')->nullable();
            $table->foreign('t_idjenisketetapan')->references('s_idjenisketetapan')->on('s_jenisketetapan');

            $table->integer('t_idpersetujuan_bphtb')->nullable();
            $table->integer('isdeleted')->nullable();
            $table->integer('t_idoperator_deleted')->nullable();
            $table->foreign('t_idoperator_deleted')->references('id')->on('users');
			$table->timestamp('t_tglbuat_ntpd')->nullable();
			$table->integer('t_nourut_ntpd')->nullable();
			$table->string('t_ntpd')->nullable();

            $table->timestamps();
        });


        Schema::create('t_nik_bersama', function (Blueprint $table) {
            $table->increments('t_id_nik_bersama');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->string('t_nik_bersama')->nullable();
            $table->string('t_nama_bersama')->nullable();
            $table->text('t_nama_jalan')->nullable();
            $table->string('t_rt_bersama')->nullable();
            $table->string('t_rw_bersama')->nullable();
            $table->string('t_kecamatan_bersama')->nullable();
            $table->string('t_kelurahan_bersama')->nullable();
            $table->string('t_kabkota_bersama')->nullable();
            $table->string('t_nohp_bersama')->nullable();
            $table->string('t_kodepos_bersama')->nullable();
            $table->string('t_npwp_bersama')->nullable();
            $table->timestamps();
        });


        Schema::create('t_file_objek', function (Blueprint $table) {
            $table->increments('t_idfile_objek');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');

            $table->integer('t_iduser_upload')->nullable();
            $table->foreign('t_iduser_upload')->references('id')->on('users');
            $table->text('letak_file')->nullable();
            $table->text('nama_file')->nullable();
            $table->timestamp('t_tgl_upload')->nullable();
            $table->text('t_keterangan_file')->nullable();
            $table->timestamps();
        });


        Schema::create('t_filesyarat_bphtb', function (Blueprint $table) {
            $table->increments('t_id_filesyarat');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('s_idpersyaratan')->nullable();
            $table->foreign('s_idpersyaratan')->references('s_idpersyaratan')->on('s_persyaratan');
            $table->integer('s_idjenistransaksi')->nullable();
            $table->foreign('s_idjenistransaksi')->references('s_idjenistransaksi')->on('s_jenistransaksi');
            $table->integer('t_iduser_upload')->nullable();
            $table->foreign('t_iduser_upload')->references('id')->on('users');
            $table->text('letak_file')->nullable();
            $table->text('nama_file')->nullable();
            $table->timestamp('t_tgl_upload')->nullable();
            $table->text('t_keterangan_file')->nullable();
            $table->timestamps();
        });



        Schema::create('t_skpdkb', function (Blueprint $table) {
            $table->increments('t_idskpdkb');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->timestamp('t_tglpenetapan')->nullable();
            $table->timestamp('t_tglby_system')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('t_iduser_buat')->nullable();
            $table->foreign('t_iduser_buat')->references('id')->on('users');

            $table->string('t_nop_sppt')->nullable();
            $table->integer('t_tahun_sppt')->nullable();


            $table->float('t_luastanah_skpdkb')->nullable();
            $table->float('t_njoptanah_skpdkb')->nullable();
            $table->float('t_totalnjoptanah_skpdkb')->nullable();

            $table->float('t_luasbangunan_skpdkb')->nullable();
            $table->float('t_njopbangunan_skpdkb')->nullable();
            $table->float('t_totalnjopbangunan_skpdkb')->nullable();

            $table->float('t_grandtotalnjop_skpdkb')->nullable();
            $table->float('t_nilaitransaksispt_skpdkb')->nullable();
            $table->float('t_trf_aphb_kali_skpdkb', 8, 2)->nullable();
            $table->float('t_trf_aphb_bagi_skpdkb', 8, 2)->nullable();
            $table->float('t_permeter_tanah_skpdkb')->nullable();

            $table->integer('t_idtarifbphtb_skpdkb')->nullable();
            $table->foreign('t_idtarifbphtb_skpdkb')->references('s_idtarifbphtb')->on('s_tarif_bphtb');
            $table->float('t_persenbphtb_skpdkb', 8, 2)->nullable();
            $table->float('t_npop_bphtb_skpdkb', 8, 2)->nullable();
            $table->float('t_npoptkp_bphtb_skpdkb', 8, 2)->nullable();
            $table->float('t_npopkp_bphtb_skpdkb', 8, 2)->nullable();
            $table->float('t_nilai_bayar_sblumnya', 8, 2)->nullable();
            $table->float('t_nilai_pokok_skpdkb', 8, 2)->nullable();

            $table->float('t_jmlh_blndenda', 8, 2)->nullable();
            $table->float('t_jmlh_dendaskpdkb', 8, 2)->nullable();
            $table->float('t_jmlh_totalskpdkb', 8, 2)->nullable();


            $table->timestamp('t_tglbuat_kodebyar')->nullable();
            $table->integer('t_nourut_kodebayar')->nullable();
            $table->string('t_kodebayar_skpdkb')->nullable();
            $table->timestamp('t_tgljatuhtempo_skpdkb')->nullable();

            $table->integer('t_idjenisketetapan')->nullable();
            $table->foreign('t_idjenisketetapan')->references('s_idjenisketetapan')->on('s_jenisketetapan');

            $table->integer('t_idpersetujuan_skpdkb')->nullable();
            $table->integer('isdeleted')->nullable();
            $table->integer('t_idoperator_deleted')->nullable();
            $table->foreign('t_idoperator_deleted')->references('id')->on('users');
			$table->string('t_ntpd_skpdkb')->nullable();

            $table->timestamps();
        });


        Schema::create('t_skpdkbt', function (Blueprint $table) {
            $table->increments('t_idskpdkbt');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_idskpdkb')->nullable();
            $table->foreign('t_idskpdkb')->references('t_idskpdkb')->on('t_skpdkb');
            $table->timestamp('t_tglpenetapan')->nullable();
            $table->timestamp('t_tglby_system')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('t_iduser_buat')->nullable();
            $table->foreign('t_iduser_buat')->references('id')->on('users');

            $table->string('t_nop_sppt')->nullable();
            $table->integer('t_tahun_sppt')->nullable();


            $table->float('t_luastanah_skpdkbt')->nullable();
            $table->float('t_njoptanah_skpdkbt')->nullable();
            $table->float('t_totalnjoptanah_skpdkbt')->nullable();

            $table->float('t_luasbangunan_skpdkbt')->nullable();
            $table->float('t_njopbangunan_skpdkbt')->nullable();
            $table->float('t_totalnjopbangunan_skpdkbt')->nullable();

            $table->float('t_grandtotalnjop_skpdkbt')->nullable();
            $table->float('t_nilaitransaksispt_skpdkbt')->nullable();
            $table->float('t_trf_aphb_kali_skpdkbt', 8, 2)->nullable();
            $table->float('t_trf_aphb_bagi_skpdkbt', 8, 2)->nullable();
            $table->float('t_permeter_tanah_skpdkbt')->nullable();

            $table->integer('t_idtarifbphtb_skpdkbt')->nullable();
            $table->foreign('t_idtarifbphtb_skpdkbt')->references('s_idtarifbphtb')->on('s_tarif_bphtb');
            $table->float('t_persenbphtb_skpdkbt', 8, 2)->nullable();
            $table->float('t_npop_bphtb_skpdkbt', 8, 2)->nullable();
            $table->float('t_npoptkp_bphtb_skpdkbt', 8, 2)->nullable();
            $table->float('t_npopkp_bphtb_skpdkbt', 8, 2)->nullable();
            $table->float('t_nilai_bayar_sblumnya', 8, 2)->nullable();
            $table->float('t_nilai_pokok_skpdkbt', 8, 2)->nullable();

            $table->float('t_jmlh_blndenda', 8, 2)->nullable();
            $table->float('t_jmlh_dendaskpdkbt', 8, 2)->nullable();
            $table->float('t_jmlh_totalskpdkbt', 8, 2)->nullable();

            $table->timestamp('t_tglbuat_kodebyar')->nullable();
            $table->integer('t_nourut_kodebayar')->nullable();
            $table->string('t_kodebayar_skpdkbt')->nullable();
            $table->timestamp('t_tgljatuhtempo_skpdkbt')->nullable();

            $table->integer('t_idjenisketetapan')->nullable();
            $table->foreign('t_idjenisketetapan')->references('s_idjenisketetapan')->on('s_jenisketetapan');

            $table->integer('t_idpersetujuan_skpdkb')->nullable();
            $table->integer('isdeleted')->nullable();
            $table->integer('t_idoperator_deleted')->nullable();
            $table->foreign('t_idoperator_deleted')->references('id')->on('users');

            $table->timestamps();
        });


        Schema::create('t_pemeriksaan', function (Blueprint $table) {
            $table->increments('t_idpemeriksa');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->timestamp('t_tglpenetapan')->nullable();
            $table->timestamp('t_tglby_system')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->integer('t_iduser_buat')->nullable();
            $table->foreign('t_iduser_buat')->references('id')->on('users');

            $table->integer('t_idpejabat1')->nullable();
            $table->foreign('t_idpejabat1')->references('s_idpejabat')->on('s_pejabat');
            $table->integer('t_idpejabat2')->nullable();
            $table->foreign('t_idpejabat2')->references('s_idpejabat')->on('s_pejabat');
            $table->integer('t_idpejabat3')->nullable();
            $table->foreign('t_idpejabat3')->references('s_idpejabat')->on('s_pejabat');
            $table->integer('t_idpejabat4')->nullable();
            $table->foreign('t_idpejabat4')->references('s_idpejabat')->on('s_pejabat');

            $table->string('t_noperiksa')->nullable();
            $table->text('t_keterangan')->nullable();

            $table->string('t_nop_sppt')->nullable();
            $table->string('t_tahun_sppt')->nullable();


            $table->float('t_luastanah_pemeriksa')->nullable();
            $table->float('t_njoptanah_pemeriksa')->nullable();
            $table->float('t_totalnjoptanah_pemeriksa')->nullable();

            $table->float('t_luasbangunan_pemeriksa')->nullable();
            $table->float('t_njopbangunan_pemeriksa')->nullable();
            $table->float('t_totalnjopbangunan_pemeriksa')->nullable();

            $table->float('t_grandtotalnjop_pemeriksa')->nullable();
            $table->float('t_nilaitransaksispt_pemeriksa')->nullable();
            $table->float('t_trf_aphb_kali_pemeriksa', 8, 2)->nullable();
            $table->float('t_trf_aphb_bagi_pemeriksa', 8, 2)->nullable();
            $table->float('t_permeter_tanah_pemeriksa')->nullable();

            $table->integer('t_idtarifbphtb_pemeriksa')->nullable();
            $table->foreign('t_idtarifbphtb_pemeriksa')->references('s_idtarifbphtb')->on('s_tarif_bphtb');
            $table->float('t_persenbphtb_pemeriksa', 8, 2)->nullable();
            $table->float('t_npop_bphtb_pemeriksa', 8, 2)->nullable();
            $table->float('t_npoptkp_bphtb_pemeriksa', 8, 2)->nullable();
            $table->float('t_npopkp_bphtb_pemeriksa', 8, 2)->nullable();

            $table->float('t_nilaibphtb_pemeriksa', 8, 2)->nullable();

            $table->integer('t_idpersetujuan_pemeriksa')->nullable();
            $table->integer('isdeleted')->nullable();
            $table->integer('t_idoperator_deleted')->nullable();
            $table->foreign('t_idoperator_deleted')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('s_jenis_bidangusaha');
        Schema::dropIfExists('s_status_npwpd');
        Schema::dropIfExists('t_spt');
        Schema::dropIfExists('t_nik_bersama');
        Schema::dropIfExists('t_filesyarat_bphtb');
        Schema::dropIfExists('t_skpdkb');
        Schema::dropIfExists('t_skpdkbt');
        Schema::dropIfExists('t_pemeriksaan');
    }

}
