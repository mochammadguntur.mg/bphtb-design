<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHispostpbbbpnTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('his_postdatabpn', function (Blueprint $table) {
            $table->increments('t_idhis_posbpn');
            $table->string('t_username');
			$table->string('t_password');
			$table->string('t_aktaid');
			$table->timestamp('t_tgl_akta');
            $table->string('t_nop');
            $table->string('t_nik');
			$table->string('t_nib');
			$table->string('t_npwp');
			$table->string('t_nama_wp');
			$table->text('t_alamat_op');
			$table->string('t_kecamatan_op');
			$table->string('t_kelurahan_op');
			$table->string('t_kota_kab_op');
			$table->float('t_luastanah_op', 8, 2)->nullable();
			$table->float('t_luasbangunan_op', 8, 2)->nullable();
			
			$table->string('t_no_sertipikat');
			$table->string('t_no_akta');
			$table->string('t_ppat');
			$table->string('t_status');
			
			$table->integer('t_iduser')->nullable();
			$table->timestamp('t_tglby_system');
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('his_postdatabpn');
    }
}
