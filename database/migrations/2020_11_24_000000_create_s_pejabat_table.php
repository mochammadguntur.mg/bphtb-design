<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSPejabatTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_pejabat', function (Blueprint $table) {
            $table->increments('s_idpejabat');
            $table->text('s_namapejabat');
            $table->text('s_jabatanpejabat')->nullable();
            $table->string('s_nippejabat')->nullable();
            $table->text('s_golonganpejabat')->nullable();
			$table->text('s_alamat')->nullable();
            $table->text('s_filettd')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_pejabat');
    }
}
