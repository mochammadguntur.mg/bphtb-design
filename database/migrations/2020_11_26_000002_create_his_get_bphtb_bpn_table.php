<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateHisgetbphtbbpnTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('his_getbphtb_bpn', function (Blueprint $table) {
            $table->increments('t_idhis_getbpn');
            $table->string('t_username')->nullable();
			$table->string('t_password')->nullable();
            $table->string('t_nop')->nullable();
            $table->string('t_ntpd')->nullable();
			$table->string('t_nik')->nullable();
			$table->string('t_nama')->nullable();
			$table->text('t_alamat')->nullable();
			$table->string('t_kelurahan_op')->nullable();
			$table->string('t_kecamatan_op')->nullable();
			$table->string('t_kota_kab_op')->nullable();
			$table->float('t_luastanah', 8, 2)->nullable();
			$table->float('t_luasbangunan', 8, 2)->nullable();
			$table->string('t_pembayaran')->nullable();
			$table->string('t_status')->nullable();
			$table->timestamp('t_tanggal_pembayaran')->nullable();
			$table->string('t_jenisbayar')->nullable();
			$table->string('respon_code')->nullable();
			$table->integer('t_iduser')->nullable();
			$table->timestamp('t_tglby_system')->default(DB::raw('CURRENT_TIMESTAMP'));
			
			
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('his_getbphtb_bpn');
    }
}
