<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSKelurahanTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_kelurahan', function (Blueprint $table) {
            $table->increments('s_idkelurahan');
            $table->integer('s_idkecamatan');
			$table->foreign('s_idkecamatan')->references('s_idkecamatan')->on('s_kecamatan');
            $table->string('s_kd_propinsi', 2)->nullable();
            $table->string('s_kd_dati2', 2)->nullable();
            $table->string('s_kd_kecamatan', 3)->nullable();
			$table->string('s_kd_kelurahan', 3)->nullable();
			$table->text('s_namakelurahan');
			$table->string('s_latitude')->nullable();
            $table->string('s_longitude')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_kelurahan');
    }
}
