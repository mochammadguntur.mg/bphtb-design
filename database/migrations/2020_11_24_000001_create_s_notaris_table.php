<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSNotarisTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('s_notaris', function (Blueprint $table) {
            $table->increments('s_idnotaris');
            $table->string('s_namanotaris')->nullable();
            $table->text('s_alamatnotaris')->nullable();
			$table->string('s_kodenotaris')->nullable();
			$table->string('s_sknotaris')->nullable();
			$table->string('s_noid_bpn')->nullable();
			$table->timestamp('s_tgl1notaris')->nullable();
			$table->timestamp('s_tgl2notaris')->nullable();
			$table->integer('s_statusnotaris')->nullable();
			$table->string('s_npwpd_notaris')->nullable();
			$table->text('s_daerahkerja')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_notaris');
    }
}
