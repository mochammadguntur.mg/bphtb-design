<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSPersyaratan extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_persyaratan', function (Blueprint $table) {
            $table->increments('s_idpersyaratan');
            $table->integer('s_idjenistransaksi');
            $table->string('s_namapersyaratan')->nullable();
			$table->integer('s_idwajib_up')->nullable();
			$table->foreign('s_idwajib_up')->references('s_idwajib_up')->on('s_wajib_up');
            $table->integer('s_lokasimenu')->nullable();
            $table->integer('order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_persyaratan');
    }
}
