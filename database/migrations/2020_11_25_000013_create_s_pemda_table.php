<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSPemdaTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('s_pemda', function (Blueprint $table) {
            $table->increments('s_idpemda');
            $table->string('s_namaprov')->nullable();
            $table->string('s_namakabkot')->nullable();
			$table->string('s_namaibukotakabkot')->nullable();
			$table->string('s_kodeprovinsi', 3)->nullable();
			$table->string('s_kodekabkot', 4)->nullable();
			$table->string('s_namainstansi')->nullable();
			$table->string('s_namasingkatinstansi')->nullable();
			$table->text('s_alamatinstansi')->nullable();
			$table->string('s_notelinstansi', 15)->nullable();
			$table->string('s_namabank')->nullable();
			$table->string('s_norekbank')->nullable();
			$table->text('s_logo')->nullable();
			$table->string('s_namacabang')->nullable();
			$table->string('s_kodecabang')->nullable();
			$table->string('s_kodepos', 5)->nullable();			
			$table->text('s_email')->nullable();		
			$table->string('s_urlbphtb')->nullable();	
			$table->string('s_latitude_pemda')->nullable();
			$table->string('s_longitude_pemda')->nullable();	
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_pemda');
    }
}
