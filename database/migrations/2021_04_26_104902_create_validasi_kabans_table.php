<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateValidasiKabansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /* MENU VALIDASI KABAN */

        Schema::create('s_status_kaban', function (Blueprint $table) {
            $table->increments('s_id_status_kaban');
            $table->string('s_nama_status_kaban')->nullable();
            $table->timestamps();
        });

        Schema::create('t_validasi_kaban', function (Blueprint $table) {
            $table->increments('t_id_validasi_kaban');
            $table->integer('t_idspt')->nullable();
            $table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('s_id_status_kaban')->nullable();
            $table->foreign('s_id_status_kaban')->references('s_id_status_kaban')->on('s_status_kaban');
            $table->timestamp('t_tglvalidasikaban')->nullable();
            $table->integer('t_iduser_validasikaban')->nullable();
            $table->foreign('t_iduser_validasikaban')->references('id')->on('users');
            $table->text('t_keterangan_kaban')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_setmenukaban', function (Blueprint $table) {
            $table->increments('s_id_setmenukaban');
			$table->integer('s_id_statusmenukaban')->nullable();
            $table->string('s_ketmenukaban')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_setmenutgkpbb', function (Blueprint $table) {
            $table->increments('s_id_setmenutgkpbb');
			$table->integer('s_id_statustgkpbb')->nullable();
            $table->string('s_ketmenutgkpbb')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_tahuntunggakanpbb', function (Blueprint $table) {
            $table->increments('s_id_thntgkpbb');
			$table->integer('s_thntgkpbb')->nullable();
			$table->integer('s_rentangthntgkpbb')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_setmenubriva', function (Blueprint $table) {
            $table->increments('s_id_setmenubriva');
			$table->integer('s_id_statusmenubriva')->nullable();
            $table->string('s_ketmenubriva')->nullable();
            $table->timestamps();
        });
		
		
		Schema::create('s_setkonekpbb', function (Blueprint $table) {
            $table->increments('s_id_setkonekpbb');
			$table->integer('s_id_statuskonekpbb')->nullable();
            $table->string('s_ketkonekpbb')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_menuupload', function (Blueprint $table) {
            $table->increments('s_id_menuupload');
			$table->integer('s_ukuranfile')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_setmenugmap', function (Blueprint $table) {
            $table->increments('s_id_setmenugmap');
			$table->integer('s_id_statusmenugmap')->nullable();
            $table->string('s_ketmenugmap')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_setmenucaptcha', function (Blueprint $table) {
            $table->increments('s_id_setmenucaptcha');
			$table->integer('s_id_statusmenucaptcha')->nullable();
            $table->string('s_ketmenucaptcha')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_setmenupelayanan', function (Blueprint $table) {
            $table->increments('s_id_setmenupelayanan');
			$table->string('s_namamenupelayanan')->nullable();
			$table->integer('s_id_statusmenupelayanan')->nullable();
            $table->string('s_ketmenupelayanan')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_status_ntpd', function (Blueprint $table) {
            $table->increments('s_id_statusmenuntpd');
            $table->string('s_nama_status_ntpd')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_setmenuntpd', function (Blueprint $table) {
            $table->increments('s_id_setmenuntpd');
			$table->integer('s_id_statusmenuntpd')->nullable();
			$table->foreign('s_id_statusmenuntpd')->references('s_id_statusmenuntpd')->on('s_status_ntpd');
            $table->string('s_ketmenuntpd')->nullable();
            $table->timestamps();
        });
		
		
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_status_kaban');
        Schema::dropIfExists('t_validasi_kaban');
    }
}
