<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogBrivasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('log_briva', function (Blueprint $table) {
            $table->increments('log_id');
            $table->string('briva_no', 5)->nullable();
            $table->string('cust_code', 10)->nullable();
            $table->string('nama')->nullable();
            $table->float('amount', 8, 2)->nullable();
            $table->string('keterangan')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('log_briva');
    }
}
