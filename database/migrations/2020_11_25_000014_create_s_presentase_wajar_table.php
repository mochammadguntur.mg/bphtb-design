<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSPresentaseWajarTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('s_presentase_wajar', function (Blueprint $table) {
            $table->increments('s_idpresentase_wajar');
            $table->float('s_nilaimin', 8, 2);
            $table->float('s_nilaimax', 8, 2);
			$table->string('s_ketpresentase_wajar')->nullable();
			$table->string('s_css_warna')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_presentase_wajar');
    }
}
