<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePelayananKeringanansTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
		
		Schema::create('s_status_pelayanan', function (Blueprint $table) {
            $table->increments('s_id_status_layanan');
            $table->string('s_nama_status_layanan')->nullable();
            $table->timestamps();
        });
		
        Schema::create('t_pelayanan_keringanan', function (Blueprint $table) {
            $table->increments('t_idkeringanan');
            $table->integer('t_idspt')->nullable();
			$table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_nokeringanan')->nullable();
            $table->timestamp('t_tglpengajuan')->nullable();
			
			$table->integer('t_iduser_pengajuan')->nullable();
			$table->foreign('t_iduser_pengajuan')->references('id')->on('users');
			$table->text('t_keterangan_permohoanan')->nullable();
			
			$table->string('t_nosk_keringanan')->nullable();
			
            $table->float('t_jmlhpotongan', 8, 2)->nullable();
            
			$table->integer('t_idstatus_disetujui')->nullable();
			$table->foreign('t_idstatus_disetujui')->references('s_id_status_layanan')->on('s_status_pelayanan');
            $table->integer('t_idoperator')->nullable();
			$table->foreign('t_idoperator')->references('id')->on('users');
			
            $table->timestamp('t_tglpersetujuan')->nullable();
			$table->text('t_keterangan_disetujui')->nullable();
            $table->float('t_jmlhpotongan_disetujui', 8, 2)->nullable();
			$table->float('t_persentase_disetujui', 8, 2)->nullable();
           
		    $table->float('t_jmlh_spt_sebenarnya', 8, 2)->nullable();
			$table->float('t_jmlh_spt_hasilpot', 8, 2)->nullable();
            
            
            $table->timestamps();
        });
		
		
		Schema::create('t_file_keringanan', function (Blueprint $table) {
            $table->increments('t_idfile_keringanan');
            $table->integer('t_idspt')->nullable();
			$table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
			
			$table->integer('t_iduser_upload')->nullable();
			$table->foreign('t_iduser_upload')->references('id')->on('users');
			$table->text('letak_file');
			$table->text('nama_file');
            $table->timestamp('t_tgl_upload')->nullable();
			$table->text('t_keterangan_file');
            $table->timestamps();
        });
		
		
		Schema::create('t_pelayanan_keberatan', function (Blueprint $table) {
            $table->increments('t_idkeberatan');
            $table->integer('t_idspt')->nullable();
			$table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_nokeberatan')->nullable();
            $table->timestamp('t_tglpengajuan')->nullable();

			$table->integer('t_iduser_pengajuan')->nullable();
			$table->foreign('t_iduser_pengajuan')->references('id')->on('users');
			$table->text('t_keterangan_permohoanan')->nullable();

			$table->string('t_nosk_keberatan')->nullable();

			$table->integer('t_idstatus_disetujui')->nullable();
			$table->foreign('t_idstatus_disetujui')->references('s_id_status_layanan')->on('s_status_pelayanan');
            $table->integer('t_idoperator')->nullable();
			$table->foreign('t_idoperator')->references('id')->on('users');

            $table->timestamp('t_tglpersetujuan')->nullable();
            $table->text('t_keterangan_disetujui')->nullable();
			$table->float('t_jmlhpotongan_disetujui', 8, 2)->nullable();
			$table->float('t_persentase_disetujui', 8, 2)->nullable();
           
		    $table->float('t_jmlh_spt_sebenarnya', 8, 2)->nullable();
			$table->float('t_jmlh_spt_hasilpot', 8, 2)->nullable();
            $table->timestamps();
        });
		
		
		Schema::create('t_bpn', function (Blueprint $table) {
            $table->increments('t_idbpn');
            $table->string('t_noakta', 100)->nullable();
            $table->timestamp('t_tglakta')->nullable();
            $table->string('t_namappat')->nullable();
            $table->string('t_nop')->nullable();
            $table->string('t_nib')->nullable();
            $table->string('t_ntpd')->nullable();
            $table->string('t_nik')->nullable();
            $table->string('t_npwp', 50)->nullable();
            $table->string('t_namawp')->nullable();
            $table->string('t_kelurahanop')->nullable();
            $table->string('t_kecamatanop')->nullable();
            $table->string('t_kotaop')->nullable();
            $table->float('t_luastanahop', 8, 2)->nullable();
            $table->string('t_jenishak')->nullable();
            $table->string('t_koordinat_x')->nullable();
            $table->string('t_koordinat_y')->nullable();
            $table->timestamp('t_tgltransaksi')->nullable();
            $table->timestamps();
        });
		
		Schema::create('s_users_api', function (Blueprint $table) {
            $table->increments('s_idusers_api');
            $table->string('s_username')->nullable();
            $table->string('s_password')->nullable();
            $table->timestamps();
        });
		
		Schema::create('t_pelayanan_penundaan', function (Blueprint $table) {
            $table->increments('t_idpenundaan');
            $table->integer('t_idspt')->nullable();
			$table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_nopenundaan')->nullable();
            $table->timestamp('t_tglpengajuan')->nullable();

			$table->integer('t_iduser_pengajuan')->nullable();
			$table->foreign('t_iduser_pengajuan')->references('id')->on('users');
			$table->text('t_keterangan_permohoanan')->nullable();

			$table->string('t_nosk_penundaan')->nullable();

			$table->integer('t_idstatus_disetujui')->nullable();
			$table->foreign('t_idstatus_disetujui')->references('s_id_status_layanan')->on('s_status_pelayanan');
            $table->integer('t_idoperator')->nullable();
			$table->foreign('t_idoperator')->references('id')->on('users');

            $table->timestamp('t_tglpersetujuan')->nullable();
            $table->text('t_keterangan_disetujui')->nullable();
            $table->timestamps();
        });
		
		Schema::create('t_pelayanan_angsuran', function (Blueprint $table) {
            $table->increments('t_idangsuran');
            $table->integer('t_idspt')->nullable();
			$table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_noangsuran')->nullable();
            $table->timestamp('t_tglpengajuan')->nullable();

			$table->integer('t_iduser_pengajuan')->nullable();
			$table->foreign('t_iduser_pengajuan')->references('id')->on('users');
			$table->text('t_keterangan_permohoanan')->nullable();

			$table->string('t_nosk_angsuran')->nullable();

            $table->float('t_jmlhangsuran', 8, 2)->nullable();

			$table->integer('t_idstatus_disetujui')->nullable();
			$table->foreign('t_idstatus_disetujui')->references('s_id_status_layanan')->on('s_status_pelayanan');
            $table->integer('t_idoperator')->nullable();
			$table->foreign('t_idoperator')->references('id')->on('users');

            $table->timestamp('t_tglpersetujuan')->nullable();
            $table->text('t_keterangan_disetujui')->nullable();
            $table->timestamps();
        });
		
		
		Schema::create('t_pelayanan_mutasi', function (Blueprint $table) {
            $table->increments('t_idmutasi');
            $table->integer('t_idspt')->nullable();
			$table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_nomutasi')->nullable();
            $table->timestamp('t_tglpengajuan')->nullable();

			$table->integer('t_iduser_pengajuan')->nullable();
			$table->foreign('t_iduser_pengajuan')->references('id')->on('users');
			$table->text('t_keterangan_permohoanan')->nullable();

			$table->string('t_nosk_mutasi')->nullable();

			$table->integer('t_idstatus_disetujui')->nullable();
			$table->foreign('t_idstatus_disetujui')->references('s_id_status_layanan')->on('s_status_pelayanan');
            $table->integer('t_idoperator')->nullable();
			$table->foreign('t_idoperator')->references('id')->on('users');

            $table->timestamp('t_tglpersetujuan')->nullable();
            $table->text('t_keterangan_disetujui')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_pelayanan_keringanan');
		Schema::dropIfExists('t_file_keringanan');
		Schema::dropIfExists('t_pelayanan_keberatan');
		Schema::dropIfExists('t_bpn');
		Schema::dropIfExists('s_users_api');
		Schema::dropIfExists('t_pelayanan_penundaan');
		Schema::dropIfExists('t_pelayanan_angsuran');
		Schema::dropIfExists('t_pelayanan_mutasi');
    }
}
