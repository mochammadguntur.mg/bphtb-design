<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSTarifbphtbTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_tarif_bphtb', function (Blueprint $table) {
			$table->increments('s_idtarifbphtb');
            $table->float('s_tarif_bphtb', 8, 2)->nullable();
			$table->date('s_tgl_awal')->nullable();
			$table->date('s_tgl_akhir')->nullable();
			$table->text('s_dasar_hukum')->nullable();		
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_tarif_bphtb');
    }
}
