<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
		
		Schema::create('s_hak_akses', function (Blueprint $table) {
            $table->increments('s_id_hakakses');
			$table->string('s_nama_hakakses')->nullable();
        });
		
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
			$table->string('username')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
			$table->integer('s_tipe_pejabat')->nullable();
            $table->integer('s_idpejabat')->nullable();
			$table->foreign('s_idpejabat')->references('s_idpejabat')->on('s_pejabat');
			$table->integer('s_idnotaris')->nullable();
			$table->foreign('s_idnotaris')->references('s_idnotaris')->on('s_notaris');
			$table->integer('s_id_hakakses')->nullable();
			$table->foreign('s_id_hakakses')->references('s_id_hakakses')->on('s_hak_akses');
			$table->text('s_gambar')->nullable();	
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('s_hak_akses');	
        Schema::dropIfExists('users');
    }
}
