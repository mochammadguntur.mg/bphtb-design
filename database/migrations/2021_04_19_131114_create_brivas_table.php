<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBrivasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_briva', function (Blueprint $table) {
            $table->increments('s_idbriva');
            $table->string('client_id')->nullable();
            $table->string('secret_id')->nullable();
            $table->string('institution_code')->nullable();
            $table->string('briva_no', 5)->nullable();
            $table->string('url_development')->nullable();
            $table->string('url_production')->nullable();
            $table->integer('url_aktif')->nullable();
            $table->integer('status')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_briva');
    }
}
