<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSTempoTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('s_tempo', function (Blueprint $table) {
            $table->increments('s_idtempo');
			$table->integer('s_haritempo');	
			$table->timestamp('s_tglberlaku_awal')->nullable();
			$table->timestamp('s_tglberlaku_akhir')->nullable();			
			$table->integer('s_id_status');	
			$table->foreign('s_id_status')->references('s_id_status')->on('s_status');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_tempo');
    }
}
