<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSJenistransaksiTable extends Migration
{
    /**
     * Run the migrations. 
     * 
     * @return void
     */
    public function up()
    {
		
		Schema::create('s_status_perhitungan', function (Blueprint $table) {
            $table->increments('s_idstatus_pht');
            $table->string('s_nama')->nullable();
			$table->timestamps();
        });
		
		Schema::create('s_status_dptnpoptkp', function (Blueprint $table) {
            $table->increments('s_id_dptnpoptkp');
            $table->string('s_nama_dptnpoptkp')->nullable();
			$table->timestamps();
        });
		
        Schema::create('s_jenistransaksi', function (Blueprint $table) {
            $table->increments('s_idjenistransaksi');
            $table->string('s_kodejenistransaksi', 5)->nullable();
            $table->string('s_namajenistransaksi')->nullable();
			$table->integer('s_idstatus_pht')->nullable();
			$table->foreign('s_idstatus_pht')->references('s_idstatus_pht')->on('s_status_perhitungan');
			$table->integer('s_id_dptnpoptkp')->nullable();
			$table->foreign('s_id_dptnpoptkp')->references('s_id_dptnpoptkp')->on('s_status_dptnpoptkp');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
		Schema::dropIfExists('s_status_perhitungan');
		Schema::dropIfExists('s_status_dptnpoptkp');
        Schema::dropIfExists('s_jenistransaksi');
    }
}
