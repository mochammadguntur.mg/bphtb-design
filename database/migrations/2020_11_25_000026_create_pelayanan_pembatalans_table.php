<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePelayananPembatalansTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void
     */
    public function up()
    {
        Schema::create('t_pelayanan_pembatalan', function (Blueprint $table) {
            $table->increments('t_idpembatalan');
            $table->integer('t_idspt')->nullable();
			$table->foreign('t_idspt')->references('t_idspt')->on('t_spt');
            $table->integer('t_nopembatalan')->nullable();
            $table->timestamp('t_tglpengajuan')->nullable();
            $table->text('t_keterangan_pembatalan')->nullable();
			
			$table->integer('t_iduser_pengajuan')->nullable();
			$table->foreign('t_iduser_pengajuan')->references('id')->on('users');
			$table->string('t_nosk_pembatalan');
            
			$table->integer('t_idstatus_disetujui')->nullable();
			$table->foreign('t_idstatus_disetujui')->references('s_id_status_layanan')->on('s_status_pelayanan');
			$table->integer('t_idoperator')->nullable();
			$table->foreign('t_idoperator')->references('id')->on('users');
			
			
            $table->timestamp('t_tglpersetujuan')->nullable();
            $table->text('t_keterangan_disetujui')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('t_pelayanan_pembatalan');
    }
}
