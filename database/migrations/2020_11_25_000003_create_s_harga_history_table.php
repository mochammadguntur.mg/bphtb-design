<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSHargaHistoryTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('s_harga_history', function (Blueprint $table) {
            $table->increments('s_idhistory');
            $table->string('s_kd_propinsi', 2)->nullable();
            $table->string('s_kd_dati2', 2)->nullable();
            $table->string('s_kd_kecamatan', 3)->nullable();
            $table->string('s_kd_kelurahan', 3)->nullable();
            $table->string('s_kd_blok', 3)->nullable();
			$table->string('s_kd_urut', 4)->nullable();
			$table->integer('s_kd_jenis')->nullable();
			$table->integer('s_tahun_sppt')->nullable();
			$table->float('s_permetertanah', 8, 2)->nullable();
			$table->integer('id_jenistanah')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_harga_history');
    }
}
