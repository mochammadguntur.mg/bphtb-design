<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSJenisketetapanTable extends Migration
{
    /**
     * Run the migrations. 
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('s_jenisketetapan', function (Blueprint $table) {
            $table->increments('s_idjenisketetapan');
            $table->string('s_namajenisketetapan');
            $table->string('s_namasingkatjenisketetapan');
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_jenisketetapan');
    }
}
