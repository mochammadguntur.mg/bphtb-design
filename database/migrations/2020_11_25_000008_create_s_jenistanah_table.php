<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSJenistanahTable extends Migration
{
    /**
     * Run the migrations. 
     *
     * @return void 
     */
    public function up()
    {
        Schema::create('s_jenistanah', function (Blueprint $table) {
            $table->increments('id_jenistanah');
            $table->string('nama_jenistanah')->nullable();
			$table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_jenistanah');
    }
}
