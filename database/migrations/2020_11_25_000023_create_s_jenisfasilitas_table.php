<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSJenisfasilitasTable extends Migration
{
    /**
     * Run the migrations. 
     * 
     * @return void
     */
    public function up()
    {
        Schema::create('s_jenisfasilitas', function (Blueprint $table) {
            $table->increments('s_idjenisfasilitas');
            $table->string('s_kodejenisfasilitas', 5)->nullable();
            $table->string('s_namajenisfasilitas')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('s_jenisfasilitas');
    }
}
