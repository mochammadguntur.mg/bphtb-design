@extends('layouts.master')

@section('judulkiri')
<h1 class="m-0">Validasi</h1>
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-9 col-lg-9 col-sm-12">
                <div class="d-flex flex-wrap align-items-top justify-content-start mt-1">
                    @can('MenuValidasiBerkas')
                    <div class="card-product mb-4 mr-4">
                        <a href="{{ url('validasi-berkas') }}" class="text-dark-75">
                            <div class="product-image">
                                <img src="{{ asset('dist/img/others/U.svg') }}">
                            </div>
                            <div class="info">
                                <div class="name">
                                    <p>Berkas</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endcan

                    @can('MenuValidasiKabid')
                    <div class="card-product mb-4 mr-4">
                        <a href="{{ url('validasi-kabid') }}" class="text-dark-75">
                            <div class="product-image">
                                <img src="{{ asset('dist/img/others/U.svg') }}">
                            </div>
                            <div class="info">
                                <div class="name">
                                    <p>Kabid</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endcan

                    @can('MenuValidasiKaban')
                    <div class="card-product mb-4 mr-4">
                        <a href="{{ url('validasi-kaban') }}" class="text-dark-75">
                            <div class="product-image">
                                <img src="{{ asset('dist/img/others/U.svg') }}">
                            </div>
                            <div class="info">
                                <div class="name">
                                    <p>Kaban</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endcan
                </div>
            </div>
            <div class="col-12 col-md-3 col-lg-3 col-sm-12 d-flex flex-column mb-4">
                <div class="flex-grow-1 bg-primary p-4 rounded-lg"
                    style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url(dist/img/custom-6.svg); background-repeat: no-repeat;">

                    <h4 class="text-inverse-danger mt-2 font-weight-bolder">Validasi</h4>

                    <p class="text-inverse-danger my-6">
                        Pemilihan validasi <br />bapenda.
                    </p>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

@endsection
@push('scripts')

<!-- /.content -->
@endpush