@extends('layouts.master')

@section('judulkiri')
PERMOHONAN MUTASI SPOP [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <form action="/pelayanan-mutasi/{{ $pelayananmutasi->t_idmutasi }}/edit" method="POST" class="form-horizontal">
                    <div class="card-body">
                        @method('patch')
                        @csrf
                        @include('pelayanan-mutasi.partials.form')
                    </div>
                </form>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-datasspdbphtb">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title"><span class="fas fa-list"></span> DATA TRANSAKSI SSPD-BPHTB</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                            <thead>
                                <tr>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">Tgl Pendaftaran</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">No Kohir</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">Nama WP</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">Jmlh Pajak</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">Jenis Transaksi</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">NOP</th>
                                    <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-tgldaftar" readonly>
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-kohirspt">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-namawp">
                                    </th>
                                    <th></th>
                                    <th>
                                        <select class="form-control form-control-sm" id="filter-jenistransaksi">
                                        </select>
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-nop">
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center" colspan="8"> Tidak ada data.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix pagination-footer">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                            class="fas fa-times-circle"></span> Tutup</button>
                </div>
            </div>
        </div>
    </div>
@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        $('#filter-tgldaftar').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tgldaftar').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tgldaftar').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        comboJenisTransaksi();
    });

    var datatables = datagrid({
    url: '{{ url('pelayanan-mutasi/datagrid_sspdbphtb') }}',
    table: "#datagrid-table",
    columns: [
        {class: "text-center"},
        {class: "text-center"},
        {class: ""},
        {class: "text-right"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: ""},
    ],
    orders: [
        {sortable: false},
        {sortable: true,name: "t_tgldaftar_spt"},
        {sortable: true,name: "t_kohirspt"},
        {sortable: true,name: "t_nama_pembeli"},
        {sortable: true,name: "t_nilai_bphtb_fix"},
        {sortable: true,name: "t_idjenistransaksi"},
        {sortable: false},
        {sortable: false},
    ],
    action: [{
            name: 'pilih',
            btnClass: 'btn btn-warning btn-sm',
            btnText: '<i class="fas fa-edit"> Pilih</i>'
        }
    ]

    });

    $("#filter-kohirspt, #filter-namawp, #filter-nop").keyup(function() {
    search();
    });

    $("#filter-jenistransaksi, #filter-tgldaftar").change(function() {
    search();
    });

    function search() {
        datatables.setFilters({
            jenistransaksi: $("#filter-jenistransaksi").val(),
            kohirspt: $("#filter-kohirspt").val(),
            namawp: $("#filter-namawp").val(),
            nop: $("#filter-nop").val(),
            tgldaftar: $("#filter-tgldaftar").val()
        });
        datatables.reload();
    }

    $("#btnCariSpt").click(function() {
        $('#modal-datasspdbphtb').modal('show');
        search();
    });

    function pilihTransaksi(a){
        $.ajax({
            url: '{{ url('pelayanan-mutasi/caridata/{ + a + }') }}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $('#t_idspt').val(data[0].t_idspt);
            $('#t_kohirspt').val(data[0].t_kohirspt);
            $('#t_nama_pembeli').val(data[0].t_nama_pembeli);
            $('#t_nop_sppt').val(data[0].t_nop_sppt);
            $('#modal-datasspdbphtb').modal('hide');
        });
    }

</script>
@endpush
