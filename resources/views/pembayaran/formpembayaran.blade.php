@extends('layouts.master')

@section('judulkiri')
PEMBAYARAN
@endsection

@section('content')
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">FORM PEMBAYARAN</h3>
    </div>

    <!-- /.card-header -->
    @php
    // dd($skpdkb);
    @endphp
    <!-- form start -->
    @if ($dataspt->s_id_status_kabid == 1)

    <form method="post" action="{{ url('pembayaran/simpanpembayaran') }}" id="formtambah" name="formtambah">
        <div class="col-md-12">
            @include('lihatdata.informasitransaksi')
            @include('lihatdata.informasipembeli')
        </div>
        @include('lihatdata.perhitunganbphtb')
        <br>
        <div class="col-md-12">
            <h5>Hasil Validasi</h5>
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <div class="row">
                        <label class="col-sm-4 ">Status Validasi Berkas<span style="color: red">*</span></label>
                        <div class="col-md-8">
                            <input type="hidden" name="t_idspt" id="t_idspt" value="{{ $dataspt->t_idspt }}">
                            <input type="hidden" name="t_id_validasi_berkas" id="t_id_validasi_berkas"
                                   value="{{ $dataspt->t_id_validasi_berkas }}">
                            <input type="hidden" name="t_id_validasi_kabid" id="t_id_validasi_kabid"
                                   value="{{ $dataspt->t_id_validasi_kabid }}">
                            <select name="s_id_status_berkas" id="s_id_status_berkas"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1"
                                    required="required" disabled="true">
                                <option value="">Silahkan Pilih</option>

                                @foreach ($data_statusberkas as $a)
                                <option value="{{ $a->s_id_status_berkas }}"
                                        {{ $dataspt->s_id_status_berkas == $a->s_id_status_berkas ? 'selected' : '' }}>
                                    {{ $a->s_nama_status_berkas }}</option>
                                @endforeach

                            </select>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <label class="col-sm-4 ">Keterangan</label>
                        <div class="col-md-8">
                            <textarea name="t_keterangan_berkas" id="t_keterangan_berkas" class="form-control"
                                      disabled="true">{{ $dataspt->t_keterangan_berkas }}</textarea>
                        </div>
                    </div>
                    <div class="row mt-2">
                        <label class="col-sm-4 ">Status Validasi Kabid<span style="color: red">*</span></label>
                        <div class="col-md-8">
                            <select name="s_id_status_kabid" id="s_id_status_kabid"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1"
                                    required="required" disabled="true">
                                <option value="">Silahkan Pilih</option>

                                @foreach ($data_statuskabid as $a)
                                <option value="{{ $a->s_id_status_kabid }}"
                                        {{ $dataspt->s_id_status_kabid == $a->s_id_status_kabid ? 'selected' : '' }}>
                                    {{ $a->s_nama_status_kabid }}</option>
                                @endforeach


                            </select>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <label class="col-sm-4 ">Keterangan</label>
                        <div class="col-md-8">
                            <textarea name="t_keterangan_kabid" id="t_keterangan_kabid" class="form-control"
                                      disabled="true">{{ $dataspt->t_keterangan_kabid }}</textarea>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="col-md-12">
            <h5>Form Pembayaran</h5>
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <div class="col-sm-12 row">
                        <label class="col-sm-3 ">Tanggal Pembayaran<span style="color: red">*</span></label>
                        <div class="col-md-3">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-calendar"></i></span>
                                </div>
                                <input name="t_tglpembayaran_pokok" id="t_tglpembayaran_pokok" class="form-control"
                                       required="required" value="{{ date('d-m-Y') }}" type="text">
                            </div>
                        </div>
                        <label class="col-sm-1 ">Jam<span style="color: red">*</span></label>
                        <div class="col-md-2">
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-calendar"></i></span>
                                </div>
                                <input name="t_waktujam" id="t_waktujam" class="form-control" required="required"
                                       value="{{ date('H:i') }}" type="text">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row">
                        <input type="hidden" name="t_idskpdkb" value="{{ isset($skpdkb['t_idskpdkb']) ? $skpdkb['t_idskpdkb'] : null }}">
                        <label class="col-sm-3 ">Jumlah Pembayaran<span style="color: red">*</span></label>
                        <div class="col-md-3">
                            <input name="t_jmlhbayar" id="t_jmlhbayar" class="form-control" required="required"
                                   value="{{ isset($skpdkb['t_jmlh_totalskpdkb']) ? number_format($skpdkb['t_jmlh_totalskpdkb'], 0, ',', '.') : number_format($dataspt->t_nilai_bphtb_fix, 0, ',', '.') }}" type="text"
                                   style="text-align: right; color: blue; font-size: 20px;" readonly="true">
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer ">
            <a href="{{ url('pembayaran') }}" class="btn btn-outline-primary prevBtn pull-left" type="button"><i class="fa fa-chevron-left"></i> KEMBALI </a>
            <!-- <a href="{{ url('validasi-kabid') }}/{{ $dataspt->t_idspt }}/tahapkedua" class="btn btn-primary prevBtn pull-right" type="button"><i class="fa fa-save"></i> SIMPAN </a> -->
            <button id="pendaftaranbutton" type="submit" class="btn btn-primary pull-right"><i class="fa fa-dollar-sign"></i> BAYAR</button>
        </div>
    </form>
    @else
    <div class="col-md-12">
        <center><b>DATA BELUM DI SETUJUI KABID</b></center>
    </div>
    <div class="card-footer ">
        <a href="{{ url('validasi-kabid') }}/pilihspt" class="btn btn-primary prevBtn pull-left" type="button"><i class="fa fa-chevron-left"></i> KEMBALI </a>
    </div>
    @endif
</div>
<div class="modal fade" id="loadingsimpanvalidasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
     style="background:transparent;">
    <!--data-backdrop="static" data-keyboard="false" -->
    <center><img style="background:transparent;" src='{{ asset('upload/loading77.gif') }}'></center>
</div>
@endsection
@push('scriptsbawah')
<script type="text/javascript">
    $('#t_tglpembayaran_pokok').mask("00-00-0000");
    $('#t_waktujam').mask("00:00");

    $("#t_tglpembayaran_pokok").datepicker({
        format: 'dd-mm-yyyy',
        language: 'id'
    });

    $('#pendaftaranbutton').click(function () {
        var myForm = document.getElementById('formtambah');
        myForm.onsubmit = function () {
            var allInputs = myForm.getElementsByTagName('input');
            var input, i;

            for (i = 0; input = allInputs[i]; i++) {
                if (input.getAttribute('name') && !input.value) {
                    //input.setAttribute('name', '');

                } else {
                    jQuery('#loadingsimpanvalidasi').modal('show');
                }
            }
        };
    });

</script>
@endpush
