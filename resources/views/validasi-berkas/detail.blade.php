@extends('layouts.master')

@section('judulkiri')
Validasi Berkas
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12">
                <h5>Informasi Transaksi</h5>
                <div class="card card-outline card-primary">
                    <div class="card-body">
                        <div class="row p-0">
                            <div class="col-md-6">
                                <div class="text-left">Jenis Transaksi: Jual Beli</div>
                            </div>
                            <div class="col-md-6">
                                <div class="text-left">Notaris: Arif Ramadhan Mustofa S.ss</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h5>Pembeli</h5>
                <div class="card card-outline card-primary">
                    <div class="card-body">
                        <strong>Ahmad</strong>
                        <div>Madalinskiego 8</div>
                        <div>71-101 Szczecin, Poland</div>
                        <div>Email: info@webz.com.pl</div>
                        <div>Phone: +48 444 666 3333</div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <h5>Penjual</h5>
                <div class="card card-outline card-primary">
                    <div class="card-body">
                        <strong>Ahmad</strong>
                        <div>Madalinskiego 8</div>
                        <div>71-101 Szczecin, Poland</div>
                        <div>Email: info@webz.com.pl</div>
                        <div>Phone: +48 444 666 3333</div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h5>Informasi Tanah</h5>
                <div class="card card-outline card-primary">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="col-sm-12 p-0">
                                    NOP : 63.10.020.006.00001.0 / 2020
                                </div>
                                <div>
                                    <div class="col-sm-12 p-0">
                                        Nama SPPPT : Nana Kus Endang Wati
                                    </div>
                                </div>
                                <div class="mt-2">
                                    <div class="col-sm-12 p-0">
                                        Letak Bangunan : Jalan Kebangsaaan Timur No.58, RT: 03, RW:01, Kelurahan :
                                        Kenanga,
                                        Kecamatan : Walikukun, Kabupaten : Bangdoi
                                    </div>
                                </div>
                                <div class="mt-2">
                                    <div class="col-sm-12 p-0">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div>
                                                    Luas Tanah : 1.000 M2
                                                </div>
                                                <div>
                                                    Nilai Pasar : 50.000
                                                </div>
                                                <div>
                                                    Total : 50.000.000
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div>
                                                    Luas Bangunan : 500 M2
                                                </div>
                                                <div>
                                                    Nilai Pasar : 50.000
                                                </div>
                                                <div>
                                                    Total : 50.000.000
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mt-2">
                                    <div class="col-sm-12 p-0">
                                        Total NJOP PBB : 1.000.000.000
                                    </div>
                                    <div class="col-sm-12 p-0">
                                        Harga Transaksi : <strong>1.000.000.000</strong>
                                    </div>
                                </div>
                                <div class="mt-2">
                                    <div class="col-sm-12 p-0">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                Jenis Tanah : Tanah Kosong
                                            </div>
                                            <div class="col-sm-6">
                                                Status Kepemilikan : Hak Milik
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                Jenis Dokumen : Sertifikat
                                            </div>
                                            <div class="col-sm-6">
                                                No Dokumen : 1231.123131.12
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-6">
                                                Tanggal Dokumen : 32/12/2020
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div>
                                    <label for="">Photo</label>
                                    <div>
                                        <img src="{{ asset('dist/img/user8-128x128.jpg') }}" class="img-rounded"
                                            alt="User Image" style="width: 100px">
                                        <img src="{{ asset('dist/img/user8-128x128.jpg') }}" class="img-rounded"
                                            alt="User Image" style="width: 100px">
                                    </div>
                                </div>

                                <div class="mt-2">
                                    <label for="">Maps</label>
                                    <div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <h5>Persyaratan</h5>
                <div class="card card-outline card-primary px-3 pt-3">
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <span>Jenis Persyaratan</span>
                                </div>
                                <div class="col-md-4">
                                    <img src="{{ asset('dist/img/user8-128x128.jpg') }}" class="img-rounded"
                                        alt="User Image" style="width: 50px">
                                    <img src="{{ asset('dist/img/user8-128x128.jpg') }}" class="img-rounded"
                                        alt="User Image" style="width: 50px">
                                    <img src="{{ asset('dist/img/user8-128x128.jpg') }}" class="img-rounded"
                                        alt="User Image" style="width: 50px">
                                    <img src="{{ asset('dist/img/user8-128x128.jpg') }}" class="img-rounded"
                                        alt="User Image" style="width: 50px">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-8">
                                    <span>Jenis Persyaratan</span>
                                </div>
                                <div class="col-md-4">
                                    <span class="badge badge-danger">Durung di upload.</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <h5>Perhitungan BPHTB</h5>
                <div class="card card-outline card-primary">
                    <div class="card-body">
                        <div>NOP : 10.000.000.000</div>
                        <div>NOPTKP : 10.000.000.000</div>
                        <div>NOPKP : 10.000.000.000</div>
                        <div>Tarif : 5%</div>
                        <div>BPHTB : <strong>10.000.000</strong></div>
                    </div>

                </div>
            </div>
            <div class="col-md-6">
                <h5>Kolom Validasi</h5>
                <div class="card card-outline card-primary">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <label for="">Pesan</label>
                                <div>
                                    <textarea name="komentar" id="" cols="30" rows="3"></textarea>
                                </div>
                            </div>
                            <div class="col-md-4 text-center">
                                <div>
                                    <button class="btn btn-primary">Setuju</button>
                                </div>
                                <div class="mt-3">
                                    <button class="btn btn-danger">Tidak</button>
                                </div> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

</div>
</div>
@endsection