@extends('layouts.master')
@section('judulkiri')
VALIDASI BERKAS
@endsection

@section('content')

<link rel="stylesheet" href="{{ asset('internet/css/blueimp-gallery.min.css')}}">

<style>
    
    .item {
        width: 200px;
        text-align: center;
        display: block;
        background-color: transparent;
        border: 1px solid transparent;
        margin-right: 10px;
        margin-bottom: 1px;
        float: left;
    }

    .dl-horizontal {
        margin: 0;

        dt {
            float: left;
            width: 80px;
            overflow: hidden;
            clear: left;
            text-align: right;
            text-overflow: ellipsis;
            white-space: nowrap;
        }

        dd {
            margin-left: 90px;
        }
    }
</style>

@if($dataspt->t_idpersetujuan_bphtb == 1)
<div class="row">
    <div class="col-12">
        <div class="card card-outline card-primary">
            <div class="card-header bg-primary">
                <h3 class="card-title">FORM VALIDASI BERKAS</h3>
            </div>
            <div class="card-body">
                <form method="post" action="{{ url('validasi-berkas/simpanvalidasiberkas') }}" id="formtambah" name="formtambah">
                    @include('lihatdata.informasitransaksi')

                    @include('lihatdata.informasipembeli')
                    @include('lihatdata.informasipenjual')

                    <div class="row">
                        @include('lihatdata.informasitanah')
                    </div>

                    <div class="row">
                        @include('lihatdata.perhitunganbphtb')
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-outline card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-file"></i> PERSYARATAN</h3>
                                </div>
                                <div class="card-body">
                                    <table class="table table-bordered">
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Persyaratan</th>
                                            <th>Centang</th>
                                        </tr>
                                        @php
                                        $no = 1;
                                        $nomer = 1;
                                        $namasyarat = '';
                                        @endphp
                                        @foreach ($cek_persyaratan as $key => $v)
                                        @php
                                        $checked = (in_array($v->s_idpersyaratan, $persyaratanCentang)) ? 'checked' : '';
                                        @endphp
                                        @if($namasyarat == '')
                                        <tr>
                                            <td class="text-center">{{$no}}</td>
                                            <td>{{$v->s_namapersyaratan}}</td>
                                            <td class="text-center">
                                                <input type="checkbox" name="t_persyaratan_validasi_berkas[]" value="{{ $v->s_idpersyaratan }}" {{ $checked }}>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <a style="display:none;" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" href="{{asset($v->letak_file.$v->nama_file)}}">
                                                            <img data-ng-src="{{asset($v->letak_file.$v->nama_file)}}" alt="" src="{{asset($v->letak_file.$v->nama_file)}}">
                                                        </a>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <a data-ng-switch-when="true" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" class="ng-binding ng-scope" href="{{asset($v->letak_file.$v->nama_file)}}">
                                                            <img src="{{ asset($v->letak_file . $v->nama_file) }}" style="width: 100px" alt="">
                                                        </a>
                                                    </li>
                                                    @php
                                                    $namasyarat = $v->s_namapersyaratan;
                                                    $no++;
                                                    @endphp
                                                    @elseif($namasyarat <> $v->s_namapersyaratan)
                                                </ul>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td class="text-center">{{$no}}</td>
                                            <td>{{$v->s_namapersyaratan}}</td>
                                            <td class="text-center"><input type="checkbox" name="t_persyaratan_validasi_berkas[]" value="{{ $v->s_idpersyaratan }}" {{ $checked }}></td>
                                        </tr>
                                        <tr>
                                            <td></td>
                                            <td>
                                                <ul class="list-group">
                                                    <li class="list-group-item">
                                                        <a style="display:none;" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" href="{{asset($v->letak_file.$v->nama_file)}}">
                                                            <img data-ng-src="{{asset($v->letak_file.$v->nama_file)}}" alt="" src="{{asset($v->letak_file.$v->nama_file)}}">
                                                        </a>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <a data-ng-switch-when="true" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" class="ng-binding ng-scope" href="{{asset($v->letak_file.$v->nama_file)}}">
                                                            <img src="{{ asset($v->letak_file . $v->nama_file) }}" style="width: 100px" alt="">
                                                        </a>
                                                    </li>
                                                    @php
                                                    $namasyarat = $v->s_namapersyaratan;
                                                    $no++;
                                                    @endphp
                                                    @else
                                                    <li class="list-group-item">
                                                        <a style="display:none;" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" href="{{asset($v->letak_file.$v->nama_file)}}">
                                                            <img data-ng-src="{{asset($v->letak_file.$v->nama_file)}}" alt="" src="{{asset($v->letak_file.$v->nama_file)}}">
                                                        </a>
                                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                                        <a data-ng-switch-when="true" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" class="ng-binding ng-scope" href="{{asset($v->letak_file.$v->nama_file)}}">
                                                            <img src="{{ asset($v->letak_file . $v->nama_file) }}" style="width: 100px" alt="">
                                                        </a>
                                                    </li>
                                                    @endif
                                                    @endforeach
                                                </ul>
                                            </td>
                                            <td></td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card card-outline card-primary">
                                <div class="card-header">
                                    <h3 class="card-title"><i class="fas fa-edit"> </i> KOLOM VALIDASI</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row mt-2">
                                        <label class="col-sm-4">Keterangan</label>
                                        <div class="col-md-8">
                                            <input type="hidden" name="t_idspt" id="t_idspt" value="{{ $dataspt->t_idspt}}">
                                            <input type="hidden" name="t_idjenistransaksi" id="t_idjenistransaksi" value="{{ $dataspt->t_idjenistransaksi}}">
                                            <input type="hidden" name="t_id_validasi_berkas" id="t_id_validasi_berkas" value="{{ $dataspt->t_id_validasi_berkas}}">
                                            <textarea name="t_keterangan_berkas" id="t_keterangan_berkas" class="form-control {{ (($errors->has('t_keterangan_berkas') ? 'is-invalid' : '')) }}">{{ $dataspt->t_keterangan_berkas}}</textarea>
                                            @if ($errors->has('t_keterangan_berkas'))
                                            <div class="col-md-12 invalid-feedback">Isilah Keterangan Validasi Berkas</div>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="card-footer ">
                                    <a href="{{ url('validasi-berkas') }}" class="btn btn-outline-primary prevBtn pull-left" type="button"><i class="fa fa-chevron-left"></i> KEMBALI</a>
                                    <!-- <a href="{{ url('validasi-berkas') }}/{{ $dataspt->t_idspt}}/tahapkedua" class="btn btn-primary prevBtn pull-right" type="button"><i class="fa fa-save"></i> SIMPAN </a> -->
                                    <button id="pendaftaranbutton" type="submit" class="btn btn-primary pull-right"><i class="fa fa-download"></i>
                                        SIMPAN</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
@else
<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">FORM VALIDASI BERKAS</h3>
    </div>
    <div class="col-md-12">
        <center><b>DATA BELUM DI SETUJUI NOTARIS PERSYARATANNYA</b></center>
    </div>
    <div class="card-footer ">
        <a href="{{ url('validasi-berkas') }}/pilihspt" class="btn btn-warning prevBtn pull-left" type="button"><i class="fa fa-refresh"></i> KEMBALI </a>
    </div>
</div>
@endif
<div class="modal fade bd-example-modal-lg" id="loadingsimpanvalidasi" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="col-12">
                <span class="spinner-grow text-muted"></span>
                <span class="spinner-grow text-primary"></span>
                <span class="spinner-grow text-success"></span>
                <span class="spinner-grow text-info"></span>
                <span class="spinner-grow text-warning"></span>
                <span class="spinner-grow text-danger"></span>
                <span class="spinner-grow text-secondary"></span>
                <span class="spinner-grow text-dark"></span>
            </div>
        </div>
    </div>
</div>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <!-- <a class="play-pause"></a>
    <ol class="indicator"></ol> -->
</div>
@endsection

@push('scriptsbawah')

<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}'
            }
        });
    });
    
    $('#pendaftaranbutton').click(function () {
        var myForm = document.getElementById('formtambah');
        myForm.onsubmit = function () {
            var allInputs = myForm.getElementsByTagName('input');
            var input, i;

            for (i = 0; input = allInputs[i]; i++) {
                if (input.getAttribute('name') && !input.value) {
                    //input.setAttribute('name', '');
                } else {
                    jQuery('#loadingsimpanvalidasi').modal('show');
                }
            }
        };
    });
</script>


<script src="{{asset('internet/js/jquery.blueimp-gallery.min.js')}}"></script>
@endpush