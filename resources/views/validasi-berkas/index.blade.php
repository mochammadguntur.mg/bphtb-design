@extends('layouts.master')

@section('judulkiri')
VALIDASI BERKAS
@endsection

@section('content')

<div class="row">
    <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-danger"><i class="fas fa-clipboard-list"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">BELUM DIVALIDASI</span>
            <span class="info-box-number">{{ $countValidasiBelum }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-success"><i class="fas fa-check"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">PERMOHONAN LENGKAP</span>
            <span class="info-box-number">{{ $countValidasiSetuju }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-warning"><i class="fas fa-minus"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">PERMOHONAN TIDAK LENGKAP</span>
            <span class="info-box-number">{{ $countValidasiTidakSetuju }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Belum Validasi Berkas</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Sudah Validasi Berkas</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                    <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                        @include('validasi-berkas.datagrid.belum')
                        @include('validasi-berkas.datagrid.sudah')
                        @stack('datagrid-belum')
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                        @stack('datagrid-sudah')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;BATALKAN DATA VALIDASI BERKAS?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin membatalkan data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">No Daftar</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="kohirHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Tgl Daftar</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="tgldaftarHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Nama Wajib Pajak</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namawpHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-alert">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;INFORMASI PEMBATALAN!</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Silahkan Batalkan Validasi Kabid terlebih dahulu!</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Ok</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagridmodif.js') }}"></script>
<script>
    $(document).ready(function() {
        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif

        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif
    });
</script>
@stack('js')
@endpush
