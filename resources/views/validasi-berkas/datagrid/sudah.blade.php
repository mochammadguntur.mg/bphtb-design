@push('datagrid-sudah')
<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                <thead>
                    <tr>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">No</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">Perintah</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">No. Daftar</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">Tgl Daftar</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">Tahun</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">PPAT/PPATS</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">Jenis Transaksi</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">Nama WP</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">NOP</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">Nilai BPHTB<br>(Rp)</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" colspan="4">STATUS</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">Kode Bayar</th>
                        <th style="background-color: #227dc7; color:white;vertical-align:middle;" class="text-center" rowspan="2">NTPD</th>
                    </tr>
                    <tr>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Pengajuan</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Validasi Berkas</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Validasi Kabid</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Pembayaran</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>CARI:</th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-nodaftar_cari_1">
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-tgldaftar_cari_1" readonly>
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-tahun_cari_1">
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-notaris_spt_cari_1" >
                                <option value="">Semua</option>
                                @php
                                    $data_notaris = (new \ComboHelper())->getDataNotaris();
                                @endphp
                                @foreach ($data_notaris as $dn)
                                <option value="{{ $dn->s_idnotaris }}"> {{ $dn->s_namanotaris }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-jenistransaksi_cari_1">
                                <option value="">Semua</option>
                                @php
                                    $data_jenistransaksi = (new \ComboHelper())->getDataJenisTransaksi();
                                @endphp
                                @foreach ($data_jenistransaksi as $djt)
                                <option value="{{ $djt->s_idjenistransaksi }}"> {{ $djt->s_namajenistransaksi }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-namawp_cari_1">
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-nop_cari_1">
                        </th>
                        <th></th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-persetujuan_bphtb_cari_1">
                                <option value="">Semua</option>
                                <option value="1">SETUJU</option>
                                <option value="2">BELUM</option>
                            </select>
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-status_berkas_cari_1">
                                <option value="">Semua</option>
                                @php
                                    $data_statusberkas = (new \ComboHelper())->getDataStatusBerkas();
                                @endphp
                                @foreach ($data_statusberkas as $dsb)
                                <option value="{{ $dsb->s_id_status_berkas }}"> {{ $dsb->s_nama_status_berkas }}</option>
                                @endforeach
                                <option value="3">Belum</option>
                            </select>
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-status_kabid_cari_1">
                                <option value="">Semua</option>
                                @php
                                    $data_statuskabid = (new \ComboHelper())->getDataStatusKabid();
                                @endphp
                                @foreach ($data_statuskabid as $dsk)
                                <option value="{{ $dsk->s_id_status_kabid }}"> {{ $dsk->s_nama_status_kabid }}</option>
                                @endforeach
                                <option value="3">Belum</option>
                            </select>
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-pembayaran_cari_1">
                                <option value="">Semua</option>
                                @php
                                    $data_statusbayar = (new \ComboHelper())->getDataStatusBayar();
                                @endphp
                                @foreach ($data_statusbayar as $dsbyr)
                                <option value="{{ $dsbyr->s_id_status_bayar }}"> {{ $dsbyr->s_nama_status_bayar }}</option>
                                @endforeach
                            </select>
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-kodebayar_cari_1">
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-ntpd_cari_1">
                        </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="16"> Tidak ada data.</td>
                    </tr>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="card-footer clearfix pagination-footer"></div>
    </div>
</div>
@endpush

@push('js')
<script type="text/javascript">
    var datatables = datagrid({
        url: '{{ url('validasi-berkas')}}/datagrid',
        table: "#datagrid-table",
        columns: [
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: "text-right"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: false},
            {sortable: true,name: "t_kohirspt"},
            {sortable: true,name: "t_tgldaftar_spt"},
            {sortable: true,name: "t_periodespt"},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
        ],
    });

    $('#filter-tgldaftar_cari_1').daterangepicker({
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear'
        }
    });

    $('#filter-tgldaftar_cari_1').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search();
    });

    $('#filter-tgldaftar_cari_1').on('cancel.datepicker', function(ev, picker) {
        $(this).val('');
        search();
    });

    $("#filter-nodaftar_cari_1, #filter-tahun_cari_1, #filter-namawp_cari_1, #filter-nop_cari_1, #filter-kodebayar_cari_1, #filter-ntpd_cari_1").keyup(function(){
        search();
    });

    $("#filter-notaris_spt_cari_1, #filter-jenistransaksi_cari_1, #filter-persetujuan_bphtb_cari_1, #filter-status_berkas_cari_1, #filter-status_kabid_cari_1").change(function(){
        search();
    })

    function search() {
        datatables.setFilters({
            nodaftar: $("#filter-nodaftar_cari_1").val(),
            tgldaftar: $("#filter-tgldaftar_cari_1").val(),
            tahun: $("#filter-tahun_cari_1").val(),
            notaris: $("#filter-notaris_spt_cari_1").val(),
            jenistransaksi: $("#filter-jenistransaksi_cari_1").val(),
            namawp: $("#filter-namawp_cari_1").val(),
            nop: $("#filter-nop_cari_1").val(),
            status_persetujuan: $("#filter-persetujuan_bphtb_cari_1").val(),
            status_validasi_berkas: $("#filter-status_berkas_cari_1").val(),
            status_validasi_kabid: $("#filter-status_kabid_cari_1").val(),
            status_pembayaran: $("#filter-pembayaran_cari_1").val(),
            kodebayar: $("#filter-kodebayar_cari_1").val(),
            ntpd: $("#filter-ntpd_cari_1").val()
        });
        datatables.reload();
    }

    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'validasi-berkas/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].t_idspt);
            $("#kohirHapus").val(data[0].t_kohirspt);
            $("#tgldaftarHapus").val(data[0].t_tgldaftar_spt);
            $("#namawpHapus").val(data[0].t_nama_pembeli);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'validasi-berkas/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Validasi Berkas Berhasil dibatalkan!');
            datatables.reload();
        });
    });
    
    function showAlertDialog() {
        $("#modal-alert").modal('show');
    }

    $("#btnExcel, #btnPDF").click(function(a) {
        var tarif = $("#filter-tarif").val();
        var dasar_hukum = $("#filter-dasar_hukum").val();
        var created_at = $("#filter-created_at").val();
        if(a.target.id == 'btnExcel'){
            window.open('/setting-tarif-bphtb/export_xls?tarif=' + tarif + '&dasar_hukum=' + dasar_hukum + '&created_at=' + created_at);
        }else{
            window.open('/setting-tarif-bphtb/export_pdf?tarif=' + tarif + '&dasar_hukum=' + dasar_hukum + '&created_at=' + created_at);
        }

    });
</script>
@endpush
