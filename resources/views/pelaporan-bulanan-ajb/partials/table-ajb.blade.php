<div class="table-responsive">
    @if ($errors->has('t_no_ajb'))
    <div class="text-danger mt-2">
        {{ $errors->first('t_no_ajb') }}
    </div>
    @endif
    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
        <thead>
            <tr>
                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No</th>
                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No Daftar</th>
                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Tgl Daftar</th>
                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Jenis Transaksi </th>
                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Nama Wajib Pajak </th>
                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No AJB Baru</th>
                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Tgl AJB Baru</th>
                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Pilih </th>
            </tr>
            <tr>
                <th scope="filter"></th>
                <th scope="filter">
                    <input type="text" class="form-control form-control-sm" id="filter-no_daftar">
                </th>
                <th scope="filter">
                    <input type="text" class="form-control form-control-sm" id="filter-tgl_daftar">
                </th>
                <th scope="filter">
                    <select class="form-control form-control-sm" id="filter-jenis_peralihan">
                        <option value="">Pilih</option>
                    </select>
                </th>
                <th scope="filter">
                    <input type="text" class="form-control form-control-sm" id="filter-nama_wp">
                </th>
                <th scope="filter"></th>
                <th scope="filter"></th>
                <th scope="filter">
                    <input id="select-all" type="checkbox" />
                </th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center" colspan="10"> Tidak ada data.</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="card-footer col-md-12 clearfix pagination-footer">
</div>

<div class="col-12 mt-5">
    <a href="{{ route('lapor-bulanan-ajb') }}" class="btn btn-danger">Kembali</a>
    <button type="submit" class="btn btn-primary float-lg-right">Laporkan</button>
</div>