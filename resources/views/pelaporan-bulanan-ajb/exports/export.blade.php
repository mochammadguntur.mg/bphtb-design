<table>
    <tr>
        <td style="font-size: 14pt; text-align: center;" colspan="15">LAPORAN BULANAN PENERBITAN AKTA OLEH PPAT/PPATS
        </td>
    </tr>
</table>

<table style="border-collapse: collapse" width="100%">
    <thead>
        <tr>
            <th rowspan="2"
                style="border: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">
                No</th>
            <th colspan="2"
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                AKTA</th>
            <th rowspan="2"
                style="border: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                BENTUK PERBUATAN HUKUM</th>
            <th colspan="2"
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                NAMA, ALAMAT DAN NPWP</th>
            <th rowspan="2"
                style="border: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                JENIS DAN NOMOR HAK</th>
            <th rowspan="2"
                style="border: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                LETAK TANAH DAN BANGUNAN</th>
            <th colspan="2"
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                LUAS</th>
            <th rowspan="2"
                style="border: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                HARGA TRANSAKSI PEROLEHAN/PENGALIHAN HAK</th>
            <th colspan="2"
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                SPPT PBB</th>
            <th colspan="2"
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                SSPD</th>
        </tr>
        <tr>
            <th
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                NOMOR</th>
            <th
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                TANGGAL</th>
            <th
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                PIHAK YANG MENGALIHKAN</th>
            <th
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                PIHAK YANG MENERIMA</th>
            <th
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                TANAH</th>
            <th
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                BGN</th>
            <th
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                NOP/TAHUN</th>
            <th
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                NJOP</th>
            <th
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                TANGGAL</th>
            <th
                style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">
                NILAI</th>
        </tr>

    </thead>
    <tbody>
        @foreach ($laporan_ajb as $v)
            @foreach ($v->inputAjbs as $index => $row)
                @if ($row->spt != null)
                    <tr>
                        <td
                            style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">
                            {{ $index + 1 }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ $row->t_no_ajb }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ Carbon\Carbon::parse($v->t_tgl_lapor)->format('d-m-Y') }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ $row->spt->jenisTransaksi['s_namajenistransaksi'] }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ $row->spt['t_nama_penjual'] }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ $row->spt['t_nama_pembeli'] }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ $row->spt->jenisHak['s_kodehaktanah'] }} - {{ $row->spt->jenisHak['s_namahaktanah'] }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ $row->spt['t_jalan_sppt'] }}, Desa/Kel. {{ $row->spt['t_kelurahan_sppt'] }},
                            Kec. {{ $row->spt['t_kecamatam_sppt'] }}, Kab./Kota  {{ $row->spt['t_kabkot_sppt'] }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ $row->spt['t_luastanah'] }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ $row->spt['t_luasbangunan'] }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ number_format($row->spt['t_nilaitransaksispt'], 0, ',', '.') }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ $row->spt['t_nop_sppt'] }} - {{ $row->spt['t_tahun_sppt'] }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ number_format($row->spt['t_grandtotalnjop'], 0, ',', '.') }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ Carbon\Carbon::parse($row->spt['t_tgldaftar_spt'])->format('d-m-Y') }}
                        </td>
                        <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                            {{ number_format($row->spt['t_nilai_bphtb_fix']) }}
                        </td>
                    </tr>
                @endif
            @endforeach
        @endforeach
    </tbody>
</table>
