@extends('layouts.master')

@section('judulkiri')
PELAPORAN BULANAN AKTA
@endsection

@section('content')
<div class="card card-outline card-info">
    <form action="{{ route('lapor-bulanan-ajb') }}" method="POST">
        @csrf
        <div class="card-header">

        </div>
        <div class="card-body">
            <div class="form-group row mt-1">
                <label class="col-md-2">Tanggal Lapor</label>
                <div class="col-md-3">
                    <input type="text" name="t_tgl_lapor" id="t_tgl_lapor" class="form-control"
                           value="{{ old('t_tgl_lapor') ?? date('d-m-Y') }}" readonly>
                    @if ($errors->has('t_tgl_lapor'))
                    <div class="text-danger">
                        {{ $errors->first('t_tgl_lapor') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row mt-1">
                <label class="col-md-2">Lapor Untuk Bulan</label>
                <div class="col-md-3">
                    <select name="t_untuk_bulan" id="t_untuk_bulan" class="custom-select">
                        <option value="">Pilh</option>
                        <option value="1">Januari</option>
                        <option value="2">Februari</option>
                        <option value="3">Maret</option>
                        <option value="4">April</option>
                        <option value="5">Mei</option>
                        <option value="6">Juni</option>
                        <option value="7">Juli</option>
                        <option value="8">Agustus</option>
                        <option value="9">September</option>
                        <option value="10">Oktober</option>
                        <option value="11">November</option>
                        <option value="12">Desember</option>
                    </select>
                </div>
            </div>
            <div class="form-group row mt-1">
                <label class="col-md-2">Tahun Pajak</label>
                <div class="col-md-3">
                    <input type="text" name="t_untuk_tahun" id="t_untuk_tahun" class="form-control" value="<?= date('Y') ?>" onkeypress="return numberOnly(event)" readonly>
                    @if ($errors->has('t_untuk_tahun'))
                    <div class="text-danger">
                        {{ $errors->first('t_untuk_tahun') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row mt-1">
                <label class="col-md-2">PPAT/Notaris</label>
                <div class="col-md-3">
                    <select name="t_ppat_notaris" id="t_ppat_notaris" class="custom-select">
                        <option value="">Pilih</option>
                        @if ($session->s_id_hakakses == 4)
                            <option value="{{ $session->s_idnotaris }}">{{ $session->name }}</option>
                        @else
                            @foreach ($notaris as $ppat)
                                <option value="{{ $ppat->s_idnotaris }}">{{ $ppat->s_namanotaris }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
            <div class="form-group row mt-1">
                <label class="col-md-2">Keterangan</label>
                <div class="col-md-3">
                    <textarea name="t_keterangan" id="t_keterangan" cols="30" rows="1" class="form-control">{{ old('t_keterangan') }}</textarea>
                    @if ($errors->has('t_keterangan'))
                    <div class="text-danger">
                        {{ $errors->first('t_keterangan') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="form-group row mt-1">
                <label class="col-md-2"></label>
                <div class="col-md-3">    
                    <button type="button" class="btn btn-primary" id="cariData">Cari</button>
                </div>
            </div>

            @include('pelaporan-bulanan-ajb.partials.table-ajb')
        </div>
    </form>
</div>
@endsection

@push('styles')
<style>
    body {
        position: relative;
    }

</style>
@endpush

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
                        $(document).ready(function () {
                            $('#t_untuk_tahun').datepicker({
                                autoclose: true,
                                format: " yyyy",
                                viewMode: "years",
                                minViewMode: "years",
                                orientation: "bottom left"
                            });
                        });

                        var selectedRowIds = [];
                        var datatables = datagrid({
                            url: "{{ url('lapor-bulanan-ajb/datagrid-ajb') }}",
                            table: "#datagrid-table",
                            data: {
                                checkbox: true,
                                filter: {},
                                sorting: {},
                                pagination: {
                                    pageNumber: 0,
                                    pageSize: 10
                                }
                            },
                            columns: [{
                                    class: "text-center"
                                },
                                {
                                    class: ""
                                },
                                {
                                    class: "text-center"
                                },
                                {
                                    class: "text-center"
                                },
                                {
                                    class: "text-center"
                                },
                                {
                                    class: "text-center"
                                },
                                {
                                    class: "text-center"
                                },
                                {
                                    class: "text-center"
                                },
                                {
                                    class: "text-center"
                                },
                            ],
                            orders: [{
                                    sortable: false
                                },
                                {
                                    sortable: true,
                                    name: "no_daftar"
                                },
                                {
                                    sortable: true,
                                    name: "tgl_daftar"
                                },
                                {
                                    sortable: true,
                                    name: "jenis_peralihan"
                                },
                                {
                                    sortable: false
                                },
                                {
                                    sortable: false
                                },
                                {
                                    sortable: false
                                },
                                {
                                    sortable: false
                                },
                                {
                                    sortable: false
                                },
                                {
                                    sortable: false
                                },
                            ],
                        });

                        $('#cariData').click(() => {

                            datatables.setFilters({
                                bulan: $('#t_untuk_bulan').val(),
                                tahun: $('#t_untuk_tahun').val(),
                                ppat: $('#t_ppat_notaris').val()
                            });
                            datatables.reload();
                        });

                        $('#select-all').click(function (event) {
                            if (this.checked) {
                                $('tbody input[type="checkbox"]', '#datagrid-table').each(function () {
                                    this.checked = true;
                                    var temp = {
                                        t_idajb: $(this).val(),
                                        no_ajb: $(this).attr('lable-value')
                                    }
                                    var index = selectedRowIds.findIndex(x => x.t_idajb === temp.t_idajb)

                                    if (index === -1 && $(this).val() != "on") {
                                        selectedRowIds.push(temp);
                                    }
                                });
                            } else {
                                $('tbody input[type="checkbox"]', '#datagrid-table').each(function () {
                                    this.checked = false;
                                    var temp = {
                                        t_idajb: $(this).val(),
                                        no_ajb: $(this).attr('lable-value')
                                    }
                                    var index = selectedRowIds.findIndex(x => x.t_idajb === temp.t_idajb)
                                    selectedRowIds.splice(index, 1);
                                });
                            }
                        });

                        function updateSlectionRows() {
                            var $chkbox_all = $('tbody input[type="checkbox"]', '#datagrid-table');
                            var $chkbox_checked = $('tbody input[type="checkbox"]:checked');
                            var chkbox_select_all = $('thead input[id="select-all"]', '#datagrid-table').get(0);


                            if ($chkbox_checked.length === 0) {
                                chkbox_select_all.checked = false;
                                if ('indeterminate' in chkbox_select_all) {
                                    chkbox_select_all.indeterminate = false;
                                }
                            } else if ($chkbox_checked.length === $chkbox_all.length) {
                                chkbox_select_all.checked = true;
                                if ('indeterminate' in chkbox_select_all) {
                                    chkbox_select_all.indeterminate = false;
                                }
                            } else {
                                chkbox_select_all.checked = true;
                                if ('indeterminate' in chkbox_select_all) {
                                    chkbox_select_all.indeterminate = true;
                                }
                            }
                        }

                        $('#datagrid-table tbody').on('change', 'input[type="checkbox"]', (e) => {
                            var temp = {
                                t_idajb: $(e.target).val(),
                                no_ajb: $(e.target).attr('lable-value')
                            }
                            var index = selectedRowIds.findIndex(x => x.t_idajb === temp.t_idajb)

                            if (index === -1) {
                                selectedRowIds.push(temp);
                            } else {
                                selectedRowIds.splice(index, 1);
                            }
                            updateSlectionRows();
                        });

                        $('#datagrid-table tbody').on('DOMSubtreeModified', () => {
                            $('tbody input[type="checkbox"]').each(function () {
                                var temp = {
                                    t_idajb: $(this).val(),
                                    no_ajb: $(this).attr('lable-value')
                                }
                                var index = selectedRowIds.findIndex(x => x.t_idajb === temp.t_idajb)
                                if (index !== -1) {
                                    $(this).prop('checked', true);
                                }
                                updateSlectionRows();
                            });
                        });

                        $('#btnPilih').on('click', () => {
                            var picked = '<span class="selected_data"><ul class="selected_selection">';
                            var input_idajb = '';

                            selectedRowIds.forEach(el => {
                                picked += `<li class="list_selected" value="${ el.t_idajb }">
                                        <span class="list_selected_remove">x</span>
                                        ${ el.no_ajb }
                                    </li>`;
                                input_idajb += `<input type="hidden" name="id_ajbs[]"  value="${ el.t_idajb }">`;
                            })
                            picked += '</ul></span>';

                            $('#t_idajbs').html(input_idajb);
                            $('#label_ajbs').html(picked);
                        });

                        function getWaktu() {
                            var bulan = '{{ now()->month - 1 }}';
                            $('#t_untuk_bulan').val(bulan);
                            // $('#t_untuk_bulan option:not(:selected)').prop('disabled', true)
                        }

</script>
@endpush
