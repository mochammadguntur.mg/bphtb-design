@extends('layouts.master')

@section('judulkiri')
DATA AJB SUDAH DILAPORKAN
@endsection

@section('content')
<div class="card card-outline card-info shadow-sm">
    <div class="card-header">
        <h3 class="card-title">
            <a href="{{ route('lapor-bulanan-ajb.create') }}" class="btn btn-primary">
                <i class="fas fa-plus"></i> Tambah
            </a>
        </h3>
        <div class="card-tools">
            <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                <i class="fas fa-file-excel"></i> Excel
            </a>
            <a href="javascript:void(0)" class="btn btn-danger" id="btnPDF">
                <i class="fas fa-file-pdf"></i> PDF
            </a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                <thead>
                    <tr>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No </th>
                        <th scope='col' style="background-color: #227dc7; color:white; width: 65px;" class="text-center">No Lapor
                        </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Tgl Lapor
                        </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">PPAT/PPATS
                        </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Nama WP
                        </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">NOP
                        </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Jenis Transaksi </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Laporan Untuk </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No AJB Baru
                        </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Tgl AJB Baru
                        </th>
                    </tr>
                    <tr>
                        <th scope="filter"></th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-no_lapor">
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-tgl_lapor">
                        </th>
                        <th scope="filter">
                            <select class="form-control form-control-sm" id="filter-ppat">
                                <option value="">Pilih</option>
                            </select>
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-nama_wp">
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm masked-nop" id="filter-nop_sppt">
                        </th>
                        <th scope="filter">
                            <select class="form-control form-control-sm" id="filter-jenis_peralihan">
                                <option value="">Pilih</option>
                            </select>
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-lapor_untuk">
                        </th>
                        <th scope="filter"></th>
                        <th scope="filter"></th>

                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="10"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix pagination-footer">
        </div>
    </div>
</div>

@include('pelaporan-ajb.partials.modal-delete')
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ('{{ session("success") }}') {
            toastr.success('{{ session("success") }}')
        }

        if ('{{ session("error") }}') {
            toastr.error('{{ session("error") }}')
        }

        $('#filter-tgl_lapor').datepicker({
            format: "dd-mm-yyyy",
        });

        $('#filter-lapor_untuk').datepicker({
            format: "mm-yyyy",
            viewMode: "months",
            minViewMode: "months"
        });

        comboJenisPeralihan();
        comboPPAT()
        search()
    });

    var datatables = datagrid({
        url: 'lapor-bulanan-ajb/datagrid',
        table: "#datagrid-table",
        columns: [{
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "no_daftar"
            },
            {
                sortable: true,
                name: "tgl_daftar"
            },
            {
                sortable: true,
                name: "jenis_peralihan"
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
        ]

    });

    $("#filter-no_lapor, #filter-tgl_lapor, #filter-nama_wp, #filter-nop_sppt, #filter-jenis_peralihan, #filter-lapor_untuk, #filter-ppat")
        .change(function() {
            search();
        });


    function search() {
        datatables.setFilters({
            no_lapor: $('#filter-no_lapor').val(),
            tgl_lapor: $('#filter-tgl_lapor').val(),
            nama_wp: $('#filter-nama_wp').val(),
            nop_sppt: $('#filter-nop_sppt').val(),
            jenis_peralihan: $("#filter-jenis_peralihan").val(),
            lapor_untuk: $("#filter-lapor_untuk").val(),
            ppat: $('#filter-ppat').val()
        });
        datatables.reload();
    }

    function comboPPAT() {
        const select = document.getElementById('filter-ppat');
        let ppats = JSON.parse('{!! json_encode($ppats) !!}');
        let option = '<option class="pl-5" value="">Pilih</option>';

        ppats.forEach(el => {
            option += `<option value="${ el.s_idnotaris }">${ el.s_namanotaris }</option>`;
        });

        select.innerHTML = option;
    }

    function comboJenisPeralihan() {
        const select = document.getElementById('filter-jenis_peralihan');
        let jenis_hak = JSON.parse('{!! json_encode($jenis_peralihan) !!}');
        // console.log(jenis_hak);
        let option = '<option class="pl-5" value="">Pilih</option>';

        jenis_hak.forEach(el => {
            option += `<option value="${ el.s_idjenistransaksi }">${ el.s_kodejenistransaksi } | ${ el.s_namajenistransaksi }</option>`;
        });

        select.innerHTML = option;
    }

    $("#btnExcel, #btnPDF").click(function(a) {
        var no_lapor = $('#filter-no_lapor').val()
        var tgl_lapor = $('#filter-tgl_lapor').val();
        var nama_wp = $('#filter-nama_wp').val();
        var nop_sppt = $('#filter-nop_sppt').val();
        var jenis_peralihan = $("#filter-jenis_peralihan").val();
        var lapor_untuk = $("#filter-lapor_untuk").val();

        if (a.target.id == 'btnExcel') {
            window.open(
                `{{ url('lapor-bulanan-ajb/export_xls') }}?no_lapor=${ no_lapor }&tgl_lapor=${ tgl_lapor }&lapor_untuk=${ lapor_untuk }&jenis_peralihan=${ jenis_peralihan }&nama_wp=${ nama_wp }&nop_sppt=${ nop_sppt }`
            );
        } else {
            window.open(
                `{{ url('lapor-bulanan-ajb/export_pdf') }}?no_lapor=${ no_lapor }&tgl_lapor=${ tgl_lapor }&lapor_untuk=${ lapor_untuk }&jenis_peralihan=${ jenis_peralihan }&nama_wp=${ nama_wp }&nop_sppt=${ nop_sppt }`
            );
        }
    });
</script>
@endpush