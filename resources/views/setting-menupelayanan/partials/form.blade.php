<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">NAMA LAYANAN</label>
    <div class="col-md-4">
        <input type="text" name="s_namamenupelayanan" class="form-control @error('s_namamenupelayanan') is-invalid @enderror" id="s_namamenupelayanan" placeholder="NIP" value="{{ old('s_namamenupelayanan') ?? $menupelayanan->s_namamenupelayanan }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_namamenupelayanan'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_namamenupelayanan') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">STATUS</label>
    <div class="col-md-4">
        <select class="form-control @error('s_id_statusmenupelayanan') is-invalid @enderror" id="s_id_statusmenupelayanan" name="s_id_statusmenupelayanan" >
            
            <!-- <option style="display:none" value="">Silahkan Pilih</option> -->
            
            <option value="1" @if($menupelayanan->s_id_statusmenupelayanan == 1) selected @endif > AKTIF </option>
            <option value="2" @if($menupelayanan->s_id_statusmenupelayanan == 2) selected @endif> TIDAK AKTIF </option>    
        </select>
    </div>
    
</div>


<a href="/setting-menupelayanan" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>

@push('scripts')
<script type="text/javascript">
    

    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert").slideUp(500);
    });

</script>
@endpush
