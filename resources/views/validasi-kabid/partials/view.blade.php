<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-check"></i> INFORMASI TRANSAKSI</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">Tanggal Daftar</div>
                        <div class="col-sm-10">: <strong
                                class="text-blue">{{ date('d-m-Y', strtotime($dataspt->t_tgldaftar_spt)) }}</strong>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">Jenis Transaksi</div>
                        <div class="col-sm-10">: {{ $dataspt->s_namajenistransaksi }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">PPAT/PPATS</div>
                        <div class="col-sm-10">: {{ $dataspt->s_namanotaris }}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-user"></i> PENERIMA HAK (PIHAK KEDUA)</h3>
                </div>
                @php
                //  dd($dataspt)
                @endphp
                <div class="card-body">
                    @if ($dataspt->t_idbidang_usaha == 1)
                        <div class="row">
                            <div class="col-sm-3 text-blue text-bold"><u>PRIBADI</u></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NIK</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nik_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NPWP</div>
                            <div class="col-sm-9">: {{ $dataspt->t_npwp_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Nama</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nama_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Alamat</div>
                            <div class="col-sm-9">: {{ $dataspt->t_jalan_pembeli }} RT/RW
                                {{ $dataspt->t_rt_pembeli . '/' . $dataspt->t_rw_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $dataspt->t_namakelurahan_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $dataspt->t_namakecamatan_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $dataspt->t_kabkota_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Kode Pos</div>
                            <div class="col-sm-9">: {{ $dataspt->t_kodepos_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">No.Telp</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nohp_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Email</div>
                            <div class="col-sm-9">: {{ $dataspt->t_email_pembeli }}</div>
                        </div>

                    @endif
                    @if ($dataspt->t_idbidang_usaha == 2)
                        <div class="row">
                            <div class="col-sm-3 text-blue text-bold"><u>BADAN</u></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">SIUP</div>
                            <div class="col-sm-9">: {{ $dataspt->t_siup_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NIB</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nib_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NPWP</div>
                            <div class="col-sm-9">: {{ $dataspt->t_npwp_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Nama</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nama_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Alamat</div>
                            <div class="col-sm-9">: {{ $dataspt->t_jalan_pembeli }} RT/RW
                                {{ $dataspt->t_rt_pembeli . '/' . $dataspt->t_rw_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $dataspt->t_namakelurahan_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $dataspt->t_namakecamatan_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $dataspt->t_kabkota_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Kode Pos</div>
                            <div class="col-sm-9">: {{ $dataspt->t_kodepos_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">No.Telp</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nohp_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Email</div>
                            <div class="col-sm-9">: {{ $dataspt->t_email_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 text-blue text-bold"><u>PENANGGUNG JAWAB</u></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NIK</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_nik_pngjwb_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NPWP</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_npwp_pngjwb_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Nama</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_nama_pngjwb_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Alamat</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_jalan_pngjwb_pembeli }} RT/RW
                                {{ $dataspt->t_b_rt_pngjwb_pembeli . '/' . $dataspt->t_b_rw_pngjwb_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kelurahan</strong>
                                {{ $dataspt->t_b_namakel_pngjwb_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kecamatan</strong>
                                {{ $dataspt->t_b_namakec_pngjwb_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $dataspt->t_b_kabkota_pngjwb_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Kode Pos</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_kodepos_pngjwb_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">No.Telp</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_nohp_pngjwb_pembeli }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Email</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_email_pngjwb_pembeli }}</div>
                        </div>

                    @endif
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-user"></i> INFROMASI PELEPAS HAK (PIHAK PERTAMA)</h3>
                </div>
                <div class="card-body">
                    @if ($dataspt->t_idbidang_usaha == 1)
                        <div class="row">
                            <div class="col-sm-3 text-blue text-bold"><u>PRIBADI</u></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NIK</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nik_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NPWP</div>
                            <div class="col-sm-9">: {{ $dataspt->t_npwp_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Nama</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nama_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Alamat</div>
                            <div class="col-sm-9">: {{ $dataspt->t_jalan_penjual }} RT/RW
                                {{ $dataspt->t_rt_penjual . '/' . $dataspt->t_rw_penjual }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $dataspt->t_namakel_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $dataspt->t_namakec_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $dataspt->t_kabkota_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Kode Pos</div>
                            <div class="col-sm-9">: {{ $dataspt->t_kodepos_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">No.Telp</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nohp_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Email</div>
                            <div class="col-sm-9">: {{ $dataspt->t_email_penjual }}</div>
                        </div>

                    @endif
                    @if ($dataspt->t_idbidang_usaha == 2)
                        <div class="row">
                            <div class="col-sm-3 text-blue text-bold"><u>BADAN</u></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">SIUP</div>
                            <div class="col-sm-9">: {{ $dataspt->t_siup_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NIB</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nib_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NPWP</div>
                            <div class="col-sm-9">: {{ $dataspt->t_npwp_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Nama</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nama_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Alamat</div>
                            <div class="col-sm-9">: {{ $dataspt->t_jalan_penjual }} RT/RW
                                {{ $dataspt->t_rt_penjual . '/' . $dataspt->t_rw_penjual }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $dataspt->t_namakel_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $dataspt->t_namakec_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $dataspt->t_kabkota_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Kode Pos</div>
                            <div class="col-sm-9">: {{ $dataspt->t_kodepos_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">No.Telp</div>
                            <div class="col-sm-9">: {{ $dataspt->t_nohp_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Email</div>
                            <div class="col-sm-9">: {{ $dataspt->t_email_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3 text-blue text-bold"><u>PENANGGUNG JAWAB</u></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NIK</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_nik_pngjwb_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">NPWP</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_npwp_pngjwb_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Nama</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_nama_pngjwb_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Alamat</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_jalan_pngjwb_penjual }} RT/RW
                                {{ $dataspt->t_b_rt_pngjwb_pembeli . '/' . $dataspt->t_b_rw_pngjwb_pembeli }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kelurahan</strong>
                                {{ $dataspt->t_b_namakel_pngjwb_penjual }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kecamatan</strong>
                                {{ $dataspt->t_b_namakec_pngjwb_penjual }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3"></div>
                            <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $dataspt->t_b_kabkota_pngjwb_penjual }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Kode Pos</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_kodepos_pngjwb_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">No.Telp</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_nohp_pngjwb_penjual }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Email</div>
                            <div class="col-sm-9">: {{ $dataspt->t_b_email_pngjwb_penjual }}</div>
                        </div>

                    @endif
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-map"></i> INFORMASI OBJEK</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">NOP/Tahun</div>
                        <div class="col-sm-9">: {{ $dataspt->t_nop_sppt . ' / ' . $dataspt->t_tahun_sppt }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Nama SPPT</div>
                        <div class="col-sm-9">: {{ $dataspt->t_nama_sppt }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Letak</div>
                        <div class="col-sm-4">: {{ $dataspt->t_jalan_sppt }}</div>
                        <div class="col-sm-2">RT/RW</div>
                        <div class="col-sm-3">: {{ $dataspt->t_rt_sppt . '/' . $dataspt->t_rw_sppt }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Kelurahan</div>
                        <div class="col-sm-4">: {{ $dataspt->t_kelurahan_sppt }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Kecamatan</div>
                        <div class="col-sm-4">: {{ $dataspt->t_kecamatan_sppt }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Kab./Kota</div>
                        <div class="col-sm-4">: {{ $dataspt->t_kabkota_sppt }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Jenis Transaksi</div>
                        <div class="col-sm-4">: {{ $dataspt->s_namajenistransaksi }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Kepemilikan</div>
                        <div class="col-sm-4">: {{ $dataspt->s_namahaktanah }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Jenis Dok.</div>
                        <div class="col-sm-4">: {{ $dataspt->s_namadoktanah }}</div>
                        <div class="col-sm-2">No Dok.</div>
                        <div class="col-sm-3">: {{ $dataspt->t_nosertifikathaktanah }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Tgl Dok.</div>
                        <div class="col-sm-4">: {{ date('d-m-Y', strtotime($dataspt->t_tgldok_tanah)) }}</div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-sm-12"><strong>Foto Objek</strong></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12 text-center">
                            @php

                            $noobjek = 1;

                            @endphp

                            @if (count($data_fotoobjek) > 0)
                                @foreach ($data_fotoobjek as $key => $vobj)

                                    @if (!empty($vobj->nama_file))
                                        <a style="display:none;"
                                            data-ng-href="{{ asset($vobj->letak_file . $vobj->nama_file) }}"
                                            title="{{ $vobj->nama_file }}" download="{{ $vobj->nama_file }}"
                                            data-gallery=""
                                            href="{{ asset($vobj->letak_file . $vobj->nama_file) }}"><img
                                                data-ng-src="{{ asset($vobj->letak_file . $vobj->nama_file) }}" alt=""
                                                src="{{ asset($vobj->letak_file . $vobj->nama_file) }}"></a>
                                        <a data-ng-switch-when="true"
                                            data-ng-href="{{ asset($vobj->letak_file . $vobj->nama_file) }}"
                                            title="{{ $vobj->nama_file }}" download="{{ $vobj->nama_file }}"
                                            data-gallery="" class="ng-binding ng-scope"
                                            href="{{ asset($vobj->letak_file . $vobj->nama_file) }}">
                                            <img src="{{ asset($vobj->letak_file . $vobj->nama_file) }}" width="180px"
                                                alt="">
                                        </a>

                                    @endif

                                    @php $noobjek++; @endphp
                                @endforeach
                            @else
                                <center>TIDAK ADA DATA</center>
                            @endif
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-map"></i> PETA OBJEK</h3>
                </div>
                <div class="card-body row">
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-calculator"></i> INFORMASI PERHITUNGAN BPHTB</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-2">Luas Tanah/Bumi</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_luastanah, 0, ',', '.') }} m<sup>2</sup></div>
                    <div class="col-sm-2">NJOP/m<sup>2</sup> Tanah/Bumi</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_njoptanah, 0, ',', '.') }}</div>
                    <div class="col-sm-2">NJOP Tanah/Bumi</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_totalnjoptanah, 0, ',', '.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Luas Bangunan</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_luasbangunan, 0, ',', '.') }} m<sup>2</sup>
                    </div>
                    <div class="col-sm-2">NJOP/m<sup>2</sup> Bangunan</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_njopbangunan, 0, ',', '.') }}</div>
                    <div class="col-sm-2">NJOP Bangunan</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_totalnjopbangunan, 0, ',', '.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">NJOP PBB</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_grandtotalnjop, 0, ',', '.') }}</div>
                    <div class="col-sm-2">Harga Transaksi</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_nilaitransaksispt, 0, ',', '.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">NPOP</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_npop_bphtb, 0, ',', '.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">NPOPTKP</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_npoptkp_bphtb, 0, ',', '.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">NPOPKP</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_npopkp_bphtb, 0, ',', '.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Tarif (%)</div>
                    <div class="col-sm-2">: {{ number_format($dataspt->t_persenbphtb, 0, ',', '.') }}%</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">BPHTB Terhutang</div>
                    <div class="col-sm-2">: <strong class="text-red"
                            style="font-size: 14pt">{{ number_format($dataspt->t_npopkp_bphtb * $dataspt->t_persenbphtb / 100, 0, ',', '.') }}</strong>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
