<style>
    #googleMap {
        height: 400px;
        width: 100%;
        border:1px solid #ccc;
    }
</style>

<div class="col-md-12">
    <div class="card card-primary card-outline">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-map"></i> HISTORY TRANSAKSI BERDASARKAN PETA TITIK KOORDINAT</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip">
                    <i class="fas fa-minus"></i>
                </button>
            </div>
        </div>
        <div class="card-body form-horizontal">
            <div id="googleMap"></div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
    $(function () {
        initialize();
    });

    function initialize() {
        var locations = <?= $historyTransaksi ?>;

        var propertiPeta = {
            center: new google.maps.LatLng(<?= $dataspt->s_latitude ?>, <?= $dataspt->s_longitude ?>),
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);
        var marker, i;
        var i = 1;
        locations.forEach(element => {
            var infowindow = new google.maps.InfoWindow({
                disableAutoPan: true
            });
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(element.s_latitude, element.s_longitude),
                map: peta,
                animation: google.maps.Animation.DROP
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    var njoptanah = '<b>' + formatRupiah(element.t_njoptanah, 0) + '/m2</b>';
                    infowindow.setContent(njoptanah);
                    infowindow.open(peta, marker);
                }
            })(marker, i));
            i++;
            google.maps.event.trigger(marker, 'click');
        });
    }
//    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endpush
