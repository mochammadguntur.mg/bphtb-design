<div class="col-md-12">
    <div class="card card-outline card-primary">
        <div class="card-header">
            <h3 class="card-title"><i class="fas fa-chart-line"></i> ANALISA SISTEM KEWAJARAN</h3>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-sm-3">
                    <strong>Harga NPOP/m<sup>2</sup></strong>
                </div>
                <div class="col-sm-1">
                    : Rp.
                </div>
                <div class="col-sm-2 text-right font-weight-bold">
                    {{ number_format($analisadata['harga_diajukan'], 0, ',', '.') }},-
                </div>
                <div class="col-sm-6">
                    {{-- Diperoleh dari : ({{ number_format($dataspt->t_nilaitransaksispt, 0, ',', '.')}} - {{ number_format($dataspt->t_totalnjopbangunan, 0, ',', '.') }} ) / {{ number_format($dataspt->t_luastanah, 0, ',', '.') }} --}}
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-12">
                    <strong>Harga History Transaksi/m<sup>2</sup></strong>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <strong>Harga Transaksi/m<sup>2</sup></strong> (<strong class="text-red">TERENDAH</strong>)
                </div>
                <div class="col-sm-1">
                    : Rp.
                </div>
                <div class="col-sm-2 text-right font-weight-bold">
                    {{ number_format($analisadata['min'], 0, ',', '.') }},-
                </div>
                <div class="col-sm-3">
                    {{ number_format($analisadata['harga_diajukan'], 0, ',', '.') }}/{{ number_format($analisadata['min'], 0, ',', '.') }} x 100% = <strong class="text-{{ $analisadata['prasentasemin'][0]->s_css_warna }}"> {{ number_format($analisadata['minWajar'], 2, ',', '.') }} % </strong>
                </div>
                <div class="col-sm-3" >
                    (<strong class="text-{{ $analisadata['prasentasemin'][0]->s_css_warna }}"> HARGA {{ strtoupper($analisadata['prasentasemin'][0]->s_ketpresentase_wajar) }}</strong> )
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <strong>Harga Transaksi/m<sup>2</sup></strong> (<strong class="text-warning">RATA-RATA</strong>)
                </div>
                <div class="col-sm-1">
                    : Rp.
                </div>
                <div class="col-sm-2 text-right font-weight-bold">
                    {{ number_format($analisadata['avg'], 0, ',', '.') }},-
                </div>
                <div class="col-sm-3">
                    {{ number_format($analisadata['harga_diajukan'], 0, ',', '.') }}/{{ number_format($analisadata['avg'], 0, ',', '.') }} x 100% = <strong class="text-{{ $analisadata['prasentaseavg'][0]->s_css_warna }}"> {{ number_format($analisadata['avgWajar'], 2, ',', '.') }} % </strong>
                </div>
                <div class="col-sm-3" >
                    (<strong class="text-{{ $analisadata['prasentaseavg'][0]->s_css_warna }}"> HARGA {{ strtoupper($analisadata['prasentaseavg'][0]->s_ketpresentase_wajar) }}</strong> )
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <strong>Harga Transaksi/m<sup>2</sup></strong> (<strong class="text-blue">TERTINGGI</strong>)
                </div>
                <div class="col-sm-1">
                    : Rp.
                </div>
                <div class="col-sm-2 text-right font-weight-bold">
                    {{ number_format($analisadata['max'], 0, ',', '.') }},-
                </div>
                <div class="col-sm-3">
                    {{ number_format($analisadata['harga_diajukan'], 0, ',', '.') }}/{{ number_format($analisadata['max'], 0, ',', '.') }} x 100% = <strong class="text-{{ $analisadata['prasentasemax'][0]->s_css_warna }}">  {{ number_format($analisadata['maxWajar'], 2, ',', '.') }} % </strong>
                </div>
                <div class="col-sm-3" >
                    (<strong class="text-{{ $analisadata['prasentasemax'][0]->s_css_warna }}"> HARGA {{ strtoupper($analisadata['prasentasemax'][0]->s_ketpresentase_wajar) }}</strong> )
                </div>
            </div>
            <br>
            <div class="row">
                <div class="col-sm-3">
                    <strong>Harga Acuan/m<sup>2</sup> <br>({{ $dataspt->nama_jenistanah }})</strong>
                </div>
                <div class="col-sm-1">
                    : Rp.
                </div>
                <div class="col-sm-2 text-right font-weight-bold">
                    {{ number_format(@$analisadata['acuan'], 0, ',', '.') }},-
                </div>
                {{-- <div class="col-sm-3">
                    {{ number_format($analisadata['harga_diajukan'], 0, ',', '.') }}/{{  number_format(@$analisadata['acuan'], 0, ',', '.') }} x 100% = <strong class="text-{{ $analisadata['prasentaseacuan'][0]->s_css_warna }}"> {{ number_format($analisadata['acuanWajar'], 2, ',', '.') }} % </strong>
                </div>
                <div class="col-sm-3" >
                    (<strong class="text-{{ $analisadata['prasentaseacuan'][0]->s_css_warna }}"> HARGA {{ strtoupper($analisadata['prasentaseacuan'][0]->s_ketpresentase_wajar) }}</strong> )
                </div> --}}
            </div>
            <br>
            <div class="row">
                <div class="col-sm-3">
                    <strong class="text-blue">SARAN HARGA TANAH/m<sup>2</sup></strong>
                </div>
                <div class="col-sm-1">
                    : Rp.
                </div>
                <div class="col-sm-2 text-right font-weight-bold text-blue">
                    {{ number_format(@$analisadata['saranPermeter'], 0, ',', '.') }},-
                </div>
            </div>
            <div class="row">
                <div class="col-sm-3">
                    <strong class="text-blue">SARAN HARGA TOTAL SISTEM</strong>
                </div>
                <div class="col-sm-1">
                    : Rp.
                </div>
                <div class="col-sm-2 text-right font-weight-bold text-blue">
                    {{ number_format(@$analisadata['saranTotal'], 0, ',', '.') }},-
                </div>
                <div class="col-sm-6">
                    ({{ number_format(@$analisadata['saranPermeter'], 0, ',', '.') }} x {{ number_format($dataspt->t_luastanah, 0, ',', '.') }}) + {{ number_format($dataspt->t_totalnjopbangunan, 0, ',', '.') }} = Rp. {{ number_format(@$analisadata['saranTotal'], 0, ',', '.') }},-
                </div>
            </div>
        </div>
    </div>
</div>
