@extends('layouts.master')

@section('judulkiri')
VALIDASI KABID
@endsection

@section('content')

<link rel="stylesheet" href="{{ asset('internet/css/blueimp-gallery.min.css')}}">
<style>
    #map {
        width: 100%;
        height: 400px;
    }
</style>

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">FORM VALIDASI KABID</h3>
    </div>

    <form method="post" action="{{ url('validasi-kabid/simpanvalidasikabid') }}" id="formtambah" name="formtambah">
        @csrf
        <br>
        <div class="col-md-12">
            @include('lihatdata.informasitransaksi')
            @include('lihatdata.informasipembeli')
            @include('lihatdata.informasipenjual')
        </div>
        <br>
        @include('lihatdata.informasitanah')
        <br>
        @include('lihatdata.perhitunganbphtb')
        <br>
        @include('lihatdata.informasipersyaratan')
        {{-- <br> --}}
        @include('validasi-kabid.analisa-sistem.analisa')
        <!--@include('validasi-kabid.analisa-sistem.peta')-->
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-clipboard-check"></i> KOLOM VALIDASI BERKAS</h3>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-3">STATUS VALIDASI BERKAS</div>
                        <div class="col-SM-9">
                            <select name="s_id_status_berkas" id="s_id_status_berkas"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1"
                                    required="required" disabled="true">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_statusberkas as $a)
                                <option value="{{ $a->s_id_status_berkas }}"
                                        {{ $dataspt->s_id_status_berkas == $a->s_id_status_berkas ? 'selected' : '' }}>
                                    {{ $a->s_nama_status_berkas }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">KETERANGAN</div>
                        <div class="col-sm-9">
                            <textarea name="t_keterangan_berkas" id="t_keterangan_berkas" class="form-control"
                                      readonly>{{ $dataspt->t_keterangan_berkas}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-check-double"></i> KOLOM VALIDASI KABID</h3>
                </div>
                <div class="card-body">
                    <div class="row mt-2">
                        <label class="col-sm-3">Keterangan</label>
                        <div class="col-md-9">
                            <input type="hidden" name="t_idspt" id="t_idspt" value="{{ old('t_idspt') ?? $dataspt->t_idspt }}">
                            <input type="hidden" name="t_id_validasi_berkas" id="t_id_validasi_berkas" value="{{ old('t_id_validasi_berkas') ?? $dataspt->t_id_validasi_berkas}}">
                            <input type="hidden" name="t_id_validasi_kabid" id="t_id_validasi_kabid" value="{{ old('t_id_validasi_kabid') ?? $dataspt->t_id_validasi_kabid}}">
                            <input type="hidden" name="s_id_status_kabid" id="s_id_status_kabid" value="{{  old('s_id_status_kabid') }}">
                            <textarea name="t_keterangan_kabid" id="t_keterangan_kabid" class="form-control @error('t_keterangan_kabid') is-invalid @enderror">{{ $dataspt->t_keterangan_kabid }}</textarea>
                            @error('t_keterangan_kabid')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
                <div class="card-footer text-center">
                    <div class="pull-left">
                        <a href="{{ url('validasi-kabid') }}" class="btn btn-outline-primary" type="button"><i class="fa fa-chevron-left"></i> KEMBALI </a>
                    </div>
                    <div class="pull-right">
                        <button type="button" id="btnReject" class="btn btn-danger" data-toggle="modal" data-target="#modal-cancel"><i class="fas fa-close"></i> Tidak Setuju</button>
                        <button type="button" id="btnApprove" class="btn btn-primary ml-2" data-toggle="modal" data-target="#modal-save"><i class="fas fa-check"></i> Setuju</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-save">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-success">
                        <h6 class="modal-title">
                            <span class="fas fa-check-double"></span>
                            &nbsp;PERSETUJUAN VALIDASI KABID
                        </h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda setuju dengan transaksi ini?</p>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tidak</button>
                        <button type="submit" class="btn btn-success btn-sm" id="buttonValidasi"><span class="fas fa-check"></span> YA</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="modal-cancel">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header bg-danger">
                        <h6 class="modal-title"><span class="fas fa-close"></span>
                            &nbsp;TOLAK PERSETUJUAN VALIDASI KABID</h6>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <p>Apakah anda tidak setuju dengan transasksi ini?</p>
                    </div>
                    <div class="modal-footer justify-content-between">
                        <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tidak</button>
                        <button type="submit" class="btn btn-success btn-sm" id="buttonValidasi"><span class="fas fa-check"></span> YA</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <!-- <a class="play-pause"></a>
    <ol class="indicator"></ol> -->
</div>

<div class="modal fade bd-example-modal-lg" id="modal-overlay" data-backdrop="static" data-keyboard="false"
     tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="col-12">
                <span class="spinner-grow text-muted"></span>
                <span class="spinner-grow text-primary"></span>
                <span class="spinner-grow text-success"></span>
                <span class="spinner-grow text-info"></span>
                <span class="spinner-grow text-warning"></span>
                <span class="spinner-grow text-danger"></span>
                <span class="spinner-grow text-secondary"></span>
                <span class="spinner-grow text-dark"></span>
            </div>
        </div>
    </div>
</div>
@endsection

<style>
    .bd-example-modal-lg .modal-dialog {
        display: table;
        position: relative;
        margin: 0 auto;
        top: calc(50% - 24px);
    }

    .bd-example-modal-lg .modal-dialog .modal-content {
        background-color: transparent;
        border: none;
        box-shadow: none;
    }

    #googleMap {
        height: 400px;
        width: 100%;
        border: 1px solid #ccc;
    }
</style>
@push('scriptsbawah')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly" defer></script>


<script type="text/javascript">
    $(document).ready(function () {
        $("#btnReject").click(function () {
            $("#s_id_status_kabid").val(2);
        })

        $("#btnApprove").click(function () {
            $("#s_id_status_kabid").val(1);
        })
    });

    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-Token': '{{ csrf_token() }}'}
        });
    });

    function initMap() {
        var latnya = <?= $dataspt->s_latitude ?>;
        var longnya = <?= $dataspt->s_longitude ?>;
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 13,
            center: {lat: latnya, lng: longnya},
        });
        marker = new google.maps.Marker({
            map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: {lat: latnya, lng: longnya},
        });

    }

    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    $('#buttonValidasi').click(function () {
        var myForm = document.getElementById('formtambah');
        myForm.onsubmit = function () {
            var allInputs = myForm.getElementsByTagName('input');
            var input, i;

            for (i = 0; input = allInputs[i]; i++) {
                if (input.getAttribute('name') && !input.value) {
                } else {
                    jQuery('#modal-overlay').modal('show');
                }
            }
        };
    });

</script>

<script src="{{asset('internet/js/jquery.blueimp-gallery.min.js')}}"></script>
@endpush
