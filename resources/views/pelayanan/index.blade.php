@extends('layouts.master')

@section('judulkiri')
<h1 class="m-0">Pelaynan</h1>
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-9 col-lg-9 col-sm-12">
                <div class="d-flex flex-wrap align-items-top justify-content-start mt-1">
                    @can('pelayananKeringanan')
                    <div class="card-product mb-4 mr-4">
                        <a href="{{ url('pelayanan-keringanan') }}" class="text-dark-75">
                            <div class="product-image">
                                <img src="{{ asset('dist/img/others/U.svg') }}">
                            </div>
                            <div class="info">
                                <div class="name">
                                    <p>Keringanan</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endcan

                    @can('pelayananPembatalan')
                    <div class="card-product mb-4 mr-4">
                        <a href="{{ url('pelayanan-pembatalan') }}" class="text-dark-75">
                            <div class="product-image">
                                <img src="{{ asset('dist/img/others/U.svg') }}">
                            </div>
                            <div class="info">
                                <div class="name">
                                    <p>Pembatalan</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endcan

                    @can('pelayananPenundaan')
                    <div class="card-product mb-4 mr-4">
                        <a href="{{ url('pelayanan-penundaan') }}" class="text-dark-75">
                            <div class="product-image">
                                <img src="{{ asset('dist/img/others/U.svg') }}">
                            </div>
                            <div class="info">
                                <div class="name">
                                    <p>Penundaan</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endcan

                    @can('pelayananAngsuran')
                    <div class="card-product mb-4 mr-4">
                        <a href="{{ url('pelayanan-angsuran') }}" class="text-dark-75">
                            <div class="product-image">
                                <img src="{{ asset('dist/img/others/U.svg') }}">
                            </div>
                            <div class="info">
                                <div class="name">
                                    <p>Angsuran</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endcan

                    @can('pelayananMutasi')
                    <div class="card-product mb-4 mr-4">
                        <a href="{{ url('pelayanan-mutasi') }}" class="text-dark-75">
                            <div class="product-image">
                                <img src="{{ asset('dist/img/others/U.svg') }}">
                            </div>
                            <div class="info">
                                <div class="name">
                                    <p>Mutasi</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endcan
                    
                </div>
            </div>
            <div class="col-12 col-md-3 col-lg-3 col-sm-12 d-flex flex-column mb-4">
                <div class="flex-grow-1 bg-primary p-4 rounded-lg"
                    style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url(dist/img/custom-6.svg); background-repeat: no-repeat;">

                    <h4 class="text-inverse-danger mt-2 font-weight-bolder">Pelaporan</h4>

                    <p class="text-inverse-danger my-6">
                        Pemilihan laporan <br />notaris.
                    </p>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

@endsection
@push('scripts')

<!-- /.content -->
@endpush