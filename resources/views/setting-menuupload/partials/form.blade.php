<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">UKURAN FILE</label>
    <div class="col-md-4">
        <input type="text" name="s_ukuranfile" class="form-control @error('s_ukuranfile') is-invalid @enderror" id="s_nippejabat" placeholder="TAHUN" value="{{ old('s_ukuranfile') ?? $menuupload->s_ukuranfile }}">
    </div>
    
</div>


<a href="/setting-menuupload" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>

@push('scripts')
<script type="text/javascript">
    

    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert").slideUp(500);
    });

</script>
@endpush
