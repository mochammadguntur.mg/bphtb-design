<table style="border-collapse: collapse" width="100%">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">No</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Periode</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Status Target</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Target Penerimaan (Rp.)</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Create At</th>
        </tr>
    </thead>
    <tbody>
        @foreach($target as $index => $row)
        {{ error_reporting(0) }}
        <tr>
            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $index + 1 }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->s_tahun_target }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->targetStatus->s_nama_status }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: right;">{{ number_format($row->s_nilai_target, 0, ",", ".") }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ date('d/m/Y', strtotime($row->created_at)) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>