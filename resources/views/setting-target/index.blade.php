@extends('layouts.master')

@section('judulkiri')
PENGATURAN TARGET
@endsection

@section('content')
<div class="card card-outline card-info shadow-sm">
    <div class="card-header">
        <h3 class="card-title">
            <a href="{{ url('setting-target/create') }}" class="btn btn-primary">
                <i class="fas fa-plus"></i> Tambah
            </a>
            <a href="{{ url('setting-target-status') }}" class="btn btn-primary">
                <i class="fas fa-plus"></i> Status Target
            </a>
        </h3>

        <div class="card-tools">
            <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                <i class="fas fa-file-excel"></i> Excel
            </a>
            <a href="javascript:void(0)" class="btn btn-danger" id="btnPDF">
                <i class="fas fa-file-pdf"></i> PDF
            </a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                <thead>
                    <tr>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No </th>
                        <th scope='col' style="background-color: #227dc7; color:white; width: 130px;" class="text-center">Periode</th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Status</th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Target Penerimaan</th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">CreateAt</th>
                        <th scope='col' style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi </th>
                    </tr>
                    <tr>
                        <th scope="filter"></th>
                        <th scope="filter"><input type="text" class="form-control form-control-sm" id="filter-periode"></th>
                        <th scope="filter"><select class="form-control form-control-sm" id="filter-target_status"></select></th>
                        <th scope="filter"></th>
                        <th scope="filter"><input type="text" class="form-control form-control-sm" id="filter-created_at" readonly></th>
                        <th scope="filter"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="7"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix pagination-footer">
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Target Penerimaan</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm masked-money" id="targetHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Periode</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="periodeHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Status Target</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="statusHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ('{{ session("success") }}') {
            toastr.success('{{ session("success") }}')
        }

        if ('{{ session("error") }}') {
            toastr.error('{{ session("error") }}')
        }

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-periode').datepicker({
            autoclose: true,
            format: "yyyy",
            viewMode: "years",
            minViewMode: "years"
        });
        comboStatusTarget();
    });

    var datatables = datagrid({
        url: 'setting-target/datagrid',
        table: "#datagrid-table",
        columns: [{
                class: ""
            },
            {
                class: ""
            },
            {
                class: "text-right"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "s_tahun_target"
            },
            {
                sortable: true,
                name: "s_id_target_status"
            },
            {
                sortable: true,
                name: "s_nilai_target"
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]

    });

    var formatter = new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
    });

    $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search();
    });

    $("#filter-periode, #filter-created_at, #filter-target_status").change(function() {
        search();
    });

    $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search();
    });

    function search() {
        datatables.setFilters({
            target_status: $("#filter-target_status").val(),
            periode: $("#filter-periode").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    function comboStatusTarget() {
        const select = document.getElementById('filter-target_status');
        let jenis_hak = JSON.parse('{!!json_encode($target_status) !!}');
        let option = '<option class="pl-5" value="">Pilih</option>';

        jenis_hak.forEach(el => {
            option += `<option value="${ el.s_id_target_status }">${ el.s_nama_status }</option>`
        });

        select.innerHTML = option;
    }

    function showDeleteDialog(a) {
        $.ajax({
            url: `setting-target/detail/${ a }`,
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            console.log(data);
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data.s_id_target_bphtb);
            $("#statusHapus").val(data.target_status.s_nama_status)
            $("#targetHapus").val(formatter.format(data.s_nilai_target));
            $("#periodeHapus").val(data.s_tahun_target);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();

        $.ajax({
            url: "setting-target/" + id,
            method: "DELETE",
            data: {
                _token: "{{ csrf_token() }}",
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            datatables.reload();
            toastr.success(data['success']);
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        var target_status = $("#filter-target_status").val();
        var periode = $("#filter-periode").val();
        var created_at = $("#filter-created_at").val();

        if (a.target.id == 'btnExcel') {
            window.open(`{{ route('setting-target.export_xls') }}?periode=${ periode }&created_at=${ created_at }&target_status=${ target_status}`);
        } else {
            window.open(`{{ route('setting-target.export_pdf') }}?periode=${ periode }&created_at=${ created_at }&target_status=${ target_status}`);
        }
    });
</script>
@endpush