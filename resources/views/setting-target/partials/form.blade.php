<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Status</label>
    <div class="col-md-4">
        <select name="s_id_target_status" id="s_id_target_status" class="form-control @error('s_id_target_status') is-invalid @enderror">
            <option value="">Pilih</option>
            @foreach ($target_status as $a)
            <option value="{{ $a['s_id_target_status'] }}" {{ (old('s_id_target_status') ?? $target->s_id_target_status) == $a['s_id_target_status'] ? 'selected' : '' }}> {{ $a['s_nama_status'] }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_id_target_status'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_id_target_status') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Priode</label> 
    <div class="col-md-4">
        <input type="text" class="form-control @error('s_tahun_target') is-invalid @enderror" name="s_tahun_target" id="s_tahun_target" value="{{ old('s_tahun_target') ?? $target->s_tahun_target }}" data-mask="0000" placeholder="yyyy">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_tahun_target'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_tahun_target') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Target BPHTB</label>
    <div class="col-md-4">
        <input type="text" name="s_nilai_target" class="form-control masked-money @error('s_nilai_target') is-invalid @enderror" id="s_nilai_target" value="{{ old('s_nilai_target') ?? $target->s_nilai_target }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_nilai_target'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_nilai_target') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Keterangan</label>
    <div class="col-md-4">
        <textarea class="form-control" name="s_keterangan" id="s_keterangan">{{ old('s_keterangan') ?? $target->s_keterangan }}</textarea>
    </div>
</div>
<a href="{{ route('setting-target') }}" class="btn btn-danger"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary float-right"><i class="{{ $icon }}"></i> {{ $sumbit }}</button>

@push('scripts')
<script type="text/javascript">

</script>
@endpush