@php
$html = '';
$html .= '
<html>
    <head>
        <title>SSPD BPHTB</title>
        <style>
            @page{margin:20px 30px 20px 30px;}
        </style>
    </head>
    <body>';
        for($x=1;$x<=6;$x++){
        if ($x == 1) { 
        $keterangan = 'Untuk Wajib Pajak';
        } elseif ($x == 2) {
        $keterangan = 'Untuk PPAT/Notaris sebagai arsip';
        } elseif ($x == 3) {
        $keterangan = 'Untuk Kepala Kantor Bidang Pertanahan';
        } elseif ($x == 4) {
        $keterangan = 'Untuk '.strtoupper($data_pemda->s_namasingkatinstansi).' dalam proses penelitian';
        } elseif ($x == 5) {
        $keterangan = 'Untuk Bank yang ditunjuk Bendahara Penerimaan';
        } elseif ($x == 6) {
        $keterangan = 'Untuk Bank yang ditunjuk Bendahara';
        }
        $html .= '
        <div>
        </div>
        <table style=" padding:0;border-spacing:0px; width: 100%px; border-bottom: 1px solid #000;border-top: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000; font-size: 12px;font-family:sans-serif">
            <tr>
                <td style="border-bottom: 1px solid #000;border-right: 1px solid #000; width: 180px;font-size:8pt">
            <center><img src="'.public_path().'/storage/' . $data_pemda->s_logo.'" style="width:70px;" /><br>
                PEMERINTAH ' . strtoupper($data_pemda->s_namakabkot) . '<br>
                ' . strtoupper($data_pemda->s_namainstansi) . '
            </center>
        </td>
        <td style="border-bottom: 1px solid #000;border-right: 1px solid #000; ">
            <table style="padding:0;border-spacing:0px; width: 100%;">
                <tr>
                    <td align="center" style="border-bottom: 1px solid #000;font-size:16px;font-weight:bold;">
                        SURAT SETORAN PAJAK DAERAH
                        <br/>
                        BEA PEROLEHAN HAK ATAS TANAH DAN BANGUNAN
                        <br/>
                        <strong style="font-size:20pt;margin-top:15px;margin-bottom:5px;">( SSPD - BPHTB )</strong>
                    </td>
                </tr>
                <tr>
                    <td align="center" style="font-size:14px;font-weight:bold;">
                        BERFUNGSI SEBAGAI SURAT PEMBERITAHUAN OBJEK PAJAK
                        <br/>
                        PAJAK BUMI DAN BANGUNAN (SPOP PBB)
                    </td>
                </tr>
            </table>
        </td>
        <td style="border-bottom: 1px solid #000; width: 190px;">
            <table>
                <tr>
                    <td style="text-align: center">
                        <strong>Lembar <span style="font-size:14pt;">'.$x.'</span></strong><br>
                        <span style="font-size:7pt;">'.$keterangan.'</span>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center">
                        '.str_replace('<?xml version="1.0" encoding="UTF-8"?>', '',$barcode).'
                    </td>
                </tr>
                <tr>
                    <td style="text-align: center; font-size: 9pt;">
                        KODE BAYAR/NTPD <strong>'.$dataspt->t_kodebayar_bphtb.'</strong>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border-bottom: 1px solid #000; padding: 4px; font-size: 9pt;">
            <strong><small>' . strtoupper($data_pemda->s_namainstansi) . ' ' . strtoupper($data_pemda->s_namakabkot) . '</strong>
            <br>
            <strong>PERHATIAN </strong>: Bacalah petunjuk pengisian pada halaman belakang lembar ini terlebih dahulu</small>
        </td>
    </tr>

    <tr>
        <td colspan="3" style="border-bottom: 1px solid #000;">
            <table width="1250px"  style="border-spacing: 0px;">
                <tr>
                    <td colspan="12" style="width: 20px; padding: 4px;">
                        A. Informasi Wajib Pajak
                    </td>
                </tr>
                <tr >
                    <td style="padding: 4px;width: 20px;">&nbsp;</td>
                    <td style="">
                        1. Nama Wajib Pajak
                    </td>
                    <td> : </td>
                    <td style="width: 160px;">
                        ' . strtoupper($dataspt->t_nama_pembeli) . '
                    </td>
                    <td colspan="8">&nbsp;</td>
                </tr>
                <tr >
                    <td style="padding: 4px;">&nbsp;</td>
                    <td style="">
                        2. NPWP
                    </td>
                    <td> : </td>
                    <td style="width: 160px;">
                        ' . $dataspt->t_npwp_pembeli . '
                    </td>
                    <td colspan="8">&nbsp;</td>
                </tr>
                <tr>
                    <td style="padding: 4px;">&nbsp;</td>
                    <td>
                        3. Alamat Wajib Pajak
                    </td>
                    <td> : </td>
                    <td colspan="9">
                        ' . strtoupper($dataspt->t_jalan_pembeli) . '
                    </td>

                </tr>
                <tr>
                    <td style="padding: 4px;">&nbsp;</td>
                    <td>
                        4. Desa / Kelurahan
                    </td>
                    <td> : </td>
                    <td>
                        ' . strtoupper($dataspt->t_namakelurahan_pembeli) . '
                    </td>
                    <td style="width: 10px;">&nbsp;</td>
                    <td style="width: 70px;">
                        5. RT/RW
                    </td>
                    <td style="width: 10px;"> : </td>
                    <td style="width: 70px;"> ' . strtoupper($dataspt->t_rt_pembeli) . ' / ' . strtoupper($dataspt->t_rw_pembeli) . ' </td>
                    <td style="width: 10px;">&nbsp;</td>
                    <td style="width: 100px;">
                        6. Kecamatan
                    </td>
                    <td style="width: 10px;"> : </td>
                    <td> ' . strtoupper($dataspt->t_namakecamatan_pembeli) . ' </td>
                </tr>

                <tr>
                    <td style="padding: 4px;">&nbsp;</td>
                    <td>
                        7. Kabupaten
                    </td>
                    <td> : </td>
                    <td>
                        ' . strtoupper($dataspt->t_kabkota_pembeli) . '
                    </td>
                    <td colspan="5">&nbsp;</td>
                    <td>
                        8. Kode Pos
                    </td>
                    <td> : </td>
                    <td> ' . $dataspt->t_kodepos_pembeli . '</td>

                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td colspan="3">
            <table style="border-spacing: 0px; width: 1250px;" >
                <tr>
                    <td colspan="8" style="width: 20px; padding: 4px;">
                        B. Informasi Objek Pajak
                    </td>
                </tr>
                <tr>
                    <td align="center" style="width: 20px; padding: 4px;"></td>
                    <td style="width: 230px">
                        1. Nomor Objek Pajak (NOP) PBB
                    </td>
                    <td style="width: 10px"> : </td>
                    <td colspan="5">
                        <table style="border-spacing: 0px;">
                            <tr>
                                ';

                                for ($i = 0; $i <= 1; $i++) {
                                $html .= '
                                <td style="width: 20px; border-bottom: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;">
                            <center>' . $dataspt->t_nop_sppt[$i] . '</center>
                    </td>';
                    }
                    $html .= '
                    <td style="width: 10px;">&nbsp;</td>';

                    for ($i = 3; $i <= 4; $i++) {
                    $html .= '
                    <td style="width: 20px; border-bottom: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;">
                <center>' . $dataspt->t_nop_sppt[$i] . '</center>
        </td>';
        }
        $html .= '
        <td style="width: 10px;">&nbsp;</td>';

        for ($i = 6; $i <= 8; $i++) {
        $html .= '
        <td style="width: 20px; border-bottom: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;">
    <center>' . $dataspt->t_nop_sppt[$i] . '</center>
</td>';
}
$html .= '
<td style="width: 10px;">&nbsp;</td>';

for ($i = 10; $i <= 12; $i++) {
$html .= '
<td style="width: 20px; border-bottom: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;">
<center>' . $dataspt->t_nop_sppt[$i] . '</center>
</td>';
}
$html .= '
<td style="width: 10px;">&nbsp;</td>';
for ($i = 14; $i <= 16; $i++) {
$html .= '
<td style="width: 20px; border-bottom: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;">
<center>' . $dataspt->t_nop_sppt[$i] . '</center>
</td>';
}
$html .= '
<td style="width: 10px;">&nbsp;</td>';
for ($i = 18; $i <= 21; $i++) {
$html .= '
<td style="width: 20px; border-bottom: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;">
<center>' . $dataspt->t_nop_sppt[$i] . '</center>
</td>';
}
$html .= '
<td style="width: 10px;">&nbsp;</td>
<td style="width: 20px; border-bottom: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;">
<center>' . $dataspt->t_nop_sppt[23] . '</center>
</td>
<td colspan="5">
</td>
</tr>

</table>
</td>
</tr>

<tr>
    <td style="padding: 4px;">&nbsp;</td>
    <td class="border_bawah">
        2. Letak Tanah dan/atau Bangunan
    </td>
    <td>:</td>
    <td>
        ' . strtoupper($dataspt->t_jalan_sppt) . '
    </td>
    <td colspan="4"> &nbsp; </td>
</tr>
<tr>
    <td style="padding: 4px;">&nbsp;</td>
    <td>
        3. Desa / Kelurahan
    </td>
    <td> : </td>
    <td> ' . $dataspt->t_kelurahan_sppt . ' </td>
    <td style="width: 10px;">&nbsp;</td>
    <td style="width: 100px;">
        4. RT/RW
    </td>
    <td style="width: 10px;">:</td>
    <td>' . strtoupper($dataspt->t_rt_sppt) . ' / ' . strtoupper($dataspt->t_rw_sppt) . '</td>
</tr>
<tr>
    <td style="padding: 4px;">&nbsp;</td>
    <td class="border_bawah">
        5. Kecamatan
    </td>
    <td>:</td>
    <td>
        ' . $dataspt->t_kecamatan_sppt . '
    </td>
    <td style="width: 10px;">&nbsp;</td>
    <td style="width: 80px;">
        6. Kabupaten
    </td>
    <td style="width: 10px;">:</td>
    <td>' . $dataspt->t_kabkota_sppt . '</td>
</tr>
<tr>
    <td style="padding: 4px;">&nbsp;</td>
    <td colspan="7">Penghitungan NJOP PBB</td>
</tr>
<tr>
    <td>&nbsp;</td>
    <td colspan="7">
        <table width="1270px" style="border-spacing: 0px; margin-right: -4px;">
            <tr >
                <td style="width: 150px; border-bottom: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;">
            <center>Uraian</center>
    </td>
    <td colspan="2" style="border-bottom: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;">
<center><b>Luas</b><br><span style="font-size:8px;">(Diisi luas tanah dan / atau bangunan yang haknya diperoleh)</span></center>
</td>
<td colspan="2" style="border-bottom: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;">
<center><b>NJOP PBB / m <sup>2</sup></b><br><span style="font-size:8px;">(Diisi berdasarkan SPPT PBB tahun terjadinya perolehan hak/Tahun '.$dataspt->t_periodespt.' )</span></center>
</td>
<td colspan="2" style="border-bottom: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;">
<center><b>Luas x NJOP PBB / m<sup>2</sup></center>
</td>
</tr>
<tr>
    <td style="padding: 4px;  border-bottom: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000;">
        Tanah (Bumi)
    </td>
    <td style="width: 20px; border-bottom: 1px solid #000;border-right: 1px solid #000;text-align: center;">
        7
    </td>
    <td style="width: 180px; border-bottom: 1px solid #000;border-right: 1px solid #000; text-align: right">

        ' . number_format($dataspt->t_luastanah, 0, ',', '.') . ' m<sup>2</sup> &nbsp;
    </td>
    <td style="width: 20px; border-bottom: 1px solid #000;border-right: 1px solid #000;  text-align: center;">
        9
    </td>
    <td style="width: 240px; border-bottom: 1px solid #000;border-right: 1px solid #000;  text-align: right">

        ' . number_format($dataspt->t_njoptanah, 0, ',', '.') . ' &nbsp;
    </td>
    <td style="width: 20px; border-bottom: 1px solid #000;border-right: 1px solid #000;  text-align: center;">
        11
    </td>
    <td style="width: 290px; border-bottom: 1px solid #000;border-right: 1px solid #000; text-align: right">

        ' . number_format($dataspt->t_totalnjoptanah, 0, ',', '.') . ' &nbsp;
    </td>
</tr>
<tr>
    <td style="padding: 4px; border-bottom: 1px solid #000;border-right: 1px solid #000;border-left: 1px solid #000; ">
        Bangunan
    </td>
    <td style="border-bottom: 1px solid #000;border-right: 1px solid #000; text-align: center;">
        8
    </td>
    <td style="border-bottom: 1px solid #000;border-right: 1px solid #000; text-align: right">

        ' . number_format($dataspt->t_luasbangunan, 0, ',', '.') . ' m<sup>2</sup> &nbsp;
    </td>
    <td style="border-bottom: 1px solid #000;border-right: 1px solid #000; text-align: center;">
        10
    </td>
    <td style="border-bottom: 1px solid #000;border-right: 1px solid #000; text-align: right">
        ' . number_format($dataspt->t_njopbangunan, 0, ',', '.') . ' &nbsp;
    </td>
    <td style="border-bottom: 1px solid #000;border-right: 1px solid #000;  text-align: center;">
        12
    </td>
    <td style="border-bottom: 1px solid #000;border-right: 1px solid #000; text-align: right">
        ' . number_format($dataspt->t_totalnjopbangunan, 0, ',', '.') . ' &nbsp;
    </td>
</tr>
<tr>
    <td colspan="5" style="padding: 4px; border-right: 1px solid #000;" align="right">
        NJOP PBB : &nbsp; &nbsp; &nbsp;
    </td>
    <td style="border-bottom: 1px solid #000;border-right: 1px solid #000; text-align: center;">
        13
    </td>
    <td style="border-bottom: 1px solid #000;border-right: 1px solid #000; text-align: right">
        ' . number_format($dataspt->t_grandtotalnjop, 0, ',', '.') . ' &nbsp;
    </td>
</tr>
';

$html .= '
<tr>
    <td colspan="7" style="padding: 2px;">
        &nbsp;
    </td>
</tr>
<tr>
    <td colspan="5" align="left">
        14. Harga Transaksi / Nilai Pasar :
    </td>
    <td style="border-bottom: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000; " align="left"> Rp.</td>
    <td style="border-bottom: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000; " align="right">
        ' . number_format($dataspt->t_nilaitransaksispt, 0, ',', '.') . ' &nbsp;
    </td>
</tr>';

$html .= '
<tr>
    <td colspan="5" align="left">
        15. Jenis perolehan hak atas Tanah dan/atau Bangunan :
        <table style="margin-top:-20px; margin-left: 450px;">
            <tr>
                ';

                $html .= '
                <td style="padding: 4px; width: 20px; text-align: center; border-bottom: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;border-left: 1px solid #000;">
                    ' . substr(str_pad($dataspt->t_idjenistransaksi, 2, '0', STR_PAD_LEFT), 0, 1) . '
                </td>

                <td style="width: 20px; text-align: center; border-bottom: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000;border-left: 1px solid #000;" >
                    ' . substr(str_pad($dataspt->t_idjenistransaksi, 2, '0', STR_PAD_LEFT), 1, 1) . '
                </td>
                <td align="right" style="width: 100px;">
                    &nbsp;&nbsp;&nbsp;
                </td>

            </tr>
        </table>
    </td>';

    if (($dataspt->t_tarif_pembagian_aphb_kali == null) || ($dataspt->t_tarif_pembagian_aphb_kali == '') || ($dataspt->t_tarif_pembagian_aphb_kali == 0) || ($dataspt->t_tarif_pembagian_aphb_bagi == null) || ($dataspt->t_tarif_pembagian_aphb_bagi == '') || ($dataspt->t_tarif_pembagian_aphb_bagi == 0)) {
    $html .= '<td colspan="4" align="right">&nbsp;</td>';
    }else{


    $html .= '


    <td style="border-bottom: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000; " align="left"> Rp.</td>

    <td style="border-bottom: 1px solid #000;border-right: 1px solid #000;border-top: 1px solid #000; " align="right">

        ' . number_format($dataspt->t_npop_bphtb, 0, ',', '.') . ' &nbsp;
    </td>';
    }
    $html .= '
</tr>
<tr>
    <td colspan="7" class="font_delapan">
        16. Nomor sertifikat : ' . $dataspt->t_nosertifikathaktanah . '
    </td>
</tr>
</table>
</td>
</tr>
</table>
</td>
</tr>
<tr>
    <td colspan="3" style="border-bottom: 1px solid #000;border-top: 1px solid #000;">
        <table style="border-spacing: 0px; width: 100%;">
            <tr>
                <td colspan="6" style="padding: 4px; border-bottom: 1px solid #000;">&nbsp;&nbsp;C. PENGHITUNGAN BPHTB (Hanya diisi berdasarkan penghitungan Wajib Pajak)</td>
            </tr>
            <tr>
                <td style="padding: 4px; border-bottom: 1px solid #000; width: 20px;">&nbsp;</td>
                <td style="border-bottom: 1px solid #000; width: 460px;">
                    1. Nilai Perolehan Objek Pajak (NPOP) <span style="font-size:10px"><i>memperhatikan nilai pada B.13 dan B.14 dan C </i></span>
                </td>
                <td style="border-bottom: 1px solid #000;">&nbsp;</td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;width: 20px; text-align: center;">
                    1
                </td>
                <td style="border-bottom: 1px solid #000; width: 10px;">
                    Rp.
                </td>
                <td style="border-bottom: 1px solid #000; text-align: right;">
                    ' . number_format($dataspt->t_npop_bphtb, 0, ',', '.') . ' &nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding: 4px; border-bottom: 1px solid #000; ">&nbsp;</td>
                <td style="border-bottom: 1px solid #000;" colspan="2">
                    2. Nilai Perolehan Objek Pajak Tidak Kena Pajak (NPOPTKP) memperhatikan nilai pada C
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;width: 20px; text-align: center;">
                    2
                </td>
                <td style="border-bottom: 1px solid #000; ">
                    Rp.
                </td>
                <td style="border-bottom: 1px solid #000; text-align: right;">
                    ' . number_format($dataspt->t_npoptkp_bphtb, 0, ',', '.') . ' &nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding: 4px; border-bottom: 1px solid #000; ">&nbsp;</td>
                <td style="border-bottom: 1px solid #000;">
                    3. Nilai Perolehan Objek Pajak Kena Pajak (NPOPKP)
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;width: 100px; text-align: center;">
                    angka 1-angka 2
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;width: 20px; text-align: center;">
                    3
                </td>
                <td style="border-bottom: 1px solid #000;">
                    Rp.
                </td>
                <td style="border-bottom: 1px solid #000; text-align: right;">
                    ' . number_format($dataspt->t_npopkp_bphtb, 0, ',', '.') . ' &nbsp;
                </td>
            </tr>
            <tr>
                <td style=" padding: 4px; border-bottom: 1px solid #000; ">&nbsp;</td>
                <td style="border-bottom: 1px solid #000;">
                    4. Bea Perolehan Hak atas Tanah dan Bangunan yang terutang
                </td>
                <td style="border-bottom: 1px solid #000; border-left: 1px solid #000;width: 110px; text-align: center;">

                    ' . $dataspt->t_persenbphtb . '% x angka 3

                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;text-align: center;">
                    4
                </td>
                <td style="border-bottom: 1px solid #000; ">
                    Rp.
                </td>
                <td style="border-bottom: 1px solid #000; text-align: right;">
                    ' . number_format($dataspt->t_nilai_bphtb_fix, 0, ',', '.') . ' &nbsp;
                </td>
            </tr>
            <tr>
                <td style="padding: 4px; border-bottom: 1px solid #000; ">&nbsp;</td>
                <td style="border-bottom: 1px solid #000; ">
                    5. Pajak Terhutang BPHTB
                </td>
                <td style="border-bottom: 1px solid #000;border-LEFT: 1px solid #000;width: 80px; text-align: center;">
                    &nbsp;
                </td>
                <td style="border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000; text-align: center;">
                    6
                </td>
                <td style="border-bottom: 1px solid #000; ">
                    Rp.
                </td>
                <td style="border-bottom: 1px solid #000; text-align: right;">
                    ' . number_format($dataspt->t_nilai_bphtb_fix, 0, ',', '.') . ' &nbsp;
                </td>
            </tr>';
            //}
            $html .= '
        </table>
    </td>
</tr>
<tr>
    <td colspan="3" style="border-bottom: 1px solid #000; ">
        <table width="1250px;" style="border-spacing: 5px">
            <tr>
                <td colspan="3" >
                    D. Jumlah Setoran berdasarkan : (Beri tanda silang "X" pada kotak yang sesuai)
                </td>
            </tr>
            <tr>
                <td style="width: 30px; height: 5px; margin-left: 10px; border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;">
                    &nbsp;&nbsp;V
                </td>
                <td colspan="2">
                    a. Penghitungan Wajib Pajak
                </td>
            </tr>
            <tr>
                <td style="width: 5px; height: 5px; margin-left: 10px; border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;text-align:center;" >
                    &nbsp;
                </td>
                <td style="width: 400px;">
                    b. STPD BPHTB / SKPDKB KB / SKPDKB KBT *)
                </td>
                <td>
                    <table width="100%" style="border-spacing: 0;">
                        <tr>
                            <td style="border-bottom: 1px solid #000; width: 50%;">
                                Nomor...........................
                            </td>
                            <td style="border-bottom: 1px solid #000;">
                                Tanggal ...............................
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="width: 5px; height: 5px; margin-left: 10px; border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;text-align:center;" >

                </td>
                <td align="left" style="vertical-align: top;">
                    c. Pengurangan dihitung sendiri menjadi :

                    <table align="right" style="margin-top:-20px; margin-right: 50px;">
                        <tr>
                            <td style="width: 20px; height: 20px; text-align: center; border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;" >

                            </td>
                            <td style="width: 20px; height: 20px; text-align: center; border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;" >

                            </td>
                        </tr>
                    </table>

                </td>
                <td>
                    % berdasarkan Perundang-undangan berlaku
                </td>
            </tr>
            <tr>
                <td style="width: 5px; height: 5px; margin-left: 10px; border-bottom: 1px solid #000; border-right: 1px solid #000;border-left: 1px solid #000;border-top: 1px solid #000;text-align:center;" >

                </td>
                <td colspan="2">
                    ..............................................................................

                </td>
            </tr>

        </table>
    </td>
</tr>
<tr>
    <td colspan="3" style="border-spacing: 10px; height: 70px; border-bottom: 1px solid #000; ">

        <table align="left" >
            <tr><td width="250" style="text-align: center;" align="center">JUMLAH YANG DISETOR (dengan angka): </td>
                <td>
                    &nbsp;&nbsp;&nbsp;
                </td>
                <td style="text-align: left;">
                    (dengan huruf) : '.$terbilang_pajak.' Rupiah

                </td>
            </tr>
            <tr>

                <td style="height: 20px; text-align: center; border-bottom: 1px solid #000; border-right: 1px solid #000; border-top: 1px solid #000; border-left: 1px solid #000;font-weight:bold;font-size:10pt" align="center" width="120">
                    Rp. ' . number_format($dataspt->t_nilai_bphtb_fix, 0, ',', '.') . '
                </td>
                <td> &nbsp;&nbsp;&nbsp;</td>
                <td>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td style="text-align: center;"> <span style="font-size: 10px;">(berdasarkan perhitungan D.4 dan pilihan di E)</span></td>
                <td>
                    &nbsp;&nbsp;&nbsp;
                </td>
                <td>
                    &nbsp;&nbsp;&nbsp;
                </td>
            </tr>
        </table>

    </td>

</tr>
<tr>
    <td colspan="3" style="border-spacing: 10px; height: 70px; border-bottom: 1px solid #000; ">
        <table style="padding:0;border-spacing:0px; width: 1250px; font-size: 11px; margin-bottom: -2px;">
            <tr>
                <td style="border-right: 1px solid #000; text-align: center; width: 290px; vertical-align: top;">
                    ' . ucfirst(strtolower($data_pemda->s_namaibukotakabkot)) . ', ' . date('d-m-Y', strtotime($dataspt->t_tgldaftar_spt)) . '<br>
                    WAJIB PAJAK / PENYETOR
                    <br><br><br><br><br><br><br><br>
                    ' . strtoupper($dataspt->t_nama_pembeli) . '
                </td>
                <td style="border-right: 1px solid #000; text-align: center; width: 290px; vertical-align: top;">
                    MENGETAHUI :
                    <br>
                    PPAT / NOTARIS / PEJABAT LELANG<br>PEJABAT PENDAFTARAN
                    <br><br><br><br><br><br><br>
                    ' . strtoupper($dataspt->s_namanotaris) . '
                </td>
                <td style="border-right: 1px solid #000; text-align: center; width: 290px; vertical-align: top;">
                    DITERIMA OLEH :
                    <br>
                    TEMPAT PEMBAYARAN BPHTB
                    <br>
                    ' . (($dataspt->t_tglpembayaran_pokok) ? 'Tanggal '.date('d-m-Y', strtotime($dataspt->t_tglpembayaran_pokok)) : '' ) . ' <b> </b>
                    <br><br><br><br><br><br>


                </td>';

                
                
                if($cek_menukaban->s_id_statusmenukaban == 1){
                    $ttd_kaban = '<br><br><br><br><br><br><br>
                    '. $dataspt->namapejabat_kaban .'<br>
                    '. $dataspt->nippejabat_kaban .'';
                }else{
                    $ttd_kaban = '<br><br><br><br><br><br><br>
                    '. $dataspt->namapejabat_kabid .'<br>
                    '. $dataspt->nippejabat_kabid .'';
                }
                
                $html .= '
                <td style=" text-align: center; width: 290px; vertical-align: top;">
                    TELAH DIVERIFIKASI :
                    <b> </b>
                    <br>
                    KEPALA ' . strtoupper($data_pemda->s_namainstansi) . '
                    <br>
                    ' . $t_tglvalidasikaban . '
                    <!--<br><br><br><br><br><br>-->
                    '.$ttd_kaban.'
                    
                </td>
            </tr>
            <tr>
                <td style="border-right: 1px solid #000; border-bottom: 1px solid #000;  font-size: 10px; text-align: center; vertical-align: top;">
                    Nama Lengkap dan tanda tangan
                </td>
                <td style="border-right: 1px solid #000; border-bottom: 1px solid #000;  font-size: 10px; text-align: center; vertical-align: top;">
                    Nama Lengkap, Stempel dan tanda tangan
                </td>
                <td style="border-right: 1px solid #000; border-bottom: 1px solid #000;  font-size: 10px; text-align: center; vertical-align: top;">
                    Nama Lengkap, Stempel dan tanda tangan
                </td>
                <td style=" border-bottom: 1px solid #000;  font-size: 10px; text-align: center; vertical-align: top;">
                    Nama Lengkap, Stempel dan tanda tangan
                </td>
            </tr>
        </table>
    </td>
</tr>';

$lebarkolom_nodok = 'padding: 4px; width: 30px;';
$html .= '
<tr>
    <td colspan="3" style="border-spacing: 0px; border-bottom: 1px solid #000; ">
        <table style="padding:0;border-spacing:0px; width: 1250px; font-size: 12px;">
            <tr>
                <td style="text-align: center; border-right: 1px solid #000; width: 125px; ">
                    Hanya diisi oleh <br>petugas ' . $data_pemda->s_namasingkatinstansi . '
                </td>
                <td>
                    <table style="border-spacing: 0; font-size: 12px;">
                        <tr>
                            <td style="vertical-align: middle">
                                Nomor Dokumen
                            </td>
                            <td style="width: 5px; vertical-align: middle">
                                : &nbsp;
                            </td>
                            <td style="height: 10px; text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '">
                                ';
                                if (empty($dataspt->t_kohirspt)) {
                                $notgl = str_pad('', 8, ' ', STR_PAD_LEFT);
                                $nosspd = str_pad('', 7, ' ', STR_PAD_LEFT);
                                } else {

                                $tgl = explode('-', date('Y-m-d', strtotime($dataspt->t_tgldaftar_spt)));
                                $tahun = $tgl[0];
                                $bulan = $tgl[1];
                                $hari = $tgl[2];

                                $hasiltgl = $hari . $bulan . $tahun;

                                $notgl = str_pad($hasiltgl, 8, '0', STR_PAD_LEFT);

                                $nosspd = str_pad($dataspt->t_kohirspt, 7, '0', STR_PAD_LEFT);
                                }
                                $html .= '
                                ' . $notgl[0] . '
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '">
                                ' . $notgl[1] . '
                            </td>
                            <td width="5">&nbsp;
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '" >
                                ' . $notgl[2] . '
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '" >
                                ' . $notgl[3] . '
                            </td>
                            <td width="5">
                                &nbsp;
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000;  ' . $lebarkolom_nodok . '" >
                                ' . $notgl[4] . '
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '" >
                                ' . $notgl[5] . '
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '" >
                                ' . $notgl[6] . '
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '" >
                                ' . $notgl[7] . '
                            </td>
                            <td width="12">&nbsp;
                            </td>

                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '" >
                                ' . $nosspd[0] . '
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000;  ' . $lebarkolom_nodok . '" >
                                ' . $nosspd[1] . '
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '" >
                                ' . $nosspd[2] . '
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '">
                                ' . $nosspd[3] . '
                            </td>
                            <td width="12">&nbsp;
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '">
                                ' . $nosspd[4] . '
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '">
                                ' . $nosspd[5] . '
                            </td>
                            <td style="text-align: center; border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '">
                                ' . $nosspd[6] . '
                            </td>
                            <td colspan="5">&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td colspan="22">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>
                                NOP PBB Baru
                            </td>
                            <td width="5">
                                :
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000;  ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000;  ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td>&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000;  ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-left: 1px solid #000; border-right: 1px solid #000; ">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000;  border-right: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="width: 5px;"></td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000;  ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-left: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>
                            <td style="width: 5px;">&nbsp;
                            </td>
                            <td style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-left: 1px solid #000; border-right: 1px solid #000; ' . $lebarkolom_nodok . '">&nbsp;
                            </td>

                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </td>
</tr>

</table>
        <small style="font-size: 6pt;background: black; color: white;">Print by my-bphtb '.$data_pemda->s_namakabkot.'</small>
';
}
$html .= '</body>

</html>
';

@endphp

{!! $html !!}

<?php  //'.str_replace('<?xml version="1.0" encoding="UTF-8"? >', '',$qrcode_validasi).' ?>