@extends('layouts.master')

@section('judulkiri')
PENDAFTARAN
@endsection
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="pendaftaran/formtambah" class="btn btn-sm btn-primary"><i class="fa fa-plus-square"></i> TAMBAH</a>
                </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">No</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Perintah</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">No. Daftar</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Tanggal Daftar</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Tahun</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">PPAT/PPATS</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Jenis Transaksi</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Nama WP</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">NOP</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Nilai BPHTB<br>(Rp.)</th>
                                <th colspan="4" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Status</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Kode Bayar</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">NTPD</th>
                                <th rowspan="2" style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Ketetapan</th>
                            </tr>
                            <tr>
                                <th style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Pengajuan</th>
                                <th style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Validasi Berkas</th>
                                <th style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Validasi Kabid</th>
                                <th style="background-color: #227dc7; color:white; vertical-align: middle;" class="text-center">Pembayaran</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-t_kohirspt"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-t_tgldaftar_spt"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-t_periodespt"></th>
                                <th>
                                    <select class="form-control form-control-sm" id="filter-t_idnotaris_spt">
                                        <option value="">-- Semua --</option>
                                        <?php $data_notaris = (new \ComboHelper())->getDataNotaris(); ?>
                                        @foreach ($data_notaris as $dn)
                                        <option value="{{ $dn->s_idnotaris }}"> {{ $dn->s_namanotaris }}</option>
                                        @endforeach
                                    </select>
                                </th>
                                <th>
                                    <select class="form-control form-control-sm" id="filter-t_idjenistransaksi">
                                        <option value="">-- Semua --</option>
                                        <?php $data_jenistransaksi = (new \ComboHelper())->getDataJenisTransaksi(); ?>
                                        @foreach ($data_jenistransaksi as $djt)
                                        <option value="{{ $djt->s_idjenistransaksi }}"> {{ $djt->s_namajenistransaksi }}</option>
                                        @endforeach
                                    </select>
                                </th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-t_nama_pembeli"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-t_nop_sppt"></th>
                                <th></th>
                                <th>
                                    <select class="form-control form-control-sm" id="filter-t_idpersetujuan_bphtb">
                                        <option value="">-- Semua --</option>
                                        <option value="1">SETUJU</option>
                                        <option value="2">BELUM</option>
                                    </select>
                                </th>
                                <th>
                                    <select class="form-control form-control-sm" id="filter-s_id_status_berkas">
                                        <option value="">-- Semua --</option>
                                        <?php $data_statusberkas = (new \ComboHelper())->getDataStatusBerkas(); ?>
                                        @foreach ($data_statusberkas as $dsb)
                                        <option value="{{ $dsb->s_id_status_berkas }}"> {{ $dsb->s_nama_status_berkas }}</option>
                                        @endforeach
                                        <option value="3">Belum</option>
                                    </select>
                                </th>
                                <th>
                                    <select class="form-control form-control-sm" id="filter-s_id_status_kabid">
                                        <option value="">-- Semua --</option>
                                        <?php $data_statuskabid = (new \ComboHelper())->getDataStatusKabid(); ?>
                                        @foreach ($data_statuskabid as $dsk)
                                        <option value="{{ $dsk->s_id_status_kabid }}"> {{ $dsk->s_nama_status_kabid }}</option>
                                        @endforeach
                                        <option value="3">Belum</option>
                                    </select>
                                </th>
                                <th>
                                    <select class="form-control form-control-sm" id="filter-t_id_pembayaran">
                                        <option value="">-- Semua --</option>
                                        <?php $data_statusbayar = (new \ComboHelper())->getDataStatusBayar(); ?>
                                        @foreach ($data_statusbayar as $dsbyr)
                                        <option value="{{ $dsbyr->s_id_status_bayar }}"> {{ $dsbyr->s_nama_status_bayar }}</option>
                                        @endforeach
                                    </select>
                                </th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-t_kodebayar_bphtb"></th>
                                <th></th>
                                <th>
                                    <select class="form-control form-control-sm" id="filter-t_idjenisketetapan">
                                        <option value="">-- Semua --</option>
                                        <?php $data_jenisketetapan = (new \ComboHelper())->cekdata_jenis_ketetapan(); ?>
                                        @foreach ($data_jenisketetapan as $jk)
                                        <option value="{{ $jk->s_idjenisketetapan }}"> {{ $jk->s_namasingkatjenisketetapan }}</option>
                                        @endforeach
                                    </select>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="17"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                    
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-batal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;BATALKAN DATA PENGAJUAN SSPD-BPHTB ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin membatalkan data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">No Daftar</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idBatal" readonly>
                        <input type="text" class="form-control form-control-sm" id="kohirBatal" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Tgl Daftar</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="tgldaftarBatal" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Nama Wajib Pajak</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namawpBatal" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnBatal"><span class="fas fa-trash"></span>Batalkan</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS PENGAJUAN BPHTB DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus permanen data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">No Daftar</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="kohirHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Tgl Daftar</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="tgldaftarHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Nama Wajib Pajak</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namawpHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>Hapus</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagridmodif.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif

        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif
        
        $('#filter-t_tgldaftar_spt').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });
        $('#filter-t_tgldaftar_spt').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });
        $('#filter-t_tgldaftar_spt').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
            search();
        });
        
        
        
        
    });
    var datatables = datagrid({
        url: 'pendaftaran/datagrid',
        table: "#datagrid-table",
        columns: [{
                class: ""
            }, {
                class: "text-center"
            }, {
                class: "text-center"
            }, {
                class: ""
            }, {
                class: ""
            }, {
                class: ""
            }, {
                class: ""
            }, {
                class: ""
            }, {
                class: "text-right"
            }, {
                class: ""
            }, {
                class: ""
            }, {
                class: ""
            }, {
                class: ""
            }, {
                class: ""
            },
            {
                class: ""
            },
            {
                class: ""
            },
            {
                class: ""
            }

        ],
        orders: [{
                sortable: false
            }, {
                sortable: false
            }, {
                sortable: true,
                name: 't_kohirspt'
            }, {
                sortable: true,
                name: 't_tgldaftar_spt'
            }, {
                sortable: true,
                name: 't_periodespt'
            }, {
                sortable: true,
                name: 't_idnotaris_spt'
            }, {
                sortable: true,
                name: 't_idjenistransaksi'
            }, {
                sortable: true,
                name: 't_nama_pembeli'
            }, {
                sortable: true,
                name: 't_nop_sppt'
            }, {
                sortable: false
            }, {
                sortable: false
            }, {
                sortable: false
            }, {
                sortable: false
            }, {
                sortable: false
            }, {
                sortable: true,
                name: 't_kodebayar_bphtb'
            }, {
                sortable: false
            }, {
                sortable: false
            }, {
                sortable: true,
                name: 't_idjenisketetapan'
            },  ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]
        
        
         

    });
    
    
     

    $("#filter-t_kohirspt, #filter-t_tgldaftar_spt, #filter-t_periodespt, " +
            "#filter-t_nama_pembeli, #filter-t_nop_sppt, #filter-t_kodebayar_bphtb").keyup(function () {
        search();
    });

    $("#filter-t_idnotaris_spt, #filter-t_idjenistransaksi, #filter-t_idpersetujuan_bphtb, #filter-s_id_status_berkas, #filter-s_id_status_kabid, #filter-t_id_pembayaran, #filter-t_idjenisketetapan").change(function () {
        search();
    });

    function search() {
        datatables.setFilters({
            t_kohirspt: $("#filter-t_kohirspt").val(),
            t_tgldaftar_spt: $("#filter-t_tgldaftar_spt").val(),
            t_periodespt: $("#filter-t_periodespt").val(),
            t_idnotaris_spt: $("#filter-t_idnotaris_spt").val(),
            t_idjenistransaksi: $("#filter-t_idjenistransaksi").val(),
            t_nama_pembeli: $("#filter-t_nama_pembeli").val(),
            t_nop_sppt: $("#filter-t_nop_sppt").val(),
            t_idpersetujuan_bphtb: $("#filter-t_idpersetujuan_bphtb").val(),
            s_id_status_berkas: $("#filter-s_id_status_berkas").val(),
            s_id_status_kabid: $("#filter-s_id_status_kabid").val(),
            t_id_pembayaran: $("#filter-t_id_pembayaran").val(),
            t_kodebayar_bphtb: $("#filter-t_kodebayar_bphtb").val(),
            t_idjenisketetapan : $("#filter-t_idjenisketetapan").val()
        });
        datatables.reload();
    }
    search();

    function showBatalDialog(a) {
        $.ajax({
            url: 'pendaftaran/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function (data) {
            $("#btnBatal").removeAttr("disabled");
            $("#idBatal").val(data[0].t_idspt);
            $("#kohirBatal").val(data[0].t_kohirspt);
            $("#tgldaftarBatal").val(data[0].t_tgldaftar_spt);
            $("#namawpBatal").val(data[0].t_nama_pembeli);
        });
        $("#modal-batal").modal('show');
    }

    $("#btnBatal").click(function () {
        var id = $("#idBatal").val();
        $.ajax({
            url: 'pendaftaran/batal/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function (data) {
            $("#modal-batal").modal("hide");
            toastr.success('Data Berhasil dibatalkan!')
            datatables.reload();
        });
    });
    
    function showDeleteDialog(a) {
        $.ajax({
            url: 'pendaftaran/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function (data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].t_idspt);
            $("#kohirHapus").val(data[0].t_kohirspt);
            $("#tgldaftarHapus").val(data[0].t_tgldaftar_spt);
            $("#namawpHapus").val(data[0].t_nama_pembeli);
        });
        $("#modal-delete").modal('show');
    }
    
    $("#btnHapus").click(function () {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'pendaftaran/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function (data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Berhasil dihapus secara permanen!')
            datatables.reload();
        });
    });
</script>
@endpush