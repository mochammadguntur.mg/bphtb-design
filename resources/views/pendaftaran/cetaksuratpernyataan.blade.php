<table style="text-align: justify;font-size: 11pt;border-collapse: collapse; width: 100%;" >
  <tr>
    <td style=" text-align: center" colspan="3"><b><u>SURAT PERNYATAAN</u></b></td>
  </tr>
   <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3">
      Yang Bertanda Tangan dibawah ini :
    </td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td style=" width: 10px;">1.</td>
    <td style="width: 5px;"></td>
    <td><u>Pembeli</u></td>
  </tr>
  <tr>
      <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
        <table>
            <tr>
                <td>Nama</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_nama_pembeli?></td>
            </tr>
            <tr>
                <td >Tempat/Tanggal Lahir</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"></td>
            </tr>
            <tr>
                <td>Pekerjaan</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"></td>
            </tr>
            <?php if($dataspt->t_idkec_pembeli != 15) {?>
            <tr>
                <td>Alamat Sesuai Identitas (KTP)</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_jalan_pembeli.' Rt '.$dataspt->t_rt_pembeli.' Rw '.$dataspt->t_rw_pembeli.' Kel. '.$dataspt->t_namakelurahan_pembeli?></td>
            </tr>
            <tr>
              <td ></td>
              <td style=" width: 10px;"></td>
              <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= 'Kec. '.$dataspt->t_namakecamatan_pembeli.' Kab. '.$dataspt->t_kabkota_pembeli?></td>
            </tr>
            <?php }
            else {?>
             <tr>
              <td >Alamat Sesuai Identitas (KTP)</td>
              <td style=" width: 10px;">:</td>
              <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_jalan_pembeli.' Rt '.$dataspt->t_rt_pembeli.' Rw '.$dataspt->t_rw_pembeli.' Kel. '.$dataspt->t_namakelurahan_pembeli?></td>
            </tr>
            <tr>
              <td ></td>
              <td style=" width: 10px;"></td>
              <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= 'Kec. '.$dataspt->t_namakecamatan_pembeli.' Kab. '.$dataspt->t_kabkota_pembeli?></td>
            </tr>
            <?php }?>
            <tr>
              <td >No. Telepon/HP</td>
              <td style=" width: 10px;">:</td>
              <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_nohp_pembeli?></td>
            </tr>
        </table>
    </td>
  </tr>
  <tr>
    <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td style=" width: 10px;">2.</td>
    <td style="width: 5px;"></td>
    <td><u>Penjual</u></td>
  </tr>
  <tr>
      <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td></td>
    <td></td>
    <td>
        <table>
            <tr>
                <td >Nama</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_nama_penjual?></td>
              </tr>
              <tr>
                <td >Tempat/Tanggal Lahir</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"></td>
              </tr>
              <tr>
                <td >Pekerjaan</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"></td>
              </tr>
              <tr>
                <td >Alamat Sesuai Identitas(KTP)</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_jalan_penjual.'Rt '.$dataspt->t_rt_penjual.' Rw '.$dataspt->t_rw_penjual.' Kel. '.$dataspt->t_namakel_penjual?></td>
              </tr>
              <tr>
                <td ></td>
                <td style=" width: 10px;"></td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= 'Kec. '.$dataspt->t_namakec_penjual.' Kab. '.$dataspt->t_kabkota_penjual?></td>
              </tr>
              <tr>
                <td>No. Telepon/HP</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?=$dataspt->t_nohp_penjual?></td>
              </tr>
        </table>
    </td>
  </tr>  
  <tr>
      <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
      <td colspan="3"><p style=" word-wrap: break-word">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dengan ini menyatakan dengan sesunggunya serta sanggup diangkat sumpah halnya benar saya telah melakukan, 
        menerima, jual-beli, tukar menukar, pemisahan hak, perlaihan wasiat/penggabungan usaha/peleburan/pemekaran usaha/pemberian hak baru/waris/hibah/hibah wasiat/hadiah penunjukan pembeli dalam lelang*) atas sebidang tanah bangunan/tanah berikut bangunan*) yang diuraikan dalam :</p></td>
  </tr>
  <tr>
      <td></td>  
      <td colspan="2">
          <table>
              <tr>
                <td style=" width: 10px;">1.</td>
                <td style=" width: 220px;">Nomor Objek Pajak (NOP-PBB)</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_nop_sppt?></td>
              </tr>
              <tr>
                <td style=" width: 10px;">2.</td>
                <td >No. Sertifikat (HM/HGB/HGU)</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_nosertifikathaktanah?></td>
              </tr>
              <tr>
                <td style=" width: 10px;">3.</td>
                <td >Terletak dijalan</td>
                <td style=" width: 10px;">:</td>
                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_jalan_sppt?></td>
              </tr>
              <tr>
                  <td></td>
                  <td></td>
                  <td colspan="2">
                      <table>
                          <tr>
                            <td >Nama Pemilik Asli</td>
                            <td style=" width: 10px;">:</td>
                            <td colspan="5" style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_nama_sppt?></td>
                          </tr>
                          <tr>
                            <td style="width: 130px;">Blok</td>
                            <td style=" width: 10px;">:</td>
                            <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= substr($dataspt->t_nop_sppt,14,3)?></td>
                            <td style=" width: 50px;">&nbsp;</td>
                            <td >No</td>
                            <td style=" width: 10px;">:</td>
                            <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= substr($dataspt->t_nop_sppt,18,4)?></td>
                            
                          </tr>
                          <tr>
                              <td >RT/RW</td>
                                <td style=" width: 10px;">:</td>
                                <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_rt_sppt.'/'.$dataspt->t_rw_sppt?></td>
                                <td colspan="4"></td>
                          </tr>
                          <tr>
                            <td >Kel</td>
                            <td style=" width: 10px;">:</td>
                            <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_kelurahan_sppt?></td>
                            <td style=" width: 50px;">&nbsp;</td>
                            <td >Kec</td>
                            <td style=" width: 10px;">:</td>
                            <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= $dataspt->t_kecamatan_sppt?></td>
                          </tr>
                          
                          <tr>
                            <td >Luas Tanah</td>
                            <td style=" width: 10px;">:</td>
                            <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= number_format($dataspt->t_luastanah, 0, ',', '.') ?> m<sup>2</sup></td>
                            <td style=" width: 50px;">&nbsp;</td>
                            <td>Luas Bangunan</td>
                            <td style=" width: 10px;">:</td>
                            <td style="border-bottom: 1px black solid; border-bottom-style: dotted"><?= number_format($dataspt->t_luasbangunan, 0, ',', '.') ?> m<sup>2</sup></td>
                          </tr>
                          
                      </table>
                  </td>
              </tr>
          </table>
      </td>
  </tr>
  <tr>
      <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
    <td colspan="3"><p style=" word-wrap: break-word">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apabila Nilai Perolehan Objek Pajak (NPOP) yaitu harga transaksi/nilai pasar tidak sesuai dengan kondisi dan keadaan yang sebenarnya dan nilainya lebih
      tinggi dari SSPD BPHTB yang saya buat, maka saya berjanji bersedia akan membayar kekurangan BPHTB sesuai dengan Surat Ketetapan Pajak Daerah Kurang Bayar (SKPDKB)
      yang diterbitkan oleh {{ucwords(strtolower($data_pemda->s_namainstansi))}} {!!$data_pemda->s_namakabkot !!}}.</p></td>
  </tr>
  <tr>
    <td colspan="3" style=" word-wrap: break-word">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Apabila kewajiban ini tidak saya penuhi, maka saya bersedia menerima sanksi sesuai dengan Peraturan dan ketentuan hukum yang berlaku.</td>
  </tr>
  <tr>
    <td colspan="3" style=" word-wrap: break-word">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Demikian Surat Pernyataan ini dibuat dengan sebnearnya, untuk dapat dipergunakan sebagaimana mestinya.</td>
  </tr>
  <tr>
      <td colspan="3">&nbsp;</td>
  </tr>
  <tr>
      <td colspan="3">
          <table style="width: 100%;">
              <tr>
                  <td style="width: 50%;">&nbsp;</td>
                  <td style="width: 50%;"><center>{{$data_pemda->s_namaibukotakabkot}}, {{ date('d-m-Y') }}</center></td>
              </tr>
              <tr>
                  <td>&nbsp;</td>
                  <td><center>YANG MEMBUAT PERNYATAAN</center></td>
              </tr>
              <tr>
                  <td><center>P E N J U A L<br><br><br><br><br><br><br><br>{{$dataspt->t_nama_penjual}}</center></td>
                    <td><center>P E M B E L I<br><br><br><br><br><br><br>Materai 6.000<br>{{$dataspt->t_nama_pembeli}}</center></td>
              </tr>
              
          </table>
      </td>
  </tr>
</table>  

