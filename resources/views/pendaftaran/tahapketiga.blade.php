@extends('layouts.master')

@section('judulkiri')
Pendaftaran
@endsection

@section('content')

<link rel="stylesheet" href="{{ asset('internet/css/blueimp-gallery.min.css')}}">
<style>
    .bs-wizard {
        margin-top: 40px;
    }

    /*Form Wizard border-bottom: solid 1px #e0e0e0; */
    .bs-wizard {
        padding: 0 0 10px 0;
    }

    .bs-wizard>.bs-wizard-step {
        padding: 0;
        position: relative;
    }

    .bs-wizard>.bs-wizard-step+.bs-wizard-step {}

    .bs-wizard>.bs-wizard-step .bs-wizard-stepnum {
        color: #595959;
        font-size: 16px;
        margin-bottom: 5px;
    }

    .bs-wizard>.bs-wizard-step .bs-wizard-info {
        color: #999;
        font-size: 14px;
    }

    .bs-wizard>.bs-wizard-step>.bs-wizard-dot {
        position: absolute;
        width: 30px;
        height: 30px;
        display: block;
        background: #fbe8aa;
        top: 45px;
        left: 50%;
        margin-top: -15px;
        margin-left: -15px;
        border-radius: 50%;
    }

    .bs-wizard>.bs-wizard-step>.bs-wizard-dot:after {
        content: ' ';
        width: 14px;
        height: 14px;
        background: #fbbd19;
        border-radius: 50px;
        position: absolute;
        top: 8px;
        left: 8px;
    }

    .bs-wizard>.bs-wizard-step>.progress {
        position: relative;
        border-radius: 0px;
        height: 8px;
        box-shadow: none;
        margin: 20px 0;
    }

    .bs-wizard>.bs-wizard-step>.progress>.progress-bar {
        width: 0px;
        box-shadow: none;
        background: #fbe8aa;
    }

    .bs-wizard>.bs-wizard-step.complete>.progress>.progress-bar {
        width: 100%;
    }

    .bs-wizard>.bs-wizard-step.active>.progress>.progress-bar {
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step:first-child.active>.progress>.progress-bar {
        width: 0%;
    }

    .bs-wizard>.bs-wizard-step:last-child.active>.progress>.progress-bar {
        width: 100%;
    }

    .bs-wizard>.bs-wizard-step.disabled>.bs-wizard-dot {
        background-color: #f5f5f5;
    }

    .bs-wizard>.bs-wizard-step.disabled>.bs-wizard-dot:after {
        opacity: 0;
    }

    .bs-wizard>.bs-wizard-step:first-child>.progress {
        left: 50%;
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step:last-child>.progress {
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step.disabled a.bs-wizard-dot {
        pointer-events: none;
    }
</style>

@php
if( (!empty($dataspt->t_nik_penjual)) && (!empty($dataspt->t_nop_sppt)) ){
$link_tahap1 = url('pendaftaran/tahappertama/'.$dataspt->t_uuidspt.'');
$link_tahap2 = url('pendaftaran/tahapkedua/'.$dataspt->t_uuidspt.'');
$link_tahap3 = url('pendaftaran/tahapketiga/'.$dataspt->t_uuidspt.'');
$link_tahap4 = url('pendaftaran/tahapkeempat/'.$dataspt->t_uuidspt.'');
$link_tahap5 = url('pendaftaran/tahapkelima/'.$dataspt->t_uuidspt.'');
$link_tahap6 = url('pendaftaran/tahapkeenam/'.$dataspt->t_uuidspt.'');
$link_tahap7 = url('pendaftaran/tahapketujuh/'.$dataspt->t_uuidspt.'');

$link_tahap1_up = '<a href="'.url('pendaftaran/tahappertama/'.$dataspt->t_uuidspt.'').'">TAHAP 1</a>';
$link_tahap2_up = '<a href="'.url('pendaftaran/tahapkedua/'.$dataspt->t_uuidspt.'').'">TAHAP 2</a>';
$link_tahap3_up = '<a href="'.url('pendaftaran/tahapketiga/'.$dataspt->t_uuidspt.'').'">TAHAP 3</a>';
$link_tahap4_up = '<a href="'.url('pendaftaran/tahapkeempat/'.$dataspt->t_uuidspt.'').'">TAHAP 4</a>';
$link_tahap5_up = '<a href="'.url('pendaftaran/tahapkelima/'.$dataspt->t_uuidspt.'').'">TAHAP 5</a>';
$link_tahap6_up = '<a href="'.url('pendaftaran/tahapkeenam/'.$dataspt->t_uuidspt.'').'">TAHAP 6</a>';
$link_tahap7_up = '<a href="'.url('pendaftaran/tahapketujuh/'.$dataspt->t_uuidspt.'').'">TAHAP 7</a>';
}else{
$link_tahap1 = '';
$link_tahap2 = '';
$link_tahap3 = '';
$link_tahap4 = '';
$link_tahap5 = '';
$link_tahap6 = '';
$link_tahap7 = '';

$link_tahap1_up = 'TAHAP 1';
$link_tahap2_up = 'TAHAP 2';
$link_tahap3_up = 'TAHAP 3';
$link_tahap4_up = 'TAHAP 4';
$link_tahap5_up = 'TAHAP 5';
$link_tahap6_up = 'TAHAP 6';
$link_tahap7_up = 'TAHAP 7';
}
@endphp

@if ($message = Session::get('error'))
<div class="alert alert-danger alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>	
    <strong>{{ $message }}</strong>
</div>
@endif

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">FORM PENDAFTARAN</h3>
    </div>
    <form method="post" action="{{ url('pendaftaran/simpantahapketiga') }}" id="formtambah" name="formtambah">
        <div class="col-sm-12">
            <div class="row bs-wizard">
                <div class="bs-wizard-step complete" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <div class="text-center bs-wizard-stepnum"><b style="color: #337ab7;">{!!$link_tahap1_up!!}</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap1}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"><b style="color: #337ab7;">INFORMASI PENERIMA HAK</b></div>
                </div>

                <div class="bs-wizard-step complete" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum"><b style="color: #337ab7;">{!!$link_tahap2_up!!}</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap2}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"><b style="color: #337ab7;">INFORMASI OBJEK PAJAK</b></div>
                </div>

                <div class="bs-wizard-step active" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum text-primary"><b>{!!$link_tahap3_up!!}</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap3}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center text-primary"><b>UPLOAD FILE PERSYARATAN</b></div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap4_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap4}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> UPLOAD FOTO & LOKASI OBJEK</div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap5_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap5}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> PERHITUNGAN BPHTB</div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap6_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap6}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> INFORMASI PELEPAS HAK</div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap7_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap7}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> PERSETUJUAN</div>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <!--<h6 class="p-2 text-bold text-gray">UPLOAD FILE PERSYARATAN</h6>-->
            <div class="box-body">
                <div class="col-sm-12" id="uploadsemuaberkas">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            @csrf
                            <input type="hidden" name="t_uuidspt" id="t_uuidspt" value="{{ $dataspt->t_uuidspt}}">
                            <table style="width: 100%;">
                                <?php
                                $no = 1;
                                foreach ($cek_persyaratan as $key => $v) {
                                    ?>
                                    <tr>
                                        <td><b style="font-size: 16px;"><?= $no ?></b></td>
                                        <td><b style="font-size: 16px;"><?= $v->s_namapersyaratan ?></b></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <div class="col-sm-12">
                                                <div class="row">
                                                    <div class="col-sm-4 custom-file">
                                                        <input type="file" name="multiple_files<?= $v->s_idpersyaratan ?>" id="multiple_files<?= $v->s_idpersyaratan ?>" multiple class="custom-file-input"/>
                                                        <label class="custom-file-label" for="customFile">Pilih file</label>
                                                    </div>
                                                    <div class="col-sm-4">
                                                        <span id="error_multiple_files<?= $v->s_idpersyaratan ?>"></span>
                                                    </div>
                                                    <?php $cek_ukuranfile = (new \ComboHelper())->cek_ukuranfileupload(); 
                                                            $ukuran_file = $cek_ukuranfile->s_ukuranfile * 1000001;
                                                    ?>
                                                    <div class="col-sm-4" style="text-align: right;">
                                                        <span class="text-muted">Max. Upload <b style="color:red">{{$cek_ukuranfile->s_ukuranfile}}MB</b><br> Hanya file <b style="color:red">.jpg, .png, .gif, .pdf</b> yg di perbolehkan</span>
                                                    </div>
                                                </div>
                                                <br />
                                                <div class="table-responsive" id="image_table<?= $v->s_idpersyaratan ?>">

                                                </div>
                                            </div>
                                        </td>  
                                    </tr>
                                    <tr>
                                        <td colspan="2">
                                            <script type="text/javascript">
                                                $(function () {
                                                    $.ajaxSetup({
                                                        headers: {'X-CSRF-Token': '{{ csrf_token() }}'}
                                                    });
                                                });
                                                $(document).ready(function () {
                                                    load_image_data<?= $v->s_idpersyaratan; ?>();
                                                    function load_image_data<?= $v->s_idpersyaratan; ?>()
                                                    {
                                                        $.ajax({
                                                            url: "<?= url('pendaftaran/fetchpersyaratan') ?>",
                                                            method: "POST",
                                                            data: {
                                                                t_idspt: <?= $dataspt->t_idspt; ?>,
                                                                s_idpersyaratan: <?= $v->s_idpersyaratan; ?>,
                                                                s_idjenistransaksi: <?= $dataspt->t_idjenistransaksi; ?>
                                                            },
                                                            success: function (data)
                                                            {
                                                                var aa = (data);
                                                                $('#image_table<?= $v->s_idpersyaratan; ?>').html(aa.tampildata);
                                                            }
                                                        });
                                                    }
                                                    $('#multiple_files<?= $v->s_idpersyaratan; ?>').change(function () {
                                                        var error_images = '';
                                                        var form_data = new FormData();
                                                        var files = $('#multiple_files<?= $v->s_idpersyaratan; ?>')[0].files;
                                                        if (files.length > 10)
                                                        {
                                                            error_images += 'Anda tidak bisa upload lebih dari 10 file';
                                                        } else
                                                        {
                                                            for (var i = 0; i < files.length; i++)
                                                            {
                                                                var name = document.getElementById("multiple_files<?= $v->s_idpersyaratan; ?>").files[i].name;
                                                                var ext = name.split('.').pop().toLowerCase();
                                                                if (jQuery.inArray(ext, ['gif', 'png', 'jpg', 'jpeg', 'pdf']) == -1)
                                                                {
                                                                    error_images += '<p>Invalid ' + i + ' File</p>';
                                                                }
                                                                var oFReader = new FileReader();
                                                                oFReader.readAsDataURL(document.getElementById("multiple_files<?= $v->s_idpersyaratan; ?>").files[i]);
                                                                var f = document.getElementById("multiple_files<?= $v->s_idpersyaratan; ?>").files[i];
                                                                var fsize = f.size || f.fileSize;
                                                                console.log(fsize);
                                                                
                                                                    
                                                                //if (fsize > 5000001)
                                                                if (fsize > <?= $ukuran_file;?>)
                                                                {
                                                                    error_images += '<p>' + i + ' Ukuran file terlalu besar</p>';
                                                                } else {
                                                                    form_data.append("file[]", document.getElementById('multiple_files<?= $v->s_idpersyaratan; ?>').files[i]);
                                                                }
                                                            }
                                                        }
                                                        if (error_images == '')
                                                        {
                                                            $.ajax({
                                                                url: "<?= url('pendaftaran') ?>/uploadpersyaratan?t_idspt=<?= $dataspt->t_idspt; ?>&s_idpersyaratan=<?= $v->s_idpersyaratan; ?>&s_idjenistransaksi=<?= $dataspt->t_idjenistransaksi; ?>",
                                                                                    method: "POST",
                                                                                    data: form_data,
                                                                                    contentType: false,
                                                                                    cache: false,
                                                                                    processData: false,
                                                                                    beforeSend: function () {
                                                                                        $('#error_multiple_files<?= $v->s_idpersyaratan; ?>').html('<br /><label class="text-primary">Proses upload...</label>');
                                                                                    },
                                                                                    success: function (data)
                                                                                    {
                                                                                        $('#error_multiple_files<?= $v->s_idpersyaratan; ?>').html('<br /><label class="text-success">Terupload</label>');
                                                                                        load_image_data<?= $v->s_idpersyaratan; ?>();
                                                                                    }
                                                                                });
                                                                            } else
                                                                            {
                                                                                $('#multiple_files<?= $v->s_idpersyaratan; ?>').val('');
                                                                                $('#error_multiple_files<?= $v->s_idpersyaratan; ?>').html("<span class='text-danger'>" + error_images + "</span>");
                                                                                return false;
                                                                            }
                                                                        });
                                                                        $(document).on('click', '.edit<?= $v->s_idpersyaratan; ?>', function () {
                                                                            var image_id = $(this).attr("id");
                                                                            $.ajax({
                                                                                url: "<?= url('pendaftaran') ?>/edituploadpersyaratan?s_idpersyaratan=<?= $v->s_idpersyaratan; ?>",
                                                                                                method: "post",
                                                                                                data: {image_id: image_id},
                                                                                                dataType: "json",
                                                                                                success: function (data)
                                                                                                {
                                                                                                    $('#imageModal').modal('show');
                                                                                                    $('#image_id').val(image_id);
                                                                                                    $('#image_name').val(data.image_name);
                                                                                                    $('#image_description').val(data.image_description);
                                                                                                    $('#s_idpersyaratan').val(data.s_idpersyaratan);

                                                                                                    //cekberkasdiupload();
                                                                                                }
                                                                                            });
                                                                                        });
                                                                                        $(document).on('click', '.delete<?= $v->s_idpersyaratan; ?>', function () {
                                                                                            var image_id = $(this).attr("id");
                                                                                            var image_name = $(this).data("image_name");
                                                                                            if (confirm("Apakah anda yakin ingin menghapus gambar ini?"))
                                                                                            {
                                                                                                $.ajax({
                                                                                                    url: "{{ url('pendaftaran') }}/deleteuploadpersyaratan?t_idspt=<?= $dataspt->t_idspt; ?>&s_idpersyaratan=<?= $v->s_idpersyaratan; ?>&s_idjenistransaksi=<?= $dataspt->t_idjenistransaksi; ?>",
                                                                                                    method: "POST",
                                                                                                    data: {image_id: image_id, image_name: image_name},
                                                                                                    success: function (data)
                                                                                                    {
                                                                                                        load_image_data<?= $v->s_idpersyaratan; ?>();
                                                                                                        alert("Gambar berhasil dihapus");

                                                                                                        //cekberkasdiupload();
                                                                                                    }
                                                                                                });
                                                                                            }
                                                                                        });
                                                                                        $('#edit_image_form').on('submit', function (event) {
                                                                                            event.preventDefault();
                                                                                            if ($('#image_name').val() == '')
                                                                                            {
                                                                                                alert("Masukkan nama gambar");
                                                                                            } else
                                                                                            {
                                                                                                var s_idpersyaratan = $('#s_idpersyaratan').val();
                                                                                                if (s_idpersyaratan == <?= $v->s_idpersyaratan; ?>) {
                                                                                                    $.ajax({
                                                                                                        url: "{{ url('pendaftaran') }}/updateuploadpersyaratan?t_idspt=<?= $dataspt->t_idspt; ?>&s_idpersyaratan=<?= $v->s_idpersyaratan; ?>&s_idjenistransaksi=<?= $dataspt->t_idjenistransaksi; ?>",
                                                                                                        method: "POST",
                                                                                                        data: $('#edit_image_form').serialize(),
                                                                                                        success: function (data)
                                                                                                        {
                                                                                                            $('#imageModal').modal('hide');
                                                                                                            load_image_data<?= $v->s_idpersyaratan; ?>();
                                                                                                            alert('Detail gambar berhasil di update');
                                                                                                        }
                                                                                                    });
                                                                                                }
                                                                                            }
                                                                                        });
                                                                                    });
                                            </script>
                                        </td>
                                    </tr>
                                    <?php
                                    $no++;
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>


        <div class="card-footer ">
            <a href="{{ url('pendaftaran') }}/tahapkedua/{{ $dataspt->t_uuidspt}}" class="btn btn-outline-primary prevBtn pull-left" type="button"><i class="fa fa-arrow-circle-left"></i> SEBELUMNYA</a>
            <button id="pendaftaranbutton" type="submit" class="btn btn-primary pull-right"> SELANJUTNYA <i class="fa fa-arrow-circle-right"></i></button>
        </div>
    </form>
</div>

<div class="modal fade bd-example-modal-lg" id="loadingsimpanvalidasi" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="col-12">
                <span class="spinner-grow text-muted"></span>
                <span class="spinner-grow text-primary"></span>
                <span class="spinner-grow text-success"></span>
                <span class="spinner-grow text-info"></span>
                <span class="spinner-grow text-warning"></span>
                <span class="spinner-grow text-danger"></span>
                <span class="spinner-grow text-secondary"></span>
                <span class="spinner-grow text-dark"></span>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="imageModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content bg-success">
            <div class="modal-header">
                <h4 class="modal-title">EDIT GAMBAR</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <form method="POST" id="edit_image_form">
                <div class="modal-body">
                    <div class="form-group">
                        <label>Nama</label>
                        <input type="text" name="image_name" id="image_name" class="form-control" />
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" name="image_description" id="image_description" class="form-control" />
                    </div>

                </div>
                <div class="modal-footer justify-content-between">
                    <input type="hidden" name="image_id" id="image_id" value="" />
                    <input type="hidden" name="s_idpersyaratan" id="s_idpersyaratan" value="" />
                    <input type="submit" name="submit" class="btn btn-info" value="Edit" />
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
</div>

</div>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <!-- <a class="play-pause"></a>
    <ol class="indicator"></ol> -->
</div>

@endsection


@push('scriptsbawah')
<script type="text/javascript">
    function showloading() {
        jQuery('#loadingsimpanvalidasi').modal('show');
    }

    $('#pendaftaranbutton').click(function () {
        var myForm = document.getElementById('formtambah');
        myForm.onsubmit = function () {
            jQuery('#loadingsimpanvalidasi').modal('show');
        }
        }
        ;
    });
</script>

<script src="{{asset('internet/js/jquery.blueimp-gallery.min.js')}}"></script>
@endpush