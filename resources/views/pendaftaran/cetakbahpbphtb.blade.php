@php
$html = '';
$html .= '
<html>
    <head>
        <title>BAHP BPHTB</title>
    </head>
    <body>
        <table align="center">
            <tr>
                <td colspan="2" style="border-bottom: 1px solid #000;">
                <table><tr>
                    <td width="18%">
                        <img width="10%" title="LOGO '.$data_pemda->s_namakabkot.'" align="left" src="'.public_path().'/storage/' . $data_pemda->s_logo.'"/>
                    </td>
                    <td style="text-align: center;">
                        <b>PEMERINTAH ' . strtoupper($data_pemda->s_namakabkot) . '</b><br>
                        <b style="font-size: 20pt;">'.strtoupper($data_pemda->s_namainstansi).' </b><br>
                        <span style="font-size: 12pt">'.ucwords($data_pemda->s_alamatinstansi).' Telp/Fax '.$data_pemda->s_notelinstansi.'<br>'.strtoupper($data_pemda->s_namaibukotakabkot).'</span>
                    </td>
                    <td width="15%"></td>
                </tr></table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                        <center><b>BERITA ACARA HASIL PEMERIKSAAN</b><br>
                    PEMERIKSAAN KEPATUHAN WAJIB PAJAK BPHTB<br>
                    NOMOR :  </center>
                </td>
            </tr>
    <br>
            <tr>
                <td colspan="2">
                <p style="text-indent: 45px;">Pada Tanggal '.date('d-m-Y',strtotime($dataspt->t_tglpenetapan)).' berdasarkan Surat Tugas dari '.$data_pemda->s_namainstansi.' No : '.$dataspt->t_noperiksa.' , kami yang bertanda tangan dibawah ini selaku petugas lapangan : </p>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>1.</td>
                            <td>Nama</td>
                            <td>:</td>
                            <td>'.$pejabat1->s_namapejabat.'</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>NIP</td>
                            <td>:</td>
                            <td>'.$pejabat1->s_nippejabat.'</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Jabatan</td>
                            <td>:</td>
                            <td>'.$pejabat1->s_jabatanpejabat.'</td>
                        </tr>
                        <tr>
                            <td colspan="4">&nbsp;</td>
                        </tr>
                        <tr>
                            <td>2.</td>
                            <td>Nama</td>
                            <td>:</td>
                            <td>'.$pejabat2->s_namapejabat.'</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>NIP</td>
                            <td>:</td>
                            <td>'.$pejabat2->s_nippejabat.'</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Jabatan</td>
                            <td>:</td>
                            <td>'.$pejabat2->s_jabatanpejabat.'</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    Telah mengadakan pemeriksaan kepatuhan wajib pajak BPHTB atas wajib pajak :
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <table>
                        <tr>
                            <td>a.</td>
                            <td>Nama</td>
                            <td>:</td>
                            <td>'.$dataspt->t_nama_pembeli.'</td>
                        </tr>
                        <tr>
                            <td>b.</td>
                            <td>Alamat</td>
                            <td>:</td>
                            <td>'.strtoupper($dataspt->t_jalan_pembeli) . ' ' . strtoupper($dataspt->t_namakelurahan_pembeli) . ' ' . strtoupper($dataspt->t_namakecamatan_pembeli).'</td>
                        </tr>
                        <tr>
                            <td>c.</td>
                            <td>Letak Objek Pajak</td>
                            <td>:</td>
                            <td>'.$dataspt->t_jalan_sppt.' RT '.$dataspt->t_rt_sppt . ' RW ' . $dataspt->t_rw_sppt.' Kel. '.$dataspt->t_kelurahan_sppt . ', Kec. ' . $dataspt->t_kecamatan_sppt.'</td>
                        </tr>

                        <tr>
                            <td>d.</td>
                            <td>Nomor NPWPD/NOP</td>
                            <td>:</td>
                            <td>'.$dataspt->t_nop_sppt.'</td>
                        </tr>
                        <tr>
                            <td>e.</td>
                            <td>Tahun Pajak</td>
                            <td>:</td>
                            <td>'.$dataspt->t_periodespt.'</td>
                        </tr>
                        <tr>
                            <td colspan="4">Adapun hasil pemeriksaan kepatuhan wajib pajak sebagai berikut :</td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">';

                $html .= '
                    <table width="100%" style="border-spacing: 0px;">
                        <tr>
                            <td colspan="12"><center><b>DATABASE HARGA PASAR</b></center></td>
                        </tr>
                        <tr>
                            <td style="width: 150px;">Luas Tanah/Bumi</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_luastanah, 0, ',', '.') . ' m<sup>2</sup></td>
                            <td style="width: 30px;">

                            <td style="width: 150px;">NJOP/m<sup>2</sup> Tanah/Bumi</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_njoptanah, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td style="width: 150px;">NJOP Tanah/Bumi</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_totalnjoptanah, 2, ',', '.') . '</td>
                            <td style="width: 30px;">
                        </tr>

                        <tr>
                            <td>Luas Bangunan</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_luasbangunan, 0, ',', '.') . ' m<sup>2</sup></td>
                            <td style="width: 30px;">

                            <td>NJOP/m<sup>2</sup> Bangunan</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_njopbangunan, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td>NJOP Bangunan</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_totalnjopbangunan, 2, ',', '.') . '</td>
                            <td style="width: 30px;">
                        </tr>

                        <tr>
                            <td>NJOP PBB</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_grandtotalnjop, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td>Harga Transaksi</td>
                            <td>:</td>
                            <td><b>' . number_format($dataspt->t_nilaitransaksispt, 0, ',', '.') . '</b></td>
                            <td style="width: 30px;">

                            <td colspan="4">&nbsp;</td>

                        </tr>

                        <tr>
                            <td>NPOP</td>
                            <td>:</td>
                            <td> ' . number_format($dataspt->t_npop_bphtb, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td colspan="8">&nbsp;</td>

                        </tr>
                        <tr>
                            <td>NPOPTKP</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_npoptkp_bphtb, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td colspan="8">&nbsp;</td>

                        </tr>
                        <tr>
                            <td>NPOPKP</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_npopkp_bphtb, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td colspan="8">&nbsp;</td>

                        </tr>
                        <tr>
                            <td>Tarif (%)</td>
                            <td>:</td>
                            <td>' . $dataspt->t_persenbphtb . '%</td>
                            <td style="width: 30px;">

                            <td colspan="8">&nbsp;</td>

                        </tr>


                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">&nbsp;</td>
            </tr>
            <tr>
                <td colspan="2">';

                $html .= '
                    <table width="100%" style="border-spacing: 0px;">
                        <tr>
                            <td colspan="12"><center><b>HASIL PENELITIAN</b></center></td>
                        </tr>
                        <tr>
                            <td style="width: 150px;">Luas Tanah/Bumi</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_luastanah_pemeriksa, 0, ',', '.') . ' m<sup>2</sup></td>
                            <td style="width: 30px;">

                            <td style="width: 150px;">NJOP/m<sup>2</sup> Tanah/Bumi</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_njoptanah_pemeriksa, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td style="width: 150px;">NJOP Tanah/Bumi</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_totalnjoptanah_pemeriksa, 2, ',', '.') . '</td>
                            <td style="width: 30px;">
                        </tr>

                        <tr>
                            <td>Luas Bangunan</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_luasbangunan_pemeriksa, 0, ',', '.') . ' m<sup>2</sup></td>
                            <td style="width: 30px;">

                            <td>NJOP/m<sup>2</sup> Bangunan</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_njopbangunan_pemeriksa, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td>NJOP Bangunan</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_totalnjopbangunan_pemeriksa, 2, ',', '.') . '</td>
                            <td style="width: 30px;">
                        </tr>

                        <tr>
                            <td>NJOP PBB</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_grandtotalnjop_pemeriksa, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td>Harga Transaksi</td>
                            <td>:</td>
                            <td><b>' . number_format($dataspt->t_nilaitransaksispt_pemeriksa, 0, ',', '.') . '</b></td>
                            <td style="width: 30px;">

                            <td colspan="4">&nbsp;</td>

                        </tr>

                        <tr>
                            <td>NPOP</td>
                            <td>:</td>
                            <td> ' . number_format($dataspt->t_npop_bphtb_pemeriksa, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td colspan="8">&nbsp;</td>

                        </tr>
                        <tr>
                            <td>NPOPTKP</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_npoptkp_bphtb_pemeriksa, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td colspan="8">&nbsp;</td>

                        </tr>
                        <tr>
                            <td>NPOPKP</td>
                            <td>:</td>
                            <td>' . number_format($dataspt->t_npopkp_bphtb_pemeriksa, 0, ',', '.') . '</td>
                            <td style="width: 30px;">

                            <td colspan="8">&nbsp;</td>

                        </tr>
                        <tr>
                            <td>Tarif (%)</td>
                            <td>:</td>
                            <td>' . $dataspt->t_persenbphtb_pemeriksa . '%</td>
                            <td style="width: 30px;">

                            <td colspan="8">&nbsp;</td>

                        </tr>


                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <br>Demikian berita acara hasil penelitian kepatuhan wajib pajak kami buat untuk proses lebih lanjut.
                </td>
            </tr>
        </table>
    </body>

</html>
';

@endphp

{!! $html !!}

