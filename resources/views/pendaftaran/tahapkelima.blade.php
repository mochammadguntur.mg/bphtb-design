@extends('layouts.master')

@section('judulkiri')
PENDAFTARAN
@endsection

@section('content')


<style>
    .bs-wizard {
        margin-top: 40px;
    }

    /*Form Wizard border-bottom: solid 1px #e0e0e0; */
    .bs-wizard {
        padding: 0 0 10px 0;
    }

    .bs-wizard>.bs-wizard-step {
        padding: 0;
        position: relative;
    }

    .bs-wizard>.bs-wizard-step+.bs-wizard-step {}

    .bs-wizard>.bs-wizard-step .bs-wizard-stepnum {
        color: #595959;
        font-size: 16px;
        margin-bottom: 5px;
    }

    .bs-wizard>.bs-wizard-step .bs-wizard-info {
        color: #999;
        font-size: 14px;
    }

    .bs-wizard>.bs-wizard-step>.bs-wizard-dot {
        position: absolute;
        width: 30px;
        height: 30px;
        display: block;
        background: #fbe8aa;
        top: 45px;
        left: 50%;
        margin-top: -15px;
        margin-left: -15px;
        border-radius: 50%;
    }

    .bs-wizard>.bs-wizard-step>.bs-wizard-dot:after {
        content: ' ';
        width: 14px;
        height: 14px;
        background: #fbbd19;
        border-radius: 50px;
        position: absolute;
        top: 8px;
        left: 8px;
    }

    .bs-wizard>.bs-wizard-step>.progress {
        position: relative;
        border-radius: 0px;
        height: 8px;
        box-shadow: none;
        margin: 20px 0;
    }

    .bs-wizard>.bs-wizard-step>.progress>.progress-bar {
        width: 0px;
        box-shadow: none;
        background: #fbe8aa;
    }

    .bs-wizard>.bs-wizard-step.complete>.progress>.progress-bar {
        width: 100%;
    }

    .bs-wizard>.bs-wizard-step.active>.progress>.progress-bar {
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step:first-child.active>.progress>.progress-bar {
        width: 0%;
    }

    .bs-wizard>.bs-wizard-step:last-child.active>.progress>.progress-bar {
        width: 100%;
    }

    .bs-wizard>.bs-wizard-step.disabled>.bs-wizard-dot {
        background-color: #f5f5f5;
    }

    .bs-wizard>.bs-wizard-step.disabled>.bs-wizard-dot:after {
        opacity: 0;
    }

    .bs-wizard>.bs-wizard-step:first-child>.progress {
        left: 50%;
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step:last-child>.progress {
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step.disabled a.bs-wizard-dot {
        pointer-events: none;
    }


</style>


@php
if( (!empty($dataspt->t_nik_penjual)) && (!empty($dataspt->t_nop_sppt)) ){
$link_tahap1 = url('pendaftaran/tahappertama/'.$dataspt->t_uuidspt.'');
$link_tahap2 = url('pendaftaran/tahapkedua/'.$dataspt->t_uuidspt.'');
$link_tahap3 = url('pendaftaran/tahapketiga/'.$dataspt->t_uuidspt.'');
$link_tahap4 = url('pendaftaran/tahapkeempat/'.$dataspt->t_uuidspt.'');
$link_tahap5 = url('pendaftaran/tahapkelima/'.$dataspt->t_uuidspt.'');
$link_tahap6 = url('pendaftaran/tahapkeenam/'.$dataspt->t_uuidspt.'');
$link_tahap7 = url('pendaftaran/tahapketujuh/'.$dataspt->t_uuidspt.'');

$link_tahap1_up = '<a href="'.url('pendaftaran/tahappertama/'.$dataspt->t_uuidspt.'').'">TAHAP 1</a>';
$link_tahap2_up = '<a href="'.url('pendaftaran/tahapkedua/'.$dataspt->t_uuidspt.'').'">TAHAP 2</a>';
$link_tahap3_up = '<a href="'.url('pendaftaran/tahapketiga/'.$dataspt->t_uuidspt.'').'">TAHAP 3</a>';
$link_tahap4_up = '<a href="'.url('pendaftaran/tahapkeempat/'.$dataspt->t_uuidspt.'').'">TAHAP 4</a>';
$link_tahap5_up = '<a href="'.url('pendaftaran/tahapkelima/'.$dataspt->t_uuidspt.'').'">TAHAP 5</a>';
$link_tahap6_up = '<a href="'.url('pendaftaran/tahapkeenam/'.$dataspt->t_uuidspt.'').'">TAHAP 6</a>';
$link_tahap7_up = '<a href="'.url('pendaftaran/tahapketujuh/'.$dataspt->t_uuidspt.'').'">TAHAP 7</a>';
}else{
$link_tahap1 = '';
$link_tahap2 = '';
$link_tahap3 = '';
$link_tahap4 = '';
$link_tahap5 = '';
$link_tahap6 = '';
$link_tahap7 = '';

$link_tahap1_up = 'TAHAP 1';
$link_tahap2_up = 'TAHAP 2';
$link_tahap3_up = 'TAHAP 3';
$link_tahap4_up = 'TAHAP 4';
$link_tahap5_up = 'TAHAP 5';
$link_tahap6_up = 'TAHAP 6';
$link_tahap7_up = 'TAHAP 7';
}
@endphp

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">FORM PENDAFTARAN</h3>
    </div>

    <!-- /.card-header -->
    <!-- form start -->
    <form method="post" action="{{ url('pendaftaran/simpantahappertama') }}" id="formtambah" name="formtambah">
        <div class="col-sm-12">
            <div class="row bs-wizard">

                <div class="bs-wizard-step complete" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <div class="text-center bs-wizard-stepnum"><b style="color: #337ab7;">{!!$link_tahap1_up!!}</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap1}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"><b style="color: #337ab7;">INFORMASI PENERIMA HAK</b></div>
                </div>

                <div class="bs-wizard-step complete" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum"><b style="color: #337ab7;">{!!$link_tahap2_up!!}</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap2}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"><b style="color: #337ab7;">INFORMASI OBJEK PAJAK</b></div>
                </div>

                <div class="bs-wizard-step complete" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum"><b style="color: #337ab7;">{!!$link_tahap3_up!!}</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap3}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"><b style="color: #337ab7;">UPLOAD FILE PERSYARATAN</b></div>
                </div>

                <div class="bs-wizard-step complete" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum"><b style="color: #337ab7;">{!!$link_tahap4_up!!}</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap4}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> <b style="color: #337ab7;">UPLOAD FOTO & LOKASI OBJEK</b></div>
                </div>

                <div class="bs-wizard-step active" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum"><b class="text-primary">{!!$link_tahap5_up!!}</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap5}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"><b class="text-primary"> PERHITUNGAN BPHTB</b></div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap6_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap6}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> INFORMASI PELEPAS HAK</div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap7_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap7}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> PERSETUJUAN</div>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <!--<h6 class="p-2 text-bold text-gray">INFORMAASI PERHITUNGAN PBB</h6>-->
            <br>
            <div class="box-body">
                <div class="col-sm-12 row">
                    <label class="col-sm-3 ">NPOP</label>
                    <div class="col-md-4">
                        @csrf
                        <input type="hidden" name="t_uuidspt" id="t_uuidspt" value="{{ $dataspt->t_uuidspt}}">
                        <input name="t_npop_bphtb" id="t_npop_bphtb" class="form-control" value="{{ number_format($dataspt->t_npop_bphtb, 0, ',', '.')}}" type="text" style="text-align: right;" readonly="true">       
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-3 ">NPOPTKP</label>
                    <div class="col-md-4">
                        <input name="t_npoptkp_bphtb" id="t_npoptkp_bphtb" class="form-control" value="{{ number_format($dataspt->t_npoptkp_bphtb, 0, ',', '.')}}" type="text" style="text-align: right;" readonly="true">       
                    </div>
                    <?php
                    if ($dataspt->t_npoptkp_bphtb > 0) {
                        $ket_npoptkp = 'Dapat NPOPTKP';
                    } else {
                        $ket_npoptkp = 'Tidak Dapat NPOPTKP';
                    }
                    ?>
                    <div class="col-sm-4" id="ket_npoptkpdiv">
                        <input class="form-control" name="ket_npoptkp" id="ket_npoptkp" value="{{$ket_npoptkp}}" style="font-weight:bold;font-size:25px;color:#18F518;background-color:black;text-align:center;" readonly="" type="text"> 
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-3 ">NPOPKP</label>
                    <div class="col-md-4">
                        <input name="t_npopkp_bphtb" id="t_npopkp_bphtb" class="form-control" value="{{ number_format($dataspt->t_npopkp_bphtb, 0, ',', '.')}}" type="text" style="text-align: right;" readonly="true">       
                    </div>
                    <label class="col-sm-2 control-label">Tarif (%)</label>
                    <div class="col-sm-1">
                        <input name="t_persenbphtb" id="t_persenbphtb" class="form-control" readonly="readonly" value="{{ $dataspt->t_persenbphtb}}" type="text">                                          
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-3 ">BPHTB YANG HARUS DIBAYAR</label>
                    <div class="col-md-4">
                        <input name="t_nilai_bphtb_fix" id="t_nilai_bphtb_fix" class="form-control" value="{{ number_format($dataspt->t_nilai_bphtb_fix, 0, ',', '.')}}" type="text" style="text-align: right;" readonly="true">       
                    </div>
                </div>
                <br>
            </div>
        </div>   
    </form>
    <div class="col-md-12">
        <table class="table table-bordered table-hover">
            <thead>
                <tr>
                    <th colspan="10" style="text-align: center; font-weight: bold; background-color: #227DC7; color: white">HISTORY TRANSAKSI WAJIB PAJAK</th>
                </tr>
                <tr>
                    <th scope="col" style="background-color: #227DC7; color: white">No.</th>
                    <th scope="col" style="background-color: #227DC7; color: white">Tgl. Daftar</th>
                    <th scope="col" style="background-color: #227DC7; color: white">NIK</th>
                    <th scope="col" style="background-color: #227DC7; color: white">Nama</th>
                    <th scope="col" style="background-color: #227DC7; color: white">No. Pendaftaran</th>
                    <th scope="col" style="background-color: #227DC7; color: white">Jenis Transaksi</th>
                    <th scope="col" style="background-color: #227DC7; color: white">NOP</th>
                    <th scope="col" style="background-color: #227DC7; color: white">Tahun Pajak</th>
                    <th scope="col" style="background-color: #227DC7; color: white">Nama Notaris</th>
                    <th scope="col" style="background-color: #227DC7; color: white">Jumlah Pajak</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($data_history_tahun_berjalan as $data)
                <tr>
                    <th scope="row">{{ $loop->iteration }}</th>
                    <td>{{ date('d-m-Y', strtotime($data->t_tgldaftar_spt)) }}</td>
                    <td>{{ $data->t_nik_pembeli }}</td>
                    <td>{{ $data->t_nama_pembeli }}</td>
                    <td>{{ $data->t_kohirspt }}</td>
                    <td>{{ $data->s_namajenistransaksi }}</td>
                    <td>{{ $data->t_nop_sppt }}</td>
                    <td>{{ $data->t_periodespt }}</td>
                    <td>{{ $data->s_namanotaris }}</td>
                    <td style="text-align:right">{{ number_format($data->t_nilai_bphtb_fix, 0, ',', '.')}}</td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
    <div class="card-footer ">
        <a href="{{ url('pendaftaran') }}/tahapkeempat/{{ $dataspt->t_uuidspt}}" class="btn btn-outline-primary prevBtn pull-left" type="button"><i class="fa fa-arrow-circle-left"></i> SEBELUMNYA</a>
        <a href="{{ url('pendaftaran') }}/tahapkeenam/{{ $dataspt->t_uuidspt}}" onclick="showloading()" class="btn btn-primary pull-right" type="button">SELANJUTNYA <i class="fa fa-arrow-circle-right"></i></a>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="loadingsimpanvalidasi" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="col-12">
                <span class="spinner-grow text-muted"></span>
                <span class="spinner-grow text-primary"></span>
                <span class="spinner-grow text-success"></span>
                <span class="spinner-grow text-info"></span>
                <span class="spinner-grow text-warning"></span>
                <span class="spinner-grow text-danger"></span>
                <span class="spinner-grow text-secondary"></span>
                <span class="spinner-grow text-dark"></span>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scriptsbawah')
<script type="text/javascript">
    function showloading() {
        jQuery('#loadingsimpanvalidasi').modal('show');
    }

    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-Token': '{{ csrf_token() }}'}
        });
    });
</script>
@endpush