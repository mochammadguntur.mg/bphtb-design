@extends('layouts.master')

@section('judulkiri')
Pendaftaran
@endsection

@section('content')


<style>
    .bs-wizard {
        margin-top: 40px;
    }

    /*Form Wizard border-bottom: solid 1px #e0e0e0; */
    .bs-wizard {
        padding: 0 0 10px 0;
    }

    .bs-wizard>.bs-wizard-step {
        padding: 0;
        position: relative;
    }

    .bs-wizard>.bs-wizard-step+.bs-wizard-step {}

    .bs-wizard>.bs-wizard-step .bs-wizard-stepnum {
        color: #595959;
        font-size: 16px;
        margin-bottom: 5px;
    }

    .bs-wizard>.bs-wizard-step .bs-wizard-info {
        color: #999;
        font-size: 14px;
    }

    .bs-wizard>.bs-wizard-step>.bs-wizard-dot {
        position: absolute;
        width: 30px;
        height: 30px;
        display: block;
        background: #fbe8aa;
        top: 45px;
        left: 50%;
        margin-top: -15px;
        margin-left: -15px;
        border-radius: 50%;
    }

    .bs-wizard>.bs-wizard-step>.bs-wizard-dot:after {
        content: ' ';
        width: 14px;
        height: 14px;
        background: #fbbd19;
        border-radius: 50px;
        position: absolute;
        top: 8px;
        left: 8px;
    }

    .bs-wizard>.bs-wizard-step>.progress {
        position: relative;
        border-radius: 0px;
        height: 8px;
        box-shadow: none;
        margin: 20px 0;
    }

    .bs-wizard>.bs-wizard-step>.progress>.progress-bar {
        width: 0px;
        box-shadow: none;
        background: #fbe8aa;
    }

    .bs-wizard>.bs-wizard-step.complete>.progress>.progress-bar {
        width: 100%;
    }

    .bs-wizard>.bs-wizard-step.active>.progress>.progress-bar {
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step:first-child.active>.progress>.progress-bar {
        width: 0%;
    }

    .bs-wizard>.bs-wizard-step:last-child.active>.progress>.progress-bar {
        width: 100%;
    }

    .bs-wizard>.bs-wizard-step.disabled>.bs-wizard-dot {
        background-color: #f5f5f5;
    }

    .bs-wizard>.bs-wizard-step.disabled>.bs-wizard-dot:after {
        opacity: 0;
    }

    .bs-wizard>.bs-wizard-step:first-child>.progress {
        left: 50%;
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step:last-child>.progress {
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step.disabled a.bs-wizard-dot {
        pointer-events: none;
    }
</style>

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">FORM PENDAFTARAN</h3>
    </div>

    <!-- /.card-header -->
    <!-- form start -->
    <form action="{{ url('pendaftaran/simpantahappertama') }}" id="formtambah" name="formtambah" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="col-sm-12">
            <div class="row bs-wizard">
                <div class="bs-wizard-step active" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <div class="text-center bs-wizard-stepnum"><b style="color: blue;">TAHAP 1</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"><b style="color: blue;">INFORMASI PENERIMA HAK</b></div>
                </div>
                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum">TAHAP 2</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center">INFORMASI OBJEK PAJAK</div>
                </div>
                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum">TAHAP 3</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center">UPLOAD FILE PERSYARATAN</div>
                </div>
                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">TAHAP 4</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> UPLOAD FOTO & LOKASI OBJEK</div>
                </div>
                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">TAHAP 5</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> PERHITUNGAN BPHTB</div>
                </div>
                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">TAHAP 6</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> INFORMASI PELEPAS HAK</div>
                </div>
                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">TAHAP 7</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="#" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> PERSETUJUAN</div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box box-primary" style="border: 1px solid #0073b7;">
                <div class="box-header" style="background-color: #337ab7;">
                    <center><h3 class="box-title" style="color: white;">&nbsp;</h3></center>
                </div>
                <div class="box-body">
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2">Tanggal</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="far fa-clock"></i></span>
                                </div>
                                <input type="text" class="form-control float-right" name="t_tgldaftar_spt" id="t_tgldaftar_spt" value="{{ date('Y-m-d H:i:s') }}" @error('t_tgldaftar_spt') is-invalid @enderror" >
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Notaris/PPAT</label>
                        <div class="col-md-4">
                            <input type="hidden" name="t_idspt" id="t_idspt">
                            <select name="t_idnotaris_spt" id="t_idnotaris_spt" class="form-control bs-select-hidden @error('t_idnotaris_spt') is-invalid @enderror">
                                <?php if (empty($all_session->s_idnotaris)) { ?>
                                    <option value="">Silahkan Pilih</option>
                                <?php } ?>
                                @foreach ($data_notaris as $a)
                                <?php if (!empty($all_session->s_idnotaris)) { ?>
                                    <?php if ($all_session->s_idnotaris == $a->s_idnotaris) { ?>
                                        <option {{ old('t_idnotaris_spt') == $a->s_idnotaris ? 'selected' : '' }} value="{{ $a->s_idnotaris }}" {{ $all_session->s_idnotaris == $a->s_idnotaris ? 'selected' : '' }}> {{ $a->s_namanotaris }}</option>
                                    <?php } ?>
                                <?php } else { ?>
                                    <option {{ old('t_idnotaris_spt') == $a->s_idnotaris ? 'selected' : '' }} value="{{ $a->s_idnotaris }}" {{ $all_session->s_idnotaris == $a->s_idnotaris ? 'selected' : '' }}> {{ $a->s_namanotaris }}</option>
                                <?php } ?>
                                @endforeach
                            </select>
                            @error('t_idnotaris_spt')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">JENIS TRANSAKSI</label>
                        <div class="col-md-4">
                            <select name="t_idjenistransaksi" id="t_idjenistransaksi" class="form-control @error('t_idjenistransaksi') is-invalid @enderror">
                                <option value="">Silahkan Pilih</option>
                                @php
                                    $no = 1;
                                @endphp
                                @foreach ($data_jenistransaksi as $b)
                                <option {{ old('t_idjenistransaksi') == $b->s_idjenistransaksi ? 'selected' : '' }} value="{{ $b->s_idjenistransaksi }}"> {{ sprintf("%02d",$no++) }} || {{ $b->s_namajenistransaksi }}</option>
                                @endforeach
                            </select>
                            @error('t_idjenistransaksi')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2 mb-2">
                        <label class="col-sm-2 ">SUBJEK BPHTB</label>
                        <div class="col-md-4">
                            <select name="t_idbidang_usaha" id="t_idbidang_usaha" class="form-control @error('t_idbidang_usaha') is-invalid @enderror" onchange="showDiv(this)">
                                <option value="">Silahkan Pilih</option>
                                <option {{ old('t_idbidang_usaha') == 1 ? 'selected' : '' }} value="1">PRIBADI</option>
                                <option {{ old('t_idbidang_usaha') == 2 ? 'selected' : '' }} value="2">BADAN USAHA</option>
                            </select>
                            @error('t_idbidang_usaha')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="col-md-12">
            <div class="box box-primary" style="border: 1px solid #0073b7;">
                <div class="box-header" style="background-color: #337ab7;">
                    <center><h3 class="box-title" style="color: white;">FORM PENERIMA HAK (PIHAK PERTAMA)</h3></center>
                </div>
                <div class="box-body">
                    <div id="hidden_nik">
                        <div class="col-sm-12 row">
                            <label class="col-sm-2 ">NIK</label>
                            <div class="col-sm-4">
                                <input name="t_nik_pembeli" id="t_nik_pembeli" class="form-control @error('t_nik_pembeli') is-invalid @enderror" value="{{ old('t_nik_pembeli') }}" maxlength="16" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text">
                                @error('t_nik_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                            <div class="col-sm-2">
                                <button type="button" name="add" id="add" class="btn btn-success"><i class="fa fa-fw fa-plus"></i> NIK BERSAMA</button>
                            </div>
                            <div class="col-md-6">
                                <div id="message_nik"> </div>
                            </div>
                        </div>
                        <div class="col-sm-12 row">
                            <label  class="col-sm-2 control-label">&nbsp;</label>
                            <div class="col-sm-10 row">
                                <div class="table-responsive">
                                    <input name="id_nik_bersama[]" id="id_nik_bersama" value="" type="hidden">
                                    <input type="hidden" maxlength="16" name="nik_bersama[]" id="nik_bersama[]" onchange="return numbersonly(this, event);" onblur="return numbersonly(this, event);" onfocus="return numbersonly(this, event);" placeholder="NIK" class="form-control name_list" />
                                    <input type="hidden" class="form-control name_list" name="nama_bersama[]" id="nama_bersama[]" placeholder="Nama"/>
                                    <input type="hidden" class="form-control name_list" name="nama_jalanbersama[]" id="nama_jalanbersama[]" placeholder="Nama Jalan"/>
                                    <input type="hidden" class="form-control name_list" name="rt_bersama[]" id="rt_bersama[]" placeholder="RT"/>
                                    <input type="hidden" class="form-control name_list" name="rw_bersama[]" id="rw_bersama[]" placeholder="RW"/>
                                    <input type="hidden" class="form-control name_list" name="kecamatan_bersama[]" id="kecamatan_bersama[]" placeholder="Kecamatan"/>
                                    <input type="hidden" class="form-control name_list" name="kelurahan_bersama[]" id="kelurahan_bersama[]" placeholder="Kelurahan"/>
                                    <input type="hidden" class="form-control name_list" name="kabkota_bersama[]" id="kabkota_bersama[]" placeholder="Kab/Kota"/>
                                    <table class="table table-bordered" id="dynamic_field">
                                        <tr>
                                             <!--<td><input type="text" name="NIK_BERSAMA[]" id="NIK_BERSAMA[]" onchange="hitungBphtb();return numbersonly(this, event);" onblur="hitungBphtb();return numbersonly(this, event);" onfocus="hitungBphtb();return numbersonly(this, event);" placeholder="NIK" class="form-control name_list" /></td> -->
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="hidden_div2" style="display: none;">
                        <div class="col-sm-12 row mt-2">
                            <label class="col-sm-2 ">NIB</label>
                            <div class="col-sm-4">
                                <input name="t_nib_pembeli" id="t_nib_pembeli" class="form-control @error('t_nib_pembeli') is-invalid @enderror" value="{{ old('t_nib_pembeli') }}" maxlength="13" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text">
                                @error('t_nib_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-12 row mt-2">
                            <label class="col-sm-2 ">SIUP</label>
                            <div class="col-md-4">
                                <input name="t_siup_pembeli" id="t_siup_pembeli" class="form-control @error('t_siup_pembeli') is-invalid @enderror" value="{{ old('t_siup_pembeli') }}" type="text">
                                @error('t_siup_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="col-sm-12 row mt-2">
                            <label class="col-sm-2 ">Ket Domisili</label>
                            <div class="col-md-4">
                                <textarea name="t_ketdomisili_pembeli" id="t_ketdomisili_pembeli" class="form-control @error('t_ketdomisili_pembeli') is-invalid @enderror">{{ old('t_ketdomisili_pembeli') }}</textarea>
                                @error('t_ketdomisili_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Nama</label>
                        <div class="col-md-4">
                            <input name="t_nama_pembeli" id="t_nama_pembeli" class="form-control @error('t_nama_pembeli') is-invalid @enderror" value="{{ old('t_nama_pembeli') }}" type="text">
                            @error('t_nama_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2">NPWP</label>
                        <div class="col-md-4">
                            <input name="t_npwp_pembeli" id="t_npwp_pembeli" class="form-control @error('t_npwp_pembeli') is-invalid @enderror" value="{{ old('t_npwp_pembeli') }}" type="text" placeholder="__.___.___._-___.___">
                            @error('t_npwp_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Jalan</label>
                        <div class="col-md-4">
                            <textarea name="t_jalan_pembeli" id="t_jalan_pembeli" class="form-control @error('t_jalan_pembeli') is-invalid @enderror">{{ old('t_jalan_pembeli') }}</textarea>
                            @error('t_jalan_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">RT</label>
                        <div class="col-md-1">
                            <input name="t_rt_pembeli" id="t_rt_pembeli" class="form-control @error('t_rt_pembeli') is-invalid @enderror" value="{{ old('t_rt_pembeli') }}" maxlength="3" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text" placeholder="000">
                            @error('t_rt_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <label class="col-sm-1">RW</label>
                        <div class="col-md-1">
                            <input name="t_rw_pembeli" id="t_rw_pembeli" class="form-control @error('t_rw_pembeli') is-invalid @enderror" value="{{ old('t_rw_pembeli') }}" maxlength="3" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text" placeholder="000">
                            @error('t_rw_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Kabupaten</label>
                        <div class="col-md-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-globe"></i></span>
                                </div>
                                <input name="t_kabkota_pembeli" id="t_kabkota_pembeli" class="form-control @error('t_kabkota_pembeli') is-invalid @enderror" value="{{ old('t_kabkota_pembeli') }}" type="text">
                                @error('t_kabkota_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Kecamatan</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-globe"></i></span>
                                </div>
                                <select name="t_idkec_pembeli" id="t_idkec_pembeli" class="form-control @error('t_idkec_pembeli') is-invalid @enderror" onchange="comboKelurahanCamat(1);">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach ($data_kecamatan as $dkec_prib)
                                    <option {{ old('t_idkec_pembeli') == $dkec_prib->s_idkecamatan ? 'selected' : '' }} value="{{ $dkec_prib->s_idkecamatan }}"> {{ $dkec_prib->s_namakecamatan }}</option>
                                    @endforeach
                                </select>
                                @error('t_idkec_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <label class="col-sm-2">Kelurahan</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-globe"></i></span>
                                </div>
                                <input type="hidden" id="t_idkel_pembeli_old" value="{{ old('t_idkel_pembeli') }}">
                                <select name="t_idkel_pembeli" id="t_idkel_pembeli" class="form-control @error('t_idkel_pembeli') is-invalid @enderror" onchange="cek_nama_kelurahan(1);">
                                    <option value="">Silahkan Pilih</option>
                                </select>
                                @error('t_idkel_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Nama Kecamatan</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-globe"></i></span>
                                </div>
                                <input name="t_namakecamatan_pembeli" id="t_namakecamatan_pembeli" class="form-control @error('t_namakecamatan_pembeli') is-invalid @enderror" value="{{ old('t_namakecamatan_pembeli') }}" type="text" readonly="true">
                                @error('t_namakecamatan_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <label class="col-sm-2 ">Nama Kelurahan</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-globe"></i></span>
                                </div>
                                <input name="t_namakelurahan_pembeli" id="t_namakelurahan_pembeli" class="form-control @error('t_namakelurahan_pembeli') is-invalid @enderror" value="{{ old('t_namakelurahan_pembeli') }}" type="text" readonly="true">
                                @error('t_namakelurahan_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">No HP</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-phone-square"></i></span>
                                </div>
                                <input name="t_nohp_pembeli" id="t_nohp_pembeli" class="form-control @error('t_nohp_pembeli') is-invalid @enderror" value="{{ old('t_nohp_pembeli') }}" maxlength="15" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text">
                                @error('t_nohp_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <label class="col-sm-1">No Telp</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-phone-square"></i></span>
                                </div>
                                <input name="t_notelp_pembeli" id="t_notelp_pembeli" class="form-control @error('t_notelp_pembeli') is-invalid @enderror" value="{{ old('t_notelp_pembeli') }}" maxlength="15" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text">
                                @error('t_notelp_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Email</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-envelope"></i></span>
                                </div>
                                <input name="t_email_pembeli" id="t_email_pembeli" class="form-control @error('t_email_pembeli') is-invalid @enderror" value="{{ old('t_email_pembeli') }}" type="text">
                                @error('t_email_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2 mb-2">
                        <label class="col-sm-2 ">Kode Pos</label>
                        <div class="col-md-3">
                            <input name="t_kodepos_pembeli" id="t_kodepos_pembeli" class="form-control @error('t_kodepos_pembeli') is-invalid @enderror" value="{{ old('t_kodepos_pembeli') }}" maxlength="5" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text">
                            @error('t_kodepos_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div><!-- /.box-body -->
            </div><!-- /.box -->
        </div>
        <br>
        <div class="col-md-12" id="hidden_div" style="display: none;">
            <div class="box box-primary" style="border: 1px solid #0073b7;">
                <div class="box-header" style="background-color: #337ab7;">
                    <center><h3 class="box-title" style="color: white;">FORM PENANGGUNG JAWAB BADAN USAHA</h3></center>
                </div>
                <div class="box-body">
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">NIK</label>
                        <div class="col-md-4">
                            <input name="t_b_nik_pngjwb_pembeli" id="t_b_nik_pngjwb_pembeli" class="form-control @error('t_b_nik_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_nik_pngjwb_pembeli') }}" maxlength="16" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text" placeholder="16 digit">
                            @error('t_b_nik_pngjwb_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Nama</label>
                        <div class="col-md-4">
                            <input name="t_b_nama_pngjwb_pembeli" id="t_b_nama_pngjwb_pembeli" class="form-control @error('t_b_nama_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_nama_pngjwb_pembeli') }}" type="text">
                            @error('t_b_nama_pngjwb_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">NPWP</label>
                        <div class="col-md-4">
                            <input name="t_b_npwp_pngjwb_pembeli" id="t_b_npwp_pngjwb_pembeli" class="form-control @error('t_b_npwp_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_npwp_pngjwb_pembeli') }}" type="text" placeholder="__.___.___._-___.___" >
                            @error('t_b_npwp_pngjwb_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Jabatan</label>
                        <div class="col-md-4">
                            <input name="t_b_statusjab_pngjwb_pembeli" id="t_b_statusjab_pngjwb_pembeli" class="form-control @error('t_b_statusjab_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_statusjab_pngjwb_pembeli') }}" type="text">
                            @error('t_b_statusjab_pngjwb_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Jalan</label>
                        <div class="col-md-4">
                            <textarea name="t_b_jalan_pngjwb_pembeli" id="t_b_jalan_pngjwb_pembeli" class="form-control @error('t_b_jalan_pngjwb_pembeli') is-invalid @enderror">{{ old('t_b_jalan_pngjwb_pembeli') }}</textarea>
                            @error('t_b_jalan_pngjwb_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">RT</label>
                        <div class="col-md-1">
                            <input name="t_b_rt_pngjwb_pembeli" id="t_b_rt_pngjwb_pembeli" class="form-control @error('t_b_rt_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_rt_pngjwb_pembeli') }}" maxlength="3" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text" placeholder="000">
                            @error('t_b_rt_pngjwb_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <label class="col-sm-1">RW</label>
                        <div class="col-md-1">
                            <input name="t_b_rw_pngjwb_pembeli" id="t_b_rw_pngjwb_pembeli" class="form-control @error('t_b_rw_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_rw_pngjwb_pembeli') }}" maxlength="3" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text" placeholder="000">
                            @error('t_b_rw_pngjwb_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Kabupaten</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-globe"></i></span>
                                </div>
                                <input name="t_b_kabkota_pngjwb_pembeli" id="t_b_kabkota_pngjwb_pembeli" class="form-control @error('t_b_kabkota_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_kabkota_pngjwb_pembeli') }}" type="text">
                                @error('t_b_kabkota_pngjwb_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Kecamatan</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-globe"></i></span>
                                </div>
                                <select name="t_b_idkec_pngjwb_pembeli" id="t_b_idkec_pngjwb_pembeli" class="form-control @error('t_b_idkec_pngjwb_pembeli') is-invalid @enderror" onchange="comboKelurahanCamat(3);">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach ($data_kecamatan as $dkec_png)
                                    <option {{ old('t_b_idkec_pngjwb_pembeli') == $dkec_png->s_idkecamatan ? 'selected' : '' }}  value="{{ $dkec_png->s_idkecamatan }}" {{ $dataspt->t_b_idkec_pngjwb_pembeli == $dkec_png->s_idkecamatan ? 'selected' : '' }}> {{ $dkec_png->s_namakecamatan }}</option>
                                    @endforeach
                                </select>
                                @error('t_b_idkec_pngjwb_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <label class="col-sm-2">Kelurahan</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-globe"></i></span>
                                </div>
                                <input type="hidden" id="t_b_idkel_pngjwb_pembeli_old" value="{{ old('t_b_idkel_pngjwb_pembeli') }}">
                                <select name="t_b_idkel_pngjwb_pembeli" id="t_b_idkel_pngjwb_pembeli" class="form-control @error('t_b_idkel_pngjwb_pembeli') is-invalid @enderror" onchange="cek_nama_kelurahan(3);">
                                    <option value="">Silahkan Pilih</option>
                                </select>
                                @error('t_b_idkel_pngjwb_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Nama Kecamatan</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-globe"></i></span>
                                </div>
                                <input name="t_b_namakec_pngjwb_pembeli" id="t_b_namakec_pngjwb_pembeli" class="form-control @error('t_b_namakec_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_namakec_pngjwb_pembeli') }}" type="text" readonly="true">
                                @error('t_b_namakec_pngjwb_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <label class="col-sm-2 ">Nama Kelurahan</label>
                        <div class="col-md-4">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-globe"></i></span>
                                </div>
                                <input name="t_b_namakel_pngjwb_pembeli" id="t_b_namakel_pngjwb_pembeli" class="form-control @error('t_b_namakel_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_namakel_pngjwb_pembeli') }}" type="text" readonly="true">
                                @error('t_b_namakel_pngjwb_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">No HP</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-phone-square"></i></span>
                                </div>
                                <input name="t_b_nohp_pngjwb_pembeli" id="t_b_nohp_pngjwb_pembeli" class="form-control @error('t_b_nohp_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_nohp_pngjwb_pembeli') }}" maxlength="15" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text">
                                @error('t_b_nohp_pngjwb_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <label class="col-sm-1">No Telp</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-phone-square"></i></span>
                                </div>
                                <input name="t_b_notelp_pngjwb_pembeli" id="t_b_notelp_pngjwb_pembeli" class="form-control @error('t_b_notelp_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_notelp_pngjwb_pembeli') }}" maxlength="15" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text">
                                @error('t_b_notelp_pngjwb_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 ">Email</label>
                        <div class="col-sm-3">
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fa fa-fw fa-envelope"></i></span>
                                </div>
                                <input name="t_b_email_pngjwb_pembeli" id="t_b_email_pngjwb_pembeli" class="form-control @error('t_b_email_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_email_pngjwb_pembeli') }}" type="text">
                                @error('t_b_email_pngjwb_pembeli')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2 mb-2">
                        <label class="col-sm-2 ">Kode Pos</label>
                        <div class="col-md-3">
                            <input name="t_b_kodepos_pngjwb_pembeli" id="t_b_kodepos_pngjwb_pembeli" class="form-control @error('t_b_kodepos_pngjwb_pembeli') is-invalid @enderror" value="{{ old('t_b_kodepos_pngjwb_pembeli') }}" maxlength="5" data-parsley-type="digits" onkeypress="return numbersonly(this, event);" type="text" placeholder="Kode Pos 5 Digit">
                            @error('t_b_kodepos_pngjwb_pembeli')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                </div>
            </div>
            <br>
        </div>
        <div class="card-footer ">
            <button id="pendaftaranbutton" type="submit" class="btn btn-primary pull-right" >SELANJUTNYA <i class="fa fa-arrow-circle-right"></i></button>
        </div>
    </form>
</div>

<div class="modal fade bd-example-modal-lg" id="loadingsimpanvalidasi" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="col-12">
                <span class="spinner-grow text-muted"></span>
                <span class="spinner-grow text-primary"></span>
                <span class="spinner-grow text-success"></span>
                <span class="spinner-grow text-info"></span>
                <span class="spinner-grow text-warning"></span>
                <span class="spinner-grow text-danger"></span>
                <span class="spinner-grow text-secondary"></span>
                <span class="spinner-grow text-dark"></span>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scriptsbawah')
<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-Token': '{{ csrf_token() }}'}
        });
    });

    
    $(document).ready(function () {
        $('#t_tgldaftar_spt').daterangepicker({
            timePicker: true,
            singleDatePicker: true,
            timePicker24Hour: true,
            timePickerSeconds: true,
            locale: {
                format: 'YYYY-MM-DD hh:mm:ss'
            }
        });
        $('#t_npwp_pembeli').mask("00.000.000.0-000.000", {placeholder: "__.___.___._-___.___"});
        $('#t_b_npwp_pngjwb_pembeli').mask("00.000.000.0-000.000", {placeholder: "__.___.___._-___.___"});
        var a = document.getElementById('t_idbidang_usaha');
        showDiv(a);
        comboKelurahanCamat(1);
        comboKelurahanCamat(3);
    });

    function showDiv(elem) {
        if (elem.value == 2) {
            document.getElementById('hidden_nik').style.display = "none";
            document.getElementById('hidden_div').style.display = "block";
            document.getElementById('hidden_div2').style.display = "block";
        } else {
            document.getElementById('hidden_nik').style.display = "block";
            document.getElementById('hidden_div').style.display = "none";
            document.getElementById('hidden_div2').style.display = "none";
        }
    }

    $(document).ready(function () {
        var i = 1;
        $('#add').click(function () {
            i++;
            $('#dynamic_field').append('<tr id="row' + i + '">\n\
                                                <td><input name="id_nik_bersama[]" id="id_nik_bersama' + i + '" value="" type="hidden"><input type="text" maxlength="16" name="nik_bersama[]" id="nik_bersama[]" onkeypress="return numbersonly(this, event)" onchange="return numbersonly(this, event);" onblur="return numbersonly(this, event);" onfocus="return numbersonly(this, event);" placeholder="NIK" class="form-control name_list" /></td>\n\
                                                <td><input type="text" class="form-control name_list" name="nama_bersama[]" id="nama_bersama[]" placeholder="NAMA"/></td>\n\
\n\                                             <td><input type="text" class="form-control name_list" name="nama_jalanbersama[]" id="nama_jalanbersama[]" placeholder="JALAN"/></td>\n\
\n\                                             <td><input type="text" class="form-control name_list" name="rt_bersama[]" id="rt_bersama[]" maxlength="3" onkeypress="return numbersonly(this, event)" onchange="return numbersonly(this, event);" onblur="return numbersonly(this, event);" onfocus="return numbersonly(this, event);" placeholder="RT"/></td>\n\
\n\                                             <td><input type="text" class="form-control name_list" name="rw_bersama[]" id="rw_bersama[]" maxlength="3" onkeypress="return numbersonly(this, event)" onchange="return numbersonly(this, event);" onblur="return numbersonly(this, event);" onfocus="return numbersonly(this, event);" placeholder="RW"/></td>\n\
\n\                                             <td><input type="text" class="form-control name_list" name="kecamatan_bersama[]" id="kecamatan_bersama[]" placeholder="Kecamatan"/></td>\n\
\n\                                             <td><input type="text" class="form-control name_list" name="kelurahan_bersama[]" id="kelurahan_bersama[]" placeholder="Kelurahan"/></td>\n\
\n\                                             <td><input type="text" class="form-control name_list" name="kabkota_bersama[]" id="kabkota_bersama[]" placeholder="Kab/Kota"/></td>\n\
                                                <td><button type="button" name="remove" id="' + i + '" class="btn btn-danger btn_remove">X</button></td>\n\
                                    </tr>');

        });
        $(document).on('click', '.btn_remove', function () {
            var button_id = $(this).attr("id");
            $('#row' + button_id + '').remove();

        });
        $('#submit').click(function () {
            $.ajax({
                url: "name.php",
                method: "POST",
                data: $('#add_name').serialize(),
                success: function (data)
                {
                    alert(data);
                    $('#add_name')[0].reset();
                }
            });
        });

    });


    function comboKelurahanCamat(dari) {
        if (dari === 1) {
            var idkec = $('#t_idkec_pembeli').val();
            var idkel = $('#t_idkel_pembeli_old').val();
        } else if (dari === 3) {
            var idkec = $('#t_b_idkec_pngjwb_pembeli').val();
            var idkel = $('#t_b_idkel_pngjwb_pembeli_old').val();
        }
        $.post('<?= url('pendaftaran') ?>/cekdatakelurahan', {
            s_idkecamatan: idkec,
            s_idkelurahan: idkel
        }, function (data) {
            var aa = (data);
            if (dari == 1) {
                $("#t_idkel_pembeli").html(aa.res);
            } else if (dari == 3) {
                $("#t_b_idkel_pngjwb_pembeli").html(aa.res);
            }
            if (aa.s_kd_kecamatan == '000') {
                if (dari == 1) {
                    $('#t_namakecamatan_pembeli').prop('readonly', false);
                    $('#t_namakecamatan_pembeli').attr('required', 'required');
                    $('#t_namakecamatan_pembeli').val("");

                    $('#t_namakelurahan_pembeli').prop('readonly', false);
                    $('#t_namakelurahan_pembeli').attr('required', 'required');
                    $('#t_namakelurahan_pembeli').val("");
                } else if (dari == 3) {
                    $('#t_b_namakec_pngjwb_pembeli').prop('readonly', false);
                    $('#t_b_namakec_pngjwb_pembeli').attr('required', 'required');
                    $('#t_b_namakec_pngjwb_pembeli').val("");

                    $('#t_b_namakel_pngjwb_pembeli').prop('readonly', false);
                    $('#t_b_namakel_pngjwb_pembeli').attr('required', 'required');
                    $('#t_b_namakel_pngjwb_pembeli').val("");
                }
            } else {
                if (dari == 1) {
                    $('#t_namakecamatan_pembeli').prop('readonly', true);
                    $('#t_namakecamatan_pembeli').val(aa.nama_kec);
                    $('#t_namakecamatan_pembeli').removeAttr('required');
                } else if (dari == 3) {
                    $('#t_b_namakec_pngjwb_pembeli').prop('readonly', true);
                    $('#t_b_namakec_pngjwb_pembeli').val(aa.nama_kec);
                    $('#t_b_namakec_pngjwb_pembeli').removeAttr('required');
                }
            }
        });
    }

    function cek_nama_kelurahan(dari) {
        if (dari == 1) {
            var idkel = $('#t_idkel_pembeli').val();
        } else if (dari == 3) {
            var idkel = $('#t_b_idkel_pngjwb_pembeli').val();
        }
        $.post('<?= url('pendaftaran') ?>/ceknamakelurahan', {s_idkelurahan: idkel}, function (data) {
            var aa = (data);

            if (aa.s_kd_kelurahan == '000') {
                if (dari == 1) {
                    $('#t_namakelurahan_pembeli').prop('readonly', false);
                    $('#t_namakelurahan_pembeli').attr('required', 'required');
                    $('#t_namakelurahan_pembeli').val("");
                } else if (dari == 3) {
                    $('#t_b_namakel_pngjwb_pembeli').prop('readonly', false);
                    $('#t_b_namakel_pngjwb_pembeli').attr('required', 'required');
                    $('#t_b_namakel_pngjwb_pembeli').val("");
                }
            } else {
                if (dari == 1) {
                    $('#t_namakelurahan_pembeli').prop('readonly', true);
                    $('#t_namakelurahan_pembeli').val(aa.nama_kel);
                    $('#t_namakelurahan_pembeli').removeAttr('required');
                } else if (dari == 3) {
                    $('#t_b_namakel_pngjwb_pembeli').prop('readonly', true);
                    $('#t_b_namakel_pngjwb_pembeli').val(aa.nama_kel);
                    $('#t_b_namakel_pngjwb_pembeli').removeAttr('required');
                }
            }
        });
    }

    $('#pendaftaranbutton').click(function () {
        var myForm = document.getElementById('formtambah');
        myForm.onsubmit = function () {
            var allInputs = myForm.getElementsByTagName('input');
            var input, i;

            for (i = 0; input = allInputs[i]; i++) {
                if (input.getAttribute('name') && !input.value) {
                    //input.setAttribute('name', '');

                } else {
                    jQuery('#loadingsimpanvalidasi').modal('show');
                }
            }
        };
    });

</script>
@endpush
