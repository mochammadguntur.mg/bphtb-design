@extends('layouts.master')

@section('judulkiri')
Pendaftaran
@endsection

@section('content')


<style>
    .bs-wizard {
        margin-top: 40px;
    }

    /*Form Wizard border-bottom: solid 1px #e0e0e0; */
    .bs-wizard {
        padding: 0 0 10px 0;
    }

    .bs-wizard>.bs-wizard-step {
        padding: 0;
        position: relative;
    }

    .bs-wizard>.bs-wizard-step+.bs-wizard-step {}

    .bs-wizard>.bs-wizard-step .bs-wizard-stepnum {
        color: #595959;
        font-size: 16px;
        margin-bottom: 5px;
    }

    .bs-wizard>.bs-wizard-step .bs-wizard-info {
        color: #999;
        font-size: 14px;
    }

    .bs-wizard>.bs-wizard-step>.bs-wizard-dot {
        position: absolute;
        width: 30px;
        height: 30px;
        display: block;
        background: #fbe8aa;
        top: 45px;
        left: 50%;
        margin-top: -15px;
        margin-left: -15px;
        border-radius: 50%;
    }

    .bs-wizard>.bs-wizard-step>.bs-wizard-dot:after {
        content: ' ';
        width: 14px;
        height: 14px;
        background: #fbbd19;
        border-radius: 50px;
        position: absolute;
        top: 8px;
        left: 8px;
    }

    .bs-wizard>.bs-wizard-step>.progress {
        position: relative;
        border-radius: 0px;
        height: 8px;
        box-shadow: none;
        margin: 20px 0;
    }

    .bs-wizard>.bs-wizard-step>.progress>.progress-bar {
        width: 0px;
        box-shadow: none;
        background: #fbe8aa;
    }

    .bs-wizard>.bs-wizard-step.complete>.progress>.progress-bar {
        width: 100%;
    }

    .bs-wizard>.bs-wizard-step.active>.progress>.progress-bar {
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step:first-child.active>.progress>.progress-bar {
        width: 0%;
    }

    .bs-wizard>.bs-wizard-step:last-child.active>.progress>.progress-bar {
        width: 100%;
    }

    .bs-wizard>.bs-wizard-step.disabled>.bs-wizard-dot {
        background-color: #f5f5f5;
    }

    .bs-wizard>.bs-wizard-step.disabled>.bs-wizard-dot:after {
        opacity: 0;
    }

    .bs-wizard>.bs-wizard-step:first-child>.progress {
        left: 50%;
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step:last-child>.progress {
        width: 50%;
    }

    .bs-wizard>.bs-wizard-step.disabled a.bs-wizard-dot {
        pointer-events: none;
    }
</style>

@php
if( (!empty($dataspt->t_nik_penjual)) && (!empty($dataspt->t_nop_sppt)) ){
$link_tahap1 = url('pendaftaran/tahappertama/'.$dataspt->t_uuidspt.'');
$link_tahap2 = url('pendaftaran/tahapkedua/'.$dataspt->t_uuidspt.'');
$link_tahap3 = url('pendaftaran/tahapketiga/'.$dataspt->t_uuidspt.'');
$link_tahap4 = url('pendaftaran/tahapkeempat/'.$dataspt->t_uuidspt.'');
$link_tahap5 = url('pendaftaran/tahapkelima/'.$dataspt->t_uuidspt.'');
$link_tahap6 = url('pendaftaran/tahapkeenam/'.$dataspt->t_uuidspt.'');
$link_tahap7 = url('pendaftaran/tahapketujuh/'.$dataspt->t_uuidspt.'');

$link_tahap1_up = '<a href="'.url('pendaftaran/tahappertama/'.$dataspt->t_uuidspt.'').'">TAHAP 1</a>';
$link_tahap2_up = '<a href="'.url('pendaftaran/tahapkedua/'.$dataspt->t_uuidspt.'').'">TAHAP 2</a>';
$link_tahap3_up = '<a href="'.url('pendaftaran/tahapketiga/'.$dataspt->t_uuidspt.'').'">TAHAP 3</a>';
$link_tahap4_up = '<a href="'.url('pendaftaran/tahapkeempat/'.$dataspt->t_uuidspt.'').'">TAHAP 4</a>';
$link_tahap5_up = '<a href="'.url('pendaftaran/tahapkelima/'.$dataspt->t_uuidspt.'').'">TAHAP 5</a>';
$link_tahap6_up = '<a href="'.url('pendaftaran/tahapkeenam/'.$dataspt->t_uuidspt.'').'">TAHAP 6</a>';
$link_tahap7_up = '<a href="'.url('pendaftaran/tahapketujuh/'.$dataspt->t_uuidspt.'').'">TAHAP 7</a>';
}else{
$link_tahap1 = '';
$link_tahap2 = '';
$link_tahap3 = '';
$link_tahap4 = '';
$link_tahap5 = '';
$link_tahap6 = '';
$link_tahap7 = '';

$link_tahap1_up = 'TAHAP 1';
$link_tahap2_up = 'TAHAP 2';
$link_tahap3_up = 'TAHAP 3';
$link_tahap4_up = 'TAHAP 4';
$link_tahap5_up = 'TAHAP 5';
$link_tahap6_up = 'TAHAP 6';
$link_tahap7_up = 'TAHAP 7';
}
@endphp

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">FORM PENDAFTARAN</h3>
    </div>
    <form method="post" action="{{ url('pendaftaran/simpantahapkedua') }}" id="formtambah" name="formtambah">
        <div class="col-sm-12">
            <div class="row bs-wizard">
                <div class="bs-wizard-step complete" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <div class="text-center bs-wizard-stepnum"><b style="color: #337ab7;">{!!$link_tahap1_up!!}</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap1}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"><b style="color: #337ab7;">INFORMASI PENERIMA HAK</b></div>
                </div>

                <div class="bs-wizard-step active" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum text-primary"><b>{!!$link_tahap2_up!!}</b></div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap2}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center text-primary"><b>INFORMASI OBJEK PAJAK</b></div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- complete -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap3_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap3}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center">UPLOAD FILE PERSYARATAN</div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap4_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap4}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> UPLOAD FOTO & LOKASI OBJEK</div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap5_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap5}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> PERHITUNGAN BPHTB</div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap6_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap6}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> INFORMASI PELEPAS HAK</div>
                </div>

                <div class="bs-wizard-step disabled" style="max-width: 14.2%; flex: 0 0 14.2%;">
                    <!-- active -->
                    <div class="text-center bs-wizard-stepnum">{!!$link_tahap7_up!!}</div>
                    <div class="progress">
                        <div class="progress-bar"></div>
                    </div>
                    <a href="{{$link_tahap7}}" class="bs-wizard-dot"></a>
                    <div class="bs-wizard-info text-center"> PERSETUJUAN</div>
                </div>
            </div>
        </div>


        <div class="col-md-12 mt-4">
            <div class="col-sm-12">
                <div class="col-sm-12 row">
                    <label class="col-sm-2 control-label">NOP/Tahun SPPT</label>
                    <div class="col-sm-3">
                        @csrf
                        <input type="hidden" name="t_uuidspt" id="t_uuidspt" value="{{ old('t_uuidspt') ?? $dataspt->t_uuidspt}}">
                        <input type="hidden" name="t_kodepos_penjual" id="t_kodepos_penjual" value="{{ old('t_kodepos_penjual') ?? $dataspt->t_kodepos_penjual}}">
                        <input name="t_nop_sppt" id="t_nop_sppt" class="form-control @error('t_nop_sppt') is-invalid @enderror"  value="{{ old('t_nop_sppt') ?? $dataspt->t_nop_sppt}}" type="text" placeholder="__.__.___.___.___.____._" onchange="datanop()">
                        @error('t_nop_sppt')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    @php
                    if(!empty($dataspt->t_tahun_sppt)){
                    $tahunsppt = $dataspt->t_tahun_sppt;
                    }else{
                    $tahunsppt = date('Y');
                    }
                    @endphp
                    <div class="col-sm-1">
                        <input name="t_tahun_sppt" id="t_tahun_sppt" class="form-control @error('t_tahun_sppt') is-invalid @enderror" onchange="datanop()" maxlength="4"  value="{{ old('t_tahun_sppt') ?? $tahunsppt }}" onkeypress="return numbersonly(this, event)" type="text">
                        @error('t_tahun_sppt')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <button class="btn btn-sm btn-primary btn-sm" id="tombol_proses" type="button" onclick="datanop();">
                        <i class="fa fa-fw fa-refresh"></i> Proses
                    </button>
                    <div id="myDiv" style="display:none; text-align: center;">
                        <img id="myImage"
                             src="{{asset('upload/loader.gif')}}">
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-2 control-label">Nama SPPT</label>
                    <div class="col-sm-4">
                        <input name="t_nama_sppt" id="t_nama_sppt" class="form-control @error('t_nop_sppt') is-invalid @enderror" value="{{ old('t_nama_sppt') ?? $dataspt->t_nama_sppt}}" type="text" readonly="true">
                        @error('t_nama_sppt')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-2 control-label">Letak Tanah/Bangunan</label>
                    <div class="col-sm-4">
                        <textarea name="t_jalan_sppt" id="t_jalan_sppt" class="form-control @error('t_jalan_sppt') is-invalid @enderror">{{ old('t_jalan_sppt') ?? $dataspt->t_jalan_sppt}}</textarea>
                        @error('t_jalan_sppt')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-2 control-label">RT</label>
                    <div class="col-sm-2">
                        <input name="t_rt_sppt" id="t_rt_sppt" class="form-control @error('t_rt_sppt') is-invalid @enderror" maxlength="3" value="{{ old('t_rt_sppt') ?? $dataspt->t_rt_sppt}}" onkeypress="return numbersonly(this, event)" type="text">
                        @error('t_rt_sppt')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <label class="col-sm-2 control-label">RW</label>
                    <div class="col-sm-2">
                        <input name="t_rw_sppt" id="t_rw_sppt" class="form-control @error('t_rw_sppt') is-invalid @enderror" maxlength="3" value="{{ old('t_rw_sppt') ?? $dataspt->t_rw_sppt}}" onkeypress="return numbersonly(this, event)" type="text">
                        @error('t_rw_sppt')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <label class="col-sm-2 control-label">Kelurahan/Desa</label>
                    <div class="col-sm-2">
                        <input name="t_kelurahan_sppt" id="t_kelurahan_sppt" class="form-control @error('t_kelurahan_sppt') is-invalid @enderror" value="{{ old('t_kelurahan_sppt') ?? $dataspt->t_kelurahan_sppt}}" type="text">
                        @error('t_kelurahan_sppt')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-2 control-label">Kecamatan</label>
                    <div class="col-sm-2">
                        <input name="t_kecamatan_sppt" id="t_kecamatan_sppt" class="form-control @error('t_kecamatan_sppt') is-invalid @enderror" value="{{ old('t_kecamatan_sppt') ?? $dataspt->t_kecamatan_sppt}}" type="text">
                        @error('t_kecamatan_sppt')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <label class="col-sm-2 control-label"><span>Kab/Kota</span></label>
                    <div class="col-sm-2">
                        <input name="t_kabkota_sppt" id="t_kabkota_sppt" class="form-control @error('t_kabkota_sppt') is-invalid @enderror" readonly="readonly" value="{{ old('t_kabkota_sppt') ?? $dataspt->t_kabkota_sppt}}" type="text">
                        @error('t_kabkota_sppt')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12 row mt-4">
                    <label class="col-sm-2 control-label">Luas Tanah (Bumi)</label>
                    <div class="col-sm-2">
                        <input name="t_luastanah" id="t_luastanah" class="form-control @error('t_luastanah') is-invalid @enderror"
                               value="{{ old('t_luastanah') ?? $dataspt->t_luastanah}}"
                               onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                               onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                               onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                               onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();"
                               onkeypress="return numbersonly(this, event); hitungpajakbphtb();" type="text" style="text-align: right">
                        <input name="t_luastanah_sismiop" id="t_luastanah_sismiop" value="{{ old('t_luastanah_sismiop') ?? $dataspt->t_luastanah_sismiop}}" type="hidden">
                        <input name="t_luasbangunan_sismiop" id="t_luasbangunan_sismiop" value="{{ old('t_luasbangunan_sismiop') ?? $dataspt->t_luasbangunan_sismiop}}" type="hidden">
                        <input name="t_njoptanah_sismiop" id="t_njoptanah_sismiop" value="{{ old('t_njoptanah_sismiop') ?? $dataspt->t_njoptanah_sismiop}}" type="hidden">
                        <input name="t_njopbangunan_sismiop" id="t_njopbangunan_sismiop" value="{{ old('t_njopbangunan_sismiop') ?? $dataspt->t_njopbangunan_sismiop}}" type="hidden">
                        <input name="t_idtarifbphtb" id="t_idtarifbphtb" value="{{ old('t_idtarifbphtb') ?? $dataspt->t_idtarifbphtb}}" type="hidden">
                        @error('t_luastanah')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <label class="col-sm-2 control-label">Nilai Pasar/NJOP (m2)</label>
                    <div class="col-sm-2">
                        <input name="t_njoptanah" id="t_njoptanah" class="form-control @error('t_njoptanah') is-invalid @enderror"
                               value="{{ old('t_njoptanah') ?? number_format($dataspt->t_njoptanah, 0, ',', '.')}}" style="text-align:right;"
                               onkeyup="this.value = formatCurrency(this.value);"
                               onchange="this.value = formatCurrency(this.value);"
                               onblur="this.value = formatCurrency(this.value);"
                               onfocus="this.value = unformatCurrency(this.value)"
                               onkeypress="return numbersonly(this, event)" type="text" readonly="true">
                        @error('t_njoptanah')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <label class="col-sm-1 control-label">Total</label>
                    <div class="col-sm-3">
                        <input name="t_totalnjoptanah" id="t_totalnjoptanah" class="form-control @error('t_totalnjoptanah') is-invalid @enderror" readonly="readonly"
                               style="text-align:right" value="{{ old('t_totalnjoptanah') ?? number_format($dataspt->t_totalnjoptanah, 0, ',', '.')}}"
                               type="text">
                        @error('t_totalnjoptanah')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-2 control-label">Luas Bangunan</label>
                    <div class="col-sm-2">
                        <input name="t_luasbangunan" id="t_luasbangunan" class="form-control @error('t_luasbangunan') is-invalid @enderror"
                               value="{{ old('t_luasbangunan') ?? $dataspt->t_luasbangunan}}"
                               onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                               onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                               onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                               onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();"
                               onkeypress="return numbersonly(this, event)" type="text" style="text-align: right">
                        @error('t_luasbangunan')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <label class="col-sm-2 control-label">Nilai Pasar/NJOP (m2)</label>
                    <div class="col-sm-2">
                        <input name="t_njopbangunan" id="t_njopbangunan" class="form-control @error('t_njopbangunan') is-invalid @enderror"
                               value="{{ old('t_njopbangunan') ?? number_format($dataspt->t_njopbangunan, 0, ',', '.')}}" style="text-align:right"
                               onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                               onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                               onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                               onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();"
                               onkeypress="return numbersonly(this, event)" type="text">
                        @error('t_njopbangunan')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <label class="col-sm-1 control-label">Total</label>
                    <div class="col-sm-3">
                        <input name="t_totalnjopbangunan" id="t_totalnjopbangunan" class="form-control @error('t_totalnjopbangunan') is-invalid @enderror"
                               readonly="readonly" style="text-align:right"
                               value="{{ old('t_totalnjopbangunan') ?? number_format($dataspt->t_totalnjopbangunan, 0, ',', '.')}}" type="text">
                        @error('t_totalnjopbangunan')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-9 control-label" style="text-align: right;">NJOP PBB</label>
                    <div class="col-sm-3">
                        <input name="t_grandtotalnjop" id="t_grandtotalnjop" class="form-control @error('t_grandtotalnjop') is-invalid @enderror" readonly="readonly"
                               style="text-align:right" value="{{ old('t_grandtotalnjop') ?? number_format($dataspt->t_grandtotalnjop, 0, ',', '.')}}"
                               type="text">
                        @error('t_grandtotalnjop')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div style="display: none;" class="col-sm-12 row mt-2" id="pembagian_aphb_1">
                    <div style="display: none;" id="pembagian_aphb_2">
                        <label class="col-sm-9 control-label" id="pembagian_aphb_3"
                               style="text-align: right; display: none;">Pembagian APHB</label>
                        <div style="display: none;" class="col-sm-3" id="pembagian_aphb_4">
                            <input style="text-align: right; width: 50px; display: none;"
                                   name="t_tarif_pembagian_aphb_kali" id="t_tarif_pembagian_aphb_kali"
                                   value="{{ old('t_tarif_pembagian_aphb_kali') ?? $dataspt->t_tarif_pembagian_aphb_kali}}"
                                   onkeyup="this.value = unformatCurrency(this.value);"
                                   onchange="this.value = unformatCurrency(this.value);"
                                   onblur="this.value = unformatCurrency(this.value);"
                                   onfocus="this.value = unformatCurrency(this.value)"
                                   onkeypress="return numbersonly(this, event)" type="text">
                            /
                            <input style="text-align: right; width: 50px; display: none;"
                                   name="t_tarif_pembagian_aphb_bagi" id="t_tarif_pembagian_aphb_bagi"
                                   value="{{ old('t_tarif_pembagian_aphb_bagi') ?? $dataspt->t_tarif_pembagian_aphb_bagi}}"
                                   onkeyup="this.value = unformatCurrency(this.value);"
                                   onchange="this.value = unformatCurrency(this.value);"
                                   onblur="this.value = unformatCurrency(this.value);"
                                   onfocus="this.value = unformatCurrency(this.value)"
                                   onkeypress="return numbersonly(this, event)" type="text">
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-9 control-label" style="text-align: right;">Harga Transaksi / Nilai Pasar</label>
                    <div class="col-sm-3">
                        <input name="t_nilaitransaksispt" id="t_nilaitransaksispt" class="form-control @error('t_nilaitransaksispt') is-invalid @enderror" style="text-align:right" value="{{ old('t_nilaitransaksispt') ?? number_format($dataspt->t_nilaitransaksispt, 0, ',', '.')}}"
                               onkeyup="this.value = formatCurrency(this.value);"
                               onfocus="this.value = unformatCurrency(this.value)"
                               onkeypress="return numbersonly(this, event)"
                               onchange="this.value = formatCurrency(this.value);"
                               onblur="this.value = formatCurrency(this.value);" type="text">
                        @error('t_nilaitransaksispt')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-2 control-label">BPHTB Tidak Kena Pajak</label>
                    <div class="col-sm-6">
                        <select name="t_idjenisfasilitas" id="t_idjenisfasilitas" class="form-control @error('t_idjenisfasilitas') is-invalid @enderror">
                            <option value="">Silahkan Pilih</option>
                            @php
                                $no=1;
                            @endphp
                            @foreach ($data_jenisfasilitas as $ht)
                            <option {{ old('t_idjenisfasilitas') == $ht->s_idjenisfasilitas ? 'selected' : '' }} value="{{ $ht->s_idjenisfasilitas }}"
                                {{ $dataspt->t_idjenisfasilitas == $ht->s_idjenisfasilitas ? 'selected' : '' }}>
                                {{ sprintf("%02d",$no++) }} || {{ $ht->s_namajenisfasilitas }}</option>
                            @endforeach
                        </select>
                        @error('t_idjenisfasilitas')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-2 control-label">Kepemilikan</label>
                    <div class="col-sm-4">
                        <select name="t_idjenishaktanah" id="t_idjenishaktanah" class="form-control @error('t_idjenishaktanah') is-invalid @enderror">
                            <option value="">Silahkan Pilih</option>
                            @foreach ($data_jenishaktanah as $ht)
                            <option {{ old('t_idjenishaktanah') == $ht->s_idhaktanah ? 'selected' : '' }} value="{{ $ht->s_idhaktanah }}"
                                {{ $dataspt->t_idjenishaktanah == $ht->s_idhaktanah ? 'selected' : '' }}>
                                {{ $ht->s_namahaktanah }}</option>
                            @endforeach
                        </select>
                        @error('t_idjenishaktanah')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-2 control-label">Jenis Tanah</label>
                    <div class="col-sm-4">
                        <select name="t_id_jenistanah" id="t_id_jenistanah" class="form-control @error('t_id_jenistanah') is-invalid @enderror" >
                            <option value="">Silahkan Pilih</option>
                            @foreach ($data_jenisperumahantanah as $dp)
                            <option {{ old('t_id_jenistanah') == $dp->id_jenistanah ? 'selected' : '' }} value="{{ $dp->id_jenistanah }}"
                                {{ $dataspt->t_id_jenistanah == $dp->id_jenistanah ? 'selected' : '' }}>
                                {{ $dp->nama_jenistanah }}</option>
                            @endforeach
                        </select>
                        @error('t_id_jenistanah')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                </div>
                <div class="col-sm-12 row mt-2">
                    <label class="col-sm-2 control-label">Jenis Dok. Tanah</label>
                    <div class="col-sm-4">
                        <select name="t_idjenisdoktanah" id="t_idjenisdoktanah" class="form-control @error('t_idjenisdoktanah') is-invalid @enderror">
                            <option value="">Silahkan Pilih</option>
                            @foreach ($data_jenisdoktanah as $dt)
                            <option {{ old('t_idjenisdoktanah') == $dt->s_iddoktanah ? 'selected' : '' }} value="{{ $dt->s_iddoktanah }}"
                                {{ $dataspt->t_idjenisdoktanah == $dt->s_iddoktanah ? 'selected' : '' }}>
                                {{ $dt->s_namadoktanah }}</option>
                            @endforeach
                        </select>
                        @error('t_idjenisdoktanah')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 control-label">No. Dokumen Tanah</label>
                        <div class="col-sm-4">
                            <input name="t_nosertifikathaktanah" id="t_nosertifikathaktanah" class="form-control @error('t_nosertifikathaktanah') is-invalid @enderror"  value="{{ old('t_nosertifikathaktanah') ?? $dataspt->t_nosertifikathaktanah }}" type="text">
                            @error('t_nosertifikathaktanah')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        
                    </div>
                    <div class="col-sm-12 row mt-2">
                        
                        <label class="col-sm-2 control-label">Tanggal Dokumen</label>
                        @if(!empty($dataspt->t_tgldok_tanah))
                        @php $tgldok = date('d/m/Y', strtotime($dataspt->t_tgldok_tanah)); @endphp
                        @else
                        @php $tgldok = ''; @endphp
                        @endif
                        <div class="col-sm-4">
                            <input type="text" name="t_tgldok_tanah" id="t_tgldok_tanah" class="form-control @error('t_tgldok_tanah') is-invalid @enderror"  value="{{ old('t_tgldok_tanah') ?? $tgldok }}">
                            @error('t_tgldok_tanah')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                    </div>
                    <br><br>&nbsp;<div class="col-sm-12 row mt-2"></div>
                </div>
            </div>
            <div class="card-footer ">
                <a href="{{ url('pendaftaran') }}/tahappertama/{{ $dataspt->t_uuidspt}}" class="btn btn-outline-primary prevBtn pull-left" type="button"><i class="fa fa-arrow-circle-left"></i>SEBELUMNYA</a>
                <button id="pendaftaranbutton" type="submit" class="btn btn-primary pull-right"> SELANJUTNYA <i class="fa fa-arrow-circle-right"></i></button>
            </div>
    </form>
</div>

<div class="modal fade bd-example-modal-lg" id="loadingsimpanvalidasi" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="col-12">
                <span class="spinner-grow text-muted"></span>
                <span class="spinner-grow text-primary"></span>
                <span class="spinner-grow text-success"></span>
                <span class="spinner-grow text-info"></span>
                <span class="spinner-grow text-warning"></span>
                <span class="spinner-grow text-danger"></span>
                <span class="spinner-grow text-secondary"></span>
                <span class="spinner-grow text-dark"></span>
            </div>
        </div>
    </div>
</div>
        
<!-- Modal Tunggakan PBB -->
<div class="modal fade" id="ModalTunggakanPBB" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h4 class="modal-title">Daftar Tunggakan PBB</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="datatunggakan"></div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="tutupmodaltunggakan()">Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="peringataninput" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog">
        <div class="modal-content bg-danger">
            <div class="modal-header">
                <h4 class="modal-title">PERINGATAN</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-sm-12 row"><center><label id="isianperingatan" style="font-size: 20px; color: white;"></label></center></div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-outline-light" data-dismiss="modal" onclick="tutupmodalperingataninput()">Tutup</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scriptsbawah')
<script type="text/javascript">
    jQuery('#t_tgldok_tanah').datepicker({
        format: 'dd/mm/yyyy',
        language: 'id'
    });

    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-Token': '{{ csrf_token() }}'}
        });
        
        <?php $cek_menukonekpbb = (new \ComboHelper())->cek_setkonekpbb(); ?>
                @if($cek_menukonekpbb->s_id_statuskonekpbb == 1)
                    
                    document.getElementById('t_kabkota_sppt').setAttribute('readonly', true);
                    document.getElementById('t_njoptanah').setAttribute('readonly', true);
                    document.getElementById('t_nama_sppt').setAttribute('readonly', true);
                    
                    document.getElementById("tombol_proses").style.display = "block";
                    
                @else    
                    
                    document.getElementById('t_kabkota_sppt').removeAttribute('readonly');
                    document.getElementById('t_njoptanah').removeAttribute('readonly');
                    document.getElementById('t_nama_sppt').removeAttribute('readonly');
                    
                    document.getElementById("tombol_proses").style.display = "none";
                @endif        
        
        
            @if(!empty($dataspt->t_grandtotalnjop) || !empty($dataspt->t_nama_sppt) || !empty($dataspt->t_nama_sppt) || !empty($dataspt->t_totalnjoptanah))
                //showlanjut();
            @else
                //hidelanjut();
            @endif
            
    });

    $('#t_nop_sppt').mask("00.00.000.000.000.0000.0", {
        placeholder: "__.__.___.___.___.____._"
    });

    $('#t_tgldok_tanah').mask("00/00/0000", {
        placeholder: "__/__/____"
    });

    $('#pendaftaranbutton').click(function () {
        var myForm = document.getElementById('formtambah');
        myForm.onsubmit = function () {
            var allInputs = myForm.getElementsByTagName('input');
            var input, i;

            for (i = 0; input = allInputs[i]; i++) {
                if (input.getAttribute('name') && !input.value) {
                    //input.setAttribute('name', '');

                } else {
                    jQuery('#loadingsimpanvalidasi').modal('show');
                }
            }
        };
    });

    function showimg() {
        document.getElementById("myDiv").style.display = "block";
    }

    function hideimg() {
        document.getElementById("myDiv").style.display = "none";
    }

    function bukamodalperingataninput() {
        jQuery('#peringataninput').modal('show');
    }

    function tutupmodalperingataninput() {
        jQuery('#peringataninput').modal('hide');
    }

    function datanop() {
        if ($('#t_nop_sppt').val() != '') {
            
            <?php $cek_menukonekpbb = (new \ComboHelper())->cek_setkonekpbb(); ?>
                @if($cek_menukonekpbb->s_id_statuskonekpbb == 1)
                    
                    showimg();
                    $.post('<?= url('pendaftaran') ?>/datanop', {
                        t_nop_sppt: $('#t_nop_sppt').val(),
                        t_tahun_sppt: $('#t_tahun_sppt').val()
                    }, function (data) {
                        var aa = (data);
                        $('#t_nama_sppt').val(aa.nm_wp_sppt);
                        $('#t_jalan_sppt').val(aa.jln_wp_sppt);
                        $('#t_rt_sppt').val(aa.rt_wp_sppt);
                        $('#t_rw_sppt').val(aa.rw_wp_sppt);
                        $('#t_kelurahan_sppt').val(aa.nm_kelurahan);
                        $('#t_kecamatan_sppt').val(aa.nm_kecamatan);
                        $('#t_kabkota_sppt').val(aa.nm_dati2);
                        $('#t_luastanah').val(currencyFormatDE(aa.luas_bumi_sppt * 1));
                        $('#t_luastanah_sismiop').val(aa.luas_bumi_sppt * 1);
                        $('#t_luasbangunan_sismiop').val(aa.luas_bng_sppt * 1);
                        $('#t_njoptanah_sismiop').val(aa.nilai_per_m2_tanah * 1000);
                        $('#t_njopbangunan_sismiop').val(aa.nilai_per_m2_bng * 1000);
                        $('#t_njoptanah').val(currencyFormatDE(aa.nilai_per_m2_tanah * 1000));
                        $('#t_totalnjoptanah').val(currencyFormatDE(aa.njop_bumi_sppt * 1));
                        $('#t_luasbangunan').val(currencyFormatDE(aa.luas_bng_sppt * 1));
                        $('#t_njopbangunan').val(currencyFormatDE(aa.nilai_per_m2_bng * 1000));
                        $('#t_totalnjopbangunan').val(currencyFormatDE(aa.njop_bng_sppt * 1));
                        $('#t_grandtotalnjop').val(currencyFormatDE(aa.njop_sppt * 1));
                        $('#t_kodepos_penjual').val((aa.kd_pos_wp_sppt));
                        hideimg();
                        cektunggakanpbb();
                    });
                    document.getElementById('t_kabkota_sppt').setAttribute('readonly', true);
                    document.getElementById('t_njoptanah').setAttribute('readonly', true);
                    document.getElementById('t_nama_sppt').setAttribute('readonly', true);
                @else    
                    
                    document.getElementById('t_kabkota_sppt').removeAttribute('readonly');
                    document.getElementById('t_njoptanah').removeAttribute('readonly');
                    document.getElementById('t_nama_sppt').removeAttribute('readonly');
                @endif    
        } else {
            $('#isianperingatan').html("NOP HARUS DI ISI!!");
            bukamodalperingataninput();
        }
    }

    function cektunggakanpbb() {
        $.post('<?= url('pendaftaran') ?>/cektunggakanpbb', {t_nop_sppt: $('#t_nop_sppt').val()}, function (data) {
            var aa = (data);
            if (aa.PBB_YG_HARUS_DIBAYAR_SPPT != '') {
                jQuery('#ModalTunggakanPBB').modal('show');
                $('#datatunggakan').html(aa.datatunggakan);
                
                <?php $cek_menutunggakanpbb = (new \ComboHelper())->cek_setmenutunggakanpbb(); ?>
                @if($cek_menutunggakanpbb->s_id_statustgkpbb == 1)
                
                    @if($session->s_id_hakakses == 1 || $session->s_id_hakakses == 14 || $session->s_id_hakakses == 13) 
                        showlanjut();
                    @else
                        hidelanjut();
                    @endif
                @endif    
            } else {
                alert('Tidak Ada Tunggakan');
                showlanjut();
            }
        });
    }
    
    
    function showlanjut() {
        $('#pendaftaranbutton').show();
        $('#kembali-tr').hide();
    }

    function hidelanjut() {
        $('#pendaftaranbutton').hide();
        $('#kembali-tr').show();
    }
    
    function tutupmodaltunggakan() {
        jQuery('#ModalTunggakanPBB').modal('hide');
    }


    function hitungpajakbphtb() {
        var t_luastanah = unformatCurrency($('#t_luastanah').val());
        var t_njoptanah = unformatCurrency($('#t_njoptanah').val());
        if (t_luastanah == '') {
            var t_luastanahnya = 0;
        } else {
            var t_luastanahnya = t_luastanah;
        }

        if (t_njoptanah == '') {
            var t_njoptanahnya = 0;
        } else {
            var t_njoptanahnya = t_njoptanah;
        }

        var t_totalnjoptanah = Math.ceil(t_luastanahnya * t_njoptanahnya);
        $('#t_totalnjoptanah').val(t_totalnjoptanah);
        document.getElementById('t_totalnjoptanah').value = formatCurrency(document.getElementById('t_totalnjoptanah').value);

        var t_luasbangunan = unformatCurrency($('#t_luasbangunan').val());
        var t_njopbangunan = unformatCurrency($('#t_njopbangunan').val());
        if (t_luasbangunan == '') {
            var t_luasbangunannya = 0;
        } else {
            var t_luasbangunannya = t_luasbangunan;
        }

        if (t_njopbangunan == '') {
            var t_njopbangunannya = 0;
        } else {
            var t_njopbangunannya = t_njopbangunan;
        }

        var t_totalnjopbangunan = Math.ceil(t_luasbangunannya * t_njopbangunannya);

        $('#t_totalnjopbangunan').val(t_totalnjopbangunan);
        document.getElementById('t_totalnjopbangunan').value = formatCurrency(document.getElementById('t_totalnjopbangunan').value);
        var t_grandtotalnjop = t_totalnjoptanah + t_totalnjopbangunan;
        $('#t_grandtotalnjop').val(t_grandtotalnjop);
        document.getElementById('t_grandtotalnjop').value = formatCurrency(document.getElementById('t_grandtotalnjop').value);
        
        /*var nama_sppt = $('#t_nama_sppt').val();
        if(t_grandtotalnjop == '' || nama_sppt == '' || t_totalnjoptanah == ''){
            hidelanjut();
        }else{
            showlanjut();
        }*/
        
        
    }
</script>
@endpush
