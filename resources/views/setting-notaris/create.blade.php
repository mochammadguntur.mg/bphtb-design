@extends('layouts.master')

@section('judulkiri')
PENGATURAN PPAT / PPATS [TAMBAH]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="{{ url('setting-notaris/store') }}" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @csrf
                        @include('setting-notaris.partials.form', ['sumbit'=> 'Simpan'])
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
