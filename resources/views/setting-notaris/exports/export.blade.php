

<table style="border-collapse: collapse; width:100%">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">No</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Nama PPAT/PPATS</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Kode PPAT/PPATS</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Status</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Created at</th>
        </tr>
    </thead>
    <tbody>
        @foreach($notariss as $index => $rows)
        {{ error_reporting(0) }}
        <tr>
            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center;">{{ $index + 1 }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center;">{{ $rows->s_namanotaris }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center;">{{ $rows->s_kodenotaris }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center;">{{ $rows->s_statusnotaris }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center;">{{ $rows->created_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
