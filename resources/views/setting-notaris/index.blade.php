@extends('layouts.master')

@section('judulkiri')
PENGATURAN PPAT / PPATS
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="setting-notaris/create" class="btn btn-primary">
                    <i class="fas fa-plus"></i> Tambah
                    </a>
                </h3>
                <div class="card-tools">
                    <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                        <i class="fas fa-file-excel"></i> Excel
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger"  id="btnPDF">
                        <i class="fas fa-file-pdf"></i> PDF
                    </a>
                </div>
            </div>
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Nama PPAT / PPATS</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Kode PPAT / PPATS</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Status</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Create At</th>
                                <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-namanotaris">
                                </th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-kode">
                                </th>
                                <th>
                                    <select class="form-control form-control-sm" id="filter-status">
                                        <option value="">All</option>
                                        <option value="1">Aktif</option>
                                        <option value="2">Tidak Aktif</option>
                                    </select>
                                </th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-created_at" readonly>
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="7"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Kode PPAT / PPATS</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="kodeHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Nama PPAT / PPATS</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namaHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>


@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif

        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
        url: 'setting-notaris/datagrid',
        table: "#datagrid-table",
        columns: [
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "s_namanotaris"
            },
            {
                sortable: true,
                name: "s_kodenotaris"
            },
            {
                sortable: true,
                name: "s_statusnotaris"
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]

    });

    $("#filter-namanotaris, #filter-kode").keyup(function() {
        search();
    });

    $("#filter-status, #filter-created_at").change(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            namanotaris: $("#filter-namanotaris").val(),
            kode: $("#filter-kode").val(),
            status: $("#filter-status").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'setting-notaris/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].s_idnotaris);
            $("#kodeHapus").val(data[0].s_kodenotaris);
            $("#namaHapus").val(data[0].s_namanotaris);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'setting-notaris/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Berhasil dihapus!')
            datatables.reload();
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        var namanotaris = $("#filter-namanotaris").val();
        var kodenotaris = $("#filter-kode").val();
        var status = $("#filter-status").val();
        var created_at = $("#filter-created_at").val();
        if(a.target.id == 'btnExcel'){
            window.open('{{ url('setting-notaris/export_xls') }}?namanotaris=' + namanotaris + '&kodenotaris=' + kodenotaris + '&created_at=' + created_at);
        }else{
            window.open('{{ url('setting-notaris/export_pdf') }}?namanotaris=' + namanotaris + '&kodenotaris=' + kodenotaris + '&created_at=' + created_at);
        }

    });

    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").slideUp(500);
    });

</script>
@endpush
