<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode PPAT / PPATS</label>
    <div class="col-md-4">
        <input type="text" name="s_kodenotaris" class="form-control @error('s_kodenotaris') is-invalid @enderror" id="s_kodenotaris" placeholder="Kode Notaris" value="{{ old('s_kodenotaris') ?? $notaris->s_kodenotaris }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kodenotaris'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_kodenotaris') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Nama PPAT / PPATS</label>
    <div class="col-md-4">
        <input type="text" name="s_namanotaris" class="form-control @error('s_namanotaris') is-invalid @enderror" id="s_namanotaris" placeholder="Nama Notaris" value="{{ old('s_namanotaris') ?? $notaris->s_namanotaris }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_namanotaris'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_namanotaris') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Alamat PPAT / PPATS</label>
    <div class="col-md-4">
        <input type="text" name="s_alamatnotaris" class="form-control @error('s_alamatnotaris') is-invalid @enderror" id="s_alamatnotaris" placeholder="Alamat Notaris" value="{{ old('s_alamatnotaris') ?? $notaris->s_alamatnotaris }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_alamatnotaris'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_alamatnotaris') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">SK PPAT / PPATS</label>
    <div class="col-md-4">
        <input type="text" name="s_sknotaris" class="form-control @error('s_sknotaris') is-invalid @enderror" id="s_sknotaris" placeholder="SK Notaris" value="{{ old('s_sknotaris') ?? $notaris->s_sknotaris }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_sknotaris'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_sknotaris') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Status</label>
    <div class="col-md-4">
        <select name="s_statusnotaris" class="form-control @error('s_statusnotaris') is-invalid @enderror" id="s_statusnotaris">
            <option value="">Silahkan Pilih</option>
            @foreach ($status as $status)
            <option {{ $status->s_id_status == $notaris->s_statusnotaris ? 'selected' : '' }} value="{{ $status->s_id_status }}">{{ $status->s_nama_status }}
            </option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_statusnotaris'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_statusnotaris') }}
        </div>
        @endif
    </div>
</div>

<a href="/setting-notaris" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>
