<table class="table" style="width: 100%;border-bottom:4px double #000;">
    <thead>
        <tr>
            <td style="width: 10%">
                <img src="{{ public_path().'/storage/'. $pemda->s_logo }}" width="10%">
            </td>
            <td style="text-align:center;"><strong style="font-size: 14pt">PEMERINTAH {{ strtoupper($pemda->s_namakabkot) }}</strong><br>
            <strong style="font-size: 16pt">{{ strtoupper($pemda->s_namainstansi) }}</strong><br>
            <span style="font-size: 12pt">{{ $pemda->s_alamatinstansi .' Kode Pos '. $pemda->s_kodepos }}<br>No. Telp {{ $pemda->s_notelinstansi }}</span></td>
            <td style="width: 10%"></td>
        </tr>
    </thead>
</table>
