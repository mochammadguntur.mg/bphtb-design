<nav class="main-header navbar navbar-expand navbar-white navbar-light border-bottom-0 elevation-3">
    <ul class="navbar-nav">
        <li class="nav-item p-1">
            <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
        </li>
    </ul>
    <ul class="navbar-nav ml-auto">

        @guest
        <li class="nav-item">
            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
        </li>
        @if (Route::has('register'))
        <li class="nav-item">
            <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
        </li>
        @endif
        @else
        <li class="nav-item dropdown">
            {{-- <a class="nav-link" data-toggle="dropdown" href="#">
                
            </a> --}}
            <div class="btn btn-sm w-auto btn-clean d-flex align-items-center px-2" data-toggle="dropdown">
                <span class="text-muted font-weight-bold font-size-base d-none d-md-inline mr-1">Hai,</span>
                <span class="text-dark-50 font-weight-bolder font-size-base d-none d-md-inline mr-3">{{ Auth::user()->name }}</span>
                <img src="{{ asset('dist/img/user1-128x128.jpg') }}" class="img-rounded elevation-3"
                    style="height: 40px; width: 40px; object-fit: scale-down;" alt="User Image">
            </div>
            <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right">
                <div class="p-3 elevation-3">
                    <div class="d-flex justify-content-start">
                        <img src="{{ asset('dist/img/user1-128x128.jpg') }}" class="img-rounded elevation-3"
                            style="height: 90px; width: 90px; object-fit: scale-down;" alt="User Image">
                        <div class="px-3">
                            <form action="{{ route('logout') }}" method="POST">
                                @csrf
                                <h5 class="mb-0 text-dark-50">{{ Auth::user()->name }}</h5>
                                <h6 class="text-muted text-sm">{{ ucfirst(trans(Auth::user()->getRoleNames()->first())) }}</h6>
                                <div class="d-flex">
                                    <a href="{{ url('profile/'. Auth::user()->id .'') }}" class="btn btn-sm btn-outline-secondary mr-2">Profile</a>
                                    
                                    <a href="#" class="btn btn-sm btn-outline-danger" data-slide="true" title="Keluar" onclick="event.preventDefault(); document.getElementById('logout-form').submit();" style="color: tomato;">
                                        Keluar
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                                
                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </li>
        @endguest  
    </ul>
</nav>
