<aside class="main-sidebar sidebar-light-primary">
    <a href="{{ route('home') }}" class="brand-link">
        <img src="{{ asset('storage/'. $pemda->s_logo .'') }}" alt="AdminLTE Logo" class="brand-image">
        <span class="brand-text">eBPHTB</span>
    </a>
    <div class="sidebar">
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column nav-child-indent" data-widget="treeview" role="menu"
                data-accordion="false">
                <li class="nav-item">
                    <a href="{{ route('home') }}" class="nav-link {{ request()->is('home') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-th-large"></i>
                        <p>DASHBOARD</p>
                    </a>
                </li>
                @if(auth()->user()->can('MenuPendaftaran') || auth()->user()->can('MenuPelaporanNotaris'))
                @can('MenuPendaftaran')
                <li class="nav-item">
                    <a href="{{ url('pendaftaran') }}"
                        class="nav-link {{ request()->is('pendaftaran*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-file-alt "></i>
                        <p class=" ">PENDAFTARAN
                        </p>
                    </a>
                </li>
                @endcan

                @can('MenuPelaporanNotaris')
                <li class="nav-item">
                    <a href="{{ url('pelaporan') }}"
                        class="nav-link {{ request()->is('pelaporan*') || request()->is('lapor-bulanan*') || request()->is('denda-*')  ? 'active' : '' }}">
                        <i class="nav-icon fas fa-file-archive "></i>
                        <p class=" ">PELAPORAN
                        </p>
                    </a>
                </li>
                @endcan

                @can('MenuPelaporanNotaris')
                <li class="nav-item">
                    <a href="{{ url('pelayanan') }}" class="nav-link {{ request()->is('pelayanan*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-handshake "></i>
                        <p class=" ">PELAYANAN
                        </p>
                    </a>
                </li>
                @endcan

                @can('MenuPelaporanNotaris')
                <li class="nav-item">
                    <a href="{{ url('informasi') }}" class="nav-link {{ request()->is('informasi*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-info "></i>
                        <p class=" ">INFORMASI
                        </p>
                    </a>
                </li>
                @endcan

                @endif

                @can('MenuValidasiBerkas')
                <li class="nav-item">
                    <a href="{{ url('validasi') }}" class="nav-link {{ request()->is('validasi*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-check-square "></i>
                        <p class=" ">VALIDASI
                        </p>
                    </a>
                </li>
                @endcan
            
                @can('MenuPemeriksaan')
                <li class="nav-item">
                    <a href="{{ url('pemeriksaan') }}"
                        class="nav-link{{ request()->is('pemeriksaan*') ? ' active' : '' }}">
                        <i class="nav-icon fas fa-search"></i>
                        <p>PEMERIKSAAN</p>
                    </a>
                </li>
                @endcan

                @can('MenuValidasiBerkas')
                <li class="nav-item">
                    <a href="{{ url('penetapan') }}" class="nav-link {{ request()->is('penetapan*') || request()->is('Skpdkb*') || request()->is('Skpdkbt*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-hammer "></i>
                        <p class=" ">PENETAPAN
                        </p>
                    </a>
                </li>
                @endcan

                @can('MenuPembayaran')
                <li class="nav-item">
                    <a href="{{ url('pembayaran') }}"
                        class="nav-link{{ request()->is('pembayaran*') ? ' active' : '' }}">
                        <i class="nav-icon fas fa-wallet"></i>
                        <p>PEMBAYARAN</p>
                    </a>
                </li>
                @endcan
                @can('MenuBpn')
                <li class="nav-item has-treeview  {{ request()->is('bpn*') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link  {{ request()->is('bpn*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-bank"></i>
                        <p>
                            INTEGRASI BPN
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview text-xs">
                        <li class="nav-item">
                            <a href="{{ url('bpn') }}" class="nav-link  {{ request()->is('bpn') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>CEK DATA BPN</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('bpn/databpn') }}"
                                class="nav-link  {{ request()->is('bpn/databpn') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>DATA INTEGRASI BPN</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{ url('bpn/password') }}"
                                class="nav-link  {{ request()->is('bpn/password') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>CHANGE PASSWORD</p>
                            </a>
                        </li>

                    </ul>
                </li>
                @endcan
                @can('MenuKppPratama')
                <li class="nav-item">
                    <a href="{{ url('kpp-pratama') }}"
                        class="nav-link {{ request()->is('kpp-pratama*') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-table"></i>
                        <p>KPP PRATAMA</p>
                    </a>
                </li>
                @endcan
                @can('approvedPembatalan')
                <?php $cek_setpelayanan_op = (new \ComboHelper())->cek_setpelayanan(1); ?>
                @if($cek_setpelayanan_op->s_id_statusmenupelayanan == 1)
                <li class="nav-item has-treeview {{ request()->is('approved*') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ request()->is('approved*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-file-signature"></i>
                        <p>
                            TINDAK LANJUT PELAYANAN
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview text-xs">
                        @can('approvedKeringanan')
                        <?php $cek_setpelayanan_op2 = (new \ComboHelper())->cek_setpelayanan(2); ?>
                        @if($cek_setpelayanan_op2->s_id_statusmenupelayanan == 1)
                        <li class="nav-item">
                            <a href="{{ url('approved-keringanan') }}"
                                class="nav-link {{ request()->is('approved-keringanan*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Keringanan</p>
                            </a>
                        </li>
                        @endif
                        @endcan
                        @can('approvedPembatalan')
                        <?php $cek_setpelayanan_op3 = (new \ComboHelper())->cek_setpelayanan(3); ?>
                        @if($cek_setpelayanan_op3->s_id_statusmenupelayanan == 1)
                        <li class="nav-item">
                            <a href="{{ url('approved-pembatalan') }}"
                                class="nav-link {{ request()->is('approved-pembatalan*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Pembatalan</p>
                            </a>
                        </li>
                        @endif
                        @endcan
                        @can('approvedPenundaan')
                        <?php $cek_setpelayanan_op4 = (new \ComboHelper())->cek_setpelayanan(4); ?>
                        @if($cek_setpelayanan_op4->s_id_statusmenupelayanan == 1)
                        <li class="nav-item">
                            <a href="{{ url('approved-penundaan') }}"
                                class="nav-link {{ request()->is('approved-penundaan*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Penundaan</p>
                            </a>
                        </li>
                        @endif
                        @endcan
                        @can('approvedAngsuran')
                        <?php $cek_setpelayanan_op5 = (new \ComboHelper())->cek_setpelayanan(5); ?>
                        @if($cek_setpelayanan_op5->s_id_statusmenupelayanan == 1)
                        <li class="nav-item">
                            <a href="{{ url('approved-angsuran') }}"
                                class="nav-link {{ request()->is('approved-angsuran*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Angsuran</p>
                            </a>
                        </li>
                        @endif
                        @endcan
                        @can('MenuSismiop')
                        <?php $cek_setpelayanan_op6 = (new \ComboHelper())->cek_setpelayanan(6); ?>
                        @if($cek_setpelayanan_op6->s_id_statusmenupelayanan == 1)
                        <li class="nav-item">
                            <a href="{{ url('sismiop') }}" class="nav-link">
                                <i class="nav-icon fa fa-table"></i>
                                <p>SISMIOP</p>
                            </a>
                        </li>
                        @endif
                        @endcan
                    </ul>
                </li>
                @endif
                @endcan
                @can('MenuCetakLaporan')
                <li class="nav-item">
                    <a href="{{ url('cetak-laporan') }}"
                        class="nav-link {{ request()->is('cetak-laporan*') ? 'active' : '' }}">
                        <i class="nav-icon fa fa-paperclip"></i>
                        <p>PEMBUKUAN & PELAPORAN</p>
                    </a>
                </li>
                @endcan
                @can('SettingPemda')
                <li
                    class="nav-item has-treeview {{ request()->is('setting*')  ? 'menu-open' : '' }} {{ request()->is('role*')  ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link {{ request()->is('setting*') ? 'active' : '' }}">
                        <i class="nav-icon fas fa-cogs"></i>
                        <p>
                            AKTIVITAS ADMIN
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        @can('SettingPemda')
                        <li class="nav-item">
                            <a href="{{ url('setting-pemda') }}"
                                class="nav-link {{ request()->is('setting-pemda') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Pemerintah Daerah</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingKecamatan')
                        <li class="nav-item">
                            <a href="{{ url('setting-kecamatan') }}"
                                class="nav-link {{ request()->is('setting-kecamatan*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Kecamatan</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingKelurahan')
                        <li class="nav-item">
                            <a href="{{ url('setting-kelurahan') }}"
                                class="nav-link {{ request()->is('setting-kelurahan*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Kelurahan</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingPejabat')
                        <li class="nav-item">
                            <a href="{{ url('setting-pejabat') }}"
                                class="nav-link {{ request()->is('setting-pejabat*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Pejabat</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingNotaris')
                        <li class="nav-item">
                            <a href="{{ url('setting-notaris') }}"
                                class="nav-link {{ request()->is('setting-notaris*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Pejabat PPAT / PPATS</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingJenisTransaksi')
                        <li class="nav-item">
                            <a href="{{ url('setting-jenis-transaksi') }}"
                                class="nav-link {{ request()->is('setting-jenis-transaksi*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Jenis Transaksi</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingPersyaratan')
                        <li class="nav-item">
                            <a href="{{ url('setting-persyaratan') }}"
                                class="nav-link {{ request()->is('setting-persyaratan*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Persyaratan Transaksi</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingHakTanah')
                        <li class="nav-item">
                            <a href="{{ url('setting-hak-tanah') }}"
                                class="nav-link {{ request()->is('setting-hak-tanah*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Hak Tanah</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingTarifBphtb')
                        <li class="nav-item">
                            <a href="{{ url('setting-target') }}"
                                class="nav-link{{ request()->is('setting-target*') ? ' active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Target Penerimaan</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingTarifBphtb')
                        <li class="nav-item">
                            <a href="{{ url('setting-tarif-bphtb') }}"
                                class="nav-link{{ request()->is('setting-tarif-bphtb*') ? ' active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Tarif BPHTB</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingTempo')
                        <li class="nav-item">
                            <a href="{{ url('setting-tempo') }}"
                                class="nav-link{{ request()->is('setting-tempo*') ? ' active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Tempo Kode Bayar</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingJenisFasilitas')
                        <li class="nav-item">
                            <a href="{{ url('setting-jenis-fasilitas') }}"
                                class="nav-link{{ request()->is('setting-jenis-fasilitas*') ? ' active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Jenis Fasilitas</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingTarifNpoptkp')
                        <li class="nav-item">
                            <a href="{{ url('setting-tarif-npoptkp') }}"
                                class="nav-link {{ request()->is('setting-tarif-npoptkp*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Tarif NPOPTKP</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingHargaAcuan')
                        <li class="nav-item">
                            <a href="{{ url('setting-harga-acuan') }}"
                                class="nav-link {{ request()->is('setting-harga-acuan*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Harga Acuan</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingPersentasewajar')
                        <li class="nav-item">
                            <a href="{{ url('setting-persentasewajar') }}"
                                class="nav-link {{ request()->is('setting-persentasewajar*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Persentase Wajar</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingAcuanTanah')
                        <li class="nav-item">
                            <a href="{{ url('setting-acuan-tanah') }}"
                                class="nav-link {{ request()->is('setting-acuan-tanah*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Harga Acuan Jenis Tanah</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingJenisDokumenTanah')
                        <li class="nav-item">
                            <a href="{{ url('setting-jenis-dokumen-tanah') }}"
                                class="nav-link {{ request()->is('setting-jenis-dokumen-tanah*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Jenis Dokumen Tanah</p>
                            </a>
                        </li>
                        @endcan
                        @can('SettingLoginBackground')
                        <li class="nav-item">
                            <a href="{{ url('setting-login-background') }}"
                                class="nav-link {{ request()->is('setting-login-background*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Login Background</p>
                            </a>
                        </li>
                        @endcan

                        @can('SettingBriva')
                        <?php $cek_menubriva = (new \ComboHelper())->cek_setmenubriva(); ?>
                        @if($cek_menubriva->s_id_statusmenubriva == 1)
                        <li class="nav-item">
                            <a href="{{ url('setting-briva') }}"
                                class="nav-link {{ request()->is('setting-briva*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>BRIVA</p>
                            </a>
                        </li>
                        @endif
                        @endcan
                        @can('SettingUser')
                        <li class="nav-item">
                            <a href="{{ url('setting-user/index') }}"
                                class="nav-link {{ request()->is('setting-user*') ? 'active' : '' }}">
                                <i class="fa fa-circle-o nav-icon"></i>
                                <p>Users</p>
                            </a>
                        </li>
                        @endcan


                        @can('SettingMenukaban','SettingMenutunggakanpbb','SettingMenutunggakantahunpbb','SettingMenubriva','SettingMenukonekpbb','SettingMenuupload','SettingMenugmap','SettingMenugmap','SettingMenupelayanan')
                        <li
                            class="nav-item has-treeview {{ request()->is('setting-menukaban*','setting-menutunggakanpbb*','setting-menutunggakantahunpbb*','setting-menutunggakantahunpbb*','setting-menubriva*','setting-menukonekpbb*','setting-menuupload*','setting-menugmap*','setting-menucaptcha*','setting-menupelayanan*') ? 'menu-open' : '' }}">
                            <a href="#"
                                class="nav-link {{ request()->is('setting-menukaban*','setting-menutunggakanpbb*','setting-menutunggakantahunpbb*','setting-menutunggakantahunpbb*','setting-menubriva*','setting-menukonekpbb*','setting-menuupload*','setting-menugmap*','setting-menucaptcha*','setting-menupelayanan*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-cogs"></i>
                                <p>
                                    MENU BANTUAN
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">

                                @can('SettingMenukaban')

                                <li class="nav-item">
                                    <a href="{{ url('setting-menukaban') }}"
                                        class="nav-link {{ request()->is('setting-menukaban*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Menu Kaban</p>
                                    </a>
                                </li>

                                @endcan
                                @can('SettingMenutunggakanpbb')

                                <li class="nav-item">
                                    <a href="{{ url('setting-menutunggakanpbb') }}"
                                        class="nav-link {{ request()->is('setting-menutunggakanpbb*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Menu Tunggakan PBB</p>
                                    </a>
                                </li>

                                @endcan
                                @can('SettingMenutunggakantahunpbb')

                                <li class="nav-item">
                                    <a href="{{ url('setting-menutunggakantahunpbb') }}"
                                        class="nav-link {{ request()->is('setting-menutunggakantahunpbb*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Set Tahun Tunggakan PBB</p>
                                    </a>
                                </li>

                                @endcan
                                @can('SettingMenubriva')

                                <li class="nav-item">
                                    <a href="{{ url('setting-menubriva') }}"
                                        class="nav-link {{ request()->is('setting-menubriva*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Menu Briva</p>
                                    </a>
                                </li>

                                @endcan
                                @can('SettingMenukonekpbb')

                                <li class="nav-item">
                                    <a href="{{ url('setting-menukonekpbb') }}"
                                        class="nav-link {{ request()->is('setting-menukonekpbb*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Menu Koneksi PBB</p>
                                    </a>
                                </li>

                                @endcan
                                @can('SettingMenuupload')

                                <li class="nav-item">
                                    <a href="{{ url('setting-menuupload') }}"
                                        class="nav-link {{ request()->is('setting-menuupload*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Ukuran Upload</p>
                                    </a>
                                </li>

                                @endcan
                                @can('SettingMenugmap')

                                <li class="nav-item">
                                    <a href="{{ url('setting-menugmap') }}"
                                        class="nav-link {{ request()->is('setting-menugmap*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Menu GMAP</p>
                                    </a>
                                </li>

                                @endcan
                                @can('SettingMenucaptcha')

                                <li class="nav-item">
                                    <a href="{{ url('setting-menucaptcha') }}"
                                        class="nav-link {{ request()->is('setting-menucaptcha*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Menu CAPTCHA</p>
                                    </a>
                                </li>

                                @endcan
                                @can('SettingMenupelayanan')

                                <li class="nav-item">
                                    <a href="{{ url('setting-menupelayanan') }}"
                                        class="nav-link {{ request()->is('setting-menupelayanan*') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Menu Pelayanan</p>
                                    </a>
                                </li>

                                @endcan
                            </ul>
                        </li>
                        @endcan


                        @can('SettingRole')
                        <li class="nav-item has-treeview {{ request()->is('role*') ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link {{ request()->is('role*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-cogs"></i>
                                <p>
                                    MASTER USER
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('SettingRole')
                                <li class="nav-item">
                                    <a href="{{ url('role-and-permission/roles') }}"
                                        class="nav-link {{ request()->is('role-and-permission/roles') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Roles</p>
                                    </a>
                                </li>
                                @endcan
                                @can('SettingPermission')
                                <li class="nav-item">
                                    <a href="{{ url('role-and-permission/permissions') }}"
                                        class="nav-link {{ request()->is('role-and-permission/permissions') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Permissions</p>
                                    </a>
                                </li>
                                @endcan
                                @can('SettingAssignPermissionToRole')
                                <li class="nav-item">
                                    <a href="{{ url('role-and-permission/assignable') }}"
                                        class="nav-link {{ request()->is('role-and-permission/assignable') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Assign Permission</p>
                                    </a>
                                </li>
                                @endcan
                                @can('SettingAssignRoleToUser')
                                <li class="nav-item">
                                    <a href="{{ url('role-and-permission/assign/user') }}"
                                        class="nav-link {{ request()->is('role-and-permission/assign/user') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>Role User</p>
                                    </a>
                                </li>
                                @endcan
                            </ul>
                        </li>
                        @endcan
                        @can('HistoryActivity')
                        <li class="nav-item has-treeview {{ request()->is('history*') ? 'menu-open' : '' }}">
                            <a href="#" class="nav-link {{ request()->is('history*') ? 'active' : '' }}">
                                <i class="nav-icon fas fa-cogs"></i>
                                <p>
                                    Activity
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview">
                                @can('HistoryActivity')
                                <li class="nav-item">
                                    <a href="{{ url('history-activity') }}"
                                        class="nav-link {{ request()->is('history-activity') ? 'active' : '' }}">
                                        <i class="fa fa-circle-o nav-icon"></i>
                                        <p>History Activity</p>
                                    </a>
                                </li>
                                @endcan
                            </ul>
                        </li>
                        @endcan
                    </ul>
                </li>
                @endcan
                @can('SettingPass')
                <li class="nav-item">
                    <a href="{{ url('ubah-pass/index') }}"
                        class="nav-link {{ request()->is('ubah-pass*') ? 'active' : '' }}">
                        <i class="fa fa-key nav-icon"></i>
                        <p>UBAH PASSWORD</p>
                    </a>
                </li>
                @endcan
            </ul>
        </nav>
    </div>
</aside>