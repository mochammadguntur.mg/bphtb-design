<title>VALIDASI BPHTB || KABUPATEN KEPAHIANG</title>
<!-- Font Awesome -->
<link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
<!-- Ionicons -->
<link rel="stylesheet" href="{{ asset('plugins/ionicons-2.0.1/css/ionicons.min.css') }}">
<link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
<style>
    body{margin: 30px;}
</style>
<body class="text-center" style="background: {{$bgValidasi}};">
    <div class="row">
        <div class="col-md-12">
            <h2 class="{{ $colorValidasi }}">
                <i class="fa {{ $iconValidasi }}"></i> {{ $statusValidasi }}
            </h2>
            {{ $tglValidasi }}
            <div class="row">
                <div class="col-sm-6 text-right">Jenis Transaksi</div>
                <div class="col-sm-6 text-left">: {{ $dataspt->s_kodejenistransaksi.'. '.$dataspt->s_namajenistransaksi }}</div>
            </div>
            @if($dataspt->t_idbidang_usaha == 1)
            <div class="row">
                <div class="col-sm-6 text-right">NIK</div>
                <div class="col-sm-6 text-left">: {{ $dataspt->t_nik_pembeli }}</div>
            </div>
            @elseif($dataspt->t_idbidang_usaha == 2)
            <div class="row">
                <div class="col-sm-6 text-right">SIUP</div>
                <div class="col-sm-6 text-left">: {{ $dataspt->t_siup_pembeli }}</div>
            </div>
            @endif
            <div class="row">
                <div class="col-sm-6 text-right">NPWP</div>
                <div class="col-sm-6 text-left">: {{ $dataspt->t_npwp_pembeli }}</div>
            </div>
            <div class="row">
                <div class="col-sm-6 text-right">Nama WP</div>
                <div class="col-sm-6 text-left">: {{ $dataspt->t_nama_pembeli }}</div>
            </div>
            <div class="row">
                <div class="col-sm-6 text-right">NOP</div>
                <div class="col-sm-6 text-left">: {{ $dataspt->t_nop_sppt }}</div>
            </div>
            <div class="row">
                <div class="col-sm-6 text-right">Tahun SPPT</div>
                <div class="col-sm-6 text-left">: {{ $dataspt->t_tahun_sppt }}</div>
            </div>
            <div class="row">
                <div class="col-sm-6 text-right">Status Bayar</div>
                @if(!empty($dataspt->t_tglpembayaran_pokok))
                <div class="col-sm-6 text-left text-green">: LUNAS Tgl. {{ date('d-m-Y H:i:s', strtotime($dataspt->t_tglpembayaran_pokok)) }}</div>
                @else
                <div class="col-sm-6 text-left text-red">: BELUM BAYAR</div>
                @endif
            </div>
            <div class="row">
                <div class="col-sm-6 text-right">Kode Bayar</div>
                <div class="col-sm-6 text-left">: {{ $dataspt->t_kodebayar_bphtb }}</div>
            </div>
        </div>
    </div>

</body>