<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title"><i class="fas fa-file"></i> PERSYARATAN</h3>
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tr>
                            <th>No</th>
                            <th>Nama Persyaratan</th>
                        </tr>
                        @php
                        $no = 1;
                        $nomer = 1;
                        $namasyarat = '';
                        @endphp
                        @foreach ($cek_persyaratan as $key => $v)
                        @if($namasyarat == '')
                        <tr>
                            <td class="text-center">{{$no}}</td>
                            <td>{{$v->s_namapersyaratan}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <a style="display:none;" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" href="{{asset($v->letak_file.$v->nama_file)}}">
                                            <img data-ng-src="{{asset($v->letak_file.$v->nama_file)}}" alt="" src="{{asset($v->letak_file.$v->nama_file)}}">
                                        </a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a data-ng-switch-when="true" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" class="ng-binding ng-scope" href="{{asset($v->letak_file.$v->nama_file)}}">
                                            <img src="{{ asset($v->letak_file . $v->nama_file) }}" style="width: 100px" alt="">
                                        </a>
                                    </li>
                                    @php
                                    $namasyarat = $v->s_namapersyaratan;
                                    $no++;
                                    @endphp
                                    @elseif($namasyarat <> $v->s_namapersyaratan)
                                </ul>
                            </td>
                        </tr>
                        <tr>
                            <td class="text-center">{{$no}}</td>
                            <td>{{$v->s_namapersyaratan}}</td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>
                                <ul class="list-group">
                                    <li class="list-group-item">
                                        <a style="display:none;" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" href="{{asset($v->letak_file.$v->nama_file)}}">
                                            <img data-ng-src="{{asset($v->letak_file.$v->nama_file)}}" alt="" src="{{asset($v->letak_file.$v->nama_file)}}">
                                        </a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a data-ng-switch-when="true" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" class="ng-binding ng-scope" href="{{asset($v->letak_file.$v->nama_file)}}">
                                            <img src="{{ asset($v->letak_file . $v->nama_file) }}" style="width: 100px" alt="">
                                        </a>
                                    </li>
                                    @php
                                    $namasyarat = $v->s_namapersyaratan;
                                    $no++;
                                    @endphp
                                    @else
                                    <li class="list-group-item">
                                        <a style="display:none;" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" href="{{asset($v->letak_file.$v->nama_file)}}">
                                            <img data-ng-src="{{asset($v->letak_file.$v->nama_file)}}" alt="" src="{{asset($v->letak_file.$v->nama_file)}}">
                                        </a>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <a data-ng-switch-when="true" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" class="ng-binding ng-scope" href="{{asset($v->letak_file.$v->nama_file)}}">
                                            <img src="{{ asset($v->letak_file . $v->nama_file) }}" style="width: 100px" alt="">
                                        </a>
                                    </li>
                                    @endif
                                    @endforeach
                                </ul>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="blueimp-gallery-display"></a>
    <a class="close">×</a>
    <ol class="indicator"></ol>
</div>