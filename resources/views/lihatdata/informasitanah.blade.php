<?php $cek_setgmap = (new \ComboHelper())->cek_setgmap(); ?>
@if($cek_setgmap->s_id_statusmenugmap == 1)
<style>
    #map {
        width: 100%;
        height: 400px;
    }
</style>
@endif
<div class="col-md-12">
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <div class="card-title">
                        <span class="fas fa-map-marked-alt"></span> &nbsp;INFORMASI OBJEK PAJAK
                    </div>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip">
                            <i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-sm-3">NOP/Tahun SPPT</div>
                                <div  class="col-sm-6">: <b class="text-primary">{{ $dataspt->t_nop_sppt .' / '.$dataspt->t_tahun_sppt }} </b></div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">Nama SPPT</div>
                                <div class="col-sm-6">: {{ $dataspt->t_nama_sppt }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">Letak</div>
                                <div class="col-sm-6">: {{$dataspt->t_jalan_sppt}} </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">RT</div>
                                <div class="col-sm-3">: {{ $dataspt->t_rt_sppt }}</div>
                                <div class="col-sm-3">RW</div>
                                <div class="col-sm-3">: {{ $dataspt->t_rw_sppt }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">Kelurahan</div>
                                <div class="col-sm-3">: {{$dataspt->t_kelurahan_sppt}} </div>
                                <div class="col-sm-3">Kecamatan</div>
                                <div class="col-sm-3">: {{$dataspt->t_kecamatan_sppt}} </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">Kabupaten/Kota</div>
                                <div class="col-sm-6">: {{$dataspt->t_kabkota_sppt}} </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">Jenis Transaksi</div>
                                <div class="col-sm-6">: {{ $dataspt->s_namajenistransaksi }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">Jenis Dokumen</div>
                                <div class="col-sm-3">: {{ $dataspt->s_namadoktanah }}</div>
                                <div class="col-sm-3">No. Dok Tanah</div>
                                <div class="col-sm-3">: {{ $dataspt->t_nosertifikathaktanah }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">Tgl Akta</div>
                                <div class="col-sm-3">: {{ date('d-m-Y', strtotime($dataspt->t_tgldok_tanah)) }}</div>
                                <div class="col-sm-3">Kepemilikan</div>
                                <div class="col-sm-3">: {{ $dataspt->s_namahaktanah }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">Luas Tanah</div>
                                <div class="col-sm-3">: {{ $dataspt->t_luastanah }}</div>
                                <div class="col-sm-3">Luas Bangunan</div>
                                <div class="col-sm-3">: {{ $dataspt->t_luasbangunan }}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">Nilai Pasar/NJOP (m<sup>2</sup>)</div>
                                <div class="col-sm-3">: {{number_format($dataspt->t_njoptanah, 0, ',', '.')}} </div>
                                <div class="col-sm-3">Nilai Pasar/NJOP (m<sup>2</sup>)</div>
                                <div class="col-sm-3">: {{number_format($dataspt->t_njopbangunan, 0, ',', '.')}}</div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">Total</div>
                                <div class="col-sm-3">: {{number_format($dataspt->t_totalnjoptanah, 0, ',', '.')}} </div>
                                <div class="col-sm-3">Total</div>
                                <div class="col-sm-3">: {{number_format($dataspt->t_totalnjopbangunan, 0, ',', '.')}}</div>
                            </div>

                            <div class="row">
                                <div class="col-sm-3">Total NJOP PBB</div>
                                <div class="col-sm-6">: {{number_format($dataspt->t_grandtotalnjop, 0, ',', '.')}} </div>
                            </div>
                        </div>
                    </div>
                    <hr>
                    <div class="row">
                        <div class="col-md-6">
                            <label for="">Photo</label>
                            <div>
                                @php
                                $noobjek = 1;
                                @endphp
                                @if(count($data_fotoobjek) > 0)
                                @foreach ($data_fotoobjek as $key => $vobj)
                                @if(!empty($vobj->nama_file))
                                <a style="display:none;"
                                   data-ng-href="{{ asset($vobj->letak_file.$vobj->nama_file)}}"
                                   title="{{$vobj->nama_file}}" download="{{$vobj->nama_file}}"
                                   data-gallery=""
                                   href="{{asset($vobj->letak_file.$vobj->nama_file)}}"><img
                                        data-ng-src="{{asset($vobj->letak_file.$vobj->nama_file)}}" alt=""
                                        src="{{asset($vobj->letak_file.$vobj->nama_file)}}"></a>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-ng-switch-when="true"
                                                                             data-ng-href="{{ asset($vobj->letak_file.$vobj->nama_file)}}"
                                                                             title="{{$vobj->nama_file}}" download="{{$vobj->nama_file}}"
                                                                             data-gallery="" class="ng-binding ng-scope"
                                                                             href="{{asset($vobj->letak_file.$vobj->nama_file)}}">
                                    <img src="{{ asset($vobj->letak_file . $vobj->nama_file) }}" style="height: 250px" alt="">
                                </a>
                                @endif
                                @php $noobjek++; @endphp
                                @endforeach
                                @else
                                <center>TIDAK ADA DATA</center>
                                @endif
                            </div>
                        </div>
                        @if($cek_setgmap->s_id_statusmenugmap == 1)
                        <div class="col-md-6">
                            <?php if (!empty($dataspt->s_latitude) && !empty($dataspt->s_longitude)) { ?>
                                <div class="">
                                    <label for="">Maps</label>
                                    <div id="map"></div>
                                </div>
                            <?php } else { ?>
                                Titik Koordinat belum diisi.
                            <?php } ?>
                        </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@push('scriptsbawah')
@if($cek_setgmap->s_id_statusmenugmap == 1)
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly"
defer></script>


<script type="text/javascript">
    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-Token': '{{ csrf_token() }}'}
        });
    });

    function initMap() {
        var latnya = <?= $dataspt->s_latitude ?>;
        var longnya = <?= $dataspt->s_longitude ?>;
        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 13,
            center: {lat: latnya, lng: longnya},
        });
        marker = new google.maps.Marker({
            map,
            draggable: false,
            animation: google.maps.Animation.DROP,
            position: {lat: latnya, lng: longnya},
        });
    }
</script>
@endif

@endpush