<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <div class="card-title">
                    <span class="fas fa-laptop"></span> &nbsp;INFORMASI TRANSAKSI
                </div>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-md-2">Tanggal Daftar</div>
                    <div class="col-md-10 text-primary">: {{date('d-m-Y', strtotime($dataspt->t_tgldaftar_spt))}}</div>
                </div>
                <div class="row">
                    <div class="col-md-2">Jenis Transaksi</div>
                    <div class="col-md-10">: {{$dataspt->s_namajenistransaksi}}</div>
                </div>
                <div class="row">
                    <div class="col-md-2">Notaris</div>
                    <div class="col-md-10">: {{$dataspt->s_namanotaris}}</div>
                </div>
            </div>
        </div>
    </div>
</div>