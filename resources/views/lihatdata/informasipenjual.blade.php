<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <div class="card-title">
                    @if($dataspt->t_idbidang_penjual == 1)
                    <span class="fas fa-user"></span> &nbsp;INFORMASI PELEPAS HAK - Pribadi
                    @elseif($dataspt->t_idbidang_penjual == 2)
                    <span class="fas fa-university"></span> &nbsp;INFORMASI PELEPAS HAK - Badan Usaha
                    @endif
                </div>
            </div>
            <div class="card-body">
                @if($dataspt->t_idbidang_penjual == 1)
                <div class="row">
                    <div class="col-sm-3">NIK</div>
                    <div class="col-sm-9">: {{ $dataspt->t_nik_penjual }}</div>
                </div>
                @elseif($dataspt->t_idbidang_penjual == 2)
                <div class="row">
                    <div class="col-sm-3">NIB</div>
                    <div class="col-sm-9">: {{ $dataspt->t_nik_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">SIUP</div>
                    <div class="col-sm-9">: {{ $dataspt->t_siup_penjual }}</div>
                </div>
                @endif
                <div class="row">
                    <div class="col-sm-3">NPWP</div>
                    <div class="col-sm-9">: {{ $dataspt->t_npwp_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama</div>
                    <div class="col-sm-9">: {{ $dataspt->t_nama_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat</div>
                    <div class="col-sm-9">: {{ $dataspt->t_jalan_penjual }} RT/RW {{ $dataspt->t_rt_penjual .'/'. $dataspt->t_rw_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $dataspt->t_namakel_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $dataspt->t_namakec_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $dataspt->t_kabkota_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Kode Pos</div>
                    <div class="col-sm-9">: {{ $dataspt->t_kodepos_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No.Telp</div>
                    <div class="col-sm-9">: {{ $dataspt->t_nohp_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Email</div>
                    <div class="col-sm-9">: {{ $dataspt->t_email_penjual }}</div>
                </div>
            </div>
        </div>
    </div>
    @if($dataspt->t_idbidang_penjual == 2)
    <div class="col-md-6">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <div class="card-title">
                    <span class="fas fa-user"></span> &nbsp;PENANGGUNG JAWAB (PIHAK KEDUA)
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">NIK</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_nik_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NPWP</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_npwp_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_nama_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_jalan_pngjwb_penjual }} RT/RW {{ $dataspt->t_b_rt_pngjwb_pembeli .'/'. $dataspt->t_b_rw_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $dataspt->t_b_namakel_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $dataspt->t_b_namakec_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $dataspt->t_b_kabkota_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Kode Pos</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_kodepos_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No.Telp</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_nohp_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Email</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_email_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">&nbsp;</div>
                    <div class="col-sm-9"></div>
                </div>
            </div>
        </div>
    </div>
    @endif
</div>
