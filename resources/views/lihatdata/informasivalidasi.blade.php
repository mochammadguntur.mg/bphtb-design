<div class="col-md-12">
    <h5>Validasi Berkas</h5>
    <div class="card card-outline card-primary">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-2">
                    Status Validasi Berkas
                </div>
                <div class="col-sm-2">
                    : @if(!empty($dataspt->s_nama_status_berkas)) {{$dataspt->s_nama_status_berkas}}  @else Belum @endif
                </div>
            </div>    
            <div class="row">
                <div class="col-sm-2">
                    Keterangan
                </div>
                <div class="col-sm-6">
                    : @if(!empty($dataspt->t_keterangan_berkas)) {{$dataspt->t_keterangan_berkas}}  @else - @endif
                </div>
            </div>  

        </div>
    </div>
</div>
<div class="col-md-12">
    <h5>Validasi Kabid</h5>
    <div class="card card-outline card-primary">
        <div class="card-body">
            <div class="row">
                <div class="col-sm-2">
                    Status Validasi Kabid
                </div>
                <div class="col-sm-2">
                    : @if(!empty($dataspt->s_nama_status_kabid)) {{$dataspt->s_nama_status_kabid}}  @else Belum @endif
                </div>
            </div>    
            <div class="row">
                <div class="col-sm-2">
                    Keterangan
                </div>
                <div class="col-sm-6">
                    : @if(!empty($dataspt->t_keterangan_kabid)) {{$dataspt->t_keterangan_kabid}}  @else - @endif
                </div>
            </div>  





        </div>

        @if(!empty($dataspt->t_idpemeriksa))
        <div class="col-md-12">
            <h5>Hasil Pemeriksaan BPHTB</h5>
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <div class="col-sm-12 row mt-4">
                        <label class="col-sm-2 control-label">No Pemeriksaan</label>
                        <div class="col-sm-4">
                            <input name="t_noperiksa" id="t_noperiksa" class="form-control" required="true"
                                   value="{{ $dataspt->t_noperiksa}}" readonly="true"
                                   type="text">
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-4">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-4">
                            <textarea name="t_keterangan" id="t_keterangan" class="form-control" rows="8" readonly="true">{{ $dataspt->t_keterangan}}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 control-label">Pemeriksa 1</label>
                        <div class="col-sm-4">
                            <select name="t_idpejabat1" id="t_idpejabat1"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1" disabled="true"
                                    >
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_pejabat as $dpej)
                                <option value="{{ $dpej->s_idpejabat }}"
                                        {{ $dataspt->t_idpejabat1 == $dpej->s_idpejabat ? 'selected' : '' }}>
                                    {{ $dpej->s_namapejabat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 control-label">Pemeriksa 2</label>
                        <div class="col-sm-4">
                            <select name="t_idpejabat2" id="t_idpejabat2"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1" disabled="true"
                                    >
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_pejabat as $dpej1)
                                <option value="{{ $dpej1->s_idpejabat }}"
                                        {{ $dataspt->t_idpejabat2 == $dpej1->s_idpejabat ? 'selected' : '' }}>
                                    {{ $dpej1->s_namapejabat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 control-label">Pemeriksa 3</label>
                        <div class="col-sm-4">
                            <select name="t_idpejabat3" id="t_idpejabat3"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1" disabled="true"
                                    >
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_pejabat as $dpej2)
                                <option value="{{ $dpej2->s_idpejabat }}"
                                        {{ $dataspt->t_idpejabat3 == $dpej2->s_idpejabat ? 'selected' : '' }}>
                                    {{ $dpej2->s_namapejabat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 control-label">Pemeriksa 3</label>
                        <div class="col-sm-4">
                            <select name="t_idpejabat4" id="t_idpejabat4"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1" disabled="true"
                                    >
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_pejabat as $dpej3)
                                <option value="{{ $dpej3->s_idpejabat }}"
                                        {{ $dataspt->t_idpejabat4 == $dpej3->s_idpejabat ? 'selected' : '' }}>
                                    {{ $dpej3->s_namapejabat }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            Luas Tanah/Bumi
                        </div>
                        <div class="col-sm-2">
                            : {{$dataspt->t_luastanah_pemeriksa}} m<sup>2</sup>
                        </div>
                        <div class="col-sm-2">
                            NJOP/m<sup>2</sup> Tanah/Bumi
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_njoptanah_pemeriksa, 0, ',', '.')}}
                        </div>
                        <div class="col-sm-2">
                            NJOP Tanah/Bumi
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_totalnjoptanah_pemeriksa, 0, ',', '.')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            Luas Bangunan
                        </div>
                        <div class="col-sm-2">
                            : {{$dataspt->t_luasbangunan_pemeriksa}} m<sup>2</sup>

                        </div>
                        <div class="col-sm-2">
                            NJOP/m<sup>2</sup> Bangunan
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_njopbangunan_pemeriksa, 0, ',', '.')}}
                        </div>
                        <div class="col-sm-2">
                            NJOP Bangunan
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_totalnjopbangunan_pemeriksa, 0, ',', '.')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            NJOP PBB
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_grandtotalnjop_pemeriksa, 0, ',', '.')}}
                        </div>
                        <div class="col-sm-2">
                            Harga Transaksi
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_nilaitransaksispt_pemeriksa, 0, ',', '.')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            NPOP
                        </div>
                        <div class="col-sm-10">
                            : {{number_format($dataspt->t_npop_bphtb_pemeriksa, 0, ',', '.')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            NPOPTKP
                        </div>
                        <div class="col-sm-2">
                            : <u>{{number_format($dataspt->t_npoptkp_bphtb_pemeriksa, 0, ',', '.')}}
                                &nbsp;&nbsp;&nbsp;&nbsp;-</u>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            NPOPKP
                        </div>
                        <div class="col-sm-10">
                            : {{number_format($dataspt->t_npopkp_bphtb_pemeriksa, 0, ',', '.')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            Tarif (%)
                        </div>
                        <div class="col-sm-10">
                            :
                            <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($dataspt->t_persenbphtb, 0, ',', '.')}}%&nbsp;&nbsp;&nbsp;&nbsp;x</u>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            Jumlah Pajak
                        </div>
                        <div class="col-sm-2">
                            : <b
                                style="color: red; font-size: 17px;">{{number_format($dataspt->t_nilaibphtb_pemeriksa, 0, ',', '.')}}</b>
                        </div>
                    </div>

                </div>

            </div>
        </div>

        @endif


    </div>
</div>