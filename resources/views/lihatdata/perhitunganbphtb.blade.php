{{-- /**
         * Daftar Pemeriksaan: 
         * 1. t_luastanah_pemeriksa
         * 2. t_njoptanah_pemeriksa
         * 3. t_totalnjoptanah_pemeriksa
         * 4. t_luasbangunan_pemeriksa.
         * 5. t_njopbangunan_pemeriksa
         * 6. t_totalnjopbangunan_pemeriksa.
         * 7. t_grandtotalnjop_pemeriksa
         * 8. t_nilaitransaksispt_pemeriksa
         * 9. t_permeter_tanah_pemeriksa
         * 10. t_npop_bphtb_pemeriksa.
         * 11. t_npoptkp_bphtb_pemeriksa
         * 12. t_npopkp_bphtb_pemeriksa
         * 13. t_nilaibphtb_pemeriksa
         */ --}}
         @php
            $kata = (!empty($dataspt->t_idpemeriksa)) ? " HASIL PEMERIKSAAN" : "" ;
         @endphp
<div class="col-md-12">
    <div class="card card-outline card-primary">
        <div class="card-header">
            <div class="card-title">
                <span class="fas fa-calculator"></span> &nbsp;INFORMASI PERHITUNGAN PAJAK BPHTB {{ $kata }}
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                    <thead>
                        <tr>
                            <th style="background-color: #227dc7; color:white; vertical-align: text-top;" class="text-center">Uraian</th>
                            <th style="background-color: #227dc7; color:white; vertical-align: text-top;" class="text-center">Luas</th>
                            <th style="background-color: #227dc7; color:white; vertical-align: text-top;" class="text-center">NJOP PBB / m<sup>2</sup></th>
                            <th style="background-color: #227dc7; color:white; vertical-align: text-top;" class="text-center">Luas x NJOP PBB / m<sup>2</sup></th>
                        </tr>
                        <tr>
                            <td>Tanah (Bumi)</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_luastanah_pemeriksa)) ? number_format($dataspt->t_luastanah_pemeriksa, 0, ',', '.') : number_format($dataspt->t_luastanah, 0, ',', '.')  }}</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_njoptanah_pemeriksa)) ? number_format($dataspt->t_njoptanah_pemeriksa, 0, ',', '.') : number_format($dataspt->t_njoptanah, 0, ',', '.') }}</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_totalnjoptanah_pemeriksa)) ? number_format($dataspt->t_totalnjoptanah_pemeriksa, 0, ',', '.') : number_format($dataspt->t_totalnjoptanah, 0, ',', '.') }}</td>
                        </tr>
                        <tr>
                            <td>Bangunan</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_luasbangunan_pemeriksa)) ? number_format($dataspt->t_luasbangunan_pemeriksa, 0, ',', '.') : number_format($dataspt->t_luasbangunan, 0, ',', '.')  }}</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_njopbangunan_pemeriksa)) ? number_format($dataspt->t_njopbangunan_pemeriksa, 0, ',', '.') : number_format($dataspt->t_njopbangunan, 0, ',', '.') }}</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_totalnjopbangunan_pemeriksa)) ? number_format($dataspt->t_totalnjopbangunan_pemeriksa, 0, ',', '.') : number_format($dataspt->t_totalnjopbangunan, 0, ',', '.') }}</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right">NJOP PBB</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_grandtotalnjop_pemeriksa)) ? number_format($dataspt->t_grandtotalnjop_pemeriksa, 0, ',', '.') : number_format($dataspt->t_grandtotalnjop, 0, ',', '.') }}</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right">Nilai Transaksi</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_nilaitransaksispt_pemeriksa)) ? number_format($dataspt->t_nilaitransaksispt_pemeriksa, 0, ',', '.') : number_format($dataspt->t_nilaitransaksispt, 0, ',', '.') }}</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right">NPOP</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_npop_bphtb_pemeriksa)) ? number_format($dataspt->t_npop_bphtb_pemeriksa, 0, ',', '.') : number_format($dataspt->t_npop_bphtb, 0, ',', '.') }}</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right">NPOPTKP</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_npoptkp_bphtb_pemeriksa)) ? number_format($dataspt->t_npoptkp_bphtb_pemeriksa, 0, ',', '.') : number_format($dataspt->t_npoptkp_bphtb, 0, ',', '.') }}</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right">NPOPKP</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_npopkp_bphtb_pemeriksa)) ? number_format($dataspt->t_npopkp_bphtb_pemeriksa, 0, ',', '.') : number_format($dataspt->t_npopkp_bphtb, 0, ',', '.') }}</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right">Tarif (%)</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_persenbphtb_pemeriksa)) ? number_format($dataspt->t_persenbphtb_pemeriksa, 0, ',', '.') : number_format($dataspt->t_persenbphtb, 0, ',', '.') }}</td>
                        </tr>
                        <tr>
                            <td colspan="3" style="text-align: right">Jumlah Pajak</td>
                            <td style="text-align: right">{{ (!empty($dataspt->t_nilaibphtb_pemeriksa)) ? number_format($dataspt->t_nilaibphtb_pemeriksa, 0, ',', '.') : number_format($dataspt->t_nilai_bphtb_real, 0, ',', '.') }}</td> 
                        </tr>
                    </thead>
                </table>
            </div>
        </div>
    </div>
</div>