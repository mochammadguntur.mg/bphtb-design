@extends('layouts.master')
@section('judulkiri')
INFORMASI TRANSAKSI PAJAK BPHTB
@endsection

@section('content')

<link rel="stylesheet" href="{{ asset('internet/css/blueimp-gallery.min.css')}}">

<div class="card card-primary">
    <form method="post" action="{{ url('pemeriksaan/simpanpemeriksaan') }}" id="formtambah" name="formtambah">
        <br>
        <div class="col-md-12">
            @include('lihatdata.informasitransaksi')
            @include('lihatdata.informasipembeli')
            @include('lihatdata.informasipenjual')
        </div>
        <br>
        @include('lihatdata.informasitanah')
        <br>
        @include('lihatdata.perhitunganbphtb')
        <br>
        @include('lihatdata.informasipersyaratan')
        @include('lihatdata.informasivalidasi')

    </form>
</div>

@endsection


@push('scriptsbawah')
<script src="{{asset('internet/js/jquery.blueimp-gallery.min.js')}}"></script>





@endpush
