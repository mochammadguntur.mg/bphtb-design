<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <div class="card-title">
                    @if($dataspt->t_idbidang_usaha == 1)
                    <span class="fas fa-user"></span> &nbsp;PENERIMA HAK - Pribadi
                    @elseif($dataspt->t_idbidang_usaha == 2)
                    <span class="fas fa-university"></span> &nbsp;PENERIMA HAK - Badan Usaha
                    @endif
                </div>
            </div>
            <div class="card-body">
                @csrf
                @if($dataspt->t_idbidang_usaha == 1)
                <div class="row">
                    <div class="col-sm-3">NIK</div>
                    <div class="col-sm-9">: {{ $dataspt->t_nik_pembeli }}</div>
                </div>
                @elseif($dataspt->t_idbidang_usaha == 2)
                <div class="row">
                    <div class="col-sm-3">NIB</div>
                    <div class="col-sm-9">: {{ $dataspt->t_nik_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">SIUP</div>
                    <div class="col-sm-9">: {{ $dataspt->t_siup_pembeli }}</div>
                </div>
                @endif
                <div class="row">
                    <div class="col-sm-3">NPWP</div>
                    <div class="col-sm-9">: {{ $dataspt->t_npwp_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama</div>
                    <div class="col-sm-9">: {{ $dataspt->t_nama_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat</div>
                    <div class="col-sm-9">: {{ $dataspt->t_jalan_pembeli }} RT/RW {{ $dataspt->t_rt_pembeli .'/'. $dataspt->t_rw_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $dataspt->t_namakelurahan_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $dataspt->t_namakecamatan_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $dataspt->t_kabkota_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Kode Pos</div>
                    <div class="col-sm-9">: {{ $dataspt->t_kodepos_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No.Telp</div>
                    <div class="col-sm-9">: {{ $dataspt->t_nohp_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Email</div>
                    <div class="col-sm-9">: {{ $dataspt->t_email_pembeli }}</div>
                </div>

            </div>
        </div>
    </div>
    @if($dataspt->t_idbidang_usaha == 2)
    <div class="col-md-12">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <div class="card-title">
                    <span class="fas fa-user"></span> &nbsp;PENANGGUNG JAWAB (PIHAK PERTAMA)
                </div>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-3">NPWP</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_npwp_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_nama_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_jalan_pngjwb_pembeli }} RT/RW {{ $dataspt->t_b_rt_pngjwb_pembeli .'/'. $dataspt->t_b_rw_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $dataspt->t_b_namakel_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $dataspt->t_b_namakec_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $dataspt->t_b_kabkota_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Kode Pos</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_kodepos_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No. Hp</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_nohp_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No. Telp</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_notelp_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Email</div>
                    <div class="col-sm-9">: {{ $dataspt->t_b_email_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">&nbsp;</div>
                    <div class="col-sm-9"></div>
                </div>
            </div>
        </div>
    </div>
    @endif
    @if (!empty($nik_bersama[0]->t_id_nik_bersama))
    <div class="col-md-12">
        <!-- DIRECT CHAT -->
        <div class="card direct-chat card-outline direct-chat-primary mt-2">
            <div class="card-header">
                <h3 class="card-title"><i class="fa fa-users mr-1" aria-hidden="true"></i> &emsp;Data Nik Bersama &emsp;&emsp;</h3>
                <div class="col-sm-6">
                    <button type="button" class="btn btn-success btn-tool text-white" style="background-color: green;" id="dat_nik" data-card-widget="collapse" data-target="#data_nik_bersama">
                        Show / Hide
                    </button>
                </div>
            </div>
            <!-- /.card-header -->
            <div class="card-body collapse" id="data_nik_bersama">
                @php
                    $no = 1;
                @endphp
                @foreach ($nik_bersama as $key => $v)
                    <div class="row">
                        <div class="col-sm-3 text-bold">&emsp;Data ke {{ $no }}</div>
                        <div class="col-sm-9"></div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">&emsp;&emsp;NIK</div>
                        <div class="col-sm-9">: {{ $v->t_nik_bersama }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">&emsp;&emsp;Nama</div>
                        <div class="col-sm-9">: {{ $v->t_nama_bersama }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">&emsp;&emsp;Alamat</div>
                        <div class="col-sm-9">: {{ $v->t_nama_jalan }} RT/RW {{ $v->t_rt_bersama .'/'. $v->t_rw_bersama }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">&emsp;&emsp;kelurahan</div>
                        <div class="col-sm-9">: {{ $v->t_kelurahan_bersama }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">&emsp;&emsp;Kecamatan</div>
                        <div class="col-sm-9">: {{ $v->t_kecamatan_bersama }}</div>
                    </div>
                    <div class="row border-bottom mb-3">
                        <div class="col-sm-3">&emsp;&emsp;Kab/Kota</div>
                        <div class="col-sm-9">: {{ $v->t_kabkota_bersama }}</div>
                    </div>
                    @php
                        $no++
                    @endphp
            @endforeach
            </div>
            <!-- /.card-body -->
        </div>
          <!--/.direct-chat -->
    </div>
    @endif
</div>
