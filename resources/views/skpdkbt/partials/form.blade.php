<div class="row">
    <div class="col-sm-2">NOP / Tahun</div>
    <div class="col-sm-3">
        <input type="hidden" id="t_idspt" name="t_idspt" value="{{ old('t_idspt') ?? $data->t_idspt }}">
        <input type="hidden" id="t_idskpdkb" name="t_idskpdkb" value="{{ old('t_idskpdkb') ?? $data->t_idskpdkb }}">
        <input type="hidden" id="t_idjenistransaksi" name="t_idjenistransaksi" value="{{ old('t_idjenistransaksi') ?? $data->t_idjenistransaksi }}">
        <input type="text" class="form-control" id="t_nop_sppt" name="t_nop_sppt" value="{{ old('t_nop_sppt') ?? $data->t_nop_sppt }}" readonly>
    </div>
    <div class="col-sm-1">
        <input type="text" class="form-control" id="t_tahun_sppt" name="t_tahun_sppt" value="{{ old('t_tahun_sppt') ?? $data->t_tahun_sppt }}" readonly>
    </div>
</div>
<hr>
<div class="row">
    <div class="col-sm-2">Luas Tanah/Bumi (m<sup>2</sup>)</div>
    <div class="col-sm-1">
        <input type="text" class="form-control" id="t_luastanah" name="t_luastanah" value="{{ old('t_luastanah') ?? number_format($data->t_luastanah,0,',','.') }}"
        onkeyup="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onchange="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onblur="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onfocus="this.value = unformatCurrency(this.value); hitungpajakskpdkb();"
        onkeypress="return numbersonly(this, event); hitungpajakskpdkb();">
    </div>
    <div class="col-sm-2">NJOP/m<sup>2</sup> Tanah/Bumi</div>
    <div class="col-sm-2">
        <input type="text" class="form-control text-right" id="t_njoptanah" name="t_njoptanah" value="{{ old('t_njoptanah') ?? number_format($data->t_njoptanah,0,',','.') }}"
        onkeyup="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onchange="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onblur="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onfocus="this.value = unformatCurrency(this.value); hitungpajakskpdkb();"
        onkeypress="return numbersonly(this, event); hitungpajakskpdkb();" readonly>
    </div>
    <div class="col-sm-2">NJOP Tanah/Bumi</div>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_totalnjoptanah" name="t_totalnjoptanah" value="{{ old('t_totalnjoptanah') ?? number_format($data->t_totalnjoptanah,0,',','.') }}" readonly>
    </div>
</div>
<div class="row">
    <div class="col-sm-2">Luas Bangunan (m<sup>2</sup>)</div>
    <div class="col-sm-1">
        <input type="text" class="form-control" id="t_luasbangunan" name="t_luasbangunan" value="{{ old('t_luasbangunan') ?? number_format($data->t_luasbangunan,0,',','.') }}"
        onkeyup="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onchange="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onblur="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onfocus="this.value = unformatCurrency(this.value); hitungpajakskpdkb();"
        onkeypress="return numbersonly(this, event); hitungpajakskpdkb();">
    </div>
    <div class="col-sm-2">NJOP/m<sup>2</sup> Bangunan</div>
    <div class="col-sm-2">
        <input type="text" class="form-control text-right" id="t_njopbangunan" name="t_njopbangunan" value="{{ old('t_njopbangunan') ?? number_format($data->t_njopbangunan,0,',','.') }}"
        onkeyup="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onchange="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onblur="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onfocus="this.value = unformatCurrency(this.value); hitungpajakskpdkb();"
        onkeypress="return numbersonly(this, event); hitungpajakskpdkb();" readonly>
    </div>
    <div class="col-sm-2">NJOP Bangunan</div>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_totalnjopbangunan" name="t_totalnjopbangunan" value="{{ old('t_totalnjopbangunan') ?? number_format($data->t_totalnjopbangunan,0,',','.') }}" readonly>
    </div>
</div>
<div class="row">
    <div class="col-sm-9 text-right"><strong>NJOP PBB</strong></div>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_grandtotalnjop" name="t_grandtotalnjop" value="{{ old('t_grandtotalnjop') ?? number_format($data->t_grandtotalnjop,0,',','.') }}" readonly>
    </div>
</div>
<div class="row">
    <div class="col-sm-9 text-right"><strong>Harga Transaksi/Nilai Pasar</strong></div>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_nilaitransaksispt" name="t_nilaitransaksispt" value="{{ old('t_nilaitransaksispt') ?? number_format($data->t_nilaitransaksispt,0,',','.') }}"
        onkeyup="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onchange="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onblur="this.value = formatCurrency(this.value); hitungpajakskpdkb();"
        onfocus="this.value = unformatCurrency(this.value); hitungpajakskpdkb();"
        onkeypress="return numbersonly(this, event); hitungpajakskpdkb();">
    </div>
</div>
<div class="row">
    <div class="col-sm-2"><strong>NPOP</strong></div>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_npop_bphtb" name="t_npop_bphtb" value="{{ old('t_npop_bphtb') ?? number_format($data->t_npop_bphtb,0,',','.') }}" readonly>
    </div>
</div>
<div class="row">
    <div class="col-sm-2"><strong>NPOPTKP</strong></div>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_npoptkp_bphtb" name="t_npoptkp_bphtb" value="{{ old('t_npoptkp_bphtb') ?? number_format($data->t_npoptkp_bphtb,0,',','.') }}" readonly>
    </div>
    <div class="col-sm-4 text-center">
        <input class="form-control" name="ket_npoptkp" id="ket_npoptkp" value="{{ ($data->t_npoptkp_bphtb) ? 'Dapat NPOPTKP' : 'Tidak Dapat NPOPTKP' }}" style="font-weight:bold;font-size:25px;color:#18F518;background-color:black;text-align:center;" readonly="" type="text">
    </div>
</div>
<div class="row">
    <div class="col-sm-2"><strong>NPOPKP</strong></div>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_npopkp_bphtb" name="t_npopkp_bphtb" value="{{ old('t_npopkp_bphtb') ?? number_format($data->t_npopkp_bphtb,0,',','.') }}" readonly>
    </div>
    <div class="col-sm-2"><strong>Tarif (%)</strong></div>
    <div class="col-sm-1">
        <input type="text" class="form-control text-right" id="t_persenbphtb" name="t_persenbphtb" value="{{ old('t_persenbphtb') ?? number_format($data->t_persenbphtb,0,',','.') }}" readonly>
    </div>
</div>
<div class="row">
    <div class="col-sm-2"><strong>BPHTB Terhutang</strong></div>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_nilai_bphtb_fix" name="t_nilai_bphtb_fix" value="{{ old('t_nilai_bphtb_fix') ?? number_format($data->t_nilai_bphtb_fix,0,',','.') }}" readonly>
    </div>
    <div class="col-sm-2"><strong>Pembayaran Sebelumnya</strong></div>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_jmlhpajakskpdkb" name="t_jmlhpajakskpdkb" value="{{ old('t_jmlhbayar_skpdkb') ?? number_format($data->t_jmlhbayar_skpdkb + $data->t_jmlhbayar_pokok,0,',','.') }}" readonly>
    </div>
</div>
<div class="row">
    <div class="col-sm-2"><strong>SKPDKBT yg Harus Dibayar</strong></div>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_jmlhpajakskpdkbt" name="t_jmlhpajakskpdkbt" value="{{ old('t_jmlhpajakskpdkbt') ?? number_format($data->t_jmlhpajakskpdkbt,0,',','.') }}" readonly>
    </div>
</div>

<div class="row card-footer">
    <div class="col-sm-12">
        <button type="button" id="btnApprove" class="btn btn-success" data-toggle="modal" data-target="#modal-save"><i class="fas fa-check"></i> Proses Penetapan</button>
        <button type="button" id="btnReject" class="btn btn-danger pull-right" data-toggle="modal" data-target="#modal-cancel"><i class="fas fa-close"></i> Batal</button>
    </div>
</div>

<div class="modal fade" id="modal-save">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h6 class="modal-title"><span class="fas fa-check-double"></span>
                    &nbsp;PROSES PENETAPAN SKPDKBT</h6>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin melanjutkan proses penetapan SKPDKBT ini?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger btn-sm"
                        data-dismiss="modal"><span
                        class="fas fa-times-circle"></span>
                    &nbsp;Tidak
                </button>
                <button type="submit" class="btn btn-success btn-sm"><span
                        class="fas fa-check"></span> &nbsp;YA
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-cancel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-exclamation"></span>
                    &nbsp;BATALKAN PENETAPAN SKPDKBT ?</h6>
                <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin membatalkan penetapan SKPDKBT tersebut?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger btn-sm"
                        data-dismiss="modal"><span
                        class="fas fa-times-circle"></span>
                    &nbsp;Tidak
                </button>
                <a href="{{ url('skpdkbt') }}" class="btn btn-success btn-sm"><span
                        class="fas fa-exclamation"></span> &nbsp;YA
                </a>
            </div>
        </div>
    </div>
</div>
