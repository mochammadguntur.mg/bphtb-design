<style>
    .border_atas{border-top: 1px solid #000;}
    .border_bawah{border-bottom: 1px solid #000;}
    .border_kiri{border-left: 1px solid #000;}
    .border_kanan{border-right: 1px solid #000;}
    .rata_kanan{text-align: right;}
    @page{margin: 40px;font-family: sans-serif;}
</style>
@include('kop.kop-surat')

@if($data->t_idbidang_usaha == 1)
    @php
    $namawp = strtoupper($data->t_nama_pembeli);
    $alamatwp = $data->t_jalan_pembeli.' RT/RW '. $data->t_rt_pembeli .'/'. $data->t_rw_pembeli .', Kel. '. $data->t_namakelurahan_pembeli .', Kec. '. $data->t_namakecamatan_pembeli . ', Kab/Kota '.$data->t_kabkota_pembeli;
    @endphp
@else
    @php
    $namawp = strtoupper($data->t_b_nama_pembeli);
    $alamatwp = $data->t_b_jalan_pembeli.' RT/RW '. $data->t_b_rt_pembeli .'/'. $data->t_b_rw_pembeli .', Kel. '. $data->t_b_namakelurahan_pembeli .', Kec. '. $data->t_b_namakecamatan_pembeli . ', Kab/Kota '.$data->t_b_kabkota_pembeli;
    @endphp
@endif

@php
    $pajakterhutang = $data->t_npopkp_bphtb_skpdkbt * $data->t_persenbphtb / 100;
@endphp

<table style="width:100%;font-size: 10pt;">
    <tr>
        <td colspan="3" style="text-align: center;font-size: 12pt;">
            <h3>SURAT KETETAPAN PAJAK DAERAH KURANG BAYAR TAMBAHAN</h3>
        </td>
    </tr>
    <tr>
        <td style="width:30%;"></td>
        <td style="width:30%;"></td>
        <td style="width:40%;">
            Kepada Yth : {{ $namawp }} <br>Di {{ $alamatwp }}
        </td>
    </tr>
</table>

<table style="width:100%;font-size: 10pt;">
    <tr>
        <td style="width:20%;">Nomor SPT</td>
        <td style="">: <strong>{{ $data->t_nourut_kodebayar }}</strong></td>
    </tr>
    <tr>
        <td style="">Kode Bayar</td>
        <td style="">: <strong>{{ $data->t_kodebayar_skpdkbt }}</strong></td>
    </tr>
    <tr>
        <td style="">Tgl Penetapan</td>
        <td style="">: {{ date('d-m-Y', strtotime($data->t_tglpenetapan)) }}</td>
    </tr>
    <tr>
        <td style="">Tgl Jatuh Tempo</td>
        <td style="">: {{ date('d-m-Y', strtotime($data->t_tgljatuhtempo_skpdkbt)) }}</td>
    </tr>
</table>

<table style="width:100%;font-size: 10pt;">
    <tr>
        <td style="vertical-align: top;width:5%;">I.</td>
        <td style="vertical-align: top;text-align:justify;" colspan="3">Berdasarkan Peraturan Bupati Nomor 24 Tahun 2013 tentang Bea Perolehan Hak Atas Tanah dan Bangunan
            telah dilakukan pemeriksaan atau berdasarkan keterangan lain mengenai pelaksanaan kewajiban
            Bea Perolehan Hak atas Tanah dan Bangunan terhadap :</td>
    </tr>
    <tr>
        <td></td>
        <td style="width: 20%;">Nama Wajib Pajak</td>
        <td style="width: 5%;">:</td>
        <td>{{ $namawp }}</td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align: top;">Alamat</td>
        <td style="vertical-align: top;">:</td>
        <td style="vertical-align: top;">{{ $alamatwp }}</td>
    </tr>
    <tr>
        <td></td>
        <td colspan="3">Atas perolehan hak atas tanah dan atau bangunannnya dengan :<br>
        Akta. {{ $data->s_namajenistransaksi }}</td>
    </tr>
    <tr>
        <td></td>
        <td style="width: 20%;">Nama </td>
        <td style="width: 5%;">:</td>
        <td>{{ strtoupper($data->t_nama_sppt) }}</td>
    </tr>
    <tr>
        <td></td>
        <td>NOP</td>
        <td>:</td>
        <td>{{ $data->t_nop_sppt }}</td>
    </tr>
    <tr>
        <td></td>
        <td style="vertical-align: top;">Alamat</td>
        <td style="vertical-align: top;">:</td>
        <td style="vertical-align: top;">{{ $data->t_jalan_sppt }}</td>
    </tr>
    <tr>
        <td style="vertical-align: top;width:5%;">II.</td>
        <td style="vertical-align: top;text-align:justify;" colspan="3">
            Dari Pemeriksaan tersebut di atas, jumlah yang masih harus dibayar adalah sebagai berikut :
        </td>
    </tr>
    <tr>
        <td></td>
        <td colspan="3">
            <table class="border_atas border_bawah border_kiri border_kanan" style="width: 100%;border-collapse: collapse;" cellspacing="2" cellpadding="2">
                <tr>
                    <td class="border_bawah">1</td>
                    <td class="border_kiri border_bawah">Nilai Perolehan Objek Pajak (NPOP)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($data->t_npop_bphtb_skpdkbt,0,',','.') }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">2</td>
                    <td class="border_kiri border_bawah">Nilai Perolehan Objek Pajak Tidak Kena Pajak (NPOPTKP)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($data->t_npoptkp_bphtb_skpdkbt,0,',','.') }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">3</td>
                    <td class="border_kiri border_bawah">Nilai Perolehan Objek Pajak Kena Pajak (1-2)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($data->t_npopkp_bphtb_skpdkbt,0,',','.') }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">4</td>
                    <td class="border_kiri border_bawah">Pajak yang seharusnya terutang : {{ $data->t_persenbphtb.'% x Rp. '.number_format($data->t_npopkp_bphtb_skpdkbt,0,',','.') }},- (3)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($pajakterhutang,0,',','.') }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">5</td>
                    <td class="border_kiri border_bawah">Pengenaan Hak Pengelolaan/Hibah Wasiat/Waris : 0% X Rp. {{ number_format($pajakterhutang,0,',','.') }},- (4)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($pajakterhutang,0,',','.') }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">6</td>
                    <td class="border_kiri border_bawah">Pajak yang seharusnya dibayar ( 4 atau 5 )</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($pajakterhutang,0,',','.') }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">7</td>
                    <td class="border_kiri border_bawah">Pajak yang telah dibayar</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($data->t_nilai_bayar_sblumnya,0,',','.') }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">8</td>
                    <td class="border_kiri border_bawah">Diperhitungkan:</td>
                    <td class="border_kiri border_bawah"></td>
                    <td class="border_bawah rata_kanan"></td>
                </tr>
                <tr>
                    <td class="border_bawah"></td>
                    <td class="border_kiri border_bawah">8.a. Pokok STPD</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format(0) }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah"></td>
                    <td class="border_kiri border_bawah">8.b. Pengurangan</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format(0) }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah"></td>
                    <td class="border_kiri border_bawah">8.c. Jumlah (8.a. + 8.b)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format(0) }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah"></td>
                    <td class="border_kiri border_bawah">8.d. Dikurangi pokok SKPDLB</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format(0) }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah"></td>
                    <td class="border_kiri border_bawah">8.e. Jumlah (8.c - 8.d.)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format(0) }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">9</td>
                    <td class="border_kiri border_bawah">Jumlah yang dapat diperhitungkan (7 + 8.e.)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($data->t_nilai_bayar_sblumnya,0,',','.') }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">10</td>
                    <td class="border_kiri border_bawah">Pajak yang kurang dibayar (6-9)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($pajakterhutang - $data->t_nilai_bayar_sblumnya,0,',','.') }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">11</td>
                    <td class="border_kiri border_bawah">Sanksi administrasi berupa bunga ({{ $denda['jmlbulan'] }} Bulan)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($denda['jmldenda'],0,',','.') }},-</td>
                </tr>
                <tr>
                    <td class="border_bawah">12</td>
                    <td class="border_kiri border_bawah">Jumlah yang masih harus dibayar (10+11)</td>
                    <td class="border_kiri border_bawah">Rp.</td>
                    <td class="border_bawah rata_kanan">{{ number_format($data->t_nilai_pokok_skpdkbt + $denda['jmldenda'],0,',','.') }},-</td>
                </tr>
            </table>
        </td>
    </tr>
</table>

<table style="width:100%;">
    <tr>
        <td style="width: 30%;color:white;">{{ $barcode }}</td>
        <td style="width: 40%;"></td>
        <td style="width: 30%;vertical-align:top;">{{ $pemda->s_namaibukotakabkot }}</td>
    </tr>
</table>
