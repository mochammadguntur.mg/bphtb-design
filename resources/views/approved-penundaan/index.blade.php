@extends('layouts.master')

@section('judulkiri')
APPROVED PENUNDAAN
@endsection

@section('content')
<div class="row">
    <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-info"><i class="far fa-envelope"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">TOTAL PERMOHONAN</span>
            <span class="info-box-number">{{ $permohonan }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-success"><i class="fas fa-check"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">TOTAL DISETUJUI</span>
            <span class="info-box-number">{{ $approved }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-danger"><i class="fas fa-minus-circle"></i></span>

            <div class="info-box-content">
            <span class="info-box-text">TOTAL DITOLAK</span>
            <span class="info-box-number">{{ $rejected }}</span>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="approved-penundaan/permohonan" class="btn btn-primary">
                    <i class="fas fa-file-signature"></i> Approved
                    </a>
                </h3>
                <div class="card-tools">
                    <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                        <i class="fas fa-file-excel"></i> Excel
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger"  id="btnPDF">
                        <i class="fas fa-file-pdf"></i> PDF
                    </a>
                </div>
            </div>
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No Penundaan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Tgl Permohonan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No Kohir</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Nama WP</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Tgl Persetujuan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Status Persetujuan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Create At</th>
                                <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-nopenundaan">
                                </th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-tglpengajuan" readonly>
                                </th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-kohirspt">
                                </th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-nama_pembeli">
                                </th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-tglpersetujuan" readonly>
                                </th>
                                <th></th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-created_at" readonly>
                                </th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="9"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">No penundaan</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="nopenundaanHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>


@endsection
@push('scripts')

<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif

        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif

        $('#filter-tglpengajuan').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglpengajuan').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tglpengajuan').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-tglpersetujuan').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglpersetujuan').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tglpersetujuan').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });


    });

    var datatables = datagrid({
        url: 'approved-penundaan/datagrid',
        table: "#datagrid-table",
        columns: [
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "t_nopenundaan"
            },
            {
                sortable: true,
                name: "t_tglpengajuan"
            },
            {
                sortable: true,
                name: "t_kohirspt"
            },
            {
                sortable: true,
                name: "t_nama_pembeli"
            },
            {
                sortable: true,
                name: "t_tglpersetujuan"
            },
            {
                sortable: false
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'print',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-print" title="Cetak SK">Cetak</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash" title="Hapus Persetujuan">Hapus</i>'
            }
        ]

    });

    $("#filter-nopenundaan, #filter-kohirspt, #filter-nama_pembeli").keyup(function() {
        search();
    });

    $("#filter-tglpengajuan, #filter-tglpersetujuan, #filter-created_at").change(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            nopenundaan: $("#filter-nopenundaan").val(),
            tglpengajuan: $("#filter-tglpengajuan").val(),
            kohirspt: $("#filter-kohirspt").val(),
            nama_pembeli: $("#filter-nama_pembeli").val(),
            tglpersetujuan: $("#filter-tglpersetujuan").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'approved-penundaan/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].t_idpenundaan);
            $("#nopenundaanHapus").val(data[0].t_nopenundaan);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'approved-penundaan/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Berhasil dihapus!')
            datatables.reload();
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        var t_nopenundaan = $("#filter-nopenundaan").val();
        var t_tglpengajuan = $("#filter-tglpengajuan").val();
        var t_kohirspt = $("#filter-kohirspt").val();
        var t_nama_pembeli = $("#filter-nama_pembeli").val();
        var t_tglpersetujuan = $("#filter-tglpersetujuan").val();
        var created_at = $("#filter-created_at").val();
        if(a.target.id == 'btnExcel'){
            window.open('/approved-penundaan/export_xls?t_nopenundaan=' + t_nopenundaan
                + '&t_tglpengajuan=' + t_tglpengajuan
                + '&t_kohirspt=' + t_kohirspt
                + '&t_nama_pembeli=' + t_nama_pembeli
                + '&t_tglpersetujuan=' + t_tglpersetujuan
                + '&created_at=' + created_at);
        }else{
            window.open('/approved-penundaan/export_pdf?t_nopenundaan=' + t_nopenundaan
                + '&t_tglpengajuan=' + t_tglpengajuan
                + '&t_kohirspt=' + t_kohirspt
                + '&t_nama_pembeli=' + t_nama_pembeli
                + '&t_tglpersetujuan=' + t_tglpersetujuan
                + '&created_at=' + created_at);
        }
    });

    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").slideUp(500);
    });

</script>
@endpush
