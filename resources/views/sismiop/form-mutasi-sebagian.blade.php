@extends('layouts.master')

@section('judulkiri')
MUTASI SPOP [<span style='color:red'>{{ $data->t_luastanah == $data->t_luastanah_sismiop ? "Mutasi Penuh" : 'Mutasi Sebagian' }}</span>]
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-primary">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-outline card-default">
                            <div class="card-header">
                                DATA WAJIB PAJAK LAMA
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-2">NIK</div>
                                    <div class="col-sm-10">: {{ $data->t_nik_penjual }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Nama</div>
                                    <div class="col-sm-10">: {{ $data->t_nama_penjual }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">NPWP</div>
                                    <div class="col-sm-10">: {{ $data->t_npwp_penjual }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Alamat</div>
                                    <div class="col-sm-10">: {{ $data->t_jalan_penjual }}, {{ $data->t_rt_penjual }}, {{ $data->t_rw_penjual }}, {{ $data->t_namakelurahan_penjual }}, {{ $data->t_namakecamatan_penjual }}, {{ $data->t_kabkota_penjual }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Telepon</div>
                                    <div class="col-sm-10">: {{ $data->t_nohp_penjual }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Email</div>
                                    <div class="col-sm-10">: {{ $data->t_email_penjual }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Kode Pos</div>
                                    <div class="col-sm-10">: {{ $data->t_kodepos_penjual }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-outline card-default">
                            <div class="card-header">
                                DATA WAJIB PAJAK BARU
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-2">NIK</div>
                                    <div class="col-sm-10">: {{ $data->t_nik_pembeli }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Nama</div>
                                    <div class="col-sm-10">: {{ $data->t_nama_pembeli }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">NPWP</div>
                                    <div class="col-sm-10">: {{ $data->t_npwp_pembeli }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Alamat</div>
                                    <div class="col-sm-10">: {{ $data->t_jalan_pembeli }}, {{ $data->t_rt_pembeli }}, {{ $data->t_rw_pembeli }}, {{ $data->t_namakelurahan_pembeli }}, {{ $data->t_namakecamatan_pembeli }}, {{ $data->t_kabkota_pembeli }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Telepon</div>
                                    <div class="col-sm-10">: {{ $data->t_nohp_pembeli }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Email</div>
                                    <div class="col-sm-10">: {{ $data->t_email_pembeli }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Kode Pos</div>
                                    <div class="col-sm-10">: {{ $data->t_kodepos_pembeli }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-outline card-default">
                            <div class="card-header">
                                DATA OBJEK PAJAK
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-2">NOP/Tahun</div>
                                    <div class="col-sm-10">: {{ $data->t_nop_sppt }}/{{ $data->t_tahun_sppt }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Nama SPPT</div>
                                    <div class="col-sm-10">: {{ $data->t_nama_sppt }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Alamat</div>
                                    <div class="col-sm-10">: {{ $data->t_jalan_sppt }}, {{ $data->t_rt_sppt }}, {{ $data->t_rw_sppt }}, {{ $data->t_kelurahan_sppt }}, {{ $data->t_kecamatan_sppt }}, {{ $data->t_kabkota_sppt }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Luas Tanah</div>
                                    <div class="col-sm-2">: {{ number_format($data->t_luastanah, 0, ',', '.') }}</div>
                                    <div class="col-sm-3">Luas Bangunan</div>
                                    <div class="col-sm-2">: {{ number_format($data->t_luasbangunan, 0, ',', '.') }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2" style="font-size: 15px;">NJOP Tanah</div>
                                    <div class="col-sm-2">: {{ number_format($data->t_totalnjoptanah, 0, ',', '.') }}</div>
                                    <div class="col-sm-3">NJOP Bangunan</div>
                                    <div class="col-sm-2">: {{ number_format($data->t_totalnjopbangunan, 0, ',', '.') }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Dok/No. Tanah</div>
                                    <div class="col-sm-10">: {{ $data->t_nosertifikathaktanah }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-outline card-default">
                            <div class="card-header">
                                DATA PENGAJUAN MUTASI
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <div class="form-group row">
                                    <div class="col-sm-2">Tgl.</div>
                                    <div class="col-sm-10">: {{ $data->t_tglpengajuan }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">No. Kohir</div>
                                    <div class="col-sm-10">: {{ $data->t_nokohir }}</div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-sm-2">Keterangan</div>
                                    <div class="col-sm-10">: {{ $data->t_keterangan_permohoanan }}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="card card-outline card-default">
                            <div class="card-header">
                                PERSETUJUAN MUTASI
                                <div class="card-tools">
                                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                                </div>
                            </div>
                            <div class="card-body">
                                <form action="/sismiop/{{ $data->t_idmutasi }}/form-mutasi-sebagian" method="POST" class="form-horizontal">
                                    @method('patch')
                                    @csrf
                                    <div class="form-group row">
                                        <label for="t_tglpersetujuan" class="col-sm-2 col-form-label">Tgl. SK</label>
                                        <div class="input-group date col-md-2" id="tgl_persetujuan" data-target-input="nearest">
                                            <input type="text" name="t_tglpersetujuan" id="t_tglpersetujuan" value="<?= date('d-m-Y') ?>" class="form-control datetimepicker-input" data-target="#tgl_persetujuan" data-toggle="datetimepicker"/>
                                        </div>
                                        @if ($errors->has('t_tglpersetujuan'))
                                        <div class="text-danger mt-2">
                                            {{ $errors->first('t_tglpersetujuan') }}
                                        </div>
                                        @endif
                                    </div>
                                    <div class="form-group row">
                                        <label for="t_nosk_mutasi" class="col-sm-2 col-form-label">No SK</label>
                                        <div class="col-sm-2">
                                            <input type="text" class="form-control" id="t_nosk_mutasi" name="t_nosk_mutasi" value="{{ old('t_nosk_mutasi') ?? $data->t_nosk_mutasi }}">
                                            @if ($errors->has('t_nosk_mutasi'))
                                            <span class="text-danger mt-2">
                                                {{ $errors->first('t_nosk_mutasi') }}
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="t_keterangan_disetujui" class="col-sm-2 col-form-label">Keterangan</label>
                                        <div class="col-sm-6">
                                            <textarea class="form-control" id="t_keterangan_disetujui" name="t_keterangan_disetujui">{{ old('t_keterangan_disetujui') ?? $data->t_keterangan_disetujui }}</textarea>
                                            @if ($errors->has('t_keterangan_disetujui'))
                                            <span class="text-danger mt-2">
                                                {{ $errors->first('t_keterangan_disetujui') }}
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label for="inputEmail3" class="col-sm-2 col-form-label">Status</label>
                                        <div class="col-sm-6">
                                            <select class="form-control" id="t_idstatus_disetujui" name="t_idstatus_disetujui">
                                                <option value="">Silahkan Pilih</option>
                                                <option value="1">Setuju</option>
                                                <option value="2">Tidak Setuju</option>
                                            </select>
                                            @if ($errors->has('t_idstatus_disetujui'))
                                            <span class="text-danger mt-2">
                                                {{ $errors->first('t_idstatus_disetujui') }}
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                    <button type="submit" class="btn btn-primary">SIMPAN</button>
                                    <a href="/sismiop" class="btn btn-danger pull-right" role="button" aria-pressed="true">Kembali</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#tgl_persetujuan').datetimepicker({
            format: 'DD-MM-YYYY'
        });

        datatables.reload();
    });
</script>
@endpush