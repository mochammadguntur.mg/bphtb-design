<div class="table-responsive" style="margin-top: 15px;">
    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table-mutasi">
        <thead>
            <tr>
                <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                <th style="background-color: #227dc7; color:white;" class="text-center">No SK</th>
                <th style="background-color: #227dc7; color:white;" class="text-center">Tgl Persetujuan</th>
                <th style="background-color: #227dc7; color:white;" class="text-center">No Kohir</th>
                <th style="background-color: #227dc7; color:white;" class="text-center">Nama WP</th>
                <th style="background-color: #227dc7; color:white;" class="text-center">Status</th>
                <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
            </tr>
            <tr>
                <th></th>
                <th><input type="text" class="form-control form-control-sm" id="filter-nosk"></th>
                <th><input type="text" class="form-control form-control-sm" id="filter-tglpersetujuan" readonly></th>
                <th><input type="text" class="form-control form-control-sm" id="filter-kohirspt"></th>
                <th><input type="text" class="form-control form-control-sm" id="filter-nama_pembeli"></th>
                <th></th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="text-center" colspan="8"> Tidak ada data.</td>
            </tr>
        </tbody>
    </table>
</div>
<div class="card-footer clearfix pagination-footer">
</div>