@extends('layouts.master')

@section('judulkiri')
SISMIOP
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-body">
                <ul class="nav nav-tabs" id="tabs-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="tabs-pengajuan-tab" data-toggle="pill" href="#tabs-pengajuan" role="tab" aria-controls="tabs-pengajuan" aria-selected="true">Pengajuan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="tabs-mutasi-tab" data-toggle="pill" href="#tabs-mutasi" role="tab" aria-controls="tabs-mutasi" aria-selected="false">Mutasi</a>
                    </li>
                </ul>
                <div class="tab-content" id="tabs-tabContent">
                    <div class="tab-pane fade show active" id="tabs-pengajuan" role="tabpanel" aria-labelledby="tabs-pengajuan-tab">
                        @include('sismiop.grid-pengajuan')
                    </div>
                    <div class="tab-pane fade" id="tabs-mutasi" role="tabpanel" aria-labelledby="tabs-mutasi-tab">
                        @include('sismiop.grid-mutasi')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#filter-tglpengajuan').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglpengajuan').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tglpengajuan').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-tglpersetujuan').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglpersetujuan').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            searchMutasi();
        });

        $('#filter-tglpersetujuan').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            searchMutasi();
        });

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

    });

    var datatables = datagrid({
        url: 'sismiop/datagridPengajuan',
        table: "#datagrid-table-pengajuan",
        columns: [{
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-right"
            },
            {
                class: "text-right"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "t_nomutasi"
            },
            {
                sortable: true,
                name: "t_tglpengajuan"
            },
            {
                sortable: true,
                name: "t_kohirspt"
            },
            {
                sortable: true,
                name: "t_nama_pembeli"
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            }
        ],
        action: [{
            name: 'mutasi',
            btnClass: 'btn btn-primary btn-sm',
            btnText: '<i class="fas fa-edit"> Mutasi</i>'
        },{
            name: 'mutasi-penuh',
            btnClass: 'btn btn-primary btn-sm',
            btnText: '<i class="fas fa-edit"> Mutasi Penuh</i>'
        },{
            name: 'mutasi-sebagian',
            btnClass: 'btn btn-primary btn-sm',
            btnText: '<i class="fas fa-edit"> Mutasi Sebagian</i>'
        }]

    });

    $("#filter-nomutasi, #filter-tglpengajuan, #filter-kohirspt, #filter-nama_pembeli, #filter-created_at").keyup(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            nomutasi: $("#filter-nomutasi").val(),
            tglpengajuan: $("#filter-tglpengajuan").val(),
            kohirspt: $("#filter-kohirspt").val(),
            nama_pembeli: $("#filter-nama_pembeli").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    $("#alert").fadeTo(2000, 500).slideUp(500, function() {
        $("#alert").slideUp(500);
    });

    var datatablesMutasi = datagrid({
        url: 'sismiop/datagridMutasi',
        table: "#datagrid-table-mutasi",
        columns: [{
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "t_nomutasi"
            },
            {
                sortable: true,
                name: "t_tglpersetujuan"
            },
            {
                sortable: true,
                name: "t_kohirspt"
            },
            {
                sortable: true,
                name: "t_nama_pembeli"
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
        ],
        action: [{
            name: 'print',
            btnClass: 'btn btn-success btn-sm',
            btnText: '<i class="fas fa-print"> SK</i>'
        }]

    });

    $("#filter-nosk, #filter-kohirspt, #filter-nama_pembeli, #filter-tglpersetujuan").keyup(function() {
        searchMutasi();
    });

    function searchMutasi() {
        datatablesMutasi.setFilters({
            nosk: $("#filter-nosk").val(),
            tglpersetujuan: $("#filter-tglpersetujuan").val(),
            kohirspt: $("#filter-kohirspt").val(),
            nama_pembeli: $("#filter-nama_pembeli").val()
        });
        datatablesMutasi.reload();
    }
    searchMutasi();
</script>
@endpush