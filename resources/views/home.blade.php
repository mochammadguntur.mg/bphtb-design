@extends('layouts.master')

@section('judulkiri')

@endsection

@section('content')
<!--
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }} test</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{ __('You are logged in!') }}
                </div>
            </div>
        </div>
    </div>
</div>
-->
<div class="row">
  <div class="col-12">
    <div class="card elevation-1"
      style="background-position: calc(100% + 1rem) bottom; background-size: 100% auto; background-repeat: no-repeat; background-image: url({{ asset('dist/img/bg/backdashboard.jpg') }})">
      <div class="card-body ">
        <div class="text-left" style="margin-left: 15%">
          <img src="{{asset('storage/'.$data_pemda->s_logo )}}" class="" style="width: 10%">
        </div>

        <div class="px-3 mb-4 mt-3">
          <h3 class="text-primary"> <b>PEMERINTAH {{ strtoupper(strtolower($data_pemda->s_namakabkot))}}</b> </h1>
          <h4 class="text-gray-700 mb-0">{!! strtoupper($data_pemda->s_namasingkatinstansi) .' | '. strtoupper(strtolower($data_pemda->s_namainstansi)).'<br>'.ucwords(strtolower($data_pemda->s_alamatinstansi))!!}</h4>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6 col-sm-12">
    <div class="row">
      <div class="col-xl-12 col-lg-12 py-2">
        <div class="card shadow-sm"
          style="height : 19rem; background-color: #FFFFFF; background-position: calc(100% + 1rem) bottom; background-size: 30% auto; background-repeat: no-repeat; background-image: url({{ asset('dist/img/patterns/rhone.svg') }})">
          <div class=" px-4 mt-4">
            <h4 class="text-primary"> <b>Hai, {{ Auth::user()->name }}</b> </h4>
            <h4 class="text-black-50 mb-0">Selamat Datang di Aplikasi my-BPHTB</h4>
          </div>
          <div class="px-4 mt-4">
            @php
                    $gambar = Illuminate\Support\Facades\DB::table('users')->where('id',Auth::user()->id)->get('s_gambar')[0];
              @endphp
              @if (!empty($gambar->s_gambar))
                    <img src="{{ asset($gambar->s_gambar) }}" style="width: 150px; border-radius:20px" >
              @else
                    <img src="{{ asset('dist/img/user8-128x128.jpg') }}" style="width: 150px; border-radius:20px" >
              @endif
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card  card-primary overflow-auto" style="height: 33rem">
          <div class="card-header">
            <h3 class="card-title">Pendaftaran Terbaru</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body pt-2">
            @foreach ($data_pendaftaran_sspd_terbaru as $data)
            @if ($loop->index > 4)
            @break
            @endif
            <div class="d-flex align-items-center rounded p-2 my-3 bg-gray-light">
              <span class="info-box-icon bg-success elevation-1 p-3 rounded"><i class="fas fa-dollar-sign"></i></span>
              <div class="d-flex flex-column flex-grow-1 mr-2 ml-2">
                <a href="/pendaftaran/{{$data->t_idspt}}/tahappertama"
                  class="font-weight-bold text-dark font-size-lg mb-1">{{$data->t_nama_pembeli}}</a>
                <span class="text-black-50 font-weight-bold ">{{$data->s_namajenistransaksi}}</span>
              </div>
              <div class="d-flex flex-column">
                @if ($data->s_nama_status_bayar != "Sudah")
                <span class="badge badge-danger">BELUM LUNAS</span>
                <span
                  class="font-weight-bolder text-dark py-1 font-size-lg text-right">Rp.{{ number_format($data->t_nilai_bphtb_fix, 2, ",", ".") }}</span>
                @else
                <span class="badge badge-success">SUDAH LUNAS</span>
                <span class="font-weight-bolder text-dark py-1 font-size-lg text-right">Rp.
                  {{ number_format($data->t_jmlhbayar_total, 2, ",", ".") }}</span>
                @endif
              </div>
            </div>
            @endforeach
          </div>
        </div>
      </div>
    </div>
  </div>
  <div class="col-md-6 col-sm-12">
    <div class="row">
      <div class="d-flex align-content-start flex-wrap mt-2">
        <a href="/pendaftaran">
          <div class="col-xl-6 col-lg-6">
            <div class="small-box bg-info">
              <div class="inner px-3 pt-2">
                <h3>{{$jml_data_pendaftaran}}</h3>
                <p>PENDAFTARAN</p>
              </div>
              <div class="icon">
                <i class="ion ion-edit"></i>
              </div>
              <a href="#" class="small-box-footer"></a>
            </div>
          </div>
        </a>

        <a href="/validasi-berkas">
          <div class="col-xl-6 col-lg-6">
            <div class="small-box bg-success">
              <div class="inner px-3 pt-2">
                <h3>{{$jml_data_berkas}}</h3>
                <p>VALIDASI BERKAS</p>
              </div>
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <a href="#" class="small-box-footer"> </a>
            </div>
          </div>
        </a>

        <a href="/validasi-kabid">
          <div class="col-xl-6 col-lg-6">
            <div class="small-box bg-primary">
              <div class="inner px-3 pt-2">
                <h3>{{$jml_data_kabid}}</h3>
                <p>VALIDASI KABID</p>
              </div>
              <div class="icon">
                <i class="fa fa-check"></i>
              </div>
              <a href="#" class="small-box-footer"> </a>
            </div>
          </div>
        </a>

        <a href="/pembayaran">
          <div class="col-xl-6 col-lg-6">
            <div class="small-box bg-warning">
              <div class="inner px-3 pt-2">
                <h3>{{$jml_data_pembayaran}}</h3>
                <p>PEMBAYARAN</p>
              </div>
              <div class="icon">
                <i class="fa fa-money"></i>
              </div>
              <a href="#" class="small-box-footer"></a>
            </div>
          </div>
        </a>

        <a href="/pelaporan-ajb">
          <div class="col-xl-6 col-lg-6">
            <div class="small-box bg-danger">
              <div class="inner px-3 pt-2">
                <h3>{{$jml_data_lapor_bulanan}}</h3>
                <p>PELAPORAN</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer"></a>
            </div>
          </div>
        </a>

        <a href="/skpdkb">
          <div class="col-xl-6 col-lg-6">
            <div class="small-box bg-info">
              <div class="inner px-3 pt-2">
                <h3>{{$jml_data_skpdkb}}</h3>
                <p>SKPDKB</p>
              </div>
              <div class="icon">
                <i class="ion ion-pie-graph"></i>
              </div>
              <a href="#" class="small-box-footer"></a>
            </div>
          </div>
        </a>
      </div>
    </div>

    <div class="row">
      <div class="col-md-12">
        <div class="card card-primary">
          <div class="card-header">
            <h3 class="card-title">Realisasi Report</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
              </button>
            </div>
          </div>
          <div class="card-body">
            <canvas id="myChart" width="400" height="200"></canvas>
          </div>
        </div>
      </div>
    </div>

  </div>
</div>

<!-- ChartJS -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.4/Chart.min.js" integrity="sha512-d9xgZrVZpmmQlfonhQUvTR7lMPtO7NkZMkA0ABN3PHCbKA5nqylQ/yWlFAyY6hYgdF1Qh6nYiuADWwKB4C2WSw==" crossorigin="anonymous"></script>
<script>
  var ctx = document.getElementById('myChart').getContext('2d');
  var myChart = new Chart(ctx, {
      type: 'bar',
      data: {
          labels: ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Ags', 'Sep', 'Okt', 'Nov', 'Des'],
          datasets: [{
              label: 'Realisasi',
              data: [{{ $grafik_jmlreali_jan }},
                  {{ $grafik_jmlreali_feb }},
                  {{ $grafik_jmlreali_mar }},
                  {{ $grafik_jmlreali_apr }},
                  {{ $grafik_jmlreali_mei }},
                  {{ $grafik_jmlreali_jun }},
                  {{ $grafik_jmlreali_jul }},
                  {{ $grafik_jmlreali_agus }},
                  {{ $grafik_jmlreali_sept }},
                  {{ $grafik_jmlreali_okt }},
                  {{ $grafik_jmlreali_nov }},
                  {{ $grafik_jmlreali_des }}
              ],
              backgroundColor: [
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)',
                  'rgba(54, 162, 235, 0.2)'
              ],
              borderColor: [
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)',
                  'rgba(54, 162, 235, 1)'
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
  });
  </script>
@endsection
