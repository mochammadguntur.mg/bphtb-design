<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">NIP</label>
    <div class="col-md-4">
        <input type="text" name="s_nippejabat" class="form-control @error('s_nippejabat') is-invalid @enderror" id="s_nippejabat" placeholder="NIP" value="{{ old('s_nippejabat') ?? $pejabat->s_nippejabat }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_nippejabat'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_nippejabat') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Nama Pejabat</label>
    <div class="col-md-4">
        <input type="text" name="s_namapejabat" class="form-control @error('s_namapejabat') is-invalid @enderror" id="s_namapejabat" placeholder="Nama Pejabat" value="{{ old('s_namapejabat') ?? $pejabat->s_namapejabat }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_namapejabat'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_namapejabat') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Jabatan</label>
    <div class="col-md-4">
        <input type="text" name="s_jabatanpejabat" class="form-control" id="s_jabatanpejabat" placeholder="Jabatan" value="{{ old('s_jabatanpejabat') ?? $pejabat->s_jabatanpejabat }}">
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Golongan Jabatan</label>
    <div class="col-md-4">
        <input type="text" name="s_golonganpejabat" class="form-control" id="s_golonganpejabat" placeholder="Golongan Jabatan" value="{{ old('s_golonganpejabat') ?? $pejabat->s_golonganpejabat }}">
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Alamat</label>
    <div class="col-md-4">
        <input type="text" name="s_alamat" class="form-control" id="s_alamat" placeholder="Alamat" value="{{ old('s_alamat') ?? $pejabat->s_alamat }}">
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2" for="exampleInputFile">File TTD</label>
    <div class="col-md-4">
        <div class="input-group">
            <div class="custom-file">
                <input type="file" name="s_filettd" id="s_filettd" value="{{ old('s_filettd') }}" class="custom-file-input form-control @error('s_filettd') is-invalid @enderror">
                <label class="custom-file-label" for="s_filettd22">Choose file</label>
            </div>
            <div class="input-group-append">
                <span class="input-group-text" id="">Upload</span>
            </div>
        </div>
        @if ($errors->has('s_filettd'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_filettd') }}
        </div>
        @endif
    </div>
    <div class="col-md-4">
        <span class="text-muted">Hanya file <b style="color:red"> .png</b> yg di perbolehkan</span>
    </div>
</div>


<div class="d-flex justify-content-center">
    <img src="{{ asset(optional($pejabat)->s_filettd) }}" id="preview" class="img-fluid" alt="logo-s_filettd" style="width: 300px;">
</div>

<a href="/setting-pejabat" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>

@push('scripts')
<script type="text/javascript">
    $(document).on("click", "#s_filettd", () => {
        var file = $(this).parents().find("#file");
        file.trigger("click");
    });

    $('input[type="file"]').change((e) => {
        var input = document.querySelector('input[type=file]');
        var file = input.files[0];

        var reader = new FileReader();
        reader.onload = (e) => {
            document.getElementById("preview").src = e.target.result;
        };

        reader.readAsDataURL(file);
    });

    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert").slideUp(500);
    });

</script>
@endpush
