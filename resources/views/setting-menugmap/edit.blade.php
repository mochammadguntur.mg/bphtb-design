@extends('layouts.master')

@section('judulkiri')
    PENGATURAN MENU GMAP [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="{{ url('setting-menugmap/' . $menugmap->s_id_setmenugmap . '/edit') }}" method="POST" enctype="multipart/form-data">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-menugmap.partials.form')
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
