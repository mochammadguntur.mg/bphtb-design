@extends('layouts.master')

@section('judulkiri')
PENGATURAN TARGET STATUS
@endsection

@section('content')
<div class="card card-outline card-info shadow-none">
    <div class="card-header">
        <h3 class="card-title">
            <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#modal-create">
                <i class="fas fa-plus"></i> Tambah
            </button>
        </h3>

        <div class="card-tools">
            <a href="{{ url('setting-target') }}" class="btn btn-success" id="btnExcel">
                <i class="fas fa-arrow-left"></i> Kembali
            </a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                <thead>
                    <tr>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Status Target</th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">CreateAt</th>
                        <th scope='col' style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi </th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="7"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix pagination-footer">
        </div>
    </div>
</div>

@include('setting-target-status.partials.modal-create')
@include('setting-target-status.partials.modal-edit')
@include('setting-target-status.partials.modal-delete')
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(function (){
        if ('{{ session("success") }}') {
            toastr.success('{{ session("success") }}')
        }

        if ('{{ session("error") }}') {
            toastr.error('{{ session("error") }}')
        }

        datatables.reload();
    });
    var datatables = datagrid({
        url: 'setting-target-status/datagrid',
        table: "#datagrid-table",
        columns: [{
                class: "text-left"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            }
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "s_nama_status"
            },
            {
                sortable: false
            },
            {
                sortable: false
            }
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]
    });

    function getDataStatus(a, b) {
        $.ajax({
            url: `setting-target-status/detail/${ a }`,
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            if (b == 1) {
                $('#idUpdate').val(data.s_id_target_status);
                $('#namaUpdate').val(data.s_nama_status);
            }
            else if (b == 2) {
                $("#idHapus").val(data.s_id_target_status);
                $("#targetStatusHapus").val(data.s_nama_status);
            }
        });

        if (b == 1) {
            $("#modal-edit").modal('show');
        } 
        else if (b == 2) {
            $("#modal-delete").modal('show');
        }
    }

    $("#btnUpdate").click(function() {
        var id = $("#idUpdate").val();

        $.ajax({
            url: "setting-target-status/" + id,
            method: "PUT",
            data: {
                // _method: "PUT",
                _token: "{{ csrf_token() }}",
                s_id_target_status: id,
                s_nama_status: $('#namaUpdate').val()
            }
        }).then(function(data) {
            $("#modal-edit").modal("hide");
            datatables.reload();
            toastr.success(data[0]);
        });
    });

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();

        $.ajax({
            url: "setting-target-status/" + id,
            method: "DELETE",
            data: {
                _token: "{{ csrf_token() }}",
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            datatables.reload();
            toastr.success(data['success']);
        });
    });

</script>
@endpush