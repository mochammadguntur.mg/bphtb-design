<div class="modal fade" id="modal-create">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"> &nbsp;TAMBAH DATA</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{ url('setting-target-status') }}" method="POST">
            <div class="modal-body">
                <div class="row">
                    @csrf
                    <div class="col-sm-4">Status Target</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" name="s_nama_status" id="s_nama_status" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-info btn-sm" data-dismiss="modal">
                    Tutup
                </button>
                <button type="submit" class="btn btn-primary btn-sm" id="btnSimpan">
                    Simpan
                </button>
            </div>
            </form>
        </div>
    </div>
</div>