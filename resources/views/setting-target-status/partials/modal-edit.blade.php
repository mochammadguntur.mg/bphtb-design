<div class="modal fade" id="modal-edit">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title"> &nbsp;UBAH DATA</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    @csrf
                    <div class="col-sm-4">Status Target</div>
                    <div class="col-sm-8">
                        <input type="hidden" name="idUpdate" id="idUpdate">
                        <input type="text" class="form-control form-control-sm" name="namaUpdate" id="namaUpdate" required>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-info btn-sm" data-dismiss="modal">
                    Tutup
                </button>
                <button type="button" class="btn btn-primary btn-sm" id="btnUpdate">
                    Update
                </button>
            </div>
        </div>
    </div>
</div>