<!DOCTYPE html>
<html>
    <head>
        @php
        $data = Illuminate\Support\Facades\DB::table('s_background')->where('s_id_status',1)->get('s_thumbnail');
        $pemda = Illuminate\Support\Facades\DB::table('s_pemda')->first();
        @endphp

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>My BPHTB {{ strtoupper($pemda->s_namakabkot) }} || Login</title>

        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- Tell the browser to be responsive to screen width -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="{{ asset('storage/'. $pemda->s_logo .'') }}">

        <!-- Font Awesome -->
        <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
        <!-- Ionicons -->
        <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
        <!-- icheck bootstrap -->
        <link rel="stylesheet" href="{{asset('plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
        <!-- Theme style -->
        <link rel="stylesheet" href="{{asset('dist/css/adminlte.min.css')}}">
        <!-- Google Font: Source Sans Pro -->
        <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
        {!! NoCaptcha::renderJs() !!}

        <style>
            .login-page,
            .register-page {
                background-image: url("storage/{{ $data[0]->s_thumbnail }}");
                background-size: cover;
                background-repeat: no-repeat;
            }

            .login-card-body {
                background: rgba(255,255,255,0.04);
                /* box-shadow: -1px 4px 28px 0px rgba(0,0,0,0.75); */
            }

            .login-box .card {
                border-radius: 30px !important;
            }
        </style>
    </head>
    <body class="hold-transition login-page">
        <div class="login-box">
            <div class="card">
                <div class="card-body login-card-body">
                    <a href="{{ url('') }}">
                        <div class="login-logo mt-3">
                            <img src="{{ asset('dist/img/mybphtb.png') }}" class="img-rounded p-2"
                                 alt="User Image" style="width: 200px">
                        </div>
                    </a>
                    <p class="login-box-msg">Log In untuk memulai sesi aplikasi.</p>
                    <form method="POST" action="{{ route('login') }}" class="mt-2">
                        @csrf
                        <div class="input-group mb-3">
                            <input id="username" type="text" class="form-control @error('username') is-invalid @enderror"
                                   name="username" placeholder="Username" value="{{ old('email') }}" required autofocus autocomplete="off">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-envelope"></span>
                                </div>
                            </div>
                            @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="input-group mb-3">
                            <input id="password" type="password" placeholder="Password" class="form-control @error('password') is-invalid @enderror" name="password">
                            <div class="input-group-append">
                                <div class="input-group-text">
                                    <span class="fas fa-lock"></span>
                                </div>
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="input-group mb-3">
                            <?php $cek_setcaptcha = (new \ComboHelper())->cek_setcaptcha(); ?>
                            @if($cek_setcaptcha->s_id_statusmenucaptcha == 1)
                            {!! NoCaptcha::display() !!}
                            @endif
                            @if ($errors->has('g-recaptcha-response'))
                            <span class="help-block" style="color: red">
                                {{ $errors->first('g-recaptcha-response') }}
                            </span>
                            @endif
                        </div>

                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-primary btn-block">Log In</button>
                            </div>
                        </div>
                    </form>

                </div>
            </div>
            <!-- /.login-box -->

            <!-- jQuery -->
            <script src="{{asset('plugins/jquery/jquery.min.js')}}"></script>
            <!-- Bootstrap 4 -->
            <script src="{{asset('plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
            <!-- AdminLTE App -->
            <script src="{{asset('dist/js/adminlte.min.js')}}"></script>

    </body>
</html>
