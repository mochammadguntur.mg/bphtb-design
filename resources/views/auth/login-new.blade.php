<link rel="stylesheet" type="text/css" href="{{ asset('css/login.css') }}">
<link href="{{ asset('css/animate-custom.css') }}" rel="stylesheet">
<style>
* {
padding: 0;
margin: 0;
box-sizing: border-box;
}

html body{
font-family: 'Poppins', sans-serif !important;
overflow: hidden !important;
background-color: white !important;
font-weight: 800 !important;
}

.wave {
position: fixed;
bottom: 0;
left: 0;
height: 100%;
z-index: -1;
}

.containers {
width: 100vw;
height: 100vh;
display: grid;
grid-template-columns: repeat(2, 1fr);
grid-gap: 7rem;
padding: 0 2rem;
}

.img {
display: flex;
justify-content: flex-end;
align-items: center;
}

.login-content {
display: flex;
justify-content: flex-start;
align-items: center;
text-align: center;
margin-left: 100px;
}

.img img {
width: 500px;
}

form {
width: 360px;
}

.login-content img {
height: 100px;
}

.login-content h2 {
margin: 15px 0;
color: #333;
text-transform: uppercase;
font-size: 4rem;
font-weight: 700 !important;
font-family: 'Poppins', sans-serif !important;
}

.login-content h4 {
margin-top: -15px;
color: #333;
font-weight: 700 !important;
text-transform: uppercase;
font-size: 2rem;
font-family: 'Poppins', sans-serif !important;
}

.login-content .input-div {
position: relative;
display: grid;
grid-template-columns: 7% 93%;
margin: 25px 0;
padding: 5px 0;
border-bottom: 2px solid #d9d9d9;
}

.login-content .input-div.one {
margin-top: 0;
}

.i {
color: #d9d9d9;
display: flex;
justify-content: center;
align-items: center;
}

.i i {
transition: .3s;
}

.lambang-atas {
margin-bottom: 30px;
}

.input-div>div {
position: relative;
height: 45px;
}

.input-div>div>h5 {
position: absolute;
left: 10px;
top: 50%;
transform: translateY(-50%);
color: #999;
font-size: 18px;
transition: .3s;
}

.input-div:before,
.input-div:after {
content: '';
position: absolute;
bottom: -2px;
width: 0%;
height: 2px;
background-color: #38d39f;
transition: .4s;
}

.input-div:before {
right: 50%;
}

.input-div:after {
left: 50%;
}

.input-div.focus:before,
.input-div.focus:after {
width: 50%;
}

.input-div.focus>div>h5 {
top: -5px;
font-size: 15px;
}

.input-div.focus>.i>i {
color: #38d39f;
}

.input-div>div>input {
position: absolute;
left: 0;
top: 0;
width: 100%;
height: 100%;
border: none;
outline: none;
background: none;
padding: 0.5rem 0.7rem;
font-size: 1.2rem;
color: #555;
font-family: 'poppins', sans-serif !important;
}

.input-div.pass {
margin-bottom: 4px;
}

a {
display: block;
text-align: right;
text-decoration: none;
color: #999;
font-size: 0.9rem;
transition: .3s;
}

a:hover {
color: #38d39f;
}

.btn {
display: block !important;
width: 100% !important;
height: 50px !important;
border-radius: 25px !important;
outline: none !important;
border: none !important;
background-image: linear-gradient(to right, #32be8f, #38d39f, #32be8f) !important;
background-size: 200% !important;
font-size: 1.2rem !important;
color: #fff !important;
font-family: 'Poppins', sans-serif !important !important;
text-transform: uppercase !important;
margin: 1rem 0 !important;
cursor: pointer !important;
transition: .5s !important;
font-weight: bolder !important;
}

.btn:hover {
background-position: right !important;
color: black !important;
}

.input-div h5{
margin-top: -1.5px !important;
font-weight: 600;
font-family: 'Poppins', sans-serif !important;
}


@media screen and (max-width: 1050px) {
.container {
    grid-gap: 5rem;
}
}

@media screen and (max-width: 1000px) {
form {
    width: 290px;
}

.login-content h2 {
    font-size: 2.4rem;
    margin: 8px 0;
}

.img img {
    width: 400px;
}
}

@media screen and (max-width: 900px) {
.container {
    grid-template-columns: 1fr;
}

.img {
    display: none;
}

.wave {
    display: none;
}

.login-content {
    justify-content: center;
    /*margin: 10px 2;*/
}
.containers{
    margin-left: -20px !important;
}
}

@media screen and (max-width: 550px) {
.containers{
    margin-left: -70px !important;
}
}
</style>
<style>
    .loader {
    border: 16px solid #f3f3f3;
    border-radius: 50%;
    border-top: 16px solid #3498db;
    width: 120px;
    height: 120px;
    -webkit-animation: spin 2s linear infinite; /* Safari */
    animation: spin 2s linear infinite;
    margin-top: -450px;
    margin-left: 600px;
    }

    /* Safari */
    @-webkit-keyframes spin {
    0% { -webkit-transform: rotate(0deg); }
    100% { -webkit-transform: rotate(360deg); }
    }

    @keyframes spin {
    0% { transform: rotate(0deg); }
    100% { transform: rotate(360deg); }
    }
    /* .loader {
        border-top: 16px solid blue;
        border-right: 16px solid green;
        border-bottom: 16px solid red;
    } */

    /* .pas-loader{
        background-color: black !important;
        opacity:0.8 !important;
        background-color: Black !important;
        z-index:10 !important;
    } */
</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
{!! NoCaptcha::renderJs() !!}
<script src="{{ asset('js/login.js') }}"></script>
<!-- <link href="https://fonts.googleapis.com/css?family=Poppins:600&display=swap" rel="stylesheet"> -->
<!-- <script src="https://kit.fontawesome.com/a81368914c.js"></script> -->
<img class="wave" src="{{ asset('dist/img/wave.png') }}">
<div class="containers">
    <div class="img">
        <img class="gambar-hp" src="{{ asset('dist/img/bg.svg') }}">
    </div>
    <div class="login-content">
        <form method="POST" action="{{ route('login') }}" class="mt-2">
            <div class="row lambang-atas">
                <div class="">
                    <img src="{{ asset('dist/img/Lambang_Kabupaten_Barito_Kuala.png') }}">
                </div>
                <div class="">
                    <h2 class="title" style="font-size: 200%">APLIKASI E-BPHTB</h2>
                    <h4 class="title" style="font-size: 150%">KABUPATEN BARITO KUALA</h4>
                </div>
            </div>
               <div class="input-div one">
                  <div class="i">
                          <i class="fa fa-user"></i>
                  </div>
                  <div class="div">
                    <input id="username" type="text" class="form-control @error('username') is-invalid @enderror input" style="height:40px;widht:90%;font-size:16px;font-family:arial"
                    name="username" placeholder="Username" value="{{ old('email') }}" required autofocus>
                    <div class="input-group-append">
                    {{-- <div class="input-group-text">
                        <span class="fas fa-envelope"></span>
                    </div> --}}
                </div>
                  </div>
               </div>
               <div class="input-div pass">
                  <div class="i">
                       <i class="fa fa-unlock-alt"></i>
                  </div>
                  <div class="div">
                    <input id="password" type="password" placeholder="Password" class="form-control input @error('password') is-invalid @enderror" name="password">
                    <div class="input-group-append">
                        {{-- <div class="input-group-text">
                            <span class="fas fa-lock"></span>
                        </div> --}}
                        @error('password')
                        <span class="invalid-feedback" role="alert">
                            <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
               </div>
            </div>
            <div class="input-group mb-3">
                {!! NoCaptcha::display() !!}
                @if ($errors->has('g-recaptcha-response'))
                <span class="help-block" style="color: red">
                    {{ $errors->first('g-recaptcha-response') }}
                </span>
                @endif
            </div>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary btn-block" style="font-size:14px;widht:100%;">Log In</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- <div class="loader"></div> -->

<script>
    // $('.loader').hide();

    // function loader(){
    // 	if (($('#password').val() != '') && ($('#username').val() != '')) {
    // 		$("body").addClass('pas-loader');
    // 		$(".containers").addClass('pas-loader');
    // 		$(".wave").addClass('pas-loader');
    // 		$(".gambar-hp").addClass('pas-loader');
    // 		$('.loader').show();
    // 	}
    // }

    const inputs = document.querySelectorAll(".input");
        function addcl(){
            let parent = this.parentNode.parentNode;
            parent.classList.add("focus");
        }

        function remcl(){
            let parent = this.parentNode.parentNode;
            if(this.value == ""){
                parent.classList.remove("focus");
            }
        }


        inputs.forEach(input => {
            input.addEventListener("focus", addcl);
            input.addEventListener("blur", remcl);
        });

</script>
