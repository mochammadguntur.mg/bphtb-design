<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">TAHUN</label>
    <div class="col-md-4">
        <input type="text" name="s_thntgkpbb" class="form-control @error('s_thntgkpbb') is-invalid @enderror" id="s_nippejabat" placeholder="TAHUN" value="{{ old('s_thntgkpbb') ?? $menutunggakanpbb->s_thntgkpbb }}">
    </div>
    
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">RENTANG TAHUN</label>
    <div class="col-md-4">
        <input type="text" name="s_rentangthntgkpbb" class="form-control @error('s_rentangthntgkpbb') is-invalid @enderror" id="s_nippejabat" placeholder="TAHUN" value="{{ old('s_rentangthntgkpbb') ?? $menutunggakanpbb->s_rentangthntgkpbb }}">
    </div>
    
</div>


<a href="/setting-menutunggakantahunpbb" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>

@push('scripts')
<script type="text/javascript">
    

    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert").slideUp(500);
    });

</script>
@endpush
