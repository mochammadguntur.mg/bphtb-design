@extends('layouts.master')

@section('judulkiri')
    PENGATURAN HARGA ACUAN JENIS TANAH
@endsection

@section('content')
    <div class="card card-outline card-info shadow-sm">
        <div class="card-header">
            <h3 class="card-title">
                <a href="{{ url('setting-acuan-tanah/create') }}" class="btn btn-primary">
                    <i class="fas fa-plus"></i> Tambah
                </a>
            </h3>
            <div class="card-tools">
                <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                    <i class="fas fa-file-excel"></i> Excel
                </a>
                <a href="javascript:void(0)" class="btn btn-danger" id="btnPDF">
                    <i class="fas fa-file-pdf"></i> PDF
                </a>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                    <thead>
                        <tr>
                            <th scope="col" style="background-color: #227dc7; color:white;" class="text-center">No </th>
                            <th scope="col" style="background-color: #227dc7; color:white;" class="text-center">KD Propinsi </th>
                            <th scope="col" style="background-color: #227dc7; color:white;" class="text-center">KD Dati2 </th>
                            <th scope="col" style="background-color: #227dc7; color:white;" class="text-center">KD Kecamatan </th>
                            <th scope="col" style="background-color: #227dc7; color:white;" class="text-center">KD Kelurahan </th>
                            <th scope="col" style="background-color: #227dc7; color:white;" class="text-center">KD Blok </th>
                            <th scope="col" style="background-color: #227dc7; color:white;" class="text-center">Permeter Tanah </th>
                            <th scope="col" style="background-color: #227dc7; color:white;" class="text-center">Jenis Tanah </th>
                            <th scope="col" style="background-color: #227dc7; color:white;" class="text-center">CreateAt </th>
                            <th scope="col" style="background-color: #227dc7; color:white;" class="text-center">Aksi </th>
                        </tr>
                        <tr>
                            <th scope="filter"></th>
                            <th scope="filter">
                                <input type="text" class="form-control form-control-sm" id="filter-kd_propinsi">
                            </th>
                            <th scope="filter">
                                <input type="text" class="form-control form-control-sm" id="filter-kd_dati2">
                            </th>
                            <th scope="filter">
                                <input type="text" class="form-control form-control-sm" id="filter-kd_kecamatan">
                            </th>
                            <th scope="filter">
                                <input type="text" class="form-control form-control-sm" id="filter-kd_kelurahan">
                            </th>
                            <th scope="filter">
                                <input type="text" class="form-control form-control-sm" id="filter-kd_blok">
                            </th>
                            <th scope="filter">
                                <input type="text" class="form-control form-control-sm" id="filter-permeter_tanah">
                            </th>
                            <th scope="filter">
                                <select class="form-control form-control-sm" id="filter-jenis_tanah"></select>
                            </th>
                            <th scope="filter">
                                <input type="text" class="form-control form-control-sm" id="filter-created_at" readonly>
                            </th>
                            <th scope="filter"></th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="text-center" colspan="10"> Tidak ada data.</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="card-footer clearfix pagination-footer">
            </div>
        </div>
    </div>
    <div class="modal fade" id="modal-delete">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header bg-danger">
                    <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                    <div class="row">
                        <div class="col-sm-4">Kode Propinsi</div>
                        <div class="col-sm-8">
                            <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                            <input type="text" class="form-control form-control-sm" id="kode_propinsiHapus" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">Kode Dati2</div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kode_dati2Hapus" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">Kode Kecamatan</div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kode_kecamatanHapus" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">Kode Kelurahan</div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kode_kelurahanHapus" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">Kode Blok</div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="kode_blokHapus" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">Permeter Tanah</div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="permeter_tanahHapus" readonly>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-4">Jenis Tanah</div>
                        <div class="col-sm-8">
                            <input type="text" class="form-control form-control-sm" id="jenis_tanahHapus" readonly>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tutup</button>
                    <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                        Hapus</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ('{{ session("success") }}') {
            toastr.success('{{ session("success") }}')
        }

        if ('{{ session("error") }}') {
            toastr.error('{{ session("error") }}')
        }

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        comboJenisTanah();
    });

    var datatables = datagrid({
        url: 'setting-acuan-tanah/datagrid',
        table: "#datagrid-table",
        columns: [
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center" 
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-right"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: ""
            }
            
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "kd_propinsi"
            },
            {
                sortable: true,
                name: "kd_dati2"
            },
            {
                sortable: true,
                name: "kd_kecamatan"
            },
			{
                sortable: true,
                name: "kd_kelurahan"
            },
			{
                sortable: true,
                name: "kd_blok"
            },
			{
                sortable: true,
                name: "permeter_tanah"
            },
			{
                sortable: false,
                name: "jenis_tanah" 
            },
			{
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]

    });

    $("#filter-kd_propinsi, #filter-kd_dati2, #filter-kd_kecamatan, #filter-kd_kelurahan, #filter-kd_blok, #filter-permeter_tanah, #filter-jenis_tanah, #filter-created_at").change(function() {
        search();
    });

    $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search();
    });

    $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

    function search() {
        datatables.setFilters({
            kd_propinsi: $("#filter-kd_propinsi").val(),
			kd_dati2: $("#filter-kd_dati2").val(),
			kd_kecamatan: $("#filter-kd_kecamatan").val(),
			kd_kelurahan: $("#filter-kd_kelurahan").val(),
			kd_blok: $("#filter-kd_blok").val(),
			permeter_tanah: $("#filter-permeter_tanah").val(),
            jenis_tanah: $("#filter-jenis_tanah").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: `setting-acuan-tanah/detail/${ a }`,
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            // console.log(data);
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data.s_idacuan_jenis);
			$("#kode_propinsiHapus").val(data.s_kd_propinsi);
			$("#kode_dati2Hapus").val(data.s_kd_dati2);
			$("#kode_kecamatanHapus").val(data.s_kd_kecamatan);
			$("#kode_kelurahanHapus").val(data.s_kd_kelurahan);
			$("#kode_blokHapus").val(data.s_kd_blok);
			$("#permeter_tanahHapus").val(data.s_permetertanah);
            $("#jenis_tanahHapus").val(data.jenis_tanah.nama_jenistanah);
        });
        $("#modal-delete").modal('show');
    }

    function comboJenisTanah() {
        const select = document.getElementById('filter-jenis_tanah');
        let jenis_tanah = JSON.parse('{!! json_encode($jenis_tanah) !!}');
        let option = '<option class="pl-5" value="">Pilih</option>';

        jenis_tanah.forEach(el => {
            option += `<option value="${ el.id_jenistanah }">${ el.nama_jenistanah }</option>`
        });

        select.innerHTML = option;
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();

        $.ajax({
            url: "setting-acuan-tanah/" + id,
            method: "DELETE",
            data: {
                _token: "{{ csrf_token() }}",
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            datatables.reload();
            toastr.success(data['success']);
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        // console.log(a);
		var kd_propinsi = $("#filter-kd_propinsi").val();
		var kd_dati2 = $("#filter-kd_dati2").val();
		var kd_kecamatan = $("#filter-kd_kecamatan").val();
		var kd_kelurahan = $("#filter-kd_kelurahan").val();
		var kd_blok = $("#filter-kd_blok").val();
        var permeter_tanah = $("#filter-permeter_tanah").val();
        var jenis_tanah = $("#filter-jenis_tanah").val();
        var created_at = $("#filter-created_at").val();

        if (a.target.id == 'btnExcel') {
            // window.open('/setting-jenis-transaksi/export_xls?s_kodejenistransaksi=' + s_kodejenistransaksi + '&s_namajenistransaksi=' + s_namajenistransaksi + '&created_at=' + created_at);
            window.open(`{{ route('setting-acuan-tanah.export_xls') }}?kd_propinsi=${ kd_propinsi }&kd_dati2=${ kd_dati2 }&kd_kecamatan=${ kd_kecamatan }&kd_kelurahan=${ kd_kelurahan }&kd_blok=${ kd_blok }&permeter_tanah=${ permeter_tanah }&jenis_tanah=${ jenis_tanah }&created_at=${ created_at }`);
        } else {
            window.open(`{{ route('setting-acuan-tanah.export_pdf') }}?kd_propinsi=${ kd_propinsi }&kd_dati2=${ kd_dati2 }&kd_kecamatan=${ kd_kecamatan }&kd_kelurahan=${ kd_kelurahan }&kd_blok=${ kd_blok }&permeter_tanah=${ permeter_tanah }&jenis_tanah=${ jenis_tanah }&created_at=${ created_at }`);
        }

    });

    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").slideUp(500);
    });
</script>
@endpush