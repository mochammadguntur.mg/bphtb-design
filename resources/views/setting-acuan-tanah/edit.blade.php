@extends('layouts.master')

@section('judulkiri')
    PENGATURAN HARGA ACUAN JENIS TANAH [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                
                <form action="{{ url('setting-acuan-tanah/'. $acuantanah->s_idacuan_jenis) }}" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-acuan-tanah.partials.form', ['submit'=> 'Update', 'icon' => 'fas fa-save'])
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
