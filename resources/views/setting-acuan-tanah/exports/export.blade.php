<table style="border-collapse: collapse">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">No</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold;">Kode Propinsi</th>
			<th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold;">Kode Dati2</th>
			<th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold;">Kode Kecamatan</th>
			<th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold;">Kode Kelurahan</th>
			<th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold;">Kode Blok</th>
			<th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold;">Permeter Tanah</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold;">Jenis Tanah</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold;">Create At</th>
        </tr>
    </thead>
    <tbody>
        @foreach($acuantanah as $index => $acuantanah)
        {{ error_reporting(0) }}
        <tr>
            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $index + 1 }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $acuantanah->s_kd_propinsi }}</td>
			<td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $acuantanah->s_kd_dati2 }}</td>
			<td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $acuantanah->s_kd_kecamatan }}</td>
			<td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $acuantanah->s_kd_kelurahan }}</td>
			<td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $acuantanah->s_kd_blok }}</td>
			<td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $acuantanah->s_permetertanah }}</td>
			<td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $acuantanah->jenisTanah->nama_jenistanah }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ date('d/m/Y', strtotime($acuantanah->created_at)) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>