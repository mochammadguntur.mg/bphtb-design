<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Propinsi</label>
    <div class="col-md-4">
        <input onkeypress="return numberOnly(event)" type="text" name="s_kd_propinsi" class="form-control @error('s_kd_propinsi') is-invalid @enderror" id="s_kd_propinsi" placeholder="Kode Propinsi"
               value="{{ old('s_kd_propinsi') ?? $acuan->s_kd_propinsi }}" maxlength="2">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kd_propinsi'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_kd_propinsi') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Dati II</label>
    <div class="col-md-4">
        <input onkeypress="return numberOnly(event)" type="text" name="s_kd_dati2" class="form-control @error('s_kd_dati2') is-invalid @enderror" id="s_kd_dati2" placeholder="Kode Dati II"
               value="{{ old('s_kd_dati2') ?? $acuan->s_kd_dati2 }}" maxlength="2">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kd_dati2'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_kd_dati2') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Kecamatan</label>
    <div class="col-md-4">
        <input onkeypress="return numberOnly(event)" type="text" name="s_kd_kecamatan" class="form-control @error('s_kd_kecamatan') is-invalid @enderror" id="s_kd_kecamatan" placeholder="Kode Kecamatan"
               value="{{ old('s_kd_kecamatan') ?? $acuan->s_kd_kecamatan }}" maxlength="3">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kd_kecamatan'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_kd_kecamatan') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Kelurahan</label>
    <div class="col-md-4">
        <input onkeypress="return numberOnly(event)" type="text" name="s_kd_kelurahan" class="form-control @error('s_kd_kelurahan') is-invalid @enderror" id="s_kd_kelurahan" placeholder="Kode Kelurahan"
               value="{{ old('s_kd_kelurahan') ?? $acuan->s_kd_kelurahan }}" maxlength="3">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kd_kelurahan'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_kd_kelurahan') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Blok</label>
    <div class="col-md-4">
        <input onkeypress="return numberOnly(event)" type="text" name="s_kd_blok" class="form-control @error('s_kd_blok') is-invalid @enderror" id="s_kd_blok" placeholder="Kode Blok"
               value="{{ old('s_kd_blok') ?? $acuan->s_kd_blok }}" maxlength="3">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kd_blok'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_kd_blok') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Per meter tanah</label>
    <div class="col-md-4">
        <input onkeypress="return numberOnly(event)" type="text" name="s_permetertanah" class="form-control @error('s_permetertanah') is-invalid @enderror" id="s_permetertanah" placeholder="Per meter tanah"
               value="{{ old('s_permetertanah') ?? $acuan->s_permetertanah }}" style="text-align: right">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_permetertanah'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_permetertanah') }}
        </div>
        @endif
    </div>
</div>

<a href="{{ url('setting-harga-acuan') }}" class="btn btn-danger"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>

@push('scripts')
<script type="text/javascript">
    function numberOnly(e) {
        var input = (e.which) ? e.which : e.keyCode

        if (input > 31 && (input < 48 || input > 57))
            return false;
        return true;
    }

    var permeter_tanah = document.getElementById('s_permetertanah');
    permeter_tanah.addEventListener('keyup', function (e)
    {
        permeter_tanah.value = formatRupiah(this.value);
    });

    permeter_tanah.addEventListener('keydown', function (event)
    {
        limitCharacter(event);
    });

    /* Fungsi */
    function formatRupiah(bilangan, prefix)
    {
        var number_string = bilangan.replace(/[^,\d]/g, '').toString(),
                split = number_string.split(','),
                sisa = split[0].length % 3,
                rupiah = split[0].substr(0, sisa),
                ribuan = split[0].substr(sisa).match(/\d{1,3}/gi);

        if (ribuan) {
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return prefix == undefined ? rupiah : (rupiah ? 'Rp. ' + rupiah : '');
    }

    function limitCharacter(event)
    {
        key = event.which || event.keyCode;
        if (key != 188 // Comma
                && key != 8 // Backspace
                && key != 17 && key != 86 & key != 67 // Ctrl c, ctrl v
                && (key < 48 || key > 57) // Non digit, etc
                )
        {
            event.preventDefault();
            return false;
        }
    }
</script>
@endpush
