@extends('layouts.master')

@section('judulkiri')
PENGATURAN HARGA ACUAN
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible callout callout-success" id="alert">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
              {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-dismissible callout callout-danger" id="alert">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-ban"></i> Gagal!</h5>
              {{ session('error') }}
            </div>
        @endif
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="setting-harga-acuan/create" class="btn btn-primary">
                    <i class="fas fa-plus"></i> Tambah
                    </a>
                </h3>
                <div class="card-tools">
                    <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                        <i class="fas fa-file-excel"></i> Excel
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger"  id="btnPDF">
                        <i class="fas fa-file-pdf"></i> PDF
                    </a>
                </div>
            </div>
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">KD Propinsi</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">KD Dati II</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">KD Kecamatan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">KD Kelurahan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">KD Blok</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Harga Per Meter Tanah</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Create At</th>
                                <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-s_kd_propinsi"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-s_kd_dati2"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-s_kd_kecamatan"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-s_kd_kelurahan"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-s_kd_blok"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-s_permetertanah"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-created_at" readonly></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="9"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Harga Acuan</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="permetertanah" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>


@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
        url: 'setting-harga-acuan/datagrid',
        table: "#datagrid-table",
        columns: [
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-right"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },

        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "s_kd_propinsi"
            },
            {
                sortable: true,
                name: "s_kd_dati2"
            },
            {
                sortable: true,
                name: "s_kd_kecamatan"
            },
            {
                sortable: true,
                name: "s_kd_kelurahan"
            },
            {
                sortable: true,
                name: "s_kd_blok"
            },
            {
                sortable: true,
                name: "s_permetertanah"
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]

    });

    $("#filter-s_kd_propinsi, #filter-s_kd_propinsi").keyup(function() {
        search();
    });
    $("#filter-s_kd_dati2, #filter-s_kd_dati2").keyup(function() {
        search();
    });
    $("#filter-s_kd_kecamatan, #filter-s_kd_kecamatan").keyup(function() {
        search();
    });
    $("#filter-s_kd_kelurahan, #filter-s_kd_kelurahan").keyup(function() {
        search();
    });
    $("#filter-s_kd_blok, #filter-s_kd_blok").keyup(function() {
        search();
    });
    $("#filter-s_permetertanah, #filter-s_permetertanah").keyup(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            kodepropinsi: $("#filter-s_kd_propinsi").val(),
            kodedati2: $("#filter-s_kd_dati2").val(),
            kodekecamatan: $("#filter-s_kd_kecamatan").val(),
            kodekelurahan: $("#filter-s_kd_kelurahan").val(),
            kodeblok: $("#filter-s_kd_blok").val(),
            permetertanah: $("#filter-s_permetertanah").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'setting-harga-acuan/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].s_idacuan);
            $("#permetertanah").val(data[0].s_permetertanah);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'setting-harga-acuan/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Berhasil dihapus!')
            datatables.reload();
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        var kd_propinsi = $("#filter-kd_propinsi").val();
        var permetertanah = $("#filter-s_permetertanah").val();
        var created_at = $("#filter-created_at").val();
        if(a.target.id == 'btnExcel'){
            window.open('{{ url('setting-harga-acuan/export_xls') }}?kd_propinsi=' + kd_propinsi + '&permetertanah=' + permetertanah + '&created_at=' + created_at);
        }else{
            window.open('{{ url('setting-harga-acuan/export_pdf') }}?kd_propinsi=' + kd_propinsi + '&permetertanah=' + permetertanah + '&created_at=' + created_at);
        }

    });

    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").slideUp(500);
    });

</script>
@endpush
