@extends('layouts.master')

@section('judulkiri')
KPP PRATAMA
@endsection

@section('content')


<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                    
                    <!-- <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                        Excel
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger" id="btnPDF">
                        PDF
                    </a> -->
                </h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip"
                        title="Collapse">
                        <i class="fas fa-minus"></i></button>
                </div>
            </div>
            
            @php
                $th_awal = '<th style="background-color: #227dc7; color:white; " class="text-center">';
                $th_end = '</th>';
                
            @endphp
            
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                {!! $th_awal!!}No{!! $th_end !!}
                                
                                @foreach ($data_field_alias as $a)
                                    {!! $th_awal!!}{{$a}}{!! $th_end !!}
                                @endforeach
                                
                                
                            </tr>
                            <tr>
                                <th></th>
                               
                                @foreach ($data_field as $b)
                                    @if($b == 't_idjenistransaksi')
                                        <th><select name="t_idjenistransaksi" id="t_idjenistransaksi" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_jenistransaksi as $djt)
                                                <option value="{{ $djt->s_idjenistransaksi }}"> {{ $djt->s_namajenistransaksi }}</option>
                                                @endforeach
                                            </select></th>
                                    @elseif ($b == 't_idnotaris_spt')
                                        <th><select name="t_idnotaris_spt" id="t_idnotaris_spt" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_notaris as $dn)
                                                <option value="{{ $dn->s_idnotaris }}"> {{ $dn->s_namanotaris }}</option>
                                                @endforeach
                                            </select></th>
                                    @elseif ($b == 't_idpersetujuan_bphtb')
                                        <th><select name="t_idpersetujuan_bphtb" id="t_idpersetujuan_bphtb" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                <option value="1">SETUJU</option>
                                                <option value="2">BELUM</option>
                                            </select></th>
                                    @elseif ($b == 's_id_status_berkas')
                                        <th><select name="s_id_status_berkas" id="s_id_status_berkas" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_statusberkas as $dsb)
                                                <option value="{{ $dsb->s_id_status_berkas }}"> {{ $dsb->s_nama_status_berkas }}</option>
                                                @endforeach
                                                <option value="3">Belum</option>
                                            </select></th>     
                                    @elseif ($b == 's_id_status_kabid')
                                        <th><select name="s_id_status_kabid" id="s_id_status_kabid" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_statuskabid as $dsk)
                                                <option value="{{ $dsk->s_id_status_kabid }}"> {{ $dsk->s_nama_status_kabid }}</option>
                                                @endforeach
                                                <option value="3">Belum</option>
                                            </select></th>       
                                    @elseif ($b == 't_id_pembayaran')
                                        <th><select name="t_id_pembayaran" id="t_id_pembayaran" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_statusbayar as $dsbyr)
                                                <option value="{{ $dsbyr->s_id_status_bayar }}"> {{ $dsbyr->s_nama_status_bayar }}</option>
                                                @endforeach
                                            </select></th>       
                                    @elseif ($b == 't_idbidang_usaha')
                                        <th><select name="t_idbidang_usaha" id="t_idbidang_usaha" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_jenisbidangusaha as $djb)
                                                <option value="{{ $djb->s_idbidang_usaha }}"> {{ $djb->s_nama_bidangusaha }}</option>
                                                @endforeach
                                            </select></th>   
                                    @elseif ($b == 't_idjenisfasilitas')
                                        <th><select name="t_idjenisfasilitas" id="t_idjenisfasilitas" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_jenisfasilitas as $djf)
                                                <option value="{{ $djf->s_idjenisfasilitas }}"> {{ $djf->s_namajenisfasilitas }}</option>
                                                @endforeach
                                            </select></th>       
                                    @elseif ($b == 't_idjenishaktanah')
                                        <th><select name="t_idjenishaktanah" id="t_idjenishaktanah" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_haktanah as $dht)
                                                <option value="{{ $dht->s_idhaktanah }}"> {{ $dht->s_namahaktanah }}</option>
                                                @endforeach
                                            </select></th>         
                                    @elseif ($b == 't_idjenisdoktanah')
                                        <th><select name="t_idjenisdoktanah" id="t_idjenisdoktanah" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_doktanah as $ddk)
                                                <option value="{{ $ddk->s_iddoktanah }}"> {{ $ddk->s_namadoktanah }}</option>
                                                @endforeach
                                            </select></th>             
                                    @elseif ($b == 't_idpengurangan')
                                        <th><select name="t_idpengurangan" id="t_idpengurangan" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_jenispengurangan as $djprg)
                                                <option value="{{ $djprg->s_idpengurangan }}"> {{ $djprg->s_namapengurangan }}</option>
                                                @endforeach
                                            </select></th>            
                                    @elseif ($b == 't_id_jenistanah')
                                        <th><select name="t_id_jenistanah" id="t_id_jenistanah" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_jenisperumahan as $djprmh)
                                                <option value="{{ $djprmh->id_jenistanah }}"> {{ $djprmh->nama_jenistanah }}</option>
                                                @endforeach
                                            </select></th>        
                                    @elseif ($b == 't_idjenisketetapan')
                                        <th><select name="t_idjenisketetapan" id="t_idjenisketetapan" class="form-control form-control-sm">
                                                <option value="">Pilih</option>
                                                @foreach ($data_jenisketetapan as $djkttp)
                                                <option value="{{ $djkttp->s_idjenisketetapan }}"> {{ $djkttp->s_namasingkatjenisketetapan }}</option>
                                                @endforeach
                                            </select></th>          
                                    @elseif ($b == 't_idspt')
                                        <th>CARI :</th>        
                                    @else
                                    <th ><input type="text" class="form-control form-control-sm" id="{{$b}}" ></th>
                                    @endif
                                @endforeach
                                
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="146"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Tarif</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="tarifHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Dasar Hukum</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="dasar_hukumHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagridmodif.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif

        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif

        $('#filter-tanggal_awal').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tanggal_awal').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tanggal_awal').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-tanggal_akhir').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tanggal_akhir').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tanggal_akhir').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
        url: 'kpp-pratama/datagrid',
        table: "#datagrid-table",
        columns: [{
                class: "text-center"
            },
            
            @foreach ($data_field as $e)
            { class: ""},
            @endforeach
            
            /* {
                class: "text-center"
            }, */
        ],
        orders: [{
                sortable: false
            },
            
            @foreach ($data_field as $c)
            { sortable: true,name: "{{$c}}" },
            @endforeach
            
            /* {
                sortable: false
            }, */
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]

    });


    $("{{$fix_data_field}}").change(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            @foreach ($data_field as $d)
                {{$d}}: $("#{{$d}}").val(),
            @endforeach
            
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'setting-tarif-bphtb/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].id);
            $("#tarifHapus").val(data[0].s_tarif_bphtb);
            $("#dasar_hukumHapus").val(data[0].s_dasar_hukum);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'setting-tarif-bphtb/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Berhasil dihapus!')
            datatables.reload();
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        var tarif = $("#filter-tarif").val();
        var dasar_hukum = $("#filter-dasar_hukum").val();
        var created_at = $("#filter-created_at").val();
        if(a.target.id == 'btnExcel'){
            window.open('/setting-tarif-bphtb/export_xls?tarif=' + tarif + '&dasar_hukum=' + dasar_hukum + '&created_at=' + created_at);
        }else{
            window.open('/setting-tarif-bphtb/export_pdf?tarif=' + tarif + '&dasar_hukum=' + dasar_hukum + '&created_at=' + created_at);
        }

    });
</script>
@endpush
