@extends('layouts.master')

@section('judulkiri')
PENGATURAN TARIF BPHTB [TAMBAH]
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary">
            <form action="/setting-tarif-bphtb/store" method="POST" enctype="multipart/form-data">
                <div class="card-body mt-4 mb-2">
                    @csrf
                    @include('setting-tarif-bphtb.partials.form', ['sumbit'=> 'Simpan'])
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
