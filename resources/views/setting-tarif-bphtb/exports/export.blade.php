<table style="border-collapse: collapse; width:100%">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">No</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Tarif</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Dasar Hukum</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Tanggal Awal</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Tanggal Akhir</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">Created at</th>
        </tr>
    </thead>
    <tbody>
        @foreach($tarifs as $index => $tarif)
        {{ error_reporting(0) }}
        <tr>
            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $index + 1 }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $tarif->s_tarif_bphtb }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $tarif->s_dasar_hukum }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $tarif->s_tgl_awal }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $tarif->s_tgl_akhir }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $tarif->created_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>