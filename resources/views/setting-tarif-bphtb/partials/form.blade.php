<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Tarif</label>
    <div class="col-md-4">
        <input onkeypress="return numberOnly(event)" type="text" name="s_tarif_bphtb" class="form-control @error('s_tarif_bphtb') is-invalid @enderror" id="s_tarif_bphtb" placeholder="0"
               value="{{ old('s_tarif_bphtb') ?? $tarif->s_tarif_bphtb }}" maxlength="2">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_tarif_bphtb'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_tarif_bphtb') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Dasar Hukum</label>
    <div class="col-md-4">
        <input type="text" name="s_dasar_hukum" class="form-control @error('s_dasar_hukum') is-invalid @enderror" id="s_dasar_hukum" placeholder="Dasar Hukum"
               value="{{ old('s_dasar_hukum') ?? $tarif->s_dasar_hukum }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_dasar_hukum'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_dasar_hukum') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Tanggal Awal</label>
    <div class="col-md-4">
        <input type="date" class="form-control @error('s_tgl_awal') is-invalid @enderror" name="s_tgl_awal" id="s_tgl_awal"
               value="{{ old('s_tgl_awal') ?? $tarif->s_tgl_awal }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_tgl_awal'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_tgl_awal') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Tanggal Akhir</label>
    <div class="col-md-4">
        <input type="date" class="form-control @error('s_tgl_akhir') is-invalid @enderror" name="s_tgl_akhir" id="s_tgl_akhir"
               value="{{ old('s_tgl_akhir') ?? $tarif->s_tgl_akhir }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_tgl_akhir'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_tgl_akhir') }}
        </div>
        @endif
    </div>
</div>
<a href="/setting-tarif-bphtb" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>

@push('scripts')
<script>
    function numberOnly(e) {
        var input = (e.which) ? e.which : e.keyCode

        if (input > 31 && (input < 48 || input > 57))
            return false;
        return true;
    }
</script>
@endpush