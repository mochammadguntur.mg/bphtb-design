@extends('layouts.master')

@section('judulkiri')
PENGATURAN TARIF BPHTB
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="setting-tarif-bphtb/create" class="btn btn-primary">
                        <i class="fas fa-plus"></i> Tambah
                    </a>
                </h3>
                <div class="card-tools">
                    <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                        <i class="fas fa-file-excel"></i> Excel
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger" id="btnPDF">
                        <i class="fas fa-file-pdf"></i> PDF
                    </a>
                </div>
            </div>
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Tarif</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">DasarHukum</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Tanggal Awal</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Tanggal Akhir</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Create At</th>
                                <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-tarif"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-dasar_hukum"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-tanggal_awal"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-tanggal_akhir"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-created_at" readonly></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="7"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Tarif</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="tarifHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Dasar Hukum</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="dasar_hukumHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
    @if (session('success'))
            toastr.success('{{session('success')}}')
            @endif

            @if (session('error'))
            toastr.error('{{session('error')}}')
            @endif

            $('#filter-tanggal_awal').daterangepicker({
    singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
            cancelLabel: 'Clear'
            }
    });
    $('#filter-tanggal_awal').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY'));
    search();
    });
    $('#filter-tanggal_awal').on('cancel.datepicker', function(ev, picker) {
    $(this).val('');
    search();
    });
    $('#filter-tanggal_akhir').daterangepicker({
    singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
            cancelLabel: 'Clear'
            }
    });
    $('#filter-tanggal_akhir').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY'));
    search();
    });
    $('#filter-tanggal_akhir').on('cancel.datepicker', function(ev, picker) {
    $(this).val('');
    search();
    });
    $('#filter-created_at').daterangepicker({
    autoUpdateInput: false,
            locale: {
            cancelLabel: 'Clear'
            }
    });
    $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
    search();
    });
    $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
    search();
    });
    });
    var datatables = datagrid({
    url: 'setting-tarif-bphtb/datagrid',
            table: "#datagrid-table",
            columns: [{
            class: "text-center"
            },
            {
            class: ""
            },
            {
            class: "text-center"
            },
            {
            class: "text-center"
            },
            {
            class: "text-center"
            },
            ],
            orders: [{
            sortable: false
            },
            {
            sortable: true,
                    name: "tarif"
            },
            {
            sortable: true,
                    name: "dasar_hukum"
            },
            {
            sortable: true,
                    name: "tanggal_awal"
            },
            {
            sortable: true,
                    name: "tanggal_akhir"
            },
            {
            sortable: true,
                    name: "created_at"
            },
            {
            sortable: false
            },
            ],
            action: [{
            name: 'edit',
                    btnClass: 'btn btn-warning btn-sm',
                    btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
            name: 'delete',
                    btnClass: 'btn btn-danger btn-sm',
                    btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
            ]

    });
    $("#filter-tarif, #filter-dasar_hukum, #filter-tanggal_awal, #filter-tanggal_akhir, #filter-status, #filter-created_at").change(function() {
    search();
    });
    function search() {
    datatables.setFilters({
    s_tarif_bphtb: $("#filter-tarif").val(),
            s_dasar_hukum: $("#filter-dasar_hukum").val(),
            s_tgl_awal: $("#filter-tanggal_awal").val(),
            s_tgl_akhir: $("#filter-tanggal_akhir").val(),
            created_at: $("#filter-created_at").val()
    });
    datatables.reload();
    }
    search();
    function showDeleteDialog(a) {
    $.ajax({
    url: 'setting-tarif-bphtb/detail/{' + a + '}',
            type: 'GET',
            data: {
            id: a
            }
    }).then(function(data) {
    $("#btnHapus").removeAttr("disabled");
    $("#idHapus").val(data[0].s_idtarifbphtb);
    $("#tarifHapus").val(data[0].s_tarif_bphtb);
    $("#dasar_hukumHapus").val(data[0].s_dasar_hukum);
    });
    $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
    var id = $("#idHapus").val();
    $.ajax({
    url: 'setting-tarif-bphtb/delete/{' + id + '}',
            method: "GET",
            data: {
            id: id
            }
    }).then(function(data) {
    $("#modal-delete").modal("hide");
    toastr.success('Data Berhasil dihapus!')
            datatables.reload();
    });
    });
    $("#btnExcel, #btnPDF").click(function(a) {
    var tarif = $("#filter-tarif").val();
    var dasar_hukum = $("#filter-dasar_hukum").val();
    var created_at = $("#filter-created_at").val();
    if (a.target.id == 'btnExcel'){
    window.open('/setting-tarif-bphtb/export_xls?tarif=' + tarif + '&dasar_hukum=' + dasar_hukum + '&created_at=' + created_at);
    } else{
    window.open('/setting-tarif-bphtb/export_pdf?tarif=' + tarif + '&dasar_hukum=' + dasar_hukum + '&created_at=' + created_at);
    }

    });
</script>
@endpush
