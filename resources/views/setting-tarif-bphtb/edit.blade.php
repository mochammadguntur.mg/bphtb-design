@extends('layouts.master')

@section('judulkiri')
PENGATURAN TARIF BPHTB [EDIT]
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary">
                    <form action="/setting-tarif-bphtb/{{ $tarif->s_idtarifbphtb }}/edit" method="POST">
                        <div class="card-body mt-4 mb-2">
                            @method('patch')
                            @csrf
                            @include('setting-tarif-bphtb.partials.form')
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection
