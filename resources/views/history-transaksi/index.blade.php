@extends('layouts.master')

@section('judulkiri')
    HISTORY TRANSAKSI
@endsection

@section('content')

<style>
    .detail {
        display : flex;
    }
    .label {

        display: inline-block;
        width: 200px;
    }

    .value {

        display: inline-block;
    }


    .input-section {

      display: flex;
    }
    #carihistorytransaksinib {
      float: right;
      margin-left: 95%;
    }
</style>

<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header"></div>
            <div class="card-body">
                <!-- /.card-header -->
                <form action="/history-transaksi/carihistorytransaksi" method="GET" class="form-inline d-flex " style="width:300px;">
                   {{--  <i class="fas fa-search" aria-hidden="true"></i> --}}
                   <div class="input-section">
                        <input name="carihistorytransaksinik" id="carihistorytransaksinik" class="form-control" type="text" placeholder="NIK"
                          aria-label="Search">
                        <button class="btn btn-primary" type="submit">
                          <i class="fa fa-search"></i>
                        </button>
                        <input name="carihistorytransaksinib" id="carihistorytransaksinib" class="form-control ml-3" type="text" placeholder="NIB"
                          aria-label="Search">
                        <button class="btn btn-primary" type="submit">
                          <i class="fa fa-search"></i>
                        </button>
                   </div>
                  </form>
                </br>
                  <div class="box box-primary" style="border: 1px solid #0073b7;">
                      <div class="box-header" style="background-color: #337ab7;">
                          <center><h3 class="box-title" style="color: white;">History Transaksi
                          </h3></center>
                      </div>
                      <table class="table table-bordered table-hover">
                          <caption>History Transaksi</caption>
                          <thead>
                          <tr>
                              <th scope="col">No</th>
                              <th scope="col">No SPT </th>
                              <th scope="col">Jumlah transaksi</th>
                              <th scope="col">Jenis Transaksi</th>
                              <th scope="col">Nama Notaris</th>
                              <th scope="col">NOP</th>
                          </tr>
                          </thead>
                          <tbody>
                            @if ($hasilcari !== NULL)
                            @foreach ($hasilcari as $data)
                            <tr>
                                <th scope="row">{{$loop->iteration}}</th>
                                <td style="text-align:center"><a href="#"  id="cekdetailhistory" onclick="cekdetailhistory(<?php echo $data->t_idspt; ?>); return false;">{{$data->t_kohirspt}}</a></td>
                                <td style="text-align:center">{{number_format($data->t_nilai_bphtb_fix, 0, ',', '.')}}</td>
                                <td style="text-align:center">{{$data->s_namajenistransaksi}}</td>
                                <td style="text-align:center">{{$data->s_namanotaris}}</td>
                                <td style="text-align:center">{{$data->t_nop_sppt}}</td>
                            </tr>
                            @endforeach
                            @endif
                          </tbody>
                      </table>
                  </div>

                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>




    <div class="modal fade" id="modal-detail">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" id="detail_history_title"></h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="table-responsive">

                        <div class="accordion" id="accordionExample">
                            <div class="card">
                              <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                  <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Pendaftaran
                                  </button>
                                </h5>
                              </div>

                              <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="detail">
                                        <div class="label">
                                            Nama Pembeli </br>
                                            NIK  </br>
                                            NIB  </br>
                                            NPWP Pembeli </br>
                                            Nama Kecamatan </br>
                                            Nama Kelurahan </br>
                                            Jalan </br>
                                            Nohp </br>
                                        </div>
                                        <div class="value" style="display: inline-block;">
                                            <span id="t_nama_pembeli"> </span>  </br>
                                            <span id="t_nik_pembeli"> </span>  </br>
                                            <span id="t_nib_pembeli"> </span>  </br>
                                            <span id="t_npwp_pembeli"> </span> </br>
                                            <span id="t_namakecamatan_pembeli"> </span>  </br>
                                            <span id="t_namakelurahan_pembeli"> </span> </br>
                                            <span id="t_jalan_pembeli"> </span> </br>
                                            <span id="t_nohp_pembeli"> </span> </br>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="card">
                              <div class="card-header" id="headingTwo">
                                <h5 class="mb-0">
                                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                    Validasi Berkas
                                  </button>
                                </h5>
                              </div>
                              <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="detail">
                                        <div class="label">
                                            Status Berkas  </br>
                                            Tgl Validasi </br>
                                            Keterangan Berkas </br>
                                        </div>
                                        <div class="value">
                                           <span id="s_nama_status_berkas"> </span> </br>
                                           <span id="t_tglvalidasi"> </span> </br>
                                           <span id="t_keterangan_berkas"> </span> </br>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="card">
                              <div class="card-header" id="headingThree">
                                <h5 class="mb-0">
                                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                    Validasi Kabid
                                  </button>
                                </h5>
                              </div>
                              <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="detail">
                                        <div class="label">
                                            Status Validasi Kabid  </br>
                                            {{-- Tgl Validasi Kabid </br> --}}
                                            Keterangan Kabid </br>
                                        </div>
                                        <div class="value">
                                           <span id="s_nama_status_kabid"> </span> </br>
                                           {{-- <span id="t_tglvalidasi"> </span> </br> --}}
                                           <span id="t_keterangan_kabid"> </span> </br>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                            <div class="card">
                              <div class="card-header" id="headingFour">
                                <h5 class="mb-0">
                                  <button class="btn btn-link collapsed" type="button" data-toggle="collapse" data-target="#collapseFour" aria-expanded="false" aria-controls="collapseThree">
                                    Pembayaran
                                  </button>
                                </h5>
                              </div>
                              <div id="collapseFour" class="collapse" aria-labelledby="headingFour" data-parent="#accordionExample">
                                <div class="card-body">
                                    <div class="detail">
                                        <div class="label">
                                            Status Bayar  </br>
                                            Tgl Pembayaran</br>
                                            Jumlah bayar </br>
                                        </div>
                                        <div class="value">
                                           <span id="s_nama_status_bayar"> </span> </br>
                                           <span id="t_tglpembayaran_pokok"> </span> </br>
                                           <span id="t_jmlhbayar_total"> </span> </br>
                                        </div>
                                    </div>
                                </div>
                              </div>
                            </div>
                          </div>

                    </div>
                    <div class="card-footer clearfix pagination-footer">
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-primary btn-sm float-right" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tutup</button>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">

$("#carihistorytransaksinik" ).focus(function() {
  $("#carihistorytransaksinib").val("");
});

$("#carihistorytransaksinib" ).focus(function() {
  $("#carihistorytransaksinik").val("");
});

function cekdetailhistory(t_idspt) {
        // alert('gol : ' + t_idspt);
        $('#modal-detail').modal('show');
        $.ajax({
            url: '/history-transaksi/detail/{' + t_idspt + '}',
            type: 'GET',
            data: {
                id: t_idspt
            }
        }).then(function(data) {

            $("#detail_history_title").html("Detail history " + data[0].t_nama_pembeli + " ");
            $("#t_nama_pembeli").html(data[0].t_nama_pembeli);
            $("#t_nik_pembeli").html(data[0].t_nik_pembeli);
            $("#t_nib_pembeli").html(data[0].t_nib_pembeli);
            $("#t_npwp_pembeli").html(data[0].t_npwp_pembeli);
            $("#t_namakecamatan_pembeli").html(data[0].t_namakecamatan_pembeli);
            $("#t_namakelurahan_pembeli").html(data[0].t_namakelurahan_pembeli);
            $("#t_jalan_pembeli").html(data[0].t_jalan_pembeli);
            $("#t_nohp_pembeli").html(data[0].t_nohp_pembeli);

            $("#s_nama_status_berkas").html((data[0].s_nama_status_berkas != null) ? data[0].s_nama_status_berkas : "Belum divalidasi");
            $("#t_tglvalidasi").html((data[0].t_tglvalidasi != null) ? data[0].t_tglvalidasi : "NULL");
            $("#t_keterangan_berkas").html((data[0].t_keterangan_berkas != null) ? data[0].t_keterangan_berkas : "Belum divalidasi");

            $("#s_nama_status_kabid").html((data[0].s_nama_status_kabid != null) ? data[0].s_nama_status_kabid : "Belum divalidasi Kabid");
            $("#t_keterangan_kabid").html((data[0].t_keterangan_kabid != null) ? data[0].t_keterangan_kabid : "Belum divalidasi Kabid");

            $("#s_nama_status_bayar").html((data[0].s_nama_status_bayar != null) ? data[0].s_nama_status_bayar : "Belum di bayar");
            $("#t_tglpembayaran_pokok").html((data[0].t_tglpembayaran_pokok != null) ? data[0].t_tglpembayaran_pokok : "NULL");
            $("#t_jmlhbayar_total").html((data[0].t_jmlhbayar_total != null) ? data[0].t_jmlhbayar_total : "Belum di bayar");

        });
        //$("#modal-delete").modal('show');
    }

</script>
@endpush

