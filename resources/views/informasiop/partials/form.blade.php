{{-- <div class="form-group row">
    <label class="col-md-6">Pilih Metode Pencarian Informasi Objek Pajak :</label>
    <select class="select2 form-control col-sm-11" id="select-metode">
        <option selected value="1">NOP / Tahun</option>
        <option value="2">NPWPD</option>
        <option value="3">NIK</option>
    </select>
</div> --}}

{{-- <div class="form-group row" id="nik">
    <label class="col-sm-2">NIK</label>
    <div class="col-sm-9">
        <input type="text" class="form-control masked-nik" id="t_nik" name="t_nik">
    </div>
</div>
<div class="form-group row" id="npwpd">
    <label class="col-sm-2">NPWPD</label>
    <div class="col-sm-9">
        <input type="text" class="form-control masked-npwpd" id="t_npwpd" name="t_npwpd" value="P">
    </div>
</div> --}}
<div class="form-group row" id="nop">
    <label class="col-sm-2">NOP-TAHUN</label>
    <div class="col-sm-9">
        <input type="text" class="form-control masked-noptahun" id="t_nop" name="t_nop" placeholder="XX.XX.XXX.XXX.XXX.XXXX.X-XXXX">
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-4">
        <span class="input-group-append">
            <button type="button" id="btnCari" class="btn btn-info btn-flat"><i class="fas fa-search"></i> CARI</button>
        </span>
    </div>
</div>
