@extends('layouts.master')

@section('judulkiri')
INFORMASI OP
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-body form-horizontal">
                    @csrf
                    @include('informasiop.partials.form')
            </div>
        </div>
    </div>
</div>

<div class="row" id="detailPbb">
    <div class="col-md-12">
        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-calculator"></i> DETAIL TUNGGAKAN SPPT PBB-P2
                <a href="#" id="btnPrintPbb" class="btn btn-warning btn-sm"><i class="fas fa-print"></i> CETAK</a>
                </h3>
                <div class="card-tools">&nbsp;
                <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip">
                    <i class="fas fa-minus"></i>
                </button>
                </div>
            </div>
            <div class="card-body form-horizontal">
                <div class="row" id="bodyDetailPbb"></div>
            </div>
        </div>
    </div>
</div>
{{-- <div class="row" id="detailBphtb">
    <div class="col-md-12">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title"><i class="fas fa-calculator"></i> DETAIL TUNGGAKAN BPHTB
            <a href="#" id="btnPrintBphtb" class="btn btn-warning btn-sm"><i class="fas fa-print"></i> CETAK</a>
        </h3>
          <div class="card-tools">&nbsp;
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body form-horizontal">
            <div class="row" id="bodyDetailBphtb"></div>
        </div>
      </div>
    </div>
</div> --}}
{{-- <div class="row" id="detailPdl">
    <div class="col-md-12">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title"><i class="fas fa-calculator"></i> DETAIL TUNGGAKAN PAJAK LAINNYA
            <a href="#" id="btnPrintPdl" class="btn btn-warning btn-sm"><i class="fas fa-print"></i> CETAK</a>
        </h3>
          <div class="card-tools">&nbsp;
            <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body form-horizontal">
            <div class="row" id="bodyDetailPdl"></div>
        </div>
      </div>
    </div>
</div> --}}

<div class="modal fade bd-example-modal-lg" id="modal-overlay" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="col-12">
                <span class="spinner-grow text-muted"></span>
                <span class="spinner-grow text-primary"></span>
                <span class="spinner-grow text-success"></span>
                <span class="spinner-grow text-info"></span>
                <span class="spinner-grow text-warning"></span>
                <span class="spinner-grow text-danger"></span>
                <span class="spinner-grow text-secondary"></span>
                <span class="spinner-grow text-dark"></span>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script type="text/javascript">
    $(function () {
        $('#btnPrintPbb').hide();
        $('#btnPrintBphtb').hide();
        $('#btnPrintPdl').hide();
        $('#detailPbb').hide();
        $('#detailBphtb').hide();
        $('#detailPdl').hide();
        $('#nik').hide();
        $('#npwpd').hide();
    });

    $("#btnCari").click(function() {
        // var data = $("#select-metode").val();
        // if (data == 1) {
        //     if ($('#t_nop').val() == '') {
        //         toastr.info('Silahkan isi Nop yang kosong!');
        //     } else {
        //         showInfo('','',$('#t_nop').val());
        //     }
        // }else if (data == 2) {
        //     if ($('#t_npwpd').val() == '') {
        //         toastr.info('Silahkan isi NPWPD yang kosong!');
        //     } else {
        //         showInfo('',$('#t_npwpd').val(),'');
        //     }
        // }else if (data == 3) {
        //     if ($('#t_nik').val() == '') {
        //         toastr.info('Silahkan isi NIK yang kosong!');
        //     } else {
        //         showInfo($('#t_nik').val(),'','');
        //     }
        // }
        if($('#t_nik').val() == '' && $('#t_npwpd').val() == '' && $('#t_npwpd').val() == 'P' && $('#t_nop').val() == ''){
            toastr.info('Silahkan isi salah satu field yang kosong!');
        }else{
            showInfo($('#t_nik').val(),$('#t_npwpd').val(),$('#t_nop').val());
        }
    });

    $("#select-metode").change(function(){
        var data = $("#select-metode").val();
        if (data == 1) {
            $('#nik').hide();
            $('#npwpd').hide();
            $('#nop').show();
        } else if(data == 2){
            $('#nik').hide();
            $('#nop').hide();
            $('#npwpd').show();
        } else {
            $('#nik').show();
            $('#nop').hide();
            $('#npwpd').hide();
        }
    })

    function showInfo(nik,npwpd,nop) {
        $('#modal-overlay').modal('show');
        $.ajax({
            url: 'informasiop/caridata/{' + nik + '}/{' + npwpd + '}/{' + nop + '}',
            type: 'GET',
            data: {
                nik: nik,
                npwpd: npwpd,
                nop: nop
            }
        }).then(function(data) {
            $('#modal-overlay').modal('hide');
            $('#detailPbb').show();
            $('#detailBphtb').show();
            $('#detailPdl').show();
            $('#bodyDetailPbb').html(data.data.pbb);
            $('#bodyDetailBphtb').html(data.data.bphtb);
            $('#bodyDetailPdl').html(data.data.pdl);
            $('#btnPrintPbb').show();
            $('#btnPrintBphtb').show();
            $('#btnPrintPdl').show();
        });
    }

    $("#btnPrintPbb, #btnPrintBphtb, #btnPrintPdl").click(function(a) {
        var t_nop = $("#t_nop").val();
        var t_nik = $("#t_nik").val();
        var t_npwpd = $("#t_npwpd").val();
        if(a.target.id == 'btnPrintPbb'){
            window.open('/informasiop/export_pdf?t_nop=' + t_nop
            + '&t_nik=' + t_nik
            + '&t_npwpd=' + t_npwpd
            + '&cetak=PBB');
        }else if(a.target.id == 'btnPrintBphtb'){
            window.open('/informasiop/export_pdf?t_nop=' + t_nop
            + '&t_nik=' + t_nik
            + '&t_npwpd=' + t_npwpd
            + '&cetak=BPHTB');
        }else{
            window.open('/informasiop/export_pdf?t_nop=' + t_nop
            + '&t_nik=' + t_nik
            + '&t_npwpd=' + t_npwpd
            + '&cetak=PDL');
        }
    });
</script>
@endpush
