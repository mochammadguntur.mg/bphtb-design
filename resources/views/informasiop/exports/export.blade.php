@php error_reporting(0) @endphp
{{-- <title>Informasi OP</title> --}}

@if ($request->query('cetak') == 'PBB')
{{-- <style>
    @page{margin: 40px;font-family: sans-serif;font-size: 10pt;size: legal portrait;}
</style> --}}
<h3>INFORMASI TUNGGAKAN SPPT PBB-P2</h3>
===========================================================
<table>
    <tr>
        <td style="font-size: 12px;">Nama WP SPPT</td>
        <td style="font-size: 12px;">:</td>
        <td style="font-size: 12px;">{{ $query[0]->nm_wp_sppt }}</td>
    </tr>
    <tr>
        <td style="font-size: 12px;">Alamat WP</td>
        <td style="font-size: 12px;">:</td>
        <td style="font-size: 12px;">{{ $query[0]->jln_wp_sppt .' RT/RW ' . $query[0]->rt_wp_sppt . '/' . $query[0]->rw_wp_sppt .' Kel/Desa '. $query[0]->nm_kelurahan .' Kec. '.$query[0]->nm_kecamatan }}</td>
    </tr>
    <tr>
        <td style="font-size: 12px;">NOP</td>
        <td style="font-size: 12px;">:</td>
        <td style="font-size: 12px;">{{ substr($NOP,0,24) }}</td>
    </tr>
    <tr>
        <td style="font-size: 12px;">Letak Objek</td>
        <td style="font-size: 12px;">:</td>
        <td style="font-size: 12px;">{{ $query[0]->jln_wp_sppt }}</td>
    </tr>
</table>
===========================================================
<table width="100%">
    <tr style="background: #ededeb;">
        <th colspan="5" style="font-size:12pt;">TUNGGAKAN SPPT-PBB</th>
    </tr>
    <tr style="background: #ededeb;">
        <th>NO.</th>
        <th>TAHUN</th>
        <th>JMLH TUNGGAKAN (Rp)</th>
        <th>TGL JATUH TEMPO</th>
        <th>JMLH DENDA (Rp)</th>
    </tr>
    <tr>
        <td colspan="5">==============================================================================================</td>
    </tr>
    @php
    $counter = 1;
    $jmlh_tunggakan = 0;
    $jmlh_denda = 0;
    @endphp

    @foreach($sql_tunggakan as $key => $v)
    @php

        $tgljatuhtempo = date('Y-m-d', strtotime($v['tgl_jatuh_tempo_sppt']));
        $tglsekarang = date('Y-m-d');
        $ts1 = strtotime($tgljatuhtempo);
        $ts2 = strtotime($tglsekarang);

        $year1 = date('Y', $ts1);
        $year2 = date('Y', $ts2);

        $month1 = date('m', $ts1);
        $month2 = date('m', $ts2);

        $day1 = date('d', $ts1);
        $day2 = date('d', $ts2);
        if ($day1 < $day2) {
            $tambahanbulan = 1;
        } else {
            $tambahanbulan = 0;
        }

        $jmlbulan = (($year2 - $year1) * 12) + ($month2 - $month1 + $tambahanbulan);
        if ($jmlbulan > 24) {
            $jmlbulan = 24;
            $jmldenda = $jmlbulan * 2 / 100 * $v['pbb_yg_harus_dibayar_sppt'];
        } elseif ($jmlbulan <= 0) {
            $jmlbulan = 0;
            $jmldenda = $jmlbulan * 2 / 100 * $v['pbb_yg_harus_dibayar_sppt'];
        } else {
            $jmldenda = $jmlbulan * 2 / 100 * $v['pbb_yg_harus_dibayar_sppt'];
        }
    @endphp
    <tr>
        <td style="border-right: 1px solid black; border-left: 1px solid black; text-align:center;">{{ $counter++ }}</td>
        <td style="text-align: center; border-right: 1px solid black;">{{ $v['thn_pajak_sppt'] }}</td>
        <td style="text-align: right; border-right: 1px solid black; padding-right: 20px;">{{ number_format($v['pbb_yg_harus_dibayar_sppt'], 0, ',', '.') }}</td>
        <td style="text-align: center; border-right: 1px solid black;">{{ date('d-m-Y', strtotime($v['tgl_jatuh_tempo_sppt'])) }}</td>
        <td style="text-align: right; border-right: 1px solid black; padding-right: 20px;">{{ number_format($jmldenda, 0, ',', '.') }}</td>
    </tr>
    @php

    $jmlh_tunggakan += $v['pbb_yg_harus_dibayar_sppt'];
    $jmlh_denda += $jmldenda;
    @endphp
    @endforeach
    <tr>
        <td colspan="5">==============================================================================================</td>
    </tr>
    <tr>
        <th colspan="2">JUMLAH TUNGGAKAN (Rp)</th>
        <th style="text-align: right; padding-right: 20px;">{{ number_format($jmlh_tunggakan, 0, ',', '.') }}</th>
        <th>JUMLAH DENDA (Rp)</th>
        <th style="text-align: right; padding-right: 20px;">{{ number_format($jmlh_denda, 0, ',', '.') }}</th>
    </tr>
    <tr>
        <td colspan="5">==============================================================================================</td>
    </tr>
    <tr class="bg-danger">
        <th colspan="3">TOTAL TUNGGAKAN (Rp)</th>
        <th colspan="2" style="font-size:14pt;">{{ number_format($jmlh_tunggakan + $jmlh_denda, 0, ',', '.') }}</th>
    </tr>
    <tr>
        <td colspan="5">==============================================================================================</td>
    </tr>
</table>

@elseif($request->query('cetak') == 'BPHTB')
<style>
    @page{margin: 40px;font-family: sans-serif;font-size: 10pt;size: legal landscape;}
</style>
<h2>INFORMASI TUNGGAKAN BPHTB</h2>
<table width="100%" style="border-collapse: collapse;" cellspacing="2" cellpadding="2" border="1">
    <thead>
        <tr>
            <th>NO.</th>
            <th>TGL DAFTAR</th>
            <th>NO KOHIR</th>
            <th>NIK</th>
            <th>NPWPD</th>
            <th>NAMA WP</th>
            <th>NOP</th>
            <th>JMLH PAJAK (Rp)</th>
            <th>JENIS TRANSAKSI</th>
        </tr>
    </thead>
    <tbody>
    {{ $counter = 1 }}
    @foreach($queryBphtb as $v)
        <tr>
            <td>{{ $counter++ }}</td>
            <td style="text-align:center;">{{ date('d-m-Y', strtotime($v['t_tgldaftar_spt'])) }}</td>
            <td style="text-align:center">{{ $v['t_kohirspt'] }}</td>
            <td style="text-align:center">{{ $v['t_p_nik_pembeli'] }}</td>
            <td style="text-align:center">{{ $v['t_npwpd'] }}</td>
            <td class="">{{ $v['t_p_nama_pembeli'] }}</td>
            <td style="text-align:center">{{ $v['t_nop_sppt'] }}</td>
            <td style="text-align:right">{{ number_format($v['t_nilai_bphtb_fix'], 0, ',', '.') }}</td>
            <td class="">{{ $v['s_namajenistransaksi'] }}</td>
        </tr>
    {{ $jmlh_tunggakan += $v['t_nilai_bphtb_fix'] }}
    @endforeach
    </tbody>
    <tfoot>
        <tr class="bg-danger">
            <th colspan="7">JUMLAH TUNGGAKAN (Rp)</th>
            <th style="text-align:right;font-size:14pt;">{{ number_format($jmlh_tunggakan, 0, ',', '.') }}</th>
            <th style="text-align:center"></th>
        </tr>
    </tfoot>
</table>

@elseif($request->query('cetak') == 'PDL')
<h2>INFORMASI TUNGGAKAN PDL (PAJAK LAINNYA)</h2>
<style>
    @page{margin: 40px;font-family: sans-serif;font-size: 10pt;size: legal landscape;}
</style>

@endif
