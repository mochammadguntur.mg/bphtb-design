<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">STATUS</label>
    <div class="col-md-4">
        <select class="form-control @error('s_id_statustgkpbb') is-invalid @enderror" id="s_id_statustgkpbb" name="s_id_statustgkpbb" >
            
            <!-- <option style="display:none" value="">Silahkan Pilih</option> -->
            
            <option value="1" @if($menutunggakanpbb->s_id_statustgkpbb == 1) selected @endif > AKTIF </option>
            <option value="2" @if($menutunggakanpbb->s_id_statustgkpbb == 2) selected @endif> TIDAK AKTIF </option>    
        </select>
    </div>
    
</div>


<a href="/setting-menutunggakanpbb" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>

@push('scripts')
<script type="text/javascript">
    

    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert").slideUp(500);
    });

</script>
@endpush
