@extends('layouts.master')

@section('judulkiri')
    PENGATURAN MENU TUNGGAKAN PBB [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="{{ url('setting-menutunggakanpbb/' . $menutunggakanpbb->s_id_setmenutgkpbb . '/edit') }}" method="POST" enctype="multipart/form-data">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-menutunggakanpbb.partials.form')
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
