@extends('layouts.master')

@section('judulkiri')
PENGATURAN KECAMATAN
@endsection

@section('content')
<style type="text/css">
    .pdfobject-container { height: 30rem; border: 1rem solid rgba(0,0,0,.1); }
</style>
<div class="row">
    <div class="col-md-12">
        @if (session('success'))
        <div class="alert alert-success alert-dismissible callout callout-success" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
            {{ session('success') }}
        </div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger alert-dismissible callout callout-danger" id="alert">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h5><i class="icon fas fa-ban"></i> Gagal!</h5>
            {{ session('error') }}
        </div>
        @endif
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="setting-kecamatan/create" class="btn btn-primary" id="createBtn">
                        <i class="fas fa-plus"></i> Tambah
                    </a>
                </h3>
                <h3 class="card-title">
                    <a href="setting-kecamatan/sync" class="btn btn-danger">
                        <i class="fas fa-plus"></i> Sinkronisasi
                    </a>
                </h3>
                <div class="card-tools">
                    <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                        <i class="fas fa-file-excel"></i> Excel
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger"  id="btnPDF">
                        <i class="fas fa-file-pdf"></i> PDF
                    </a>
                </div>
            </div>
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Kode Propinsi</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Kode Dati 2</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Kode Kecamatan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Nama Kecamatan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Latitude</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Longitude</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Tgl Pembaharuan</th>
                                <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                            </tr>
                            <tr>
                                <th scope="filter"></th>
                                <th scope="filter">
                                    <input type="text" class="form-control form-control-sm" id="filter-kd_propinsi">
                                </th>
                                <th scope="filter">
                                    <input type="text" class="form-control form-control-sm" id="filter-kd_dati2">
                                </th>
                                <th scope="filter">
                                    <input type="text" class="form-control form-control-sm" id="filter-kd_kecamatan">
                                </th>
                                <th scope="filter">
                                    <input type="text" class="form-control form-control-sm" id="filter-namakecamatan">
                                </th>
                                <th scope="filter">
                                    <input type="text" class="form-control form-control-sm" id="filter-latitude">
                                </th>
                                <th scope="filter">
                                    <input type="text" class="form-control form-control-sm" id="filter-longitude">
                                </th>
                                <th scope="filter">
                                    <input type="text" class="form-control form-control-sm" id="filter-updated_at" readonly>
                                </th>
                                <th scope="filter"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="9"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Kode Kecamatan</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="kodekecamatanHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Nama Kecamatan</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namakecamatanHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="formModalViewBerkas" tabindex="-1" role="dialog" aria-labelledby="formModalViewBerkas" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-light"><i class="far fa-file-pdf text-light"></i> Preview Dokumen</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <form class="form" name="formViewBerkas" id="formViewBerkas">
                <div class="modal-body" id="body-preview"></div>
            </form>
        </div>
    </div>
</div>


@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('.media').media({
            width: "100%",
            height: 600
        });

        $('#filter-updated_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-updated_at').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-updated_at').on('cancel.daterangepicker', function (ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
        url: 'setting-kecamatan/datagrid',
        table: "#datagrid-table",
        columns: [
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "s_kd_propinsi"
            },
            {
                sortable: true,
                name: "s_kd_dati2"
            },
            {
                sortable: true,
                name: "s_kd_kecamatan"
            },
            {
                sortable: true,
                name: "s_namakecamatan"
            },
            {
                sortable: true,
                name: "s_latitude"
            },
            {
                sortable: true,
                name: "s_longitude"
            },
            {
                sortable: true,
                name: "updated_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]
    });

    $("#filter-kd_propinsi, #filter-kd_dati2, #filter-kd_kecamatan, #filter-namakecamatan, #filter-latitude, #filter-longitude").keyup(function () {
        search();
    });

    function search() {
        datatables.setFilters({
            kd_propinsi: $("#filter-kd_propinsi").val(),
            kd_dati2: $("#filter-kd_dati2").val(),
            kd_kecamatan: $("#filter-kd_kecamatan").val(),
            namakecamatan: $("#filter-namakecamatan").val(),
            latitude: $("#filter-latitude").val(),
            longitude: $("#filter-longitude").val(),
            updated_at: $("#filter-updated_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'setting-kecamatan/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function (data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].s_idkecamatan);
            $("#kodekecamatanHapus").val(data[0].s_kd_kecamatan);
            $("#namakecamatanHapus").val(data[0].s_namakecamatan);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function () {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'setting-kecamatan/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function (data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Berhasil dihapus!')
            datatables.reload();
        });
    });

    $("#btnExcel, #btnPDF").click(function (a) {
        var kodekecamatan = $("#filter-kodekecamatan").val();
        var namakecamatan = $("#filter-namakecamatan").val();
        var updated_at = $("#filter-updated_at").val();
        if (a.target.id == 'btnExcel') {
            window.open('setting-kecamatan/export_xls?kodekecamatan=' + kodekecamatan + '&namakecamatan=' + namakecamatan + '&updated_at=' + updated_at);
        } else {
            window.open('setting-kecamatan/export_pdf?kodekecamatan=' + kodekecamatan + '&namakecamatan=' + namakecamatan + '&updated_at=' + updated_at);
//            $.ajax({
//                url: 'setting-kecamatan/export_pdf/',
//                method: "GET",
//                dataType: "html",
//                data: {
//                    kodekecamatan: kodekecamatan,
//                    namakecamatan: namakecamatan,
//                    updated_at: updated_at
//                }
//            }).then(function (data) {
////                console.log(data);
//                $("#body-preview").html(data);
//                $('#formModalViewBerkas').modal('show');
//            });
        }

    });

    $("#alert").fadeTo(2000, 500).slideUp(500, function () {
        $("#alert").slideUp(500);
    });

</script>
@endpush
