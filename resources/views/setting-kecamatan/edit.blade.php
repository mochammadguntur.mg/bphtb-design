@extends('layouts.master')

@section('judulkiri')
PENGATURAN KECAMATAN [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="{{ url('setting-kecamatan/'.  $kecamatan->s_idkecamatan .'/edit') }}" method="post">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-kecamatan.partials.form')
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
