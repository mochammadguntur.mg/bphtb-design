<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Propinsi</label>
    <div class="col-md-4">
        <input onkeypress="return numberOnly(event)" type="text" name="s_kd_propinsi" class="form-control @error('s_kd_propinsi') is-invalid @enderror" id="s_kd_propinsi" placeholder="Kode Propinsi" value="{{ old('s_kd_propinsi') ?? $kecamatan->s_kd_propinsi }}" {{ $sumbit ?? 'readonly' }} maxlength="2">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kd_propinsi'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_kd_propinsi') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Dati 2</label>
    <div class="col-md-4">
        <input onkeypress="return numberOnly(event)" type="text" name="s_kd_dati2" class="form-control @error('s_kd_dati2') is-invalid @enderror" id="s_kd_dati2" placeholder="Kode Dati 2" value="{{ old('s_kd_dati2') ?? $kecamatan->s_kd_dati2 }}" {{ $sumbit ?? 'readonly' }} maxlength="2">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kd_dati2'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_kd_dati2') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Kecamatan</label>
    <div class="col-md-4">
        <input onkeypress="return numberOnly(event)" type="text" name="s_kd_kecamatan" class="form-control @error('s_kd_kecamatan') is-invalid @enderror" id="s_kd_kecamatan" placeholder="Kode Kecamatan" value="{{ old('s_kd_kecamatan') ?? $kecamatan->s_kd_kecamatan }}" {{ $sumbit ?? 'readonly' }} maxlength="3">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kd_kecamatan'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_kd_kecamatan') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Nama Kecamatan</label>
    <div class="col-md-4">
        <input type="text" name="s_namakecamatan" class="form-control @error('s_namakecamatan') is-invalid @enderror" id="s_namakecamatan" placeholder="Nama Kecamatan" value="{{ old('s_namakecamatan') ?? $kecamatan->s_namakecamatan }}" {{ $sumbit ?? 'readonly' }}>
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_namakecamatan'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_namakecamatan') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Latitude</label>
    <div class="col-md-4">
        <input type="text" name="s_latitude" class="form-control @error('s_latitude') is-invalid @enderror" id="s_latitude" placeholder="Latitude" value="{{ old('s_latitude') ?? $kecamatan->s_latitude }}">
        @if ($errors->has('s_latitude'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_latitude') }}
        </div>
        @endif
    </div>
    <label class="col-sm-2">Longitude</label>
    <div class="col-md-4">
        <input type="text" name="s_longitude" class="form-control @error('s_longitude') is-invalid @enderror" id="s_longitude" placeholder="Longitude" value="{{ old('s_longitude') ?? $kecamatan->s_longitude }}">
        @if ($errors->has('s_longitude'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_longitude') }}
        </div>
        @endif
    </div>
</div>
<div class="form-group row mb-2" id="googleMap">
    <span id="resMaps"></span>
</div>

<a href="/setting-kecamatan" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>

<style>
    #googleMap {
        height: 400px;
        width: 100%;
        border: 1px solid #ccc;
    }
</style>
@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIcfDt4yUMeAiGKDTNyIufrBMuif-efms&callback"></script>
<script type="text/javascript">
            function numberOnly(e) {
                var input = (e.which) ? e.which : e.keyCode

                if (input > 31 && (input < 48 || input > 57))
                    return false;
                return true;
            }

            // variabel global marker
            var marker;

            function taruhMarker(peta, posisiTitik) {

                if (marker) {
                    // pindahkan marker
                    marker.setPosition(posisiTitik);
                } else {
                    // buat marker baru
                    marker = new google.maps.Marker({
                        position: posisiTitik,
                        map: peta,
                        // draggable:true,
                    });
                }
                // isi nilai koordinat ke form
                document.getElementById("s_latitude").value = posisiTitik.lat();
                document.getElementById("s_longitude").value = posisiTitik.lng();

            }

            function initialize() {
                var lat = document.getElementById('s_latitude').value;
                var lng = document.getElementById('s_longitude').value;
                var latlng;

                if (lat == '' && lng == '') {
                    latlng = new google.maps.LatLng(-3.4512244, 115.5681084);
                } else {
                    latlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                }

                var propertiPeta = {
                    center: latlng,
                    zoom: 10,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                };

                console.log(propertiPeta);

                var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);

                if (lat != '' && lng != '') {
                    taruhMarker(peta, latlng);
                }

                // even listner ketika peta diklik
                google.maps.event.addListener(peta, 'click', function (event) {
                    taruhMarker(this, event.latLng);
                });

            }

            // event jendela di-load
            google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endpush