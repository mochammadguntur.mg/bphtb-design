

@extends('layouts.master')
{{-- <meta content="Divi v.4.7.0" name="generator"/> --}}

@section('judulkiri')
    PENGATURAN UBAH PASSWORD
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            @if (session('success'))
                <div class="alert alert-success alert-dismissible callout callout-success" id="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
                {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger alert-dismissible callout callout-danger" id="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-ban"></i> Gagal!</h5>
                {{ session('error') }}
                </div>
            @endif
            <div class="card mb-4">
                <div class="card-header">Ubah Password</div>
                <div class="card-body">
                    <form action="{{ route('ubah-pass.updatepass') }}" id="change_password_form" method="post">
                        @csrf
                        <div class="form-group">
                            <label for="old_password">Password Lama</label>
                            <input id="old_password" type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password"  autocomplete="old_password" >
                            {{-- <div class="group">
                                <i id="icon" class="fa fa-eye-slash" style="position: absolute; bottom: 12px; float: right; left: 324px; cursor: pointer;"></i>
                            </div> --}}
                            @error('old_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="password">Password Baru</label>
                            <input id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password"  autocomplete="new_password">
                            @error('new_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group">
                            <label for="confirm_password">Ulangi Password Baru</label>
                            <input id="confirm_password" type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="confirm_password"  autocomplete="confirm_password">
                            @error('confirm_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <button type="submit" class="btn btn-md btn-warning btn-block"><i class="fa fa-key"></i> Ubah Password</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection


@push('script')
<script type="text/javascript">
    @if (session('success'))
        toastr.success('{{session('success')}}')
    @endif

    @if (session('error'))
        toastr.error('{{session('error')}}')
    @endif
    var masukanpass = document.getElementById('old_password');
    var input = document.getElementById('old_password');
    icon = document.getElementById('icon');

    icon.onclick = function () {
        if(input.className == 'active') {
            input.setAttribute('type', 'text');
            icon.className = 'fa fa-eye';
            input.className = '';

        } else {
            input.setAttribute('type', 'password');
            icon.className = 'fa fa-eye-slash';
            input.className = 'active';
        }
    }

    $('#change_password_form').validate({
        ignore:'.ignore',
        errorClass:'invalid',
        validClass:'success',
        rules:{
            old_password:{
                required:true,
                minlength:8,
                maxlength:100
            },
            new_password:{
                required:true,
                minlength:8,
                maxlength:100
            },
            confirm_password:{
                required:true,
                equalTo:'#new_password'
            },
        },
        message: {
        
            old_password:{
                required:"Masukkan Password Lama"
            },
            new_password:{
                required:"Masukkan Password Baru"
            },
            confirm_password:{
                required:"ulangi Password Baru"
            },

        },
        submitHandler:function(form){
            $.LoadingOverlay("show");
            form.submit();
        }

    });

</script>
    
@endpush
