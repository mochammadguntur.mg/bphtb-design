

@extends('layouts.master')
{{-- <meta content="Divi v.4.7.0" name="generator"/> --}}

@section('judulkiri')
    PENGATURAN UBAH PASSWORD DAN FOTO PROFIL
@endsection

@section('content')
    <section class="content">
        <div class="container-fluid">
            @if (session('success'))
                <div class="alert alert-success alert-dismissible callout callout-success" id="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
                {{ session('success') }}
                </div>
            @endif
            @if (session('error'))
                <div class="alert alert-danger alert-dismissible callout callout-danger" id="alert">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h5><i class="icon fas fa-ban"></i> Gagal!</h5>
                {{ session('error') }}
                </div>
            @endif
            <div class="card mb-4">
                <div class="card-header">Ubah Password</div>
                <div class="card-body">
                    <form action="{{ route('ubah-pass.updatepass') }}" id="change_password_form" method="POST" enctype="multipart/form-data">
                        @csrf
                        <div class="form-group">
                            <label for="old_password">Password Lama</label>
                            <input id="old_password" type="password" class="form-control @error('old_password') is-invalid @enderror" name="old_password"  autocomplete="old_password" >
                            {{-- <div class="group">
                                <i id="icon" class="fa fa-eye-slash" style="position: absolute; bottom: 12px; float: right; left: 324px; cursor: pointer;"></i>
                            </div> --}}
                            @error('old_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group" id="pass1">
                            <label for="password">Password Baru</label>
                            <input id="new_password" type="password" class="form-control @error('new_password') is-invalid @enderror" name="new_password"  autocomplete="new_password">
                            @error('new_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group" id="pass2">
                            <label for="confirm_password">Ulangi Password Baru</label>
                            <input id="confirm_password" type="password" class="form-control @error('confirm_password') is-invalid @enderror" name="confirm_password"  autocomplete="confirm_password">
                            @error('confirm_password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="form-group" id="gambar1">
                            <label class="col-sm-12" for="exampleInputFile">File Gambar</label>
                            <div class="col-md-12">
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="s_gambar" id="s_gambar" value="{{ old('s_gambar') }}" class="custom-file-input form-control @error('s_gambar') is-invalid @enderror">
                                        <label class="custom-file-label" for="s_gambar22">Choose file</label>
                                    </div>
                                    <div class="input-group-append">
                                        <span class="input-group-text" id="">Upload</span>
                                    </div>
                                </div>
                                @if ($errors->has('s_gambar'))
                                <div class="text-danger mb-2">
                                    {{ $errors->first('s_gambar') }}
                                </div>
                                @endif
                            </div>
                            <div class="col-md-4">
                                <span class="text-muted">Hanya file <b style="color:red"> gambar</b> yg di perbolehkan</span>
                            </div>
                        </div>

                        <div class="d-flex justify-content-center">
                            <img src="{{ asset(optional($datauser)->s_gambar) }}" id="preview" class="img-fluid" alt="logo-s_gambar" style="width: 300px;">
                        </div>

                        <div class="row justify-content-center mt-5">
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block bg-gradient-warning btn-lg" onclick="hideGambarShowPass();">UBAH PASSWORD</button>
                            </div>
                            <div class="col-md-3">
                                <button type="button" class="btn btn-block bg-gradient-success btn-lg" onclick="hidePassShowGambar();">UBAH PROFIL</button>
                            </div>
                        </div>
                        <button type="submit" class="btn btn-primary" id="simpan">Update</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
@endsection


@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $('#pass1').hide();
        $('#pass2').hide();
        $('#gambar1').hide();
        $('#preview').hide();
        $('#simpan').hide();
        $('#simpan').hide();
    });

    function hidePassShowGambar(){
        $('#pass1').hide();
        $('#pass2').hide();
        $('#gambar1').show();
        $('#preview').show();
        $('#simpan').show();
    }

    function hideGambarShowPass(){
        $('#gambar1').hide();
        $('#preview').hide();
        $('#pass1').show();
        $('#pass2').show();
        $('#simpan').show();
    }

    $(document).on("click", "#s_gambar", () => {
        var file = $(this).parents().find("#file");
        file.trigger("click");
    });

    $('input[type="file"]').change((e) => {
        var input = document.querySelector('input[type=file]');
        var file = input.files[0];

        var reader = new FileReader();
        reader.onload = (e) => {
            document.getElementById("preview").src = e.target.result;
        };

        reader.readAsDataURL(file);
    });

    $(".alert").fadeTo(2000, 500).slideUp(500, function () {
        $(".alert").slideUp(500);
    });

    // @if (session('success'))
    //     toastr.success('{{session('success')}}')
    // @endif

    // @if (session('error'))
    //     toastr.error('{{session('error')}}')
    // @endif
    // var masukanpass = document.getElementById('old_password');
    // var input = document.getElementById('old_password');
    // icon = document.getElementById('icon');

    // icon.onclick = function () {
    //     if(input.className == 'active') {
    //         input.setAttribute('type', 'text');
    //         icon.className = 'fa fa-eye';
    //         input.className = '';

    //     } else {
    //         input.setAttribute('type', 'password');
    //         icon.className = 'fa fa-eye-slash';
    //         input.className = 'active';
    //     }
    // }

    // $('#change_password_form').validate({
    //     ignore:'.ignore',
    //     errorClass:'invalid',
    //     validClass:'success',
    //     rules:{
    //         old_password:{
    //             required:true,
    //             minlength:8,
    //             maxlength:100
    //         },
    //         new_password:{
    //             required:true,
    //             minlength:8,
    //             maxlength:100
    //         },
    //         confirm_password:{
    //             required:true,
    //             equalTo:'#new_password'
    //         },
    //     },
    //     message: {
        
    //         old_password:{
    //             required:"Masukkan Password Lama"
    //         },
    //         new_password:{
    //             required:"Masukkan Password Baru"
    //         },
    //         confirm_password:{
    //             required:"ulangi Password Baru"
    //         },

    //     },
    //     submitHandler:function(form){
    //         $.LoadingOverlay("show");
    //         form.submit();
    //     }

    // });

</script>
@endpush
