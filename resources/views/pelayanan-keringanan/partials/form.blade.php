<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Tgl Permohonan <span style="color:red;">*</span></label>
    <div class="col-sm-2">
        <input type="text" class="form-control" id="t_tglpengajuan" name="t_tglpengajuan"
            value="{{ old('t_tglpengajuan') ? $datakeringanan->t_tglpengajuan : date('d-m-Y') }}" readonly="true">
        @if ($errors->has('t_tglpengajuan'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_tglpengajuan') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">No Kohir <span style="color:red;">*</span></label>
    <div class="col-sm-2">
        <input type="hidden" id="t_idspt" name="t_idspt" value="{{ old('t_idspt') ?? $datakeringanan->t_idspt }}">
        <input type="text" class="form-control" id="t_kohirspt" name="t_kohirspt"
            value="{{ old('t_kohirspt') ?? $datakeringanan->t_kohirspt }}" readonly>
        @if ($errors->has('t_kohirspt'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_kohirspt') }}
            </span>
        @endif
    </div>
    <div class="col-sm-2">
        <span class="btn btn-md btn-warning" id="btnCariSpt"><i class="fas fa-search"></i> CARI</span>
    </div>
</div>
<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Wajib Pajak <span style="color:red;">*</span></label>
    <div class="col-sm-6">
        <input type="text" class="form-control" id="t_nama_pembeli" name="t_nama_pembeli"
            value="{{ old('t_nama_pembeli') ?? $datakeringanan->t_nama_pembeli }}" readonly="true">
        @if ($errors->has('t_nama_pembeli'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_nama_pembeli') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">NOP <span style="color:red;">*</span></label>
    <div class="col-sm-4">
        <input type="text" class="form-control" id="t_nop_sppt" name="t_nop_sppt"
            value="{{ old('t_nop_sppt') ?? $datakeringanan->t_nop_sppt }}" readonly>
        @if ($errors->has('t_nop_sppt'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_nop_sppt') }}
            </span>
        @endif
    </div>

</div>
<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Keterangan Permohonan <span
            style="color:red;">*</span></label>
    <div class="col-sm-6">
        <textarea class="form-control" id="t_keterangan_permohonan"
            name="t_keterangan_permohonan">{{ old('t_keterangan_permohoanan') ?? $datakeringanan->t_keterangan_permohoanan }}</textarea>
        @if ($errors->has('t_keterangan_permohonan'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_keterangan_permohonan') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
        <button type="button" id="btnApprove" class="btn btn-primary" data-toggle="modal" data-target="#modal-save"><i
                class="fas fa-check"></i> {{ $sumbit ?? 'Update' }}</button>
        <button type="button" id="btnReject" class="btn btn-danger pull-right" data-toggle="modal"
            data-target="#modal-cancel"><i class="fas fa-close"></i> Batal</button>
    </div>
</div>

<div class="modal fade" id="modal-save">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h6 class="modal-title"><span class="fas fa-send"></span>
                    &nbsp;PERMOHONAN PELAYANAN</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin pengajukan permohonan layanan ini?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span>
                    &nbsp;Tutup
                </button>
                <button type="submit" class="btn btn-success btn-sm"><span class="fas fa-check"></span> &nbsp;IYA
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-cancel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-exclamation"></span>
                    &nbsp;BATAL PERMOHONAN ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin membatalkan pengajuan data permohonan
                    tersebut?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span>
                    &nbsp;Tutup
                </button>
                <a href="{{ url('pelayanan-keringanan') }}" class="btn btn-danger btn-sm"><span
                        class="fas fa-exclamation"></span> &nbsp;IYA
                </a>
            </div>
        </div>
    </div>
</div>
