@extends('layouts.master')

@section('judulkiri')
VALIDASI KABAN
@endsection

@section('content')

<div class="row">
    <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-danger"><i class="fas fa-clipboard-list"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">BELUM DIVALIDASI</span>
            <span class="info-box-number">{{ $count_belum }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-success"><i class="fas fa-check"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">DITERIMA</span>
            <span class="info-box-number">{{ $count_diterima }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-4 col-sm-6 col-12">
        <div class="info-box">
            <span class="info-box-icon bg-warning"><i class="fas fa-minus"></i></span>
            <div class="info-box-content">
            <span class="info-box-text">DITOLAK</span>
            <span class="info-box-number">{{ $count_ditolak }}</span>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Belum Divalidasi Kaban</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Sudah Divalidasi Kaban</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                    <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                        @include('validasi-kaban.datagrid.belum')
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                        @include('validasi-kaban.datagrid.sudah')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;BATALKAN DATA VALIDASI KABAN?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin membatalkan data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">No Daftar</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="kohirHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Tgl Daftar</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="tgldaftarHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Nama Wajib Pajak</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namawpHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-alert">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;INFORMASI PEMBATALAN!</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Silahkan Batalkan Pembayaran terlebih dahulu!</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Ok</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-alertp">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-warning">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;INFORMASI PEMBATALAN!</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Silahkan Batalkan Pemeriksaan terlebih dahulu!</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Ok</button>
            </div>
        </div>
    </div>
</div>

@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif
        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif

        $('#filter-tgldaftar_cari_0').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tgldaftar_cari_0').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tgldaftar_cari_0').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-tgldaftar_cari_1').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tgldaftar_cari_1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tgldaftar_cari_1').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
    url: '{{ url('validasi-kaban')}}/datagridbelum',
    table: "#datagrid-table",
    columns: [
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: ""},
        {class: "text-center"},
        {class: "text-right"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
    ],
    orders: [
        {sortable: false},
        {sortable: false},
        {sortable: true,name: "t_kohirspt"},
        {sortable: true,name: "t_tgldaftar_spt"},
        {sortable: true,name: "t_periodespt"},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
    ],

    });

    $("#filter-nodaftar_cari_0, #filter-tahun_cari_0, #filter-namawp_cari_0, #filter-nop_cari_0, #filter-kodebayar_cari_0, #filter-ntpd_cari_0").keyup(function() {
        search();
    });

    $("#filter-tgldaftar_cari_0, #filter-notaris_spt_cari_0, #filter-jenistransaksi_cari_0, #filter-persetujuan_bphtb_cari_0, #filter-status_berkas_cari_0, #filter-status_kabid_cari_0, #filter-pemeriksaan_cari_0, #filter-pembayaran_cari_0").change(function() {
        search();
    });

    function search() {
    datatables.setFilters({
        nodaftar: $("#filter-nodaftar_cari_0").val(),
        tgldaftar: $("#filter-tgldaftar_cari_0").val(),
        tahun: $("#filter-tahun_cari_0").val(),
        notaris: $("#filter-notaris_spt_cari_0").val(),
        jenistransaksi: $("#filter-jenistransaksi_cari_0").val(),
        namawp: $("#filter-namawp_cari_0").val(),
        nop: $("#filter-nop_cari_0").val(),
        status_persetujuan: $("#filter-persetujuan_bphtb_cari_0").val(),
        status_validasi_berkas: $("#filter-status_berkas_cari_0").val(),
        status_validasi_kabid: $("#filter-status_kabid_cari_0").val(),
        status_pemeriksaan: $("#filter-pemeriksaan_cari_0").val(),
        status_pembayaran: $("#filter-pembayaran_cari_0").val(),
        kodebayar: $("#filter-kodebayar_cari_0").val(),
        ntpd: $("#filter-ntpd_cari_0").val()
    });
    datatables.reload();
    }
    search();

    var datatables1 = datagrid({
    url: '{{ url('validasi-kaban')}}/datagrid',
    table: "#datagridsudah-table",
    columns: [
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: ""},
        {class: "text-center"},
        {class: "text-right"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
        {class: "text-center"},
    ],
    orders: [
        {sortable: false},
        {sortable: false},
        {sortable: true,name: "t_kohirspt"},
        {sortable: true,name: "t_tgldaftar_spt"},
        {sortable: true,name: "t_periodespt"},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
        {sortable: false},
    ],

    });

    $("#filter-nodaftar_cari_1, #filter-tahun_cari_1, #filter-namawp_cari_1, #filter-nop_cari_1, #filter-kodebayar_cari_1, #filter-ntpd_cari_1").keyup(function() {
        search_1();
    });

    $("#filter-tgldaftar_cari_1, #filter-notaris_spt_cari_1, #filter-jenistransaksi_cari_1, #filter-persetujuan_bphtb_cari_1, #filter-status_berkas_cari_1, #filter-status_kabid_cari_1, #filter-pemeriksaan_cari_1, #filter-pembayaran_cari_1").change(function() {
        search_1();
    });

    function search_1() {
    datatables1.setFilters({
        nodaftar: $("#filter-nodaftar_cari_1").val(),
        tgldaftar: $("#filter-tgldaftar_cari_1").val(),
        tahun: $("#filter-tahun_cari_1").val(),
        notaris: $("#filter-notaris_spt_cari_1").val(),
        jenistransaksi: $("#filter-jenistransaksi_cari_1").val(),
        namawp: $("#filter-namawp_cari_1").val(),
        nop: $("#filter-nop_cari_1").val(),
        status_persetujuan: $("#filter-persetujuan_bphtb_cari_1").val(),
        status_validasi_berkas: $("#filter-status_berkas_cari_1").val(),
        status_validasi_kabid: $("#filter-status_kabid_cari_1").val(),
        status_pemeriksaan: $("#filter-pemeriksaan_cari_1").val(),
        status_pembayaran: $("#filter-pembayaran_cari_1").val(),
        kodebayar: $("#filter-kodebayar_cari_1").val(),
        ntpd: $("#filter-ntpd_cari_1").val()
    });
    datatables1.reload();
    }
    search_1();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'validasi-kaban/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].t_idspt);
            $("#kohirHapus").val(data[0].t_kohirspt);
            $("#tgldaftarHapus").val(data[0].t_tgldaftar_spt);
            $("#namawpHapus").val(data[0].t_nama_pembeli);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'validasi-kaban/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Validasi Kaban Berhasil dibatalkan!');
            datatables.reload();
            datatables1.reload();
        });
    });
    
    function showAlertDialog() {
        $("#modal-alert").modal('show');
    }
    
    function showAlertpDialog() {
        $("#modal-alertp").modal('show');
    }

</script>
@endpush
