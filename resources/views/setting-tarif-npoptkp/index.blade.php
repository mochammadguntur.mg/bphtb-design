@extends('layouts.master')

@section('judulkiri')
PENGATURAN TARIF NPOPTKP
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="setting-tarif-npoptkp/create" class="btn btn-primary">
                    <i class="fas fa-plus"></i> Tambah
                    </a>
                </h3>
                <div class="card-tools">
                    <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                        <i class="fas fa-file-excel"></i> Excel
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger"  id="btnPDF">
                        <i class="fas fa-file-pdf"></i> PDF
                    </a>
                </div>
            </div>
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Jenis Transaksi</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Tarif NPOPTKP (Rp)</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Tarif NPOPTKP Tambahan (Rp)</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Dasar Hukum</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Tanggal Awal</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Tanggal Akhir</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Status</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Create At</th>
                                <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th><select class="form-control form-control-sm" id="filter-jenistransaksinpoptkp"></select></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-tarifnpoptkp"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-tarifnpoptkptambahan"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-dasarhukum"></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-tanggal_awal" readonly></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-tanggal_akhir" readonly></th>
                                <th>
                                    <select class="form-control form-control-sm" id="filter-status">
                                        <option value="">All</option>
                                        <option value="1">Aktif</option>
                                        <option value="2">Tidak Aktif</option>
                                    </select>
                                </th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-created_at" readonly></th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="10"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Jenis Transaksi</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="jenistransaksinpoptkpHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Tarif NPOPTKP</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="tarifnpoptkpHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>


@endsection
@push('scripts')

<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif

        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif

        $('#filter-tanggal_awal').daterangepicker({
            // singleDatePicker: true,
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tanggal_awal').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tanggal_awal').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-tanggal_akhir').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tanggal_akhir').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tanggal_akhir').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        comboJenisTransaksi();
    });

    var datatables = datagrid({
        url: 'setting-tarif-npoptkp/datagrid',
        table: "#datagrid-table",
        columns: [
            {
                class: ""
            },
            {
                class: "text-right"
            },
            {
                class: "text-right"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "s_idjenistransaksinpoptkp"
            },
            {
                sortable: true,
                name: "s_tarifnpoptkp"
            },
            {
                sortable: true,
                name: "s_tarifnpoptkptambahan"
            },
            {
                sortable: true,
                name: "s_dasarhukumnpoptkp"
            },
            {
                sortable: true,
                name: "s_tglberlaku_awal"
            },
            {
                sortable: true,
                name: "s_tglberlaku_akhir"
            },
            {
                sortable: true,
                name: "s_statusnpoptkp"
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]

    });

    $("#filter-tarifnpoptkp, #filter-tarifnpoptkptambahan, #filter-dasarhukum").keyup(function() {
        search();
    });

    $("#filter-jenistransaksinpoptkp, #filter-status, #filter-tanggal_awal, #filter-tanggal_akhir, #filter-created_at").change(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            jenistransaksinpoptkp: $("#filter-jenistransaksinpoptkp").val(),
            tarifnpoptkp: $("#filter-tarifnpoptkp").val(),
            tarifnpoptkptambahan: $("#filter-tarifnpoptkptambahan").val(),
            dasarhukum: $("#filter-dasarhukum").val(),
            status: $("#filter-status").val(),
            tanggal_awal: $("#filter-tanggal_awal").val(),
            tanggal_akhir: $("#filter-tanggal_akhir").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'setting-tarif-npoptkp/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].s_idtarifnpoptkp);
            $("#jenistransaksinpoptkpHapus").val(data[0].s_idjenistransaksinpoptkp);
            $("#tarifnpoptkpHapus").val(data[0].s_tarifnpoptkp);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'setting-tarif-npoptkp/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Berhasil dihapus!')
            datatables.reload();
        });
    });

    function comboJenisTransaksi() {
        const select = document.getElementById('filter-jenistransaksinpoptkp');
        let jenis_transaksi = JSON.parse('{!!json_encode($jenis_transaksi) !!}');
        let option = '<option class="pl-5" value="">All</option>';

        jenis_transaksi.forEach(el => {
            option += `<option value="${ el.s_idjenistransaksi }">${ el.s_kodejenistransaksi } | ${ el.s_namajenistransaksi }</option>`
        });

        select.innerHTML = option;
    }

    $("#btnExcel, #btnPDF").click(function(a) {
        var s_idjenistransaksinpoptkp = $("#filter-jenistransaksinpoptkp").val();
        var s_tarifnpoptkp = $("#filter-tarifnpoptkp").val();
        var s_tarifnpoptkptambahan = $("#filter-tarifnpoptkptambahan").val();
        var s_dasarhukumnpoptkp = $("#filter-dasarhukum").val();
        var s_tglberlaku_awal = $("#filter-tanggal_awal").val();
        var s_tglberlaku_akhir = $("#filter-tanggal_akhir").val();
        var s_statusnpoptkp = $("#filter-status").val();
        var created_at = $("#filter-created_at").val();
        if(a.target.id == 'btnExcel'){
            window.open('/setting-tarif-npoptkp/export_xls?s_idjenistransaksinpoptkp=' + s_idjenistransaksinpoptkp
             + '&s_tarifnpoptkp=' + s_tarifnpoptkp
             + '&s_tarifnpoptkptambahan=' + s_tarifnpoptkptambahan
             + '&s_dasarhukumnpoptkp=' + s_dasarhukumnpoptkp
             + '&s_tglberlaku_awal=' + s_tglberlaku_awal
             + '&s_tglberlaku_akhir=' + s_tglberlaku_akhir
             + '&s_statusnpoptkp=' + s_statusnpoptkp
             + '&created_at=' + created_at);
        }else{
            window.open('/setting-tarif-npoptkp/export_pdf?s_idjenistransaksinpoptkp=' + s_idjenistransaksinpoptkp
             + '&s_tarifnpoptkp=' + s_tarifnpoptkp
             + '&s_tarifnpoptkptambahan=' + s_tarifnpoptkptambahan
             + '&s_dasarhukumnpoptkp=' + s_dasarhukumnpoptkp
             + '&s_tglberlaku_awal=' + s_tglberlaku_awal
             + '&s_tglberlaku_akhir=' + s_tglberlaku_akhir
             + '&s_statusnpoptkp=' + s_statusnpoptkp
             + '&created_at=' + created_at);
        }

    });

    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").slideUp(500);
    });

</script>
@endpush
