<table style="border-collapse: collapse; width:100%">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">No</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Jenis Transaksi</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Tarif NPOPTKP (Rp)</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Tarif NPOPTKP Tambahan (Rp)</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Dasar Hukum</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Tgl Berlaku Awal</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Tgl Berlaku Akhir</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Status</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Created at</th>
        </tr>
    </thead>
    <tbody>
        @foreach($tarifnpoptkp as $index => $rows)
        {{ error_reporting(0) }}
        <tr>
            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $index + 1 }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->jenisTransaksi['s_namajenistransaksi'] }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->s_tarifnpoptkp }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->s_tarifnpoptkptambahan }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->s_dasarhukumnpoptkp }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->s_tglberlaku_awal }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->s_tglberlaku_akhir }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ ($rows->s_statusnpoptkp=1) ? 'Aktif' : 'Tidak Aktif' }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->created_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
