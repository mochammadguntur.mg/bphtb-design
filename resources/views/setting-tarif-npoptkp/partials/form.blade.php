<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Jenis Transaksi</label>
    <div class="col-md-4">
        <select name="s_idjenistransaksinpoptkp" id="s_idjenistransaksinpoptkp" class="form-control @error('s_idjenistransaksinpoptkp') is-invalid @enderror">
            <option value="">Pilih</option>
            @foreach ($jenis_transaksi as $a)
            <option value="{{ $a['s_idjenistransaksi'] }}" {{ (old('s_idjenistransaksinpoptkp') ?? $tarifnpoptkp->s_idjenistransaksinpoptkp) == $a['s_idjenistransaksi'] ? 'selected' : '' }}> {{ $a['s_kodejenistransaksi'] .' | '. $a['s_namajenistransaksi'] }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_idjenistransaksinpoptkp'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_idjenistransaksinpoptkp') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Tarif NPOPTKP</label>
    <div class="col-md-4">
        <input type="text" name="s_tarifnpoptkp" class="form-control @error('s_tarifnpoptkp') is-invalid @enderror" id="s_tarifnpoptkp" placeholder="Tarif"
               value="{{ old('s_tarifnpoptkp') ?? $tarifnpoptkp->s_tarifnpoptkp }}"
               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_tarifnpoptkp'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_tarifnpoptkp') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Tarif NPOPTKP Tambahan</label>
    <div class="col-md-4">
        <input type="text" name="s_tarifnpoptkptambahan" class="form-control @error('s_tarifnpoptkptambahan') is-invalid @enderror" id="s_tarifnpoptkptambahan" placeholder="Tarif Tambahan"
               value="{{ old('s_tarifnpoptkptambahan') ?? $tarifnpoptkp->s_tarifnpoptkptambahan }}"
               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_tarifnpoptkptambahan'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_tarifnpoptkptambahan') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Dasar Hukum</label>
    <div class="col-md-4">
        <input type="text" name="s_dasarhukumnpoptkp" class="form-control @error('s_dasarhukumnpoptkp') is-invalid @enderror" id="s_dasarhukumnpoptkp" placeholder="Dasar Hukum"
               value="{{ old('s_dasarhukumnpoptkp') ?? $tarifnpoptkp->s_dasarhukumnpoptkp }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_dasarhukumnpoptkp'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_dasarhukumnpoptkp') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Tgl Berlaku Awal</label>
    <div class="col-md-4">
        <input type="date" name="s_tglberlaku_awal" class="form-control @error('s_tglberlaku_awal') is-invalid @enderror" id="s_tglberlaku_awal"
               value="{{ old('s_tglberlaku_awal') ?? $tarifnpoptkp->s_tglberlaku_awal }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_tglberlaku_awal'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_tglberlaku_awal') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Tgl Berlaku Akhir</label>
    <div class="col-md-4">
        <input type="date" name="s_tglberlaku_akhir" class="form-control @error('s_tglberlaku_akhir') is-invalid @enderror" id="s_tglberlaku_akhir"
               value="{{ old('s_tglberlaku_akhir') ?? $tarifnpoptkp->s_tglberlaku_akhir }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_tglberlaku_akhir'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_tglberlaku_akhir') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Status</label>
    <div class="col-md-4">
        <select name="s_statusnpoptkp" class="form-control @error('s_statusnpoptkp') is-invalid @enderror" id="s_statusnpoptkp">
            <option value="">Silahkan Pilih</option>
            @foreach ($status as $status)
            <option {{ (old('s_idjenistransaksinpoptkp') ?? $tarifnpoptkp->s_statusnpoptkp) == $status->s_id_status ? 'selected' : '' }} value="{{ $status->s_id_status }}">{{ $status->s_nama_status }}
            </option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_statusnpoptkp'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_statusnpoptkp') }}
        </div>
        @endif
    </div>
</div>

<a href="{{ url('setting-tarif-npoptkp') }}" class="btn btn-danger"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>
