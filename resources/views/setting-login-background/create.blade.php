@extends('layouts.master')

@section('judulkiri')
Setting Login Background
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary">
            <form action="{{ url('setting-login-background/store') }}" method="POST" enctype="multipart/form-data">
                <div class="card-body mt-4 mb-2">
                    @csrf
                    @include('setting-login-background.partials.form', ['sumbit'=> 'Simpan'])
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
