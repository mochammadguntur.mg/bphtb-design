@extends('layouts.master')

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12">
                <div class="card card-primary">
                    <form action="/setting-login-background/{{ $logins->id }}/edit" method="POST" enctype="multipart/form-data">
                        <div class="card-body mt-4 mb-2">
                            @method('patch')
                            @csrf
                            @include('setting-login-background.partials.form')
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
</section>
@endsection
