<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kecamatan</label>
    <div class="col-md-4">
        <select class="form-control @error('s_idkecamatan') is-invalid @enderror" id="s_idkecamatan" name="s_idkecamatan" {{ $sumbit ?? 'readonly' }}>
            @if ($action == 'edit')
            @foreach ($kecamatan_selected as $kec)
            <option value="{{$kec->s_idkecamatan}}" {{ $sumbit ?? 'readonly' }}> {{$kec->s_kd_kecamatan}} || {{$kec->s_namakecamatan}}</option>  
            @endforeach
            @else
            <option style="display:none" value="">Silahkan Pilih</option>
            @foreach ($kecamatan as $kec)
            <option {{ $kec->s_idkecamatan == old('s_idkecamatan') ? 'selected' : '' }}  value="{{ $kec->s_idkecamatan}}"> {{$kec->s_kd_kecamatan}} || {{$kec->s_namakecamatan}} </option>
            @endforeach
            @endif
        </select>
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_idkecamatan'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_idkecamatan') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Kelurahan</label>
    <div class="col-md-4">
        <input type="text" name="s_kd_kelurahan" class="form-control @error('s_kd_kelurahan') is-invalid @enderror" id="s_kd_kelurahan" placeholder="Kode kelurahan"
               value="{{ old('s_kd_kelurahan') ?? $kelurahan->s_kd_kelurahan }}" {{ $sumbit ?? 'readonly' }} maxlength="3" onkeypress="return numberOnly(event)">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kd_kelurahan'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_kd_kelurahan') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Nama Kelurahan</label>
    <div class="col-md-4">
        <input type="text" name="s_namakelurahan" class="form-control @error('s_namakelurahan') is-invalid @enderror" id="s_namakelurahan" placeholder="Nama kelurahan"
               value="{{ old('s_namakelurahan') ?? $kelurahan->s_namakelurahan }}" {{ $sumbit ?? 'readonly' }}>
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_namakelurahan'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_namakelurahan') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Latitude</label>
    <div class="col-md-4">
        <input type="text" name="s_latitude" class="form-control @error('s_latitude') is-invalid @enderror" id="s_latitude" placeholder="Latitude"
               value="{{ old('s_latitude') ?? $kelurahan->s_latitude }}">
        @if ($errors->has('s_latitude'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_latitude') }}
        </div>
        @endif
    </div>
    <label class="col-sm-2">Longitude</label>
    <div class="col-md-4">
        <input type="text" name="s_longitude" class="form-control @error('s_longitude') is-invalid @enderror" id="s_longitude" placeholder="Longitude"
               value="{{ old('s_longitude') ?? $kelurahan->s_longitude }}">
        @if ($errors->has('s_longitude'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_longitude') }}
        </div>
        @endif
    </div>
</div>
<div class=" form-group" hidden="">
    <center>
        <button type="button" onclick="initMap()" class="btn btn-primary">Lokasi Pilih</button>
        <button type="button" onclick="initMap2()" class="btn btn-primary">Lokasi Ini</button>
    </center>
</div>
<div class="form-group row mb-2" id="googleMap">
    <span id="resMaps"></span>
</div>

<a href="/setting-kelurahan" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>

<style>
    #googleMap {
        height: 400px;
        width: 100%;
        border:1px solid #ccc;
    }
</style>
@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIcfDt4yUMeAiGKDTNyIufrBMuif-efms&callback=initMap"></script>
<script type="text/javascript">
            function numberOnly(e) {
                var input = (e.which) ? e.which : e.keyCode

                if (input > 31 && (input < 48 || input > 57))
                    return false;
                return true;
            }
            // variabel global marker
            var marker;

            function taruhMarker(peta, posisiTitik) {

                if (marker) {
                    // pindahkan marker
                    marker.setPosition(posisiTitik);
                } else {
                    // buat marker baru
                    marker = new google.maps.Marker({
                        position: posisiTitik,
                        map: peta,
                        // draggable:true,
                    });
                }

                // isi nilai koordinat ke form
                document.getElementById("s_latitude").value = posisiTitik.lat();
                document.getElementById("s_longitude").value = posisiTitik.lng();

            }

            function initialize() {
                var lat = document.getElementById('s_latitude').value;
                var lng = document.getElementById('s_longitude').value;
                var latlng;

                if (lat == '' && lng == '') {
                    latlng = new google.maps.LatLng(-3.4512244, 115.5681084);
                } else {
                    latlng = new google.maps.LatLng(parseFloat(lat), parseFloat(lng));
                }

                var propertiPeta = {
                    center: latlng,
                    zoom: 10,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                };

                console.log(propertiPeta);

                var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);

                if (lat != '' && lng != '') {
                    taruhMarker(peta, latlng);
                }

                // even listner ketika peta diklik
                google.maps.event.addListener(peta, 'click', function (event) {
                    taruhMarker(this, event.latLng);
                });

            }


            // event jendela di-load
            google.maps.event.addDomListener(window, 'load', initialize);

</script>
@endpush
