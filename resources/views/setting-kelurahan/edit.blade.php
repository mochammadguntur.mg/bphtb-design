@extends('layouts.master')

@section('judulkiri')
PENGATURAN KELURAHAN [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="/setting-kelurahan/{{ $kelurahan->s_idkelurahan }}/edit" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-kelurahan.partials.form')
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
