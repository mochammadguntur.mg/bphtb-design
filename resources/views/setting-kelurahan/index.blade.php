@extends('layouts.master')

@section('judulkiri')
PENGATURAN KELURAHAN
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible callout callout-success" id="alert">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
              {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-dismissible callout callout-danger" id="alert">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-ban"></i> Gagal!</h5>
              {{ session('error') }}
            </div>
        @endif
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="setting-kelurahan/create" class="btn btn-primary">
                    <i class="fas fa-plus"></i> Tambah
                    </a>
                </h3>
                <h3 class="card-title">
                    <a href="setting-kelurahan/sync" class="btn btn-danger">
                    <i class="fas fa-plus"></i> Sync
                    </a>
                </h3>
                <div class="card-tools">
                    <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                        <i class="fas fa-file-excel"></i> Excel
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger"  id="btnPDF">
                        <i class="fas fa-file-pdf"></i> PDF
                    </a>
                </div>
            </div>
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Kode Kecamatan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Kode Kelurahan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Nama Kelurahan</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Latitude</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Longitude</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Update At</th>
                                <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                            </tr>
                            <tr>
                                <th scope="filter"></th>
                                <th scope="filter"><select class="form-control form-control-sm" id="filter-nama_kecamatan"></select></th>
                                <th scope="filter"><input type="text" class="form-control form-control-sm" id="filter-kd_kelurahan"></th>
                                <th scope="filter"><input type="text" class="form-control form-control-sm" id="filter-namakelurahan"></th>
                                <th scope="filter"><input type="text" class="form-control form-control-sm" id="filter-latitude"></th>
                                <th scope="filter"><input type="text" class="form-control form-control-sm" id="filter-longitude"></th>
                                <th scope="filter"><input type="text" class="form-control form-control-sm" id="filter-updated_at" readonly></th>
                                <th scope="filter"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="8"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Kode Kelurahan</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="kodekelurahanHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Nama Kelurahan</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namakelurahanHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>


@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#filter-updated_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-updated_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-updated_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
        comboKecamatan();
    });

    var datatables = datagrid({
        url: '<?= url('setting-kelurahan/datagrid') ?>',
        table: "#datagrid-table",
        columns: [
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "nama_kecamatan"
            },
            {
                sortable: true,
                name: "s_kd_kelurahan"
            },
            {
                sortable: true,
                name: "s_namakelurahan"
            },
            {
                sortable: true,
                name: "s_latitude"
            },
            {
                sortable: true,
                name: "s_longitude"
            },
            {
                sortable: true,
                name: "updated_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]

    });

    $("#filter-nama_kecamatan").change(function() {
        search();
    });

    $("#filter-kd_kelurahan, #filter-namakelurahan, #filter-latitude, #filter-longitude").keyup(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            kecamatan: $("#filter-nama_kecamatan").val(),
            kd_kelurahan: $("#filter-kd_kelurahan").val(),
            namakelurahan: $("#filter-namakelurahan").val(),
            latitude: $("#filter-latitude").val(),
            longitude: $("#filter-longitude").val(),
            updated_at: $("#filter-updated_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'setting-kelurahan/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data.s_idkelurahan);
            $("#kodekelurahanHapus").val(data.s_kd_kelurahan);
            $("#namakelurahanHapus").val(data.s_namakelurahan);
        });
        $("#modal-delete").modal('show');
    }

    function comboKecamatan(){
        const select = document.getElementById('filter-nama_kecamatan');
        let kecamatan = JSON.parse('{!! json_encode($kecamatan) !!}');
        let option = '<option class="pl-5" value="">Pilih</option>';

        kecamatan.forEach(el => {
            option += `<option value="${ el.s_idkecamatan }">${ el.s_kd_kecamatan } || ${ el.s_namakecamatan }</option> `
        });
        select.innerHTML = option;
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'setting-kelurahan/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Berhasil dihapus!')
            datatables.reload();
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        var kd_kelurahan = $("#filter-kd_kelurahan").val();
        var namakelurahan = $("#filter-namakelurahan").val();
        var updated_at = $("#filter-updated_at").val();
        if(a.target.id == 'btnExcel'){
            window.open('{{ url('setting-kelurahan/export_xls') }}?kd_kelurahan=' + kd_kelurahan + '&namakelurahan=' + namakelurahan + '&updated_at=' + updated_at);
        }else{
            window.open('{{ url('setting-kelurahan/export_pdf') }}?kd_kelurahan=' + kd_kelurahan + '&namakelurahan=' + namakelurahan + '&updated_at=' + updated_at);
        }

    });

    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").slideUp(500);
    });

</script>
@endpush
