@extends('layouts.master')

@section('judulkiri')
PENGATURAN PERSYARATAN
@endsection

@section('content')
<div class="card card-outline card-info shadow-sm">
    <div class="card-header">
        <h3 class="card-title">
            <a href="{{ url('setting-persyaratan/create') }}" class="btn btn-primary">
                <i class="fas fa-plus"></i> Tambah
            </a>
        </h3>
        <div class="card-tools">
            <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                <i class="fas fa-file-excel"></i> Excel
            </a>
            <a href="javascript:void(0)" class="btn btn-danger" id="btnPDF">
                <i class="fas fa-file-pdf"></i> PDF
            </a>
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                <thead>
                    <tr>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Jenis Peralihan Hak </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Nama Persyaratan </th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">CreateAt</th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">UpdateAt</th>
                        <th scope='col' style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi </th>
                    </tr>
                    <tr>
                        <th scope="filter"></th>
                        <th scope="filter">
                            <select class="form-control form-control-sm" id="filter-jenis_peralihan">
                            </select>
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-nama_persyaratan">
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-created_at" readonly>
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-update_at" readonly>
                        </th>
                        <th scope="filter"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="8"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix pagination-footer">
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Jenis Peralihan Hak</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="jenisPeralihanHakHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Nama Persyaratan</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namaHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        if ('{{ session("success") }}') {
            toastr.success('{{ session("success") }}')
        }

        if ('{{ session("error") }}') {
            toastr.error('{{ session("error") }}')
        }

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });
        
        $('#filter-update_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        comboJenisPeralihan();
    });

    var datatables = datagrid({
        url: 'setting-persyaratan/datagrid',
        table: "#datagrid-table",
        columns: [{
                class: ""
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "jenis_peralihan"
            },
            {
                sortable: true,
                // name: "nama_persyaratan"
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: true,
                name: "update_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]

    });

    $("#filter-jenis_peralihan, #filter-nama_persyaratan, #filter-created_at, #filter-update_at").change(function() {
        search();
    });

    $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search();
    });

    $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search();
    });
    
    $('#filter-update_at').on('apply.daterangepicker', function(ev, picker) {
        $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        search();
    });

    $('#filter-update_at').on('cancel.daterangepicker', function(ev, picker) {
        $(this).val('');
        search();
    });

    function search() {
        datatables.setFilters({
            jenis_peralihan: $("#filter-jenis_peralihan").val(),
            nama_persyaratan: $("#filter-nama_persyaratan").val(),
            created_at: $("#filter-created_at").val(),
            update_at: $("#filter-update_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: `setting-persyaratan/detail/${ a }`,
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            console.log(data);
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data.s_idpersyaratan);
            $("#jenisPeralihanHakHapus").val(data.jenis_transaksi.s_namajenistransaksi);
            $("#namaHapus").val(data.s_namapersyaratan);
        });
        $("#modal-delete").modal('show');
    }

    function comboJenisPeralihan() {
        const select = document.getElementById('filter-jenis_peralihan');
        let jenis_hak = JSON.parse('{!!json_encode($jenis_hak) !!}');
        let option = '<option class="pl-5" value="">Pilih</option>';

        jenis_hak.forEach(el => {
            option += `<option value="${ el.s_idjenistransaksi }">${ el.s_kodejenistransaksi } | ${ el.s_namajenistransaksi }</option>`
        });

        select.innerHTML = option;
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();

        $.ajax({
            url: "setting-persyaratan/" + id,
            method: "DELETE",
            data: {
                _token: "{{ csrf_token() }}",
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            datatables.reload();
            toastr.success(data['success']);
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        var jenis_peralihan = $("#filter-jenis_peralihan").val();
        var nama_persyaratan = $("#filter-nama_persyaratan").val();
        var created_at = $("#filter-created_at").val();

        if (a.target.id == 'btnExcel') {
            window.open(`{{ route('setting-persyaratan.export_xls') }}?jenis_peralihan=${ jenis_peralihan }&nama_persyaratan=${ nama_persyaratan }&created_at=${ created_at }`);
        } else {
            window.open(`{{ route('setting-persyaratan.export_pdf') }}?jenis_peralihan=${ jenis_peralihan }&nama_persyaratan=${ nama_persyaratan }&created_at=${ created_at }`);
        }

    });
</script>
@endpush