@extends('layouts.master')

@section('judulkiri')
PENGATURAN PERSYARATAN [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="{{ url('setting-persyaratan/'. $persyaratan->s_idpersyaratan) }}" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-persyaratan.partials.form', ['sumbit'=> 'Update', 'icon' => 'fas fa-save'])
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
