<table style="border-collapse: collapse">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold">No</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold;">Jensi Peralihan Hak</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold;">Nama Persyaratan</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:center; text-align: center; font-weight: bold;">Create At</th>
        </tr>
    </thead>
    <tbody>
        @foreach($persyaratans as $index => $persyaratan)
        {{ error_reporting(0) }}
        <tr>
            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $index + 1 }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $persyaratan->jenisTransaksi->s_namajenistransaksi }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $persyaratan->s_namapersyaratan }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ date('d/m/Y', strtotime($persyaratan->created_at)) }}</td>
        </tr>
        @endforeach
    </tbody>
</table>