<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Jenis Peralihan Hak</label>
    <div class="col-md-4">
        <select name="s_idjenistransaksi" id="s_idjenistransaksi" class="form-control @error('s_idjenistransaksi') is-invalid @enderror" onchange="cariMaxUrut(this.value)">
            <option value="">Pilih</option>
            @foreach ($jenis_hak as $a)
            <option value="{{ $a['s_idjenistransaksi'] }}" {{ (old('s_idjenistransaksi') ?? $persyaratan->s_idjenistransaksi) == $a['s_idjenistransaksi'] ? 'selected' : '' }}> {{ $a['s_kodejenistransaksi'] .' | '. $a['s_namajenistransaksi'] }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_idjenistransaksi'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_idjenistransaksi') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Nama Persyaratan</label>
    <div class="col-md-4">
        <input type="text" name="s_namapersyaratan" class="form-control @error('s_namapersyaratan') is-invalid @enderror" id="s_namapersyaratan" placeholder="Nama Persyaratan" value="{{ old('s_namapersyaratan') ?? $persyaratan->s_namapersyaratan }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_namapersyaratan'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_namapersyaratan') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Urutan</label>
    <div class="col-md-4">
        <input type="text" name="order" id="order" class="form-control @error('order') is-invalid @enderror" value="{{ old('order') ?? $persyaratan->order }}" placeholder="0" onkeypress="return numberOnly(event)">
    </div>
    <div class="col-md-4">
        @if ($errors->has('order'))
        <div class="text-danger mt-2">
            {{ $errors->first('order') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Wajib Upload</label>
    <div class="col-md-4">
        <select name="s_idwajib_up" id="s_idwajib_up" class="form-control @error('s_idwajib_up') is-invalid @enderror">
            <option value="">Pilih</option>
            @foreach ($wajib_upload as $a)
            <option value="{{ $a['s_idwajib_up'] }}" {{(old('s_idwajib_up') ?? $persyaratan->s_idwajib_up) == $a['s_idwajib_up'] ? 'selected' : '' }}> {{ $a['s_ket_wjb'] }}</option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_idwajib_up'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_idwajib_up') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Lokasi Menu</label>
    <div class="col-md-4">
        <select name="s_lokasimenu" id="s_lokasimenu" class="form-control">
            <option value="">Pilih</option>
            @foreach ($menus as $a)
            <option value="{{ $a['s_idmenu'] }}" {{ $persyaratan->s_lokasimenu == $a['s_idmenu'] ? 'selected' : '' }}> {{ $a['s_nama_menu'] }}</option>
            @endforeach
        </select>
    </div>
</div>

<a href="{{ route('setting-persyaratan') }}" class="btn btn-danger"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{ $sumbit }}</button>

@push('scripts')
<script type="text/javascript">
    function numberOnly(e) {
        var input = (e.which) ? e.which : e.keyCode

        if (input > 31 && (input < 48 || input > 57))
            return false;
        return true;
    }

    function cariMaxUrut(a) {
        $.ajax({
            url: `cariMaxUrut/${ a }`,
            type: 'GET',
            data: {
                id: a
            }
        }).then(function (data) {
            $("#order").val(data.max_urut);
        });
    }
</script>
@endpush