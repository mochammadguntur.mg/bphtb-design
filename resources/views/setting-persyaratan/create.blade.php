@extends('layouts.master')

@section('judulkiri')
PENGATURAN PERSYARATAN [TAMBAH]
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary">
            <form action="{{ url('setting-persyaratan') }}" method="POST">
                <div class="card-body mt-4 mb-2">
                    @csrf
                    @include('setting-persyaratan.partials.form', ['sumbit'=> 'Simpan', 'icon' => 'fas fa-save'])
                </div>
            </form>
        </div>
    </div>

</div>

@endsection