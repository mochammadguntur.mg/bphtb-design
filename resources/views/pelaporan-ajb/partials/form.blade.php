@include('lihatdata.informasitransaksi')
@include('lihatdata.informasipembeli')
@include('lihatdata.informasipenjual')
<div class="row">
    @include('lihatdata.informasitanah')
</div>
<div class="form-group row mb-1">
    <label class="col-md-3">No Daftar</label>
    <div class="col-md-3">
        <input type="hidden" name="t_idspt" id="t_idspt" value="{{ old('t_idspt') ?? $spt->t_idspt ?? $ajb->t_idspt}}">
        <input type="hidden" name="t_idnotaris_spt" id="t_idnotaris_spt" value="{{ old('t_idnotaris_spt') ?? $spt->t_idnotaris_spt}}">
        <input type="text" name="t_kohirspt" id="t_kohirspt" class="form-control col-md-3" value="{{ old('t_kohirspt') ?? $spt->t_kohirspt }}" readonly>
        @if ($errors->has('t_idspt'))
        <div class="text-danger mt-2">
            {{ $errors->first('t_idspt') }}
        </div>
        @endif
    </div>
</div>
<div class="form-group row mb-1">
    <label class="col-md-3">No Akta</label>
    <div class="col-md-3">
        <input type="text" name="t_no_ajb" id="t_no_ajb" class="form-control" value="{{ old('t_no_ajb') ?? $ajb->t_no_ajb ?? ''  }}">
        @if ($errors->has('t_no_ajb'))
        <div class="text-danger mt-2">
            {{ $errors->first('t_no_ajb') }}
        </div>
        @endif
    </div>
</div>

<div class="form-group row mb-1">
    <label class="col-md-3">Tanggal Akta</label>
    <div class="col-md-3">
        <div class="input-group date" id="tgl_ajb" data-target-input="nearest">
            <input type="text" name="t_tgl_ajb" id="t_tgl_ajb" value="{{ old('t_tgl_ajb') ?? $ajb->t_tgl_ajb ?? '' }}" class="form-control datetimepicker-input" data-target="#tgl_ajb" />
            <div class="input-group-append" data-target="#tgl_ajb" data-toggle="datetimepicker">
                <div class="input-group-text"><i class="fa fa-calendar"></i></div>
            </div>
        </div>
        @if ($errors->has('t_tgl_ajb'))
        <div class="text-danger mt-2">
            {{ $errors->first('t_tgl_ajb') }}
        </div>
        @endif  
    </div>
</div>
<a href="{{ route('pelaporan-ajb') }}" class="btn btn-danger"><i class="fas fa-undo"></i> Kembali</a>
<button type="submit" class="btn btn-primary float-right"><i class="{{ $icon }}"></i> {{ $sumbit }}</button>
