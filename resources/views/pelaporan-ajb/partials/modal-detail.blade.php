<div class="modal fade" id="modal-detail">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h6 class="modal-title"><span class="fas fa-info"></span> &nbsp;DETAIL DATA</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="post">
                    <ul class="nav nav-pills justify-content-center">
                        <li class="nav-item my-1">
                            <a class="nav-link active py-1 px-2" href="#transaksi" data-toggle="tab">Informasi Transaksi</a>
                        </li>
                        <li class="nav-item my-1">
                            <a class="nav-link py-1 px-2" href="#wp-data" data-toggle="tab">Pembeli</a>
                        </li>
                        <li class="nav-item my-1">
                            <a class="nav-link  py-1 px-2" href="#penjual-data" data-toggle="tab">Penjual</a>
                        </li>
                        <li class="nav-item my-1">
                            <a class="nav-link  py-1 px-2" href="#objek-data" data-toggle="tab">Informasi Tanah</a>
                        </li>
                    </ul>
                </div>

                <div class="tab-content">
                    <div class="tab-pane active" id="transaksi">
                        <div class="col-sm-12 invoice-col">
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Tanggal Daftar</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="tgl_dafatar_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Jenis Transasi</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="jns_transaksi_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Nama PPAT/Notaris</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="ppat_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Nilai Transaksi</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="nilai_transaksi_detail"></span>
                                </div>
                                <div class="col-sm-3">
                                    <b>NPOP</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="npop_detail"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="wp-data">
                        <div class="col-sm-12 invoice-col">
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>NIK</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="nik_pembeli_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>NPWP</b> 
                                </div>
                                <div class="col-sm-3">
                                    : <span id="npwp_pembeli_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Nama WP</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="nama_wp_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Jalan</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="jalan_wp_detai"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>RT/RW</b> 
                                </div>
                                <div class="col-sm-3">
                                    : <span id="rt_rw_wp_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Kelurahan</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="kel_wp_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Kecamatan</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="kec_wp_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Kab./Kota</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="kab_kot_wp_detail"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="tab-pane" id="penjual-data">
                        <div class="col-sm-12 invoice-col">
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>NIK</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="nik_penjual_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>NPWP</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="npwp_penjual_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Nama Penjual</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="nama_penjual_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Jalan</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="jalan_penjual_detai"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>RT/RW</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="rt_rw_penjual_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Kelurahan</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="kel_penjual_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Kecamatan</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="kec_penjual_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-3">
                                    <b>Kab./Kota</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="kab_kot_penjual_detail"></span>
                                </div>
                            </div>
                                                        
                            
                        </div>
                    </div>

                    <div class="tab-pane col-sm-12" id="objek-data">
                        <div class="col-sm-12 invoice-col">
                            <div class="row">
                                <div class="col-sm-2">
                                    <b>NOP</b>
                                </div>
                                <div class="col-sm-4">
                                    : <span id="nop_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <b>Nama SPPT</b>
                                </div>
                                <div class="col-sm-4">
                                    : <span id="nama_sppt_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <b>Letak Objek</b>
                                </div>
                                <div class="col-sm-4">
                                    : <span id="letak_detail"></span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-2">
                                    <b>Luas Tanah</b>
                                </div>
                                <div class="col-sm-4">
                                    : <span id="luas_tanah_detail"></span>
                                </div>
                                <div class="col-sm-3">
                                    <b>Luas bangunan</b>
                                </div>
                                <div class="col-sm-3">
                                    :  <span id="luas_bangunan_detail"></span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <b>NJOP/m<sup>2</sup> Tanah</b>
                                </div>
                                <div class="col-sm-4">
                                    : <span id="njopm_detail"></span>
                                </div>
                                <div class="col-sm-3">
                                    <b>NJOP/m<sup>2</sup> bangunan</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="njopm_bangunan_detail"></span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <b>NJOP Tanah</b>
                                </div>
                                <div class="col-sm-4">
                                    : <span id="njop_detail"></span>
                                </div>
                                <div class="col-sm-3">
                                    <b>NJOP bangunan</b>
                                </div>
                                <div class="col-sm-3">
                                    : <span id="njop_bangunan_detail"></span>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-sm-2">
                                    <b>Total NJOP</b>
                                </div>
                                <div class="col-sm-4">
                                    : <span id="tot_njop_detail"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tutup</button>

            </div>
        </div>
    </div>
</div>