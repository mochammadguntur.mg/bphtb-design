@extends('layouts.master')

@section('judulkiri')
INPUT AJB BARU [TAMBAH]
@endsection

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="card card-primary">
            <form action="{{ route('pelaporan-ajb.store', $spt) }}" method="POST">
                <div class="card-body mt-4 mb-2">
                    @csrf
                    @include('pelaporan-ajb.partials.form', ['sumbit'=> 'Simpan', 'icon' => 'fas fa-save'])
                </div>
            </form>
        </div>
    </div>
</div>

@include('pelaporan-ajb.partials.modal-spt')
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(() => {
        $('#tgl_ajb').datetimepicker({
            format: 'DD-MM-YYYY'
        });

        $('#filter-tgl_daftar').datepicker({
            format: "dd-mm-yyyy",
        });

        comboJenisPeralihan();
        datatables.reload();
    });
</script>
@endpush
