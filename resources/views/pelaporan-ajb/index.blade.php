@extends('layouts.master')

@section('judulkiri')
DATA AKTA TRANSAKSI BPHTB
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Belum Dilaporkan</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Sudah Dilaporkan</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                    <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                        @include('pelaporan-ajb.datagrid.belum')
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                        @include('pelaporan-ajb.datagrid.sudah')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('pelaporan-ajb.partials.modal-delete')
@include('pelaporan-ajb.partials.modal-detail')
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagridmodif.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#filter-tgl_daftar').datepicker({
            format: "dd-mm-yyyy",
        });

        $('#filter-tgl_ajb').datepicker({
            format: "dd-mm-yyyy",
        });
        comboJenisPeralihan();
        comboPPAT()
        search();
    });

    $(document).ready(function () {
        $('#filter-tgldaftar_cari_0').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tgldaftar_cari_0').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tgldaftar_cari_0').on('cancel.datepicker', function (ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-tgldaftar_cari_1').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tgldaftar_cari_1').on('apply.daterangepicker', function (ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tgldaftar_cari_1').on('cancel.datepicker', function (ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
        url: '<?= url('pelaporan-ajb') ?>/datagridbelum',
        table: "#datagrid-table",
        columns: [
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: "text-right"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: false},
            {sortable: true, name: "t_kohirspt"},
            {sortable: true, name: "t_tgldaftar_spt"},
            {sortable: true, name: "t_periodespt"},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
        ],

    });

    $("#filter-nodaftar_cari_0, #filter-tahun_cari_0, #filter-namawp_cari_0, #filter-nop_cari_0, #filter-kodebayar_cari_0, #filter-ntpd_cari_0").keyup(function () {
        search();
    });

    $("#filter-tgldaftar_cari_0, #filter-notaris_spt_cari_0, #filter-jenistransaksi_cari_0, #filter-persetujuan_bphtb_cari_0, #filter-status_berkas_cari_0, #filter-status_kabid_cari_0, #filter-pembayaran_cari_0").change(function () {
        search();
    });

    function search() {
        datatables.setFilters({
            nodaftar: $("#filter-nodaftar_cari_0").val(),
            tgldaftar: $("#filter-tgldaftar_cari_0").val(),
            tahun: $("#filter-tahun_cari_0").val(),
            notaris: $("#filter-notaris_spt_cari_0").val(),
            jenistransaksi: $("#filter-jenistransaksi_cari_0").val(),
            namawp: $("#filter-namawp_cari_0").val(),
            nop: $("#filter-nop_cari_0").val(),
            status_persetujuan: $("#filter-persetujuan_bphtb_cari_0").val(),
            status_validasi_berkas: $("#filter-status_berkas_cari_0").val(),
            status_validasi_kabid: $("#filter-status_kabid_cari_0").val(),
            status_pembayaran: $("#filter-pembayaran_cari_0").val(),
            kodebayar: $("#filter-kodebayar_cari_0").val(),
            ntpd: $("#filter-ntpd_cari_0").val()
        });
        datatables.reload();
    }
    search();

    var datatables1 = datagrid({
        url: '<?= url('pelaporan-ajb') ?>/datagrid',
        table: "#datagridsudah-table",
        columns: [
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: ""},
            {class: "text-center"},
            {class: "text-right"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: false},
            {sortable: true, name: "t_kohirspt"},
            {sortable: true, name: "t_tgldaftar_spt"},
            {sortable: true, name: "t_periodespt"},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
            {sortable: false},
        ],

    });

    $("#filter-nodaftar_cari_1, #filter-tahun_cari_1, #filter-namawp_cari_1, #filter-nop_cari_1, #filter-kodebayar_cari_1, #filter-ntpd_cari_1").keyup(function () {
        search_1();
    });

    $("#filter-tgldaftar_cari_1, #filter-notaris_spt_cari_1, #filter-jenistransaksi_cari_1, #filter-persetujuan_bphtb_cari_1, #filter-status_berkas_cari_1, #filter-status_kabid_cari_1, #filter-pembayaran_cari_1").change(function () {
        search_1();
    });

    function search_1() {
        datatables1.setFilters({
            nodaftar: $("#filter-nodaftar_cari_1").val(),
            tgldaftar: $("#filter-tgldaftar_cari_1").val(),
            tahun: $("#filter-tahun_cari_1").val(),
            notaris: $("#filter-notaris_spt_cari_1").val(),
            jenistransaksi: $("#filter-jenistransaksi_cari_1").val(),
            namawp: $("#filter-namawp_cari_1").val(),
            nop: $("#filter-nop_cari_1").val(),
            status_persetujuan: $("#filter-persetujuan_bphtb_cari_1").val(),
            status_validasi_berkas: $("#filter-status_berkas_cari_1").val(),
            status_validasi_kabid: $("#filter-status_kabid_cari_1").val(),
            status_pembayaran: $("#filter-pembayaran_cari_1").val(),
            kodebayar: $("#filter-kodebayar_cari_1").val(),
            ntpd: $("#filter-ntpd_cari_1").val()
        });
        datatables1.reload();
    }
    search_1();

    function showModalInput() {
        $('#modal-input').modal('show');
    }

    const options = {weekday: 'long', year: 'numeric', month: 'long', day: 'numeric'};

    function showDeleteDialog(a) {
        $.ajax({
            url: `pelaporan-ajb/detail/${ a }`,
            type: 'GET',
            data: {
                id: a
            }
        }).then(function (data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data.t_idajb);
            $("#noDaftarHapus").val(data.spt.t_kohirspt)
            $("#namaWpHapus").val(data.spt.t_nama_pembeli);
            $("#nopHapus").val(data.spt.t_nop_sppt);
            $("#tglAktaHapus").val(moment(data.t_tgl_ajb).format("L"));
            $("#noAktaHapus").val(data.t_no_ajb);
        });
        $("#modal-delete").modal('show');
    }

    function showDetailDialog(a) {
        $.ajax({
            url: `pelaporan-ajb/get_detile_spt/${ a }`,
            type: 'GET',
            data: {
                id: a
            }
        }).then(function (data) {
            console.log(data);
            $("#tgl_dafatar_detail").text(data.t_kohirspt);
            $("#jns_transaksi_detail").text(data.s_namajenistransaksi);
            $("#ppat_detail").text(data.s_namanotaris);
            $("#nilai_transaksi_detail").text(data.t_nilai_bphtb_fix);
            $("#npop_detail").text(data.t_npop_bphtb);

            $("#nik_pembeli_detail").text(data.t_nik_pembeli);
            $("#npwp_pembeli_detail").text(data.t_npwp_pembeli);
            $("#nama_wp_detail").text(data.t_nama_pembeli);
            $("#jalan_wp_detai").text(data.t_jalan_pembeli);
            $("#rt_rw_wp_detail").text(data.rt_rw_pembeli);
            $("#kel_wp_detail").text(data.t_namakelurahan_pembeli);
            $("#kec_wp_detail").text(data.t_namakecamatan_pembeli);
            $("#kab_kot_wp_detail").text(data.t_kabkota_pembeli);

            $("#nik_penjual_detail").text(data.t_nik_penjual);
            $("#npwp_penjual_detail").text(data.t_npwp_penjual);
            $("#nama_penjual_detail").text(data.t_nama_penjual);
            $("#jalan_penjual_detai").text(data.t_jalan_penjual);
            $("#rt_rw_penjual_detail").text(data.rt_rw_penjual);
            $("#kel_penjual_detail").text(data.t_namakel_penjual);
            $("#kec_penjual_detail").text(data.t_namakec_penjual);
            $("#kab_kot_penjual_detail").text(data.t_kabkota_penjual);

            $("#nop_detail").text(data.t_nop_sppt);
            $("#nama_sppt_detail").text(data.t_nama_sppt);
            $("#letak_detail").text(data.t_jalan_sppt);
            $("#luas_tanah_detail").text(data.t_luastanah);
            $("#njopm_detail").text(data.t_njoptanah);
            $("#njop_detail").text(data.t_totalnjoptanah);
            $("#luas_bangunan_detail").text(data.t_luasbangunan);
            $("#njopm_bangunan_detail").text(data.t_njopbangunan);
            $("#njop_bangunan_detail").text(data.t_totalnjopbangunan);
            $("#tot_njop_detail").text(data.t_grandtotalnjop);

            $("#modal-detail").modal('show');
        });
    }

    $("#btnHapus").click(function () {
        var id = $("#idHapus").val();

        $.ajax({
            url: "pelaporan-ajb/" + id,
            method: "DELETE",
            data: {
                _token: "{{ csrf_token() }}",
                id: id
            }
        }).then(function (data) {
            $("#modal-delete").modal("hide");
            datatables.reload();
            toastr.success(data['success']);
        });
    });

    $("#btnExcel, #btnPDF").click(function (a) {
        var no_daftar = '';
        var tgl_daftar = '';
        var jenis_peralihan = $("#filter-jenis_peralihan").val();
        var nama_wp = $("#filter-nama_wp").val();
        var no_ajb = $("#filter-no_ajb").val();
        var tgl_ajb = $("#filter-tgl_ajb").val();

        if (a.target.id == 'btnExcel') {
            window.open(
                    `{{ url('pelaporan-ajb/export_xls') }}?no_daftar=${ no_daftar }&tgl_daftar=${ tgl_daftar }&jenis_peralihan=${ jenis_peralihan }&nama_wp=${ nama_wp }&no_ajb=${ no_ajb }&tgl_ajb=${ tgl_ajb }`
                    );
        } else {
            window.open(
                    `{{ url('pelaporan-ajb/export_pdf') }}?no_daftar=${ no_daftar }&tgl_daftar=${ tgl_daftar }&jenis_peralihan=${ jenis_peralihan }&nama_wp=${ nama_wp }&no_ajb=${ no_ajb }&tgl_ajb=${ tgl_ajb }`
                    );
        }

    });
</script>
@endpush