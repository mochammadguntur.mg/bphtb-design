@extends('layouts.master')

@section('judulkiri')
INPUT AJB BARU [EDIT]
@endsection

@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="card card-primary">
            <form action="{{ url('pelaporan-ajb/'. $ajb->t_idajb) }}" method="POST">
            <div class="card-body mt-4 mb-2">
                @method('put')
                @csrf
                @include('pelaporan-ajb.partials.form', ['sumbit'=> 'Simpan', 'icon' => 'fas fa-save','cari' => 'disabled'])
            </div>
            </form>
        </div>
    </div>
</div>
@endsection