<table>
    <tr>
        <td colspan="7">DAFTAR AJB SUDAH SELESAI</td>
    </tr>
</table>

<table style="border-collapse: collapse;">
    <thead>
        <tr>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 8px">No </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">No Daftar
            </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 15px">Tgl Daftar
            </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 28px">Jenis
                Transaksi </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 25px">Nama Wajib
                Pajak </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 19px">No AJB Baru
            </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 15px">Tgl AJB Baru
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($ajb as $index => $v)
        <tr>
            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">
                {{ $index + 1 }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ $v->spt->t_kohirspt }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;text-align: center;">
                {{ Carbon\Carbon::parse($v->spt->t_tgldaftar_spt)->format('d-m-Y') }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ $v->spt->jenisTransaksi->s_namajenistransaksi }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ $v->spt->t_nama_pembeli }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ $v->t_no_ajb }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ Carbon\Carbon::parse($v->tgl_ajb)->format('d-m-Y') }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>