@extends('layouts.master')

@section('judulkiri')
VALIDASI KABID
@endsection

@section('content')

<link rel="stylesheet" href="{{ asset('internet/css/blueimp-gallery.min.css')}}">
<style>
    #map {
        width: 100%;
        height: 400px;
    }
</style>

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">FORM VALIDASI KABID</h3>
    </div>

    <!-- /.card-header -->
    <!-- form start -->
    <form method="post" action="{{ url('pemeriksaan/simpanpemeriksaan') }}" id="formtambah" name="formtambah">
        <br>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h5>Informasi Transaksi</h5>
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <div class="row p-0">
                                <div class="col-md-6">
                                    <div class="text-left">Tanggal Daftar: <b class="text-primary">{{date('d-m-Y', strtotime($dataspt->t_tgldaftar_spt))}}</b>
                                    </div>
                                </div>
                            </div>
                            <div class="row p-0">
                                <div class="col-md-6">
                                    <div class="text-left">Jenis Transaksi: {{$dataspt->s_namajenistransaksi}}</div>
                                </div>
                                <div class="col-md-6">
                                    <div class="text-left">Notaris: {{$dataspt->s_namanotaris}}</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h5>Pembeli</h5>
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            @csrf
                            <u class="text-primary" style="font-size: 14px;"><strong>Pribadi</strong></u>
                            <div class="row mt-2">
                                <div class="col-sm-6">
                                    NIK : {{$dataspt->t_nik_pembeli}}
                                </div>
                                <div class="col-sm-6">
                                    NPWP : {{$dataspt->t_npwp_pembeli}}
                                </div>
                            </div>
                            <div>Nama : {{$dataspt->t_nama_pembeli}}</div>
                            <div class="mt-2">Jalan : {{$dataspt->t_jalan_pembeli}}</div>
                            <div class="row">
                                <div class="col-sm-6">
                                    RT/RW : {{$dataspt->t_rt_pembeli}} / {{$dataspt->t_rw_pembeli}}
                                </div>
                                <div class="col-sm-6">
                                    Kelurahan : {{$dataspt->t_namakelurahan_pembeli}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    Kecamatan : {{$dataspt->t_namakecamatan_pembeli}}
                                </div>
                                <div class="col-sm-6">
                                    Kab/Kota : {{$dataspt->t_kabkota_pembeli}}
                                </div>
                            </div>
                            <div>
                                Kode Pos : {{$dataspt->t_kodepos_pembeli}}
                            </div>
                            <div class="row mt-2">
                                <div class="col-sm-6">
                                    No.Telp : {{$dataspt->t_nohp_pembeli}}
                                </div>
                                <div class="col-sm-6">
                                    Email : {{$dataspt->t_email_pembeli}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($dataspt->t_idbidang_usaha == 2)

                <div class="col-md-6">
                    <h5>Penanggung Jawab (Badan)</h5>
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <u class="text-primary" style="font-size: 14px;"><strong>Badan</strong></u>
                            <div class="row mt-2">
                                <div class="col-sm-6">
                                    NIK : {{$dataspt->t_b_nik_pngjwb_pembeli}}
                                </div>
                                <div class="col-sm-6">
                                    NPWP : {{$dataspt->t_b_npwp_pngjwb_pembeli}}
                                </div>
                            </div>
                            <div>Nama : {{$dataspt->t_b_nama_pngjwb_pembeli}}</div>
                            <div class="mt-2">Jalan : {{$dataspt->t_b_jalan_pngjwb_pembeli}}</div>
                            <div class="row">
                                <div class="col-sm-6">
                                    RT/RW : {{$dataspt->t_b_rt_pngjwb_pembeli}} / {{$dataspt->t_b_rw_pngjwb_pembeli}}
                                </div>
                                <div class="col-sm-6">
                                    Kelurahan : {{$dataspt->t_b_namakel_pngjwb_pembeli}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    Kecamatan : {{$dataspt->t_b_namakec_pngjwb_pembeli}}
                                </div>
                                <div class="col-sm-6">
                                    Kab/Kota : {{$dataspt->t_b_kabkota_pngjwb_pembeli}}
                                </div>
                            </div>
                            <div>
                                Kode Pos : {{$dataspt->t_b_kodepos_pngjwb_pembeli}}
                            </div>
                            <div class="row mt-2">
                                <div class="col-sm-6">
                                    No.Telp : {{$dataspt->t_b_nohp_pngjwb_pembeli}}
                                </div>
                                <div class="col-sm-6">
                                    Email : {{$dataspt->t_b_email_pngjwb_pembeli}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-md-6">
                    <h5>Penjual</h5>
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <u class="text-primary" style="font-size: 14px;"><strong>Pribadi</strong></u>
                            <div class="row mt-2">
                                <div class="col-sm-6">
                                    NIK : {{$dataspt->t_nik_penjual}}
                                </div>
                                <div class="col-sm-6">
                                    NPWP : {{$dataspt->t_npwp_penjual}}
                                </div>
                            </div>
                            <div>Nama : {{$dataspt->t_nama_penjual}}</div>
                            <div class="mt-2">Jalan : {{$dataspt->t_jalan_penjual}}</div>
                            <div class="row">
                                <div class="col-sm-6">
                                    RT/RW : {{$dataspt->t_rt_penjual}} / {{$dataspt->t_rw_penjual}}
                                </div>
                                <div class="col-sm-6">
                                    Kelurahan : {{$dataspt->t_namakel_penjual}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    Kecamatan : {{$dataspt->t_namakec_penjual}}
                                </div>
                                <div class="col-sm-6">
                                    Kab/Kota : {{$dataspt->t_kabkota_penjual}}
                                </div>
                            </div>
                            <div>
                                Kode Pos : {{$dataspt->t_kodepos_penjual}}
                            </div>
                            <div class="row mt-2">
                                <div class="col-sm-6">
                                    No.Telp : {{$dataspt->t_nohp_penjual}}
                                </div>
                                <div class="col-sm-6">
                                    Email : {{$dataspt->t_email_penjual}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @if($dataspt->t_idbidang_usaha == 2)

                <div class="col-md-6">
                    <h5>Penanggung Jawab (Badan)</h5>
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <u class="text-primary" style="font-size: 14px;"><strong>Badan</strong></u>
                            <div class="row mt-2">
                                <div class="col-sm-6">
                                    NIK : {{$dataspt->t_b_nik_pngjwb_penjual}}
                                </div>
                                <div class="col-sm-6">
                                    NPWP : {{$dataspt->t_b_npwp_pngjwb_penjual}}
                                </div>
                            </div>
                            <div>Nama : {{$dataspt->t_b_nama_pngjwb_penjual}}</div>
                            <div class="mt-2">Jalan : {{$dataspt->t_b_jalan_pngjwb_penjual}}</div>
                            <div class="row">
                                <div class="col-sm-6">
                                    RT/RW : {{$dataspt->t_b_rt_pngjwb_penjual}} / {{$dataspt->t_b_rw_pngjwb_penjual}}
                                </div>
                                <div class="col-sm-6">
                                    Kelurahan : {{$dataspt->t_b_namakel_pngjwb_penjual}}
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-6">
                                    Kecamatan : {{$dataspt->t_b_namakec_pngjwb_penjual}}
                                </div>
                                <div class="col-sm-6">
                                    Kab/Kota : {{$dataspt->t_b_kabkota_pngjwb_penjual}}
                                </div>
                            </div>
                            <div>
                                Kode Pos : {{$dataspt->t_b_kodepos_pngjwb_penjual}}
                            </div>
                            <div class="row mt-2">
                                <div class="col-sm-6">
                                    No.Telp : {{$dataspt->t_b_nohp_pngjwb_penjual}}
                                </div>
                                <div class="col-sm-6">
                                    Email : {{$dataspt->t_b_email_pngjwb_penjual}}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
            </div>
        </div>
        <br>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h5>Informasi Tanah</h5>
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="col-sm-12 p-0">
                                        NOP : <b class="text-primary">{{$dataspt->t_nop_sppt}} /
                                            {{$dataspt->t_tahun_sppt}}</b>
                                    </div>
                                    <div>
                                        <div class="col-sm-12 p-0">
                                            Nama SPPPT : {{$dataspt->t_nama_sppt}}
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="col-sm-12 p-0">
                                            Letak Bangunan : {{$dataspt->t_jalan_sppt}}, RT {{$dataspt->t_rt_sppt}},
                                            RW: {{$dataspt->t_rw_sppt}}, Kelurahan {{$dataspt->t_kelurahan_sppt}},
                                            Kecamatan {{$dataspt->t_kecamatan_sppt}}, Kabupaten
                                            {{$dataspt->t_kabkota_sppt}}
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="col-sm-12 p-0">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    <div>
                                                        Luas Tanah : {{ $dataspt->t_luastanah }} m<sup>2</sup>
                                                    </div>
                                                    <div>
                                                        Nilai Pasar :
                                                        {{ number_format($dataspt->t_njoptanah, 0, ',', '.') }}
                                                    </div>
                                                    <div>
                                                        Total :
                                                        {{ number_format($dataspt->t_totalnjoptanah, 0, ',', '.') }}
                                                    </div>
                                                </div>
                                                <div class="col-sm-6">
                                                    <div>
                                                        Luas Bangunan : {{ $dataspt->t_luasbangunan ?: "0" }} m<sup>2</sup>
                                                    </div>
                                                    <div>
                                                        Nilai Pasar :
                                                        {{ number_format($dataspt->t_njopbangunan, 0, ',', '.') }}
                                                    </div>
                                                    <div>
                                                        Total :
                                                        {{ number_format($dataspt->t_totalnjopbangunan, 0, ',', '.') }}
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="col-sm-12 p-0">
                                            Total NJOP PBB : {{number_format($dataspt->t_grandtotalnjop, 0, ',', '.')}}
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <div class="col-sm-12 p-0">
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    Jenis Tanah : Tanah Kosong
                                                </div>
                                                <div class="col-sm-6">
                                                    Status Kepemilikan : {{$dataspt->s_namahaktanah}}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    Jenis Dokumen : {{$dataspt->s_namadoktanah}}
                                                </div>
                                                <div class="col-sm-6">
                                                    No Dokumen : {{$dataspt->t_nosertifikathaktanah}}
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col-sm-6">
                                                    Tanggal Dokumen :
                                                    {{date('d-m-Y', strtotime($dataspt->t_tgldok_tanah))}}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-2">
                                        <label for="">Photo</label>
                                        <div>
                                            @php
                                            $noobjek = 1;
                                            @endphp

                                            @if(count($data_fotoobjek) > 0)
                                            @foreach ($data_fotoobjek as $key => $vobj)

                                            @if(!empty($vobj->nama_file))
                                            <a style="display:none;" data-ng-href="{{ asset($vobj->letak_file.$vobj->nama_file)}}" title="{{$vobj->nama_file}}" download="{{$vobj->nama_file}}" data-gallery="" href="{{asset($vobj->letak_file.$vobj->nama_file)}}">
                                                <img data-ng-src="{{asset($vobj->letak_file.$vobj->nama_file)}}" alt="" src="{{asset($vobj->letak_file.$vobj->nama_file)}}">
                                            </a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                            <a data-ng-switch-when="true" data-ng-href="{{ asset($vobj->letak_file.$vobj->nama_file)}}" title="{{$vobj->nama_file}}" download="{{$vobj->nama_file}}" data-gallery="" class="ng-binding ng-scope" href="{{asset($vobj->letak_file.$vobj->nama_file)}}">
                                                {{$vobj->nama_file}}
                                            </a>
                                            @endif

                                            @php $noobjek++; @endphp
                                            @endforeach
                                            @else
                                            <center>TIDAK ADA DATA</center>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="">
                                        <label for="">Maps</label>
                                        <div id="map"></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <br>
        <div class="col-md-12">
            <h5>Perhitungan BPHTB</h5>
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-2">
                            Luas Tanah/Bumi
                        </div>
                        <div class="col-sm-2">
                            : {{$dataspt->t_luastanah}} m<sup>2</sup>
                        </div>
                        <div class="col-sm-2">
                            NJOP/m<sup>2</sup> Tanah/Bumi
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_njoptanah, 0, ',', '.')}}
                        </div>
                        <div class="col-sm-2">
                            NJOP Tanah/Bumi
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_totalnjoptanah, 0, ',', '.')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            Luas Bangunan
                        </div>
                        <div class="col-sm-2">
                            : {{$dataspt->t_luasbangunan}} m<sup>2</sup>
                        </div>
                        <div class="col-sm-2">
                            NJOP/m<sup>2</sup> Bangunan
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_njopbangunan, 0, ',', '.')}}
                        </div>
                        <div class="col-sm-2">
                            NJOP Bangunan
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_totalnjopbangunan, 0, ',', '.')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            NJOP PBB
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_grandtotalnjop, 0, ',', '.')}}
                        </div>
                        <div class="col-sm-2">
                            Harga Transaksi
                        </div>
                        <div class="col-sm-2">
                            : {{number_format($dataspt->t_nilaitransaksispt, 0, ',', '.')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            NPOP
                        </div>
                        <div class="col-sm-10">
                            : {{number_format($dataspt->t_npop_bphtb, 0, ',', '.')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            NPOPTKP
                        </div>
                        <div class="col-sm-2">
                            : <u>{{number_format($dataspt->t_npoptkp_bphtb, 0, ',', '.')}}
                                &nbsp;&nbsp;&nbsp;&nbsp;-</u>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            NPOPKP
                        </div>
                        <div class="col-sm-10">
                            : {{number_format($dataspt->t_npopkp_bphtb, 0, ',', '.')}}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-2">
                            Tarif (%)
                        </div>
                        <div class="col-sm-10">
                            :
                            <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{number_format($dataspt->t_persenbphtb, 0, ',', '.')}}%&nbsp;&nbsp;&nbsp;&nbsp;x</u>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-2">
                            Jumlah Pajak
                        </div>
                        <div class="col-sm-2">
                            : <b style="color: red; font-size: 17px;">{{number_format($dataspt->t_nilai_bphtb_fix, 0, ',', '.')}}</b>
                        </div>
                    </div>

                </div>

            </div>
        </div>
        <br>
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <h5>Persyaratan</h5>
                    <div class="card card-outline card-primary px-3 pt-3">
                        @php
                        $no = 1;
                        $nomer = 1;
                        $namasyarat = '';
                        @endphp
                        @foreach ($cek_persyaratan as $key => $v)
                        @if($namasyarat <> $v->s_namapersyaratan)
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <span>{{$no}}. {{$v->s_namapersyaratan}}</span>
                                        </div>
                                        @php
                                        $namasyarat = $v->s_namapersyaratan;
                                        $no++;
                                        $nomer = 1;
                                        @endphp

                                        @endif

                                        @if(!empty($v->nama_file))
                                        <div class="col-md-4">
                                            <a style="display:none;" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" href="{{asset($v->letak_file.$v->nama_file)}}"><img data-ng-src="{{asset($v->letak_file.$v->nama_file)}}" alt="" src="{{asset($v->letak_file.$v->nama_file)}}"></a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-ng-switch-when="true" data-ng-href="{{ asset($v->letak_file.$v->nama_file)}}" title="{{$v->nama_file}}" download="{{$v->nama_file}}" data-gallery="" class="ng-binding ng-scope" href="{{asset($v->letak_file.$v->nama_file)}}">{{$v->nama_file}}</a>
                                        </div>
                                        @else
                                        <div class="col-md-4">
                                            <span class="badge badge-danger">Durung di upload.</span>
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            @php $nomer++; @endphp
                            @endforeach
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <h5>Kolom Validasi Berkas</h5>
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <div class="row">
                        <label class="col-sm-4 ">STATUS VALIDASI BERKAS<span style="color: red">*</span></label>
                        <div class="col-md-8">
                            <select name="s_id_status_berkas" id="s_id_status_berkas" class="selectpicker form-control bs-select-hidden" data-live-search="1" required="required" disabled="true">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_statusberkas as $a)
                                <option value="{{ $a->s_id_status_berkas }}" {{ $dataspt->s_id_status_berkas == $a->s_id_status_berkas ? 'selected' : '' }}>
                                    {{ $a->s_nama_status_berkas }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <label class="col-sm-4 ">Keterangan</label>
                        <div class="col-md-8">
                            <textarea name="t_keterangan_berkas" id="t_keterangan_berkas" class="form-control" readonly>{{ $dataspt->t_keterangan_berkas}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <h5>Kolom Validasi Kabid</h5>
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <div class="row">
                        <label class="col-sm-4 ">Status Validasi Kabid<span style="color: red">*</span></label>
                        <div class="col-md-8">

                            <input type="hidden" name="t_id_validasi_berkas" id="t_id_validasi_berkas" value="{{ $dataspt->t_id_validasi_berkas}}">
                            <input type="hidden" name="t_id_validasi_kabid" id="t_id_validasi_kabid" value="{{ $dataspt->t_id_validasi_kabid}}">

                            <select name="s_id_status_kabid" id="s_id_status_kabid" class="selectpicker form-control bs-select-hidden" data-live-search="1" disabled="true" required="required">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_statuskabid as $a)
                                <option value="{{ $a->s_id_status_kabid }}" {{ $dataspt->s_id_status_kabid == $a->s_id_status_kabid ? 'selected' : '' }}>
                                    {{ $a->s_nama_status_kabid }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>

                    <div class="row mt-2">
                        <label class="col-sm-4">Keterangan</label>
                        <div class="col-md-4">
                            <textarea name="t_keterangan_kabid" id="t_keterangan_kabid" disabled="true" class="form-control">{{ $dataspt->t_keterangan_kabid}}</textarea>
                        </div>
                    </div>




                </div>

                <div class="col-md-12">
                    <h5>FORM PEMERIKSAAN</h5>
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <div class="col-sm-12 row mt-4">
                                <label class="col-sm-2 control-label">No Pemeriksaan</label>
                                <div class="col-sm-4">
                                    <input name="t_noperiksa" id="t_noperiksa" class="form-control" required="true" value="{{ @$cek_data_idspt_pemeriksaan->t_noperiksa}}" type="text">
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-4">
                                <label class="col-sm-2 control-label">Keterangan</label>
                                <div class="col-sm-4">
                                    <textarea name="t_keterangan" id="t_keterangan" class="form-control" rows="8">{{ @$cek_data_idspt_pemeriksaan->t_keterangan}}</textarea>
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-4">
                                <label class="col-sm-2 control-label">Luas Tanah (Bumi)</label>
                                @php
                                if(!empty(@$cek_data_idspt_pemeriksaan->t_luastanah_pemeriksa)){
                                $t_luastanah = @$cek_data_idspt_pemeriksaan->t_luastanah_pemeriksa;
                                }else{
                                $t_luastanah = $dataspt->t_luastanah;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_njoptanah_pemeriksa)){
                                $t_njoptanah = @$cek_data_idspt_pemeriksaan->t_njoptanah_pemeriksa;
                                }else{
                                $t_njoptanah = $dataspt->t_njoptanah;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_totalnjoptanah_pemeriksa)){
                                $t_totalnjoptanah = @$cek_data_idspt_pemeriksaan->t_totalnjoptanah_pemeriksa;
                                }else{
                                $t_totalnjoptanah = $dataspt->t_totalnjoptanah;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_luasbangunan_pemeriksa)){
                                $t_luasbangunan = @$cek_data_idspt_pemeriksaan->t_luasbangunan_pemeriksa;
                                }else{
                                $t_luasbangunan = $dataspt->t_luasbangunan;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_njopbangunan_pemeriksa)){
                                $t_njopbangunan = @$cek_data_idspt_pemeriksaan->t_njopbangunan_pemeriksa;
                                }else{
                                $t_njopbangunan = $dataspt->t_njopbangunan;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_totalnjopbangunan_pemeriksa)){
                                $t_totalnjopbangunan = @$cek_data_idspt_pemeriksaan->t_totalnjopbangunan_pemeriksa;
                                }else{
                                $t_totalnjopbangunan = $dataspt->t_totalnjopbangunan;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_grandtotalnjop_pemeriksa)){
                                $t_grandtotalnjop = @$cek_data_idspt_pemeriksaan->t_grandtotalnjop_pemeriksa;
                                }else{
                                $t_grandtotalnjop = $dataspt->t_grandtotalnjop;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_nilaitransaksispt_pemeriksa)){
                                $t_nilaitransaksispt = @$cek_data_idspt_pemeriksaan->t_nilaitransaksispt_pemeriksa;
                                }else{
                                $t_nilaitransaksispt = $dataspt->t_nilaitransaksispt;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_npop_bphtb_pemeriksa)){
                                $t_npop_bphtb = @$cek_data_idspt_pemeriksaan->t_npop_bphtb_pemeriksa;
                                }else{
                                $t_npop_bphtb = $dataspt->t_npop_bphtb;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_npoptkp_bphtb_pemeriksa)){
                                $t_npoptkp_bphtb = @$cek_data_idspt_pemeriksaan->t_npoptkp_bphtb_pemeriksa;
                                }else{
                                    $t_npoptkp_bphtb = $dataspt->t_npoptkp_bphtb;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_npopkp_bphtb_pemeriksa)){
                                $t_npopkp_bphtb = @$cek_data_idspt_pemeriksaan->t_npopkp_bphtb_pemeriksa;
                                }else{
                                $t_npopkp_bphtb = $dataspt->t_npopkp_bphtb;
                                }

                                if(!empty(@$cek_data_idspt_pemeriksaan->t_nilaibphtb_pemeriksa)){
                                $t_nilai_bphtb_fix = @$cek_data_idspt_pemeriksaan->t_nilaibphtb_pemeriksa;
                                }else{
                                $t_nilai_bphtb_fix = $dataspt->t_nilai_bphtb_fix;
                                }
                                @endphp
                                <div class="col-sm-2">
                                    <input name="t_luastanah" id="t_luastanah" class="form-control" value="{{ $t_luastanah}}" onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();" onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();" onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();" onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();" required="required" onkeypress="return numbersonly(this, event); hitungpajakbphtb();" type="text">
                                    <input name="t_luastanah_sismiop" id="t_luastanah_sismiop" value="{{ $dataspt->t_luastanah_sismiop}}" type="hidden">
                                    <input name="t_luasbangunan_sismiop" id="t_luasbangunan_sismiop" value="{{ $dataspt->t_luasbangunan_sismiop}}" type="hidden">
                                    <input name="t_njoptanah_sismiop" id="t_njoptanah_sismiop" value="{{ $dataspt->t_njoptanah_sismiop}}" type="hidden">
                                    <input name="t_njopbangunan_sismiop" id="t_njopbangunan_sismiop" value="{{ $dataspt->t_njopbangunan_sismiop}}" type="hidden">
                                    <input name="t_idtarifbphtb" id="t_idtarifbphtb" value="{{ $dataspt->t_idtarifbphtb}}" type="hidden">
                                </div>
                                <label class="col-sm-2 control-label">Nilai Pasar/NJOP (m2)</label>
                                <div class="col-sm-2">
                                    <input name="t_njoptanah" id="t_njoptanah" class="form-control" value="{{ number_format($t_njoptanah, 0, ',', '.')}}" style="text-align:right;" onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();" onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();" onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();" onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();" required="required" onkeypress="return numbersonly(this, event)" type="text">
                                </div>
                                <label class="col-sm-1 control-label">Total</label>
                                <div class="col-sm-3">
                                    <input name="t_totalnjoptanah" id="t_totalnjoptanah" class="form-control" readonly="readonly" style="text-align:right" value="{{ number_format($t_totalnjoptanah, 0, ',', '.')}}" type="text">
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-2 control-label">Luas Bangunan</label>
                                <div class="col-sm-2">
                                    <input name="t_luasbangunan" id="t_luasbangunan" class="form-control" value="{{ $t_luasbangunan ?? 0 }}" onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();" onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();" onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();" onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();" required="required" onkeypress="return numbersonly(this, event)" type="text">
                                </div>
                                <label class="col-sm-2 control-label">Nilai Pasar/NJOP (m2)</label>
                                <div class="col-sm-2">
                                    <input name="t_njopbangunan" id="t_njopbangunan" class="form-control" value="{{ number_format($t_njopbangunan, 0, ',', '.')}}" style="text-align:right" onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();" onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();" onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();" onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();" required="required" onkeypress="return numbersonly(this, event)" type="text">
                                </div>
                                <label class="col-sm-1 control-label">Total</label>
                                <div class="col-sm-3">
                                    <input name="t_totalnjopbangunan" id="t_totalnjopbangunan" class="form-control" readonly="readonly" style="text-align:right" value="{{ number_format($t_totalnjopbangunan, 0, ',', '.')}}" type="text">
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-9 control-label" style="text-align: right;">NJOP PBB</label>
                                <div class="col-sm-3">
                                    <input name="t_grandtotalnjop" id="t_grandtotalnjop" class="form-control" readonly="readonly" style="text-align:right" value="{{ number_format($t_grandtotalnjop, 0, ',', '.')}}" type="text">
                                </div>
                            </div>

                            <div style="display: none;" class="col-sm-12 row mt-2" id="pembagian_aphb_1">
                                <div style="display: none;" id="pembagian_aphb_2">
                                    <label class="col-sm-9 control-label" id="pembagian_aphb_3" style="text-align: right; display: none;">Pembagian APHB</label>
                                    <div style="display: none;" class="col-sm-3" id="pembagian_aphb_4">
                                        <input style="text-align: right; width: 50px; display: none;" name="t_tarif_pembagian_aphb_kali" id="t_tarif_pembagian_aphb_kali" value="{{ $dataspt->t_tarif_pembagian_aphb_kali}}" onkeyup="this.value = unformatCurrency(this.value);" onchange="this.value = unformatCurrency(this.value);" onblur="this.value = unformatCurrency(this.value);" onfocus="this.value = unformatCurrency(this.value)" onkeypress="return numbersonly(this, event)" type="text">
                                        /
                                        <input style="text-align: right; width: 50px; display: none;" name="t_tarif_pembagian_aphb_bagi" id="t_tarif_pembagian_aphb_bagi" value="{{ $dataspt->t_tarif_pembagian_aphb_bagi}}" onkeyup="this.value = unformatCurrency(this.value);" onchange="this.value = unformatCurrency(this.value);" onblur="this.value = unformatCurrency(this.value);" onfocus="this.value = unformatCurrency(this.value)" onkeypress="return numbersonly(this, event)" type="text">
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-9 control-label" style="text-align: right;">Harga Transaksi / Nilai
                                    Pasar</label>
                                <div class="col-sm-3">
                                    <input name="t_nilaitransaksispt" id="t_nilaitransaksispt" class="form-control" style="text-align:right" value="{{ number_format($t_nilaitransaksispt, 0, ',', '.')}}" onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();" onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();" required="required" onkeypress="return numbersonly(this, event); hitungpajakbphtb();" onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();" onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();" type="text">
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-2 control-label">Pemeriksa 1</label>
                                <div class="col-sm-4">
                                    <select name="t_idpejabat1" id="t_idpejabat1" class="selectpicker form-control bs-select-hidden" data-live-search="1">
                                        <option value="">Silahkan Pilih</option>
                                        @foreach ($data_pejabat as $dpej)
                                        <option value="{{ $dpej->s_idpejabat }}" {{ @$cek_data_idspt_pemeriksaan->t_idpejabat1 == $dpej->s_idpejabat ? 'selected' : '' }}>
                                            {{ $dpej->s_namapejabat }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-2 control-label">Pemeriksa 2</label>
                                <div class="col-sm-4">
                                    <select name="t_idpejabat2" id="t_idpejabat2" class="selectpicker form-control bs-select-hidden" data-live-search="1">
                                        <option value="">Silahkan Pilih</option>
                                        @foreach ($data_pejabat as $dpej1)
                                        <option value="{{ $dpej1->s_idpejabat }}" {{ @$cek_data_idspt_pemeriksaan->t_idpejabat2 == $dpej1->s_idpejabat ? 'selected' : '' }}>
                                            {{ $dpej1->s_namapejabat }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-2 control-label">Pemeriksa 3</label>
                                <div class="col-sm-4">
                                    <select name="t_idpejabat3" id="t_idpejabat3" class="selectpicker form-control bs-select-hidden" data-live-search="1">
                                        <option value="">Silahkan Pilih</option>
                                        @foreach ($data_pejabat as $dpej2)
                                        <option value="{{ $dpej2->s_idpejabat }}" {{ @$cek_data_idspt_pemeriksaan->t_idpejabat3 == $dpej2->s_idpejabat ? 'selected' : '' }}>
                                            {{ $dpej2->s_namapejabat }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-2 control-label">Pemeriksa 4</label>
                                <div class="col-sm-4">
                                    <select name="t_idpejabat4" id="t_idpejabat4" class="selectpicker form-control bs-select-hidden" data-live-search="1">
                                        <option value="">Silahkan Pilih</option>
                                        @foreach ($data_pejabat as $dpej3)
                                        <option value="{{ $dpej3->s_idpejabat }}" {{ @$cek_data_idspt_pemeriksaan->t_idpejabat4 == $dpej3->s_idpejabat ? 'selected' : '' }}>
                                            {{ $dpej3->s_namapejabat }}
                                        </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-12">
                    <h5>HASIL PERHITUNGAN PEMERIKSAAN</h5>
                    <div class="card card-outline card-primary">
                        <div class="card-body">
                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-3 ">NPOP<span style="color: red">*</span></label>
                                <div class="col-md-4">
                                    <input type="hidden" name="t_idpemeriksa" id="t_idpemeriksa" value="{{ @$cek_data_idspt_pemeriksaan->t_idpemeriksa}}">
                                    <input type="hidden" name="t_idspt" id="t_idspt" value="{{ $dataspt->t_idspt}}">
                                    <input name="t_npop_bphtb" id="t_npop_bphtb" class="form-control" value="{{ number_format($t_npop_bphtb, 0, ',', '.')}}" type="text" style="text-align: right;" readonly="true">
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-3 ">NPOPTKP<span style="color: red">*</span></label>
                                <div class="col-md-4">
                                    <input name="t_npoptkp_bphtb" id="t_npoptkp_bphtb" class="form-control" value="{{ number_format($t_npoptkp_bphtb, 0, ',', '.')}}" type="text" style="text-align: right;" readonly="true">
                                </div>
                                <?php
                                if ($dataspt->t_npoptkp_bphtb > 0) {
                                    $ket_npoptkp = 'Dapat NPOPTKP';
                                } else {
                                    $ket_npoptkp = 'Tidak Dapat NPOPTKP';
                                }
                                ?>
                                <div class="col-sm-4" id="ket_npoptkpdiv">
                                    <input class="form-control" name="ket_npoptkp" id="ket_npoptkp" value="{{$ket_npoptkp}}" style="font-weight:bold;font-size:25px;color:white;background-color:#1E8E3E;text-align:center;" readonly="" type="text">
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-3 ">NPOPKP<span style="color: red">*</span></label>
                                <div class="col-md-4">
                                    <input name="t_npopkp_bphtb" id="t_npopkp_bphtb" class="form-control" value="{{ number_format($t_npopkp_bphtb, 0, ',', '.')}}" type="text" style="text-align: right;" readonly="true">
                                </div>
                                <label class="col-sm-2 control-label">Tarif (%)</label>
                                <div class="col-sm-1">
                                    <input name="t_persenbphtb" id="t_persenbphtb" class="form-control" readonly="readonly" value="{{ $dataspt->t_persenbphtb}}" type="text">
                                </div>
                            </div>
                            <div class="col-sm-12 row mt-2">
                                <label class="col-sm-3 ">BPHTB YANG HARUS DIBAYAR<span style="color: red">*</span></label>
                                <div class="col-md-4">
                                    <input name="t_nilai_bphtb_fix" id="t_nilai_bphtb_fix" class="form-control" value="{{ number_format($t_nilai_bphtb_fix, 0, ',', '.')}}" type="text" style="text-align: right;" readonly="true">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="card-footer">
                    <a href="{{ url('pemeriksaan') }}" class="btn btn-outline-primary prevBtn pull-left" type="button"><i class="fa fa-refresh"></i> KEMBALI </a>
                    <!-- <a href="{{ url('pemeriksaan') }}/{{ $dataspt->t_idspt}}/tahapkedua" class="btn btn-primary prevBtn pull-right" type="button"><i class="fa fa-save"></i> SIMPAN </a> -->
                    <button id="pendaftaranbutton" type="submit" class="btn btn-primary pull-right">
                        <i class="fa fa-save"></i>
                        SIMPAN
                    </button>
                </div>
            </div>
        </div>
    </form>
</div>



<div class="modal fade" id="loadingsimpanvalidasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" style="background:transparent;">
    <center><img style="background:transparent;" src='{{asset('upload/loading77.gif')}}'></center>
</div>

<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
    <div class="slides"></div>
    <h3 class="title"></h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
</div>

@endsection


@push('scriptsbawah')

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly" defer></script>


<script type="text/javascript">
    $(function() {
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}'
            }
        });
    });

    function initMap() {
        var latnya = {{ (empty($dataspt->s_latitude)) ? -7.423844 : $dataspt->s_latitude }};
        var longnya = {{ (empty($dataspt->s_longitude)) ? 109.231146 : $dataspt->s_longitude }};

        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 13,
            center: {
                lat: latnya,
                lng: longnya
            },
        });

        marker = new google.maps.Marker({
            map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: {
                lat: latnya,
                lng: longnya
            },
        });

        marker.addListener("click", toggleBounce);

        google.maps.event.addListener(marker, 'dragend', function(marker) {
            var latLng = marker.latLng;
        });

    }

    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }

    // $('#pendaftaranbutton').click(function() {
    //     var myForm = document.getElementById('formtambah');
    //     myForm.onsubmit = function() {
    //         var allInputs = myForm.getElementsByTagName('input');
    //         var input, i;

    //         for (i = 0; input = allInputs[i]; i++) {
    //             if (input.getAttribute('name') && !input.value) {
    //                 //input.setAttribute('name', '');

    //             } else {
    //                 jQuery('#loadingsimpanvalidasi').modal('show');
    //             }
    //         }
    //     };
    // });


    function hitungpajakbphtb() {
        $.ajax({
            url: `{{ url('pemeriksaan/hitung-pemeriksaan') }}`,
            type: 'POST',
            data: {
                t_idspt: document.getElementById('t_idspt').value,
                t_luastanah: document.getElementById('t_luastanah').value,
                t_njoptanah: document.getElementById('t_njoptanah').value,
                t_luasbangunan: document.getElementById('t_luasbangunan').value,
                t_njopbangunan: document.getElementById('t_njopbangunan').value,
                t_nilaitransaksispt: document.getElementById('t_nilaitransaksispt').value,
            }
        }).then((data) => {
            console.log(data);
            document.getElementById('t_totalnjopbangunan').value = formatCurrency(data.t_totalnjoptanah);
            document.getElementById('t_grandtotalnjop').value = formatCurrency(data.t_grandtotalnjop);
            document.getElementById('t_npop_bphtb').value = formatCurrency(data.t_npop_bphtb);
            document.getElementById('t_npopkp_bphtb').value = formatCurrency(data.t_npoptkp_bphtb);
            document.getElementById('t_nilai_bphtb_fix').value = formatCurrency(data.t_nilai_bphtb_fix);
        });

        var t_luastanahnya = ($('#t_luastanah').val() == '') ? 0 : unformatCurrency($('#t_luastanah').val());
        var t_njoptanahnya = ($('#t_njoptanah').val() == '') ? 0 : unformatCurrency($('#t_njoptanah').val());
        var t_totalnjoptanah = Math.ceil(t_luastanahnya * t_njoptanahnya);

        $('#t_totalnjoptanah').val(t_totalnjoptanah);

        var t_luasbangunannya = ($('#t_luasbangunan').val() == '') ? 0 : unformatCurrency($('#t_luasbangunan').val());
        var t_njopbangunannya = ($('#t_njopbangunan').val() == '') ? 0 : unformatCurrency($('#t_njopbangunan').val());

        var t_totalnjopbangunan = Math.ceil(t_luasbangunannya * t_njopbangunannya);
        $('#t_totalnjopbangunan').val(t_totalnjopbangunan);
        document.getElementById('t_totalnjopbangunan').value = formatCurrency(document.getElementById('t_totalnjopbangunan').value);

        var t_grandtotalnjop = t_totalnjoptanah + t_totalnjopbangunan;
        $('#t_grandtotalnjop').val(t_grandtotalnjop);
        document.getElementById('t_grandtotalnjop').value = formatCurrency(document.getElementById('t_grandtotalnjop').value);

        var t_nilaitransaksisptnya = ($('#t_nilaitransaksispt').val() == '') ? 0 : unformatCurrency($('#t_nilaitransaksispt').val());

        var s_idstatus_pht = {{ $cek_jenis_transaksi->s_idstatus_pht }};

        switch (s_idstatus_pht) {
            case 2:
                var t_npop_bphtb = t_grandtotalnjop;
                break;
            case 3:
                var t_npop_bphtb = t_grandtotalnjop;
                break;
            default:
                if (t_grandtotalnjop > t_nilaitransaksisptnya) {
                    var t_npop_bphtb = t_grandtotalnjop;
                } else {
                    var t_npop_bphtb = t_nilaitransaksisptnya;
                }
                break;
        }

        $('#t_npop_bphtb').val(t_npop_bphtb);
        document.getElementById('t_npop_bphtb').value = formatCurrency(document.getElementById('t_npop_bphtb').value);

        var t_npoptkp_bphtbnya = ($('#t_npoptkp_bphtb').val() == '') ? 0 : unformatCurrency($('#t_npoptkp_bphtb').val());
        var t_npopkp_bphtb = t_npop_bphtb - t_npoptkp_bphtbnya;

        if (t_npopkp_bphtb <= 0) {
            t_npopkp_bphtb = 0;
        } else {
            t_npopkp_bphtb = t_npopkp_bphtb;
        }
        $('#t_npopkp_bphtb').val(t_npopkp_bphtb);
        document.getElementById('t_npopkp_bphtb').value = formatCurrency(document.getElementById('t_npopkp_bphtb').value);

        var t_persenbphtb = ($('#t_persenbphtb').val()) / 100;

        @if(!empty($dataspt->t_idjenisfasilitas))
            var t_nilai_bphtb_fix = t_npopkp_bphtb * t_persenbphtb;
        @else
            @if(!empty($dataspt->t_idpengurangan))
                var tarifpengurangan = {{ $tarif_pengurangan }};
                var hitung_bphtbnya = t_npopkp_bphtb * t_persenbphtb;
                var hitung_potonganya = hitung_bphtbnya * tarifpengurangan;
                var t_nilai_bphtb_fix = hitung_bphtbnya - hitung_potonganya;
            @else
                var t_nilai_bphtb_fix = t_npopkp_bphtb * t_persenbphtb;
            @endif
        @endif


        $('#t_nilai_bphtb_fix').val(t_nilai_bphtb_fix);
        document.getElementById('t_nilai_bphtb_fix').value = formatCurrency(document.getElementById('t_nilai_bphtb_fix').value);
    }
</script>

<script src="{{asset('internet/js/jquery.blueimp-gallery.min.js')}}"></script>
@endpush