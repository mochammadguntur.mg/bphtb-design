@extends('layouts.master')

@section('judulkiri')
PEMERIKSAAN
@endsection

@section('content')

<link rel="stylesheet" href="{{ asset('internet/css/blueimp-gallery.min.css') }}">
<style>
    #map {
        width: 100%;
        height: 400px;
    }

</style>

<div class="card card-primary">
    <div class="card-header">
        <h3 class="card-title">FORM PEMERIKSAAN</h3>
    </div>
    <form method="post" action="{{ url('pemeriksaan/simpanpemeriksaan') }}" id="formtambah" name="formtambah">
        <br>
        <div class="col-md-12">
            @include('lihatdata.informasitransaksi')

            @include('lihatdata.informasipembeli')
            @include('lihatdata.informasipenjual')
        </div>
        <br>
        @include('lihatdata.informasitanah')
        <br>
        @include('lihatdata.perhitunganbphtb')
        <br>
        @include('lihatdata.informasipersyaratan')
        @include('lihatdata.informasivalidasi')


        <div class="col-md-12">
            <h5>FORM PEMERIKSAAN</h5>
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <div class="col-sm-12 row mt-4">
                        <label class="col-sm-2 control-label">No Pemeriksaan<span style="color: red">*</span></label>
                        <div class="col-sm-4">
                            <input name="t_noperiksa" id="t_noperiksa" class="form-control" required="true"
                                   value="{{ @$cek_data_idspt_pemeriksaan->t_noperiksa }}" type="text">
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-4">
                        <label class="col-sm-2 control-label">Keterangan</label>
                        <div class="col-sm-10">
                            <textarea name="t_keterangan" id="t_keterangan" class="form-control" required="true"
                                      rows="4">{{ @$cek_data_idspt_pemeriksaan->t_keterangan }}</textarea>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-4">
                        <label class="col-sm-2 control-label">Luas Tanah (Bumi)<span style="color: red">*</span></label>
                        @php
                        if (!empty(@$cek_data_idspt_pemeriksaan->t_luastanah_pemeriksa)) {
                        $t_luastanah = @$cek_data_idspt_pemeriksaan->t_luastanah_pemeriksa;
                        } else {
                        $t_luastanah = $dataspt->t_luastanah;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_njoptanah_pemeriksa)) {
                        $t_njoptanah = @$cek_data_idspt_pemeriksaan->t_njoptanah_pemeriksa;
                        } else {
                        $t_njoptanah = $dataspt->t_njoptanah;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_totalnjoptanah_pemeriksa)) {
                        $t_totalnjoptanah = @$cek_data_idspt_pemeriksaan->t_totalnjoptanah_pemeriksa;
                        } else {
                        $t_totalnjoptanah = $dataspt->t_totalnjoptanah;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_luasbangunan_pemeriksa)) {
                        $t_luasbangunan = @$cek_data_idspt_pemeriksaan->t_luasbangunan_pemeriksa;
                        } else {
                        $t_luasbangunan = $dataspt->t_luasbangunan;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_njopbangunan_pemeriksa)) {
                        $t_njopbangunan = @$cek_data_idspt_pemeriksaan->t_njopbangunan_pemeriksa;
                        } else {
                        $t_njopbangunan = $dataspt->t_njopbangunan;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_totalnjopbangunan_pemeriksa)) {
                        $t_totalnjopbangunan = @$cek_data_idspt_pemeriksaan->t_totalnjopbangunan_pemeriksa;
                        } else {
                        $t_totalnjopbangunan = $dataspt->t_totalnjopbangunan;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_grandtotalnjop_pemeriksa)) {
                        $t_grandtotalnjop = @$cek_data_idspt_pemeriksaan->t_grandtotalnjop_pemeriksa;
                        } else {
                        $t_grandtotalnjop = $dataspt->t_grandtotalnjop;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_nilaitransaksispt_pemeriksa)) {
                        $t_nilaitransaksispt = @$cek_data_idspt_pemeriksaan->t_nilaitransaksispt_pemeriksa;
                        } else {
                        $t_nilaitransaksispt = $dataspt->t_nilaitransaksispt;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_npop_bphtb_pemeriksa)) {
                        $t_npop_bphtb = @$cek_data_idspt_pemeriksaan->t_npop_bphtb_pemeriksa;
                        } else {
                        $t_npop_bphtb = $dataspt->t_npop_bphtb;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_npoptkp_bphtb_pemeriksa)) {
                        $t_npoptkp_bphtb = @$cek_data_idspt_pemeriksaan->t_npoptkp_bphtb_pemeriksa;
                        } else {
                        $t_npoptkp_bphtb = $dataspt->t_npoptkp_bphtb;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_npopkp_bphtb_pemeriksa)) {
                        $t_npopkp_bphtb = @$cek_data_idspt_pemeriksaan->t_npopkp_bphtb_pemeriksa;
                        } else {
                        $t_npopkp_bphtb = $dataspt->t_npopkp_bphtb;
                        }

                        if (!empty(@$cek_data_idspt_pemeriksaan->t_nilaibphtb_pemeriksa)) {
                        $t_nilai_bphtb_fix = @$cek_data_idspt_pemeriksaan->t_nilaibphtb_pemeriksa;
                        } else {
                        $t_nilai_bphtb_fix = $dataspt->t_nilai_bphtb_fix;
                        }
                        @endphp
                        <div class="col-sm-2">
                            <input name="t_luastanah" id="t_luastanah" class="form-control" value="{{ $t_luastanah }}"
                                   onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();"
                                   required="required" onkeypress="return numbersonly(this, event); hitungpajakbphtb();"
                                   type="text" style="text-align: right">
                            <input name="t_luastanah_sismiop" id="t_luastanah_sismiop"
                                   value="{{ $dataspt->t_luastanah_sismiop }}" type="hidden">
                            <input name="t_luasbangunan_sismiop" id="t_luasbangunan_sismiop"
                                   value="{{ $dataspt->t_luasbangunan_sismiop }}" type="hidden">
                            <input name="t_njoptanah_sismiop" id="t_njoptanah_sismiop"
                                   value="{{ $dataspt->t_njoptanah_sismiop }}" type="hidden">
                            <input name="t_njopbangunan_sismiop" id="t_njopbangunan_sismiop"
                                   value="{{ $dataspt->t_njopbangunan_sismiop }}" type="hidden">
                            <input name="t_idtarifbphtb" id="t_idtarifbphtb" value="{{ $dataspt->t_idtarifbphtb }}"
                                   type="hidden">
                        </div>
                        <label class="col-sm-2 control-label">Nilai Pasar/NJOP (m2)<span
                                style="color: red">*</span></label>
                        <div class="col-sm-2">
                            <input name="t_njoptanah" id="t_njoptanah" class="form-control"
                                   value="{{ number_format($t_njoptanah, 0, ',', '.') }}" style="text-align:right;"
                                   onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();"
                                   required="required" onkeypress="return numbersonly(this, event)" type="text">
                        </div>
                        <label class="col-sm-1 control-label">Total</label>
                        <div class="col-sm-3">
                            <input name="t_totalnjoptanah" id="t_totalnjoptanah" class="form-control"
                                   readonly="readonly" style="text-align:right"
                                   value="{{ number_format($t_totalnjoptanah, 0, ',', '.') }}" type="text">
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 control-label">Luas Bangunan<span style="color: red">*</span></label>
                        <div class="col-sm-2">
                            <input name="t_luasbangunan" id="t_luasbangunan" class="form-control"
                                   value="{{ $t_luasbangunan ?? 0 }}"
                                   onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();"
                                   required="required" onkeypress="return numbersonly(this, event)" type="text"
                                   style="text-align: right">
                        </div>
                        <label class="col-sm-2 control-label">Nilai Pasar/NJOP (m2)<span
                                style="color: red">*</span></label>
                        <div class="col-sm-2">
                            <input name="t_njopbangunan" id="t_njopbangunan" class="form-control"
                                   value="{{ number_format($t_njopbangunan, 0, ',', '.') }}" style="text-align:right"
                                   onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();"
                                   required="required" onkeypress="return numbersonly(this, event)" type="text">
                        </div>
                        <label class="col-sm-1 control-label">Total</label>
                        <div class="col-sm-3">
                            <input name="t_totalnjopbangunan" id="t_totalnjopbangunan" class="form-control"
                                   readonly="readonly" style="text-align:right"
                                   value="{{ number_format($t_totalnjopbangunan, 0, ',', '.') }}" type="text">
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-9 control-label" style="text-align: right;">NJOP PBB</label>
                        <div class="col-sm-3">
                            <input name="t_grandtotalnjop" id="t_grandtotalnjop" class="form-control"
                                   readonly="readonly" style="text-align:right"
                                   value="{{ number_format($t_grandtotalnjop, 0, ',', '.') }}" type="text">
                        </div>
                    </div>

                    <div style="display: none;" class="col-sm-12 row mt-2" id="pembagian_aphb_1">
                        <div style="display: none;" id="pembagian_aphb_2">
                            <label class="col-sm-9 control-label" id="pembagian_aphb_3"
                                   style="text-align: right; display: none;">Pembagian APHB</label>
                            <div style="display: none;" class="col-sm-3" id="pembagian_aphb_4">
                                <input style="text-align: right; width: 50px; display: none;"
                                       name="t_tarif_pembagian_aphb_kali" id="t_tarif_pembagian_aphb_kali"
                                       value="{{ $dataspt->t_tarif_pembagian_aphb_kali }}"
                                       onkeyup="this.value = unformatCurrency(this.value);"
                                       onchange="this.value = unformatCurrency(this.value);"
                                       onblur="this.value = unformatCurrency(this.value);"
                                       onfocus="this.value = unformatCurrency(this.value)"
                                       onkeypress="return numbersonly(this, event)" type="text">
                                /
                                <input style="text-align: right; width: 50px; display: none;"
                                       name="t_tarif_pembagian_aphb_bagi" id="t_tarif_pembagian_aphb_bagi"
                                       value="{{ $dataspt->t_tarif_pembagian_aphb_bagi }}"
                                       onkeyup="this.value = unformatCurrency(this.value);"
                                       onchange="this.value = unformatCurrency(this.value);"
                                       onblur="this.value = unformatCurrency(this.value);"
                                       onfocus="this.value = unformatCurrency(this.value)"
                                       onkeypress="return numbersonly(this, event)" type="text">
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-9 control-label" style="text-align: right;">Hasil Pemeriksaan atas Harga Transaksi / Nilai Pasar</label>
                        <div class="col-sm-3">
                            <input name="t_nilaitransaksispt" id="t_nilaitransaksispt" class="form-control"
                                   style="text-align:right" value="{{ number_format($t_nilaitransaksispt, 0, ',', '.') }}"
                                   onkeyup="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onfocus="this.value = unformatCurrency(this.value); hitungpajakbphtb();"
                                   required="required" onkeypress="return numbersonly(this, event); hitungpajakbphtb();"
                                   onchange="this.value = formatCurrency(this.value); hitungpajakbphtb();"
                                   onblur="this.value = formatCurrency(this.value); hitungpajakbphtb();" type="text">
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 control-label">Pemeriksa 1<span style="color: red">*</span></label>
                        <div class="col-sm-4">
                            <select name="t_idpejabat1" id="t_idpejabat1" required="true"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_pejabat as $dpej)
                                <option value="{{ $dpej->s_idpejabat }}"
                                        {{ @$cek_data_idspt_pemeriksaan->t_idpejabat1 == $dpej->s_idpejabat ? 'selected' : '' }}>
                                    {{ $dpej->s_namapejabat }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 control-label">Pemeriksa 2<span style="color: red">*</span></label>
                        <div class="col-sm-4">
                            <select name="t_idpejabat2" id="t_idpejabat2" required="true"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_pejabat as $dpej1)
                                <option value="{{ $dpej1->s_idpejabat }}"
                                        {{ @$cek_data_idspt_pemeriksaan->t_idpejabat2 == $dpej1->s_idpejabat ? 'selected' : '' }}>
                                    {{ $dpej1->s_namapejabat }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 control-label">Pemeriksa 3</label>
                        <div class="col-sm-4">
                            <select name="t_idpejabat3" id="t_idpejabat3"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_pejabat as $dpej2)
                                <option value="{{ $dpej2->s_idpejabat }}"
                                        {{ @$cek_data_idspt_pemeriksaan->t_idpejabat3 == $dpej2->s_idpejabat ? 'selected' : '' }}>
                                    {{ $dpej2->s_namapejabat }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-2 control-label">Pemeriksa 4</label>
                        <div class="col-sm-4">
                            <select name="t_idpejabat4" id="t_idpejabat4"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1">
                                <option value="">Silahkan Pilih</option>
                                @foreach ($data_pejabat as $dpej3)
                                <option value="{{ $dpej3->s_idpejabat }}"
                                        {{ @$cek_data_idspt_pemeriksaan->t_idpejabat4 == $dpej3->s_idpejabat ? 'selected' : '' }}>
                                    {{ $dpej3->s_namapejabat }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-12">
            <h5>HASIL PERHITUNGAN PEMERIKSAAN</h5>
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-3 ">NPOP<span style="color: red">*</span></label>
                        <div class="col-md-4">
                            <input type="hidden" name="t_idpemeriksa" id="t_idpemeriksa"
                                   value="{{ @$cek_data_idspt_pemeriksaan->t_idpemeriksa }}">
                            <input type="hidden" name="t_idspt" id="t_idspt" value="{{ $dataspt->t_idspt }}">
                            <input name="t_npop_bphtb" id="t_npop_bphtb" class="form-control"
                                   value="{{ number_format($t_npop_bphtb, 0, ',', '.') }}" type="text"
                                   style="text-align: right;" readonly="true">
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-3 ">NPOPTKP<span style="color: red">*</span></label>
                        <div class="col-md-4">
                            <input name="t_npoptkp_bphtb" id="t_npoptkp_bphtb" class="form-control"
                                   value="{{ number_format($t_npoptkp_bphtb, 0, ',', '.') }}" type="text"
                                   style="text-align: right;" readonly="true">
                        </div>

                        @php
                        $ket_npoptkp = ($dataspt->t_npoptkp_bphtb > 0)
                        ? 'Dapat NPOPTKP'
                        : 'TidakDapat NPOPTKP';
                        @endphp

                        <div class="col-sm-4" id="ket_npoptkpdiv">
                            <input class="form-control" name="ket_npoptkp" id="ket_npoptkp" value="{{ $ket_npoptkp }}"
                                   style="font-weight:bold;font-size:25px;color:white;background-color:#1E8E3E;text-align:center;"
                                   readonly="" type="text">
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-3 ">NPOPKP<span style="color: red">*</span></label>
                        <div class="col-md-4">
                            <input name="t_npopkp_bphtb" id="t_npopkp_bphtb" class="form-control"
                                   value="{{ number_format($t_npopkp_bphtb, 0, ',', '.') }}" type="text"
                                   style="text-align: right;" readonly="true">
                        </div>
                        <label class="col-sm-2 control-label">Tarif (%)</label>
                        <div class="col-sm-1">
                            <input name="t_persenbphtb" id="t_persenbphtb" class="form-control" readonly="readonly"
                                   value="{{ $dataspt->t_persenbphtb }}" type="text">
                        </div>
                    </div>
                    <div class="col-sm-12 row mt-2">
                        <label class="col-sm-3 ">BPHTB YANG HARUS DIBAYAR<span style="color: red">*</span></label>
                        <div class="col-md-4">
                            <input name="t_nilai_bphtb_fix" id="t_nilai_bphtb_fix" class="form-control"
                                   value="{{ number_format($t_nilai_bphtb_fix, 0, ',', '.') }}" type="text"
                                   style="text-align: right;" readonly="true">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="card-footer">
            <a href="{{ url('pemeriksaan') }}" class="btn btn-outline-primary prevBtn pull-left" type="button"><i class="fa fa-refresh"></i> KEMBALI </a>
            <button id="pendaftaranbutton" type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i>SIMPAN</button>
        </div>

    </form>
    <div class="modal fade" id="loadingsimpanvalidasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         style="background:transparent;">
        <center><img style="background:transparent;" src='{{ asset('upload/loading77.gif') }}'></center>
    </div>

    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
    </div>
    @endsection
    @push('scriptsbawah')
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly" defer></script>
    <script type="text/javascript">
                                       $(function () {
                                           $.ajaxSetup({
                                               headers: {
                                                   'X-CSRF-Token': '{{ csrf_token() }}'
                                               }
                                           });
                                       });

                                       function initMap() {
                                           var latnya = <?= empty($dataspt->s_latitude) ? -7.423844 : $dataspt->s_latitude ?>;
                                           var longnya = <?= empty($dataspt->s_longitude) ? 109.231146 : $dataspt->s_longitude ?>;

                                           const map = new google.maps.Map(document.getElementById("map"), {
                                               zoom: 13,
                                               center: {
                                                   lat: latnya,
                                                   lng: longnya
                                               },
                                           });

                                           marker = new google.maps.Marker({
                                               map,
                                               draggable: true,
                                               animation: google.maps.Animation.DROP,
                                               position: {
                                                   lat: latnya,
                                                   lng: longnya
                                               },
                                           });

                                           marker.addListener("click", toggleBounce);

                                           google.maps.event.addListener(marker, 'dragend', function (marker) {
                                               var latLng = marker.latLng;
                                           });

                                       }

                                       function toggleBounce() {
                                           if (marker.getAnimation() !== null) {
                                               marker.setAnimation(null);
                                           } else {
                                               marker.setAnimation(google.maps.Animation.BOUNCE);
                                           }
                                       }

                                       $('#pendaftaranbutton').click(function () {
                                           var myForm = document.getElementById('formtambah');
                                           myForm.onsubmit = function () {
                                               var allInputs = myForm.getElementsByTagName('input');
                                               var input, i;

                                               for (i = 0; input = allInputs[i]; i++) {
                                                   if (input.getAttribute('name') && !input.value) {
                                                       //input.setAttribute('name', '');

                                                   } else {
                                                       jQuery('#loadingsimpanvalidasi').modal('show');
                                                   }
                                               }
                                           };
                                       });


                                       function hitungpajakbphtb() {
                                           $.ajax({
                                               url: `{{ url('pemeriksaan/hitung-pemeriksaan') }}`,
                                               type: 'POST',
                                               data: {
                                                   t_idspt: document.getElementById('t_idspt').value,
                                                   t_luastanah: document.getElementById('t_luastanah').value,
                                                   t_njoptanah: document.getElementById('t_njoptanah').value,
                                                   t_luasbangunan: document.getElementById('t_luasbangunan').value,
                                                   t_njopbangunan: document.getElementById('t_njopbangunan').value,
                                                   t_nilaitransaksispt: document.getElementById('t_nilaitransaksispt').value,
                                               }
                                           }).then((res) => {
                                               let data = res[0];
                                               $('#t_totalnjoptanah').val(formatRupiah(data.t_totalnjoptanah.toString(), 0));
                                               $('#t_totalnjopbangunan').val(formatRupiah(data.t_totalnjopbangunan.toString(), 0))
                                               $('#t_grandtotalnjop').val(formatRupiah(data.t_grandtotalnjop.toString(), 0))
                                               $('#t_npop_bphtb').val(formatRupiah(data.t_npop_bphtb.toString(), 0))
                                               $('#t_npopkp_bphtb').val(formatRupiah(data.t_npopkp_bphtb.toString(), 0))
                                               $('#t_nilai_bphtb_fix').val(formatRupiah(data.t_nilai_bphtb_fix.toString(), 0))
                                           });
                                       }

    </script>
    <script src="{{ asset('internet/js/jquery.blueimp-gallery.min.js') }}"></script>
    @endpush
