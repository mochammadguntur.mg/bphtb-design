@extends('layouts.master')

@section('judulkiri')
    LIHAT DATA
@endsection

@section('content')

    <link rel="stylesheet" href="{{ asset('internet/css/blueimp-gallery.min.css') }}">
    <style>
        #map {
            width: 100%;
            height: 400px;
        }

    </style>

    @if (!empty($dataspt->t_idpemeriksa))

        <div class="card card-primary">
            <div class="card-header">
                <h3 class="card-title">LIHAT DATA</h3>
            </div>

            <!-- /.card-header -->
            <!-- form start -->
            <form method="post" action="{{ url('pemeriksaan/simpanpemeriksaan') }}" id="formtambah" name="formtambah">
                <br>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Informasi Transaksi</h5>
                            @include('pemeriksaan/partials/info_transaksi')
                        </div>
                    </div>
                    @include('pemeriksaan/partials/pembeli_penjual')
                </div>
                <br>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Informasi Tanah</h5>
                            @include('pemeriksaan/partials/info_tanah')
                        </div>
                    </div>
                </div>
                <br>
                <div class="col-md-12">
                    <h5>Perhitungan BPHTB</h5>
                    @include('pemeriksaan/partials/perhitungan')
                </div>
                <br>
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-12">
                            <h5>Persyaratan</h5>
                            <div class="card card-outline card-primary px-3 pt-3">
                                @php
                                $no = 1;
                                $nomer = 1;
                                $namasyarat = '';
                                @endphp
                                @foreach ($cek_persyaratan as $key => $v)
                                    @if ($namasyarat != $v->s_namapersyaratan)
                                        <div class="card">
                                            <div class="card-body">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <span>{{ $no }}. {{ $v->s_namapersyaratan }}</span>
                                                    </div>
                                                    @php
                                                    $namasyarat = $v->s_namapersyaratan;
                                                    $no++;
                                                    $nomer = 1;
                                                    @endphp

                                    @endif

                                    @if (!empty($v->nama_file))
                                        <div class="col-md-4">
                                            <a style="display:none;"
                                                data-ng-href="{{ asset($v->letak_file . $v->nama_file) }}"
                                                title="{{ $v->nama_file }}" download="{{ $v->nama_file }}" data-gallery=""
                                                href="{{ asset($v->letak_file . $v->nama_file) }}"><img
                                                    data-ng-src="{{ asset($v->letak_file . $v->nama_file) }}" alt=""
                                                    src="{{ asset($v->letak_file . $v->nama_file) }}"></a>
                                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a data-ng-switch-when="true"
                                                data-ng-href="{{ asset($v->letak_file . $v->nama_file) }}"
                                                title="{{ $v->nama_file }}" download="{{ $v->nama_file }}" data-gallery=""
                                                class="ng-binding ng-scope"
                                                href="{{ asset($v->letak_file . $v->nama_file) }}">{{ $v->nama_file }}</a>
                                        </div>
                                    @else
                                        <div class="col-md-4">
                                            <span class="badge badge-danger">Durung di upload.</span>
                                        </div>
                                    @endif
                            </div>
                        </div>
                    </div>
                    @php $nomer++; @endphp
    @endforeach
    </div>
    </div>
    </div>
    </div>
    <div class="col-md-12">
        <h5>Kolom Validasi Berkas</h5>
        <div class="card card-outline card-primary">
            <div class="card-body">
                <div class="row">
                    <label class="col-sm-4 ">STATUS VALIDASI BERKAS<span style="color: red">*</span></label>
                    <div class="col-md-8">
                        <select name="s_id_status_berkas" id="s_id_status_berkas"
                            class="selectpicker form-control bs-select-hidden" data-live-search="1" required="required"
                            disabled="true">
                            <option value="">Silahkan Pilih</option>
                            @foreach ($data_statusberkas as $a)
                                <option value="{{ $a->s_id_status_berkas }}"
                                    {{ $dataspt->s_id_status_berkas == $a->s_id_status_berkas ? 'selected' : '' }}>
                                    {{ $a->s_nama_status_berkas }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-4 ">Keterangan</label>
                    <div class="col-md-8">
                        <textarea name="t_keterangan_berkas" id="t_keterangan_berkas" class="form-control"
                            readonly>{{ $dataspt->t_keterangan_berkas }}</textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <h5>Kolom Validasi Kabid</h5>
        <div class="card card-outline card-primary">
            <div class="card-body">
                <div class="row">
                    <label class="col-sm-4 ">Status Validasi Kabid<span style="color: red">*</span></label>
                    <div class="col-md-8">

                        <input type="hidden" name="t_id_validasi_berkas" id="t_id_validasi_berkas"
                            value="{{ $dataspt->t_id_validasi_berkas }}">
                        <input type="hidden" name="t_id_validasi_kabid" id="t_id_validasi_kabid"
                            value="{{ $dataspt->t_id_validasi_kabid }}">

                        <select name="s_id_status_kabid" id="s_id_status_kabid"
                            class="selectpicker form-control bs-select-hidden" data-live-search="1" disabled="true"
                            required="required">
                            <option value="">Silahkan Pilih</option>
                            @foreach ($data_statuskabid as $a)
                                <option value="{{ $a->s_id_status_kabid }}"
                                    {{ $dataspt->s_id_status_kabid == $a->s_id_status_kabid ? 'selected' : '' }}>
                                    {{ $a->s_nama_status_kabid }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="row mt-2">
                    <label class="col-sm-4">Keterangan</label>
                    <div class="col-md-4">
                        <textarea name="t_keterangan_kabid" id="t_keterangan_kabid" disabled="true"
                            class="form-control">{{ $dataspt->t_keterangan_kabid }}</textarea>
                    </div>
                </div>




            </div>


            <div class="col-md-12">
                <h5>Hasil Pemeriksaan BPHTB</h5>
                <div class="card card-outline card-primary">
                    <div class="card-body">
                        <div class="col-sm-12 row mt-4">
                            <label class="col-sm-2 control-label">No Pemeriksaan</label>
                            <div class="col-sm-4">
                                <input name="t_noperiksa" id="t_noperiksa" class="form-control" required="true"
                                    value="{{ $dataspt->t_noperiksa }}" readonly="true" type="text">
                            </div>
                        </div>
                        <div class="col-sm-12 row mt-4">
                            <label class="col-sm-2 control-label">Keterangan</label>
                            <div class="col-sm-4">
                                <textarea name="t_keterangan" id="t_keterangan" class="form-control" rows="8"
                                    readonly="true">{{ $dataspt->t_keterangan }}</textarea>
                            </div>
                        </div>
                        <div class="col-sm-12 row mt-2">
                            <label class="col-sm-2 control-label">Pemeriksa 1</label>
                            <div class="col-sm-4">
                                <select name="t_idpejabat1" id="t_idpejabat1"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1" disabled="true">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach ($data_pejabat as $dpej)
                                        <option value="{{ $dpej->s_idpejabat }}"
                                            {{ $dataspt->t_idpejabat1 == $dpej->s_idpejabat ? 'selected' : '' }}>
                                            {{ $dpej->s_namapejabat }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 row mt-2">
                            <label class="col-sm-2 control-label">Pemeriksa 2</label>
                            <div class="col-sm-4">
                                <select name="t_idpejabat2" id="t_idpejabat2"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1" disabled="true">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach ($data_pejabat as $dpej1)
                                        <option value="{{ $dpej1->s_idpejabat }}"
                                            {{ $dataspt->t_idpejabat2 == $dpej1->s_idpejabat ? 'selected' : '' }}>
                                            {{ $dpej1->s_namapejabat }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 row mt-2">
                            <label class="col-sm-2 control-label">Pemeriksa 3</label>
                            <div class="col-sm-4">
                                <select name="t_idpejabat3" id="t_idpejabat3"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1" disabled="true">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach ($data_pejabat as $dpej2)
                                        <option {{ $dataspt->t_idpejabat3 == $dpej2->s_idpejabat ? 'selected' : '' }}
                                            value="{{ $dpej2->s_idpejabat }}">
                                            {{ $dpej2->s_namapejabat }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12 row mt-2">
                            <label class="col-sm-2 control-label">Pemeriksa 3</label>
                            <div class="col-sm-4">
                                <select name="t_idpejabat4" id="t_idpejabat4"
                                    class="selectpicker form-control bs-select-hidden" data-live-search="1" disabled="true">
                                    <option value="">Silahkan Pilih</option>
                                    @foreach ($data_pejabat as $dpej3)
                                        <option {{ $dataspt->t_idpejabat4 == $dpej3->s_idpejabat ? 'selected' : '' }}
                                            value="{{ $dpej3->s_idpejabat }}">
                                            {{ $dpej3->s_namapejabat }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                Luas Tanah/Bumi
                            </div>
                            <div class="col-sm-2">
                                : {{ $dataspt->t_luastanah_pemeriksa }} m<sup>2</sup>
                            </div>
                            <div class="col-sm-2">
                                NJOP/m<sup>2</sup> Tanah/Bumi
                            </div>
                            <div class="col-sm-2">
                                : {{ number_format($dataspt->t_njoptanah_pemeriksa, 0, ',', '.') }}
                            </div>
                            <div class="col-sm-2">
                                NJOP Tanah/Bumi
                            </div>
                            <div class="col-sm-2">
                                : {{ number_format($dataspt->t_totalnjoptanah_pemeriksa, 0, ',', '.') }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                Luas Bangunan
                            </div>
                            <div class="col-sm-2">
                                : {{ $dataspt->t_luasbangunan_pemeriksa }} m<sup>2</sup>
                            </div>
                            <div class="col-sm-2">
                                NJOP/m<sup>2</sup> Bangunan
                            </div>
                            <div class="col-sm-2">
                                : {{ number_format($dataspt->t_njopbangunan_pemeriksa, 0, ',', '.') }}
                            </div>
                            <div class="col-sm-2">
                                NJOP Bangunan
                            </div>
                            <div class="col-sm-2">
                                : {{ number_format($dataspt->t_totalnjopbangunan_pemeriksa, 0, ',', '.') }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                NJOP PBB
                            </div>
                            <div class="col-sm-2">
                                : {{ number_format($dataspt->t_grandtotalnjop_pemeriksa, 0, ',', '.') }}
                            </div>
                            <div class="col-sm-2">
                                Harga Transaksi
                            </div>
                            <div class="col-sm-2">
                                : {{ number_format($dataspt->t_nilaitransaksispt_pemeriksa, 0, ',', '.') }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                NPOP
                            </div>
                            <div class="col-sm-10">
                                : {{ number_format($dataspt->t_npop_bphtb_pemeriksa, 0, ',', '.') }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                NPOPTKP
                            </div>
                            <div class="col-sm-2">
                                : <u>{{ number_format($dataspt->t_npoptkp_bphtb_pemeriksa, 0, ',', '.') }}
                                    &nbsp;&nbsp;&nbsp;&nbsp;- </u>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                NPOPKP
                            </div>
                            <div class="col-sm-10">
                                : {{ number_format($dataspt->t_npopkp_bphtb_pemeriksa, 0, ',', '.') }}
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-2">
                                Tarif (%)
                            </div>
                            <div class="col-sm-10">
                                :
                                <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ number_format($dataspt->t_persenbphtb, 0, ',', '.') }}%&nbsp;&nbsp;&nbsp;&nbsp;x</u>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-2">
                                Jumlah Pajak
                            </div>
                            <div class="col-sm-2">
                                : <b
                                    style="color: red; font-size: 17px;">{{ number_format($dataspt->t_nilaibphtb_pemeriksa, 0, ',', '.') }}</b>
                            </div>
                        </div>

                    </div>

                </div>
            </div>

            <div class="card-footer">
                <a href="{{ url('pemeriksaan') }}" class="btn btn-outline-primary prevBtn pull-left" type="button"><i
                        class="fa fa-refresh"></i> KEMBALI </a>
                <!-- <a href="{{ url('pemeriksaan') }}/{{ $dataspt->t_idspt }}/tahapkedua" class="btn btn-primary prevBtn pull-right" type="button"><i class="fa fa-save"></i> SIMPAN </a> -->

            </div>
        </div>
    </div>
    </form>
    </div>

@else
    <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">LIHAT DATA</h3>
        </div>
        <div class="col-md-12">
            <center><b>DATA INI TIDAK DI PERIKSA</b></center>
        </div>
        <div class="card-footer ">
            <a href="{{ url('pemeriksaan') }}" class="btn btn-warning prevBtn pull-left" type="button"><i
                    class="fa fa-refresh"></i> KEMBALI </a>

        </div>
    </div>
    @endif

    <div class="modal fade" id="loadingsimpanvalidasi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
        style="background:transparent;">
        <!--data-backdrop="static" data-keyboard="false" -->
        <center><img style="background:transparent;" src='{{ asset('upload/loading77.gif') }}'></center>
    </div>

    <div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls" data-filter=":even">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <!-- <a class="play-pause"></a>
                <ol class="indicator"></ol> -->
    </div>

@endsection


@push('scriptsbawah')
    <script
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly"
        defer></script>
    <script type="text/javascript">
        $(function() {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-Token': '{{ csrf_token() }}'
                }
            });
        });

        function initMap() {
            var latnya = {{ empty($dataspt->s_latitude) ? -7.423844 : $dataspt->s_latitude }};
            var longnya = {{ empty($dataspt->s_longitude) ? 109.231146 : $dataspt->s_longitude }};

            const map = new google.maps.Map(document.getElementById("map"), {
                zoom: 13,
                center: {
                    lat: latnya,
                    lng: longnya
                },
            });

            marker = new google.maps.Marker({
                map,
                draggable: true,
                animation: google.maps.Animation.DROP,
                position: {
                    lat: latnya,
                    lng: longnya
                },
            });

            marker.addListener("click", toggleBounce);

            google.maps.event.addListener(marker, 'dragend', function(marker) {
                var latLng = marker.latLng;
            });

        }

        function toggleBounce() {
            if (marker.getAnimation() !== null) {
                marker.setAnimation(null);
            } else {
                marker.setAnimation(google.maps.Animation.BOUNCE);
            }
        }

        $('#pendaftaranbutton').click(function() {
            var myForm = document.getElementById('formtambah');
            myForm.onsubmit = function() {
                var allInputs = myForm.getElementsByTagName('input');
                var input, i;

                for (i = 0; input = allInputs[i]; i++) {
                    if (input.getAttribute('name') && !input.value) {
                        //input.setAttribute('name', '');

                    } else {
                        jQuery('#loadingsimpanvalidasi').modal('show');
                    }
                }
            };
        });

    </script>

    <script src="{{ asset('internet/js/jquery.blueimp-gallery.min.js') }}"></script>
@endpush
