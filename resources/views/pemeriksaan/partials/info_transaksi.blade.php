<div class="card card-outline card-primary">
    <div class="card-body">
        <div class="row p-0">
            <div class="col-md-2">
                <div class="text-left">Tanggal Daftar</div>
            </div>
            <div class="col-md-3">
                <div class="text-left">
                    : <b class="text-primary">{{ Carbon\Carbon::parse($dataspt->t_tgldaftar_spt)->format('d-m-Y') }}</b>
                </div>
            </div>
        </div>
        <div class="row p-0">
            <div class="col-md-2">
                <div class="text-left">Jenis Transaksi</div>
            </div>
            <div class="col-md-3">
                <div class="text-left">: {{ $dataspt->s_namajenistransaksi }}</div>
            </div>

            <div class="col-md-2">
                <div class="text-left">Notaris</div>
            </div>
            <div class="col-md-3">
                <div class="text-left">: {{ $dataspt->s_namanotaris }}</div>
            </div>
        </div>
    </div>
</div>
