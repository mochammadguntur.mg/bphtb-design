<div class="card card-outline card-primary">
    <div class="card-body">
        <div class="row">
            <div class="col-sm-2">
                Luas Tanah/Bumi
            </div>
            <div class="col-sm-2">
                : {{ $dataspt->t_luastanah }} m<sup>2</sup>
            </div>
            <div class="col-sm-2">
                NJOP/m<sup>2</sup> Tanah/Bumi
            </div>
            <div class="col-sm-2">
                : {{ number_format($dataspt->t_njoptanah, 0, ',', '.') }}
            </div>
            <div class="col-sm-2">
                NJOP Tanah/Bumi
            </div>
            <div class="col-sm-2">
                : {{ number_format($dataspt->t_totalnjoptanah, 0, ',', '.') }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                Luas Bangunan
            </div>
            <div class="col-sm-2">
                : {{ $dataspt->t_luasbangunan }} m<sup>2</sup>
            </div>
            <div class="col-sm-2">
                NJOP/m<sup>2</sup> Bangunan
            </div>
            <div class="col-sm-2">
                : {{ number_format($dataspt->t_njopbangunan, 0, ',', '.') }}
            </div>
            <div class="col-sm-2">
                NJOP Bangunan
            </div>
            <div class="col-sm-2">
                : {{ number_format($dataspt->t_totalnjopbangunan, 0, ',', '.') }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                NJOP PBB
            </div>
            <div class="col-sm-2">
                : {{ number_format($dataspt->t_grandtotalnjop, 0, ',', '.') }}
            </div>
            <div class="col-sm-2">
                Harga Transaksi
            </div>
            <div class="col-sm-2">
                : {{ number_format($dataspt->t_nilaitransaksispt, 0, ',', '.') }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                NPOP
            </div>
            <div class="col-sm-10">
                : {{ number_format($dataspt->t_npop_bphtb, 0, ',', '.') }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                NPOPTKP
            </div>
            <div class="col-sm-2">
                : <u>{{ number_format($dataspt->t_npoptkp_bphtb, 0, ',', '.') }}
                    &nbsp;&nbsp;&nbsp;&nbsp;-</u>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                NPOPKP
            </div>
            <div class="col-sm-10">
                : {{ number_format($dataspt->t_npopkp_bphtb, 0, ',', '.') }}
            </div>
        </div>
        <div class="row">
            <div class="col-sm-2">
                Tarif (%)
            </div>
            <div class="col-sm-10">
                :
                <u>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;{{ number_format($dataspt->t_persenbphtb, 0, ',', '.') }}%&nbsp;&nbsp;&nbsp;&nbsp;x</u>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-2">
                Jumlah Pajak
            </div>
            <div class="col-sm-2">
                : <b style="color: red; font-size: 17px;">{{ number_format($dataspt->t_nilai_bphtb_fix, 0, ',', '.') }}</b>
            </div>
        </div>

    </div>

</div>
