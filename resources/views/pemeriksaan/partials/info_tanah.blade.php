<div class="card card-outline card-primary">
    <div class="card-body">
        <div class="row">
            <div class="col-md-6">
                <div class="row">
                    <div class="col-sm-3">
                        NOP
                    </div>
                    <div class="col-sm-8 p-0">
                        : <b class="text-primary">{{ $dataspt->t_nop_sppt }}/{{ $dataspt->t_tahun_sppt }}</b>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        Nama SPPT
                    </div>
                    <div class="col-sm-8 p-0">
                        : {{ $dataspt->t_nama_sppt }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        Letak Bangunan
                    </div>
                    <div class="col-sm-9 p-0">
                        : {{ $dataspt->t_jalan_sppt }}, RT./RW. {{ $dataspt->t_rt_sppt }}/
                        {{ $dataspt->t_rw_sppt }}, Kelurahan {{ $dataspt->t_kelurahan_sppt }},
                        Kecamatan {{ $dataspt->t_kecamatan_sppt }}, Kabupaten
                        {{ $dataspt->t_kabkota_sppt }}
                    </div>
                </div>
                <div class="mt-2">
                    <div class="col-sm-12 p-0">
                        <div class="row">
                            <div class="col-sm-3">Luas Tanah</div>
                            <div class="col-sm-3 p-0">: {{ $dataspt->t_luastanah }} m<sup>2</sup></div>
                            <div class="col-sm-3 p-0">Luas Bangunan</div>
                            <div class="col-sm-3 p-0">: {{ $dataspt->t_luasbangunan ?: '0' }}
                                m<sup>2</sup></div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Nilai Pasar</div>
                            <div class="col-sm-3 p-0">: {{ number_format($dataspt->t_njoptanah, 0, ',', '.') }}</div>
                            <div class="col-sm-3 p-0">Nilai Pasar</div>
                            <div class="col-sm-3 p-0">: {{ number_format($dataspt->t_njopbangunan, 0, ',', '.') }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Total</div>
                            <div class="col-sm-3 p-0">: {{ number_format($dataspt->t_totalnjoptanah, 0, ',', '.') }}
                            </div>
                            <div class="col-sm-3 p-0">Total</div>
                            <div class="col-sm-3 p-0">: {{ number_format($dataspt->t_totalnjopbangunan, 0, ',', '.') }}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="mt-2">
                    <div class="col-sm-12 p-0">
                        <div class="row">
                            <div class="col-sm-3">Total NJOP PBB</div>
                            <div class="col-sm-3 p-0">: {{ number_format($dataspt->t_grandtotalnjop, 0, ',', '.') }}
                            </div>
                        </div>

                    </div>
                </div>
                <div class="mt-2">
                    <div class="col-sm-12 p-0">
                        <div class="row">
                            <div class="col-sm-3">Jenis Tanah</div>
                            <div class="col-sm-3 p-0">: Tanah Kosong</div>

                            <div class="col-sm-3 p-0">Status Kepemilikan</div>
                            <div class="col-sm-3 p-0">: {{ $dataspt->s_namahaktanah }}</div>

                        </div>
                        <div class="row">
                            <div class="col-sm-3">Jenis Dokumen</div>
                            <div class="col-sm-3 p-0">: {{ $dataspt->s_namadoktanah }}</div>
                            <div class="col-sm-3 p-0">No Dokumen</div>
                            <div class="col-sm-3 p-0">: {{ $dataspt->t_nosertifikathaktanah }}</div>
                        </div>
                        <div class="row">
                            <div class="col-sm-3">Tanggal Dokumen</div>
                            <div class="col-sm-3 p-0">: {{ date('d-m-Y', strtotime($dataspt->t_tgldok_tanah)) }}</div>

                        </div>
                    </div>
                </div>
                <div class="mt-2">
                    <label for="">Photo</label>
                    <div>
                        @php
                        $noobjek = 1;
                        @endphp

                        @if (count($data_fotoobjek) > 0)
                            @foreach ($data_fotoobjek as $key => $vobj)

                                @if (!empty($vobj->nama_file))
                                    <a style="display:none;"
                                        data-ng-href="{{ asset($vobj->letak_file . $vobj->nama_file) }}"
                                        title="{{ $vobj->nama_file }}" download="{{ $vobj->nama_file }}" data-gallery=""
                                        href="{{ asset($vobj->letak_file . $vobj->nama_file) }}">
                                        <img data-ng-src="{{ asset($vobj->letak_file . $vobj->nama_file) }}" alt=""
                                            src="{{ asset($vobj->letak_file . $vobj->nama_file) }}">
                                    </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <a data-ng-switch-when="true"
                                        data-ng-href="{{ asset($vobj->letak_file . $vobj->nama_file) }}"
                                        title="{{ $vobj->nama_file }}" download="{{ $vobj->nama_file }}" data-gallery=""
                                        class="ng-binding ng-scope"
                                        href="{{ asset($vobj->letak_file . $vobj->nama_file) }}">
                                        {{ $vobj->nama_file }}
                                    </a>
                                @endif

                                @php $noobjek++; @endphp
                            @endforeach
                        @else
                            <center>TIDAK ADA DATA</center>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="">
                    <label for="">Maps</label>
                    <div id="map"></div>
                </div>
            </div>
        </div>
    </div>
</div>
