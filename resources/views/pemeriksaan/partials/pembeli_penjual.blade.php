<div class="row">
    <div class="col-md-6">
        <h5>Pihak Pertama (Penerima Hak)</h5>
        <div class="card card-outline card-primary">
            <div class="card-body">
                @csrf
                <u class="text-primary" style="font-size: 14px;"><strong>PRIBADI</strong></u>
                <div class="row mt-2">
                    <div class="col-sm-3">
                        NIK
                    </div>
                    <div class="col-sm-6">
                        : {{ $dataspt->t_nik_pembeli }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        NPWP
                    </div>
                    <div class="col-sm-6">
                        : {{ $dataspt->t_npwp_pembeli }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama </div>
                    <div class="col-sm-6">: {{ $dataspt->t_nama_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat </div>
                    <div class="col-sm-8">: {{ $dataspt->t_jalan_pembeli }} Rt./Rw. {{ $dataspt->t_rt_pembeli }}/{{ $dataspt->t_rw_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-6">
                        : Kelurahan {{ $dataspt->t_namakelurahan_pembeli }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-6">
                        : Kecamatan {{ $dataspt->t_namakecamatan_pembeli }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-6">
                        : Kab/Kota {{ $dataspt->t_kabkota_pembeli }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        Kode Pos
                    </div>
                    <div class="col-sm-6">
                        : {{ $dataspt->t_kodepos_pembeli }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        No.Telp
                    </div>
                    <div class="col-sm-6">
                        : {{ $dataspt->t_nohp_pembeli }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        Email
                    </div>
                    <div class="col-sm-6">
                        : {{ $dataspt->t_email_pembeli }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($dataspt->t_idbidang_usaha == 2)

        <div class="col-md-6">
            <h5>Penanggung Jawab (Badan)</h5>
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <u class="text-primary" style="font-size: 14px;"><strong>BADAN</strong></u>
                    <div class="row mt-2">
                        <div class="col-sm-3">
                            NIK
                        </div>
                        <div class="col-sm-6">
                            : {{ $dataspt->t_b_nik_pngjwb_pembeli }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            NPWP
                        </div>
                        <div class="col-sm-6">
                            : {{ $dataspt->t_b_npwp_pngjwb_pembeli }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Nama </div>
                        <div class="col-sm-6">: {{ $dataspt->t_b_nama_pngjwb_pembeli }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Alamat </div>
                        <div class="col-sm-8">: {{ $dataspt->t_b_jalan_pngjwb_pembeli }} Rt./Rw. {{ $dataspt->t_b_rt_pngjwb_pembeli }} / {{ $dataspt->t_b_rw_pngjwb_pembeli }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            : Kelurahan {{ $dataspt->t_b_namakel_pngjwb_pembeli }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            : Kecamatan {{ $dataspt->t_b_namakec_pngjwb_pembeli }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            : Kab/Kota {{ $dataspt->t_b_kabkota_pngjwb_pembeli }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            Kode Pos
                        </div>
                        <div class="col-sm-6">
                            : {{ $dataspt->t_b_kodepos_pngjwb_pembeli }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            No.Telp
                        </div>
                        <div class="col-sm-6">
                            : {{ $dataspt->t_b_nohp_pngjwb_pembeli }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            Email
                        </div>
                        <div class="col-sm-6">
                            : {{ $dataspt->t_b_email_pngjwb_pembeli }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
    <div class="col-md-6">
        <h5>Pihak Kedua (Pelepas Hak)</h5>
        <div class="card card-outline card-primary">
            <div class="card-body">
                <u class="text-primary" style="font-size: 14px;"><strong>PRIBADI</strong></u>
                <div class="row mt-2">
                    <div class="col-sm-3">
                        NIK
                    </div>
                    <div class="col-sm-6">
                        : {{ $dataspt->t_nik_penjual }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        NPWP
                    </div>
                    <div class="col-sm-6">
                        : {{ $dataspt->t_npwp_penjual }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama </div>
                    <div class="col-sm-6">: {{ $dataspt->t_nama_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat </div>
                    <div class="col-sm-8">: {{ $dataspt->t_jalan_penjual }} Rt./Rw. {{ $dataspt->t_rt_penjual }}/{{ $dataspt->t_rw_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-8">
                        : Kelurahan {{ $dataspt->t_namakel_penjual }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-8">
                        : Kecamatan {{ $dataspt->t_namakec_penjual }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                    </div>
                    <div class="col-sm-8">
                        : Kab/Kota {{ $dataspt->t_kabkota_penjual }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        Kode Pos
                    </div>
                    <div class="col-sm-6">
                        : {{ $dataspt->t_kodepos_penjual }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        No.Telp
                    </div>
                    <div class="col-sm-6">
                        : {{ $dataspt->t_nohp_penjual }}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-3">
                        Email
                    </div>
                    <div class="col-sm-6">
                        : {{ $dataspt->t_email_penjual }}
                    </div>
                </div>
            </div>
        </div>
    </div>
    @if ($dataspt->t_idbidang_usaha == 2)

        <div class="col-md-6">
            <h5>Penanggung Jawab (Badan)</h5>
            <div class="card card-outline card-primary">
                <div class="card-body">
                    <u class="text-primary" style="font-size: 14px;"><strong>BADAN</strong></u>
                    <div class="row mt-2">
                        <div class="col-sm-3">
                            NIK
                        </div>
                        <div class="col-sm-6">
                            : {{ $dataspt->t_b_nik_pngjwb_penjual }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            NPWP
                        </div>
                        <div class="col-sm-6">
                            : {{ $dataspt->t_b_npwp_pngjwb_penjual }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Nama </div>
                        <div class="col-sm-6">: {{ $dataspt->t_b_nama_pngjwb_penjual }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">Alamat </div>
                        <div class="col-sm-8">: {{ $dataspt->t_b_jalan_pngjwb_penjual }} Rt./Rw. {{ $dataspt->t_b_rt_pngjwb_penjual }}/{{ $dataspt->t_b_rw_pngjwb_penjual }}</div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            : Kelurahan {{ $dataspt->t_b_namakel_pngjwb_penjual }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-8">
                            : Kecamatan {{ $dataspt->t_b_namakec_pngjwb_penjual }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                        </div>
                        <div class="col-sm-6">
                            : Kab/Kota {{ $dataspt->t_b_kabkota_pngjwb_penjual }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            Kode Pos
                        </div>
                        <div class="col-sm-6">
                            : {{ $dataspt->t_b_kodepos_pngjwb_penjual }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            No.Telp
                        </div>
                        <div class="col-sm-6">
                            : {{ $dataspt->t_b_nohp_pngjwb_penjual }}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-3">
                            Email
                        </div>
                        <div class="col-sm-6">
                            : {{ $dataspt->t_b_email_pngjwb_penjual }}
                        </div>
                    </div>

                </div>
            </div>
        </div>
    @endif
</div>
