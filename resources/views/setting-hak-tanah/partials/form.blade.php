<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Jenis Hak Tanah</label>
    <div class="col-md-4">
        <input type="text" maxlength="2" name="s_kodehaktanah" class="form-control @error('s_kodehaktanah') is-invalid @enderror" id="s_kodehaktanah" placeholder="Kode Jenis Hak Tanah"
               value="{{ old('s_kodehaktanah') ?? $haktanah->s_kodehaktanah }}" onkeypress="return numberOnly(event)">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kodehaktanah'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_kodehaktanah') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Nama Jenis Hak Tanah</label>
    <div class="col-md-4">
        <input type="text" name="s_namahaktanah" class="form-control  @error('s_namahaktanah') is-invalid @enderror" id="s_namahaktanah" placeholder="Nama Jenis Hak Tanah"
               value="{{ old('s_namahaktanah') ?? $haktanah->s_namahaktanah }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_namahaktanah'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_namahaktanah') }}
        </div>
        @endif
    </div>
</div>
<a href="/setting-hak-tanah" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $submit ?? 'Update' }}</button>

@push('scripts')
<script>
    function numberOnly(e) {
        var input = (e.which) ? e.which : e.keyCode

        if (input > 31 && (input < 48 || input > 57))
            return false;
        return true;
    }
</script>
@endpush