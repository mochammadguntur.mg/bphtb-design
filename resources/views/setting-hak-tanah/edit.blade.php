@extends('layouts.master')

@section('judulkiri')
    PENGATURAN JENIS HAK TANAH [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="/setting-hak-tanah/{{ $haktanah->s_idhaktanah }}/edit" method="post">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-hak-tanah.partials.form')
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection