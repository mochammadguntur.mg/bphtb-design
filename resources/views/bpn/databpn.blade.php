@extends('layouts.master')

@section('judulkiri')
DATA INTEGRASI BPN
@endsection

@section('content')

    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-info">
                <div class="card-header">
                    <h3 class="card-title">
                        <i class="fas fa-check-circle text-green"></i> : Ada Transaksi BPHTB<br>
                        <i class="fas fa-minus-circle text-red"></i> : Tidak Ada Transaksi BPHTB
                    </h3>
                    <div class="card-tools">
                        <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                            <i class="fas fa-file-excel"></i> Excel
                        </a>
                        <a href="javascript:void(0)" class="btn btn-danger" id="btnPDF">
                            <i class="fas fa-file-pdf"></i> PDF
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                            <thead>
                                <tr class="bg-blue">
                                    <th>No</th>
                                    <th>Tgl Transaksi</th>
                                    <th>Status</th>
                                    <th>No/Tgl Akta</th>
                                    <th>Nama PPAT</th>
                                    <th>NOP</th>
                                    <th>NTPD</th>
                                    <th>N.I.B</th>
                                    <th>NIK</th>
                                    <th>NPWP</th>
                                    <th>Nama WP</th>
                                    <th>Alamat OP</th>
                                    <th>Luas Tanah</th>
                                    <th>Jenis Hak</th>
                                    <th>created_at</th>
                                    <th>Aksi</th>
                                </tr>
                                <tr>
                                    <th>CARI</th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-tgltransaksi"
                                            readonly>
                                    </th>
                                    <th></th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-akta">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-ppat">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-nop">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-ntpd">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-nib">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-nik">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-npwp">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-namawp">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-alamatop">
                                    </th>
                                    <th></th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-jenishak">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-created_at"
                                            readonly>
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center" colspan="15"> Tidak ada data.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix pagination-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modal-map">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header bg-primary">
                    <h6 class="modal-title"><span class="fas fa-map"></span> DATA TITIK KOORDINAT OP</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div id="googleMap"></div>
                    <div id="detailMapOP"></div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                            class="fas fa-times-circle"></span> Tutup</button>
                </div>
            </div>
        </div>
    </div>

@endsection

<style type="text/css">
    #googleMap {
        height: 400px;
        width: 100%;
        border: 1px solid #ccc;
    }

</style>
@push('scripts')
    <script src="{{ asset('/datagrid/datagrid.js') }}"></script>

    <script type="text/javascript">
        $(document).ready(function() {

            $('#filter-tgltransaksi').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#filter-tgltransaksi').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format(
                    'DD-MM-YYYY'));
                search();
            });

            $('#filter-tgltransaksi').on('cancel.datepicker', function(ev, picker) {
                $(this).val('');
                search();
            });

            $('#filter-created_at').daterangepicker({
                autoUpdateInput: false,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
                $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format(
                    'DD-MM-YYYY'));
                search();
            });

            $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
                $(this).val('');
                search();
            });

        });

        var datatables = datagrid({
            url: 'datagrid',
            table: "#datagrid-table",
            columns: [{
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: "text-right"
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
                {
                    class: ""
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: true,
                    name: "t_tgltransaksi"
                },
                {
                    sortable: false
                },
                {
                    sortable: true,
                    name: "t_tglakta"
                },
                {
                    sortable: true,
                    name: "t_namappat"
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
            ],
            action: [{
                name: 'review',
                btnClass: 'btn btn-info btn-sm',
                btnText: '<i class="fas fa-eye"> Review</i>'
            }]
        });

        $("#filter-akta, #filter-ppat, #filter-nop, #filter-ntpd, #filter-nib, #filter-nik, #filter-npwp, #filter-namawp, #filter-alamatop")
            .keyup(function() {
                search();
            });

        $("#filter-tgltransaksi, #filter-created_at").change(function() {
            search();
        });

        function search() {
            datatables.setFilters({
                akta: $("#filter-akta").val(),
                ppat: $("#filter-ppat").val(),
                nop: $("#filter-nop").val(),
                ntpd: $("#filter-ntpd").val(),
                nib: $("#filter-nib").val(),
                nik: $("#filter-nik").val(),
                npwp: $("#filter-npwp").val(),
                namawp: $("#filter-namawp").val(),
                alamatop: $("#filter-alamatop").val(),
                jenishak: $("#filter-jenishak").val(),
                tgltransaksi: $("#filter-tgltransaksi").val(),
                created_at: $("#filter-created_at").val()
            });
            datatables.reload();
        }
        search();

        function showReview(a) {
            $.ajax({
                url: 'detail/{' + a + '}',
                type: 'GET',
                data: {
                    id: a
                }
            }).then(function(data) {
                $("#detailMapOP").html(data.data.html);
                var koordinat = data.data.detail[0];
                initialize(koordinat.t_koordinat_y, koordinat.t_koordinat_x);
            });
            $("#modal-map").modal('show');
        }

        $("#btnExcel, #btnPDF").click(function(a) {
            var akta = $("#filter-akta").val();
            var ppat = $("#filter-ppat").val();
            var nop = $("#filter-nop").val();
            var ntpd = $("#filter-ntpd").val();
            var nib = $("#filter-nib").val();
            var nik = $("#filter-nik").val();
            var npwp = $("#filter-npwp").val();
            var namawp = $("#filter-namawp").val();
            var alamatop = $("#filter-alamatop").val();
            var jenishak = $("#filter-jenishak").val();
            var tgltransaksi = $("#filter-tgltransaksi").val();
            var created_at = $("#filter-created_at").val();
            if (a.target.id == 'btnExcel') {
                window.open('/bpn/exportbpn_xls?tgltransaksi=' + tgltransaksi +
                    '&akta=' + akta +
                    '&ppat=' + ppat +
                    '&nop=' + nop +
                    '&ntpd=' + ntpd +
                    '&nib=' + nib +
                    '&nik=' + nik +
                    '&npwp=' + npwp +
                    '&namawp=' + namawp +
                    '&alamatop=' + alamatop +
                    '&jenishak=' + jenishak +
                    '&created_at=' + created_at);
            } else {
                window.open('/bpn/exportbpn_pdf?tgltransaksi=' + tgltransaksi +
                    '&akta=' + akta +
                    '&ppat=' + ppat +
                    '&nop=' + nop +
                    '&ntpd=' + ntpd +
                    '&nib=' + nib +
                    '&nik=' + nik +
                    '&npwp=' + npwp +
                    '&namawp=' + namawp +
                    '&alamatop=' + alamatop +
                    '&jenishak=' + jenishak +
                    '&created_at=' + created_at);
            }
        });

    </script>
    <script>
        function initialize(y, x) {
            var propertiPeta = {
                center: new google.maps.LatLng(y, x),
                zoom: 15,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(y, x),
                map: peta,
                animation: google.maps.Animation.BOUNCE
            });
        }
        // event jendela di-load
        google.maps.event.addDomListener(window, 'load', initialize);

    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIcfDt4yUMeAiGKDTNyIufrBMuif-efms&callback=initMap">
    </script>
@endpush
