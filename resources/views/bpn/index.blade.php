@extends('layouts.master')

@section('judulkiri')
CEK DATA INTEGRASI BPN
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <form action="/bpn/store" method="POST" onsubmit="return confirm('Apakah anda akan menyimpan data ini?');">
            <div class="card-body form-horizontal">
                @csrf
                    @include('bpn.partials.form')
            </div>
            <div class="card-body">

                <button type="submit" id="btnSimpan" class="btn btn-success btn-flat"><i class="fas fa-save"></i> SIMPAN DATA BPN</button>
                <div class="table-responsive" id="dataRow">
                    <table class="table table-bordered table-hover table-striped">
                        <thead>
                            <tr class="bg-blue">
                                <th>No</th>
                                <th>No Akta</th>
                                <th>Tgl Akta</th>
                                <th>Nama PPAT</th>
                                <th>NOP</th>
                                <th>NTPD</th>
                                <th>N.I.B</th>
                                <th>NIK</th>
                                <th>NPWP</th>
                                <th>Nama WP</th>
                                <th>Kelurahan OP</th>
                                <th>Kecamatan OP</th>
                                <th>Kota OP</th>
                                <th>Luas Tanah</th>
                                <th>Jenis Hak</th>
                                <th>Koordinat-X</th>
                                <th>Koordinat-Y</th>
                            </tr>
                        </thead>
                        <tbody id="gridBody"></tbody>
                    </table>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-map">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h6 class="modal-title"><span class="fas fa-map"></span> CEK DATA BPN BERDASARKAN MARKER <span id="resMaps"></span></h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-sm-2">Tanggal Transaksi</label>
                    <div class="col-sm-2">
                        <input type="text" class="form-control datepicker-date" id="t_tgltransaksi_map" name="t_tgltransaksi_map" value="{{ date('d-m-Y') }}" readonly>
                    </div>
                    <div class="col-sm-2">
                        <span class="input-group-append">
                            <button type="button" id="btnCariMap" class="btn btn-info btn-flat"><i class="fas fa-search"></i> CARI</button>
                        </span>
                    </div>
                    <div class="col-sm-6 text-blue" id="rescount"></div>
                </div>
                <div id="googleMap"></div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="modal-overlay" data-backdrop="static" data-keyboard="false" tabindex="-1">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="col-12">
                <span class="spinner-grow text-muted"></span>
                <span class="spinner-grow text-primary"></span>
                <span class="spinner-grow text-success"></span>
                <span class="spinner-grow text-info"></span>
                <span class="spinner-grow text-warning"></span>
                <span class="spinner-grow text-danger"></span>
                <span class="spinner-grow text-secondary"></span>
                <span class="spinner-grow text-dark"></span>
            </div>
        </div>
    </div>
</div>
@endsection

<style>
    .bd-example-modal-lg .modal-dialog{
        display: table;
        position: relative;
        margin: 0 auto;
        top: calc(50% - 24px);
    }
    .bd-example-modal-lg .modal-dialog .modal-content{
        background-color: transparent;
        border: none;
        box-shadow: none;
    }
    #googleMap {
        height: 400px;
        width: 100%;
		border:1px solid #ccc;
    }
</style>
@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIcfDt4yUMeAiGKDTNyIufrBMuif-efms&callback=initMap"></script>
<script type="text/javascript">
    $(document).ready(function() {

        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif

        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif


        $('#btnExcel').hide();
        $('#btnPDF').hide();
        $('#btnSimpan').hide();
        $('#dataRow').hide();
    });

    $("#btnCari").click(function() {
        $('#modal-overlay').modal('show');
        var tgltransaksi = $('#t_tgltransaksi').val();
        $.ajax({
            url: 'bpn/caridata/{' + tgltransaksi + '}',
            type: 'GET',
            data: {
                tgltransaksi: tgltransaksi
            }
        }).then(function(data) {
            $('#modal-overlay').modal('hide');
            if(data != ''){
                $('#gridBody').html(data);
                $('#dataRow').show();
                $('#btnExcel').show();
                $('#btnPDF').show();
                $('#btnSimpan').show();
            }else{
                toastr.error('Tidak ada data yang ditampilkan!')
                $('#dataRow').hide();
                $('#btnExcel').hide();
                $('#btnPDF').hide();
                $('#btnSimpan').hide();
            }
        });
    });

    $("#btnCaribyMap").click(function() {
        $('#modal-map').modal('show');
    });

    $("#btnCariMap").click(function() {
        $('#modal-overlay').modal('show');
        var tgltransaksi = $('#t_tgltransaksi_map').val();
        $.ajax({
            url: 'bpn/map/{' + tgltransaksi + '}',
            type: 'GET',
            data: {
                tgltransaksi: tgltransaksi
            }
        }).then(function(data) {
            $('#modal-overlay').modal('hide');
            if(data.count > 0){
                $('#rescount').html('Jumlah Data yang tampilkan <b>'+data.count+'</b> Item');
            }else{
                $('#rescount').html('');
                toastr.warning('Tidak ada data yang ditampilkan!')
            }
            initialize(data.data);
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        var t_tgltransaksi = $("#t_tgltransaksi").val();
        if(a.target.id == 'btnExcel'){
            window.open('/bpn/export_xls?t_tgltransaksi=' + t_tgltransaksi);
        }else{
            window.open('/bpn/export_pdf?t_tgltransaksi=' + t_tgltransaksi);
        }
    });

    function initialize(data) {

        var locations = data;

        var propertiPeta = {
            center:new google.maps.LatLng(-3.6446123,102.5915404),
            zoom:12,
            mapTypeId:google.maps.MapTypeId.ROADMAP
        };

        var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);
        var infowindow = new google.maps.InfoWindow();
        var marker, i;

        var i = 1;
        locations.forEach(element => {
            var marker=new google.maps.Marker({
                position: new google.maps.LatLng(element.KOORDINAT_Y, element.KOORDINAT_X),
                map: peta,
                animation: google.maps.Animation.DROP
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    var datawp = 'NOP : ' + element.NOP + '<br>NAMA WP : ' + element.NAMA_WP + '<br>ALAMAT OP : Kel/Desa. ' + element.KELURAHAN_OP + ', Kec. ' + element.KELURAHAN_OP;
                    infowindow.setContent(datawp);
                    infowindow.open(peta, marker);
                }
            })(marker, i));
            i++;
        });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endpush
