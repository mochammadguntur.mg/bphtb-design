<div class="form-group row">
    <label class="col-sm-2">Tanggal Transaksi</label>
    <div class="col-sm-2">
        <input type="text" class="form-control datepicker-date" id="t_tgltransaksi" name="t_tgltransaksi" value="{{ date('d-m-Y') }}" readonly>
    </div>
    <div class="col-sm-6">
        <span class="input-group-append">
            <button type="button" id="btnCari" class="btn btn-info btn-flat"><i class="fas fa-search"></i> CARI</button>
            <button type="button" id="btnCaribyMap" class="btn btn-warning btn-flat"><i class="fas fa-map"></i> CARI By MAP</button>
            <a href="#" id="btnPDF" class="btn btn-danger btn-flat"><i class="fas fa-file-pdf"></i> PDF</a>
            <a href="#" id="btnExcel" class="btn btn-success btn-flat"><i class="fas fa-file-excel"></i> EXCEL</a>
        </span>
      </div>
</div>
