@extends('layouts.master')

@section('judulkiri')
RUBAH PASSWORD API BPN
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-body form-horizontal">
                <form action="/bpn/{{ $bpnapi[0]->s_idusers_api }}/change" method="POST">
                    @method('patch')
                    @csrf
                <div class="form-group row">
                    <label class="col-sm-2">Username</label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="s_username" name="s_username" value="{{ $bpnapi[0]->s_username }}" readonly>
                        @if ($errors->has('s_username'))
                        <span class="text-danger mt-2">
                            {{ $errors->first('s_username') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Password Lama</label>
                    <div class="col-sm-3">
                        <input type="password" class="form-control" id="s_passold" name="s_passold" value="">
                        @if ($errors->has('s_passold'))
                        <span class="text-danger mt-2">
                            {{ $errors->first('s_passold') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Password Baru</label>
                    <div class="col-sm-3">
                        <input type="password" class="form-control" id="s_password" name="s_password" value="">
                        @if ($errors->has('s_password'))
                        <span class="text-danger mt-2">
                            {{ $errors->first('s_password') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <label class="col-sm-2">Ulangi Password Baru</label>
                    <div class="col-sm-3">
                        <input type="password" class="form-control" id="s_password2" name="s_password2" value="">
                        @if ($errors->has('s_password2'))
                        <span class="text-danger mt-2">
                            {{ $errors->first('s_password2') }}
                        </span>
                        @endif
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <span class="input-group-append">
                            <button type="submit" id="btnChange" class="btn btn-warning btn-block"><i class="fas fa-key"></i> Ganti Password API</button>
                        </span>
                      </div>
                </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {

        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif

        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif
    });
</script>
@endpush
