@extends('layouts.master')

@section('judulkiri')
APPROVED KERINGANAN [TAMBAH]
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header bg-primary">
                    <h3 class="card-title">FROM APPROVED KERINGANAN</h3>
                    <div class="card-tools">
                        <a href="{{ url('approved-keringanan/permohonan') }}" class="btn btn-sm btn-warning">
                            <i class="fas fa-backward"></i> Kembali
                        </a>
                    </div>
                </div>
                <form action="/approved-keringanan/store" method="POST" class="form-horizontal">
                    <div class="card-body">

                        @csrf
                        @include('approved-keringanan.partials.view')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title"><i class="fas fa-edit"></i> FORM APPROVED KERINGANAN</h3>
                                    </div>
                                    <div class="card-body">
                                    @include('approved-keringanan.partials.form', ['sumbit'=> 'Simpan'])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>


@endsection

@push('scripts')

<script type="text/javascript">
    $(document).ready(function() {
        $("#btnReject").click(function(){
            $("#t_idstatus_disetujui").val(2);
        })

        $("#btnApprove").click(function(){
            $("#t_idstatus_disetujui").val(1);
        })

    });

    $("#t_persentase_disetujui").keyup(function() {
        if($('#t_persentase_disetujui').val() > 100){
            alert('Persentase tidak boleh lebih 100%');
            $('#t_persentase_disetujui').val(0);
        }
        hitung();
    });

    function hitung() {
        var a = $('#t_persentase_disetujui').val();
        var b = $('#t_jmlh_spt_sebenarnya').val();
        $.ajax({
            url: 'hitung/{' + a + '}/{' + b + '}',
            type: 'GET',
            data: {
                id: a,
                key: b
            }
        }).then(function(data) {
            $('#t_jmlhpotongan_disetujui').val(data.data.potongan);
            $('#t_jmlh_spt_hasilpot').val(data.data.hasil);
        });
    }
</script>
@endpush
