<!DOCTYPE html>
<html lang="en">
<head>
    <style>
        @page{margin: 40px;font-family: sans-serif;font-size: 10pt;size: A4 portrait;}
    </style>
</head>
<body>
@include('kop.kop-surat')

<table class="table" style="width: 100%; margin-top:10px;">
    <tr>
        <th colspan="3" style="font-size: 14pt;"><u>SURAT KEPUTUSAN KERINGANAN</u></th>
    </tr>
    <tr>
        <th colspan="3" style="padding-bottom:10px;">NO SK : {{ $data->t_nosk_keringanan }}</th>
    </tr>
    <tr>
        <td style="width: 25%">Tgl Permohonan</td>
        <td style="width: 5%; text-align:right;">:</td>
        <td>{{ date('d-m-Y', strtotime($data->t_tglpengajuan)) }}</td>
    </tr>
    <tr>
        <td>No Permohonan</td>
        <td style="text-align:right;">:</td>
        <td>{{ $data->t_nokeringanan }}</td>
    </tr>
    <tr>
        <td>No Kohir SPT</td>
        <td style="text-align:right;">:</td>
        <td>{{ $data->t_kohirspt }}</td>
    </tr>
    <tr>
        <td>Periode SPT</td>
        <td style="text-align:right;">:</td>
        <td>{{ $data->t_periodespt }}</td>
    </tr>
    <tr>
        <td>Nama WP</td>
        <td style="text-align:right;">:</td>
        <td>{{ ($data->t_idbidang_usaha == 1) ? $data->t_p_nama_pembeli : $data->t_b_nama_pembeli }}</td>
    </tr>
    <tr>
        <td>NOP</td>
        <td style="text-align:right;">:</td>
        <td>{{ $data->t_nop_sppt }}</td>
    </tr>
    <tr>
        <td style="vertical-align:top;">Keterangan Permohonan</td>
        <td style="text-align:right;vertical-align:top;">:</td>
        <td>{{ $data->t_keterangan_permohoanan }}</td>
    </tr>
    <tr>
        <td>Tgl Persetujuan</td>
        <td style="text-align:right;">:</td>
        <td>{{ date('d-m-Y', strtotime($data->t_tglpersetujuan)) }}</td>
    </tr>
    <tr>
        <td>Status Persetujuan</td>
        <td style="text-align:right;">:</td>
        <td>{{ ($data->t_idstatus_disetujui == 1) ? 'Disetujui':'Ditolak' }}</td>
    </tr>
    <tr>
        <td>Persentase Disetujui</td>
        <td style="text-align:right;">:</td>
        <td>{{ number_format($data->t_persentase_disetujui,0,',','.') }}%</td>
    </tr>
    <tr>
        <td>BPHTB Terhutang</td>
        <td style="text-align:right;">:</td>
        <td>Rp. {{ number_format($data->t_jmlh_spt_sebenarnya,0,',','.') }}</td>
    </tr>
    <tr>
        <td>Jumlah Potongan</td>
        <td style="text-align:right;">:</td>
        <td>Rp. {{ number_format($data->t_jmlhpotongan_disetujui,0,',','.') }}</td>
    </tr>
    <tr>
        <td>BPHTB yg Harus Dibayar</td>
        <td style="text-align:right;">:</td>
        <td>Rp. {{ number_format($data->t_jmlh_spt_hasilpot,0,',','.') }}</td>
    </tr>
</table>


</body>
</html>
