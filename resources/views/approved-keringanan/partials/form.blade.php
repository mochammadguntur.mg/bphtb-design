
<div class="row">
    <div class="col-sm-2">No Permohonan</div>
    <div class="col-sm-2">
        : <strong class="text-blue">{{ $data->t_nokeringanan }}</strong>
        <input type="hidden" id="t_idkeringanan" name="t_idkeringanan" value="{{ old('t_idkeringanan') ?? $data->t_idkeringanan }}">
    </div>
</div>
<div class="row">
    <div class="col-sm-2">Tgl Permohonan</div>
    <div class="col-sm-2">
        : {{ date('d-m-Y', strtotime($data->t_tglpengajuan)) }}
    </div>
</div>
<div class="row">
    <div class="col-sm-2">No Kohir SPT</div>
    <div class="col-sm-2">: <strong class="text-blue">{{ $data->t_kohirspt }}</strong></div>
    <div class="col-sm-2">Periode SPT</div>
    <div class="col-sm-2">: {{ $data->t_periodespt }}</div>
</div>
<div class="form-group row">
    <div class="col-sm-2 col-form-label">Ket. Permohonan</div>
    <div class="col-sm-8">
        <textarea class="form-control" id="t_keterangan_permohonan" name="t_keterangan_permohonan"
            readonly>{{ $data->t_keterangan_permohoanan }}</textarea>
    </div>
</div>
<div class="form-group row">
    <div for="inputEmail3" class="col-sm-2 col-form-label">Tgl Persetujuan <span style="color:red;">*</span></div>
    <div class="col-sm-2">
        <input type="text" class="form-control" id="t_tglpersetujuan" name="t_tglpersetujuan"
            value="{{ date('d-m-Y') }}" readonly="true">
    </div>
    @if ($errors->has('t_tglpersetujuan'))
        <div class="text-danger mt-2">
            {{ $errors->first('t_tglpersetujuan') }}
        </div>
    @endif
</div>

<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">No SK Keringanan <span style="color:red;">*</span></label>
    <div class="col-sm-3">
        <input type="text" class="form-control" id="t_nosk_keringanan" name="t_nosk_keringanan">
        @if ($errors->has('t_nosk_keringanan'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_nosk_keringanan') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Persentase (%) <span
            style="color:red;">*</span></label>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right rupiah" id="t_persentase_disetujui" name="t_persentase_disetujui">
        @if ($errors->has('t_persentase_disetujui'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_persentase_disetujui') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Jmlh Ketetapan (Rp)<span
            style="color:red;">*</span></label>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_jmlh_spt_sebenarnya" name="t_jmlh_spt_sebenarnya"
            value="{{ number_format($data->t_nilai_bphtb_fix,0,',','.') }}" readonly>
        @if ($errors->has('t_jmlh_spt_sebenarnya'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_jmlh_spt_sebenarnya') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Jmlh Potongan (Rp) <span
            style="color:red;">*</span></label>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_jmlhpotongan_disetujui" name="t_jmlhpotongan_disetujui"
            value="{{ old('t_jmlhpotongan_disetujui') ?? $data->t_jmlhpotongan_disetujui }}" readonly>
        @if ($errors->has('t_jmlhpotongan_disetujui'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_jmlhpotongan_disetujui') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Jmlh Disetujui (Rp) <span
            style="color:red;">*</span></label>
    <div class="col-sm-3">
        <input type="text" class="form-control text-right" id="t_jmlh_spt_hasilpot" name="t_jmlh_spt_hasilpot"
            value="{{ old('t_jmlh_spt_hasilpot') ?? $data->t_jmlh_spt_hasilpot }}" readonly>
        @if ($errors->has('t_jmlh_spt_hasilpot'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_jmlh_spt_hasilpot') }}
            </span>
        @endif
    </div>
</div>
<div class="form-group row">
    <label for="inputEmail3" class="col-sm-2 col-form-label">Keterangan Persetujuan <span
            style="color:red;">*</span></label>
    <div class="col-sm-6">
        <input type="hidden" id="t_idstatus_disetujui" name="t_idstatus_disetujui">
        <textarea class="form-control" id="t_keterangan_disetujui"
            name="t_keterangan_disetujui">{{ old('t_keterangan_disetujui') ?? $data->t_keterangan_disetujui }}</textarea>
        @if ($errors->has('t_keterangan_disetujui'))
            <span class="text-danger mt-2">
                {{ $errors->first('t_keterangan_disetujui') }}
            </span>
        @endif
    </div>
</div>

<div class="form-group row">
    <div class="col-sm-2"></div>
    <div class="col-sm-10">
        <button type="button" id="btnApprove" class="btn btn-success" data-toggle="modal" data-target="#modal-save"><i
                class="fas fa-check"></i> Setuju</button>
        <button type="button" id="btnReject" class="btn btn-danger" data-toggle="modal" data-target="#modal-cancel"><i
                class="fas fa-close"></i> Tidak Setuju</button>
    </div>
</div>

<div class="modal fade" id="modal-save">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-success">
                <h6 class="modal-title"><span class="fas fa-send"></span>
                    &nbsp;TERIMA PERMOHONAN PELAYANAN</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda ingin menerima permohonan layanan ini?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span>
                    &nbsp;Tutup
                </button>
                <button type="submit" class="btn btn-success btn-sm"><span class="fas fa-check"></span> &nbsp;IYA
                </button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modal-cancel">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-exclamation"></span>
                    &nbsp;TOLAK PENGAJUAN PERMOHONAN ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menolak pengajuan data permohonan
                    tersebut?</p>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span>
                    &nbsp;Tutup
                </button>
                <button type="submit" class="btn btn-danger btn-sm"><span class="fas fa-exclamation"></span> &nbsp;IYA
                </button>
            </div>
        </div>
    </div>
</div>
