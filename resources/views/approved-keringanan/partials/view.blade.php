
<div class="row">
    <div class="col-md-6">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-user"></i> PENERIMA HAK (PIHAK KEDUA)</h3>
            </div>
            <div class="card-body">
                @if($data->t_idbidang_usaha == 1)
                <div class="row">
                    <div class="col-sm-3 text-blue text-bold"><u>PRIBADI</u></div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NIK</div>
                    <div class="col-sm-9">: {{ $data->t_nik_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NPWP</div>
                    <div class="col-sm-9">: {{ $data->t_npwp_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama</div>
                    <div class="col-sm-9">: {{ $data->t_nama_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat</div>
                    <div class="col-sm-9">: {{ $data->t_jalan_pembeli }} RT/RW {{ $data->t_rt_pembeli .'/'. $data->t_rw_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $data->t_namakelurahan_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $data->t_namakecamatan_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $data->t_kabkota_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Kode Pos</div>
                    <div class="col-sm-9">: {{ $data->t_kodepos_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No.Telp</div>
                    <div class="col-sm-9">: {{ $data->t_nohp_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Email</div>
                    <div class="col-sm-9">: {{ $data->t_email_pembeli }}</div>
                </div>

                @endif
                @if($data->t_idbidang_usaha == 2)
                <div class="row">
                    <div class="col-sm-3 text-blue text-bold"><u>BADAN</u></div>
                </div>
                <div class="row">
                    <div class="col-sm-3">SIUP</div>
                    <div class="col-sm-9">: {{ $data->t_siup_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NIB</div>
                    <div class="col-sm-9">: {{ $data->t_nib_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NPWP</div>
                    <div class="col-sm-9">: {{ $data->t_b_npwp_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama</div>
                    <div class="col-sm-9">: {{ $data->t_b_nama_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat</div>
                    <div class="col-sm-9">: {{ $data->t_b_jalan_pembeli }} RT/RW {{ $data->t_b_rt_pembeli .'/'. $data->t_b_rw_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $data->t_b_namakelurahan_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $data->t_b_namakecamatan_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $data->t_b_kabkota_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Kode Pos</div>
                    <div class="col-sm-9">: {{ $data->t_b_kodepos_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No.Telp</div>
                    <div class="col-sm-9">: {{ $data->t_b_nohp_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Email</div>
                    <div class="col-sm-9">: {{ $data->t_b_email_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3 text-blue text-bold"><u>PENANGGUNG JAWAB</u></div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NIK</div>
                    <div class="col-sm-9">: {{ $data->t_b_nik_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NPWP</div>
                    <div class="col-sm-9">: {{ $data->t_b_npwp_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama</div>
                    <div class="col-sm-9">: {{ $data->t_b_nama_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat</div>
                    <div class="col-sm-9">: {{ $data->t_b_jalan_pngjwb_pembeli }} RT/RW {{ $data->t_b_rt_pngjwb_pembeli .'/'. $data->t_b_rw_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $data->t_b_namakel_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $data->t_b_namakec_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $data->t_b_kabkota_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Kode Pos</div>
                    <div class="col-sm-9">: {{ $data->t_b_kodepos_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No.Telp</div>
                    <div class="col-sm-9">: {{ $data->t_b_nohp_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Email</div>
                    <div class="col-sm-9">: {{ $data->t_b_email_pngjwb_pembeli }}</div>
                </div>

                @endif
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-user"></i> INFROMASI PELEPAS HAK (PIHAK PERTAMA)</h3>
            </div>
            <div class="card-body">
                @if($data->t_idbidang_usaha == 1)
                <div class="row">
                    <div class="col-sm-3 text-blue text-bold"><u>PRIBADI</u></div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NIK</div>
                    <div class="col-sm-9">: {{ $data->t_nik_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NPWP</div>
                    <div class="col-sm-9">: {{ $data->t_npwp_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama</div>
                    <div class="col-sm-9">: {{ $data->t_nama_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat</div>
                    <div class="col-sm-9">: {{ $data->t_jalan_penjual }} RT/RW {{ $data->t_rt_penjual .'/'. $data->t_rw_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $data->t_namakel_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $data->t_namakec_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $data->t_kabkota_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Kode Pos</div>
                    <div class="col-sm-9">: {{ $data->t_kodepos_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No.Telp</div>
                    <div class="col-sm-9">: {{ $data->t_nohp_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Email</div>
                    <div class="col-sm-9">: {{ $data->t_email_penjual }}</div>
                </div>

                @endif
                @if($data->t_idbidang_usaha == 2)
                <div class="row">
                    <div class="col-sm-3 text-blue text-bold"><u>BADAN</u></div>
                </div>
                <div class="row">
                    <div class="col-sm-3">SIUP</div>
                    <div class="col-sm-9">: {{ $data->t_siup_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NIB</div>
                    <div class="col-sm-9">: {{ $data->t_nib_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NPWP</div>
                    <div class="col-sm-9">: {{ $data->t_b_npwp_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama</div>
                    <div class="col-sm-9">: {{ $data->t_b_nama_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat</div>
                    <div class="col-sm-9">: {{ $data->t_b_jalan_penjual }} RT/RW {{ $data->t_b_rt_penjual .'/'. $data->t_b_rw_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $data->t_b_namakel_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $data->t_b_namakec_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $data->t_b_kabkota_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Kode Pos</div>
                    <div class="col-sm-9">: {{ $data->t_b_kodepos_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No.Telp</div>
                    <div class="col-sm-9">: {{ $data->t_b_nohp_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Email</div>
                    <div class="col-sm-9">: {{ $data->t_b_email_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3 text-blue text-bold"><u>PENANGGUNG JAWAB</u></div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NIK</div>
                    <div class="col-sm-9">: {{ $data->t_b_nik_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">NPWP</div>
                    <div class="col-sm-9">: {{ $data->t_b_npwp_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Nama</div>
                    <div class="col-sm-9">: {{ $data->t_b_nama_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Alamat</div>
                    <div class="col-sm-9">: {{ $data->t_b_jalan_pngjwb_penjual }} RT/RW {{ $data->t_b_rt_pngjwb_pembeli .'/'. $data->t_b_rw_pngjwb_pembeli }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kelurahan</strong> {{ $data->t_b_namakel_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kecamatan</strong> {{ $data->t_b_namakec_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3"></div>
                    <div class="col-sm-9">: <strong>Kab/Kota</strong> {{ $data->t_b_kabkota_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Kode Pos</div>
                    <div class="col-sm-9">: {{ $data->t_b_kodepos_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">No.Telp</div>
                    <div class="col-sm-9">: {{ $data->t_b_nohp_pngjwb_penjual }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-3">Email</div>
                    <div class="col-sm-9">: {{ $data->t_b_email_pngjwb_penjual }}</div>
                </div>

                @endif
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-map"></i> INFORMASI OBJEK</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-2">NOP/Tahun SPPT</div>
                    <div class="col-sm-10">: {{ $data->t_nop_sppt .' / '.$data->t_tahun_sppt }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Nama SPPT</div>
                    <div class="col-sm-10">: {{ $data->t_nama_sppt }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Letak</div>
                    <div class="col-sm-4">: {{ $data->t_jalan_sppt }}</div>
                    <div class="col-sm-2">RT/RW</div>
                    <div class="col-sm-4">: {{ $data->t_rt_sppt .''.$data->t_rw_sppt }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Kelurahan</div>
                    <div class="col-sm-4">: {{ $data->t_kelurahan_sppt }}</div>
                    <div class="col-sm-2">Kecamatan</div>
                    <div class="col-sm-4">: {{ $data->t_kecamatan_sppt }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Kabupaten/Kota</div>
                    <div class="col-sm-4">: {{ $data->t_kabkota_sppt }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Jenis Transaksi</div>
                    <div class="col-sm-4">: {{ $data->s_namajenistransaksi }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Jenis Dokumen</div>
                    <div class="col-sm-4">: {{ $data->s_namadoktanah }}</div>
                    <div class="col-sm-2">No. Dok Tanah</div>
                    <div class="col-sm-4">: {{ $data->t_nosertifikathaktanah }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Tgl Akta</div>
                    <div class="col-sm-4">: {{ date('d-m-Y', strtotime($data->t_tgldok_tanah)) }}</div>
                    <div class="col-sm-2">Kepemilikan</div>
                    <div class="col-sm-4">: {{ $data->s_namahaktanah }}</div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-primary">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-calculator"></i> INFORMASI TRANSAKSI BPHTB</h3>
            </div>
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-2">Luas Tanah/Bumi</div>
                    <div class="col-sm-2">: {{ number_format($data->t_luastanah,0,',','.') }} m<sup>2</sup></div>
                    <div class="col-sm-2">NJOP/m<sup>2</sup> Tanah/Bumi</div>
                    <div class="col-sm-2">: {{ number_format($data->t_njoptanah,0,',','.') }}</div>
                    <div class="col-sm-2">NJOP Tanah/Bumi</div>
                    <div class="col-sm-2">: {{ number_format($data->t_totalnjoptanah,0,',','.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Luas Bangunan</div>
                    <div class="col-sm-2">: {{ number_format($data->t_luasbangunan,0,',','.') }} m<sup>2</sup></div>
                    <div class="col-sm-2">NJOP/m<sup>2</sup> Bangunan</div>
                    <div class="col-sm-2">: {{ number_format($data->t_njopbangunan,0,',','.') }}</div>
                    <div class="col-sm-2">NJOP Bangunan</div>
                    <div class="col-sm-2">: {{ number_format($data->t_totalnjopbangunan,0,',','.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">NJOP PBB</div>
                    <div class="col-sm-2">: {{ number_format($data->t_grandtotalnjop,0,',','.') }}</div>
                    <div class="col-sm-2">Harga Transaksi</div>
                    <div class="col-sm-2">: {{ number_format($data->t_nilaitransaksispt,0,',','.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">NPOP</div>
                    <div class="col-sm-2">: {{ number_format($data->t_npop_bphtb,0,',','.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">NPOPTKP</div>
                    <div class="col-sm-2">: {{ number_format($data->t_npoptkp_bphtb,0,',','.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">NPOPKP</div>
                    <div class="col-sm-2">: {{ number_format($data->t_npopkp_bphtb,0,',','.') }}</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">Tarif (%)</div>
                    <div class="col-sm-2">: {{ number_format($data->t_persenbphtb,0,',','.') }}%</div>
                </div>
                <div class="row">
                    <div class="col-sm-2">BPHTB Terhutang</div>
                    <div class="col-sm-2">: <strong class="text-red" style="font-size: 14pt">{{ number_format($data->t_nilai_bphtb_fix,0,',','.') }}</strong></div>
                </div>
            </div>
        </div>
    </div>
</div>
