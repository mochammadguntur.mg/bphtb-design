@extends('layouts.master')

@section('judulkiri')
CEK HARGA WAJAR & HARGA BLOK
@endsection

@section('content')


<div class="row">
    <div class="col-md-6">
        <div class="card card-outline card-info">
            <div class="card-body form-horizontal" style="height: 145px">
                @csrf
                <div class="form-group row">
                    <label class="col-sm-2">NOP</label>
                    <div class="col-sm-7">
                        <input type="text" class="form-control masked-nop" id="t_nop_sppt" name="t_nop_sppt" placeholder="xx.xx.xxx.xxx.xxx.xxxx.x">
                    </div>
                    <div class="col-sm-3">
                        <span class="input-group-append">
                            <button type="button" id="btnCari" class="btn btn-info btn-flat"><i class="fas fa-search"></i> CARI</button>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-calculator"></i> ANALISA SISTEM</h3>
            </div>
            <div class="card-body form-horizontal" style="height: 100px">
                <div class="row" id="bodyDetailAcuan"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-calculator"></i> History Harga/Blok</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body form-horizontal">
                <div style="overflow: auto">
                    <div class="scroll-columns" style="height: 400px">
                        <table class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th rowspan="2" style="background-color: #227dc7; color:white;" class="text-center">NO</th>
                                    <th colspan="4" style="background-color: #227dc7; color:white;" class="text-center">TRANSAKSI</th>
                                    <th colspan="2" style="background-color: #227dc7; color:white;" class="text-center">TANAH</th>
                                    <th colspan="2" style="background-color: #227dc7; color:white;" class="text-center">BANGUNAN</th>
                                    <th rowspan="2" style="background-color: #227dc7; color:white;" class="text-center">HARGA TRANSAKSI</th>
                                    <th rowspan="2" style="background-color: #227dc7; color:white;" class="text-center">ESTIMASI HARGA TRANSAKSI/m<sup>2</sup></th>
                                </tr>
                                <tr>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">NOP</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">NAMA WP</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">TAHUN</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">JENIS TRANSAKSI</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">LUAS </th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">NJOP</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">LUAS</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">NJOP</th>
                                </tr>
                            </thead>
                            <tbody id="bodyDetail"></tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row" id="detailMap">
    <div class="col-md-12">
        <div class="card card-primary card-outline">
            <div class="card-header">
                <h3 class="card-title"><i class="fas fa-map"></i> RINCIAN TRANSAKSI BERDASARKAN PETA TITIK KOORDINAT</h3>
                <div class="card-tools">&nbsp;
                    <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip">
                        <i class="fas fa-minus"></i>
                    </button>
                </div>
            </div>
            <div class="card-body form-horizontal">
                <div id="googleMap"></div>
            </div>
        </div>
    </div>
</div>

@endsection

<style>
    .bd-example-modal-lg .modal-dialog{
        display: table;
        position: relative;
        margin: 0 auto;
        top: calc(50% - 24px);
    }
    .bd-example-modal-lg .modal-dialog .modal-content{
        background-color: transparent;
        border: none;
        box-shadow: none;
    }
    #googleMap {
        height: 400px;
        width: 100%;
        border:1px solid #ccc;
    }
</style>
@push('scripts')
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCIcfDt4yUMeAiGKDTNyIufrBMuif-efms&callback=initMap"></script>
<script type="text/javascript">
    $(function () {
//        $('#detailAcuan').hide();
        $('#detailMap').hide();
    });

    $("#btnCari").click(function () {
        if ($('#t_nop_sppt').val() == '') {
            toastr.info('Silahkan masukkan NOP!');
        } else {
            showInfo($('#t_nop_sppt').val());
        }
    });

    function showInfo(nop) {
        $.ajax({
            url: 'harga-wajar/caridata/{' + nop + '}',
            type: 'GET',
            data: {
                nop: nop
            }
        }).then(function (data) {
            $('#bodyDetail').html(data.data.response);
//            $('#detailAcuan').show();
            $('#bodyDetailAcuan').html(data.data.acuan);
            $('#detailMap').show();
            initialize(data.data.map);
        });
    }

    function initialize(data) {

        var locations = data;

        var propertiPeta = {
            center: new google.maps.LatLng(-7.423844, 109.231146),
            zoom: 14,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };

        var peta = new google.maps.Map(document.getElementById("googleMap"), propertiPeta);
        var marker, i;

        var i = 1;
        locations.forEach(element => {

            var infowindow = new google.maps.InfoWindow({
                disableAutoPan: true
            });
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(element.s_latitude, element.s_longitude),
                map: peta,
                animation: google.maps.Animation.DROP
            });

            google.maps.event.addListener(marker, 'click', (function (marker, i) {
                return function () {
                    var njoptanah = '<b>' + formatRupiah(element.t_njoptanah, 0) + '/m2</b>';
                    infowindow.setContent(njoptanah);
                    infowindow.open(peta, marker);
                }
            })(marker, i));
            i++;
            google.maps.event.trigger(marker, 'click');
        });
    }
//    google.maps.event.addDomListener(window, 'load', initialize);
</script>
@endpush
