@extends('layouts.master')

@section('judulkiri')
PENGATURAN USER [TAMBAH]
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="card mb-4">
            <div class="card-body">
                <form action="{{ route('setting-user.create') }}" method="post">
                    @csrf
                    <div class="col-sm-12 row mb-2">
                        <label class="col-sm-2" for="name">Nama Lengkap</label>
                        <div class="col-md-4">
                            <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') ?? $user->name }}"  autocomplete="name" autofocus>
                        </div>
                        <div class="col-md-4">
                            @if ($errors->has('name'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('name') }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 row mb-2">
                        <label class="col-sm-2" for="username">Username</label>
                        <div class="col-md-4">
                            <input id="username" type="username" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') ?? $user->username }}" >
                        </div>
                        <div class="col-md-4">
                            @if ($errors->has('username'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('username') }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 row mb-2">
                        <label class="col-sm-2" for="email">Email</label>
                        <div class="col-md-4">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') ?? $user->email }}"  autocomplete="email">
                        </div>
                        <div class="col-md-4">
                            @if ($errors->has('email'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('email') }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 row mb-2">
                        <label class="col-sm-2" for="password">Password</label>
                        <div class="col-md-4">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password"  autocomplete="new-password">
                        </div>
                        <div class="col-md-4">
                            @if ($errors->has('password'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('password') }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 row mb-2">
                        <label class="col-sm-2" for="s_tipe_pejabat">Tipe Pejabat</label>
                        <div class="col-md-4">
                            <select id="s_tipe_pejabat" type="text" class="form-control @error('s_tipe_pejabat') is-invalid @enderror" name="s_tipe_pejabat"  onchange="listpejabatnotaris(this.value)">
                                <option value="">Silahkan Pilih</option>
                                <option value="1">Pejabat</option>
                                <option value="2">Notaris</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            @if ($errors->has('s_tipe_pejabat'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_tipe_pejabat') }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 row mb-2">
                        <label class="col-sm-2" for="s_idpejabat_idnotaris">Pejabat</label>
                        <div class="col-md-4">
                            <select id="s_idpejabat_idnotaris" type="text" class="form-control @error('s_idpejabat_idnotaris') is-invalid @enderror" name="s_idpejabat_idnotaris" onchange="listhakakses()">
                                <option value="">Silahkan Pilih</option>
                            </select>
                        </div>
                        <div class="col-md-4">
                            @if ($errors->has('s_idpejabat_idnotaris'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_idpejabat_idnotaris') }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="col-sm-12 row mb-2">
                        <label class="col-sm-2" for="s_id_hakakses">Jenis Hak Akses</label>
                        <div class="col-md-4">
                            <select id="s_id_hakakses" type="text" class="form-control @error('s_id_hakakses') is-invalid @enderror" name="s_id_hakakses" >
                                <option value="">Silahkan Pilih</option>
                                @foreach ($tipeHakAkses as $a)
                                <option value="{{ $a->id }}"> {{ $a->name }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div>
                            <h5 class="col-sm-12 mt-2 text-green" style="font-size: 100%" >Auto Submit for Master User->Role User</h5>
                        </div>
                        <div class="col-md-4">
                            @if ($errors->has('s_id_hakakses'))
                            <div class="text-danger mt-2">
                                {{ $errors->first('s_id_hakakses') }}
                            </div>
                            @endif
                        </div>
                    </div>
                    <a href="{{ route('setting-user.index') }}" class="btn btn-danger" data-dismiss="modal"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
                    <button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> Simpan</button>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection
@push('scripts')
<script type="text/javascript">
    function listpejabatnotaris(a) {
        var pejabatNotarisSelect = $("#s_idpejabat_idnotaris");
        $.ajax({
            url: 'listpejabatnotaris/' + a,
            type: 'GET',
            data: {
                's_tipe_pejabat': a
            }
        })
                .then(function (response) {
                    pejabatNotarisSelect.empty();
                    pejabatNotarisSelect.append('<option value="">SILAHKAN PILIH</option>');
                    // console.log(response);
                    response.forEach(function (col) {
                        pejabatNotarisSelect.append('<option value="' + col.id + '">' +
                                col.id + ' - ' +
                                col.nama + '</option>');
                    });
                })
                .catch(function (response) {
                    //handle error
                });
    }

</script>
@endpush
