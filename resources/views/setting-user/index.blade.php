@extends('layouts.master')

@section('judulkiri')
PENGATURAN USER
@endsection

@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-outline card-info">
                    <div class="card-header">
                        <h3 class="card-title">
                            <a href="{{ route('setting-user.tambah') }}" class="btn btn-primary"><i class="fas fa-plus-circle"></i> Tambah</a>
                            <!-- <a href="{{ route('setting-user.backupdb') }}"  class="btn btn-primary">Backup DB</a> -->
                            <!-- <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                                Excel
                            </a>
                            <a href="javascript:void(0)" class="btn btn-danger"  id="btnPDF">
                                PDF
                            </a> -->
                        </h3>
                        <div class="card-tools">
                            <button type="button" class="btn btn-tool btn-sm" data-card-widget="collapse" data-toggle="tooltip" title="Collapse">
                                <i class="fas fa-minus"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <!-- /.card-header -->
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                                <thead>
                                    <tr>
                                        <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                        <th style="background-color: #227dc7; color:white;" class="text-center">Name</th>
                                        <th style="background-color: #227dc7; color:white;" class="text-center">Username</th>
                                        <th style="background-color: #227dc7; color:white;" class="text-center">Email</th>
                                        <th style="background-color: #227dc7; color:white;" class="text-center">Create At</th>
                                        <th style="background-color: #227dc7; color:white;" class="text-center">Aksi</th>
                                    </tr>
                                    <tr>
                                        <th></th>
                                        <th><input type="text" class="form-control form-control-sm" id="filter-name"></th>
                                        <th><input type="text" class="form-control form-control-sm" id="filter-username"></th>
                                        <th><input type="text" class="form-control form-control-sm" id="filter-email"></th>
                                        <th><input type="text" class="form-control form-control-sm" id="filter-created_at" readonly></th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td class="text-center" colspan="5"> Tidak ada data.</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="card-footer clearfix pagination-footer">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">Name</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="nameHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">Email</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="emailHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span> Hapus</button>
            </div>
        </div>
    </div>
</div>
@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
        url: 'datagrid',
        table: "#datagrid-table",
        columns: [{
                class: ""
            },
            {
                class: ""
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "name"
            },
            {
                sortable: true,
                name: "username"
            },
            {
                sortable: true,
                name: "email"
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]

    });

    $("#filter-name, #filter-username, #filter-email, #filter-created_at").change(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            name: $("#filter-name").val(),
            username: $("#filter-username").val(),
            email: $("#filter-email").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: '/setting-user/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].id);
            $("#nameHapus").val(data[0].name);
            $("#emailHapus").val(data[0].email);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: '/setting-user/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            datatables.reload();
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        var name = $("#filter-name").val();
        var email = $("#filter-email").val();
        var created_at = $("#filter-created_at").val();
        if (a.target.id == 'btnExcel') {
            window.open('/setting-user/export?name=' + name + '&email=' + email + '&created_at=' + created_at);
        } else {
            window.open('/setting-user/export_pdf?name=' + name + '&email=' + email + '&created_at=' + created_at);
        }

    });
</script>
@endpush