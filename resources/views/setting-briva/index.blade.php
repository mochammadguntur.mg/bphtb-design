@extends('layouts.master')

@section('judulkiri')
PENGATURAN BRIVA
@endsection

@section('content')

<div class="card card-outline card-info shadow-sm">
    <div class="card-body">
        <form action="{{ route('setting-briva.createOrUpdate') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <input type="hidden" class="form-control" name="s_idbriva" id="s_idbriva" value="{{ optional($briva)->s_idbriva }}">
            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Client ID</label>
                    <input type="text" class="form-control @error('client_id') is-invalid @enderror" name="client_id" id="client_id" value="{{ old('client_id', optional($briva)->client_id) }}" readonly>
                    @if ($errors->has('client_id'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('client_id') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Secret ID</label>
                    <input type="text" class="form-control @error('secret_id') is-invalid @enderror" name="secret_id" id="secret_id" value="{{ old('secret_id', optional($briva)->secret_id) }}" readonly>
                    @if ($errors->has('secret_id'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('secret_id') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Institution Code </label>
                    <input type="text" class="form-control col-4 @error('institution_code') is-invalid @enderror" name="institution_code" id="institution_code" value="{{ old('institution_code', optional($briva)->institution_code) }}" readonly>
                    @if ($errors->has('institution_code'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('institution_code') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Briva No</label>
                    <input type="text" class="form-control col-4 @error('briva_no') is-invalid @enderror" name="briva_no" id="briva_no" onkeypress="return numberOnly(event)" value="{{ old('briva_no', optional($briva)->briva_no) }}" maxlength="5" readonly>
                    @if ($errors->has('briva_no'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('briva_no') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>URL Development</label>
                    <input type="text" class="form-control col-12 @error('url_development') is-invalid @enderror" name="url_development" id="url_development" value="{{ old('url_development', optional($briva)->url_development) }}"  readonly>
                    @if ($errors->has('url_development'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('url_development') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>URL Production</label>
                    <input type="text" class="form-control col-12 @error('url_production') is-invalid @enderror" name="url_production" id="url_production" value="{{ old('url_production', optional($briva)->url_production) }}" readonly>
                    @if ($errors->has('url_production'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('url_production') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>URL Aktif</label>

                    <select type="text" name="url_aktif" class="form-control col-6 @error('url_aktif') is-invalid @enderror" id="url_aktif">
                        <option disabled selected value="">Pilih URL</option>
                        @foreach ($urlaktif as $urlaktif)
                        <option {{ (old('urlaktif') ?? $briva->url_aktif) == $urlaktif->s_idurl ? 'selected' : '' }} value="{{ $urlaktif->s_idurl }}">{{ $urlaktif->s_nama_url }}
                        </option>
                        @endforeach
                    </select>
                    @if ($errors->has('url_aktif'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('url_aktif') }}
                    </div>
                    @endif
                </div>
            </div>
            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Status</label>
                    <select type="text" name="status" class="form-control col-4 @error('status') is-invalid @enderror" id="status">
                        <option disabled selected value="">Pilih Status</option>
                        @foreach ($status as $status)
                        <option {{ (old('status') ?? $briva->status) == $status->s_id_status ? 'selected' : '' }} value="{{ $status->s_id_status }}">{{ $status->s_nama_status }}
                        </option>
                        @endforeach
                    </select>
                    @if ($errors->has('status'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('status') }}
                    </div>
                    @endif
                </div>
            </div>

    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm float-right" id="submit">
            <i class="fas fa-save"></i>
            Simpan
        </button>
        <a href="{{ url('setting-briva') }}" class="btn btn-danger btn-sm float-left" id="batalBtn">
            <i class="fas fa-redo"></i>
            Batal
        </a>

        <button type="button" class="btn btn-warning btn-sm float-right" id="editBtn">
            <i class="fas fa-edit"></i>
            Edit
        </button>

        </form>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(() => {
    @if (session('success'))
            toastr.success('{{session('success')}}')
            @endif

            @if (session('error'))
            toastr.error('{{session('error')}}')
            @endif

            $('#upload_row').css('display', 'none');
    $('#submit').css('display', 'none');
    $('#batalBtn').css('display', 'none');
    @if (count($errors) > 0)
            $('#upload_row').css('display', 'block');
    $('#submit').css('display', 'block');
    $('#batalBtn').css('display', 'block');
    $('#editBtn').css('display', 'none');
    $('input').removeAttr('readonly');
    $('textarea').removeAttr('readonly');
    @endif
    });
    $(document).on("click", "#s_logo", () => {
    var file = $(this).parents().find("#file");
    file.trigger("click");
    });
    $('input[type="file"]').change((e) => {
    var input = document.querySelector('input[type=file]');
    var file = input.files[0];
    var reader = new FileReader();
    reader.onload = (e) => {
    document.getElementById("preview").src = e.target.result;
    };
    reader.readAsDataURL(file);
    });
    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
    $(".alert").slideUp(500);
    });
    $('#editBtn').on("click", () => {

    $('#upload_row').css('display', 'block');
    $('#submit').css('display', 'block');
    $('#batalBtn').css('display', 'block');
    $('#editBtn').css('display', 'none');
    $('input').removeAttr('readonly');
    $('textarea').removeAttr('readonly');
    })

            function numberOnly(e) {
            var input = (e.which) ? e.which : e.keyCode

                    if (input > 31 && (input < 48 || input > 57))
                    return false;
            return true;
            }

</script>
@endpush
