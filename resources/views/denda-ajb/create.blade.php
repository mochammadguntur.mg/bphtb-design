@extends('layouts.master')

@section('judulkiri')
    TETAPKAN DENDA
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-6">
            <div class="card card-primary">
                <form action="{{ url('denda-ajb') }}" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @csrf
                        <div class="form-group">
                            <label for="name">No Daftar</label>
                            <div class="row pl-2">
                                <input type="hidden" name="t_idspt" id="t_idspt"
                                    value="{{ old('t_idspt') ?? ($data['t_idspt'] ?? '') }}">
                                <input type="text" name="t_kohir" id="t_kohir" class="form-control col-md-3"
                                    value="{{ old('t_kohir') ?? ($data['t_kohir'] ?? '') }}" readonly>
                                <button type="button" class="btn btn-warning ml-2" data-toggle="modal"
                                    onclick="datatables.reload()" data-target="#modal-lapor-bulanan"
                                    {{ $cari ?? '' }}>Cari</button>
                            </div>
                            @if ($errors->has('t_idlaporbulanan'))
                                <div class="text-danger mt-2">
                                    {{ $errors->first('t_idlaporbulanan') }}
                                </div>
                            @endif
                        </div>

                        <div class="form-group">
                            <label>Nama PPAT/Notaris</label>
                            <input type="hidden" name="t_idnotaris" id="t_idnotaris"
                                value="{{ old('s_namanotaris') ?? ($data['t_idnotaris'] ?? '') }}">
                            <input type="text" name="s_namanotaris" id="s_namanotaris" class="form-control col-md-6"
                                value="{{ old('s_namanotaris') ?? ($data['s_namanotaris'] ?? '') }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>Tanggal AJB</label>
                            <input type="text" name="t_tgl_ajb" id="t_tgl_ajb" class="form-control col-md-6"
                                value="{{ old('t_tgl_ajb') ?? ($data['t_tgl_ajb'] ?? '') }}" readonly>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Bayar BPHTB</label>
                            <input type="text" name="t_tglpembayaran_pokok" id="t_tglpembayaran_pokok"
                                class="form-control col-md-6"
                                value="{{ old('t_tglpembayaran_pokok') ?? ($data['t_tglpembayaran_pokok'] ?? '') }}"
                                readonly>
                        </div>

                        <div class="form-group">
                            <label>Tanggal Penetapan</label>
                            <div class="input-group date col-md-6" id="tgl_penetapan" data-target-input="nearest">
                                <div class="input-group-append" data-target="#tgl_penetapan" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                                <input type="text" name="t_tglpenetapan" id="t_tglpenetapan"
                                    value="{{ old('t_tglpenetapan') ?? ($data['t_tglpenetapan'] ?? '') }}"
                                    class="form-control datetimepicker-input" data-target="#tgl_penetapan" />
                            </div>
                        </div>


                        <a href="{{ url('denda-ajb') }}" class="btn btn-info"><i class="fas fa-redo"></i>
                            Kembali</a>
                        <button type="submit" class="btn btn-primary"> Tetapkan</button>

                        {{-- @include('denda-laporan-ajb.partials.form', ['sumbit'=>
                        'Tetapkan', 'icon' => 'fas fa-save']) --}}
                    </div>
                </form>
            </div>
        </div>
    </div>
    @include('denda-ajb.partials.modal-tabel')
@endsection

@push('scripts')
    <script src="{{ asset('/datagrid/datagrid.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(() => {
            $('#tgl_penetapan').datetimepicker({
                format: 'DD-MM-YYYY'
            });
        });

        var datatables = datagrid({
            url: 'datagrid_lapor_bulanan',
            table: "#datagrid-table",
            columns: [{
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
                {
                    class: ""
                },
                {
                    class: "text-center"
                },
                {
                    class: "text-center"
                },
            ],
            orders: [{
                    sortable: false
                },
                {
                    sortable: false
                },
                {
                    sortable: true,
                    name: "no_daftar"
                },
                {
                    sortable: true,
                    name: "tgl_daftar"
                },
                {
                    sortable: true,
                    name: "jenis_peralihan"
                },
                {
                    sortable: false
                },
                {
                    sortable: false
                },
            ],
            action: [{
                name: 'pilih',
                btnClass: 'btn btn-info btn-sm',
                btnText: '<i class="fas fa-hand-point-up"> Pilih</i>'
            }]
        });

        function pilihData(a) {
            $.ajax({
                url: 'caridata/' + a,
                type: 'GET',
                data: {
                    id: a,
                }
            }).then(function(data) {
                console.log(data);
                $('#t_idspt').val(data.t_idspt)
                $('#t_kohir').val(data.t_kohir)
                $('#t_idnotaris').val(data.t_idnotaris)
                $('#s_namanotaris').val(data.s_namanotaris)
                $('#t_tgl_ajb').val(data.t_tgl_ajb)
                $('#t_tglpembayaran_pokok').val(data.t_tglpembayaran_pokok)
            });

            $('#modal-lapor-bulanan').modal('hide');
            $('body').removeClass('modal-open');
            $('.modal-backdrop').remove();
        }

    </script>
@endpush
