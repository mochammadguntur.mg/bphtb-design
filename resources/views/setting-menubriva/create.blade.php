@extends('layouts.master')

@section('judulkiri')
PENGATURAN PEJABAT [TAMBAH]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="{{ url('setting-pejabat/store') }}" method="POST" enctype="multipart/form-data">
                    <div class="card-body mt-4 mb-2">
                        @csrf
                        @include('setting-pejabat.partials.form', ['sumbit'=> 'Simpan'])
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
