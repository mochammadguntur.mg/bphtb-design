@extends('layouts.master')
@section('judulkiri')
    PENGATURAN ROLES
@endsection
@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Roles</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Role & Permission</a></li>
                    <li class="breadcrumb-item active">Roles</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        <div class="card mb-4">
            <div class="card-header">Edit Role</div>
            <div class="card-body">
                <form action="{{ route('roles.edit', $role) }}" method="post">
                    @csrf
                    @method('PUT')
                    @include('permission.roles.partials.form-control')
                    <a href="{{ route('roles.index') }}" class="btn btn-danger float-right">Kembali</a>
                </form>
            </div>
        </div>
    </div>
</section>
@endsection