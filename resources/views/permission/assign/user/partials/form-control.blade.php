<div class="form-group">
    <label for="email">Email</label>
    <input type="text" name="email" id="email" class="form-control"></input>
</div>
<div class="form-group">
    <label for="roles">Pick Roles</label>
    <select name="roles[]" id="roles" class="form-control duallistbox" multiple style="height: 400px;">
        @foreach($roles as $role)
        <option value="{{ $role->id }}">{{ $role->name }}</option>
        @endforeach
    </select>
</div>
<button type="submit" class="btn btn-primary">Assign</button>
@push('scripts')
<script type="text/javascript">
    $(function () {
        $('.duallistbox').bootstrapDualListbox();
    });
</script>
@endpush