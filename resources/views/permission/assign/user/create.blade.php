@extends('layouts.master')
@section('judulkiri')
    PENGATURAN ASSIGN ROLE TO USER
@endsection

@section('content')
<div class="content-header">
    <div class="container-fluid">
        <div class="row mb-2">
            <div class="col-sm-6">
                <h1 class="m-0 text-dark">Dashboard</h1>
            </div>
            <div class="col-sm-6">
                <ol class="breadcrumb float-sm-right">
                    <li class="breadcrumb-item"><a href="#">Home</a></li>
                    <li class="breadcrumb-item active">Dashboard v1</li>
                </ol>
            </div>
        </div>
    </div>
</div>
<section class="content">
    <div class="container-fluid">
        @if(session('success'))
        <div class="alert alert-success">
            {{ session('success') }}
        </div>
        @endif
        <div class="card">
            <div class="card-header">Assign Permission</div>
            <div class="card-body">
                <form action="{{ route('assign.user.create') }}" method="post">
                    @csrf
                    <div class="form-group">
                        <label for="email">User</label>
                        <input type="text" name="email" id="email" class="form-control"></input>
                    </div>
                    <div class="form-group">
                        <label for="roles">Pick Roles</label>
                        <select name="roles[]" id="roles" class="form-control duallistbox" multiple style="height: 400px;">
                            @foreach($roles as $role)
                            <option value="{{ $role->id }}">{{ $role->name }}</option>
                            @endforeach
                        </select>
                    </div>
                    <button type="submit" class="btn btn-primary">Assign</button>
                </form>
            </div>
        </div>
        <div class="card">
            <div class="card-header">Role & Permissions Table</div>
            <div class="card-body">
                <table class="table table-hover">
                    <tr>
                        <th>#</th>
                        <th>Name</th>
                        <th>The Roles</th>
                        <th>Act</th>
                    </tr>
                    @foreach($users as $index => $user)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $user->name }}</td>
                        <td>{{ implode(", ", $user->getRoleNames()->toArray()) }}</td>
                        <td><a href="{{ route('assign.user.edit', $user) }}">Sync</a></td>
                    </tr>
                    @endforeach
                </table>
            </div>
        </div>
    </div>
</section>
@endsection

@push('scripts')
<script type="text/javascript">
    $(function () {
        $('.duallistbox').bootstrapDualListbox();
    });
</script>
@endpush