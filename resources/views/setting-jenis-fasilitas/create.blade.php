@extends('layouts.master')

@section('judulkiri')
    PENGATURAN JENIS FASILITAS [TAMBAH]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="{{ url('setting-jenis-fasilitas/store') }}" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @csrf
                        @include('setting-jenis-fasilitas.partials.form', ['submit'=> 'Simpan'])
                    </div>
                </form>
            </div>
        </div>
    </div>
    
@endsection