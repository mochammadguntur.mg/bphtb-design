@extends('layouts.master')

@section('judulkiri')
PENGATURAN JENIS FASILITAS [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="/setting-jenis-fasilitas/{{ $jenisfasilitas->s_idjenisfasilitas }}/edit" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-jenis-fasilitas.partials.form')
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
