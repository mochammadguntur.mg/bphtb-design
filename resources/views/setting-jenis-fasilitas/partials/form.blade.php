<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Jenis Fasilitas</label>
    <div class="col-md-4">
        <input type="text" name="s_kodejenisfasilitas" class="form-control @error('s_kodejenisfasilitas') is-invalid @enderror" id="s_kodejenisfasilitas" placeholder="Kode Jenis Fasilitas"
               value="{{ old('s_kodejenisfasilitas') ?? $jenisfasilitas->s_kodejenisfasilitas }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kodejenisfasilitas'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_kodejenisfasilitas') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Nama Jenis Fasilitas</label>
    <div class="col-md-4">
        <input type="text" name="s_namajenisfasilitas" class="form-control @error('s_namajenisfasilitas') is-invalid @enderror" id="s_namajenisfasilitas" placeholder="Nama Jenis Fasilitas"
               value="{{ old('s_namajenisfasilitas') ?? $jenisfasilitas->s_namajenisfasilitas }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_namajenisfasilitas'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_namajenisfasilitas') }}
        </div>
        @endif
    </div>
</div>

<a href="/setting-jenis-fasilitas" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $submit ?? 'Update' }}</button>
