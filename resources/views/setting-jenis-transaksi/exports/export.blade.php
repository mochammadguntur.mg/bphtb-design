<table style="border-collapse: collapse; width:100%">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">No</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Kode Jenis Transaksi</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Nama Jenis Transaksi</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Jenis Perhitungan</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Status Dapat NPOPTKP</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Created at</th>
        </tr>
    </thead>
    <tbody>
        @foreach($jenistransaksi as $index => $rows)
        {{ error_reporting(0) }}
        <?php
        $jenisPerhitungan = !empty($rows['s_nama']) ? $rows['s_nama'] : '';
        $statusNpoptkp = !empty($rows['s_nama_dptnpoptkp']) ? $rows['s_nama_dptnpoptkp'] : '';
        ?>
        <tr>
            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $index + 1 }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $rows->s_kodejenistransaksi }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->s_namajenistransaksi }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $jenisPerhitungan }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $statusNpoptkp }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->created_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
