<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Kode Jenis Transaksi</label>
    <div class="col-md-4">
        <input type="text" name="s_kodejenistransaksi" class="form-control @error('s_kodejenistransaksi') is-invalid @enderror" id="s_kodejenistransaksi"
               value="{{ old('s_kodejenistransaksi') ?? $jenistransaksi->s_kodejenistransaksi }}" maxlength="2" onkeypress="return numberOnly(event)" placeholder="Kode Jenis Transaksi">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_kodejenistransaksi'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_kodejenistransaksi') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Nama Jenis Transaksi</label>
    <div class="col-md-4">
        <input type="text" name="s_namajenistransaksi" class="form-control @error('s_namajenistransaksi') is-invalid @enderror" id="s_namajenistransaksi"
               value="{{ old('s_namajenistransaksi') ?? $jenistransaksi->s_namajenistransaksi }}" placeholder="Nama Jenis Transaksi">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_namajenistransaksi'))
        <div class="text-danger mb-2">
            {{ $errors->first('s_namajenistransaksi') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Jenis Perhitungan</label>
    <div class="col-md-4">
        <select name="s_idstatus_pht" id="s_idstatus_pht" class="form-control">
            <option value="">Pilih</option>
            @foreach ($data_statusperhitungan as $a)
            <option value="{{ $a->s_idstatus_pht }}" {{ (old('s_idstatus_pht') ?? $jenistransaksi->s_idstatus_pht) == $a->s_idstatus_pht ? 'selected' : '' }}> {{ $a->s_nama }}</option>
            @endforeach
        </select>
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Status Dapat NPOPTKP</label>
    <div class="col-md-4">
        <select name="s_id_dptnpoptkp" id="s_id_dptnpoptkp" class="form-control">
            <option value="">Pilih</option>
            @foreach ($data_statusdptnpoptkp as $b)
            <option value="{{ $b->s_id_dptnpoptkp }}" {{ (old('s_id_dptnpoptkp') ?? $jenistransaksi->s_id_dptnpoptkp ) == $b->s_id_dptnpoptkp ? 'selected' : '' }}> {{ $b->s_nama_dptnpoptkp }}</option>
            @endforeach
        </select>
    </div>
</div>

@push('scripts')
<script>
    function numberOnly(e) {
        var input = (e.which) ? e.which : e.keyCode

        if (input > 31 && (input < 48 || input > 57))
            return false;
        return true;
    }
</script>
@endpush