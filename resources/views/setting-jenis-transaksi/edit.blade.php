@extends('layouts.master')

@section('judulkiri')
PENGATURAN JENIS TRANSAKSI [EDIT]
@endsection

@section('content')
<form action="/setting-jenis-transaksi/{{ $jenistransaksi->s_idjenistransaksi }}/edit" method="POST">
    <div class="card card-primary">
        <div class="card-body mt-4 mb-2">
            @method('patch')
            @csrf
            @include('setting-jenis-transaksi.partials.form')
        </div>
        <div class="card-footer ">
            <a href="{{ url('setting-jenis-transaksi') }}" class="btn btn-danger prevBtn pull-left" type="button"><i class="fa fa-arrow-circle-left"></i> KEMBALI</a>
            <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> Update </button>
        </div>
    </div>
</form>
@endsection
