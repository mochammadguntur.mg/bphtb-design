<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@php
    $pemda = Illuminate\Support\Facades\DB::table('s_pemda')->first();
@endphp
<head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1.0">
   <meta http-equiv="X-UA-Compatible" content="ie=edge">
   <title>My BPHTB || {{ strtoupper($pemda->s_namakabkot) }}</title>
   <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('depan/assets/images/favicons/apple-touch-icon.png') }}">
   <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('storage/'. $pemda->s_logo .'') }}">
   <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('storage/'. $pemda->s_logo .'') }}">
   <link rel="manifest" href="{{ asset('depan/assets/images/favicons/site.webmanifest') }}">

   <!-- plugin scripts -->
   <link href="https://fonts.googleapis.com/css?family=Barlow:300,400,500,600,700,800,900&display=swap"
      rel="stylesheet">
   <link rel="stylesheet" href="{{asset('depan/assets/css/animate.min.css')}}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/bootstrap.min.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/owl.carousel.min.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/owl.theme.default.min.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/magnific-popup.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/font-awesome.min.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/swiper.min.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/plugins/gimpo-icons/style.css') }}">

   <!-- template styles -->
   <link rel="stylesheet" href="{{ asset('depan/assets/css/style.css') }}">
   <link rel="stylesheet" href="{{ asset('depan/assets/css/responsive.css') }}">

   <link rel="stylesheet" href="{{asset('plugins/fontawesome-free/css/all.min.css')}}">
   
   <style>
        #mapkantor {
            width: 100%;
            height: 250px;
        }
    </style>

</head>

<body>
   <div class="preloader"><span></span></div>
   <div class="page-wrapper">
      <header class="site-header site-header__header-two ">
         <nav class="navbar navbar-expand-lg navbar-light header-navigation stricky">
            <div class="container clearfix">
               <div class="logo-box clearfix">
                  <a class="navbar-brand" href="{{ url('') }}">
                     <img src="{{ asset('storage/'. $pemda->s_logo .'') }}" width="45" /> &nbsp; <img src="{{ asset('dist/img/mybphtb.png') }}" class="main-logo" width="88" alt="Awesome Image" />
                  </a>
                  <button class="menu-toggler" data-target=".main-navigation">
                     <span class="fa fa-bars"></span>
                  </button>
               </div>
               <div class="main-navigation">
                  <ul class=" one-page-scroll-menu navigation-box">
                     <li class="current scrollToLink">
                        <a href="#banner">Halaman Utama</a>
                     </li>
                     <li class="scrollToLink">
                        <a href="#features">Fitur</a>
                     </li>
                     <li class="scrollToLink">
                        <a href="#about">Tentang</a>
                     </li>
                     <li class="scrollToLink">
                        <a href="#timeline">Alur</a>
                     </li>
                     <li class="scrollToLink">
                        <a href="#payments">Pembayaran</a>
                     </li>
                     <li class="scrollToLink">
                        <a href="#faqs">FAQ</a>
                     </li>
                     <li class="scrollToLink">
                        <a href="#contacts">Kontak</a>
                     </li>
                  </ul>
               </div>
               <div class="right-side-box">
                  @if (Route::has('login'))
                  @auth
                  <a class="thm-btn header__cta-btn2" href="{{ url('/home') }}">Log In</a>
                  @else
                  <a class="thm-btn header__cta-btn2" href="{{ route('login') }}">Log In</a>
                  @endif
                  @endif
               </div>
            </div>
         </nav>
      </header>
      <section class="banner-two" id="banner">
         <div class="container">
            <div class="banner-two__moc">
               <img src="{{ asset('depan/assets/images/thumbnail-10.png') }}" alt="">
            </div>
            <div class="row">
               <div class="col-xl-6 col-lg-8">
                  <div class="banner-two__content">
                     <img src="{{ asset('depan/assets/images/cta-line-1-1.png') }}" class="banner-two__line" alt="">
                     <h3 class="banner-two__title">Cara Termudah Daftar <b>BPHTB</b> </h3>
                     <p class="banner-two__text">Ayo segera daftarkan BPHTB ke {{ $pemda->s_namasingkatinstansi .' '.$pemda->s_namakabkot }}, Silahkan Login untuk masuk ke
                        aplikasi.</p>
                     @if (Route::has('login'))
                     @auth
                     <a class="banner-two__btn thm-btn" href="{{ url('/home') }}">Log in Sekarang</a>
                     @else
                     <a class="banner-two__btn thm-btn" href="{{ route('login') }}">Log in Sekarang</a>
                     @endif
                     @endif
                  </div>
               </div>
            </div>
         </div>
         {{-- <div class="brand-one">
            <div class="container">
               <div class="brand-one__carousel owl-carousel owl-theme">
                  <div class="item">
                     <img src="{{ asset('depan/assets/images/gopay.png') }}" alt="">
                  </div>
                  <div class="item">
                     <img src="{{ asset('depan/assets/images/madiun.png') }}" alt="">
                  </div>
                  <div class="item">
                     <img src="{{ asset('depan/assets/images/logo-ovo.png') }}" alt="">
                  </div>
                  <div class="item">
                     <img src="{{ asset('depan/assets/images/iconQris.png') }}" alt="">
                  </div>
                  <div class="item">
                     <img src="{{ asset('depan/assets/images/logo-bank-jatim.png') }}" alt="">
                  </div>
               </div>
            </div>
         </div> --}}
      </section>

      <section class="blog-one" id="features">
         <div class="container">
            <div class="block-title text-center">
               <p class="block-title__tag-line">Fitur-fitur Aplikasi
               </p>
               <h2 class="block-title__title">Keunggulan Aplikasi Hanya Untuk Anda</h2>
            </div>
            <div class="service-one__content-wrap mb-lg-5">
               <div class="service-one__single">
                  <div class="service-one__inner">
                     <i class="fas fa-door-open fa-2x"></i>
                     <h3 class="service-one__title"><a href="#">Cepat<br>Diakses</a></h3>
                     <div class="mt-2 p-2">
                        <span>Aplikasi sangat cepat diakses dari mana saja.</span>
                     </div>
                  </div>
               </div>
               <div class="service-one__single">
                <div class="service-one__inner">
                   <i class="fas fa-shapes fa-2x"></i>
                   <h3 class="service-one__title"><a href="#">Aplikasi<br>Responsif</a></h3>
                   <div class="mt-2 p-2">
                      <span>Aplikasi bisa dibuka dengan berbagai macam device.</span>
                   </div>
                </div>
             </div>
               <div class="service-one__single">
                  <div class="service-one__inner">
                     <i class="fas fa-diagnoses fa-2x"></i>
                     <h3 class="service-one__title"><a href="#">Mudah<br>Digunakan</a></h3>
                     <div class="mt-2 p-2">
                        <span>Aplikasi sangat mudah digunakan oleh user.</span>
                     </div>
                  </div>
               </div>
               <div class="service-one__single">
                  <div class="service-one__inner">
                     <i class="fas fa-shield-alt fa-2x"></i>
                     <h3 class="service-one__title"><a href="#">Keamanan <br> Terjaga</a></h3>
                     <div class="mt-2 p-2">
                        <span>Keamanan aplikasi yang sudah terjaga dengan baik.</span>
                     </div>
                  </div>
               </div>
               <div class="service-one__single">
                  <div class="service-one__inner">
                     <i class="fas fa-donate fa-2x"></i>
                     <h3 class="service-one__title"><a href="#">Pembayaran <br> Mudah</a></h3>
                     <div class="mt-2 p-2">
                        <span>Akses pembayaran yang sangant mudah.</span>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="cta-two" id="about">
         <div class="container">
            <div class="block-title text-center">
               <p class="block-title__tag-line">Tentang
               </p>
               <h2 class="block-title__title" >Penjelasan Singkat BPHTB</h2>
            </div>
            <div class="cta-two__image" style="z-index: -1;">
               <img src="{{ asset('depan/assets/images/cta-line-1-1.png') }}" class="cta-two__line" alt="">
               <img src="{{ asset('depan/assets/images/bphtb.png') }}" alt="">
            </div>
            <div class="row justify-content-end">
               <div class="col-xl-6 col-lg-7">
                  <div class="cta-two__content">
                     <p class="cta-two__tagline text-muted">&emsp;&emsp;Merujuk UU 28/2009, Bea Perolehan Hak atas Tanah dan Bangunan adalah pajak atas perolehan hak atas tanah dan/atau bangunan. Perolehan Hak atas Tanah dan/atau Bangunan adalah perbuatan atau peristiwa hukum yang mengakibatkan diperolehnya hak atas tanah dan/atau bangunan oleh orang pribadi atau Badan.</p>
                     <!-- /.cta-two__tagline -->

                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="blog-one blog-one__home thm-gray-bg" id="timeline">
         <div class="container">
            <div class="block-title text-center">
               <p class="block-title__tag-line">Alur</p>
               <h2 class="block-title__title">Alur Aplikasi</h2>
            </div>
            <div class="timeline">
               <ul>
                  <li>
                     <div class="content">
                        <h3>Pendaftaran</h3>
                        <p>Lakukan pengisian form dan lengkapi persyaratan sesuai dengan data anda yang benar.</p>
                     </div>
                     <div class="time">
                        <h4>Langkah 1</h4>
                     </div>
                  </li>

                  <li>
                     <div class="content">
                        <h3>Validasi Berkas</h3>
                        <p>Formulir pendaftaran dan kelengkapan syarat pendaftaran akan dicek oleh petugas validasi
                           berkas</p>
                     </div>
                     <div class="time">
                        <h4>Langkah 2</h4>
                     </div>
                  </li>

                  <li>
                     <div class="content">
                        <h3>Validasi Kabid</h3>
                        <p>Setelah kebenaran formulir pendaftaran dan syarat pendaftaran telah lengkap, selanjutnya
                           tahap proses validasi oleh kepala bidang.</p>
                     </div>
                     <div class="time">
                        <h4>Langkah 3</h4>
                     </div>
                  </li>
                  <?php $cek_menukaban = (new \ComboHelper())->cekmenukaban(); ?>
                    @if($cek_menukaban->s_id_statusmenukaban == 1)
                
                  <li>
                     <div class="content">
                        <h3>Validasi Kaban</h3>
                        <p>Setelah kebenaran formulir pendaftaran telah lengkap dan diverifikasi kabid, selanjutnya
                           tahap proses validasi oleh kepala badan.</p>
                     </div>
                     <div class="time">
                        <h4>Langkah 4</h4>
                     </div>
                  </li>
                  <li>
                     <div class="content">
                        <h3>Pembayaran</h3>
                        <p>Setelah semua proses validasi sudah terpenuhi, maka bisa dilakukan pembayaran ketempat
                           pembayaran yang telah ditunjuk. </p>
                     </div>
                     <div class="time">
                        <h4>Langkah 5</h4>
                     </div>
                  </li>
                  @else
                  <li>
                     <div class="content">
                        <h3>Pembayaran</h3>
                        <p>Setelah semua proses validasi sudah terpenuhi, maka bisa dilakukan pembayaran ketempat
                           pembayaran yang telah ditunjuk. </p>
                     </div>
                     <div class="time">
                        <h4>Langkah 4</h4>
                     </div>
                  </li>
                  @endif

                  <div style="clear:both;"></div>
               </ul>
            </div>
         </div>
      </section>

      <section class="pricing-one" id="payments">
         <div class="container">
            <div class="block-title text-center">
               <p class="block-title__tag-line">Pembayaran
               </p>
               <h2 class="block-title__title">Metode Pembayaran</h2>
            </div>
            <div class="row">
               <div class="col-lg-4">
                  <div class="pricing-one__single color-1 wow fadeInDown" data-wow-duration="1500ms">
                     <div class="pricing-one__top">
                        <h3 class="pricing-one__amount"> Bank Transfer </h3>
                     </div>
                     <ul class="list-unstyled pricing-one__list">
                        <li><img src="{{ asset('dist/img/bpd-bengkulu.jpg') }}" alt="" width="160"></li>
                        <li><img src="{{ asset('dist/img/briva.png') }}" alt="" width="100"></li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="pricing-one__single color-2 wow fadeInUp" data-wow-duration="1500ms">
                     <div class="pricing-one__top">
                        <h3 class="pricing-one__amount"> Online Payments </h3>
                     </div>
                     <ul class="list-unstyled pricing-one__list">
                        <li><img src="{{ asset('dist/img/briva.png') }}" alt="" width="120"></li>
                        <li>&emsp;</li>
                     </ul>
                  </div>
               </div>
               <div class="col-lg-4">
                  <div class="pricing-one__single color-3 wow fadeInDown" data-wow-duration="1500ms">
                     <div class="pricing-one__top">
                        <h3 class="pricing-one__amount"> Market Payments </h3>
                     </div>
                     <ul class="list-unstyled pricing-one__list">
                        <li>Cooming Soon</li>
                        <li></li>
                     </ul>
                  </div>
               </div>
            </div>
         </div>
      </section>

      <section class="faq thm-gray-bg" id="faqs">
         <div class="container">
            <div class="block-title text-center">
               <p class="block-title__tag-line">FAQ
               </p>
               <h2 class="block-title__title">Seputar BPHTB</h2>
            </div>
            <ul class="faq-list">
               <li data-aos="fade-up" data-aos-delay="100" class="aos-init aos-animate">
                  <a data-toggle="collapse" class="collapsed" href="#faq1" aria-expanded="false">Apa itu BPHTB? <i
                        class="fas fa-arrow-up"></i></a>
                  <div id="faq1" class="collapse" data-parent=".faq-list" style="">
                     <p>
                        Merujuk Pasal 1 No 41-43 UU 28/2009, Bea Perolehan Hak atas Tanah dan Bangunan adalah pajak atas
                        perolehan hak atas tanah dan/atau bangunan. Bea Perolehan Hak atas Tanah dan Bangunan adalah
                        pajak atas perolehan hak atas tanah dan/atau bangunan. Perolehan Hak atas Tanah dan/atau
                        Bangunan adalah perbuatan atau peristiwa hukum yang mengakibatkan diperolehnya hak atas tanah
                        dan/atau bangunan oleh orang pribadi atau Badan. Hak atas Tanah dan/atau Bangunan adalah hak
                        atas tanah, termasuk hak pengelolaan, beserta bangunan di atasnya, sebagaimana dimaksud dalam
                        undang-undang di bidang pertanahan dan bangunan.

                     </p>
                  </div>
               </li>

               <li data-aos="fade-up" data-aos-delay="200" class="aos-init aos-animate">
                  <a data-toggle="collapse" href="#faq2" class="collapsed">Berapa tarif BPHTB? <i
                        class="fas fa-arrow-up"></i></a>
                  <div id="faq2" class="collapse" data-parent=".faq-list">
                     <p>
                        Merujuk Pasal 88 no 1-2 UU 28/2009, Tarif Bea Perolehan Hak atas Tanah dan Bangunan ditetapkan
                        paling tinggi sebesar 5% (lima persen). Tarif Bea Perolehan Hak atas Tanah dan Bangunan
                        ditetapkan dengan Peraturan Daerah.
                     </p>
                  </div>
               </li>

               <li data-aos="fade-up" data-aos-delay="300" class="aos-init aos-animate">
                  <a data-toggle="collapse" href="#faq3" class="collapsed">BPHTB kewajiban siapa? <i
                        class="fas fa-arrow-up"></i></a>
                  <div id="faq3" class="collapse" data-parent=".faq-list">
                     <p>
                        Merujuk Pasal 86 no 1 UU 28/2009, Subjek Pajak Bea Perolehan Hak atas Tanah dan Bangunan adalah
                        orang pribadi atau Badan yang memperoleh Hak atas Tanah dan/atau Bangunan.
                     </p>
                  </div>
               </li>
            </ul>
         </div>
      </section>

      <footer class="site-footer" id="contacts">
         <div class="site-footer__bubble-1"></div>
         <div class="site-footer__line"></div>
         <div class="site-footer__bubble-2"></div>
         <div class="site-footer__upper">
            <div class="container">
               <div class="row">
                  <div class="col-lg-6 d-flex justify-content-between footer-widget__links-wrap">
                     <div class="footer-widget">
                        <h3 class="footer-widget__title"><i class="fa fa-bank"></i> {{ 'Pemerintah '.$pemda->s_namakabkot }}</h3>
                        <p class="footer-widget__contact"><a>{{ $pemda->s_namainstansi.' ('.$pemda->s_namasingkatinstansi.')' }}</a>
                           <p class="footer-widget__contact"><a>{{ $pemda->s_alamatinstansi }} <br> {{ $pemda->s_namakabkot .', '.$pemda->s_namaprov }}<br> {{'No. Telp '.$pemda->s_notelinstansi }}</a></p>
                     </div>
                  </div>
<!--                  <div class="col-lg-4 d-flex justify-content-between footer-widget__links-wrap">
                     <div class="footer-widget">
                        <h3 class="footer-widget__title">Alamat</h3>
                        <p class="footer-widget__contact"><a>Kantor {{ $pemda->s_namasingkatinstansi }}</a>
                           <p class="footer-widget__contact"><a>{{ $pemda->s_alamatinstansi }}</a></p>
                     </div>
                  </div>-->
                  <div class="col-lg-6" style="display: block;">
                     <div class="footer-widget footer-widget__mailchimp">
                        <h3 class="footer-widget__title"><i class="fa fa-map"></i> Lokasi Kantor</h3>
                        <div class="mc-form__response">
                                <div id="mapkantor"></div>
                              <!-- <iframe src="https://maps.google.com/maps?q=Badan%20Keuangan%20Daerah%20Kabupaten%20Kepahiang&amp;t=m&amp;z=10&amp;output=embed&amp;iwloc=near"  width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen=""
                              aria-hidden="false" tabindex="0" title="Badan Keuangan Daerah Kabupaten Kepahiang" aria-label="Badan Keuangan Daerah Kabupaten Kepahiang"></iframe> -->
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
         <div class="site-footer__bottom">
            <div class="container">
               <div class="inner-container">
                  <p class="site-footer__copy">&copy; copyright 2021 by <a href="#">PT. Mitra Prima Utama</a></p>
               </div>
            </div>
         </div>
      </footer>
   </div>

   <a href="#" data-target="html" class="scroll-to-target scroll-to-top"><i class="fa fa-angle-up"></i></a>

   <script src="{{ asset('depan/assets/js/jquery.min.js') }}"></script>
   <script src="{{ asset('depan/assets/js/bootstrap.bundle.min.js') }}"></script>
   <script src="{{ asset('depan/assets/js/owl.carousel.min.js') }}"></script>
   <script src="{{ asset('depan/assets/js/waypoints.min.js') }}"></script>
   <script src="{{ asset('depan/assets/js/jquery.counterup.min.js') }}"></script>
   <script src="{{ asset('depan/assets/js/TweenMax.min.js') }}"></script>
   <script src="{{ asset('depan/assets/js/wow.js') }}"></script>
   <script src="{{ asset('depan/assets/js/jquery.magnific-popup.min.js') }}"></script>
   <script src="{{ asset('depan/assets/js/jquery.ajaxchimp.min.js') }}"></script>
   <script src="{{ asset('depan/assets/js/swiper.min.js') }}"></script>
   <script src="{{ asset('depan/assets/js/jquery.easing.min.js') }}"></script>

   <!-- template scripts -->
   <script src="{{ asset('depan/assets/js/theme.js') }}"></script>
   
<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&callback=initMap&libraries=&v=weekly"
defer></script>
   

<script>
    var cekLok = false;
    @php
        $cekLokPhp = false;
    @endphp
    //geolokasi
    var view = document.getElementById("tampilkan");
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            view.innerHTML = "Yah browsernya ngga support Geolocation bro!";
        }
    }
    function showPosition(position) {
        this.cekLok = true;
        @php
            $cekLokPhp = true;
        @endphp
        var latnya = position.coords.latitude;
        var longnya = position.coords.longitude;
        //$('#s_longitude').val(longnya);
        //$('#s_latitude').val(latnya);
        console.log(longnya);
        console.log(latnya);
        initMap();
    }
    //end geolokasi sadsadsadsadasda

    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-Token': '{{ csrf_token() }}'}
        });
        
        initMap();
    });
    
    function initMap3() {
    var input = document.getElementById('searchMapInput');
  
    var autocomplete = new google.maps.places.Autocomplete(input);
   
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        document.getElementById('location-snap').innerHTML = place.formatted_address;
        document.getElementById('lat-span').innerHTML = place.geometry.location.lat();
        document.getElementById('lon-span').innerHTML = place.geometry.location.lng();
        
        //$('#s_latitude_pemda').val(place.geometry.location.lat());
        //$('#s_longitude_pemda').val(place.geometry.location.lng());
        
        initMap2();
    });
    
    initMap2();
}
    
    function initMap() {

    @if (!empty($dataspt->s_latitude) && $cekLokPhp == false)
            var latnya = {{ $dataspt->s_latitude }};
    @else
           
                var latnya = {{$pemda->s_latitude_pemda}}; //-7.575488699999999; //
            
    @endif

    @if (!empty($dataspt->s_longitude) && $cekLokPhp == false)
            var longnya = {{ $dataspt->s_longitude }};
    @else
        
                var longnya = {{$pemda->s_longitude_pemda}}; //110.8243272; //
            
    @endif

        const map = new google.maps.Map(document.getElementById("mapkantor"), {
            zoom: 13,
            center: {lat: latnya, lng: longnya},
        });
        marker = new google.maps.Marker({
            map,
            draggable: false,
            animation: google.maps.Animation.DROP,
            position: {lat: latnya, lng: longnya},
        });
        //$('#s_latitude_pemda').val(latnya);
        //$('#s_longitude_pemda').val(longnya);
        marker.addListener("click", toggleBounce);
        google.maps.event.addListener(marker, 'dragend', function (marker) {
            var latLng = marker.latLng;
            //$('#s_latitude_pemda').val(latLng.lat());
            //$('#s_longitude_pemda').val(latLng.lng());
        });
        this.cekLok = false;
        @php
            $cekLokPhp = false;
        @endphp
    }

    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }


</script>


</body>

</html>
