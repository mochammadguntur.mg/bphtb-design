<table width="100%">
    <tr>
        <td rowspan="3">
            <img src="{{ asset('storage/'. $pemda->s_logo) }}" alt="" width="60px">
        </td>
        <td style="font-size: 16px; font-weight: 800; text-transform: uppercase; text-align: center" colspan="9">{{ $pemda->s_namakabkot }}</td>
    </tr>
    <tr>
        <td style="font-size: 16px; font-weight: 800; text-transform: uppercase; text-align: center" colspan="9">{{ $pemda->s_namainstansi }} ({{ $pemda->s_namasingkatinstansi }})</td>
    </tr>
    <tr>
        <td style="font-size: 10px; font-weight: 800; text-transform: uppercase; font-style: italic; text-align: center" colspan="9">{{ $pemda->s_alamatinstansi }}, {{ $pemda->s_namaibukotakabkot }}, {{ $pemda->s_namakabkot }}, {{ $pemda->s_namaprov }} {{ $pemda->s_kodepos }} Telp. {{ $pemda->s_notelinstansi }}</td>
    </tr>
</table>
<hr style="border-top: medium double #000;">