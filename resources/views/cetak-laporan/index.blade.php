@extends('layouts.master')

@section('judulkiri')
    CETAK LAPORAN
@endsection

@section('content')

    <div class="row">
        <div class="col-md-6">
            <div class="card card-outline card-primary shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">LAPORAN PENERIMAAN BPHTB</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label col-form-label-sm">Tanggal Cetak</label>
                        <div class="col-md-4">
                            <div class="input-group date" id="tgl_cetak_penerimaan" data-target-input="nearest">
                                <input type="text" name="cetak_tgl_penerimaan" id="cetak_tgl_penerimaan"
                                    class="form-control form-control-sm datetimepicker-input"
                                    data-target="#tgl_cetak_penerimaan" />
                                <div class="input-group-append" data-target="#tgl_cetak_penerimaan"
                                    data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Tanggal Penerimaan</label>
                        <div class="col-md-6">
                            <div class="row pl-2">
                                <input type="text" class="form-control form-control-sm col-md-5" name="tgl_penerimaan_awal"
                                    id="tgl_penerimaan_awal">
                                <span class="col-md-2"> s/d </span>
                                <input type="text" class="form-control form-control-sm col-md-5" name="tgl_penerimaan_akhir"
                                    id="tgl_penerimaan_akhir">
                            </div>
                            <div class="text-danger" id="tgl_penerimaan_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Jenis Transaksi</label>
                        <div class="col-md-7">
                            <select name="jenis_transaksi_penerimaan" id="jenis_transaksi_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Notaris/PPAT</label>
                        <div class="col-md-7">
                            <select name="notaris_ppat_penerimaan" id="notaris_ppat_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Kecamatan</label>
                        <div class="col-md-7">
                            <select name="kecamatan_penerimaan" id="kecamatan_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Kelurahan</label>
                        <div class="col-md-7">
                            <select name="kelurahan_penerimaan" id="kelurahan_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Megetahui</label>
                        <div class="col-md-7">
                            <select name="pejabat1_penerimaan" id="pejabat1_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                            <div class="text-danger" id="pejabat1_penerimaan_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Di Periksa oleh</label>
                        <div class="col-md-7">
                            <select name="pejabat2_penerimaan" id="pejabat2_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <button name="btnCetakPenerimaanPDF" id="btnCetakPenerimaanPDF" class="btn btn-primary">
                        <i class="fas fa-file-pdf"></i> PDF</button>
                    <button name="btnCetakPenerimaanExcel" id="btnCetakPenerimaanExcel" class="btn btn-primary">
                        <i class="fas fa-file-excel"></i> Excel</button>
                    <button name="btnCetakPenerimaanPDFv2" id="btnCetakPenerimaanPDFv2" class="btn btn-primary">
                        <i class="fas fa-file-pdf"></i> PDF V2</button>
                    <button name="btnCetakPenerimaanExcelv2" id="btnCetakPenerimaanExcelv2" class="btn btn-primary">
                        <i class="fas fa-file-excel"></i> Excel V2</button>        
                        
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card card-outline card-primary shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">LAPORAN REKAPITULASI PENERIMAAN TOTAL</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label col-form-label-sm">Tanggal Cetak</label>
                        <div class="col-md-4">
                            <div class="input-group date" id="tgl_cetak_rekap_penerimaan" data-target-input="nearest">
                                <input type="text" id="cetak_tgl_rekap_penerimaan"
                                    class="form-control form-control-sm datetimepicker-input"
                                    data-target="#tgl_cetak_rekap_penerimaan" />
                                <div class="input-group-append" data-target="#tgl_cetak_rekap_penerimaan"
                                    data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Tanggal Penerimaan</label>
                        <div class="col-md-6">
                            <div class="row pl-2">
                                <input type="text" class="form-control form-control-sm col-md-5" name="tgl_rekap_penerimaan_awal"
                                    id="tgl_rekap_penerimaan_awal">
                                <span class="col-md-2"> s/d </span>
                                <input type="text" class="form-control form-control-sm col-md-5" name="tgl_rekap_penerimaan_akhir"
                                    id="tgl_rekap_penerimaan_akhir">
                            </div>
                            <div class="text-danger" id="tgl_rekap_penerimaan_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Kecamatan</label>
                        <div class="col-md-7">
                            <select name="kecamatan_rekap_penerimaan" id="kecamatan_rekap_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Kelurahan</label>
                        <div class="col-md-7">
                            <select name="kelurahan_rekap_penerimaan" id="kelurahan_rekap_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Megetahui</label>
                        <div class="col-md-7">
                            <select name="pejabat1_rekap_penerimaan" id="pejabat1_rekap_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                            <div class="text-danger" id="pejabat1_rekap_penerimaan_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Di Periksa oleh</label>
                        <div class="col-md-7">
                            <select name="pejabat2_rekap_penerimaan" id="pejabat2_rekap_penerimaan"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <button name="btnCetakRekapitulasiPDF" id="btnCetakRekapitulasiPDF" class="btn btn-primary">
                        <i class="fas fa-file-pdf"></i> PDF</button>
                    <button name="btnCetakRekapitulasiXLS" id="btnCetakRekapitulasiXLS" class="btn btn-primary">
                        <i class="fas fa-file-excel"></i> Excel</button>
                        
                    
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card card-outline card-primary shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">LAPORAN PENDAFTARAN BPHTB</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label col-form-label-sm">Tanggal Cetak</label>
                        <div class="col-md-4">
                            <div class="input-group date" id="tgl_cetak_pendaftaran" data-target-input="nearest">
                                <input type="text" id="cetak_tgl_pendaftaran"
                                    class="form-control form-control-sm datetimepicker-input"
                                    data-target="#tgl_cetak_pendaftaran" required />
                                <div class="input-group-append" data-target="#tgl_cetak_pendaftaran"
                                    data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Tanggal Pendaftaran</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control form-control-sm" name="tgl_pendaftaran"
                                id="tgl_pendaftaran">
                            <div class="text-danger" id="tgl_pendaftaran_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Jenis Transaksi</label>
                        <div class="col-md-7">
                            <select name="jenis_transaksi_pendaftaran" id="jenis_transaksi_pendaftaran"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Notaris/PPAT</label>
                        <div class="col-md-7">
                            <select name="notaris_ppat_pendaftaran" id="notaris_ppat_pendaftaran"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Kecamatan</label>
                        <div class="col-md-7">
                            <select name="kecamatan_pendaftaran" id="kecamatan_pendaftaran"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Kelurahan</label>
                        <div class="col-md-7">
                            <select name="kelurahan_pendaftaran" id="kelurahan_pendaftaran"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Megetahui</label>
                        <div class="col-md-7">
                            <select name="pejabat1_pendaftaran" id="pejabat1_pendaftaran"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                            <div class="text-danger" id="pejabat1_pendaftaran_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Di Periksa oleh</label>
                        <div class="col-md-7">
                            <select name="pejabat2_pendaftaran" id="pejabat2_pendaftaran"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <button name="btnCetakPendaftaranPDF" id="btnCetakPendaftaranPDF" class="btn btn-primary">
                        <i class="fas fa-file-pdf"></i> PDF</button>
                    <button name="btnCetakPendaftaranExcel" id="btnCetakPendaftaranExcel" class="btn btn-primary">
                        <i class="fas fa-file-excel"></i> Excel</button>
                </div>
            </div>
        </div>

        <div class="col-md-6">
            <div class="card card-outline card-primary shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">LAPORAN SERTIFIKAT YANG SUDAH JADI</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label col-form-label-sm">Tanggal Cetak</label>
                        <div class="col-md-4">
                            <div class="input-group date" id="tgl_cetak_sertipikat" data-target-input="nearest">
                                <input type="text" id="cetak_tgl_sertipikat"
                                    class="form-control form-control-sm datetimepicker-input"
                                    data-target="#tgl_cetak_sertipikat" />
                                <div class="input-group-append" data-target="#tgl_cetak_sertipikat"
                                    data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Tanggal Akta</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control form-control-sm" name="tgl_sertipikat"
                                id="tgl_sertipikat">
                            <div class="text-danger" id="tgl_sertipikat_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Kecamatan</label>
                        <div class="col-md-7">
                            <select name="kecamatan_sertipikat" id="kecamatan_sertipikat"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Kelurahan</label>
                        <div class="col-md-7">
                            <select name="kelurahan_sertipikat" id="kelurahan_sertipikat"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Megetahui</label>
                        <div class="col-md-7">
                            <select name="pejabat1_sertipikat" id="pejabat1_sertipikat"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                            <div class="text-danger" id="pejabat1_sertipikat_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Di Periksa oleh</label>
                        <div class="col-md-7">
                            <select name="pejabat2_sertipikat" id="pejabat2_sertipikat"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <button name="btnCetakSertipikatPDF" id="btnCetakSertipikatPDF" class="btn btn-primary">
                        <i class="fas fa-file-pdf"></i> PDF</button>
                    <button name="btnCetakSertipikatXLS" id="btnCetakSertipikatXLS" class="btn btn-primary">
                        <i class="fas fa-file-excel"></i> Excel</button>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="card card-outline card-primary shadow-sm">
                <div class="card-header">
                    <h3 class="card-title">LAPORAN DATA AJB</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i>
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <div class="form-group row">
                        <label class="col-md-4 col-form-label col-form-label-sm">Tanggal Cetak</label>
                        <div class="col-md-4">
                            <div class="input-group date" id="tgl_cetak_ajb" data-target-input="nearest">
                                <input type="text" id="cetak_tgl_ajb"
                                    class="form-control form-control-sm datetimepicker-input"
                                    data-target="#tgl_cetak_ajb" />
                                <div class="input-group-append" data-target="#tgl_cetak_ajb" data-toggle="datetimepicker">
                                    <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Tanggal AJB</label>
                        <div class="col-md-5">
                            <input type="text" class="form-control form-control-sm" name="tgl_ajb" id="tgl_ajb">
                            <div class="text-danger" id="tgl_ajb_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Jenis Transaksi</label>
                        <div class="col-md-7">
                            <select name="jenis_transaksi_ajb" id="jenis_transaksi_ajb"
                                class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Notaris/PPAT</label>
                        <div class="col-md-7">
                            <select name="notaris_ppat_ajb" id="notaris_ppat_ajb" class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Kecamatan</label>
                        <div class="col-md-7">
                            <select name="kecamatan_ajb" id="kecamatan_ajb" class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Kelurahan</label>
                        <div class="col-md-7">
                            <select name="kelurahan_ajb" id="kelurahan_ajb" class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Megetahui</label>
                        <div class="col-md-7">
                            <select name="pejabat1_ajb" id="pejabat1_ajb" class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                            <div class="text-danger" id="pejabat1_ajb_error"></div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-md-4">Di Periksa oleh</label>
                        <div class="col-md-7">
                            <select name="pejabat2_ajb" id="pejabat2_ajb" class="custom-select custom-select-sm">
                                <option value="">Pilih</option>
                            </select>
                        </div>
                    </div>

                    <button name="btnCetakAjbPDF" id="btnCetakAjbPDF" class="btn btn-primary">
                        <i class="fas fa-file-pdf"></i> PDF</button>
                    <button name="btnCetakAjbXLS" id="btnCetakAjbXLS" class="btn btn-primary">
                        <i class="fas fa-file-excel"></i> Excel</button>
                </div>
            </div>
        </div>
    </div>




@endsection

@push('scripts')
    <script type="text/javascript">
        $(document).ready(function() {

            $('#tgl_cetak_penerimaan').datetimepicker({
                date: new Date(),
                format: 'DD-MM-YYYY',
            });

            $('#tgl_cetak_rekap_penerimaan').datetimepicker({
                date: new Date(),
                format: 'DD-MM-YYYY'
            });

            $('#tgl_cetak_pendaftaran').datetimepicker({
                date: new Date(),
                format: 'DD-MM-YYYY'
            });

            $('#tgl_cetak_sertipikat').datetimepicker({
                date: new Date(),
                format: 'DD-MM-YYYY'
            });

            $('#tgl_cetak_ajb').datetimepicker({
                date: new Date(),
                format: 'DD-MM-YYYY'
            });

            $('#tgl_rekap_penerimaan_awal').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                format: 'DD-MM-YYYY',
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#tgl_rekap_penerimaan_akhir').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                format: 'DD-MM-YYYY',
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#tgl_penerimaan_awal').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                format: 'DD-MM-YYYY',
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#tgl_penerimaan_akhir').daterangepicker({
                showDropdowns: true,
                singleDatePicker: true,
                format: 'DD-MM-YYYY',
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#tgl_pendaftaran').daterangepicker({
                autoUpdateInput: false,
                showDropdowns: true,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#tgl_sertipikat').daterangepicker({
                autoUpdateInput: false,
                showDropdowns: true,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            $('#tgl_ajb').daterangepicker({
                autoUpdateInput: false,
                showDropdowns: true,
                locale: {
                    cancelLabel: 'Clear'
                }
            });

            comboPejabat();
            comboNotaris();
            comboKecamatan();
            comboJenisTransaksi();
        });

        $('#tgl_penerimaan_awal').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#tgl_penerimaan_akhir').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY'));
        });

        $('#tgl_rekap_penerimaan').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        });

        $('#tgl_pendaftaran').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        });

        $('#tgl_sertipikat').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        });

        $('#tgl_ajb').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
        });

        const ppat = JSON.parse('{!!  $ppat !!}');
        const pejabat = JSON.parse('{!!  $pejabat !!}');
        const kecamatan = JSON.parse('{!!  $kecamatan !!}');
        const jenisTransaksi = JSON.parse('{!!  $jenis_transaksi !!}');

        function comboPejabat() {
            var option = '<option value="">Pilih</option>';
            pejabat.forEach(el => {
                option += `<option value="${ el.s_idpejabat }">${ el.s_namapejabat }</option>`;
            });

            $('#pejabat1_ajb').html(option);
            $('#pejabat2_ajb').html(option);
            $('#pejabat1_sertipikat').html(option);
            $('#pejabat2_sertipikat').html(option);
            $('#pejabat1_penerimaan').html(option);
            $('#pejabat2_penerimaan').html(option);
            $('#pejabat1_rekap_penerimaan').html(option);
            $('#pejabat2_rekap_penerimaan').html(option);
            $('#pejabat1_pendaftaran').html(option);
            $('#pejabat2_pendaftaran').html(option);
        }

        function comboJenisTransaksi() {
            var option = '<option value="">Pilih</option>';
            jenisTransaksi.forEach(el => {
                option += `<option value="${ el.s_idjenistransaksi }">${ el.s_namajenistransaksi }</option>`;
            });

            $('#jenis_transaksi_penerimaan').html(option);
            $('#jenis_transaksi_rekap_penerimaan').html(option);
            $('#jenis_transaksi_pendaftaran').html(option);
            $('#jenis_transaksi_ajb').html(option);
        }

        function comboNotaris() {
            var option = '<option value="">Pilih</option>';
            ppat.forEach(el => {
                option += `<option value="${ el.s_idnotaris }">${ el.s_namanotaris }</option>`;
            });

            $('#notaris_ppat_ajb').html(option);
            $('#notaris_ppat_penerimaan').html(option);
            $('#notaris_ppat_pendaftaran').html(option);
        }

        function comboKecamatan() {
            var option = '<option value="">Pilih</option>';
            kecamatan.forEach(el => {
                option += `<option value="${ el.s_idkecamatan }">${ el.s_namakecamatan }</option>`;
            });

            $('#kecamatan_ajb').html(option);
            $('#kecamatan_sertipikat').html(option);
            $('#kecamatan_penerimaan').html(option);
            $('#kecamatan_pendaftaran').html(option);
            $('#kecamatan_rekap_penerimaan').html(option);
        }

        $('#kecamatan_ajb, #kecamatan_sertipikat, #kecamatan_penerimaan, #kecamatan_pendaftaran, #kecamatan_rekap_penerimaan')
            .change(function(a) {
                let id = a.target.id.split("_");
                let sambung = '';
                if (id.length == 3) {
                    sambung = id[1] + "_" + id[2];
                } else {
                    sambung = id[1];
                }

                $.ajax({
                    url: `cetak-laporan/combo_kelurahan/${a.target.value}`,
                    type: 'GET',
                    data: {
                        id: a.target.value
                    }
                }).then(function(data) {
                    let option = '<option>Pilih</option>';

                    data.forEach(el => {
                        option +=
                            `<option value="${ el.s_idkelurahan }">${ el.s_namakelurahan }</option>`;
                    });

                    $('#kelurahan_' + sambung).html(option);
                });
            })


        $('#btnCetakPendaftaranPDF, #btnCetakPendaftaranExcel').on('click', (a) => {
            var tgl_cetak = $('#cetak_tgl_pendaftaran').val();
            var tgl_pendaftaran = $('#tgl_pendaftaran').val();
            var notaris = $('#notaris_ppat_pendaftaran').val();
            var jenis_peralihan = $('#jenis_transaksi_pendaftaran').val();
            var kecamatan_pilih = $('#kecamatan_pendaftaran  option:selected').text();
            var kelurahan_pilih = $('#kelurahan_pendaftaran  option:selected').text();
            var kecamatan = (kecamatan_pilih == "Pilih") ? '' : kecamatan_pilih;
            var kelurahan = (kelurahan_pilih == "Pilih") ? '' : kelurahan_pilih;
            var mengetahui = $('#pejabat1_pendaftaran').val();
            var diporses = $('#pejabat2_pendaftaran').val();

            if (tgl_pendaftaran == '') {
                $('#tgl_pendaftaran_error').text('Tanggal pendaftaran harus di isi!')
                return false;
            }
            
            if (mengetahui == '') {
                $('#pejabat1_pendaftaran_error').text('Silahkan Pilih Pejabat Mengetahui.')
                return false;
            }

            if (a.target.id === 'btnCetakPendaftaranPDF') {
                window.open(
                    `{{ url('cetak-laporan/pendaftaran_pdf') }}?tgl_pendaftaran=${ tgl_pendaftaran }&notaris=${ notaris }&jenis_peralihan=${ jenis_peralihan }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }`
                );
            } else {
                window.open(
                    `{{ url('cetak-laporan/pendaftaran_xls') }}?tgl_pendaftaran=${ tgl_pendaftaran }&notaris=${ notaris }&jenis_peralihan=${ jenis_peralihan }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }`
                );
            }
        });

        $('#btnCetakPenerimaanPDF, #btnCetakPenerimaanExcel,#btnCetakPenerimaanPDFv2,#btnCetakPenerimaanExcelv2').on('click', (a) => {
            var tgl_cetak = $('#cetak_tgl_penerimaan').val();
            var tgl_penerimaan_awal = $('#tgl_penerimaan_awal').val();
            var tgl_penerimaan_akhir = $('#tgl_penerimaan_akhir').val();
            var notaris = $('#notaris_ppat_penerimaan').val();
            var jenis_peralihan = $('#jenis_transaksi_penerimaan').val();
            var kecamatan_pilih = $('#kecamatan_penerimaan option:selected').text();
            var kelurahan_pilih = $('#kelurahan_penerimaan option:selected').text();
            var kecamatan = (kecamatan_pilih == "Pilih") ? '' : kecamatan_pilih;
            var kelurahan = (kelurahan_pilih == "Pilih") ? '' : kelurahan_pilih;
            var mengetahui = $('#pejabat1_penerimaan').val();
            var diporses = $('#pejabat2_penerimaan').val();

            if (tgl_penerimaan_awal == '') {
                $('#tgl_penerimaan_error').text('Tanggal penerimaan harus di isi!')
                return false;
            }
            
            if (mengetahui == '') {
                $('#pejabat1_penerimaan_error').text('Silahkan Pilih Pejabat Mengetahui!')
                return false;
            }

            if (a.target.id === 'btnCetakPenerimaanPDF') {
                window.open(
                    `{{ url('cetak-laporan/penerimaan_pdf') }}?tgl_penerimaan_awal=${ tgl_penerimaan_awal }&tgl_penerimaan_akhir=${ tgl_penerimaan_akhir }&notaris=${ notaris }&jenis_peralihan=${ jenis_peralihan }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }&tipecetak=1&jeniscetak=1`
                );
            }else if (a.target.id === 'btnCetakPenerimaanPDFv2') {
                window.open(
                    `{{ url('cetak-laporan/penerimaan_pdf') }}?tgl_penerimaan_awal=${ tgl_penerimaan_awal }&tgl_penerimaan_akhir=${ tgl_penerimaan_akhir }&notaris=${ notaris }&jenis_peralihan=${ jenis_peralihan }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }&tipecetak=2&jeniscetak=1`
                );
            }else if (a.target.id === 'btnCetakPenerimaanExcelv2') {
                window.open( //penerimaan_xls
                    `{{ url('cetak-laporan/penerimaan_pdf') }}?tgl_penerimaan_awal=${ tgl_penerimaan_awal }&tgl_penerimaan_akhir=${ tgl_penerimaan_akhir }&notaris=${ notaris }&jenis_peralihan=${ jenis_peralihan }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }&tipecetak=2&jeniscetak=2`
                );
            } else {
                window.open(
                    `{{ url('cetak-laporan/penerimaan_pdf') }}?tgl_penerimaan_awal=${ tgl_penerimaan_awal }&tgl_penerimaan_akhir=${ tgl_penerimaan_akhir }&notaris=${ notaris }&jenis_peralihan=${ jenis_peralihan }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }&tipecetak=1&jeniscetak=2`
                );

            }
        });

        $('#btnCetakSertipikatPDF, #btnCetakSertipikatXLS').on('click', (a) => {
            var tgl_cetak = $('#cetak_tgl_sertipikat').val();
            var tgl_sertipikat = $('#tgl_sertipikat').val();
            var kecamatan_pilih = $('#kecamatan_sertipikat option:selected').text();
            var kelurahan_pilih = $('#kelurahan_sertipikat option:selected').text();
            var kecamatan = (kecamatan_pilih == "Pilih") ? '' : kecamatan_pilih;
            var kelurahan = (kelurahan_pilih == "Pilih") ? '' : kelurahan_pilih;
            var mengetahui = $('#pejabat1_sertipikat').val();
            var diporses = $('#pejabat2_sertipikat').val();

            if (tgl_sertipikat == '') {
                $('#tgl_sertipikat_error').text('Tanggal proses harus di isi!')
                return false;
            }
            
            if (mengetahui == '') {
                $('#pejabat1_sertipikat_error').text('Silahkan Pilih Pejabat Mengetahui.')
                return false;
            }

            if (a.target.id === 'btnCetakSertipikatPDF') {
                window.open(
                    `{{ url('cetak-laporan/sertifikat_pdf') }}?tgl_sertipikat=${ tgl_sertipikat }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }`
                );
            } else {
                window.open(
                    `{{ url('cetak-laporan/sertifikat_xls') }}?tgl_sertipikat=${ tgl_sertipikat }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }`
                );
            }
        });

        $('#btnCetakAjbPDF, #btnCetakAjbXLS').on('click', (a) => {
            var tgl_cetak = $('#cetak_tgl_ajb').val();
            var tgl_penerimaan = $('#tgl_ajb').val();
            var notaris = $('#notaris_ppat_ajb').val();
            var jenis_peralihan = $('#jenis_transaksi_ajb').val();
            var kecamatan_pilih = $('#kecamatan_ajb option:selected').text();
            var kelurahan_pilih = $('#kelurahan_ajb option:selected').text();
            var kecamatan = (kecamatan_pilih == "Pilih") ? '' : kecamatan_pilih;
            var kelurahan = (kelurahan_pilih == "Pilih") ? '' : kelurahan_pilih;
            var mengetahui = $('#pejabat1_ajb').val();
            var diporses = $('#pejabat2_ajb').val();

            if (tgl_penerimaan == '') {
                $('#tgl_ajb_error').text('Tanggal AJB harus di isi!')
                return false;
            }
            
            if (mengetahui == '') {
                $('#pejabat1_ajb_error').text('Silahkan Pilih Pejabat Mengetahui.')
                return false;
            }

            if (a.target.id === 'btnCetakAjbPDF') {
                window.open(
                    `{{ url('cetak-laporan/ajb_pdf') }}?tgl_penerimaan=${ tgl_penerimaan }&notaris=${ notaris }&jenis_peralihan=${ jenis_peralihan }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }`
                );
            } else {
                window.open(
                    `{{ url('cetak-laporan/ajb_xls') }}?tgl_penerimaan=${ tgl_penerimaan }&notaris=${ notaris }&jenis_peralihan=${ jenis_peralihan }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }`
                );
            }
        });

        $('#btnCetakRekapitulasiPDF, #btnCetakRekapitulasiXLS').on('click', (a) => {
            var tgl_cetak = $('#tgl_cetak_rekap_penerimaan').val();
            var tgl_penerimaan_awal = $('#tgl_rekap_penerimaan_awal').val();
            var tgl_penerimaan_akhir = $('#tgl_rekap_penerimaan_akhir').val();
            var kecamatan_pilih = $('#kecamatan_rekap_penerimaan option:selected').text();
            var kelurahan_pilih = $('#kelurahan_rekap_penerimaan option:selected').text();
            var kecamatan = (kecamatan_pilih == "Pilih") ? '' : kecamatan_pilih;
            var kelurahan = (kelurahan_pilih == "Pilih") ? '' : kelurahan_pilih;
            var mengetahui = $('#pejabat1_rekap_penerimaan').val();
            var diporses = $('#pejabat2_rekap_penerimaan').val();

            if (tgl_rekap_penerimaan_awal == '') {
                $('#tgl_rekap_penerimaan_error').text('Tanggal penerimaan awal & akhir harus di isi!')
                return false;
            }
            
            if (mengetahui == '') {
                $('#pejabat1_rekap_penerimaan_error').text('Silahkan Pilih Pejabat Mengetahui.')
                return false;
            }

            if (a.target.id === 'btnCetakRekapitulasiPDF') {
                window.open(
                    `{{ url('cetak-laporan/rekap_penerimaan_pdf') }}?tgl_penerimaan_awal=${ tgl_penerimaan_awal }&tgl_penerimaan_akhir=${ tgl_penerimaan_akhir }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }`
                );
            }else{
                window.open(
                    `{{ url('cetak-laporan/rekap_penerimaan_xls') }}?tgl_penerimaan_awal=${ tgl_penerimaan_awal }&tgl_penerimaan_akhir=${ tgl_penerimaan_akhir }&kecamatan=${ kecamatan }&kelurahan=${ kelurahan }&mengetahui=${ mengetahui }&diproses=${ diporses }&tgl_cetak=${ tgl_cetak }`
                );
            }
        });

    </script>
@endpush
