{{-- @include('kop.kop-surat') --}}

<table width="100%" style="border-bottom: medium double #000;">
    <tr>
        <td style="width: 10%">
            <img src="{{ public_path() . '/storage/' . $pemda->s_logo }}" width="90px">
        </td>
        <td style="text-align:center;" colspan="10">
            <strong style="font-size: 14pt">PEMERINTAH {{ strtoupper($pemda->s_namakabkot) }}</strong><br>
            <strong style="font-size: 16pt">{{ strtoupper($pemda->s_namainstansi) }}</strong><br>
            <span style="font-size: 12pt">{{ $pemda->s_alamatinstansi .' Kode Pos '. $pemda->s_kodepos }}<br>No. Telp {{ $pemda->s_notelinstansi }}</span>
        </td>
        <td style="width: 10%"></td>
    </tr>
</table>


<table width="100%">
    <tr>
        <td style="width: 10%"></td>
        <td style="font-size: 16px; font-weight: bold; text-align: center;" colspan="10">LAPORAN PENDAFTARAN</td>
        <td style="width: 10%"></td>
    </tr>
    <tr>
        <td style="width: 10%"></td>
        <td style="text-align: center; padding-top: 10px; vertical-align: bottom; height: 15px;" colspan="10">PERIODE {{ Carbon\Carbon::parse($tgl_awal)->format('d-m-Y') }} s/d {{ Carbon\Carbon::parse($tgl_akhir)->format('d-m-Y') }}</td>
        <td style="width: 10%"></td>
    </tr>
</table>

<table style="border-collapse: collapse; margin-top: 20px; font-size: 11px;" width="100%">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center; font-weight: bold;">No</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center; font-weight: bold;">Nama WP/NIK</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center; font-weight: bold;">Alamat</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center; font-weight: bold;">Data Penjual</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center; font-weight: bold;">Data Objek</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center; font-weight: bold;">Jenis Transaksi</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center; font-weight: bold;">Nama Notaris/PPAT<br>Tanggal Pendaftaran</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center; font-weight: bold;">Luas Tanah<br>Luas Bangunan</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center; font-weight: bold;">Harga Transaksi/Nilai Pasar</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($pendaftaran as $key => $row)
        <tr>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: center; border-left: 1px solid #000; vertical-align:top;">{{ $key+1 }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{!! $row->t_nama_pembeli .'<br>'. $row->t_nik_pembeli !!}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{!! $row->t_jalan_pembeli !!}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; width: 150px;">{!! $row->t_nama_penjual .'<br>'. $row->t_namakec_penjual .'<br>'. $row->t_namakel_penjual .'<br>'. $row->t_kabkota_penjual !!}</td> {{-- {{ width: 150px; }} --}}
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{!! $row->t_nop_sppt .'-'. $row->t_tahun_sppt .'<br>'. $row->t_nama_sppt .'<br>'. $row->t_jalan_sppt !!}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{!! $row->s_namajenistransaksi !!}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{!! $row->s_namanotaris !!}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{!! $row->t_luastanah .'<br>'. $row->t_luasbangunan !!}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; text-align: right; vertical-align:top;">{!! number_format($row->t_npop_bphtb, 2, ',', '.') !!}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<table style="margin-top: 20px; font-size: 12px;" width="100%">
    <tr>
        <td style="text-align: center;" colspan="4"></td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="4">{{ Carbon\Carbon::parse($tgl_cetak)->isoFormat("dddd, D MMMM Y") }}</td>
    </tr>
    <tr>
        <td style="text-align: center;" colspan="4">Mengetahui</td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="4">Di Proses Oleh</td>
    </tr>
    <tr>
        <td style="text-align: center; text-transform: uppercase; text-decoration: underline; vertical-align: bottom;" colspan="4">{{ $mengetahui['s_namapejabat'] ?? '' }}</td>
        <td></td>
        <td style="text-align: center; text-transform: uppercase; text-decoration: underline; height: 100px; vertical-align: bottom;" colspan="4">{{ $diproses['s_namapejabat'] ?? '' }}</td>
    </tr>
    <tr>
        <td style="text-align: center;" colspan="4"> {{ $mengetahui['s_nippejabat'] }} </td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="4"> {{ $diproses['s_nippejabat'] ?? '' }}</td>
    </tr>
</table>
