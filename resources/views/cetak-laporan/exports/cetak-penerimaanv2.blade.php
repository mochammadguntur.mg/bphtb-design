<table width="100%" style="border-spacing: 0;">
    <tr>
        <td rowspan="3" style="border-bottom: medium double #000;">
            <img src="{{ public_path() . '/storage/' . $pemda->s_logo }}" style="width:55px; height: 70px">
        </td>
        <td style="font-size: 16px; font-weight: 800; text-transform: uppercase; text-align: center" colspan="18">{{ $pemda->s_namakabkot }}</td>
    </tr>
    <tr>
        <td style="font-size: 16px; font-weight: 800; text-transform: uppercase; text-align: center" colspan="18">{{ $pemda->s_namainstansi }} ({{ $pemda->s_namasingkatinstansi }})</td>
    </tr>
    <tr>
        <td style="border-bottom: medium double #000; font-size: 10px; font-weight: 800; text-transform: uppercase; font-style: italic; text-align: center" colspan="18">{{ $pemda->s_alamatinstansi }}, {{ $pemda->s_namaibukotakabkot }}, {{ $pemda->s_namakabkot }}, {{ $pemda->s_namaprov }} {{ $pemda->s_kodepos }} Telp. {{ $pemda->s_notelinstansi }}</td>
    </tr>
    <tr>
        <td style="font-size: 16px; font-weight: bold; text-align: center;" colspan="19">LAPORAN PENERIMAAN</td>
    </tr>
    <tr>
        <td style="text-align: center; padding-top: 10px; vertical-align: bottom; height: 15px;" colspan="19">PERIODE {{ Carbon\Carbon::parse($tgl_awal)->format('d-m-Y') }} s/d {{ Carbon\Carbon::parse($tgl_akhir)->format('d-m-Y') }}</td>
    </tr>
</table>


<table style="border-collapse: collapse; width: 100%;">
    <thead>
        <tr><th colspan="19" style="border-bottom: 1px solid #000;">&nbsp;</th></tr>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 10px;" rowspan="2">No</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 15px;" rowspan="2">NO DAFTAR</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 15px;" rowspan="2">PPAT</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 24px;" rowspan="2">NAMA PENJUAL/PEWARIS</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 24px;" rowspan="2">ALAMAT PENJUAL/PEWARIS</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 24px;" rowspan="2">NAMA PEMBELI/AHLI WARIS</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">ALAMAT PEMBELI/AHLI WARIS</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">NOMOR OBJEK PAJAK (NOP)</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">LETAK OBJEK PAJAK (TANAH DAN BANGUNAN)</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">SEKTOR</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" colspan="2">LUAS</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">NJOP PBB (Rp.)</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">HARGA TRANSAKSI (Rp.)</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">JENIS PEROLEHAN HAK</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">TGL SETOR BPHTB</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">JML SETORAN BPHTB (Rp.)</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">NAMA PENYETOR</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;" rowspan="2">KET</th>
        </tr>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 10px;">TANAH M<sup>2</sup></th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 15px;">BANGUNAN M<sup>2</sup></th>
        </tr>   
    </thead>
    @php
    $tot_penerimaan = 0;
    @endphp
    <tbody>
        @foreach ($penerimaan as $key => $row)
        <tr>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top; text-align: center;">{{ $key+1 }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $row->t_kohirspt }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $row->s_namanotaris }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->t_nama_penjual }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->t_jalan_penjual.', Kec.'.$row->t_namakec_penjual.', Kel.'.$row->t_namakel_penjual }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->t_nama_pembeli }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->t_jalan_pembeli.', Kec.'.$row->t_namakecamatan_pembeli.', Kel.'.$row->t_namakelurahan_pembeli }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->t_nop_sppt }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->t_jalan_sppt.', RT '.$row->t_rt_sppt.', RW '.$row->t_rw_sppt.', Kel. '.$row->t_kelurahan_sppt.', Kec. '.$row->t_kecamatan_sppt }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->nama_jenistanah }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->t_luastanah }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->t_luasbangunan }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ number_format($row->t_grandtotalnjop, 2, ',', '.') }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ number_format($row->t_nilaitransaksispt, 2, ',', '.') }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->s_namajenistransaksi }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">@if(!empty($row->t_tglpembayaran_pokok)) {{ date('d-m-Y', strtotime($row->t_tglpembayaran_pokok)) }} @endif</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ number_format($row->t_jmlhbayar_total, 2, ',', '.') }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $row->t_nama_penjual }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">&nbsp;</td>
           
            
             @php
            $tot_penerimaan += $row->t_jmlhbayar_total;
            @endphp
        </tr>
        @endforeach
        <tr>
            
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top; font-weight: 800; height: 20px; text-align: center;" colspan="16">TOTAL PENERIMAAN</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top; font-weight: 800; height: 20px; text-align: right">{{ number_format($tot_penerimaan, 2, ',', '.') }}</td>
             <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top; font-weight: 800; height: 20px; text-align: center;" colspan="2">&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="19"><br>&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="9"></td>
            <td style="text-align: center; vertical-align: bottom;" colspan="10">{{ Carbon\Carbon::parse($tgl_cetak)->isoFormat("dddd, D MMMM Y") }}</td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="9">Mengetahui<br><br><br><br><br><br>&nbsp;</td>
            <td style="text-align: center; vertical-align: bottom;" colspan="10">Di Proses Oleh<br><br><br><br><br><br>&nbsp;</td>
        </tr>
        <tr>
            <td style="text-align: center; text-transform: uppercase; text-decoration: underline;" colspan="9">{{ $mengetahui['s_namapejabat'] ?? '' }}</td>
            <td style="text-align: center; text-transform: uppercase; text-decoration: underline; vertical-align: bottom;" colspan="10">{{ $diproses['s_namapejabat'] ?? '' }}</td>
        </tr>
        <tr>
            <td style="text-align: center;" colspan="9"> {{ $mengetahui['s_nippejabat'] }} </td>
            <td style="text-align: center; vertical-align: bottom;" colspan="10"> {{ $diproses['s_nippejabat'] ?? '' }}</td>
        </tr>
    </tbody>
</table>

@php
//  die;
@endphp
