<table width="100%">
    <tr>
        <td rowspan="3">
        </td>
        <td style="font-size: 16px; font-weight: 800; text-transform: uppercase; text-align: center" colspan="9">{{ $pemda->s_namakabkot }}</td>
    </tr>
    <tr>
        <td style="font-size: 16px; font-weight: 800; text-transform: uppercase; text-align: center" colspan="9">{{ $pemda->s_namainstansi }} ({{ $pemda->s_namasingkatinstansi }})</td>
    </tr>
    <tr>
        <td style="font-size: 10px; font-weight: 800; text-transform: uppercase; font-style: italic; text-align: center" colspan="9">{{ $pemda->s_alamatinstansi }}, {{ $pemda->s_namaibukotakabkot }}, {{ $pemda->s_namakabkot }}, {{ $pemda->s_namaprov }} {{ $pemda->s_kodepos }} Telp. {{ $pemda->s_notelinstansi }}</td>
    </tr>
</table>
<hr style="border-top: medium double #000;">

<table>
    <tr>
        <td style="font-size: 16px; font-weight: bold; text-align: center;" colspan="11">DATA SERTIFIKAT YANG SUDAH JADI</td>
    </tr>
    <tr>
        <td style="text-align: center; padding-top: 10px; vertical-align: bottom; height: 15px;" colspan="11">PERIODE {{ Carbon\Carbon::parse($tgl_awal)->format('d-m-Y') }} s/d {{ Carbon\Carbon::parse($tgl_akhir)->format('d-m-Y') }}</td>
    </tr>
</table>

<table style="border-collapse: collapse;">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 10px;">No</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 15px;">No Akta</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 14px;">Tanggal Akta</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 30px;">Nama PPAT</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; ">NPWP</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 20px;">Nama Wajib Pajak</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 25px;">NOP</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 26px;">NIB</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 20px;">NTPD</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 25px;">Jenis Hak</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 14px;">Tanggal Transaksi</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($sertifikat as $key => $row)
        <tr>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top;">{{ $key+1 }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top;">{{ $row->t_noakta }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top;">{{ Carbon\Carbon::parse($row->t_tglakta)->format('d-m-Y') }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top;">{{ $row->t_namappat }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top;">{{ $row->t_npwp }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top;">{{ $row->t_namawp }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top; text-align: center;">{{ $row->t_nop }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top; text-align: center;">{{ $row->t_nib }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top; text-align: center;">{{ $row->t_ntpd }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top; text-align: center;">{{ $row->t_jenishak }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top;">{{ Carbon\Carbon::parse($row->t_tgltransaksi)->format('d-m-Y') }}</td>
        </tr>
        @endforeach
    </tbody>
</table>

<table>
    <tr>
        <td style="text-align: center;" colspan="5"></td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="5">{{ Carbon\Carbon::parse($tgl_cetak)->isoFormat("dddd, D MMMM Y") }}</td>
    </tr>
    <tr>
        <td style="text-align: center;" colspan="5">Mengetahui</td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="5">Di Proses Oleh</td>
    </tr>
    <tr>
        <td style="text-align: center; text-transform: uppercase; text-decoration: underline;" colspan="5">{{ $mengetahui['s_namapejabat'] ?? '' }}</td>
        <td></td>
        <td style="text-align: center; text-transform: uppercase; text-decoration: underline; height: 100px; vertical-align: bottom;" colspan="5">{{ $diproses['s_namapejabat'] ?? '' }}</td>
    </tr>
    <tr>
        <td style="text-align: center;" colspan="5"> {{ $mengetahui['s_nippejabat'] }} </td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="5"> {{ $diproses['s_nippejabat'] ?? '' }}</td>
    </tr>
</table>