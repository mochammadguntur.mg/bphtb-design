@include('kop.kop-surat')
<table width="100%">
    <tr>
        <td style="font-size: 16px; font-weight: bold; text-align: center;" colspan="9">LAPORAN DATA AJB</td>
    </tr>
    <tr>
        <td style="text-align: center; padding-top: 10px; vertical-align: bottom; height: 15px;" colspan="9">PERIODE {{ Carbon\Carbon::parse($tgl_awal)->format('d-m-Y') }} s/d {{ Carbon\Carbon::parse($tgl_akhir)->format('d-m-Y') }}</td>
    </tr>
</table>

<table style="border-collapse: collapse;">
    <thead>
        <tr>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 8px">No </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">No Daftar
            </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 15px">Tgl Daftar
            </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 28px">Jenis
                Transaksi </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 25px">Nama Wajib
                Pajak </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 25px">NOP</th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 19px">No AJB Baru
            </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 15px">Tgl AJB Baru
            </th>
            <th scope='col' style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 15px">Status Lapor
            </th>
        </tr>
    </thead>
    <tbody>
        @foreach ($ajb as $index => $v)
            @php
                $hasLapor = $v->laporBulanans()->where('t_idajb', $v['t_idajb'])->exists();
            @endphp
        <tr>
            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">
                {{ $index + 1 }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ $v->spt->t_kohirspt }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;text-align: center;">
                {{ Carbon\Carbon::parse($v->spt->t_tgldaftar_spt)->format('d-m-Y') }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ $v->spt->jenisTransaksi->s_namajenistransaksi }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ $v->spt->t_nama_pembeli }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ $v->spt->t_nop_sppt }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ $v->t_no_ajb }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">
                {{ Carbon\Carbon::parse($v->tgl_ajb)->format('d-m-Y') }}
            </td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">
                {{ ($hasLapor) ? 'Sudah' : 'Belum' }}
            </td>
        </tr>
        @endforeach
    </tbody>
</table>

<table>
    <tr>
        <td style="text-align: center;" colspan="4"></td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="4">{{ Carbon\Carbon::parse($tgl_cetak)->isoFormat("dddd, D MMMM Y") }}</td>
    </tr>
    <tr>
        <td style="text-align: center;" colspan="4">Mengetahui</td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="4">Di Proses Oleh</td>
    </tr>
    <tr>
        <td style="text-align: center; text-transform: uppercase; text-decoration: underline;" colspan="4">{{ $mengetahui['s_namapejabat'] ?? '' }}</td>
        <td></td>
        <td style="text-align: center; text-transform: uppercase; text-decoration: underline; height: 100px; vertical-align: bottom;" colspan="4">{{ $diproses['s_namapejabat'] ?? '' }}</td>
    </tr>
    <tr>
        <td style="text-align: center;" colspan="4"> {{ $mengetahui['s_nippejabat'] }} </td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="4"> {{ $diproses['s_nippejabat'] ?? '' }}</td>
    </tr>
</table>