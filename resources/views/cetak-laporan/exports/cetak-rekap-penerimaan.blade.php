<table width="100%">
    <tr>
        <td style="width: 10%"><img src="{{ public_path() . '/storage/' . $pemda->s_logo }}" width="60px"></td>
        <td style="text-align:center;" colspan="4"><strong style="font-size: 14pt">PEMERINTAH {{ strtoupper($pemda->s_namakabkot) }}</strong><br>
            <strong style="font-size: 16pt">{{ strtoupper($pemda->s_namainstansi) }}</strong><br>
            <span style="font-size: 12pt">{{ $pemda->s_alamatinstansi .' Kode Pos '. $pemda->s_kodepos }}<br>No. Telp {{ $pemda->s_notelinstansi }}</span></td>
        <td style="width: 10%"></td>
    </tr>
</table>
<hr style="border-top: medium double #000;">

<table width="100%">
    <tr>
        <td style="font-size: 16px; font-weight: bold; text-align: center;" colspan="5">LAPORAN REKAPITULASI TOTAL PENERIMAAN</td>
    </tr>
    <tr>
        <td style="text-align: center; padding-top: 10px; vertical-align: bottom; height: 15px;" colspan="5">PERIODE {{ Carbon\Carbon::parse($tgl_awal)->format('d-m-Y') }} s/d {{ Carbon\Carbon::parse($tgl_akhir)->format('d-m-Y') }}</td>
    </tr>
    <tr>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
        <td></td>
    </tr>
</table>

<table style="border-collapse: collapse; margin-top: 10px;">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 10px;">No</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 70px;" colspan="3">Jenis Transaksi</th>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold; width: 20px;">Total Penerimaan <br> (Rp.)</th>
        </tr>
    </thead>
    @php
    $tot_penerimaan = 0;
    @endphp
    <tbody>
        @foreach ($penerimaan as $key => $row)
        <tr>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; border-left: 1px solid #000; vertical-align:top; text-align: center;">{{ $key+1 }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;" colspan="3">{{ $row['s_namajenistransaksi'] }}</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: right">{{ number_format($row['total_penerimaan'], 2, ',', '.')  }}</td>
            @php
            $tot_penerimaan += $row['total_penerimaan'];
            @endphp
        </tr>
        @endforeach
        <tr>
            <td style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align: auto; text-align: center; font-weight: 800;" colspan="4">TOTAL PENERIMAAN</td>
            <td style="padding: 5px; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align: auto; font-weight: 800; text-align: right">{{ number_format($tot_penerimaan, 2, ',', '.') }}</td>
        </tr>
    </tbody>
</table>

<table style="margin-top: 15px;" width="100%">
    <tr>
        <td style="text-align: center;" colspan="2"></td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="2">{{ Carbon\Carbon::parse($tgl_cetak)->isoFormat("dddd, D MMMM Y") }}</td>
    </tr>
    <tr>
        <td style="text-align: center;" colspan="2">Mengetahui</td>
        <td></td>
        <td style="text-align: center;" colspan="2">Di Proses Oleh</td>
    </tr>
    <tr>
        <td style="text-align: center; text-transform: uppercase; text-decoration: underline; vertical-align: bottom;" colspan="2">{{ $mengetahui['s_namapejabat'] ?? '' }}</td>
        <td></td>
        <td style="text-align: center; text-transform: uppercase; text-decoration: underline; height: 100px; vertical-align: bottom;" colspan="2">{{ $diproses['s_namapejabat'] ?? '' }}</td>
    </tr>
    <tr>
        <td style="text-align: center;" colspan="2"> {{ $mengetahui['s_nippejabat'] }} </td>
        <td></td>
        <td style="text-align: center; vertical-align: bottom;" colspan="2"> {{ $diproses['s_nippejabat'] ?? '' }} </td>
    </tr>
</table>
