@extends('layouts.master')

@section('judulkiri')
PENGATURAN PERSENTASE WAJAR
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        @if (session('success'))
            <div class="alert alert-success alert-dismissible callout callout-success" id="alert">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-check"></i> Berhasil!</h5>
              {{ session('success') }}
            </div>
        @endif
        @if (session('error'))
            <div class="alert alert-danger alert-dismissible callout callout-danger" id="alert">
              <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
              <h5><i class="icon fas fa-ban"></i> Gagal!</h5>
              {{ session('error') }}
            </div>
        @endif
        <div class="card card-outline card-info">
            <div class="card-header">
                <h3 class="card-title">
                    <a href="setting-persentasewajar/create" class="btn btn-primary">
                    <i class="fas fa-plus"></i> Tambah
                    </a>
                </h3>
                <div class="card-tools">
                    <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                        <i class="fas fa-file-excel"></i> Excel
                    </a>
                    <a href="javascript:void(0)" class="btn btn-danger"  id="btnPDF">
                        <i class="fas fa-file-pdf"></i> PDF
                    </a>
                </div>
            </div>
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                
                                <th style="background-color: #227dc7; color:white;" class="text-center">MIN %</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">MAX %</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">KETERANGAN</th>
                                
                                <th style="background-color: #227dc7; color:white;" class="text-center">WARNA CSS</th>
                                
                                <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                                
                            </tr>
                            <tr>
                                <th></th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-s_nilaimin">
                                </th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-s_nilaimax">
                                </th>
                                
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-s_ketpresentase_wajar">
                                </th>
                                <th>
                                    <input type="text" class="form-control form-control-sm" id="filter-s_css_warna">
                                </th>
                                
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="7"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">MIN %</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="kodeHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">MAX %</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="namaHapus" readonly>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-4">KETERANGAN</div>
                    <div class="col-sm-8">
                        <input type="text" class="form-control form-control-sm" id="ketHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>


@endsection
@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
        url: 'setting-persentasewajar/datagrid',
        table: "#datagrid-table",
        columns: [
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "s_nilaimin"
            },
            {
                sortable: true,
                name: "s_nilaimax"
            },
            {
                sortable: true,
                name: "s_ketpresentase_wajar"
            },
            {
                sortable: true,
                name: "s_css_warna"
            },
            
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]

    });

    $("#filter-s_nilaimin, #filter-s_nilaimax, #filter-s_ketpresentase_wajar, #filter-s_css_warna").keyup(function() {
        search();
    });

    
    function search() {
        datatables.setFilters({
            s_nilaimin: $("#filter-s_nilaimin").val(),
            s_nilaimax: $("#filter-s_nilaimax").val(),
            s_ketpresentase_wajar: $("#filter-s_ketpresentase_wajar").val(),
            s_css_warna: $("#filter-s_css_warna").val()
        });
        datatables.reload();
    }
    search();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'setting-persentasewajar/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].s_idpresentase_wajar);
            $("#kodeHapus").val(data[0].s_nilaimin);
            $("#namaHapus").val(data[0].s_nilaimax);
            $("#ketHapus").val(data[0].s_ketpresentase_wajar);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'setting-persentasewajar/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Berhasil dihapus!')
            datatables.reload();
        });
    });

    $("#btnExcel, #btnPDF").click(function(a) {
        var s_nilaimin = $("#filter-s_nilaimin").val();
        var s_nilaimax = $("#filter-s_nilaimax").val();
        
        
        if(a.target.id == 'btnExcel'){
            window.open('{{ url('setting-persentasewajar/export_xls') }}?s_nilaimin=' + s_nilaimin + '&s_nilaimax=' + s_nilaimax );
        }else{
            window.open('{{ url('setting-persentasewajar/export_pdf') }}?s_nilaimin=' + s_nilaimin + '&s_nilaimax=' + s_nilaimax );
        }

    });

    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").slideUp(500);
    });

</script>
@endpush
