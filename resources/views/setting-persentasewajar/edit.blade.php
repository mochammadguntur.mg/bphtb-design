@extends('layouts.master')

@section('judulkiri')
    PENGATURAN PERSENTASE WAJAR [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="{{ url('setting-persentasewajar/' . $persentasewajar->s_idpresentase_wajar . '/edit') }}"
                    method="POST">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-persentasewajar.partials.form')
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
