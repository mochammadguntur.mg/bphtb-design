<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">MIN %</label>
    <div class="col-md-4">
        <input type="text" name="s_nilaimin" class="form-control @error('s_nilaimin') is-invalid @enderror" id="s_nilaimin" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="MIN %"
               value="{{ old('s_nilaimin') ?? $persentasewajar->s_nilaimin }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_nilaimin'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_nilaimin') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">MAX %</label>
    <div class="col-md-4">
        <input type="text" name="s_nilaimax" class="form-control @error('s_nilaimax') is-invalid @enderror" id="s_nilaimax" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" placeholder="MAX %"
               value="{{ old('s_nilaimax') ?? $persentasewajar->s_nilaimax }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_nilaimax'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_nilaimax') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Keterangan</label>
    <div class="col-md-4">
        <input type="text" name="s_ketpresentase_wajar" class="form-control" id="s_ketpresentase_wajar" placeholder="KETERANGAN"
               value="{{ old('s_ketpresentase_wajar') ?? $persentasewajar->s_ketpresentase_wajar }}">
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Warna CSS</label>
    <div class="col-md-4">
        <input type="text" name="s_css_warna" class="form-control my-colorpicker1" id="s_css_warna" placeholder="WARNA CSS"
               value="{{ old('s_css_warna') ?? $persentasewajar->s_css_warna }}">
    </div>
</div>


<a href="/setting-persentasewajar" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>



@push('scriptsbawah')
<script type="text/javascript">
    //Colorpicker
    $('.my-colorpicker1').colorpicker()

</script>
@endpush