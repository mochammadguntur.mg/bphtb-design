<?php error_reporting(0) ?>
<table style="border-collapse: collapse; width:100%">
    <thead>
        <tr>
            <th style="border-left: 1px solid #000; border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">No</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">No SKPDKB</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Tgl Penetapan</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Nama WP</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">NOP</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Jmlh Pajak (Rp)</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Kode Bayar</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Status Bayar</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Tgl Bayar</th>
            <th style="border-top: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center; font-weight: bold">Created at</th>
        </tr>
    </thead>
    <tbody>
        @foreach($skpdkb as $index => $rows)
        @php
            $status = (empty($rows->t_tglpembayaran_skpdkb)) ? 'Belum Bayar' : 'Lunas';
        @endphp
        <tr>
            <td style="border-left: 1px solid #000; border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top; text-align: center;">{{ $index + 1 }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->t_nourut_kodebayar }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->t_tglpenetapan }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->t_nama_pembeli }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->t_nop_sppt }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->t_jmlh_totalskpdkb }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->t_kodebayar_skpdkb }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $status }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->t_tglpembayaran_skpdkb }}</td>
            <td style="border-bottom: 1px solid #000; border-right: 1px solid #000; vertical-align:top;">{{ $rows->created_at }}</td>
        </tr>
        @endforeach
    </tbody>
</table>
