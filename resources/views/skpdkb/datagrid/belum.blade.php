<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                <thead>
                    <tr>
                        <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">No Daftar</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Nama WP</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">NOP</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Jmlh Pembayaran</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Tgl Pembayaran</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Kode Bayar</th>
                        <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-kohirspt_cari_0">
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-nama_pembeli_cari_0">
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-nop_cari_0">
                        </th>
                        <th></th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-tglpembayaran_cari_0" readonly>
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-kodebayar_cari_0">
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="16"> Tidak ada data.</td>
                    </tr>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="card-footer clearfix pagination-footer"></div>
    </div>
</div>
