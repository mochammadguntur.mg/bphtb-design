<div class="row">
    <div class="col-12">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover table-sm" id="datagridsudah-table">
                <thead>
                    <tr>
                        <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">No Daftar</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">No SKPDKB</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Tgl SKPDKB</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Nama WP</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">NOP</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Jmlh Pajak (Rp)</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Kode Bayar</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Status Bayar</th>
                        <th style="background-color: #227dc7; color:white;" class="text-center">Tgl Bayar</th>
                        <th style="background-color: #227dc7; color:white; width:100px" class="text-center">Aksi</th>
                    </tr>
                    <tr>
                        <th></th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-kohirspt_cari_1">
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-noskpdkb_cari_1">
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-tglskpdkb_cari_1" readonly>
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-nama_pembeli_cari_1">
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-nop_cari_1">
                        </th>
                        <th></th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-kodebayar_cari_1">
                        </th>
                        <th>
                            <select class="form-control form-control-sm" id="filter-status_cari_1">
                                <option value="">All</option>
                                <option value="1">Lunas</option>
                                <option value="2">Belum Bayar</option>
                            </select>
                        </th>
                        <th>
                            <input type="text" class="form-control form-control-sm" id="filter-tglbayar_cari_1" readonly>
                        </th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="16"> Tidak ada data.</td>
                    </tr>
                </tbody>
                <tfoot>
                </tfoot>
            </table>
        </div>
        <div class="card-footer clearfix pagination-footer"></div>
    </div>
</div>
