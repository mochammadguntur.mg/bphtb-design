@extends('layouts.master')

@section('judulkiri')
PENETAPAN SKPDKB (KURANG BAYAR)
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header bg-primary">
                    <h3 class="card-title">FORM PENETAPAN SKPDKB (KURANG BAYAR)</h3>
                    <div class="card-tools">
                        <a href="{{ url('skpdkb') }}" class="btn btn-sm btn-warning">
                            <i class="fas fa-backward"></i> Kembali
                        </a>
                    </div>
                </div>
                <form action="{{ url('skpdkb/store') }}" method="POST" class="form-horizontal">
                    <div class="card-body">
                        @csrf
                        @include('skpdkb.partials.view')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title"><i class="fas fa-edit"></i> FORM PENETAPAN SKPDKB (KURANG BAYAR)</h3>
                                    </div>
                                    <div class="card-body">
                                        @include('skpdkb.partials.form', ['sumbit'=> 'Simpan'])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script type="text/javascript">
    function hitungpajakskpdkb() {
        var t_idjenistransaksi = $('#t_idjenistransaksi').val();
        var t_luastanah = unformatCurrency($('#t_luastanah').val());
        var t_njoptanah = unformatCurrency($('#t_njoptanah').val());

        var t_luastanahnya = (t_luastanah > 0) ? t_luastanah : 0;
        var t_njoptanahnya = (t_njoptanah > 0) ? t_njoptanah : 0;

        var t_totalnjoptanah = Math.round(t_luastanahnya * t_njoptanahnya);

        $('#t_totalnjoptanah').val(t_totalnjoptanah);
        document.getElementById('t_totalnjoptanah').value = formatCurrency(document.getElementById('t_totalnjoptanah').value);

        var t_luasbangunan = unformatCurrency($('#t_luasbangunan').val());
        var t_njopbangunan = unformatCurrency($('#t_njopbangunan').val());

        var t_luasbangunannya = (t_luasbangunan > 0) ? t_luasbangunan : 0;
        var t_njopbangunannya = (t_njopbangunan > 0) ? t_njopbangunan : 0;

        var t_totalnjopbangunan = Math.round(t_luasbangunannya * t_njopbangunannya);

        $('#t_totalnjopbangunan').val(t_totalnjopbangunan);
        document.getElementById('t_totalnjopbangunan').value = formatCurrency(document.getElementById('t_totalnjopbangunan').value);

        var t_grandtotalnjop = t_totalnjoptanah + t_totalnjopbangunan;
        $('#t_grandtotalnjop').val(t_grandtotalnjop);
        document.getElementById('t_grandtotalnjop').value = formatCurrency(document.getElementById('t_grandtotalnjop').value);

        var t_nilaitransaksispt = unformatCurrency($('#t_nilaitransaksispt').val());

        if(t_idjenistransaksi == 8){
            var npop = t_nilaitransaksispt;
        }else{
            var npop = (t_nilaitransaksispt > t_grandtotalnjop) ? t_nilaitransaksispt : t_grandtotalnjop;
        }

        $('#t_npop_bphtb').val(npop);
        document.getElementById('t_npop_bphtb').value = formatCurrency(document.getElementById('t_npop_bphtb').value);

        var t_npoptkp_bphtb = unformatCurrency($('#t_npoptkp_bphtb').val());
        var npoptkp = (t_npoptkp_bphtb > 0) ? t_npoptkp_bphtb : 0;

        var t_npopkp = npop - npoptkp;
        var npopkp = (t_npopkp > 0) ? t_npopkp : 0;
        $('#t_npopkp_bphtb').val(npopkp);
        document.getElementById('t_npopkp_bphtb').value = formatCurrency(document.getElementById('t_npopkp_bphtb').value);

        var terhutang = npopkp * $('#t_persenbphtb').val() / 100;
        $('#t_nilai_bphtb_fix').val(terhutang);
        document.getElementById('t_nilai_bphtb_fix').value = formatCurrency(document.getElementById('t_nilai_bphtb_fix').value);

        var skpdkb = terhutang - unformatCurrency($('#t_jmlhbayar_pokok').val());
        $('#t_jmlhpajakskpdkb').val(skpdkb);
        document.getElementById('t_jmlhpajakskpdkb').value = formatCurrency(document.getElementById('t_jmlhpajakskpdkb').value);

    }

</script>
@endpush
