@extends('layouts.master')

@section('judulkiri')
SKPDKB (KURANG BAYAR)
@endsection

@section('content')

<div class="row">
    <div class="col-md-12">
        <div class="card card-primary card-outline card-outline-tabs">
            <div class="card-header p-0 border-bottom-0">
                <ul class="nav nav-tabs" id="custom-tabs-three-tab" role="tablist">
                    <li class="nav-item">
                    <a class="nav-link active" id="custom-tabs-three-home-tab" data-toggle="pill" href="#custom-tabs-three-home" role="tab" aria-controls="custom-tabs-three-home" aria-selected="true">Belum Ditetapkan</a>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" id="custom-tabs-three-profile-tab" data-toggle="pill" href="#custom-tabs-three-profile" role="tab" aria-controls="custom-tabs-three-profile" aria-selected="false">Sudah Ditetapkan</a>
                    </li>
                </ul>
            </div>
            <div class="card-body">
                <div class="tab-content" id="custom-tabs-three-tabContent">
                    <div class="tab-pane fade show active" id="custom-tabs-three-home" role="tabpanel" aria-labelledby="custom-tabs-three-home-tab">
                        @include('skpdkb.datagrid.belum')
                    </div>
                    <div class="tab-pane fade" id="custom-tabs-three-profile" role="tabpanel" aria-labelledby="custom-tabs-three-profile-tab">
                        @include('skpdkb.datagrid.sudah')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-delete">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header bg-danger">
                <h6 class="modal-title"><span class="fas fa-trash"></span> &nbsp;HAPUS DATA ?</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Apakah anda yakin ingin menghapus data sebagai berikut?</p>
                <div class="row">
                    <div class="col-sm-4">No SKPDKB</div>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control form-control-sm" id="idHapus" readonly>
                        <input type="text" class="form-control form-control-sm" id="noskpdkbHapus" readonly>
                    </div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal"><span
                        class="fas fa-times-circle"></span> Tutup</button>
                <button type="button" class="btn btn-danger btn-sm" id="btnHapus"><span class="fas fa-trash"></span>
                    Hapus</button>
            </div>
        </div>
    </div>
</div>


@endsection
@push('scripts')

<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {

        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif

        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif

        $('#filter-tglskpdkb_cari_1').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglskpdkb_cari_1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tglskpdkb_cari_1').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-tglsbayar_cari_1').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglsbayar_cari_1').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tglsbayar_cari_1').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-tglpembayaran_cari_0').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglpembayaran_cari_0').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tglpembayaran_cari_0').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

    });

    var datatables = datagrid({
    url: 'skpdkb/datagrid_ketetapan',
    table: "#datagrid-table",
    columns: [
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-right"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: true,name: "t_kohirspt"},
            {sortable: true,name: "t_p_nama_pembeli"},
            {sortable: true,name: "t_nop_sppt"},
            {sortable: false},
            {sortable: true,name: "t_tglpembayaran_pokok"},
            {sortable: true,name: "t_kodebayar"},
            {sortable: false},
        ],
    action: [{
            name: 'tetapkan',
            btnClass: 'btn btn-warning btn-sm',
            btnText: '<i class="fas fa-check"> Tetapkan</i>'
        }
    ]

    });

    $("#filter-kohirspt_cari_0, #filter-nama_pembeli_cari_0, #filter-nop_cari_0, filter-kodebayar_cari_0").keyup(function() {
        search();
    });

    $("#filter-tglpembayaran_cari_0").change(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            tglpembayaran: $("#filter-tglpembayaran_cari_0").val(),
            kohirspt: $("#filter-kohirspt_cari_0").val(),
            nama_pembeli: $("#filter-nama_pembeli_cari_0").val(),
            nop: $("#filter-nop_cari_0").val(),
            kodebayar: $("#filter-kodebayar_cari_0").val()
        });
        datatables.reload();
    }
    search();

    var datatables1 = datagrid({
        url: 'skpdkb/datagrid',
        table: "#datagridsudah-table",
        columns: [
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
            {class: "text-center"},
        ],
        orders: [
            {sortable: false},
            {sortable: true,name: "t_kohirspt"},
            {sortable: true,name: "t_nourut_kodebayar"},
            {sortable: true,name: "t_tglpenetapan"},
            {sortable: true,name: "t_nama_pembeli"},
            {sortable: true,name: "t_nop_sppt"},
            {sortable: false},
            {sortable: true,name: "t_kodebayar_skpdkb"},
            {sortable: false},
            {sortable: true,name: "t_tglpembayaran_skpdkb"},
            {sortable: false},
        ],
        action: [{
                name: 'print',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-print" title="Cetak SKPDKB"></i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash" title="Hapus"></i>'
            }
        ]

    });

    $("#filter-kohirspt_cari_1, #filter-noskpdkb_cari_1, #filter-nop_cari_1, #filter-nama_pembeli_cari_1, #filter-kodebayar_cari_1").keyup(function() {
        search_1();
    });

    $("#filter-status_cari_1, #filter-tglskpdkb_cari_1, #filter-tglbayar_cari_1").change(function() {
        search_1();
    });

    function search_1() {
        datatables1.setFilters({
            kohirspt: $("#filter-kohirspt_cari_1").val(),
            noskpdkb: $("#filter-noskpdkb_cari_1").val(),
            tglskpdkb: $("#filter-tglskpdkb_cari_1").val(),
            nop: $("#filter-nop_cari_1").val(),
            nama_pembeli: $("#filter-nama_pembeli_cari_1").val(),
            kodebayar: $("#filter-kodebayar_cari_1").val(),
            status: $("#filter-status_cari_1").val(),
            tglbayar: $("#filter-tglbayar_cari_1").val()
        });
        datatables1.reload();
    }
    search_1();

    function showDeleteDialog(a) {
        $.ajax({
            url: 'skpdkb/detail/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data[0].t_idskpdkb);
            $("#noskpdkbHapus").val(data[0].t_nourut_kodebayar);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();
        $.ajax({
            url: 'skpdkb/delete/{' + id + '}',
            method: "GET",
            data: {
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            toastr.success('Data Berhasil dihapus!')
            datatables.reload();
        });
    });

    function printSk(a){
        window.open('{{ url("skpdkb/cetaksurat") }}/' + a);
    }

    $("#btnExcel, #btnPDF").click(function(a) {
        var noskpdkb = $("#filter-noskpdkb").val();
        var tglskpdkb = $("#filter-tglskpdkb").val();
        var nop = $("#filter-nop").val();
        var nama_pembeli = $("#filter-nama_pembeli").val();
        var kodebayar = $("#filter-kodebayar").val();
        var status = $("#filter-status").val();
        var tglbayar = $("#filter-tglbayar").val();
        var created_at = $("#filter-created_at").val();
        if(a.target.id == 'btnExcel'){
            window.open('/skpdkb/export_xls?noskpdkb=' + noskpdkb
                + '&tglskpdkb=' + tglskpdkb
                + '&nop=' + nop
                + '&nama_pembeli=' + nama_pembeli
                + '&kodebayar=' + kodebayar
                + '&status=' + status
                + '&tglbayar=' + tglbayar
                + '&created_at=' + created_at);
        }else{
            window.open('/skpdkb/export_pdf?noskpdkb=' + noskpdkb
                + '&tglskpdkb=' + tglskpdkb
                + '&nop=' + nop
                + '&nama_pembeli=' + nama_pembeli
                + '&kodebayar=' + kodebayar
                + '&status=' + status
                + '&tglbayar=' + tglbayar
                + '&created_at=' + created_at);
        }
    });

    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").slideUp(500);
    });

</script>
@endpush
