@extends('layouts.master')

@section('judulkiri')
PENGATURAN PEMDA
@endsection

@section('content')

<?php $cek_setgmap = (new \ComboHelper())->cek_setgmap(); ?>
@if($cek_setgmap->s_id_statusmenugmap == 1)
<style>
    #map {
        width: 100%;
        height: 400px;
    }
</style>
@endif

<div class="card card-outline card-info shadow-sm">
    <div class="card-body">
        <form action="{{ route('setting-pemda.createOrUpdate') }}" method="POST" enctype="multipart/form-data">
            @csrf

            <input type="hidden" class="form-control" name="s_idpemda" id="s_idpemda" value="{{ optional($pemda)->s_idpemda }}">
            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Nama Provinsi</label>
                    <input type="text" class="form-control" name="s_namaprov" id="s_namaprov" value="{{ old('s_namaprov', optional($pemda)->s_namaprov) }}" readonly>
                    @if ($errors->has('s_namaprov'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('s_namaprov') }}
                    </div>
                    @endif
                </div>
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Kode Provinsi</label>
                    <input type="text" class="form-control col-lg-2 col-sm-2" name="s_kodeprovinsi" id="s_kodeprovinsi" onkeypress="return numberOnly(event)" value="{{ old('s_kodeprovinsi', optional($pemda)->s_kodeprovinsi) }}" maxlength="2" readonly>
                    @if ($errors->has('s_kodeprovinsi'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('s_kodeprovinsi') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Nama Kabupaten/Kota</label>
                    <input type="text" class="form-control" name="s_namakabkot" id="s_namakabkot" value="{{ old('s_namakabkot', optional($pemda)->s_namakabkot) }}" readonly>
                    @if ($errors->has('s_namakabkot'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('s_namakabkot') }}
                    </div>
                    @endif
                </div>
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Kode Kabupaten/Kota</label>
                    <input type="text" class="form-control col-lg-2 col-sm-2" name="s_kodekabkot" id="s_kodekabkot" onkeypress="return numberOnly(event)" value="{{ old('s_kodekabkot', optional($pemda)->s_kodekabkot) }}" maxlength="2" readonly>
                    @if ($errors->has('s_kodekabkot'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('s_kodekabkot') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Nama Ibukota Kabupaten/Kota </label>
                    <input type="text" class="form-control" name="s_namaibukotakabkot" id="s_namaibukotakabkot" value="{{ old('s_namaibukotakabkot', optional($pemda)->s_namaibukotakabkot) }}" readonly>
                    @if ($errors->has('s_namaibukotakabkot'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('s_namaibukotakabkot') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Nama Instansi </label>
                    <input type="text" class="form-control" name="s_namainstansi" id="s_namainstansi" value="{{ old('s_namainstansi', optional($pemda)->s_namainstansi) }}" readonly>
                    @if ($errors->has('s_namainstansi'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('s_namainstansi') }}
                    </div>
                    @endif
                </div>
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Nama Singkatan Instansi </label>
                    <input type="text" class="form-control col-6" name="s_namasingkatinstansi" id="s_namasingkatinstansi" value="{{ old('s_namasingkatinstansi', optional($pemda)->s_namasingkatinstansi) }}" readonly>
                    @if ($errors->has('s_namasingkatinstansi'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('s_namasingkatinstansi') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Alamat Kantor Instansi </label>
                    <textarea class="form-control" name="s_alamatinstansi" id="s_alamatinstansi" cols="30" rows="2" readonly>{{ old('s_alamatinstansi', optional($pemda)->s_alamatinstansi) }}</textarea>
                </div>
                <div class="form-group col-lg-6 col-sm-12">
                    <label>No. Telp. Kantor </label>
                    <input type="text" class="form-control col-6" name="s_notelinstansi" id="s_notelinstansi" value="{{ old('s_notelinstansi', optional($pemda)->s_notelinstansi) }}" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Kode Pos </label>
                    <input type="text" class="form-control col-4" name="s_kodepos" id="s_kodepos" onkeypress="return numberOnly(event)" value="{{ old('s_kodepos', optional($pemda)->s_kodepos) }}" maxlength="5" readonly>
                </div>
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Surel </label>
                    <input type="text" class="form-control col-6" name="s_email" id="s_email" value="{{ old('s_email', optional($pemda)->s_email) }}" readonly>
                    @if ($errors->has('s_email'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('s_email') }}
                    </div>
                    @endif
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Nama Bank </label>
                    <input type="text" class="form-control" name="s_namabank" id="s_namabank" value="{{ old('s_namabank', optional($pemda)->s_namabank) }}" readonly>
                </div>
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Kode Rekening Bank </label>
                    <input type="text" class="form-control col-6" name="s_norekbank" id="s_norekbank" onkeypress="return numberOnly(event)" value="{{ old('s_norekbank', optional($pemda)->s_norekbank) }}" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Nama Cabang </label>
                    <input type="text" class="form-control" name="s_namacabang" id="s_namacabang" value="{{ old('s_namacabang', optional($pemda)->s_namacabang) }}" readonly>
                </div>
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Kode Cabang </label>
                    <input type="text" class="form-control col-6" name="s_kodecabang" id="s_kodecabang" value="{{ old('s_kodecabang', optional($pemda)->s_kodecabang) }}" readonly>
                </div>
            </div>

            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>URL eBPHTB </label>
                    <input type="text" class="form-control" name="s_urlbphtb" id="s_urlbphtb" value="{{ old('s_urlbphtb', optional($pemda)->s_urlbphtb) }}" readonly>
                </div>
            </div>

            <div class="row" id="upload_row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Logo Kabupaten/Kota </label>
                    <div class="input-group">
                        <div class="custom-file">
                            <input type="file" name="s_logo" id="s_logo" value="{{ old('s_logo') }}" class="custom-file-input form-control @error('s_logo') is-invalid @enderror">
                            <label class="custom-file-label" for="s_filettd22">Choose file</label>
                        </div>
                        <div class="input-group-append">
                            <span class="input-group-text" id="">Upload</span>
                        </div>
                    </div>
                    <!-- <input type="file" class="form-control col-6" name="s_logo" id="s_logo" value="{{ old('s_logo') }}"> -->
                    @if ($errors->has('s_logo'))
                    <div class="text-danger mt-2">
                        {{ $errors->first('s_logo') }}
                    </div>
                    @else
                    <div class="text-info mt-2">
                        Ukuran maximal file 1MB.
                    </div>
                    @endif
                </div>
            </div>
            

            <div class="d-flex justify-content-center">
            <img src="{{asset('storage/'.$pemda->s_logo )}}" id="preview" class="img-fluid" alt="logo-pemda" style="width: 100px; height: 120px;">
            </div>

            @if($cek_setgmap->s_id_statusmenugmap == 1)
            <div class="row">
                <div class="form-group col-lg-6 col-sm-12">
                    <label>Pencarian Lokasi Peta </label>
                    <input id="searchMapInput" class="mapControls form-control" type="text" placeholder="Isi Lokasi" readonly>
                        <ul id="geoData">
                            <li>Alamat Lengkap: <span id="location-snap"></span></li>
                            <li>Latitude: <span id="lat-span"></span></li>
                            <li>Longitude: <span id="lon-span"></span></li>
                        </ul>
                </div>
            </div>   
            @endif
            <div class="row">
                <div class="form-group col-lg-4 col-sm-12">
                    <label>Latitude </label>
                    <input type="text" class="form-control col-6" name="s_latitude_pemda" id="s_latitude_pemda" value="{{ old('s_latitude_pemda', optional($pemda)->s_latitude_pemda) }}" readonly>
                </div>
                <div class="form-group col-lg-4 col-sm-12">
                    <label>Longitude </label>
                    <input type="text" class="form-control col-6" name="s_longitude_pemda" id="s_longitude_pemda" value="{{ old('s_longitude_pemda', optional($pemda)->s_longitude_pemda) }}" readonly>
                </div>
                @if($cek_setgmap->s_id_statusmenugmap == 1)
                <div class="form-group col-lg-4 col-sm-12">
                    <br>
                    <button type="button" class="btn btn-success" onclick="initMap2()">Cek Lokasi  <i class="fas fa-map-pin"></i></button> <!-- getLocation()-->
                </div>
                @endif
            </div>
            
            <div class="col-md-12">
                <div id="map"></div>
            </div>

    </div>
    <div class="card-footer">
        <button type="submit" class="btn btn-primary btn-sm float-right" id="submit">
            <i class="fas fa-save"></i>
            Simpan
        </button>
        <a href="{{ url('setting-pemda') }}" class="btn btn-primary btn-sm float-left" id="batalBtn">
            <i class="fas fa-redo"></i>
            Batal
        </a>

        <button type="button" class="btn btn-warning btn-sm float-right" id="editBtn">
            <i class="fas fa-edit"></i>
            Edit
        </button>

        </form>
    </div>
</div>
@endsection

@push('scripts')


@if($cek_setgmap->s_id_statusmenugmap == 1)

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDyq1l-IUYHhDWoWwF4HqTTVw-ixal-JzQ&libraries=places&callback=initMap" async defer></script>
  

<script>
    var cekLok = false;
    @php
        $cekLokPhp = false;
    @endphp
    //geolokasi
    var view = document.getElementById("tampilkan");
    function getLocation() {
        if (navigator.geolocation) {
            navigator.geolocation.getCurrentPosition(showPosition);
        } else {
            view.innerHTML = "Yah browsernya ngga support Geolocation bro!";
        }
    }
    function showPosition(position) {
        this.cekLok = true;
        @php
            $cekLokPhp = true;
        @endphp
        var latnya = position.coords.latitude;
        var longnya = position.coords.longitude;
        $('#s_longitude').val(longnya);
        $('#s_latitude').val(latnya);
        console.log(longnya);
        console.log(latnya);
        initMap();
    }
    //end geolokasi sadsadsadsadasda

    $(function () {
        $.ajaxSetup({
            headers: {'X-CSRF-Token': '{{ csrf_token() }}'}
        });
    });
    
    function initMap() {
    var input = document.getElementById('searchMapInput');
  
    var autocomplete = new google.maps.places.Autocomplete(input);
   
    autocomplete.addListener('place_changed', function() {
        var place = autocomplete.getPlace();
        document.getElementById('location-snap').innerHTML = place.formatted_address;
        document.getElementById('lat-span').innerHTML = place.geometry.location.lat();
        document.getElementById('lon-span').innerHTML = place.geometry.location.lng();
        
        $('#s_latitude_pemda').val(place.geometry.location.lat());
        $('#s_longitude_pemda').val(place.geometry.location.lng());
        
        initMap2();
    });
    
    initMap2();
}
    
    function initMap2() {

    @if (!empty($dataspt->s_latitude) && $cekLokPhp == false)
            var latnya = {{ $dataspt->s_latitude }};
    @else
            if ($('#s_latitude_pemda').val() != '' || this.cekLok == true) {
                var latnya = parseFloat($('#s_latitude_pemda').val());
            } else {
                var latnya = -7.575488699999999; //{{$pemda->s_latitude_pemda}}; //
            }
    @endif

    @if (!empty($dataspt->s_longitude) && $cekLokPhp == false)
            var longnya = {{ $dataspt->s_longitude }};
    @else
        if ($('#s_longitude_pemda').val() != '' || this.cekLok == true) {
                var longnya = parseFloat($('#s_longitude_pemda').val());
            } else {
                var longnya = 110.8243272; //{{$pemda->s_longitude_pemda}}; //
            }
    @endif

        const map = new google.maps.Map(document.getElementById("map"), {
            zoom: 13,
            center: {lat: latnya, lng: longnya},
        });
        marker = new google.maps.Marker({
            map,
            draggable: true,
            animation: google.maps.Animation.DROP,
            position: {lat: latnya, lng: longnya},
        });
        $('#s_latitude_pemda').val(latnya);
        $('#s_longitude_pemda').val(longnya);
        marker.addListener("click", toggleBounce);
        google.maps.event.addListener(marker, 'dragend', function (marker) {
            var latLng = marker.latLng;
            $('#s_latitude_pemda').val(latLng.lat());
            $('#s_longitude_pemda').val(latLng.lng());
        });
        this.cekLok = false;
        @php
            $cekLokPhp = false;
        @endphp
    }

    function toggleBounce() {
        if (marker.getAnimation() !== null) {
            marker.setAnimation(null);
        } else {
            marker.setAnimation(google.maps.Animation.BOUNCE);
        }
    }


</script>

@endif


<script type="text/javascript">
    $(document).ready(() => {
        @if (session('success'))
            toastr.success('{{session('success')}}')
        @endif

        @if (session('error'))
            toastr.error('{{session('error')}}')
        @endif

        $('#upload_row').css('display', 'none');
        $('#submit').css('display', 'none');
        $('#batalBtn').css('display', 'none');

        @if (count($errors) > 0)
            $('#upload_row').css('display', 'block');
            $('#submit').css('display', 'block');
            $('#batalBtn').css('display', 'block');
            $('#editBtn').css('display', 'none');
            $('input').removeAttr('readonly');
            $('textarea').removeAttr('readonly');
        @endif
    });

    $(document).on("click", "#s_logo", () => {
        var file = $(this).parents().find("#file");
        file.trigger("click");
    });

    $('input[type="file"]').change((e) => {
        var input = document.querySelector('input[type=file]');
        var file = input.files[0];

        var reader = new FileReader();
        reader.onload = (e) => {
            document.getElementById("preview").src = e.target.result;
        };

        reader.readAsDataURL(file);
    });

    $(".alert").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert").slideUp(500);
    });

    $('#editBtn').on("click", () => {

        $('#upload_row').css('display', 'block');
        $('#submit').css('display', 'block');
        $('#batalBtn').css('display', 'block');
        $('#editBtn').css('display', 'none');
        $('input').removeAttr('readonly');
        $('textarea').removeAttr('readonly');
    })

    function numberOnly(e) {
          var input = (e.which) ? e.which : e.keyCode

          if (input > 31 && (input < 48 || input > 57))
              return false;
          return true;
      }

</script>
@endpush
