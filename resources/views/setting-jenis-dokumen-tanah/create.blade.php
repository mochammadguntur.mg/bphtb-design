@extends('layouts.master')

@section('judulkiri')
PENGATURAN JENIS DOKUMEN TANAH [TAMBAH]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="{{ url('setting-jenis-dokumen-tanah/store') }}" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @csrf
                        @include('setting-jenis-dokumen-tanah.partials.form', ['sumbit'=> 'Simpan'])
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
