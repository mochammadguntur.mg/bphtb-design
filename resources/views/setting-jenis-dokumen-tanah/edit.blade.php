@extends('layouts.master')

@section('judulkiri')
PENGATURAN JENIS DOKUMEN TANAH [EDIT]
@endsection
@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="/setting-jenis-dokumen-tanah/{{ $jenis_dokumen_tanah->s_iddoktanah }}/edit" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-jenis-dokumen-tanah.partials.form')
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
