<div class="col-sm-12 row mb-2">
    <label class="col-sm-3">Nama Jenis Dokumen Tanah</label>
    <div class="col-md-4">
        <input type="text" name="s_namadoktanah" class="form-control @error('s_namadoktanah') is-invalid @enderror" id="s_namadoktanah" placeholder="Nama Dokumen Tanah"
               value="{{ old('s_namadoktanah') ?? $jenis_dokumen_tanah->s_namadoktanah }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_namadoktanah'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_namadoktanah') }}
        </div>
        @endif
    </div>
</div>

<a href="/setting-jenis-dokumen-tanah" class="btn btn-danger" role="button" aria-pressed="true"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary  pull-right"><i class="fa fa-save"></i> {{ $sumbit ?? 'Update' }}</button>
