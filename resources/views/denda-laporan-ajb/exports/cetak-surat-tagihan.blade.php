<style type="text/css">
    body {
        font-family: arial
    }

    .border_atas {
        border-top: 1px solid black;
        border-collapse: collapse;
    }

    .border_kiri {
        border-left: 1px solid black;
        border-collapse: collapse;
    }

    .border_kanan {
        border-right: 1px solid black;
        border-collapse: collapse;
    }

    .border_bawah {
        border-bottom: 1px solid black;
        border-collapse: collapse;
    }

    .padding_lima {
        padding-left: 10px;
    }

    .font_italic {
        font-style: italic;
    }

    .font_besar {
        font-size: 12pt;
    }

    .font_kecil {
        font-size: 10pt;
    }

    .font_lima {
        font-size: 7pt;
    }

    .font_tujuh {
        font-size: 7pt;
    }

    .font_delapan {
        font-size: 8pt;
    }

    .font_sembilan {
        font-size: 9pt;
    }
</style>
<table class="border_atas border_bawah border_kanan border_kiri font_delapan" style="width: 100%">
    <tr>
        <td style="width: 10%;text-align:center;">
            <img src="{{ asset('storage/'. $pemda->s_logo) }}" style="width:50px;" />
        </td>
        <td class="border_kiri" style="width: 50%; text-align: center;">
            <span style="font-size: 12pt;">PEMERINTAH {{ strtoupper($pemda->s_namakabkot) }}</span>
            <br>
            <b style="font-size:12pt;">{{ strtoupper($pemda->s_namainstansi) }}</b>
            <br>
            <span style="font-size: 10pt">{{ $pemda->s_alamatinstansi .' Kode Pos '. $pemda->s_kodepos }}<br>No. Telp {{ $pemda->s_notelinstansi }}</span>
        </td>
        <td class="border_kiri" style="width: 20%; text-align:center;">
            &nbsp;
        </td>
    </tr>
</table>

<table class="border_bawah border_kanan border_kiri" style="width: 100%">
    <tr>
        <td style="width: 70%; text-align:center; font-size: 16px; font-weight: 800; padding: 0 20px;">
            SURAT PEMBERITAHUAN DENDA TIDAK/TERLAMBAT MELAPORKAN DATA AJB
        </td>
        <td class="border_kiri font_kecil" style="width: 30%; padding: 10px 0 10px 10px;">
            Kepada Yth:<br>
            {{ $data->s_namanotaris }} <br>
            PPAT/Notaris {{ strtoupper($pemda->s_namakabkot) }} <br>
            di <br>
            &nbsp; Tempat
        </td>
    </tr>
</table>
<table class="border_bawah border_kanan border_kiri" style="width: 100%">
    <tr>
        <td class="border_bawah font_kecil" style="padding: 5px 0 5px 5px ; font-weight: 600; vertical-align: auto;" colspan="2">
            DATA PELAPORAN :
        </td>
    </tr>
    <tr class="">
        <td class="padding_lima font_kecil" style="width: 18%;">
            No Pelaporan
        </td>
        <td class="font_kecil" style="width: 50%;">
            : {{ $data->t_no_lapor }}
        </td>
    </tr>
    <tr class="">
        <td class="padding_lima font_kecil" style="width: 18%;">
            Nama PPAT/Notaris
        </td>
        <td class="font_kecil" style="width: 50%;">
            : {{ $data->s_namanotaris }}
        </td>
    </tr>
    <tr class="">
        <td class="padding_lima font_kecil" style="width: 18%;">
            Laporan Akta untuk bulan
        </td>
        <td class="font_kecil" style="width: 50%;">
            : {{ date("F", mktime(0, 0, 0, $data->t_untuk_bulan, 10)) }} {{ $data->t_untuk_tahun }}
        </td>
    </tr>
    <tr class="">
        <td class="padding_lima font_kecil" style="width: 18%; padding-bottom: 10px;">
            Tanggal Lapor
        </td>
        <td class="font_kecil" style="width: 50%; padding-bottom: 10px;">
            : {{ Carbon\Carbon::parse($data->t_tgl_ajb)->format('d-m-Y') }}
        </td>
    </tr>
    <tr>
        <td class="border_atas border_bawah font_kecil" style="padding: 5px 0 5px 5px ; font-weight: 600; vertical-align: auto;" colspan="2">
            DENDA ADMINSITRASI :
        </td>
    </tr>
    <tr class="">
        <td class="padding_lima font_kecil" style="width: 18%;">
            Tanggal Penetapan Denda
        </td>
        <td class="font_kecil" style="width: 50%;">
            : {{ Carbon\Carbon::parse($data->t_tglpenetapan)->format('d-m-Y') }}
        </td>
    </tr>
    <tr class="">
        <td class="padding_lima font_kecil" style="width: 18%;">
            Nilai Denda
        </td>
        <td class="font_kecil" style="width: 50%;">
            : Rp. {{ convertCurrency($data->t_nilai_pendenda) }}
        </td>
    </tr>
    <tr class="">
        <td class="padding_lima font_kecil" style="width: 18%; padding-bottom: 10px;">
            Terbilang
        </td>
        <td class="font_kecil" style="width: 50%; padding-bottom: 10px;">
            : <span class="font_italic">{{ terbilang($data->t_nilai_pendenda) }} Rupiah</span>
        </td>
    </tr>
    <tr class="">
        <td class="border_atas padding_lima font_kecil" colspan="2" style="padding: 5px 0 10px 10px; ">
            Perhatian:<br>
            1. Denda ini diberikan berdasarkan UU No. 28 Tahun 2009 Pasal 93 ayat 2 (dua) tentang Bea Perolehan Hak atas Tanah dan Bangunan.<br>
            2. Harap penyetoran dilakukan melalui Kas Daerah/Bank Daerah.
        </td>
    </tr>
</table>
<table class="border_bawah border_kanan border_kiri" style="width: 100%; font-size: 12px;">
    <tr>
        <td style="width: 50%;">
            &nbsp;
        </td>
        <td style="width: 50%; text-align: center; padding-top: 10px;">
            {{ $pemda->s_namaibukotakabkot }}, {{ Carbon\Carbon::now()->locale('id')->format('d F Y') }}<br>
            KEPALA BADAN PENDAPATAN DAERAH<br>
            {{ strtoupper($pemda->s_namakabkot) }} <br><br><br><br><br><br>
            (..............................)<br>
            NIP. &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;  &nbsp;
        </td>
    </tr>
</table>

@php
@endphp