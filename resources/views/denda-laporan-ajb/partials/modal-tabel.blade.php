<div class="modal fade" id="modal-lapor-bulanan">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title">PILIH DATA</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No </th>
                                <th scope='col' style="background-color: #227dc7; color:white; width: 50px;" class="text-center">No Pelaporan</th>
                                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Nama PPAT/Notaris</th>
                                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Tgl Pelaporan</th>
                                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Laporan Bulan</th>
                                <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Aksi</th>
                            </tr>
                            <tr>
                                <th scope="filter"></th>
                                <th scope="filter">
                                    <input type="text" class="form-control form-control-sm" id="filter-no_daftar">
                                </th>
                                <th scope="filter">
                                    <input type="text" class="form-control form-control-sm" id="filter-tgl_daftar">
                                </th>
                                <th scope="filter"></th>
                                <th scope="filter"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="9"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <button type="button" class="btn btn-primary btn-sm float-right" data-dismiss="modal"><span class="fas fa-times-circle"></span> Tutup</button>
            </div>
        </div>
    </div>
</div>