<div class="form-group">
    <label for="name">No Pelaporan</label>
    <div class="row pl-2">
        <input type="hidden" name="t_idlaporbulanan" id="t_idlaporbulanan" value="{{ old('t_idlaporbulanan') ?? $data['t_idlaporbulanan'] ?? '' }}">
        <input type="text" name="t_no_lapor" id="t_no_lapor" class="form-control col-md-3" value="{{ old('t_no_lapor') ?? $data['t_no_lapor'] ?? '' }}" readonly>
        <button type="button" class="btn btn-warning ml-2" data-toggle="modal" data-target="#modal-lapor-bulanan" {{ $cari ?? '' }}>Cari</button>
    </div>
    @if ($errors->has('t_idlaporbulanan'))
    <div class="text-danger mt-2">
        {{ $errors->first('t_idlaporbulanan') }}
    </div>
    @endif
</div>

<div class="form-group">
    <label>Nama PPAT/Notaris</label>
    <input type="hidden" name="t_idnotaris" id="t_idnotaris" value="{{ old('s_namanotaris') ?? $data['t_idnotaris'] ?? '' }}">
    <input type="text" name="s_namanotaris" id="s_namanotaris" class="form-control col-md-6" value="{{ old('s_namanotaris') ?? $data['s_namanotaris'] ?? '' }}" readonly>
</div>

<div class="form-group">
    <label>Tanggal Lapor</label>
    <input type="text" name="t_tgl_lapor" id="t_tgl_lapor" class="form-control col-md-6" value="{{ old('t_tgl_lapor') ?? $data['t_tgl_lapor'] ?? '' }}" readonly>
</div>

<div class="form-group">
    <label>Bulan Laporan</label>
    <input type="text" name="t_untuk_bulan" id="t_untuk_bulan" class="form-control col-md-6" value="{{ old('t_untuk_bulan') ?? $data['t_untuk_bulan'] ?? '' }}" readonly>
</div>

<div class="form-group">
    <label>Tanggal Penetapan</label>
    <div class="input-group date col-md-6" id="tgl_penetapan" data-target-input="nearest">
        <div class="input-group-append" data-target="#tgl_penetapan" data-toggle="datetimepicker">
            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
        </div>
        <input type="text" name="t_tglpenetapan" id="t_tglpenetapan" value="{{ old('t_tglpenetapan') ?? $data['t_tglpenetapan'] ?? '' }}" class="form-control datetimepicker-input" data-target="#tgl_penetapan" />
    </div>
    @if ($errors->has('t_tglpenetapan'))
    <div class="text-danger mt-2">
        {{ $errors->first('t_tglpenetapan') }}
    </div>
    @endif
</div>

<a href="{{ url('denda-laporan-ajb') }}" class="btn btn-info"><i class="fas fa-redo"></i>
    Kembali</a>
<button type="submit" class="btn btn-primary"><i class=""></i> {{ $sumbit }}</button>