@extends('layouts.master')

@section('judulkiri')
TETAPKAN DENDA PELAPORAN BULANAN [EDIT]
@endsection

@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="card card-primary">
            <form action="{{ url('denda-laporan-ajb', $data['t_id_pendenda']) }}" method="POST">
                <div class="card-body mt-4 mb-2">
                    @method('put')
                    @csrf
                    @include('denda-laporan-ajb.partials.form', ['sumbit'=> 'Update', 'cari' => 'disabled'])
                </div>
            </form>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(() => {
        $('#tgl_penetapan').datetimepicker({
            format: 'DD-MM-YYYY'
        });
    });
</script>
@endpush