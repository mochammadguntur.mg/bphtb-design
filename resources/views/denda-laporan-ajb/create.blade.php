@extends('layouts.master')

@section('judulkiri')
TETAPKAN DENDA PELAPORAN BULANAN [TAMBAH]
@endsection

@section('content')
<div class="row">
    <div class="col-lg-6">
        <div class="card card-primary">
            <form action="{{ route('denda-laporan-ajb.store') }}" method="POST">
                <div class="card-body mt-4 mb-2">
                    @csrf
                    @include('denda-laporan-ajb.partials.form', ['sumbit'=> 'Tetapkan', 'icon' => 'fas fa-save'])
                </div>
            </form>
        </div>
    </div>
</div>
@include('denda-laporan-ajb.partials.modal-tabel')
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(() => {
        $('#tgl_penetapan').datetimepicker({
            format: 'DD-MM-YYYY'
        });

        datatables.reload();
    });

    var datatables = datagrid({
        url: 'datagrid_lapor_bulanan',
        table: "#datagrid-table",
        columns: [{
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "no_daftar"
            },
            {
                sortable: true,
                name: "tgl_daftar"
            },
            {
                sortable: true,
                name: "jenis_peralihan"
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
        ],
        action: [{
            name: 'pilih',
            btnClass: 'btn btn-info btn-sm',
            btnText: '<i class="fas fa-hand-point-up"> Pilih</i>'
        }]
    });

    function pilihData(a, b) {
        $.ajax({
            url: 'caridata/' + a,
            type: 'GET',
            data: {
                id_laporan: a,
                id_ppat: b,
            }
        }).then(function(data) {
            console.log(data);
            $('#t_idlaporbulanan').val(data.t_idlaporbulanan)
            $('#t_no_lapor').val(data.t_no_lapor)
            $('#t_idnotaris').val(data.s_idnotaris)
            $('#s_namanotaris').val(data.s_namanotaris)
            $('#t_tgl_lapor').val(data.t_tgl_lapor)
            $('#t_untuk_bulan').val(data.t_untuk_bulan)
        });

        $('#modal-lapor-bulanan').modal('hide');
        $('body').removeClass('modal-open');
        $('.modal-backdrop').remove();
    }
</script>
@endpush