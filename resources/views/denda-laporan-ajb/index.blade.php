@extends('layouts.master')

@section('judulkiri')
DATA TERLAMBAT LAPOR AJB
@endsection

@section('content')
<div class="card card-outline card-info shadow-sm">
    <div class="card-header">
        <h3 class="card-title">
        <a href="{{ url('denda-laporan-ajb/create') }}" class="btn btn-primary">
                <i class="fas fa-plus"></i> Tambah
            </a>
        </h3>
        <div class="card-tools">
            {{-- <a href="javascript:void(0)" class="btn btn-success" id="btnExcel">
                <i class="fas fa-file-excel"></i> Excel
            </a>
            <a href="javascript:void(0)" class="btn btn-danger" id="btnPDF">
                <i class="fas fa-file-pdf"></i> PDF
            </a> --}}
        </div>
    </div>
    <div class="card-body">
        <div class="table-responsive">
            <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                <thead>
                    <tr>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No </th>
                        <th scope='col' style="background-color: #227dc7; color:white; width: 80px;" class="text-center">No Pelapor</th>
                        <th scope='col' style="background-color: #227dc7; color:white; width: 100px;" class="text-center">Tgl Pelaporan</th>
                        <th scope='col' style="background-color: #227dc7; color:white; width: 170px;" class="text-center">Nama PPAT/Notaris</th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">No Penetapan Denda</th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Tgl Penetapan Denda</th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Nilai Penetapan</th>
                        <th scope='col' style="background-color: #227dc7; color:white;" class="text-center">Status Bayar</th>
                        <th scope='col' style="background-color: #227dc7; color:white; width:280px" class="text-center">Aksi</th>
                    </tr>
                    <tr>
                        <th scope="filter"></th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-no_daftar">
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-tgl_daftar">
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-notaris">
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-nama_wp">
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-no_ajb">
                        </th>
                        <th scope="filter">
                            <input type="text" class="form-control form-control-sm" id="filter-tgl_ajb">
                        </th>
                        <th scope="filter"></th>
                        <th scope="filter"></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td class="text-center" colspan="9"> Tidak ada data.</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="card-footer clearfix pagination-footer">
        </div>
    </div>
</div>
@include('denda-laporan-ajb.partials.modal-delete')
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(() => {
        if ('{{ session("success") }}') {
            toastr.success('{{ session("success") }}')
        }

        if ('{{ session("error") }}') {
            toastr.error('{{ session("error") }}')
        }

        datatables.reload();
    });

    var datatables = datagrid({
        url: 'denda-laporan-ajb/datagrid',
        table: "#datagrid-table",
        columns: [{
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "no_daftar"
            },
            {
                sortable: true,
                name: "tgl_daftar"
            },
            {
                sortable: true,
                name: "jenis_peralihan"
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },
        ],
        action: [{
                name: 'edit',
                btnClass: 'btn btn-warning btn-sm',
                btnText: '<i class="fas fa-edit"> Edit</i>'
            },
            {
                name: 'cetak-tagihan',
                btnClass: 'btn btn-success btn-sm',
                btnText: '<i class="fas fa-print"> Cetak</i>'
            },
            {
                name: 'delete',
                btnClass: 'btn btn-danger btn-sm',
                btnText: '<i class="fas fa-trash"> Hapus</i>'
            }
        ]
    });

    function showDeleteDialog(a) {
        $.ajax({
            url: `denda-laporan-ajb/detile/${ a }`,
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $("#btnHapus").removeAttr("disabled");
            $("#idHapus").val(data.t_id_pendenda);
            $("#noDaftarHapus").val(data.t_nourut_pendenda)
            $("#namaPPATHapus").val(data.s_namanotaris);
            // $("#nopHapus").val(data.spt.t_nop_sppt);
            // $("#tglAktaHapus").val(moment(data.t_tgl_ajb).format("L"));
            // $("#noAktaHapus").val(data.t_no_ajb);
        });
        $("#modal-delete").modal('show');
    }

    $("#btnHapus").click(function() {
        var id = $("#idHapus").val();

        $.ajax({
            url: "denda-laporan-ajb/" + id,
            method: "DELETE",
            data: {
                _token: "{{ csrf_token() }}",
                id: id
            }
        }).then(function(data) {
            $("#modal-delete").modal("hide");
            datatables.reload();
            toastr.success(data['success']);
        });
    });

    function cetakTagihanDenda(a) {
        window.open(`{{ url('denda-laporan-ajb/cetak_tagihan') }}?t_id_pendenda=${ a }`);
    }
</script>
@endpush