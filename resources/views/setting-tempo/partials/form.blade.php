<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Hari Tempo</label>
    <div class="col-md-4">
        <input type="text" name="s_haritempo" class="form-control @error('s_haritempo') is-invalid @enderror" id="s_haritempo" placeholder="Tempo dalam hari"
               value="{{ old('s_haritempo') ?? $tempo->s_haritempo }}"
               oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_haritempo'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_haritempo') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Tgl Berlaku Awal</label>
    <div class="col-md-4">
        <input type="date" name="s_tglberlaku_awal" class="form-control @error('s_tglberlaku_awal') is-invalid @enderror" id="s_tglberlaku_awal"
               value="{{ old('s_tglberlaku_awal') ?? $tempo->s_tglberlaku_awal }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_tglberlaku_awal'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_tglberlaku_awal') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Tgl Berlaku Akhir</label>
    <div class="col-md-4">
        <input type="date" name="s_tglberlaku_akhir" class="form-control @error('s_tglberlaku_akhir') is-invalid @enderror" id="s_tglberlaku_akhir"
               value="{{ old('s_tglberlaku_akhir') ?? $tempo->s_tglberlaku_akhir }}">
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_tglberlaku_akhir'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_tglberlaku_akhir') }}
        </div>
        @endif
    </div>
</div>
<div class="col-sm-12 row mb-2">
    <label class="col-sm-2">Status</label>
    <div class="col-md-4">
        <select name="s_id_status" class="form-control @error('s_id_status') is-invalid @enderror" id="s_id_status">
            <option value="">Silahkan Pilih</option>
            @foreach ($status as $status)
            <option {{ (old('s_id_status') ?? $tempo->s_id_status) == $status->s_id_status ? 'selected' : '' }} value="{{ $status->s_id_status }}">{{ $status->s_nama_status }}
            </option>
            @endforeach
        </select>
    </div>
    <div class="col-md-4">
        @if ($errors->has('s_id_status'))
        <div class="text-danger mt-2">
            {{ $errors->first('s_id_status') }}
        </div>
        @endif
    </div>
</div>

<a href="{{ url('setting-tempo') }}" class="btn btn-danger"><i class="fa fa-arrow-circle-left"></i> Kembali</a>
<button type="submit" class="btn btn-primary float-right"><i class="fa fa-save"></i> {{ $submit ?? 'Update' }}</button>
