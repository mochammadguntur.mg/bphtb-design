@extends('layouts.master')

@section('judulkiri')
    PENGATURAN TEMPO KODE BAYAR [EDIT]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="/setting-tempo/{{ $tempo->s_idtempo }}/edit" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @method('patch')
                        @csrf
                        @include('setting-tempo.partials.form')
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection
