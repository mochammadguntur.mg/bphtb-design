@extends('layouts.master')

@section('judulkiri')
    PENGATURAN TEMPO KODE BAYAR [TAMBAH]
@endsection

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card card-primary">
                <form action="/setting-tempo/store" method="POST">
                    <div class="card-body mt-4 mb-2">
                        @csrf
                        @include('setting-tempo.partials.form', ['submit'=> 'Simpan'])
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection