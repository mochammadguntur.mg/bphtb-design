@extends('layouts.master')

@section('judulkiri')
APPROVED ANGSURAN [TAMBAH]
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header bg-primary">
                    <h3 class="card-title">FROM APPROVED ANGSURAN</h3>
                    <div class="card-tools">
                        <a href="{{ url('approved-angsuran') }}" class="btn btn-sm btn-warning">
                            <i class="fas fa-backward"></i> Kembali
                        </a>
                    </div>
                </div>
                <form action="/approved-angsuran/store" method="POST" class="form-horizontal">
                    <div class="card-body">
                        @csrf
                        @include('approved-angsuran.partials.view')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title"><i class="fas fa-edit"></i> FORM APPROVED ANGSURAN</h3>
                                    </div>
                                    <div class="card-body">
                                        @include('approved-angsuran.partials.form', ['sumbit'=> 'Simpan'])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btnReject").click(function(){
            $("#t_idstatus_disetujui").val(2);
        })

        $("#btnApprove").click(function(){
            $("#t_idstatus_disetujui").val(1);
        })
    });

    function pilihPelayanan(a){
        $.ajax({
            url: 'caridata/{' + a + '}',
            type: 'GET',
            data: {
                id: a
            }
        }).then(function(data) {
            $('#t_idangsuran').val(data[0].t_idangsuran);
            $('#t_noangsuran').val(data[0].t_noangsuran);
            $('#t_kohirspt').val(data[0].t_kohirspt);
            $('#t_periodespt').val(data[0].t_periodespt);
            $('#t_tgldaftar_spt').val(data[0].t_tgldaftar_spt);
            $('#t_p_nama_pembeli').val(data[0].t_p_nama_pembeli);
            $('#t_nop_sppt').val(data[0].t_nop_sppt);
            $('#t_nilai_bphtb_fix').val(formatRupiah(data[0].t_nilai_bphtb_fix,0));
            $('#t_keterangan_permohonan').val(data[0].t_keterangan_permohoanan);
            $('#modal-datapelayanan').modal('hide');
        });
    }

    $("#t_jmlhkaliangsuran").keyup(function() {
        var tgldaftar = $('#t_tgldaftar_spt').val();
        var jmlkali = $('#t_jmlhkaliangsuran').val();
        var jmlpajak = $('#t_nilai_bphtb_fix').val().replace('.', '');
        if(jmlkali == '' || jmlkali == 0){
            alert('Silahkan Masukkan Jumlah Kali Angsuran!');
        }else{
            rinciAngsuran(jmlkali,jmlpajak,tgldaftar);
        }
    });

    function rinciAngsuran(a,b,c) {
        $.ajax({
            url: 'rinciangsuran/{' + a + '}/{' + b + '}/{' + c + '}',
            type: 'GET',
            data: {
                id: a,
                key: b,
                page: c
            }
        }).then(function(data) {
            $('#rincianangsuran').html(data);
        });
    }

</script>
@endpush
