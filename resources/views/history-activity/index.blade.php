@extends('layouts.master')

@section('judulkiri')
    HISTORY ACTIVITY
@endsection

@section('content')<div class="row">
    <div class="col-md-12">
        <div class="card card-outline card-info">
            <div class="card-header"></div>
            <div class="card-body">
                <!-- /.card-header -->
                <div class="table-responsive">
                    <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                        <thead>
                            <tr>
                                <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Log Name</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Description</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Subject Type</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Subject Id</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Causer Type</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Causer Name</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Properties</th>
                                <th style="background-color: #227dc7; color:white;" class="text-center">Created At</th>
                            </tr>
                            <tr>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th></th>
                                <th><input type="text" class="form-control form-control-sm" id="filter-created_at" readonly></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center" colspan="10"> Tidak ada data.</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <div class="card-footer clearfix pagination-footer">
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
        url: 'history-activity/datagrid',
        table: "#datagrid-table",
        columns: [
            {
                class: ""
            },
            {
                class: ""
            },
            {
                class: ""
            },
            {
                class: ""
            },
            {
                class: ""
            },
            {
                class: ""
            },
            {
                class: ""
            },
            {
                class: "text-center"
            }
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },{
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: false
            },{
                sortable: false
            },
            {
                sortable: false
            },
            {
                sortable: true,
                name: "created_at"
            }
        ],
        action: []

    });

    $("#filter-created_at").change(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();

    $("#btnExcel, #btnPDF").click(function(a) {
        var s_kodehaktanah = $("#filter-kodehaktanah").val();
        var s_namahaktanah = $("#filter-namahaktanah").val();
        var created_at = $("#filter-created_at").val();
        if(a.target.id == 'btnExcel'){
            window.open('/setting-hak-tanah/export_xls?s_kodehaktanah=' + s_kodehaktanah + '&s_namahaktanah=' + s_namahaktanah + '&created_at=' + created_at);
        }else{
            window.open('/setting-hak-tanah/export_pdf?s_kodehaktanah=' + s_kodehaktanah + '&s_namahaktanah=' + s_namahaktanah + '&created_at=' + created_at);
        }

    });

    $("#alert").fadeTo(2000, 500).slideUp(500, function(){
        $("#alert").slideUp(500);
    });

</script>
@endpush
