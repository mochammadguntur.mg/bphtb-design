@extends('layouts.master')

@section('judulkiri')
<h1 class="m-0">Penetapan</h1>
@endsection


@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-md-9 col-lg-9 col-sm-12">
                <div class="d-flex flex-wrap align-items-top justify-content-start mt-1">
                    @can('Skpdkb')
                    <div class="card-product mb-4 mr-4">
                        <a href="{{ url('skpdkb') }}" class="text-dark-75">
                            <div class="product-image">
                                <img src="{{ asset('dist/img/others/U.svg') }}">
                            </div>
                            <div class="info">
                                <div class="name">
                                    <p>SKPDKB</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endcan

                    @can('Skpdkbt')
                    <div class="card-product mb-4 mr-4">
                        <a href="{{ url('skpdkbt') }}" class="text-dark-75">
                            <div class="product-image">
                                <img src="{{ asset('dist/img/others/U.svg') }}">
                            </div>
                            <div class="info">
                                <div class="name">
                                    <p>SKPDKBT</p>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endcan
                    
                </div>
            </div>
            <div class="col-12 col-md-3 col-lg-3 col-sm-12 d-flex flex-column mb-4">
                <div class="flex-grow-1 bg-primary p-4 rounded-lg"
                    style="background-position: calc(100% + 0.5rem) bottom; background-size: auto 70%; background-image: url(dist/img/custom-6.svg); background-repeat: no-repeat;">

                    <h4 class="text-inverse-danger mt-2 font-weight-bolder">Penetapan</h4>

                    <p class="text-inverse-danger my-6">
                        Pemilihan penetapan <br />transaksi bphtb.
                    </p>
                </div>
            </div>
        </div>
    </div><!-- /.container-fluid -->
</section>

@endsection
@push('scripts')

<!-- /.content -->
@endpush