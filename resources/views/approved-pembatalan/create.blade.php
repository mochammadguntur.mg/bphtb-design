@extends('layouts.master')

@section('judulkiri')
APPROVED PEMBATALAN [TAMBAH]
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header bg-primary">
                    <h3 class="card-title">FROM APPROVED PEMBATALAN</h3>
                    <div class="card-tools">
                        <a href="{{ url('approved-pembatalan') }}" class="btn btn-sm btn-warning">
                            <i class="fas fa-backward"></i> Kembali
                        </a>
                    </div>
                </div>
                <form action="/approved-pembatalan/store" method="POST" class="form-horizontal">
                    <div class="card-body">
                        @csrf
                        @include('approved-pembatalan.partials.view')
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card card-outline card-primary">
                                    <div class="card-header">
                                        <h3 class="card-title"><i class="fas fa-edit"></i> FORM APPROVED PEMBATALAN</h3>
                                    </div>
                                    <div class="card-body">
                                        @include('approved-pembatalan.partials.form', ['sumbit'=> 'Simpan'])
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script type="text/javascript">
    $(document).ready(function() {
        $("#btnReject").click(function(){
            $("#t_idstatus_disetujui").val(2);
        })

        $("#btnApprove").click(function(){
            $("#t_idstatus_disetujui").val(1);
        })
    });

</script>
@endpush
