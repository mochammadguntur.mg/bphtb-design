@extends('layouts.master')

@section('judulkiri')
PILIH PERMOHONAN PEMBATALAN
@endsection

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card card-outline card-primary">
                <div class="card-header">
                    <h3 class="card-title">PILIH PERMOHONAN YANG AKAN DISETUJUI</h3>
                    <div class="card-tools">
                        <a href="{{ url('approved-pembatalan') }}" class="btn btn-sm btn-warning">
                            <i class="fas fa-backward"></i> Kembali
                        </a>
                    </div>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-striped table-hover table-sm" id="datagrid-table">
                            <thead>
                                <tr>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">No</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">No Pembatalan</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">Tgl Permohonan</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">No Kohir</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">Nama WP</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">NOP</th>
                                    <th style="background-color: #227dc7; color:white;" class="text-center">Create At</th>
                                    <th style="background-color: #227dc7; color:white; width:180px" class="text-center">Aksi</th>
                                </tr>
                                <tr>
                                    <th></th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-nopembatalan">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-tglpengajuan" readonly>
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-kohirspt">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-nama_pembeli">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-nop">
                                    </th>
                                    <th>
                                        <input type="text" class="form-control form-control-sm" id="filter-created_at" readonly>
                                    </th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center" colspan="8"> Tidak ada data.</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="card-footer clearfix pagination-footer">
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@push('scripts')
<script src="{{ asset('/datagrid/datagrid.js') }}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("#btnReject").click(function(){
            $("#t_idstatus_disetujui").val(2);
        })

        $("#btnApprove").click(function(){
            $("#t_idstatus_disetujui").val(1);
        })

        $('#filter-tglpengajuan').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-tglpengajuan').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-tglpengajuan').on('cancel.datepicker', function(ev, picker) {
            $(this).val('');
            search();
        });

        $('#filter-created_at').daterangepicker({
            autoUpdateInput: false,
            locale: {
                cancelLabel: 'Clear'
            }
        });

        $('#filter-created_at').on('apply.daterangepicker', function(ev, picker) {
            $(this).val(picker.startDate.format('DD-MM-YYYY') + ' - ' + picker.endDate.format('DD-MM-YYYY'));
            search();
        });

        $('#filter-created_at').on('cancel.daterangepicker', function(ev, picker) {
            $(this).val('');
            search();
        });
    });

    var datatables = datagrid({
    url: 'datagrid_pelayanan',
    table: "#datagrid-table",
    columns: [
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: ""
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
            {
                class: "text-center"
            },
        ],
        orders: [{
                sortable: false
            },
            {
                sortable: true,
                name: "t_nopembatalan"
            },
            {
                sortable: true,
                name: "t_tglpengajuan"
            },
            {
                sortable: true,
                name: "t_kohirspt"
            },
            {
                sortable: true,
                name: "t_nama_pembeli"
            },
            {
                sortable: true,
                name: "t_nop_sppt"
            },
            {
                sortable: true,
                name: "created_at"
            },
            {
                sortable: false
            },
        ],
    action: [{
            name: 'pilih',
            btnClass: 'btn btn-warning btn-sm',
            btnText: '<i class="fas fa-hand-point-up"> Pilih</i>'
        }
    ]

    });

    $("#filter-nopembatalan, #filter-kohirspt, #filter-nama_pembeli, #filter-nop").keyup(function() {
        search();
    });

    $("#filter-tglpengajuan, #filter-created_at").change(function() {
        search();
    });

    function search() {
        datatables.setFilters({
            nopembatalan: $("#filter-nopembatalan").val(),
            tglpengajuan: $("#filter-tglpengajuan").val(),
            kohirspt: $("#filter-kohirspt").val(),
            nama_pembeli: $("#filter-nama_pembeli").val(),
            nop: $("#filter-nop").val(),
            created_at: $("#filter-created_at").val()
        });
        datatables.reload();
    }
    search();


</script>
@endpush
